package com.paipai.c2c.api.user.ao.idl;

import com.paipai.c2c.api.util.VBossChecker;
import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::user", needInit=true)
public class UserBossApiAo
{
  @ApiProtocol(cmdid="0xf1411801L", desc="BOSS修改用户属性位类")
  class BossModifyUserProperty
  {
    BossModifyUserProperty()
    {
    }

    @ApiProtocol(cmdid="0xf1418801L", desc="BOSS修改用户属性位返回类")
    class Resp
    {

      @Field(desc="返回保留字")
      String outReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0xf1411801L", desc="BOSS修改用户属性位请求类")
    class Req
    {

      @Field(desc="bosscheck，用于检查是否来自Boss")
      VBossChecker bossChecker;

      @Field(desc="用户QQ号")
      uint32_t uin;

      @Field(desc="需要修改的用户属性位")
      uint16_t offset;

      @Field(desc="标志位：0--置用户属性位为0；1--置用户属性位为1")
      uint8_t flag;

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="调用来源")
      String source;

      @Field(desc="场景id")
      uint32_t scene;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.user.ao.idl.UserBossApiAo
 * JD-Core Version:    0.6.2
 */