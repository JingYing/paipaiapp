package com.paipai.c2c.api.item.ao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
import java.util.Vector;

@HeadApiProtocol(cPlusNamespace="c2cent::ao::itemmsg", needInit=true)
public class ItemMsgApiAo
{
  @ApiProtocol(cmdid="0x26221805L", desc="删除留言")
  class ApiDelMsg
  {
    ApiDelMsg()
    {
    }

    @ApiProtocol(cmdid="0x26221805L", desc="删除留言请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="操作者用户QQ")
      uint32_t uin;

      @Field(desc="留言ID")
      uint32_t msgID;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26228805L", desc="删除留言返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26221803L", desc="获取商品留言列表")
  class ApiGetMsgList
  {
    ApiGetMsgList()
    {
    }

    @ApiProtocol(cmdid="0x26221803L", desc="获取商品留言列表请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="用户QQ")
      uint32_t uin;

      @Field(desc="商品ID")
      String itemId;

      @Field(desc="消息状态,0为全部 2为只能查看已回复的留言和自己发表的留言")
      uint32_t msgStatus;

      @Field(desc="标记位，比如排序方法等")
      uint32_t flag;

      @Field(desc="每页大小,最多20")
      uint32_t pageSize;

      @Field(desc="页号,从0开始")
      uint32_t startIndex;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26228803L", desc="获取商品留言列表返回类")
    class Resp
    {

      @Field(desc="留言总数")
      uint32_t totalCount;

      @Field(desc="留言Po列表")
      Vector<ItemMsgApiAo.ApiItemMsg> vecApiItemMsgPo;

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26221801L", desc="获取单条留言")
  class ApiGetSingleMsg
  {
    ApiGetSingleMsg()
    {
    }

    @ApiProtocol(cmdid="0x26221801L", desc="获取单条留言请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="用户QQ")
      uint32_t uin;

      @Field(desc="消息ID")
      uint32_t msgID;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26228801L", desc="获取单条留言返回类")
    class Resp
    {

      @Field(desc="留言Po")
      ItemMsgApiAo.ApiItemMsg apiItemMsgPo;

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @Member(desc="留言Po", cPlusNamespace="c2cent::po::itemmsg", isNeedUFlag=false)
  class ApiItemMsg
  {

    @Field(desc="版本", defaultValue="20100601")
    uint32_t version;

    @Field(desc="留言id")
    uint32_t msgID;

    @Field(desc="留言状态")
    uint32_t msgStatus;

    @Field(desc="外部相关主题id（商品id）")
    String relateSubjectID;

    @Field(desc="外部相关主题名称（商品名称）")
    String relateSubjectName;

    @Field(desc="留言发起者id（qq号码）")
    uint32_t questionerUin;

    @Field(desc="留言发起者昵称")
    String questionerNickName;

    @Field(desc="留言回复者id（qq号码）")
    uint32_t replierUin;

    @Field(desc="留言回复者昵称")
    String replierNickName;

    @Field(desc="留言内容")
    String questionContent;

    @Field(desc="留言时间")
    int questionTimestamp;

    @Field(desc="回复内容")
    String replyContent;

    @Field(desc="回复时间")
    int replyTimestamp;

    ApiItemMsg()
    {
    }
  }

  @ApiProtocol(cmdid="0x26221802L", desc="发表留言")
  class ApiPostMsg
  {
    ApiPostMsg()
    {
    }

    @ApiProtocol(cmdid="0x26221802L", desc="发表留言请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="操作者用户QQ")
      uint32_t uin;

      @Field(desc="被留言的商品ID")
      String itemId;

      @Field(desc="留言内容")
      String msgContent;

      @Field(desc="留言是否隐藏")
      uint32_t hideFlag;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26228802L", desc="发表留言返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x26221804L", desc="回复留言")
  class ApiReplyMsg
  {
    ApiReplyMsg()
    {
    }

    @ApiProtocol(cmdid="0x26221804L", desc="回复留言请求类")
    class Req
    {

      @Field(desc="机器码")
      String machineKey;

      @Field(desc="来源")
      String source;

      @Field(desc="场景id,预留")
      uint32_t scene;

      @Field(desc="操作者用户QQ")
      uint32_t uin;

      @Field(desc="留言ID")
      uint32_t msgID;

      @Field(desc="回复的留言内容")
      String replyContent;

      @Field(desc="留言是否隐藏")
      uint32_t hideFlag;

      @Field(desc="请求保留字")
      String inReserve;

      Req()
      {
      }
    }

    @ApiProtocol(cmdid="0x26228804L", desc="回复留言返回类")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String errmsg;

      @Field(desc="返回保留字")
      String reserve;

      Resp()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.item.ao.idl.ItemMsgApiAo
 * JD-Core Version:    0.6.2
 */