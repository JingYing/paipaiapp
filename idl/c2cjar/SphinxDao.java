package com.paipai.c2c.api.sphinx.dao.idl;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;

@HeadApiProtocol(cPlusNamespace="c2cent::dao::sphinx", needInit=true)
public class SphinxDao
{
  @ApiProtocol(cmdid="0x69051802L", desc="获取商品的排序因子priority")
  class GetCommodityPriority
  {
    GetCommodityPriority()
    {
    }

    @ApiProtocol(cmdid="0x69058802L", desc="GetCommodityPriority resp")
    class Resp
    {

      @Field(desc="返回的商品排序因子值")
      int Priority;

      @Field(desc="错误信息,用于debug")
      String ErrMsg;

      @Field(desc="返回保留字")
      String OutReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x69051802L", desc="GetCommodityPriority req")
    class Req
    {

      @Field(desc="商品id")
      uint64_t CommodityID;

      @Field(desc="商品spuid")
      uint64_t SpuID;

      @Field(desc="卖家QQ")
      uint32_t Uin;

      @Field(desc="请求来源")
      String Source;

      @Field(desc="请求保留字")
      String InReserve;

      Req()
      {
      }
    }
  }

  @ApiProtocol(cmdid="0x69051801L", desc="设置商品的排序因子priority")
  class SetCommodityPriority
  {
    SetCommodityPriority()
    {
    }

    @ApiProtocol(cmdid="0x69058801L", desc="SetCommodityPriority resp")
    class Resp
    {

      @Field(desc="错误信息,用于debug")
      String ErrMsg;

      @Field(desc="返回保留字")
      String OutReserve;

      Resp()
      {
      }
    }

    @ApiProtocol(cmdid="0x69051801L", desc="SetCommodityPriority req")
    class Req
    {

      @Field(desc="商品id")
      uint64_t CommodityID;

      @Field(desc="商品spuid")
      uint64_t SpuID;

      @Field(desc="卖家QQ")
      uint32_t Uin;

      @Field(desc="要设置新的商品排序因子值")
      int NewPriority;

      @Field(desc="请求来源")
      String Source;

      @Field(desc="请求保留字")
      String InReserve;

      Req()
      {
      }
    }
  }
}

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.sphinx.dao.idl.SphinxDao
 * JD-Core Version:    0.6.2
 */