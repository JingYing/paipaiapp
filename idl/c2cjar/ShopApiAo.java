/*     */ package com.paipai.c2c.api.shop.ao.idl;
/*     */ 
/*     */ import com.paipai.c2c.api.item.ao.idl.ItemApiAo.ApiItem;
/*     */ import com.paipai.lang.uint32_t;
/*     */ import com.paipai.lang.uint64_t;
/*     */ import com.paipai.lang.uint8_t;
/*     */ import com.paipai.util.annotation.ApiProtocol;
/*     */ import com.paipai.util.annotation.Field;
/*     */ import com.paipai.util.annotation.HeadApiProtocol;
/*     */ import com.paipai.util.annotation.Member;
/*     */ import java.util.Vector;
/*     */ 
/*     */ @HeadApiProtocol(cPlusNamespace="c2cent::ao::shop", needInit=true, needReset=true)
/*     */ public class ShopApiAo
/*     */ {
/*     */   @ApiProtocol(cmdid="0x30141803L", desc="查询店铺友情链接")
/*     */   class ApiGetFriendShop
/*     */   {
/*     */     ApiGetFriendShop()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141803L", desc="查询店铺友情链接请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店主Uin")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="过滤器", cPlusNamespace="ApiShopActionInfo|c2cent::po::shop")
/*     */       ShopApiAo.ApiShopFriendFilter shopFriendFilterPo;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148803L", desc="查询店铺友情链接返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="店铺友情链接列表", cPlusNamespace="ApiShopFriend|c2cent::po::shop")
/*     */       Vector<ShopApiAo.ApiShopFriend> shopFriend;
/*     */ 
/*     */       @Field(desc="总数")
/*     */       uint32_t totalSize;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141806L", desc="获取店铺推荐商品信息")
/*     */   class ApiGetRecommedComdyList
/*     */   {
/*     */     ApiGetRecommedComdyList()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141806L", desc="获取店铺推荐信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店铺id")
/*     */       uint32_t shopID;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148806L", desc="获取店铺推荐信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="返回的商品信息列表", cPlusNamespace="ApiItem|c2cent::po::item")
/*     */       Vector<ItemApiAo.ApiItem> recommedComdyInfoList;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141802L", desc="获取店铺活动信息")
/*     */   class ApiGetShopActionInfo
/*     */   {
/*     */     ApiGetShopActionInfo()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141802L", desc="获取店铺活动信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店主Uin")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148802L", desc="获取店铺活动信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="店铺活动信息", cPlusNamespace="ApiShopActionInfo|c2cent::po::shop")
/*     */       Vector<ShopApiAo.ApiShopActionInfo> shopActionInfo;
/*     */ 
/*     */       @Field(desc="总数")
/*     */       uint32_t totalSize;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141804L", desc="获取公告页部分")
/*     */   class ApiGetShopAnnounceInfoByUin
/*     */   {
/*     */     ApiGetShopAnnounceInfoByUin()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141804L", desc="获取公告页部分请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店主Uin")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148804L", desc="获取公告页部分返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="店铺公告信息", cPlusNamespace="ApiShopAnnounce|c2cent::po::shop")
/*     */       Vector<ShopApiAo.ApiShopAnnounce> shopAnnounce;
/*     */ 
/*     */       @Field(desc="总数")
/*     */       uint32_t totalSize;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141801L", desc="获取店铺信息")
/*     */   class ApiGetShopInfo
/*     */   {
/*     */     ApiGetShopInfo()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141801L", desc="获取店铺信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店主Uin")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148801L", desc="获取店铺信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="店铺信息", cPlusNamespace="ApiShopInfo|c2cent::po::shop")
/*     */       ShopApiAo.ApiShopInfo shopInfo;
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺活动信息po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopActionInfo
/*     */   {
/*     */ 
/*     */     @Field(desc="po版本")
/*     */     uint32_t pOVersion;
/*     */ 
/*     */     @Field(desc="店铺id")
/*     */     uint32_t shopID;
/*     */ 
/*     */     @Field(desc="活动id")
/*     */     uint32_t actionID;
/*     */ 
/*     */     @Field(desc="属性 取值只有0和1，0表示不展示，1表示展示")
/*     */     uint32_t property;
/*     */ 
/*     */     @Field(desc="顺序号 用于排序")
/*     */     uint32_t sequence;
/*     */ 
/*     */     @Field(desc="图片规格")
/*     */     uint32_t style;
/*     */ 
/*     */     @Field(desc="一行展示几个图片")
/*     */     uint32_t dispLinage;
/*     */ 
/*     */     @Field(desc="活动名称")
/*     */     String title;
/*     */ 
/*     */     @Field(desc="活动描述")
/*     */     String description;
/*     */     uint8_t pOVersion_u;
/*     */     uint8_t shopID_u;
/*     */     uint8_t actionID_u;
/*     */     uint8_t property_u;
/*     */     uint8_t sequence_u;
/*     */     uint8_t style_u;
/*     */     uint8_t dispLinage_u;
/*     */     uint8_t title_u;
/*     */     uint8_t description_u;
/*     */ 
/*     */     public ApiShopActionInfo()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺公告po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopAnnounce
/*     */   {
/*     */ 
/*     */     @Field(desc="po版本号")
/*     */     uint32_t pOVersion;
/*     */ 
/*     */     @Field(desc="店铺id")
/*     */     uint32_t shopID;
/*     */ 
/*     */     @Field(desc="公告id")
/*     */     uint32_t announceID;
/*     */ 
/*     */     @Field(desc="公告属性")
/*     */     uint32_t property;
/*     */ 
/*     */     @Field(desc="公告顺序号 用于排序")
/*     */     uint32_t sequence;
/*     */ 
/*     */     @Field(desc="公告标题")
/*     */     String title;
/*     */     uint8_t pOVersion_u;
/*     */     uint8_t shopID_u;
/*     */     uint8_t announceID_u;
/*     */     uint8_t property_u;
/*     */     uint8_t sequence_u;
/*     */     uint8_t title_u;
/*     */ 
/*     */     public ApiShopAnnounce()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺审核后数据po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopCheckDoneInfo
/*     */   {
/*     */ 
/*     */     @Field(desc="版本号", defaultValue="20101216")
/*     */     uint32_t poVersion;
/*     */ 
/*     */     @Field(desc="店铺id，即qq号")
/*     */     uint32_t shopID;
/*     */ 
/*     */     @Field(desc="店铺模块唯一标识，模块化店铺,修改店名时该字段不填")
/*     */     uint64_t fsid;
/*     */ 
/*     */     @Field(desc="更新内容类型，店名:1 店招:2 ....")
/*     */     uint32_t updateType;
/*     */ 
/*     */     @Field(desc="图片格式，更新店招的时候指定，GIF:0 JPG:1")
/*     */     uint32_t imageType;
/*     */ 
/*     */     @Field(desc="更新的数据，店名或者是模块的标题")
/*     */     String name;
/*     */ 
/*     */     @Field(desc="更新的数据，比如店铺招牌的图片内容、店铺自定义内容区的内容等!目前只用于店招的图片内容!")
/*     */     String data;
/*     */ 
/*     */     @Field(desc="预留字段")
/*  51 */     String reserve = "";
/*     */     uint8_t poVersion_u;
/*     */     uint8_t shopID_u;
/*     */     uint8_t fsid_u;
/*     */     uint8_t updateType_u;
/*     */     uint8_t imageType_u;
/*     */     uint8_t name_u;
/*     */     uint8_t data_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     public ApiShopCheckDoneInfo()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺友情链接po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopFriend
/*     */   {
/*     */ 
/*     */     @Field(desc="版本号")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="")
/*     */     uint32_t innerID;
/*     */ 
/*     */     @Field(desc="用户id")
/*     */     uint32_t userID;
/*     */ 
/*     */     @Field(desc="友情店铺id")
/*     */     uint32_t friendShopID;
/*     */ 
/*     */     @Field(desc="友情用户id")
/*     */     uint32_t friendUserID;
/*     */ 
/*     */     @Field(desc="添加时间")
/*     */     uint32_t addTime;
/*     */ 
/*     */     @Field(desc="友情店铺名")
/*     */     String shopName;
/*     */ 
/*     */     @Field(desc="友情店铺logo")
/*     */     String logoName;
/*     */ 
/*     */     @Field(desc="友情店铺店铺类型")
/*     */     String shopType;
/*     */ 
/*     */     @Field(desc="保留字")
/*     */     String reserve;
/*     */     uint8_t version_u;
/*     */     uint8_t innerID_u;
/*     */     uint8_t userID_u;
/*     */     uint8_t friendShopID_u;
/*     */     uint8_t friendUserID_u;
/*     */     uint8_t addTime_u;
/*     */     uint8_t shopName_u;
/*     */     uint8_t logoName_u;
/*     */     uint8_t shopType_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     public ApiShopFriend()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺友情链接过滤器po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopFriendFilter
/*     */   {
/*     */ 
/*     */     @Field(desc="版本号")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="友情店铺id")
/*     */     uint32_t friendShopID;
/*     */ 
/*     */     @Field(desc="每页大小")
/*     */     uint32_t pageSize;
/*     */ 
/*     */     @Field(desc="页号")
/*     */     uint32_t startIndex;
/*     */ 
/*     */     @Field(desc="排序方式")
/*     */     uint32_t orderType;
/*     */ 
/*     */     @Field(desc="预留")
/*     */     String type;
/*     */ 
/*     */     @Field(desc="关键字")
/*     */     String keyWord;
/*     */ 
/*     */     @Field(desc="")
/*     */     String reserve;
/*     */     uint8_t version_u;
/*     */     uint8_t friendShopID_u;
/*     */     uint8_t pageSize_u;
/*     */     uint8_t startIndex_u;
/*     */     uint8_t orderType_u;
/*     */     uint8_t type_u;
/*     */     uint8_t keyWord_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     public ApiShopFriendFilter()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @Member(desc="店铺信息po", cPlusNamespace="c2cent::po::shop", isNeedUFlag=true)
/*     */   public class ApiShopInfo
/*     */   {
/*     */ 
/*     */     @Field(desc="版本号", defaultValue="20100531")
/*     */     uint32_t poVersion;
/*     */ 
/*     */     @Field(desc="店铺id，即qq号")
/*     */     uint32_t shopID;
/*     */ 
/*     */     @Field(desc="店主qq号")
/*     */     uint32_t ownerUin;
/*     */ 
/*     */     @Field(desc="注册时间")
/*     */     uint32_t regTime;
/*     */ 
/*     */     @Field(desc="店铺属性 1代表店铺2.0")
/*     */     uint32_t property;
/*     */ 
/*     */     @Field(desc="店名更新次数")
/*     */     uint32_t nameUpdateTimes;
/*     */ 
/*     */     @Field(desc="店名更新时间")
/*     */     uint32_t nameUpdTime;
/*     */ 
/*     */     @Field(desc="店铺已推荐商品数量")
/*     */     uint32_t recommendNum;
/*     */ 
/*     */     @Field(desc="店铺出售中商品数(已废弃)")
/*     */     uint32_t commCount;
/*     */ 
/*     */     @Field(desc="店铺自定义类型对多值")
/*     */     uint32_t categoryIndex;
/*     */ 
/*     */     @Field(desc="店铺属性标记 已废弃")
/*     */     uint32_t flag;
/*     */ 
/*     */     @Field(desc="店铺颜色风格")
/*     */     uint32_t shopStyle;
/*     */ 
/*     */     @Field(desc="版本号")
/*     */     uint32_t version;
/*     */ 
/*     */     @Field(desc="店铺最后更新时间")
/*     */     uint32_t lastUpdTime;
/*     */ 
/*     */     @Field(desc="自定义分类最后更新时间")
/*     */     uint32_t cateGoryLastUpdTime;
/*     */ 
/*     */     @Field(desc="友情链接最后更新时间")
/*     */     uint32_t friendLinkLastUpdTime;
/*     */ 
/*     */     @Field(desc="公告最后更新时间")
/*     */     uint32_t announceLastUpdTime;
/*     */ 
/*     */     @Field(desc="店铺活动最后更新时间")
/*     */     uint32_t actionLastUpdTime;
/*     */ 
/*     */     @Field(desc="店铺允许设置的最大橱窗数")
/*     */     uint32_t shopWindowNum;
/*     */ 
/*     */     @Field(desc="店铺审核版本号(给Boss使用)")
/*     */     uint32_t auditVersion;
/*     */ 
/*     */     @Field(desc="店铺审核结果位")
/*     */     uint32_t auditResult1;
/*     */ 
/*     */     @Field(desc="店铺审核结果位,预留")
/*     */     uint32_t auditResult2;
/*     */ 
/*     */     @Field(desc="店铺审核标记位(给Boss使用)")
/*     */     uint32_t auditFlag1;
/*     */ 
/*     */     @Field(desc="店铺审核标记位(给Boss使用)")
/*     */     uint32_t auditFlag2;
/*     */ 
/*     */     @Field(desc="废弃")
/*     */     uint32_t customadFlag;
/*     */ 
/*     */     @Field(desc="废弃")
/*     */     uint32_t customactFlag;
/*     */ 
/*     */     @Field(desc="店铺名称")
/* 152 */     String shopName = "";
/*     */ 
/*     */     @Field(desc="店铺Logo")
/* 155 */     String mainLogoName = "";
/*     */ 
/*     */     @Field(desc="店铺介绍TSS文件名")
/* 158 */     String shopIntroName = "";
/*     */ 
/*     */     @Field(desc="店铺招牌TSS文件名")
/* 161 */     String shopSignName = "";
/*     */ 
/*     */     @Field(desc="店铺活动区域TSS文件名")
/* 164 */     String actionAreaName = "";
/*     */ 
/*     */     @Field(desc="店铺类别")
/* 167 */     String shopType = "";
/*     */ 
/*     */     @Field(desc="店铺主营区域")
/* 170 */     String mainArea = "";
/*     */ 
/*     */     @Field(desc="自定义html代码存放文件名称")
/* 173 */     String customHtmlName = "";
/*     */ 
/*     */     @Field(desc="店铺公告")
/* 176 */     String annouce = "";
/*     */ 
/*     */     @Field(desc="店铺描述")
/* 179 */     String description = "";
/*     */ 
/*     */     @Field(desc="预留字段")
/* 182 */     String reserve = "";
/*     */     uint8_t poVersion_u;
/*     */     uint8_t shopID_u;
/*     */     uint8_t ownerUin_u;
/*     */     uint8_t regTime_u;
/*     */     uint8_t property_u;
/*     */     uint8_t nameUpdateTimes_u;
/*     */     uint8_t nameUpdTime_u;
/*     */     uint8_t recommendNum_u;
/*     */     uint8_t commCount_u;
/*     */     uint8_t categoryIndex_u;
/*     */     uint8_t flag_u;
/*     */     uint8_t shopStyle_u;
/*     */     uint8_t version_u;
/*     */     uint8_t lastUpdTime_u;
/*     */     uint8_t cateGoryLastUpdTime_u;
/*     */     uint8_t friendLinkLastUpdTime_u;
/*     */     uint8_t announceLastUpdTime_u;
/*     */     uint8_t actionLastUpdTime_u;
/*     */     uint8_t shopWindowNum_u;
/*     */     uint8_t auditVersion_u;
/*     */     uint8_t auditResult1_u;
/*     */     uint8_t auditResult2_u;
/*     */     uint8_t auditFlag1_u;
/*     */     uint8_t auditFlag2_u;
/*     */     uint8_t customadFlag_u;
/*     */     uint8_t customactFlag_u;
/*     */     uint8_t shopName_u;
/*     */     uint8_t mainLogoName_u;
/*     */     uint8_t shopIntroName_u;
/*     */     uint8_t shopSignName_u;
/*     */     uint8_t actionAreaName_u;
/*     */     uint8_t shopType_u;
/*     */     uint8_t mainArea_u;
/*     */     uint8_t customHtmlName_u;
/*     */     uint8_t annouce_u;
/*     */     uint8_t description_u;
/*     */     uint8_t reserve_u;
/*     */ 
/*     */     @Field(desc="热点信息")
/*     */     Vector<String> vecHotKey;
/*     */ 
/*     */     @Field(desc="热点信息_u")
/*     */     uint8_t vecHotKey_u;
/*     */ 
/*     */     @Field(desc="店铺最后更新时间")
/*     */     uint32_t shopTypeLastUpdTime;
/*     */ 
/*     */     @Field(desc="店铺类型更新次数")
/*     */     uint32_t shopTypeUpdateTimes;
/*     */ 
/*     */     @Field(desc="店铺最后更新时间_u")
/*     */     uint8_t shopTypeLastUpdTime_u;
/*     */ 
/*     */     @Field(desc="店铺类型更新次数_u")
/*     */     uint8_t shopTypeUpdateTimes_u;
/*     */ 
/*     */     public ApiShopInfo()
/*     */     {
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141807L", desc="更新店铺已审核信息")
/*     */   class ApiUpdateShopCheckDoneInfo
/*     */   {
/*     */     ApiUpdateShopCheckDoneInfo()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141807L", desc="更新店铺已审核信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店铺已审核信息", cPlusNamespace="ApiShopCheckDoneInfo|c2cent::po::shop")
/*     */       ShopApiAo.ApiShopCheckDoneInfo shopcheckdoneinfo;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148807L", desc="更新店铺已审核信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ 
/*     */   @ApiProtocol(cmdid="0x30141805L", desc="修改店铺信息")
/*     */   class ApiUpdateShopInfo
/*     */   {
/*     */     ApiUpdateShopInfo()
/*     */     {
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30141805L", desc="修改店铺信息请求类")
/*     */     class Req
/*     */     {
/*     */ 
/*     */       @Field(desc="机器码")
/*     */       String machineKey;
/*     */ 
/*     */       @Field(desc="来源,必填")
/*     */       String source;
/*     */ 
/*     */       @Field(desc="场景id")
/*     */       uint32_t scene;
/*     */ 
/*     */       @Field(desc="店主Uin")
/*     */       uint32_t uin;
/*     */ 
/*     */       @Field(desc="店铺信息", cPlusNamespace="ApiShopInfo|c2cent::po::shop")
/*     */       ShopApiAo.ApiShopInfo shopInfo;
/*     */ 
/*     */       @Field(desc="请求保留字")
/*     */       String inReserve;
/*     */ 
/*     */       Req()
/*     */       {
/*     */       }
/*     */     }
/*     */ 
/*     */     @ApiProtocol(cmdid="0x30148805L", desc="修改店铺信息返回类")
/*     */     class Resp
/*     */     {
/*     */ 
/*     */       @Field(desc="错误信息,用于debug")
/*     */       String errmsg;
/*     */ 
/*     */       @Field(desc="返回保留字")
/*     */       String outReserve;
/*     */ 
/*     */       Resp()
/*     */       {
/*     */       }
/*     */     }
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.idl.ShopApiAo
 * JD-Core Version:    0.6.2
 */