package bi.userlink;

import java.util.Map;
import java.util.Vector;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;

@HeadApiProtocol(cPlusNamespace = "bi::userlink", needInit = true, needLib = false, needReset = true)
public class PaipaiUserLinkAO {
    @ApiProtocol(cmdid = "0x86111801L", desc = "Get UserLink info")
        class GetUserLinkInfo {
            @ApiProtocol(cmdid = "0x86111801L", desc = "get UserLink req")    
                class Req {
                    @Field(desc = "版本号, 必填, 默认值1")
                        uint32_t version;

                    @Field(desc = "来源")
                        String source;

                    @Field(desc = "场景ID, 必填, 默认0, 以后扩展")
                        uint64_t sceneId;

                    @Field(desc = "UserID,必填,用户ID")
                        uint64_t uin;

                    @Field(desc = "Req number of links")
                        uint32_t reqnum;

                    @Field(desc = "req reserve")
                        Map<String, String> reserveIn;
                }
            @ApiProtocol(cmdid = "0x86118801L", desc = "get UserLink resp")
                class Resp {
                    @Field(desc = "")
                        uint32_t errCode;

                    @Field(desc = "错误信息")
                        String errMsg;

                    @Field(desc = "user id list")
                        Vector<uint64_t> userIDList;

                    @Field(desc = "weight list")
                        Vector<uint32_t> UserWeightList;

                    @Field(desc = "resp number of user link")
                        uint32_t respNum;

                    @Field(desc = "rsp reserve")
                        Map<String, String> reserveOut;
                }
        }
}
