package com.qq.qqbuy.user.util;

import java.util.Set;

import com.paipai.lang.uint16_t;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;
import com.qq.qqbuy.user.po.User;

/**
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class UserHelper {
	/**
	 * 获取用户的详细信息
	 * @param uin
	 * @return
	 * @date:2013-6-20
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static User getUserInfo(long uin) {
		APIUserProfile profile = ApiUserClient.getUserSimpleInfo(uin);
		if (profile != null) {
			User userInfo = new User();
			userInfo.setUin(profile.getUserId());
			userInfo.setNickName(profile.getNickName());
			userInfo.setSex(profile.getSex());
			userInfo.setEmail(profile.getEmail());
			userInfo.setBuyerCredit(profile.getBuyerCredit());
			userInfo.setSellerCredit(profile.getSellerCredit());
			userInfo.setAuthMask(profile.getAuthMask());
			userInfo.setLoginLevel(profile.getLoginLevel());
			userInfo.setCity(profile.getCity());
			userInfo.setSproperties(profile.getSproperty());
			StringBuffer properties = new StringBuffer();
			if (profile.getProp().size() > 0) {
				Set<uint16_t> set = profile.getProp().keySet();
				for (uint16_t key : set) {
					properties.append(key).append("_").append(
							profile.getProp().get(key)).append("|");
				}

				properties.deleteCharAt(properties.length() - 1);
			}
			userInfo.setProperties(properties.toString());

			String[] split = profile.getContacts().split("\\|");
			if ((split != null) && (split.length >= 1)) {
				for (String str : split) {
					String[] tm = str.split(":");
					if ("0".equals(tm[0]))
						userInfo.setTelephone(tm[1]);
					else if ("1".equals(tm[0]))
						userInfo.setTelephoneAfter(tm[1]);
					else if ("2".equals(tm[0]))
						userInfo.setMobileAfter(tm[1]);
					else if ("3".equals(tm[0]))
						userInfo.setTelephoneBefore(tm[1]);
					else if ("4".equals(tm[0])) {
						userInfo.setMobileBefore(tm[1]);
					}
				}
			}
			
			//TODO 店铺属性
			uint16_t key = new uint16_t(50);
			if (profile.getProp().get(key) != null)
				userInfo.setShopType(profile.getProp().get(key).intValue());
			else
				userInfo.setShopType(0);
			
			return userInfo;
		}
		return null;
	}
}
