package com.qq.qqbuy.user.po;

import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;

public class UserCenterPo extends CommonPo	{

    /**
     * 商品信息
     *
     * 版本 >= 0
     */
	FavoriteItemsPo favoriteItemsPo = new FavoriteItemsPo();
	
    /**
     * 店铺信息
     *
     * 版本 >= 0
     */
    FavoriteShopDetails FavoriteShopDetails = new FavoriteShopDetails();
    
    /**
     * 商品信息
     *
     * 版本 >= 0
     */
    GetUserTagsRespPo getUserTagsRespPo = new GetUserTagsRespPo();

    @Override
	public String toString() {
		return "FavoriteItemsPo [favoriteItemsPo=" + favoriteItemsPo
				+ ",FavoriteShopDetails=" + FavoriteShopDetails
				+ ",GetUserTagsRespPo=" + getUserTagsRespPo + ", errCode="
				+ errCode + ", msg=" + msg + ", retCode=" + retCode + "]";
	}

	public FavoriteItemsPo getFavoriteItemsPo() {
		return favoriteItemsPo;
	}

	public void setFavoriteItemsPo(FavoriteItemsPo favoriteItemsPo) {
		this.favoriteItemsPo = favoriteItemsPo;
	}

	public FavoriteShopDetails getFavoriteShopDetails() {
		return FavoriteShopDetails;
	}

	public void setFavoriteShopDetails(FavoriteShopDetails favoriteShopDetails) {
		FavoriteShopDetails = favoriteShopDetails;
	}

	public GetUserTagsRespPo getGetUserTagsRespPo() {
		return getUserTagsRespPo;
	}

	public void setGetUserTagsRespPo(GetUserTagsRespPo getUserTagsRespPo) {
		this.getUserTagsRespPo = getUserTagsRespPo;
	}
}
