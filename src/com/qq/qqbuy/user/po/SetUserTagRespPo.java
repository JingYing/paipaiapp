package com.qq.qqbuy.user.po;

import java.util.Vector;

import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiExt;

public class SetUserTagRespPo extends CommonPo{
	

	/**
	 * 标签id组
	 *
	 * 版本 >= 0
	 */
	 private Vector<Long> tagIds = new Vector<Long>();


	/**
	 * 获取标签信息
	 * 
	 */

	public Vector<Long> getTagIds() {
		return tagIds;
	}




	/**
	 * 设置标签信息
	 * 
	 */

	public void setTagIds(Long tagId) {
		this.tagIds.add(tagId);
	}


}
