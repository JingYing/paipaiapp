/**   
 * @Title: UserBizImpl.java 
 * @Package com.qq.qqbuy.user.biz.impl 
 * @Description: TODO
 * @author Wendyhu wendyhu@tencent.com  
 * @date 2012-5-30 下午7:38:32 
 * @version V1.0   
 */
package com.qq.qqbuy.user.biz.impl;

import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.thirdparty.idl.interestTags.InterestTagsClient;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.GetUserTagsReq;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.GetUserTagsResp;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiTag;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiUser;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.SetUserTagReq;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.SetUserTagResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.FindBuyerVIPAccountResp;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetFlagLevel4SingleResp;
import com.qq.qqbuy.user.biz.UserBiz;
import com.qq.qqbuy.user.po.ColorDiamondInfo;
import com.qq.qqbuy.user.po.GetUserTagsRespPo;
import com.qq.qqbuy.user.po.SetUserTagRespPo;
import com.qq.qqbuy.user.po.User;
import com.qq.qqbuy.user.util.UserHelper;

/**
 * @ClassName: UserBizImpl
 * @Description: TODO
 * @author wendyhu wendyhu@tencent.com
 * @date 2012-5-30 下午7:38:32
 * 
 */
public class UserBizImpl implements UserBiz {

	/**
	 * 用户服务
	 */
	private UserInfoClient client = new UserInfoClient();

	/*
	 * (非 Javadoc) <p>Title: getLevel</p> <p>Description: </p>
	 * 
	 * @param uin 用户qq号码
	 * 
	 * @return 返回超Q等级 ，0 代表没有
	 * 
	 * @see com.qq.qqbuy.user.biz.UserBiz#getLevel(long)
	 */
	@Override
	public int getSuperQQLevel(long uin) throws BusinessException {
		int level = 0;
		GetFlagLevel4SingleResp resp = client.getSuperQQLevel(uin);
		if (resp == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_SUPERQQ_FAIL,
					"getSuperQQLevel error ,uin[" + uin + "]");
		}
		if (resp.getErrCode() == 0) {
			level = resp.getFlagLevel();
		}
		return level;
	}

	@Override
	public ColorDiamondInfo getColorDiamondLevel(long uin)
			throws BusinessException {
		ColorDiamondInfo colorDiamondInfo = new ColorDiamondInfo();
		FindBuyerVIPAccountResp resp = client.findBuyerVIPAccount(uin);
		if (resp == null || resp.getResult() != 0) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_COLORDIEMOND_FAIL,
					"getColorDiamondLevel error ,uin[" + uin + "]");
		}
		if (resp.getErrCode() == 0) {
			colorDiamondInfo.setColorDiamondLevel(resp.getBuyerVIPAccountInfo()
					.getDwLevel());
			colorDiamondInfo.setColorDiamondState(resp.getBuyerVIPAccountInfo()
					.getDwState());
		}
		return colorDiamondInfo;
	}

	// @Override
	// public GetUserDetailInfoResponse getUserDetail(long uin) throws
	// BusinessException {
	// GetUserDetailInfoRequest req = new GetUserDetailInfoRequest();
	// req.setUin(uin);
	// OpenApiProxy proxy = OpenApi.getProxy();
	// return proxy.getUserDetailInfo(req);
	// }

	@Override
	public User getUserDetailInfo(long uin) throws BusinessException {
		User user = UserHelper.getUserInfo(uin);
		if (user == null) {
			throw BusinessException.createInstance(
					ErrConstant.ERRCODE_QUERY_USER_FAIL,
					"getUserDetail error ,uin[" + uin + "]");
		}
		return user;
	}
	
	@Override
	public SetUserTagRespPo setUserTag(Long wid,String mk,Vector<String> tagIds) {
		MyPaipaiUser userInfo=new MyPaipaiUser();
		userInfo.setDdwUin(wid);
		userInfo.setStrVisitKey(mk);
		
		SetUserTagReq req=new SetUserTagReq ();
		req.setMachineKey(mk);
		req.setSceneId(2);
		req.setSource("com.qq.qqbuy.interestTags.biz.impl.ApiInterestTagsBizImpl.setUserTag");
		req.setUserInfo(userInfo);
		
		Vector<uint32_t> vec=new Vector<uint32_t>();
		for(String id:tagIds){
			uint32_t uin=new uint32_t();
			uin.setValue(id);
			vec.add(uin);
		}
		req.setTagIds(vec);
		// 1、调用idl
		SetUserTagResp  resq=InterestTagsClient.setUserTag(req);
		// 2、返回数据
		SetUserTagRespPo respPo=new SetUserTagRespPo();
		respPo.msg=resq.getErrMsg();
		respPo.errCode=resq.getResult();
		
		Vector<MyPaipaiTag> list=resq.getTagInfo();
		for(MyPaipaiTag tag:list){
			respPo.setTagIds(tag.getDwTagId());
		}
		
				
		return respPo;
	}

	@Override
	public GetUserTagsRespPo getUserTag(Long wid,String mk) {
		
		MyPaipaiUser userInfo=new MyPaipaiUser();
		userInfo.setDdwUin(wid);
		userInfo.setStrVisitKey(mk);
		
		GetUserTagsReq req=new GetUserTagsReq ();
		req.setMachineKey(mk);
		req.setSceneId(2);
		req.setSource("com.qq.qqbuy.interestTags.biz.impl.ApiInterestTagsBizImpl.getUserTag");
		req.setUserInfo(userInfo);
		
		
		// 1、调用idl
		GetUserTagsResp resp=InterestTagsClient.getUserTag(req);
		// 2、返回数据
		GetUserTagsRespPo respPo=new GetUserTagsRespPo();
		
		respPo.errCode=resp.getResult();
	
		respPo.msg=resp.getErrMsg();
		
		
		Vector<MyPaipaiTag>  vec=resp.getTagInfo();
		for(MyPaipaiTag tag:vec){
			respPo.setTagIds(tag.getDwTagId());
		}
		return respPo;
	}

}
