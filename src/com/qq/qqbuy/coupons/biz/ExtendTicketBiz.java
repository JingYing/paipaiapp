package com.qq.qqbuy.coupons.biz;


import java.util.Date;
import java.util.List;

import com.qq.qqbuy.coupons.model.ExtendTicketPo;

/**
 * 

 * @ClassName: ExtendTicketBiz 

 * @Description: 发券的方法接口
 
 * @author lianghaining1@jd.com

 * @date 2015-2-4 下午1:43:37 

 *
 */
public interface ExtendTicketBiz {
	/**
	 * @param allnum 
	 * @param deviceid 
	 * @param packetstockid 
	 * 
	
	 * @Title: getExpBywid 
	
	 * @Description: 根据wid批次号以及设备号
	
	 * @param @param wid
	 * @param @return    设定文件 
	
	 * @return ExtendTicketPo    返回类型 
	
	 * @throws
	 */
	ExtendTicketPo getExpBywid(String packetstockid, String deviceid,boolean target);
	/**
	 * @param busineStringssid 
	 * 
	
	 * @Title: sendTicket 
	
	 * @Description: 进行发券
	
	 * @param @param wid
	 * @param @param packetstockid
	 * @param @param deviceid
	 * @param @param httpHeadIp
	 * @param @param endtime
	 * @param @param allnum
	 * @param @return    设定文件 
	
	 * @return ExtendTicketPo    返回类型 
	
	 * @throws
	 */
	String sendTicket(Long wid, String packetstockid, String deviceid,
			String httpHeadIp, Long endtime, int allnum, String businessid,String commondata,String sk);
	/**
	 * 
	
	 * @Title: saveExp 
	
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	
	 * @param @param i
	 * @param @param packetstockid1
	 * @param @param string
	 * @param @param string2
	 * @param @param endtime
	 * @param @param allnum
	 * @param @param businessid
	 * @param @param string3    设定文件 
	
	 * @return void    返回类型 
	
	 * @throws
	 */
	void saveExp(Long i, String packetstockid1, String string, String string2,
			Long endtime, int allnum, String businessid, String con);
	/**
	 * 
	 * @Title: getLimitExp 
	 * @Description: 查出要限制的信息
	 * @param @param limitPacket1
	 * @param @param limitPacket2
	 * @param @param mk
	 * @param @return    设定文件 
	 * @return ExtendTicketPo    返回类型 
	 * @throws
	 */
	List<ExtendTicketPo> getLimitExp(String limitPacket1, String limitPacket2,
			String mk);

}
