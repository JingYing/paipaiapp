package com.qq.qqbuy.coupons.biz.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.coupons.biz.ExtendTicketBiz;
import com.qq.qqbuy.coupons.model.ExtendTicketPo;
import com.qq.qqbuy.redpacket.po.RedpacketSendPo;
import com.qq.qqbuy.virtualgoods.action.TimeUtils;

/**
 * 

 * @ClassName: ExtendTicketBizImpl 

 * @Description: 发券实现 
 
 * @author lianghaining1@jd.com

 * @date 2015-2-4 下午1:44:22 

 *
 */
public class ExtendTicketBizImpl implements ExtendTicketBiz {
	
	private JdbcTemplate sendTicketJdbcTemplate;
	//发券的卖家号
	private static final int seller = 393462;
	/**
	 * 
	 * @Title: getExpBywid 
	 * @Description:根据设备id查出该设备是否已经领取了
	 * @param @param wid
	 * @param @param packetstockid
	 * @param @param deviceid
	 * @param @return    设定文件 
	 * @return ExtendTicketPo    返回类型 
	 * @throws
	 */
	@Override
	public ExtendTicketPo getExpBywid(String packetstockid, String deviceid,boolean target) {
			Log.run.info("getExpBywid------------------");
				List<ExtendTicketPo> list = sendTicketJdbcTemplate.query(
						"select * from pp_app_sendticketinfo where packetstockid= ? and deviceid=? and target = ?",
						new Object[] { packetstockid, deviceid,target==true?1:0}, new SendTicketPoRowMapper());
				if (list != null && list.size() > 0) {
					Log.run.info("getExpBywidgetWid------------------"+list.get(0).getWid());
					return list.get(0) ;
				} else {
					Log.run.info("getExpBywidlist------------------"+packetstockid+"deviceid:"+deviceid+"target:"+target);
					return null ;
				}
	}
	
	class SendTicketPoRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			ExtendTicketPo extendTicketPo = new ExtendTicketPo();
			extendTicketPo.setBusinessid(rs.getString("businessid"));
			extendTicketPo.setWid(rs.getLong("wid")) ;
			extendTicketPo.setDeviceid(rs.getString("deviceid"));
			extendTicketPo.setTarget(rs.getBoolean("target"));
			extendTicketPo.setPacketstockid(rs.getString("packetstockid"));
			extendTicketPo.setEndtime(rs.getLong("endtime"));
			extendTicketPo.setCommondata(rs.getString("commondata"));
			extendTicketPo.setCreatetime(rs.getLong("createtime"));
			extendTicketPo.setReserve1(rs.getString("reserve1"));
			extendTicketPo.setReserve1(rs.getString("reserve2"));
			extendTicketPo.setGetnum(rs.getInt("getnum"));
			return extendTicketPo;
		}
	}
	
	//该算法是用于从skey中解释中出token;
	private static int time33(String skey) {
        char str[] = skey.toCharArray();
        //哈希time33算法
        int len = str.length;
        int hash = 5381;
        for (int i = 0; i < len; ++i) {
            hash += (hash << 5) + str[i];
        }
        return hash & 0x7fffffff;
    }
	
	
	/**
	 * 发券
	 */
	/*返回内容：	try{ShopCouponGet({"uin":"","iRet":"","packetid":"","packetkey":"","selleruin":"","name":"","maxissue":"","maxhave":"","begintime":"","endtime":"","maxexpierdays":"","minimum":"","facevalue":"","msg":""});
	}catch(e){}
	iRet = 0 成功
	     uin 为买家号
	iRet = 999 用户未登录
	iRet = 100000001 参数有误
	iRet = 1806  店铺优惠券过期
	iRet = 10001  非法参数
	iRet = 1000001 批量店铺优惠券必须为同一卖家
	iRet = 1000002 店铺优惠券已发完
	iRet = 1000003 个人领取达到上限  
	iRet = 其他  系统错误
	*/
	@Override
	public String sendTicket(Long wid, String packetstockid,
			String deviceid, String httpHeadIp, Long endtime, int allnum,String businessid,String commondata,String sk) {
		//将数据写入数据库记录,先组装一下数据呗^_^
		ExtendTicketPo extendTicketPo = new ExtendTicketPo();
		extendTicketPo.setBusinessid(businessid);
		extendTicketPo.setWid(wid);
		extendTicketPo.setGetnum(1);
		extendTicketPo.setDeviceid(deviceid);
		extendTicketPo.setTarget(false);
		extendTicketPo.setPacketstockid(packetstockid);
		extendTicketPo.setEndtime(endtime);
		extendTicketPo.setCommondata(commondata);
		extendTicketPo.setCreatetime(System.currentTimeMillis());
	   String retCode = "";
	   String uin = "";
		//----组装好吗？好了，那就发券吧。
		//调用http接口
		 String url="http://party.paipai.com/cgi-bin/v2/shopcoupon_get?seller="+seller+"&packetid="+packetstockid+"&g_tk="+time33(sk)+"&g_ty=ls";	
	    //String url = "http://act.paipai.com/docoupon/DispenseCoupon?t="+System.currentTimeMillis()+"&uin="+wid+"&couponid="+packetstockid+"&logintype=1&sendtype=2&source=bjppapp-wuxian&selleruin=393462";
		Log.run.info("sendTicketurl:"+url+"sk:"+sk);
		try {
			String recoresult = HttpUtil.get(url, 10000, 10000, "gbk", true," skey="+sk+"; wg_uin=" + wid + "; wg_skey=" + sk+"; uin="+wid+";");
			Log.run.info("----------sendTicket发券结果：recoresult"+recoresult);
			if(StringUtil.isEmpty(recoresult)){
				return null;
			}else {
				String st = "try{ShopCouponGet(";
				//String st = "try{SendRedpacket(";
				String ed = ");}catch(e){}";
				int stn = recoresult.indexOf(st);
				int edn = recoresult.indexOf(ed);
				//如果没有jsonp中的特殊字段则返回null
				if (stn >= 0 && edn > 0) {
					recoresult = recoresult.substring(stn + st.length(), edn);
					//取出json中的字段
					// 取出库存里的东西
					JsonParser jsonParser = new JsonParser();
					JsonElement json= jsonParser.parse(recoresult);
					//retCode  = json.getAsJsonObject().get("retCode").getAsString();
					retCode  = json.getAsJsonObject().get("iRet").getAsString();
					uin  = json.getAsJsonObject().get("uin").getAsString();
					if(retCode!=null&&retCode.equals("0")){
						try {
							this.saveExp(wid, packetstockid, deviceid, httpHeadIp, endtime, allnum, businessid, commondata);
						} catch (Exception e) {
							//如果保存失败再次保存
							Log.run.error("发券保存数据库失败了");
						}
					}
					return retCode;
				}else {
					return null;
				}
			}
		} catch (Exception e) {
			Log.run.error("sendTicket 错误：e:msg"+e.getMessage());
			return null;
		}finally{
			Log.run.info("sendTicket wid:"+wid+"uin:"+uin+" packetstockid "+packetstockid+"  retCode:"+retCode);
		}
	}

	

	@Override
	public void saveExp(Long i, String packetstockid, String mk,
			String ip, Long endtime, int allnum, String businessid,
			String con) {
		sendTicketJdbcTemplate.update("insert into pp_app_sendticketinfo (businessid,wid,deviceid,getnum,target,packetstockid,endtime"
				+ ",commondata,createtime,reserve1,reserve2) values ("
				+ "?,?,?,?,0,?,?,?,?,?,?)",new Object[]{businessid,i,mk,1,packetstockid,endtime,"",System.currentTimeMillis(),"",""});
	}

	@Override
	public List<ExtendTicketPo> getLimitExp(String limitPacket1, String limitPacket2,
			String deviceid) {
		List<ExtendTicketPo> list = sendTicketJdbcTemplate.query(
				"select * from pp_app_sendticketinfo where (packetstockid= ? or packetstockid=?) and deviceid=? ",
				new Object[] { limitPacket1,limitPacket2, deviceid}, new SendTicketPoRowMapper());
		return list;
	}



	
	
	//--------------------------------get set ----------------------------------------------------------------------
	public JdbcTemplate getSendTicketJdbcTemplate() {
		return sendTicketJdbcTemplate;
	}

	public void setSendTicketJdbcTemplate(JdbcTemplate sendTicketJdbcTemplate) {
		this.sendTicketJdbcTemplate = sendTicketJdbcTemplate;
	}




	
}
