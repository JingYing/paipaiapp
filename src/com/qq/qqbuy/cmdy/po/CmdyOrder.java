package com.qq.qqbuy.cmdy.po;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.paipai.lang.uint64_t;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.CommodityInformation;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.DealInformation;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.OrderInformation;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ShopInformation;

/**
 * @author winsonwu
 * @Created 2013-2-4
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyOrder {
	/********************************************* 交易信息 ***************************************/
	// 运送方式
	private int transportType;
	// 促销规则id
	private int promotionRuleId;
	// 订单类型
	private int dealType;
	// 指定费用类型
	private int specialFeeType;
	// 指定费用
	private int specialFee;
	// 套餐id
	private String comboId;
	// 订单留言
	private String dealNote;
	// 发票类型
	private int invoiceType;
	// 发票title
	private String invoiceTitle;
	// 店铺优惠券
	private long couponId;
	
	/********************************************* 店铺信息 ****************************************/
	// 卖家uin
	private long sellerUin;
	// 卖家属性
	private int sellerProperty;
	
	/**********************************************优惠信息**************************************/
	// 包邮卡
	private long freeMailCardId;
	// 代金券
	private long onlineShoppingCoupons;
	// 平台积分
	private long redBagScore;

	/********************************************* 商品信息 ****************************************/
	private List<CmdyItem> cmdyItems = new ArrayList<CmdyItem>();
	
	/********************************************* 对象转换区 **************************************/
	public OrderInformation toIDLOrderInformation() {
		OrderInformation orderInfo = new OrderInformation();
		
		// 交易信息
		DealInformation dealInfo = new DealInformation();
		dealInfo.setComboId(comboId);
		dealInfo.setDealNote(dealNote);
		dealInfo.setDealType(dealType);
		dealInfo.setInvoiceTitle(invoiceTitle);
		dealInfo.setInvoiceType(invoiceType);
		dealInfo.setPromotionRuleId(promotionRuleId);
		dealInfo.setSpecialFee(specialFee);
		dealInfo.setSpecialFeeType(specialFeeType);
		dealInfo.setTransportType(transportType);
		
		// add by candela 2013-09-13
		dealInfo.setFreeMailCardId(freeMailCardId);
		dealInfo.setOnlineShoppingCoupons(onlineShoppingCoupons);
		// end
		// add by wanghao 2015-=3-18
		dealInfo.setRedBagScore(redBagScore);
		//end

		if(couponId > 0){
		    Vector<uint64_t> couponIds = new Vector<uint64_t>();
		    couponIds.add(new uint64_t(couponId));
		    dealInfo.setCouponId(couponIds);
		}
		orderInfo.setDealInfo(dealInfo);
		
		// 店铺信息
		ShopInformation shopInfo = new ShopInformation();
		shopInfo.setSellerProperty(sellerProperty);
		shopInfo.setSellerUin(sellerUin);
		orderInfo.setShopInfo(shopInfo);
		
		// 商品信息列表
		if (cmdyItems != null && cmdyItems.size() > 0) {
			Vector<CommodityInformation> cmdyInfos = new Vector<CommodityInformation>();
			for (CmdyItem cmdyItem : cmdyItems) {
				cmdyInfos.add(cmdyItem.toIDLCommodityInformation());
			}
			orderInfo.setCmdyInfo(cmdyInfos);
		}
		
		return orderInfo;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CmdyOrder [cmdyItems=");
		builder.append(cmdyItems);
		builder.append(", comboId=");
		builder.append(comboId);
		builder.append(", dealNote=");
		builder.append(dealNote);
		builder.append(", dealType=");
		builder.append(dealType);
		builder.append(", invoiceTitle=");
		builder.append(invoiceTitle);
		builder.append(", invoiceType=");
		builder.append(invoiceType);
		builder.append(", promotionRuleId=");
		builder.append(promotionRuleId);
		builder.append(", sellerProperty=");
		builder.append(sellerProperty);
		builder.append(", sellerUin=");
		builder.append(sellerUin);
		builder.append(", specialFee=");
		builder.append(specialFee);
		builder.append(", specialFeeType=");
		builder.append(specialFeeType);
		builder.append(", transportType=");
		builder.append(transportType);
		builder.append("]");
		return builder.toString();
	}

	public int getTransportType() {
		return transportType;
	}

	public void setTransportType(int transportType) {
		this.transportType = transportType;
	}

	public int getPromotionRuleId() {
		return promotionRuleId;
	}

	public void setPromotionRuleId(int promotionRuleId) {
		this.promotionRuleId = promotionRuleId;
	}

	public int getDealType() {
		return dealType;
	}

	public void setDealType(int dealType) {
		this.dealType = dealType;
	}

	public int getSpecialFeeType() {
		return specialFeeType;
	}

	public void setSpecialFeeType(int specialFeeType) {
		this.specialFeeType = specialFeeType;
	}

	public int getSpecialFee() {
		return specialFee;
	}

	public void setSpecialFee(int specialFee) {
		this.specialFee = specialFee;
	}

	public String getComboId() {
		return comboId;
	}

	public void setComboId(String comboId) {
		this.comboId = comboId;
	}

	public String getDealNote() {
		return dealNote;
	}

	public void setDealNote(String dealNote) {
		this.dealNote = dealNote;
	}

	public int getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(int invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public int getSellerProperty() {
		return sellerProperty;
	}

	public void setSellerProperty(int sellerProperty) {
		this.sellerProperty = sellerProperty;
	}

	public long getFreeMailCardId() {
		return freeMailCardId;
	}

	public void setFreeMailCardId(long freeMailCardId) {
		this.freeMailCardId = freeMailCardId;
	}

	public long getOnlineShoppingCoupons() {
		return onlineShoppingCoupons;
	}

	public void setOnlineShoppingCoupons(long onlineShoppingCoupons) {
		this.onlineShoppingCoupons = onlineShoppingCoupons;
	}

	public List<CmdyItem> getCmdyItems() {
		return cmdyItems;
	}

	public void setCmdyItems(List<CmdyItem> cmdyItems) {
		this.cmdyItems = cmdyItems;
	}

    public long getCouponId() {
        return couponId;
    }

    public void setCouponId(long couponId) {
        this.couponId = couponId;
    }

	public long getRedBagScore() {
		return redBagScore;
	}

	public void setRedBagScore(long redBagScore) {
		this.redBagScore = redBagScore;
	}
}
