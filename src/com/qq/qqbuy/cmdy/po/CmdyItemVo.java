package com.qq.qqbuy.cmdy.po;

import java.util.List;

import com.qq.qqbuy.item.util.DispFormater;

/**
 * @author winsonwu
 * @Created 2013-2-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyItemVo {

	private long sceneId = 0;
	
	private String itemTitle;

	private String itemCode;

	private String itemAttr;

	// Decode后的商品库存属性， 用于展示 TODO winson
	private String decodeAttr;

	private long price;
	
	private long normalPrice;

	private String disPrice;

	private long buyNum;

	private String mainLogoUrl;

	// 最大购买数量
	private long maxNum;

	// 支持货到付款
	private boolean supportCod;

	// 商品总价
	private long totalPrice;

	// 商品中总价展示
	private String disTotalPrice;

	private long sellerUin;

	private String shopName;
	private long redPacketValue;
	
	private long errCode ;
	
	private String errDesc ;

	private MutiPriceVo mostLowPrice;

	private List<MutiPriceVo> mutiPrices;

	public List<MutiPriceVo> getMutiPrices() {
		return mutiPrices;
	}

	public void setMutiPrices(List<MutiPriceVo> mutiPrices) {
		this.mutiPrices = mutiPrices;
	}

	public long lastCalculateTotalPrice() {
		long total = price * buyNum;

		totalPrice = total;
		disTotalPrice = DispFormater.priceDispFormater(total);

		return total;
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getDisPrice() {
		return disPrice;
	}

	public void setDisPrice(String disPrice) {
		this.disPrice = disPrice;
	}

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public String getMainLogoUrl() {
		if (!mainLogoUrl.contains(".80x80") && !mainLogoUrl.contains(".120x120")) {
			mainLogoUrl = mainLogoUrl.replace(".jpg", ".120x120.jpg");
		}
		return mainLogoUrl;
	}

	public void setMainLogoUrl(String mainLogoUrl) {
		this.mainLogoUrl = mainLogoUrl;
	}

	public String getDecodeAttr() {
		return decodeAttr;
	}

	public void setDecodeAttr(String decodeAttr) {
		this.decodeAttr = decodeAttr;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDisTotalPrice() {
		return disTotalPrice;
	}

	public void setDisTotalPrice(String disTotalPrice) {
		this.disTotalPrice = disTotalPrice;
	}

	public long getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(long maxNum) {
		this.maxNum = maxNum;
	}

	public boolean isSupportCod() {
		return supportCod;
	}

	public void setSupportCod(boolean supportCod) {
		this.supportCod = supportCod;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public MutiPriceVo getMostLowPrice() {
		return mostLowPrice;
	}

	public void setMostLowPrice(MutiPriceVo mostLowPrice) {
		this.mostLowPrice = mostLowPrice;
	}

	public long getRedPacketValue() {
		return redPacketValue;
	}

	public void setRedPacketValue(long redPacketValue) {
		this.redPacketValue = redPacketValue;
	}

	public long getErrCode() {
		return errCode;
	}

	public void setErrCode(long errCode) {
		this.errCode = errCode;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public long getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(long normalPrice) {
		this.normalPrice = normalPrice;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}

	
	
}
