package com.qq.qqbuy.cmdy.action;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.struts2.ServletActionContext;

import com.paipai.component.configcenter.itil.ItilReporter;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.CartMsg;
import com.qq.qqbuy.bi.biz.OrderMsg;
import com.qq.qqbuy.cmdy.biz.CmdyBiz;
import com.qq.qqbuy.cmdy.po.CmdyItem;
import com.qq.qqbuy.cmdy.po.CmdyListVo;
import com.qq.qqbuy.cmdy.po.DealInfo;
import com.qq.qqbuy.common.ChannelParam;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.constant.BusinessErrConstants;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.ItilConstants;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.biz.QueryDealBiz;
import com.qq.qqbuy.deal.po.ConfirmOrderPo;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.deal.po.DealCmdyInfo;
import com.qq.qqbuy.deal.po.DealDetailInfo;
import com.qq.qqbuy.deal.po.MakeOrderPo;
import com.qq.qqbuy.deal.po.MakeOrderVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.tenpay.biz.IPayBiz;
import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.OrderView;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPo_v2;

/**
 * Sample测试url： http://localhost:8080/webapp_o2o_v1/cn/cmdy/add.xhtml?ic=
 * EEAC6E370000000004010000000090C2&attr=&bc=2&debug=true
 * http://localhost:8080/webapp_o2o_v1
 * /cn/cmdy/add.xhtml?ic=EEAC6E370000000004010000000090C9
 * &attr=dadada:dadfdf|sss:ddddddddddddddd|ere:ss&bc=1
 * http://localhost:8080/webapp_o2o_v1/cn/cmdy/list.xhtml?debug=true
 * http://localhost:8080/webapp_o2o_v1/cn/cmdy/modifyNum.xhtml?debug=true&ic=
 * EEAC6E370000000004010000000090C2&bc=3&attr=
 * http://localhost:8080/webapp_o2o_v1
 * /cn/cmdy/remove.xhtml?debug=true&itemList=EEAC6E370000000004010000000090C2--3
 * 
 * 货到付款 http://localhost:8080/webapp_o2o_v1/cn/cmdy/add.xhtml?ic=7
 * AD78C4600000000007A3B100000FCCD&attr=&bc=2&debug=true
 * 
 * @Created 2013-2-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ApiCartAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = -3053893505653695688L;
	private CmdyListVo listVo;
	private ConfirmOrderVo confirmVo;
	private Vector<MakeOrderVo> makeOrderVos;
	private IPayBiz payBiz;
	private PayTokenInfo payTokenInfo;
	private String dealsStr = "";
	private String dealList = "[]";
	private String packageList = "[]";
	private long totalFee = 0;
	private long secenId = 0;//场景ID，默认=0
	/**
	 * useRedPackage=1表示使用红包进行批价，useRedPackage=0表示不用红包批价
	 */
	private int useRedPackage = 0;
	private int ver = 1;
	private BiBiz biBiz;

	/**
	 * 订单操作场景
	 * DealConstant.DEAL_SCENE_INITIALIZE = 1; //初始化订单
	 * DealConstant.DEAL_SCENE_CHANGERECVADDR = 2; //切换收货地址
	 * DealConstant.DEAL_SCENE_CHANGEPROMOTIONS = 3; //切换优惠活动
	 * DealConstant.DEAL_SCENE_CHANGEPAYTYPE = 4; //切换支付方式
	 */
	private int scene = 0;
	/**
	 * 订单串，在购物车订单切换优惠活动场景时传如
	 */
	private String dealStrs = "";
	/**
	 * 订单查询服务
	 */
    private QueryDealBiz queryDealBiz;
    
	public String getPackageList() {
		return packageList;
	}

	public void setPackageList(String packageList) {
		this.packageList = packageList;
	}

	public String getDealList() {
		return dealList;
	}

	public void setDealList(String dealList) {
		this.dealList = dealList;
	}

	//
	// /**************************** 注入参数 ***************************/
	private CmdyBiz cmdyBiz;

	public CmdyBiz getCmdyBiz() {
		return cmdyBiz;
	}

	public void setCmdyBiz(CmdyBiz cmdyBiz) {
		this.cmdyBiz = cmdyBiz;
	}

	public CmdyListVo getListVo() {
		return listVo;
	}

	public void setListVo(CmdyListVo listVo) {
		this.listVo = listVo;
	}

	public String viewCmdy() {
		HttpServletRequest request=getRequest();  
        
		String strBackUrl = "http://" + request.getServerName() //服务器地址  
		                    + ":"   
		                    + request.getServerPort()           //端口号  
		                    + request.getContextPath()      //项目名称  
		                    + request.getServletPath()      //请求页面或其他地址  
		                + "?" + (request.getQueryString()); //参数  
		Log.run.info("=====HttpServletRequest"+getWid()+"=================="+strBackUrl);
		
		
		
		Log.run.debug("start viewCmdy");
		try {
			// 鉴权
			if (!checkLogin()) {
				result.setErrCode(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL);
				result.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if (!validateUserInfo()) {
				return RST_FAILURE;
			}
			listVo = cmdyBiz.list(getWid());
			Log.run.info("=====listVo"+getWid()+"=================="+strBackUrl);
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#viewCmdy", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getClass().getSimpleName());
			return RST_FAILURE;
		}  
	}

	private String oldItem ;
	private String newItem ;
	
	public String modifyCmdyV2() {
		try {
			// 鉴权
			if (!checkLogin()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if (StringUtils.isEmpty(oldItem) || StringUtils.isEmpty(newItem)) {
				return RST_FAILURE;
			}
			String[] oldItemStrs = oldItem.split("\\$") ;
			String[] newItemStrs = newItem.split("\\$");
			if (newItemStrs.length<2 || oldItemStrs.length < 2 || !newItemStrs[0].equals(oldItemStrs[0])) {
				throw BusinessException.createInstance(
						ErrConstant.ERRCODE_INVALID_PARAMETER, "商品参数错误");
			}
			if (newItemStrs[1].equals(oldItemStrs[1])) {
				cmdyBiz.modifyItemNum(getWid(), newItem);
			} else {
				CmdyItem item = new CmdyItem();
				item.setItemCode(newItemStrs[0]);
				item.setItemAttr(newItemStrs[1]);
				item.setBuyNum(StringUtil.toInt(newItemStrs[2], 1));
				cmdyBiz.add(getWid(), item.getItemCode(), item.getItemAttr(), item.getBuyNum(), true, secenId);
				cmdyBiz.remove(getWid(), payType, oldItem);
				try {
					CartMsg msg = CartMsg.build(getWid(), item.getItemCode(), item.getBuyNum(), getHttpHeadIp(), 
							ServletActionContext.getRequest().getRemotePort(), 
							new ChannelParam(getChannel()).getPprdP());
					biBiz.uploadMsg(msg);
				} catch (Exception e) {
					Log.run.error(e.getMessage(), e);
				}	
			}			
			if (result.getErrCode() == ErrConstant.SUCCESS) {
				viewCmdy();
			}		
					
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#addCmdy", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		}  
	}
	
	private String itemCode;
	private String itemAttr;
	private int buyNum;
	private int totalNum;

	public String addCmdy() {
		
		try {
			// 鉴权
			if (!checkLogin()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.ADD_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}

			cmdyBiz.add(getWid(), itemCode, itemAttr, buyNum, true, secenId);
			
try {
				CartMsg msg = CartMsg.build(getWid(), itemCode, buyNum, getHttpHeadIp(), 
						ServletActionContext.getRequest().getRemotePort(), 
						new ChannelParam(getChannel()).getPprdP());
				biBiz.uploadMsg(msg);
			} catch (Exception e) {
				Log.run.error(e.getMessage(), e);
			}
			
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#addCmdy", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		}  
	}

	private int payType;
	private String itemList;// itemCode-itemAttr-buyNum~itemCode1-itemAttr1-buyNum1~

	public String rmvCmdy() {
		
		try {
			// 鉴权
			if (!checkLogin()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if ((!validateInput(BizConstant.REMOVE_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}
			cmdyBiz.remove(getWid(), payType, itemList);

			if (result.getErrCode() == ErrConstant.SUCCESS) {
				viewCmdy();
			}
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#rmvCmdy", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		} 
	}

	private int adid; // 收货地址id

	public String modifyCmdy() {
		
		try {
			// 鉴权
			if (!checkLogin()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if ((!validateInput(BizConstant.MODIFY_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}

			cmdyBiz.modifyItemNum(getWid(), itemList);
			if (result.getErrCode() == ErrConstant.SUCCESS) {
				viewCmdy();
			}
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#modifyCmdy", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		}  
	}

	public String viewCmdyNum() {
		
		try {
			// 鉴权
			if (!checkLogin()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			totalNum = cmdyBiz.cartNum(getWid());
			return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_FAILURE;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#viewCmdyNum", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		}  
	}

	/**
	 * 购物车确认订单
	 * 
	 * @return
	 */
	public String confirmOrder() {
		
		try {
			// 鉴权
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.CONFIRM_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}
			ConfirmOrderPo po = new ConfirmOrderPo(getWid(), payType, itemList, adid);
			po.setSk(getSk());
			po.setVer(ver);

			confirmVo = cmdyBiz.confirmOrderCmdy(po, getSk());
			JsonConfig jsoncfg = new JsonConfig();
			packageList = JSONSerializer.toJSON(confirmVo, jsoncfg).toString();
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		}  
	}

	/**
	 * 购物车确认订单
	 * 
	 * @return
	 */
	public String confirmOrderV2() {
		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_REQUEST, 1);

		
		try {
			// 鉴权
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.CONFIRM_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}
			ConfirmOrderPo po = new ConfirmOrderPo(getWid(), payType, itemList, adid);
			po.setSk(getSk());
			po.setMk(getMk());
			po.setVer(ver);
			po.setUseRedPackage(useRedPackage);//是否使用红包
			DealFavorVo favorVo = cmdyBiz.confirmOrderCmdyV2(po, getWid(), this.getPPSkey());
			setErrCodeAndMsg(favorVo.getErrorCode(), favorVo.getErrorMsg());
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "errorCode", "errorMsg", "queryPromoteAgain" };
			jsoncfg.setExcludes(excludes);
			packageList = JSONSerializer.toJSON(favorVo, jsoncfg).toString();
			Log.run.info("packageList#favorVo:"+packageList);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_BUSINESS_FAILED, 1);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.info(e.toString()) ;
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_FAILED, 1);
			return RST_FAILURE;
		}  
	}

	/**
	 * 购物车确认订单
	 * 合并收货地址接口+confirmOrderV2+getMultiCod
	 * 购物车下单中的所有操作都调用该接口，包括
	 * 初次到订单页面 : scene = DealConstant.DEAL_SCENE_INITIALIZE
	 * 切换订单收货地址 : scene = DealConstant.DEAL_SCENE_CHANGERECVADDR
	 * 切换优惠活动 : scene = DealConstant.DEAL_SCENE_CHANGEPROMOTIONS
	 * 切换支付方式 : scene = DealConstant.DEAL_SCENE_CHANGEPAYTYPE
	 * 在线支付时 : payType = 0
	 * 货到付款时 : payType = 1
	 * 初始化场景、切换支付方式和切换收货地址场景时会重新分单，需要传参itemList
	 * 切换优惠活动场景时不会重新分单传入参数dealstrs
	 * dealStrs和queryPromote接口的dealList格式一致
	 * @return
	 */
	public String confirmOrderV3() {
		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_REQUEST, 1);

		
		try {
			// 鉴权
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.CONFIRM_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}
			//应用场景参数校验
			if(scene < DealConstant.DEAL_SCENE_INITIALIZE || scene > DealConstant.DEAL_SCENE_CHANGEPAYTYPE){
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "订单场景参数不正确");
				return RST_FAILURE;
			}
			//当应用场景是切换优惠活动时校验dealStrs
			if(scene == DealConstant.DEAL_SCENE_CHANGEPROMOTIONS && StringUtil.isEmpty(dealStrs)){
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				return RST_FAILURE;
			}
			// 查询收货地址列表，无收货地址时抛出异常码给前端
			RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
			RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, this.getPPSkey());
			if (addrList == null || addrList.getAddressList() == null || addrList.getAddressList().size() <= 0) {
				throw BusinessException.createInstance(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
			}
			//adid不大于0时取列表第一个作为收货地址
			ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(addrList.getAddressList(), adid);
			if (defaultAddr == null) {
				throw BusinessException.createInstance(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址");
			}
			ConfirmOrderPo po = new ConfirmOrderPo(getWid(), payType, itemList, adid);
			po.setSk(getSk());
			po.setMk(getMk());
			po.setVer(ver);
			po.setUseRedPackage(useRedPackage);//是否使用红包
			String MobileNo = defaultAddr.getMobile();
			if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
				defaultAddr.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
			}
			
			DealFavorVo favorVo = cmdyBiz.confirmOrderCmdyV3(po, getWid(), this.getPPSkey(), scene, dealStrs, addrList, defaultAddr);
			setErrCodeAndMsg(favorVo.getErrorCode(), favorVo.getErrorMsg());
			
			//去掉，提供统一接口
//			try {
//				if(Integer.parseInt(getVersionCode()) > 320){
//					/**
//					 * 用户类型
//					 * 1=QQ
//					 * 2=WX
//					 * 3=JD
//					   1、qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿)为1
//					   2、wx账号非绑定qq(login_type=wx切uin>39亿)
//					   3、JD账号
//					 */
//					int uType = 1;
//					long uin = getWid();
//					if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_QQ) || getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WT)){
//						uType = 1;
//					}
//					if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WX)){
//						if(uin < 3900000000L){
//							uType = 1;
//						}else{
//							uType = 2;
//						}
//					}
//					if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_JD)){
//						uType = 3;
//					}
//					
//					
//					List<String> itemCodeList = new ArrayList<String>();
//					List<String> sellerIdList = new ArrayList<String>();
//					List<String> categoryList = new ArrayList<String>();
//					List<String> supportcodList = new ArrayList<String>();
//					
//					Vector<TradeFavorVo> dealList = favorVo.getDealList();
//					for (TradeFavorVo tradeFavorVo : dealList) {
//						Vector<ItemPromoteResultVo> itemPromoteResultVos = tradeFavorVo.getItemPromoteResult();
//						for (ItemPromoteResultVo itemPromoteResultVo : itemPromoteResultVos) {
//							itemCodeList.add(itemPromoteResultVo.getItemId());
//							sellerIdList.add(tradeFavorVo.getSellerUin() + "");
//							categoryList.add("1");//全是实物商品
//							supportcodList.add("2");//...........要支持货到付款
//						}
//					}
//					
//					Vector<PayRecommend> payRecommendList = cmdyBiz.getPayRecommends(getMk(), getWid(), uType, favorVo.getTotalPrice(),
//							StringUtils.join(itemCodeList, ","), StringUtils.join(sellerIdList, ","), 
//							StringUtils.join(categoryList, ","), StringUtils.join(supportcodList, ","));
//					
//					favorVo.setPayRecommendList(payRecommendList);
//				}
//			} catch (Exception e) {
//				Log.run.error("购物车批价--支付桥出错" + e.getMessage(), e);
//			}
			
			
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "errorCode", "errorMsg", "queryPromoteAgain" };
			jsoncfg.setExcludes(excludes);
			packageList = JSONSerializer.toJSON(favorVo, jsoncfg).toString();
			Log.run.info("packageList#favorVo:"+packageList);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_BUSINESS_FAILED, 1);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiCartAction#confirmOrderV3#error",e);
			LogManager.getLogger(ApiCartAction.class).info("ApiCartAction#confirmOrderV3#error",e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_ORDERCOMFIRM_FAILED, 1);
			return RST_FAILURE;
		}  
	}
	
	private String orderStrList; // 分单后下单的包裹列表信息:
	// dealType~promotionRuleId~sellerUin~transportType~itemCode-itemAttr-buyNum-priceType~item1-itemAttr1-buyNum1-priceType1
	private long sellerUin = 0;

	public Vector<MakeOrderVo> getMakeOrderVos() {
		return makeOrderVos;
	}

	public void setMakeOrderVos(Vector<MakeOrderVo> makeOrderVos) {
		this.makeOrderVos = makeOrderVos;
	}

	public String makeOrder() {
		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_REQUEST, 1);
		
		Map<String, String> map = new HashMap<String, String>();//商品编码：订单编码
		try {
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.MAKEORDER_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}

			// String callbackUrl = basePath + "/api/pay/callback.xhtml";
			// String clientTenPayUrl = "pay/retback.xhtml";
			MakeOrderPo req = MakeOrderPo.createInstance4Cmdy(getWid(), getSk(), orderStrList, adid, getWid() + "from:"
					+ sellerUin,getSourceMk());
			req.setVer(ver);
			Log.run.info("orderStrList.startsWith"+orderStrList.startsWith("5"));
			//如果是京东下单
			if(orderStrList.startsWith("5")){
				//从spring 容器中获取recvAddrBiz对象
				RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
				// 查询某QQ号收货地址列表（idl接口）
				RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
				//如果查出收货地址列表为异常则返回错误
				if (addrList == null || addrList.getAddressList() == null
						|| addrList.getAddressList().size() <= 0) {
					setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
					return RST_FAILURE;				
				}
				//Log.run.info("defaultAddrdefaultAddrdefaultAddr++"+addrList.getAddressList().size()+addrList.getAddressList().get(0).getAddress());
				//从列表中取出要选择的（adid）收货地址
				ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
						addrList.getAddressList(), adid);
				//如果异常则返回错误码
				if (defaultAddr == null) {
					setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
					return RST_FAILURE;					
				}
				makeOrderVos = cmdyBiz.makeOrderByJd(req,defaultAddr);
			}else {
				makeOrderVos = cmdyBiz.makeOrder(req);
			}
			// add by wendyhu 2013.03.19
			// for check NULL and add deal List output.
			List<DealInfo> dealInfos = new ArrayList<DealInfo>();
			boolean isNeedAddpre = false;
			
			// dealCode~sellerUin|dealCode~sellerUin
			Log.run.info("makeOrderVossize"+makeOrderVos.size());
			for (int i = 0; i < makeOrderVos.size(); i++) {

				MakeOrderVo makeOrderVo = null;
				if (makeOrderVos != null && (makeOrderVo = makeOrderVos.get(i)) != null) {
					totalFee += makeOrderVo.getTotalFee();
					DealInfo item = new DealInfo();
					item.setDealCode(makeOrderVo.getDealCode());
					item.setSellerUin("" + makeOrderVo.getSellerUin());
					dealInfos.add(item);
					if (!StringUtil.isBlank(makeOrderVo.getDealCode())) {
						if (isNeedAddpre) {
							dealsStr += "|" + makeOrderVo.getDealCode() + "~" + makeOrderVo.getSellerUin();
						} else {
							dealsStr += makeOrderVo.getDealCode() + "~" + makeOrderVo.getSellerUin();
							isNeedAddpre = true;
						}
					}
					Log.run.info("dealsStr"+dealsStr);
					//add by wanghao start 购物车下单上报 
					try {
						//根据订单号获取订单详情
						DealDetailInfo ret = queryDealBiz.dealDetail(getWid(), makeOrderVo.getDealCode(), false, 1, getMk());
						List<DealCmdyInfo> childrenDealList = ret.getItemList();
						for(DealCmdyInfo dealInfo : childrenDealList){
							String itemCode = dealInfo.getItemCode();
							ItemPo_v2 itemPo = ItemClient.fetchItemInfo(itemCode).getItemPo();
							long lCatId = itemPo.getOItemBase().getLeafClassId();
							long sellerUin = itemPo.getOItemBase().getQqUin();
							OrderMsg msg = OrderMsg.build(makeOrderVo.getDealCode(), dealInfo.getDealSubCode(),//
									getWid(), sellerUin,itemCode, lCatId, dealInfo.getItemDealPrice(),//
									dealInfo.getItemDealCount(),new ChannelParam(getChannel()).getPprdP(),//
									getHttpHeadIp(), ServletActionContext.getRequest().getRemotePort(),//
									OrderMsg.下单入口_购物车下单, payType);
							biBiz.uploadMsg(msg);
							map.put(itemCode,makeOrderVo.getDealCode());
							Log.run.info("cart deal upload data : dealCode = " + makeOrderVo.getDealCode()//
									+ "-- SUbDealCode = " + dealInfo.getDealSubCode() + "-- leafCatId = " + lCatId );
						}
					} catch (Exception e) {
						Log.run.error("BI upload error:" + e.getMessage(), e);
					}
					//add by wanghao end
				}
			}

			
			try {
				//记录订单的日志
				String[] goods = orderStrList.split(",");
				
				String channel = "";
				try {
					Log.run.info("channel:" + getChannel());
					channel = new JSONObject().fromObject(URLDecoder.decode(getChannel(),"utf-8")).get("market").toString();
				} catch (Exception e1) {
					Log.run.error(e1.getMessage(), e1);
				}
				
				for (String good : goods) {
					
					String[] split = good.split("~");
					String[] split2 = null;
					if(ver >=4 ){
						split2 = split[6].split("\\$");
					}else{
						split2 = split[5].split("\\$");//商品id$库存属性$商品数量$价格类型$红包id$internalSource$externalSource
					}
					StringBuffer buffer = new StringBuffer();
					buffer
						.append(getMachineKey()).append("\t")//"mk", 
						.append(getMt()).append("\t")//"mt", 
						.append(getOsVersion()).append("\t")//osversion
						.append(getWid()).append("\t")//wid
						.append(getPinid()).append("\t")//pinid
						.append(getVersionCode()).append("\t")//app_version
						.append(getHttpHeadIp()).append("\t")//ip
						.append(map.get(split2[0])).append("\t")//order_id
						.append(split2[0]).append("\t")//sku_id
						.append(split2[2]).append("\t")//sku_num
						.append(split2[3]).append("\t")//price
						.append(channel)//channel
					;
					if(split2.length > 5){
						buffer.append("\t").append(null == split2[5] ? "" : URLDecoder.decode(split2[5], "utf-8")).append("\t");//InternalSource
						if(split2.length > 6){
							buffer.append(null == split2[6] ? "" : URLDecoder.decode(split2[6], "utf-8"));//ExternalSource
						}
					}
					Log.makeorder.info(buffer.toString());
				}
			} catch (Exception e) {
				Log.run.error("makeorder error---" + e.getMessage(),e);
			}
			
			
			
			// add for dealList output
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "size", "empty" };
			jsoncfg.setExcludes(excludes);
			dealList = JSONSerializer.toJSON(dealInfos, jsoncfg).toString();
			Log.run.info("-----------------dealList------------"+dealList);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#makeOrder", e);
			if(e.getErrCode()==BusinessErrConstants.ERR_APP_BID_RESULT_TIMEOUT){
				result.setErrInfo(e.getErrCode(), "此商品已下架，您不能购买");
			}
			setErrCodeAndMsg4BExp(e);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_BUSINESS_FAILED, 1);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiCartAction#makeOrder", e);
			LogManager.getLogger(ApiCartAction.class).info("ApiCartAction#makeOrder#error", e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_FAILED, 1);
			return RST_FAILURE;
		}  
	}
	
	
	/**
	 * 购物车多店铺下单
	 * @return
	 */
	public String multiMakeOrder() {
		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_REQUEST, 1);
		Vector<OrderView> multiMakeOrder = null;
		Map<String, String> map = new HashMap<String, String>();//商品编码：订单编码
		
		try {
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if ((!validateInput(BizConstant.MAKEORDER_TYPE)) || (!validateUserInfo())) {
				return RST_FAILURE;
			}

			// String callbackUrl = basePath + "/api/pay/callback.xhtml";
			// String clientTenPayUrl = "pay/retback.xhtml";
			MakeOrderPo req = MakeOrderPo.createInstance4Cmdy(getWid(), getSk(), orderStrList, adid, getWid() + "from:"
					+ sellerUin,getSourceMk());
			req.setVer(ver);
			Log.run.info("orderStrList.startsWith"+orderStrList.startsWith("5"));
			//如果是京东下单
			if(orderStrList.startsWith("5")){
				//从spring 容器中获取recvAddrBiz对象
				RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
				// 查询某QQ号收货地址列表（idl接口）
				RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
				//如果查出收货地址列表为异常则返回错误
				if (addrList == null || addrList.getAddressList() == null
						|| addrList.getAddressList().size() <= 0) {
					setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
					return RST_FAILURE;				
				}
				//从列表中取出要选择的（adid）收货地址
				ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
						addrList.getAddressList(), adid);
				//如果异常则返回错误码
				if (defaultAddr == null) {
					setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
					return RST_FAILURE;					
				}
				multiMakeOrder = cmdyBiz.multiMakeOrderByJd(req,defaultAddr);
			}else {
				multiMakeOrder = cmdyBiz.multiMakeOrder(req);
			}
			// add by wendyhu 2013.03.19
			// for check NULL and add deal List output.
//			List<DealInfo> dealInfos = new ArrayList<DealInfo>();
//			boolean isNeedAddpre = false;
			
			// dealCode~sellerUin|dealCode~sellerUin
			Log.run.info("makeOrderVossize" + multiMakeOrder.size());
			for (int i = 0; i < multiMakeOrder.size(); i++) {

				OrderView orderView = null;
				if (multiMakeOrder != null && (orderView = multiMakeOrder.get(i)) != null) {
//					totalFee += orderView.getTotalFee();
//					DealInfo item = new DealInfo();
//					item.setDealCode(makeOrderVo.getDealCode());
//					item.setSellerUin("" + makeOrderVo.getSellerUin());
//					dealInfos.add(item);
//					if (!StringUtil.isBlank(orderView.getDealId())) {
//						if (isNeedAddpre) {
//							dealsStr += "|" + orderView.getDealId() + "~" + orderView.getShopViewInfo().getSellerUin();
//						} else {
//							dealsStr += orderView.getDealId() + "~" + orderView.getShopViewInfo().getSellerUin();
//							isNeedAddpre = true;
//						}
//					}
//					Log.run.info("dealsStr"+dealsStr);
					//add by wanghao start 购物车下单上报 
					try {
						//根据订单号获取订单详情
						DealDetailInfo ret = queryDealBiz.dealDetail(getWid(), orderView.getDealId(), false, 1, getMk());
						List<DealCmdyInfo> childrenDealList = ret.getItemList();
						for(DealCmdyInfo dealInfo : childrenDealList){
							String itemCode = dealInfo.getItemCode();
							ItemPo_v2 itemPo = ItemClient.fetchItemInfo(itemCode).getItemPo();
							long lCatId = itemPo.getOItemBase().getLeafClassId();
							long sellerUin = itemPo.getOItemBase().getQqUin();
							OrderMsg msg = OrderMsg.build(orderView.getDealId(), dealInfo.getDealSubCode(),//
									getWid(), sellerUin,itemCode, lCatId, dealInfo.getItemDealPrice(),//
									dealInfo.getItemDealCount(),new ChannelParam(getChannel()).getPprdP(),//
									getHttpHeadIp(), ServletActionContext.getRequest().getRemotePort(),//
									OrderMsg.下单入口_购物车下单, payType);
							biBiz.uploadMsg(msg);
							map.put(itemCode, orderView.getDealId());
							Log.run.info("cart deal upload data : dealCode = " + orderView.getDealId()//
									+ "-- SUbDealCode = " + dealInfo.getDealSubCode() + "-- leafCatId = " + lCatId );
						}
					} catch (Exception e) {
						Log.run.error("BI upload error:" + e.getMessage(), e);
					}
					//add by wanghao end
				}
			}

			
			try {
				//记录订单的日志
				String[] goods = orderStrList.split(",");
				
				String channel = "";
				try {
					Log.run.info("channel:" + getChannel());
					channel = new JSONObject().fromObject(URLDecoder.decode(getChannel(),"utf-8")).get("market").toString();
				} catch (Exception e1) {
					Log.run.error(e1.getMessage(), e1);
				}
				
				for (String good : goods) {
					
					String[] split = good.split("~");
					String[] split2 = null;
					if(ver >=4 ){
						split2 = split[6].split("\\$");
					}else{
						split2 = split[5].split("\\$");//商品id$库存属性$商品数量$价格类型$红包id$internalSource$externalSource
					}
					StringBuffer buffer = new StringBuffer();
					buffer
						.append(getMachineKey()).append("\t")//"mk", 
						.append(getMt()).append("\t")//"mt", 
						.append(getOsVersion()).append("\t")//osversion
						.append(getWid()).append("\t")//wid
						.append(getPinid()).append("\t")//pinid
						.append(getVersionCode()).append("\t")//app_version
						.append(getHttpHeadIp()).append("\t")//ip
						.append(map.get(split2[0])).append("\t")//order_id
						.append(split2[0]).append("\t")//sku_id
						.append(split2[2]).append("\t")//sku_num
						.append(split2[3]).append("\t")//price
						.append(channel)//channel
					;
					if(split2.length > 5){
						buffer.append("\t").append(null == split2[5] ? "" : URLDecoder.decode(split2[5], "utf-8")).append("\t");//InternalSource
						if(split2.length > 6){
							buffer.append(null == split2[6] ? "" : URLDecoder.decode(split2[6], "utf-8"));//ExternalSource
						}
					}
					Log.makeorder.info(buffer.toString());
				}
			} catch (Exception e) {
				Log.run.error("makeorder error---" + e.getMessage(),e);
			}
			
			/*
			 * 将移动端的参数支付方式返回，用于支付路由
			 * 由于购物车中的所有商品肯定是同一种支付方式，所以直接取orderStrList的第一个数字，即支付方式
			 */
			for (OrderView orderView : multiMakeOrder) {
				orderView.setPayType(Integer.parseInt(orderStrList.substring(0, 1)));
			}
			
			
			// add for dealList output
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "size", "empty" };
			jsoncfg.setExcludes(excludes);
			dealList = JSONSerializer.toJSON(multiMakeOrder, jsoncfg).toString();
			Log.run.info("-----------------dealList------------"+dealList);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#makeOrder", e);
			if(e.getErrCode()==BusinessErrConstants.ERR_APP_BID_RESULT_TIMEOUT){
				result.setErrInfo(e.getErrCode(), "此商品已下架，您不能购买");
			}
			setErrCodeAndMsg4BExp(e);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_BUSINESS_FAILED, 1);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiCartAction#makeOrder", e);
			LogManager.getLogger(ApiCartAction.class).info("ApiCartAction#makeOrder#error", e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_MAKEORDER_FAILED, 1);
			return RST_FAILURE;
		}  
	}
	
	
	
	/**
	 * 参数检查方法
	 * 
	 * @return
	 */
	private boolean validateInput(int type) {
		if (type == BizConstant.ADD_TYPE) {
			if (StringUtil.isEmpty(itemCode)) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "商品信息错误");
				return false;
			}
			if (buyNum <= 0) {
				return false;
			}
			return true;
		}
		if (type == BizConstant.REMOVE_TYPE || type == BizConstant.MODIFY_TYPE) {
			if (StringUtil.isEmpty(itemList))
				return false;
		}

		if (type == BizConstant.CONFIRM_TYPE) {
			if (StringUtil.isEmpty(itemList))
				return false;
			if (ver > 2) {
				return false;
			}
		}
		if (type == BizConstant.MAKEORDER_TYPE) {
			if (StringUtil.isEmpty(orderStrList))
				return false;
		}
		return true;
	}

	public int getAdid() {
		return adid;
	}

	public void setAdid(int adid) {
		this.adid = adid;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemAttr() {
		return itemAttr;
	}

	public void setItemAttr(String itemAttr) {
		this.itemAttr = itemAttr;
	}

	public int getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(int buyNum) {
		this.buyNum = buyNum;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public ConfirmOrderVo getConfirmVo() {
		return confirmVo;
	}

	public void setConfirmVo(ConfirmOrderVo confirmVo) {
		this.confirmVo = confirmVo;
	}

	public IPayBiz getPayBiz() {
		return payBiz;
	}

	public void setPayBiz(IPayBiz payBiz) {
		this.payBiz = payBiz;
	}

	public String getOrderStrList() {
		return orderStrList;
	}

	public void setOrderStrList(String orderStrList) {
		this.orderStrList = orderStrList;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}


	public PayTokenInfo getPayTokenInfo() {
		return payTokenInfo;
	}

	public void setPayTokenInfo(PayTokenInfo payTokenInfo) {
		this.payTokenInfo = payTokenInfo;
	}

	public String getDealsStr() {
		return dealsStr;
	}

	public void setDealsStr(String dealsStr) {
		this.dealsStr = dealsStr;
	}

	public long getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(long totalFee) {
		this.totalFee = totalFee;
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public QueryDealBiz getQueryDealBiz() {
		return queryDealBiz;
	}

	public void setQueryDealBiz(QueryDealBiz queryDealBiz) {
		this.queryDealBiz = queryDealBiz;
	}

	public int getScene() {
		return scene;
	}

	public void setScene(int scene) {
		this.scene = scene;
	}

	public String getDealStrs() {
		return dealStrs;
	}

	public void setDealStrs(String dealStrs) {
		this.dealStrs = dealStrs;
	}

	public int getUseRedPackage() {
		return useRedPackage;
	}

	public void setUseRedPackage(int useRedPackage) {
		this.useRedPackage = useRedPackage;
	}


	public String getOldItem() {
		return oldItem;
	}

	public void setOldItem(String oldItem) {
		this.oldItem = oldItem;
	}

	public String getNewItem() {
		return newItem;
	}

	public void setNewItem(String newItem) {
		this.newItem = newItem;
	}

	public long getSecenId() {
		return secenId;
	}

	public void setSecenId(long secenId) {
		this.secenId = secenId;
	}

}
