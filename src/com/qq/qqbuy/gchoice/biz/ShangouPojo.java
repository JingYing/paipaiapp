package com.qq.qqbuy.gchoice.biz;
public class ShangouPojo{
	
		private String id;
		private String qq;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getQq() {
			return qq;
		}
		public void setQq(String qq) {
			this.qq = qq;
		}
		public ShangouPojo() {
			super();
			// TODO Auto-generated constructor stub
		}
		public ShangouPojo(String id, String qq) {
			super();
			this.id = id;
			this.qq = qq;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((qq == null) ? 0 : qq.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ShangouPojo other = (ShangouPojo) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (qq == null) {
				if (other.qq != null)
					return false;
			} else if (!qq.equals(other.qq))
				return false;
			return true;
		}
	}