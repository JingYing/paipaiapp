 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.lang.int64;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *resp
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  UpdateTuanStatisticResp implements IServiceObject
{
	public long result;
	/**
	 * 返回码, 0表示正常, 非0表示错误
	 *
	 * 版本 >= 0
	 */
	 private int64 retcode = new int64();

	/**
	 * 错误提示信息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(retcode);
		bs.pushString(errmsg);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retcode = (com.paipai.lang.int64)bs.popObject(com.paipai.lang.int64.class);
		errmsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72078807L;
	}


	/**
	 * 获取返回码, 0表示正常, 非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @return retcode value 类型为:int64
	 * 
	 */
	public int64 getRetcode()
	{
		return retcode;
	}


	/**
	 * 设置返回码, 0表示正常, 非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int64
	 * 
	 */
	public void setRetcode(int64 value)
	{
		this.retcode = value;
	}


	/**
	 * 获取错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UpdateTuanStatisticResp)
				length += ByteStream.getObjectSize(retcode, null);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, null);  //计算字段errmsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateTuanStatisticResp)
				length += ByteStream.getObjectSize(retcode, encoding);  //计算字段retcode的长度 size_of(int64)
				length += ByteStream.getObjectSize(errmsg, encoding);  //计算字段errmsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
