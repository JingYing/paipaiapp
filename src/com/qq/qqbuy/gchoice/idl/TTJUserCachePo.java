//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.AddUserCacheReq.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *用户访问缓存
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class TTJUserCachePo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20141215;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickname = new String();

	/**
	 * 版本 >= 0
	 */
	 private short nickname_u;

	/**
	 * 头像url
	 *
	 * 版本 >= 0
	 */
	 private String avatar = new String();

	/**
	 * 版本 >= 0
	 */
	 private short avatar_u;

	/**
	 * 最近访问时间
	 *
	 * 版本 >= 0
	 */
	 private long lastAccessTime;

	/**
	 * 版本 >= 0
	 */
	 private short lastAccessTime_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushString(nickname);
		bs.pushUByte(nickname_u);
		bs.pushString(avatar);
		bs.pushUByte(avatar_u);
		bs.pushUInt(lastAccessTime);
		bs.pushUByte(lastAccessTime_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		nickname = bs.popString();
		nickname_u = bs.popUByte();
		avatar = bs.popString();
		avatar_u = bs.popUByte();
		lastAccessTime = bs.popUInt();
		lastAccessTime_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickname value 类型为:String
	 * 
	 */
	public String getNickname()
	{
		return nickname;
	}


	/**
	 * 设置用户昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickname(String value)
	{
		this.nickname = value;
		this.nickname_u = 1;
	}

	public boolean issetNickname()
	{
		return this.nickname_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return nickname_u value 类型为:short
	 * 
	 */
	public short getNickname_u()
	{
		return nickname_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNickname_u(short value)
	{
		this.nickname_u = value;
	}


	/**
	 * 获取头像url
	 * 
	 * 此字段的版本 >= 0
	 * @return avatar value 类型为:String
	 * 
	 */
	public String getAvatar()
	{
		return avatar;
	}


	/**
	 * 设置头像url
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAvatar(String value)
	{
		this.avatar = value;
		this.avatar_u = 1;
	}

	public boolean issetAvatar()
	{
		return this.avatar_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return avatar_u value 类型为:short
	 * 
	 */
	public short getAvatar_u()
	{
		return avatar_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAvatar_u(short value)
	{
		this.avatar_u = value;
	}


	/**
	 * 获取最近访问时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastAccessTime value 类型为:long
	 * 
	 */
	public long getLastAccessTime()
	{
		return lastAccessTime;
	}


	/**
	 * 设置最近访问时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastAccessTime(long value)
	{
		this.lastAccessTime = value;
		this.lastAccessTime_u = 1;
	}

	public boolean issetLastAccessTime()
	{
		return this.lastAccessTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastAccessTime_u value 类型为:short
	 * 
	 */
	public short getLastAccessTime_u()
	{
		return lastAccessTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastAccessTime_u(short value)
	{
		this.lastAccessTime_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJUserCachePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(nickname, null);  //计算字段nickname的长度 size_of(String)
				length += 1;  //计算字段nickname_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(avatar, null);  //计算字段avatar的长度 size_of(String)
				length += 1;  //计算字段avatar_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lastAccessTime的长度 size_of(uint32_t)
				length += 1;  //计算字段lastAccessTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJUserCachePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(nickname, encoding);  //计算字段nickname的长度 size_of(String)
				length += 1;  //计算字段nickname_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(avatar, encoding);  //计算字段avatar的长度 size_of(String)
				length += 1;  //计算字段avatar_u的长度 size_of(uint8_t)
				length += 4;  //计算字段lastAccessTime的长度 size_of(uint32_t)
				length += 1;  //计算字段lastAccessTime_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
