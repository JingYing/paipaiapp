 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  AddUserGroupCacheReq implements IServiceObject
{
	/**
	 * 用户群信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<TTJUserIdentifyPo> inUserGroupIdentify = new Vector<TTJUserIdentifyPo>();

	/**
	 * 用户群头像、昵称缓存
	 *
	 * 版本 >= 0
	 */
	 private Vector<TTJUserCachePo> inUserGroupCache = new Vector<TTJUserCachePo>();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(inUserGroupIdentify);
		bs.pushObject(inUserGroupCache);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		inUserGroupIdentify = (Vector<TTJUserIdentifyPo>)bs.popVector(TTJUserIdentifyPo.class);
		inUserGroupCache = (Vector<TTJUserCachePo>)bs.popVector(TTJUserCachePo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x7207180FL;
	}


	/**
	 * 获取用户群信息
	 * 
	 * 此字段的版本 >= 0
	 * @return inUserGroupIdentify value 类型为:Vector<TTJUserIdentifyPo>
	 * 
	 */
	public Vector<TTJUserIdentifyPo> getInUserGroupIdentify()
	{
		return inUserGroupIdentify;
	}


	/**
	 * 设置用户群信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TTJUserIdentifyPo>
	 * 
	 */
	public void setInUserGroupIdentify(Vector<TTJUserIdentifyPo> value)
	{
		if (value != null) {
				this.inUserGroupIdentify = value;
		}else{
				this.inUserGroupIdentify = new Vector<TTJUserIdentifyPo>();
		}
	}


	/**
	 * 获取用户群头像、昵称缓存
	 * 
	 * 此字段的版本 >= 0
	 * @return inUserGroupCache value 类型为:Vector<TTJUserCachePo>
	 * 
	 */
	public Vector<TTJUserCachePo> getInUserGroupCache()
	{
		return inUserGroupCache;
	}


	/**
	 * 设置用户群头像、昵称缓存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TTJUserCachePo>
	 * 
	 */
	public void setInUserGroupCache(Vector<TTJUserCachePo> value)
	{
		if (value != null) {
				this.inUserGroupCache = value;
		}else{
				this.inUserGroupCache = new Vector<TTJUserCachePo>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddUserGroupCacheReq)
				length += ByteStream.getObjectSize(inUserGroupIdentify, null);  //计算字段inUserGroupIdentify的长度 size_of(Vector)
				length += ByteStream.getObjectSize(inUserGroupCache, null);  //计算字段inUserGroupCache的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddUserGroupCacheReq)
				length += ByteStream.getObjectSize(inUserGroupIdentify, encoding);  //计算字段inUserGroupIdentify的长度 size_of(Vector)
				length += ByteStream.getObjectSize(inUserGroupCache, encoding);  //计算字段inUserGroupCache的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
