 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  DelUserCacheReq implements IServiceObject
{
	/**
	 * 用户id的类型
	 *
	 * 版本 >= 0
	 */
	 private short IdType;

	/**
	 * 用户id
	 *
	 * 版本 >= 0
	 */
	 private String id = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUByte(IdType);
		bs.pushString(id);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		IdType = bs.popUByte();
		id = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x7207180CL;
	}


	/**
	 * 获取用户id的类型
	 * 
	 * 此字段的版本 >= 0
	 * @return IdType value 类型为:short
	 * 
	 */
	public short getIdType()
	{
		return IdType;
	}


	/**
	 * 设置用户id的类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIdType(short value)
	{
		this.IdType = value;
	}


	/**
	 * 获取用户id
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:String
	 * 
	 */
	public String getId()
	{
		return id;
	}


	/**
	 * 设置用户id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setId(String value)
	{
		this.id = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DelUserCacheReq)
				length += 1;  //计算字段IdType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(id, null);  //计算字段id的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(DelUserCacheReq)
				length += 1;  //计算字段IdType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(id, encoding);  //计算字段id的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
