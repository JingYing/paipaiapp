//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.AddUserGroupCacheReq.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *用户标识信息
 *
 *@date 2015-03-04 11:43:57
 *
 *@since version:0
*/
public class TTJUserIdentifyPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20150105;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 用户id
	 *
	 * 版本 >= 0
	 */
	 private String userId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short userId_u;

	/**
	 * 用户类型
	 *
	 * 版本 >= 0
	 */
	 private long userType;

	/**
	 * 版本 >= 0
	 */
	 private short userType_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushString(userId);
		bs.pushUByte(userId_u);
		bs.pushUInt(userType);
		bs.pushUByte(userType_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		userId = bs.popString();
		userId_u = bs.popUByte();
		userType = bs.popUInt();
		userType_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取用户id
	 * 
	 * 此字段的版本 >= 0
	 * @return userId value 类型为:String
	 * 
	 */
	public String getUserId()
	{
		return userId;
	}


	/**
	 * 设置用户id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUserId(String value)
	{
		this.userId = value;
		this.userId_u = 1;
	}

	public boolean issetUserId()
	{
		return this.userId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userId_u value 类型为:short
	 * 
	 */
	public short getUserId_u()
	{
		return userId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserId_u(short value)
	{
		this.userId_u = value;
	}


	/**
	 * 获取用户类型
	 * 
	 * 此字段的版本 >= 0
	 * @return userType value 类型为:long
	 * 
	 */
	public long getUserType()
	{
		return userType;
	}


	/**
	 * 设置用户类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserType(long value)
	{
		this.userType = value;
		this.userType_u = 1;
	}

	public boolean issetUserType()
	{
		return this.userType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return userType_u value 类型为:short
	 * 
	 */
	public short getUserType_u()
	{
		return userType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserType_u(short value)
	{
		this.userType_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJUserIdentifyPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(userId, null);  //计算字段userId的长度 size_of(String)
				length += 1;  //计算字段userId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段userType的长度 size_of(uint32_t)
				length += 1;  //计算字段userType_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TTJUserIdentifyPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(userId, encoding);  //计算字段userId的长度 size_of(String)
				length += 1;  //计算字段userId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段userType的长度 size_of(uint32_t)
				length += 1;  //计算字段userType_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
