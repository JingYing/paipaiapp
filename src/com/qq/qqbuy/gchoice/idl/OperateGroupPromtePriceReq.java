 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  OperateGroupPromtePriceReq implements IServiceObject
{
	/**
	 * 团ID
	 *
	 * 版本 >= 0
	 */
	 private String groupID = new String();

	/**
	 * uin
	 *
	 * 版本 >= 0
	 */
	 private String uin = new String();

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 场景ID,备用
	 *
	 * 版本 >= 0
	 */
	 private int sceneID;

	/**
	 * 操作类型 读填0，插入填1，重置填2
	 *
	 * 版本 >= 0
	 */
	 private short operateCmd;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(groupID);
		bs.pushString(uin);
		bs.pushString(itemId);
		bs.pushInt(sceneID);
		bs.pushUByte(operateCmd);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		groupID = bs.popString();
		uin = bs.popString();
		itemId = bs.popString();
		sceneID = bs.popInt();
		operateCmd = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x7207180AL;
	}


	/**
	 * 获取团ID
	 * 
	 * 此字段的版本 >= 0
	 * @return groupID value 类型为:String
	 * 
	 */
	public String getGroupID()
	{
		return groupID;
	}


	/**
	 * 设置团ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setGroupID(String value)
	{
		this.groupID = value;
	}


	/**
	 * 获取uin
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:String
	 * 
	 */
	public String getUin()
	{
		return uin;
	}


	/**
	 * 设置uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setUin(String value)
	{
		this.uin = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
	}


	/**
	 * 获取场景ID,备用
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneID value 类型为:int
	 * 
	 */
	public int getSceneID()
	{
		return sceneID;
	}


	/**
	 * 设置场景ID,备用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSceneID(int value)
	{
		this.sceneID = value;
	}


	/**
	 * 获取操作类型 读填0，插入填1，重置填2
	 * 
	 * 此字段的版本 >= 0
	 * @return operateCmd value 类型为:short
	 * 
	 */
	public short getOperateCmd()
	{
		return operateCmd;
	}


	/**
	 * 设置操作类型 读填0，插入填1，重置填2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOperateCmd(short value)
	{
		this.operateCmd = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateGroupPromtePriceReq)
				length += ByteStream.getObjectSize(groupID, null);  //计算字段groupID的长度 size_of(String)
				length += ByteStream.getObjectSize(uin, null);  //计算字段uin的长度 size_of(String)
				length += ByteStream.getObjectSize(itemId, null);  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段sceneID的长度 size_of(int)
				length += 1;  //计算字段operateCmd的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateGroupPromtePriceReq)
				length += ByteStream.getObjectSize(groupID, encoding);  //计算字段groupID的长度 size_of(String)
				length += ByteStream.getObjectSize(uin, encoding);  //计算字段uin的长度 size_of(String)
				length += ByteStream.getObjectSize(itemId, encoding);  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段sceneID的长度 size_of(int)
				length += 1;  //计算字段operateCmd的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
