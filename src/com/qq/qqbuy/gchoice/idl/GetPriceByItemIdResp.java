 package com.qq.qqbuy.gchoice.idl;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *返回信息
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class  GetPriceByItemIdResp implements IServiceObject
{
	public long result;
	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 商品价格类型
	 *
	 * 版本 >= 0
	 */
	 private long priceType;

	/**
	 * 返回保留字，拓展用
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> ReserveOut = new HashMap<String,String>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(ErrMsg);
		bs.pushUInt(price);
		bs.pushUInt(priceType);
		bs.pushObject(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		ErrMsg = bs.popString();
		price = bs.popUInt();
		priceType = bs.popUInt();
		ReserveOut = (Map<String,String>)bs.popMap(String.class,String.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x875b8805L;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
	}


	/**
	 * 获取商品价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @return priceType value 类型为:long
	 * 
	 */
	public long getPriceType()
	{
		return priceType;
	}


	/**
	 * 设置商品价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPriceType(long value)
	{
		this.priceType = value;
	}


	/**
	 * 获取返回保留字，拓展用
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置返回保留字，拓展用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveOut(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveOut = value;
		}else{
				this.ReserveOut = new HashMap<String,String>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPriceByItemIdResp)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += 4;  //计算字段priceType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveOut, null);  //计算字段ReserveOut的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPriceByItemIdResp)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += 4;  //计算字段priceType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ReserveOut, encoding);  //计算字段ReserveOut的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
