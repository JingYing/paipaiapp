//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: tuantuanjiang.idl.QueryTuanReq.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *团信息
 *
 *@date 2015-03-04 10:57:53
 *
 *@since version:0
*/
public class TuanQueryCondBo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version = 0;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 团id
	 *
	 * 版本 >= 0
	 */
	 private long TuanId;

	/**
	 * 版本 >= 0
	 */
	 private short TuanId_u;

	/**
	 * uid
	 *
	 * 版本 >= 0
	 */
	 private long Uid;

	/**
	 * 版本 >= 0
	 */
	 private short Uid_u;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealId_u;

	/**
	 * 团起始序号，默认为0
	 *
	 * 版本 >= 0
	 */
	 private long Start;

	/**
	 * 版本 >= 0
	 */
	 private short Start_u;

	/**
	 * 团数目，建议一次取20
	 *
	 * 版本 >= 0
	 */
	 private long Len;

	/**
	 * 版本 >= 0
	 */
	 private short Len_u;

	/**
	 * 获取成员数目，小于0为拉取所有用户；0为不拉取用户信息；其余为尝试拉取用户数
	 *
	 * 版本 >= 0
	 */
	 private int TryGetMemberCount;

	/**
	 * 版本 >= 0
	 */
	 private short TryGetMemberCount_u;

	/**
	 * 排序字段 PayTime:支付时间 CreateTime:创建时间
	 *
	 * 版本 >= 0
	 */
	 private String MemberOrderField = new String();

	/**
	 * 版本 >= 0
	 */
	 private short MemberOrderField_u;

	/**
	 * 排序类型 0:desc(降序) 1:asc(升序)
	 *
	 * 版本 >= 0
	 */
	 private short MemberOrderType;

	/**
	 * 版本 >= 0
	 */
	 private short MemberOrderType_u;

	/**
	 * 保留输入
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> ReserveIn = new HashMap<String,String>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(TuanId);
		bs.pushUByte(TuanId_u);
		bs.pushLong(Uid);
		bs.pushUByte(Uid_u);
		bs.pushString(DealId);
		bs.pushUByte(DealId_u);
		bs.pushUInt(Start);
		bs.pushUByte(Start_u);
		bs.pushUInt(Len);
		bs.pushUByte(Len_u);
		bs.pushInt(TryGetMemberCount);
		bs.pushUByte(TryGetMemberCount_u);
		bs.pushString(MemberOrderField);
		bs.pushUByte(MemberOrderField_u);
		bs.pushUByte(MemberOrderType);
		bs.pushUByte(MemberOrderType_u);
		bs.pushObject(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		TuanId = bs.popLong();
		TuanId_u = bs.popUByte();
		Uid = bs.popLong();
		Uid_u = bs.popUByte();
		DealId = bs.popString();
		DealId_u = bs.popUByte();
		Start = bs.popUInt();
		Start_u = bs.popUByte();
		Len = bs.popUInt();
		Len_u = bs.popUByte();
		TryGetMemberCount = bs.popInt();
		TryGetMemberCount_u = bs.popUByte();
		MemberOrderField = bs.popString();
		MemberOrderField_u = bs.popUByte();
		MemberOrderType = bs.popUByte();
		MemberOrderType_u = bs.popUByte();
		ReserveIn = (Map<String,String>)bs.popMap(String.class,String.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取团id
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanId value 类型为:long
	 * 
	 */
	public long getTuanId()
	{
		return TuanId;
	}


	/**
	 * 设置团id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTuanId(long value)
	{
		this.TuanId = value;
		this.TuanId_u = 1;
	}

	public boolean issetTuanId()
	{
		return this.TuanId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TuanId_u value 类型为:short
	 * 
	 */
	public short getTuanId_u()
	{
		return TuanId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTuanId_u(short value)
	{
		this.TuanId_u = value;
	}


	/**
	 * 获取uid
	 * 
	 * 此字段的版本 >= 0
	 * @return Uid value 类型为:long
	 * 
	 */
	public long getUid()
	{
		return Uid;
	}


	/**
	 * 设置uid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUid(long value)
	{
		this.Uid = value;
		this.Uid_u = 1;
	}

	public boolean issetUid()
	{
		return this.Uid_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uid_u value 类型为:short
	 * 
	 */
	public short getUid_u()
	{
		return Uid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUid_u(short value)
	{
		this.Uid_u = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
		this.DealId_u = 1;
	}

	public boolean issetDealId()
	{
		return this.DealId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId_u value 类型为:short
	 * 
	 */
	public short getDealId_u()
	{
		return DealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealId_u(short value)
	{
		this.DealId_u = value;
	}


	/**
	 * 获取团起始序号，默认为0
	 * 
	 * 此字段的版本 >= 0
	 * @return Start value 类型为:long
	 * 
	 */
	public long getStart()
	{
		return Start;
	}


	/**
	 * 设置团起始序号，默认为0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStart(long value)
	{
		this.Start = value;
		this.Start_u = 1;
	}

	public boolean issetStart()
	{
		return this.Start_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Start_u value 类型为:short
	 * 
	 */
	public short getStart_u()
	{
		return Start_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStart_u(short value)
	{
		this.Start_u = value;
	}


	/**
	 * 获取团数目，建议一次取20
	 * 
	 * 此字段的版本 >= 0
	 * @return Len value 类型为:long
	 * 
	 */
	public long getLen()
	{
		return Len;
	}


	/**
	 * 设置团数目，建议一次取20
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLen(long value)
	{
		this.Len = value;
		this.Len_u = 1;
	}

	public boolean issetLen()
	{
		return this.Len_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Len_u value 类型为:short
	 * 
	 */
	public short getLen_u()
	{
		return Len_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLen_u(short value)
	{
		this.Len_u = value;
	}


	/**
	 * 获取获取成员数目，小于0为拉取所有用户；0为不拉取用户信息；其余为尝试拉取用户数
	 * 
	 * 此字段的版本 >= 0
	 * @return TryGetMemberCount value 类型为:int
	 * 
	 */
	public int getTryGetMemberCount()
	{
		return TryGetMemberCount;
	}


	/**
	 * 设置获取成员数目，小于0为拉取所有用户；0为不拉取用户信息；其余为尝试拉取用户数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setTryGetMemberCount(int value)
	{
		this.TryGetMemberCount = value;
		this.TryGetMemberCount_u = 1;
	}

	public boolean issetTryGetMemberCount()
	{
		return this.TryGetMemberCount_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TryGetMemberCount_u value 类型为:short
	 * 
	 */
	public short getTryGetMemberCount_u()
	{
		return TryGetMemberCount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTryGetMemberCount_u(short value)
	{
		this.TryGetMemberCount_u = value;
	}


	/**
	 * 获取排序字段 PayTime:支付时间 CreateTime:创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return MemberOrderField value 类型为:String
	 * 
	 */
	public String getMemberOrderField()
	{
		return MemberOrderField;
	}


	/**
	 * 设置排序字段 PayTime:支付时间 CreateTime:创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMemberOrderField(String value)
	{
		this.MemberOrderField = value;
		this.MemberOrderField_u = 1;
	}

	public boolean issetMemberOrderField()
	{
		return this.MemberOrderField_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MemberOrderField_u value 类型为:short
	 * 
	 */
	public short getMemberOrderField_u()
	{
		return MemberOrderField_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMemberOrderField_u(short value)
	{
		this.MemberOrderField_u = value;
	}


	/**
	 * 获取排序类型 0:desc(降序) 1:asc(升序)
	 * 
	 * 此字段的版本 >= 0
	 * @return MemberOrderType value 类型为:short
	 * 
	 */
	public short getMemberOrderType()
	{
		return MemberOrderType;
	}


	/**
	 * 设置排序类型 0:desc(降序) 1:asc(升序)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMemberOrderType(short value)
	{
		this.MemberOrderType = value;
		this.MemberOrderType_u = 1;
	}

	public boolean issetMemberOrderType()
	{
		return this.MemberOrderType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MemberOrderType_u value 类型为:short
	 * 
	 */
	public short getMemberOrderType_u()
	{
		return MemberOrderType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMemberOrderType_u(short value)
	{
		this.MemberOrderType_u = value;
	}


	/**
	 * 获取保留输入
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setReserveIn(Map<String,String> value)
	{
		if (value != null) {
				this.ReserveIn = value;
		}else{
				this.ReserveIn = new HashMap<String,String>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanQueryCondBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段TuanId的长度 size_of(uint64_t)
				length += 1;  //计算字段TuanId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uid的长度 size_of(uint64_t)
				length += 1;  //计算字段Uid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Start的长度 size_of(uint32_t)
				length += 1;  //计算字段Start_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Len的长度 size_of(uint32_t)
				length += 1;  //计算字段Len_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TryGetMemberCount的长度 size_of(int)
				length += 1;  //计算字段TryGetMemberCount_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(MemberOrderField, null);  //计算字段MemberOrderField的长度 size_of(String)
				length += 1;  //计算字段MemberOrderField_u的长度 size_of(uint8_t)
				length += 1;  //计算字段MemberOrderType的长度 size_of(uint8_t)
				length += 1;  //计算字段MemberOrderType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReserveIn, null);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(TuanQueryCondBo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段TuanId的长度 size_of(uint64_t)
				length += 1;  //计算字段TuanId_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uid的长度 size_of(uint64_t)
				length += 1;  //计算字段Uid_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Start的长度 size_of(uint32_t)
				length += 1;  //计算字段Start_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Len的长度 size_of(uint32_t)
				length += 1;  //计算字段Len_u的长度 size_of(uint8_t)
				length += 4;  //计算字段TryGetMemberCount的长度 size_of(int)
				length += 1;  //计算字段TryGetMemberCount_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(MemberOrderField, encoding);  //计算字段MemberOrderField的长度 size_of(String)
				length += 1;  //计算字段MemberOrderField_u的长度 size_of(uint8_t)
				length += 1;  //计算字段MemberOrderType的长度 size_of(uint8_t)
				length += 1;  //计算字段MemberOrderType_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReserveIn, encoding);  //计算字段ReserveIn的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
