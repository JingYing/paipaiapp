 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  UpdateTuanStatisticReq implements IServiceObject
{
	/**
	 * 活动期数
	 *
	 * 版本 >= 0
	 */
	 private String activeNum = new String();

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String comodityID = new String();

	/**
	 * 团值
	 *
	 * 版本 >= 0
	 */
	 private int value;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(activeNum);
		bs.pushString(comodityID);
		bs.pushInt(value);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		activeNum = bs.popString();
		comodityID = bs.popString();
		value = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72071807L;
	}


	/**
	 * 获取活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @return activeNum value 类型为:String
	 * 
	 */
	public String getActiveNum()
	{
		return activeNum;
	}


	/**
	 * 设置活动期数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActiveNum(String value)
	{
		this.activeNum = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return comodityID value 类型为:String
	 * 
	 */
	public String getComodityID()
	{
		return comodityID;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComodityID(String value)
	{
		this.comodityID = value;
	}


	/**
	 * 获取团值
	 * 
	 * 此字段的版本 >= 0
	 * @return value value 类型为:int
	 * 
	 */
	public int getValue()
	{
		return value;
	}


	/**
	 * 设置团值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setValue(int value)
	{
		this.value = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateTuanStatisticReq)
				length += ByteStream.getObjectSize(activeNum, null);  //计算字段activeNum的长度 size_of(String)
				length += ByteStream.getObjectSize(comodityID, null);  //计算字段comodityID的长度 size_of(String)
				length += 4;  //计算字段value的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateTuanStatisticReq)
				length += ByteStream.getObjectSize(activeNum, encoding);  //计算字段activeNum的长度 size_of(String)
				length += ByteStream.getObjectSize(comodityID, encoding);  //计算字段comodityID的长度 size_of(String)
				length += 4;  //计算字段value的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
