 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.TuanTJCossDao.java

package com.qq.qqbuy.gchoice.idl;

import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *req
 *
 *@date 2015-03-04 11:43:58
 *
 *@since version:0
*/
public class  OperateItemReq implements IServiceObject
{
	/**
	 * item
	 *
	 * 版本 >= 0
	 */
	 private TTJAuditItemPo inItem = new TTJAuditItemPo();

	/**
	 * 操作类型 读填0，增加填1，修改填2
	 *
	 * 版本 >= 0
	 */
	 private short operateId;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(inItem);
		bs.pushUByte(operateId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		inItem = (TTJAuditItemPo) bs.popObject(TTJAuditItemPo.class);
		operateId = bs.popUByte();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x72071809L;
	}


	/**
	 * 获取item
	 * 
	 * 此字段的版本 >= 0
	 * @return inItem value 类型为:TTJAuditItemPo
	 * 
	 */
	public TTJAuditItemPo getInItem()
	{
		return inItem;
	}


	/**
	 * 设置item
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TTJAuditItemPo
	 * 
	 */
	public void setInItem(TTJAuditItemPo value)
	{
		if (value != null) {
				this.inItem = value;
		}else{
				this.inItem = new TTJAuditItemPo();
		}
	}


	/**
	 * 获取操作类型 读填0，增加填1，修改填2
	 * 
	 * 此字段的版本 >= 0
	 * @return operateId value 类型为:short
	 * 
	 */
	public short getOperateId()
	{
		return operateId;
	}


	/**
	 * 设置操作类型 读填0，增加填1，修改填2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOperateId(short value)
	{
		this.operateId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateItemReq)
				length += ByteStream.getObjectSize(inItem, null);  //计算字段inItem的长度 size_of(TTJAuditItemPo)
				length += 1;  //计算字段operateId的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(OperateItemReq)
				length += ByteStream.getObjectSize(inItem, encoding);  //计算字段inItem的长度 size_of(TTJAuditItemPo)
				length += 1;  //计算字段operateId的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
