package com.qq.qqbuy.redpacket;

public enum RedPacketState {

    NOT_USED(100, "未使用"),
    FREEZING(101, "使用中"),
    USED(102, "已使用"),
    EXPIRED(2, "过期");
    
    private final int state;
    private final String stateInfo;
    
    private RedPacketState (int state, String stateInfo){
        this.state = state;
        this.stateInfo = stateInfo;
    }

    public int getState() {
        return state;
    }

    public String getStateInfo() {
        return stateInfo;
    }
    
    
}
