package com.qq.qqbuy.redpacket.util;

public class RedpacketConstants {

	/**
	 * 1：未发放  0：发放成功  -1：发放中  -2：发放失败，补单程序会进行补单操作 -3：当前无活动 
	 */
	public static int NO_SEND = 1 ;
	public static int SEND_SUCCESS = 0 ;
	public static int SEND_SENDING = -1 ;
	public static int SEND_ERROR = -2 ;
	public static int SEND_NO_ACTIVE = -3 ;
	
	public static int BUDAN_BEFORE_TIME = 600 ;//补10分钟之前的单（单位为秒）
	
	public static String KEY_FOR_BUDAN = "D54fdrte%^^FWsft456YTF^%fFYTf$%^$W%FGkEdgh" ;
	
	public static String KEY_FOR_SEND = "FAEU13586FW%^f$^FWFGQW5D%$fydsfuewf$d%6CASJSAHU" ;
	
}
