package com.qq.qqbuy.redpacket.dao;

import java.util.List;

import com.qq.qqbuy.redpacket.po.RedpacketSendPo;

public interface RedpacketSendDao {

	
	/**
	 * 查找发送对象
	 * @param wid
	 * @return
	 * @throws Exception
	 */
	public RedpacketSendPo findById(long wid) throws Exception ;
	/**
	 * 查找发送对象
	 * @param wid
	 * @param activeId
	 * @param redpacketId
	 * @return
	 * @throws Exception
	 */
	public RedpacketSendPo findById(long wid, String activeId, String redpacketId) throws Exception ;
	/**
	 * 根据活动ID和红包批次号查找( 0：数据入库，未调用发放代金券接口   1：发放成功  -1：发放失败)发送红包失败的对象
	 * @param activeId
	 * @param redpacketId
	 * @return
	 * @throws Exception
	 */
	public List<RedpacketSendPo> findListForBuDan(String activeId, String redpacketId,Long beforeTime) throws Exception ;
	
	/**
	 * 代金券入库
	 * @param redpacketPo
	 * @throws Exception
	 */
	public int insertRedpacket(RedpacketSendPo redpacketSendPo) throws Exception ;
	
	/**
	 * 更新代金券发放信息
	 * @param redpacketPo
	 * @throws Exception
	 */
	public int updateRedpacketStatus(RedpacketSendPo redpacketSendPo) throws Exception ;
	/**
	 * 根据活动ID和代金券批次号计算已发放总数
	 * @param activeId
	 * @param redpacketId
	 * @return
	 * @throws Exception
	 */
	public int countSendNum(String activeId, String redpacketId) throws Exception ;
	
	
}
