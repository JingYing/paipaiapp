package com.qq.qqbuy.virtualgoods.biz.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.MD5Util;
import com.qq.qqbuy.virtualgoods.VirtualClient;
import com.qq.qqbuy.virtualgoods.action.JsonUtils;
import com.qq.qqbuy.virtualgoods.biz.VirtualBiz;
import com.qq.qqbuy.virtualgoods.model.Order;
import com.qq.qqbuy.virtualgoods.model.OrderProduct;
import com.qq.qqbuy.virtualgoods.model.PagVritual;
import com.qq.qqbuy.virtualgoods.model.Pagination;
import com.qq.qqbuy.virtualgoods.model.VritualOrder;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderInfo;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderReq;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderResp;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualProductInfo;

/**
 * 
 * 
 * @ClassName: VirtualImpl
 * 
 * @Description: 实现类
 * 
 * @author lhn
 * 
 * @date 2014-12-18 下午3:20:09
 * 
 * 
 */
public class VirtualImpl implements VirtualBiz {
												
	private static final String CONSTANTS_KEY = "9f8f9e801455ac377ded";
	@Override
	public SubmitVirtualOrderInfo getVirtualOrder(long buyerUin, String cp,
			short dealType, short dealSource, short dealPayType,
			String rechargeAccount, String itemId, int isAutoSendItem,
			long itemOriginalPrice, long skuId, long dealItemCount) {
		SubmitVirtualOrderInfo virtualorder = new SubmitVirtualOrderInfo();
		// 设置单个设置信息
		SubmitVirtualProductInfo virtualProduct = new SubmitVirtualProductInfo();
		virtualProduct.setDealItemCount(dealItemCount);
		virtualProduct.setIsAutoSendItem(isAutoSendItem);
		virtualProduct.setItemId(itemId);
		virtualProduct.setItemOriginalPrice(itemOriginalPrice);
		virtualProduct.setSkuId(skuId);
		virtualorder.setBuyerUin(buyerUin);
		virtualorder.setIp(cp);
		virtualorder.setDealPayFeeTotal(dealItemCount * itemOriginalPrice);
		virtualorder.setDealPayType(dealPayType);
		virtualorder.setDealSource(dealSource);
		virtualorder.setDealType(dealType);
		virtualorder.setRechargeAccount(rechargeAccount);
		virtualorder.setSubmitVirtualProductInfo(virtualProduct);
		return virtualorder;
	}
	@Override
	public SubmitVirtualOrderResp submitVirtual(
			SubmitVirtualOrderInfo virtualorder) {
		SubmitVirtualOrderReq req = new SubmitVirtualOrderReq();
		req.setSubmitVirtualOrderInfo(virtualorder);
		SubmitVirtualOrderResp resp = new SubmitVirtualOrderResp();
		resp = VirtualClient.submitVir(req);
		return resp;
	}
	
	public Order getVirtualOrder(String fdealId) {
		Order order  = new Order();
		Log.run.info("订单数据查询开始--");
		String md5Text = "fdealId=" + fdealId + "&key=" + CONSTANTS_KEY;
		String sign = "";
		String result = "";
		try {
			sign = MD5Util.encrypt(md5Text.getBytes("gb2312"));
			String url = "http://vorder.soa.jd.com/order/select?fdealId=" + fdealId
					+ "&sign=" + sign;
			result = HttpUtil.get(url, 10000, 10000, "utf-8", true);
			Log.run.info("订单数据查询结束--result:" + result);
			
			if (result != null && !"".equals(result)) {
				 order = JsonUtils.readValue(result, Order.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.run.info("订单数据查询结束--");
		return order;
	}
	@Override
	public PagVritual getVirtualOrderList(long fbuyerUin,int pageNo,int pageSize,String fdealType) {
		String miText = "fbuyerUin="+fbuyerUin+"&key="+CONSTANTS_KEY;
		String recoresult = "";
		try {
			String signKey = MD5Util.encrypt(miText.getBytes("GB2312"));
			System.out.println(signKey);
			// 准备好两个cgi请求URL
			String url = "http://vorder.soa.jd.com/buyerOrder/selectOption?sign="+signKey+"&fbuyerUin="+fbuyerUin+"&pageNo="+pageNo+"&pageSize="+pageSize+"&fdealType="+fdealType;
			Log.run.info(url);
			recoresult = HttpUtil.get(url, 10000, 10000, "utf-8", true);
			//Pagination pagination = JsonUtils.readValue(recoresult, Pagination.class);
			Pagination pagination = new Gson().fromJson(recoresult, Pagination.class);
			Log.run.info("-----------------JsonUtils.readValue-----------end");
			List<Map> resultList = pagination.getDataList();
			List<VritualOrder> voVector = splitResut(resultList);
			// 拆分数据
			PagVritual page = new PagVritual();
			page.setPageCount(pagination.getPageCount());
			page.setPageNo(pagination.getPageNo());
			page.setPageSize(pagination.getPageSize());
			page.setTotalItem(pagination.getTotalItem());
			page.setVirtualVc(voVector);
			return page;
		} catch (Exception e) {
			// 如果有异常。返回调用失败
			Log.run.info("-----------------recoresult"+recoresult);
			PagVritual page = new PagVritual();
			return page;
		}
	}
	/**
	 * 拆分接口返回订单数据
	 * @param resultList
	 * @return
	 * @throws Exception
	 */
	private List<VritualOrder> splitResut(List<Map> resultList) {
		List<VritualOrder> list = new ArrayList<VritualOrder>();
		Log.run.info("-----------------splitResut-----------begin");
		Log.run.info("---->-->-->splitResutresultList:"+resultList);
		for (Map map : resultList) {
			VritualOrder virtualOrderTemp = new VritualOrder();
			// 订单编号
			virtualOrderTemp.setFdealId(new Double((Double) map.get("fdealId")).intValue());
			//订单类型，5：Q币 6：话费
			virtualOrderTemp.setFdealType(new Double((Double) map.get("fdealType")).intValue());
			// 下单时间
			virtualOrderTemp.setFdealCreateTime((String)map.get("fdealCreateTime"));
			virtualOrderTemp.setFrechargeAccount((String)map.get("frechargeAccount"));
			String text = (String)map.get("orderDetailText");
			if(text!=null && !"".equals(text)){
				OrderProduct orderProduct = JsonUtils.readValue(text.substring(1, text.length()-1), OrderProduct.class);
				virtualOrderTemp.setFitemId(orderProduct.getFitemId());
				virtualOrderTemp.setFitemName(orderProduct.getFitemName());
				if(orderProduct.getFitemPrice()!=null){
				/*	BigDecimal itemPrice = new BigDecimal(orderProduct.getFitemPrice());
					virtualOrderTemp.setFitemPrice(itemPrice.divide(new BigDecimal("100")));*/
					virtualOrderTemp.setFitemPrice(orderProduct.getFitemPrice());
				}
				virtualOrderTemp.setFitemSkuId(orderProduct.getFitemSkuId());
				virtualOrderTemp.setFdealItemCount(orderProduct.getFdealItemCount());
				virtualOrderTemp.setFitemLogo(orderProduct.getFitemLogo());
			}
			Integer amount = new Double((Double) map.get("fdealPayFeeTotal")).intValue();
			//virtualOrderTemp.setAmount(new BigDecimal(amount).divide(new BigDecimal("100")));
			virtualOrderTemp.setFdealPayFeeTotal(amount);
			virtualOrderTemp.setFbuyerUin(new Double((Double) map.get("fbuyerUin")).intValue());
			virtualOrderTemp.setFsellerUin(new Double((Double) map.get("fsellerUin")).intValue());
			virtualOrderTemp.setFdealState(new Double((Double) map.get("fdealState")).intValue());
			virtualOrderTemp.setFsellerName((String)map.get("fsellerName"));
			// 支付方式 0未定义 1财付通 2货到付款 3分期付款 4移动积分5微信支付
			virtualOrderTemp.setFdealPayType(new Double((Double) map.get("fdealPayType")).intValue());
			list.add(virtualOrderTemp);
		}
		Log.run.info("-----------------splitResut-----------end");
		return list;
	}
	/**
	 * 取消订单
	 */
	@Override
	public String updateCancel(String fdealId) {
		String flag = "0";
		try {
			String md5Text = "fdealId="+fdealId+"&key="+CONSTANTS_KEY;
			String sign  = MD5Util.encrypt(md5Text.getBytes("gb2312"));
			String url = "http://vorder.soa.jd.com/order/updateOrderCancel?sign="+sign+"&fdealId="+fdealId;
			System.out.println(url);
			String result = HttpUtil.get(url, 10000, 10000, "utf-8", true);
			Log.run.info("订单关闭数据返回结果："+result);
			flag =result;
			/*if(result!=null){
				Map<String, Object> map = JsonUtils.readValue(result);
				// 成功更改订单状态后，支付表和任务表数据同步更改
				if("F10000".equals(map.get("code"))){
					flag = "1";
				}
			}*/
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return flag;
	}

}
