package com.qq.qqbuy.virtualgoods.biz;

import com.qq.qqbuy.virtualgoods.model.Order;
import com.qq.qqbuy.virtualgoods.model.PagVritual;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderInfo;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderResp;
/**
 * 

 * @ClassName: VirtualBiz 

 * @Description: 虚拟商品的接口
 
 * @author lhn

 * @date 2014-12-18 下午3:19:30 

 *
 */
public interface VirtualBiz {
	/**
	 * @param dealItemCount 
	 * 
	
	 * @Title: getVirtualOrder 
	
	 * @Description: 用于根据传入的值来构造对象 
	
	 * @param @param buyerUin
	 * @param @param cp
	 * @param @param dealType
	 * @param @param dealSource
	 * @param @param dealPayType
	 * @param @param rechargeAccount
	 * @param @param itemId
	 * @param @param isAutoSendItem
	 * @param @param itemOriginalPrice
	 * @param @param skuId
	 * @param @return    设定文件 
	
	 * @return SubmitVirtualOrderInfo    返回类型 
	
	 * @throws
	 */
	SubmitVirtualOrderInfo getVirtualOrder(long buyerUin, String cp,
										   short dealType, short dealSource, short dealPayType,
										   String rechargeAccount, String itemId, int isAutoSendItem,
										   long itemOriginalPrice, long skuId, long dealItemCount);
	/**
	 * 
	
	 * @Title: submitVirtual 
	
	 * @Description: 提交订单 
	
	 * @param @param virtualorder
	 * @param @return    设定文件 
	
	 * @return SubmitVirtualOrderResp    返回类型 
	
	 * @throws
	 */
	SubmitVirtualOrderResp submitVirtual(SubmitVirtualOrderInfo virtualorder);
	/**
	 * 
	
	 * @Title: getVirtualOrder 
	
	 * @Description: 根据ID查出订单
	
	 * @param @param fdealId
	 * @param @return    设定文件 
	
	 * @return Order    返回类型 
	
	 * @throws
	 */
	Order getVirtualOrder(String fdealId);
	/**
	 * 
	
	 * @Title: getVirtualOrderList 
	
	 * @Description: 根据当前时间查出该人虚拟商品列表
	
	 * @param @param fbuyerUin
	 * @param @return    设定文件 
	
	 * @return Pagination    返回类型 
	
	 * @throws
	 */
	PagVritual getVirtualOrderList(long fbuyerUin, int pageNo, int pageSize, String fdealType);
	/**
	 * 
	
	 * @Title: updateCancel 
	
	 * @Description: 取消虚拟订单
	
	 * @param @param fdealId
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	String updateCancel(String fdealId);


}
