package com.qq.qqbuy.virtualgoods;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderReq;
import com.qq.qqbuy.virtualgoods.protocol.SubmitVirtualOrderResp;

/**
 * 
 * 
 * @ClassName: VirtualClient
 * 
 * @Description: 处理idl
 * 
 * @author lhn
 * 
 * @date 2014-12-18 下午3:51:29
 * 
 * 
 */
public class VirtualClient extends SupportIDLBaseClient {

	public static SubmitVirtualOrderResp submitVir(SubmitVirtualOrderReq req) {
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		SubmitVirtualOrderResp resp = new SubmitVirtualOrderResp();

		invoke(stub, req, resp);
		if(resp.getResult() != 0)
			throw new ExternalInterfaceException(0, resp.getResult(), resp.getErrMsg());
		return resp;
	}

}
