package com.qq.qqbuy.virtualgoods.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *提交虚拟订单请求类
 *
 *@date 2014-12-17 04:53:39
 *
 *@since version:0
*/
public class  SubmitVirtualOrderReq extends NetMessage
{
	/**
	 * 要添加的订单PO
	 *
	 * 版本 >= 0
	 */
	 private SubmitVirtualOrderInfo submitVirtualOrderInfo = new SubmitVirtualOrderInfo();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(submitVirtualOrderInfo);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		submitVirtualOrderInfo = (SubmitVirtualOrderInfo) bs.popObject(SubmitVirtualOrderInfo.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x55221801L;
	}


	/**
	 * 获取要添加的订单PO
	 * 
	 * 此字段的版本 >= 0
	 * @return submitVirtualOrderInfo value 类型为:SubmitVirtualOrderInfo
	 * 
	 */
	public SubmitVirtualOrderInfo getSubmitVirtualOrderInfo()
	{
		return submitVirtualOrderInfo;
	}


	/**
	 * 设置要添加的订单PO
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SubmitVirtualOrderInfo
	 * 
	 */
	public void setSubmitVirtualOrderInfo(SubmitVirtualOrderInfo value)
	{
		if (value != null) {
				this.submitVirtualOrderInfo = value;
		}else{
				this.submitVirtualOrderInfo = new SubmitVirtualOrderInfo();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(SubmitVirtualOrderReq)
				length += ByteStream.getObjectSize(submitVirtualOrderInfo);  //计算字段submitVirtualOrderInfo的长度 size_of(SubmitVirtualOrderInfo)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
