package com.qq.qqbuy.virtualgoods.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *订单po
 *
 *@date 2014-12-17 04:53:39
 *
 *@since version:0
*/
public class SubmitVirtualOrderInfo  implements ICanSerializeObject
{
	/**
	 * 订单po版本
	 *
	 * 版本 >= 0
	 */
	 private short version = 0;

	/**
	 * 买家编号，必填项
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 订单类型：5-虚拟直充订单，必填项
	 *
	 * 版本 >= 0
	 */
	 private short dealType;

	/**
	 * 订单来源	1：网站，2：APP
	 *
	 * 版本 >= 0
	 */
	 private short dealSource;

	/**
	 * 支付方式：1-财付通5-微信支付，必填项
	 *
	 * 版本 >= 0
	 */
	 private short dealPayType;

	/**
	 * 访问者ip，必填项
	 *
	 * 版本 >= 0
	 */
	 private String ip = new String();

	/**
	 * 充值账户，必填项
	 *
	 * 版本 >= 0
	 */
	 private String rechargeAccount = new String();

	/**
	 * 费用合计，非必填
	 *
	 * 版本 >= 0
	 */
	 private long dealPayFeeTotal;

	/**
	 * 订单中的商品，目前只支持一单一品，必填项
	 *
	 * 版本 >= 0
	 */
	 private SubmitVirtualProductInfo submitVirtualProductInfo = new SubmitVirtualProductInfo();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short buyerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealType_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealSource_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealPayType_u;

	/**
	 * 版本 >= 0
	 */
	 private short ip_u;

	/**
	 * 版本 >= 0
	 */
	 private short rechargeAccount_u;

	/**
	 * 版本 >= 0
	 */
	 private short dealPayFeeTotal_u;

	/**
	 * 版本 >= 0
	 */
	 private short submitVirtualProductInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUByte(version);
		bs.pushUInt(buyerUin);
		bs.pushUByte(dealType);
		bs.pushUByte(dealSource);
		bs.pushUByte(dealPayType);
		bs.pushString(ip);
		bs.pushString(rechargeAccount);
		bs.pushUInt(dealPayFeeTotal);
		bs.pushObject(submitVirtualProductInfo);
		bs.pushUByte(version_u);
		bs.pushUByte(buyerUin_u);
		bs.pushUByte(dealType_u);
		bs.pushUByte(dealSource_u);
		bs.pushUByte(dealPayType_u);
		bs.pushUByte(ip_u);
		bs.pushUByte(rechargeAccount_u);
		bs.pushUByte(dealPayFeeTotal_u);
		bs.pushUByte(submitVirtualProductInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUByte();
		buyerUin = bs.popUInt();
		dealType = bs.popUByte();
		dealSource = bs.popUByte();
		dealPayType = bs.popUByte();
		ip = bs.popString();
		rechargeAccount = bs.popString();
		dealPayFeeTotal = bs.popUInt();
		submitVirtualProductInfo = (SubmitVirtualProductInfo) bs.popObject(SubmitVirtualProductInfo.class);
		version_u = bs.popUByte();
		buyerUin_u = bs.popUByte();
		dealType_u = bs.popUByte();
		dealSource_u = bs.popUByte();
		dealPayType_u = bs.popUByte();
		ip_u = bs.popUByte();
		rechargeAccount_u = bs.popUByte();
		dealPayFeeTotal_u = bs.popUByte();
		submitVirtualProductInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取订单po版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:short
	 * 
	 */
	public short getVersion()
	{
		return version;
	}


	/**
	 * 设置订单po版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion(short value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取买家编号，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家编号，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
		this.buyerUin_u = 1;
	}


	/**
	 * 获取订单类型：5-虚拟直充订单，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType value 类型为:short
	 * 
	 */
	public short getDealType()
	{
		return dealType;
	}


	/**
	 * 设置订单类型：5-虚拟直充订单，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType(short value)
	{
		this.dealType = value;
		this.dealType_u = 1;
	}


	/**
	 * 获取订单来源	1：网站，2：APP
	 * 
	 * 此字段的版本 >= 0
	 * @return dealSource value 类型为:short
	 * 
	 */
	public short getDealSource()
	{
		return dealSource;
	}


	/**
	 * 设置订单来源	1：网站，2：APP
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealSource(short value)
	{
		this.dealSource = value;
		this.dealSource_u = 1;
	}


	/**
	 * 获取支付方式：1-财付通5-微信支付，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return dealPayType value 类型为:short
	 * 
	 */
	public short getDealPayType()
	{
		return dealPayType;
	}


	/**
	 * 设置支付方式：1-财付通5-微信支付，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayType(short value)
	{
		this.dealPayType = value;
		this.dealPayType_u = 1;
	}


	/**
	 * 获取访问者ip，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return ip value 类型为:String
	 * 
	 */
	public String getIp()
	{
		return ip;
	}


	/**
	 * 设置访问者ip，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIp(String value)
	{
		this.ip = value;
		this.ip_u = 1;
	}


	/**
	 * 获取充值账户，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return rechargeAccount value 类型为:String
	 * 
	 */
	public String getRechargeAccount()
	{
		return rechargeAccount;
	}


	/**
	 * 设置充值账户，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRechargeAccount(String value)
	{
		this.rechargeAccount = value;
		this.rechargeAccount_u = 1;
	}


	/**
	 * 获取费用合计，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @return dealPayFeeTotal value 类型为:long
	 * 
	 */
	public long getDealPayFeeTotal()
	{
		return dealPayFeeTotal;
	}


	/**
	 * 设置费用合计，非必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayFeeTotal(long value)
	{
		this.dealPayFeeTotal = value;
		this.dealPayFeeTotal_u = 1;
	}


	/**
	 * 获取订单中的商品，目前只支持一单一品，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @return submitVirtualProductInfo value 类型为:SubmitVirtualProductInfo
	 * 
	 */
	public SubmitVirtualProductInfo getSubmitVirtualProductInfo()
	{
		return submitVirtualProductInfo;
	}


	/**
	 * 设置订单中的商品，目前只支持一单一品，必填项
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SubmitVirtualProductInfo
	 * 
	 */
	public void setSubmitVirtualProductInfo(SubmitVirtualProductInfo value)
	{
		if (value != null) {
				this.submitVirtualProductInfo = value;
				this.submitVirtualProductInfo_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin_u value 类型为:short
	 * 
	 */
	public short getBuyerUin_u()
	{
		return buyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerUin_u(short value)
	{
		this.buyerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType_u value 类型为:short
	 * 
	 */
	public short getDealType_u()
	{
		return dealType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType_u(short value)
	{
		this.dealType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealSource_u value 类型为:short
	 * 
	 */
	public short getDealSource_u()
	{
		return dealSource_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealSource_u(short value)
	{
		this.dealSource_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealPayType_u value 类型为:short
	 * 
	 */
	public short getDealPayType_u()
	{
		return dealPayType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayType_u(short value)
	{
		this.dealPayType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ip_u value 类型为:short
	 * 
	 */
	public short getIp_u()
	{
		return ip_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIp_u(short value)
	{
		this.ip_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return rechargeAccount_u value 类型为:short
	 * 
	 */
	public short getRechargeAccount_u()
	{
		return rechargeAccount_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRechargeAccount_u(short value)
	{
		this.rechargeAccount_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dealPayFeeTotal_u value 类型为:short
	 * 
	 */
	public short getDealPayFeeTotal_u()
	{
		return dealPayFeeTotal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealPayFeeTotal_u(short value)
	{
		this.dealPayFeeTotal_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return submitVirtualProductInfo_u value 类型为:short
	 * 
	 */
	public short getSubmitVirtualProductInfo_u()
	{
		return submitVirtualProductInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSubmitVirtualProductInfo_u(short value)
	{
		this.submitVirtualProductInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SubmitVirtualOrderInfo)
				length += 1;  //计算字段version的长度 size_of(uint8_t)
				length += 4;  //计算字段buyerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段dealType的长度 size_of(uint8_t)
				length += 1;  //计算字段dealSource的长度 size_of(uint8_t)
				length += 1;  //计算字段dealPayType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ip);  //计算字段ip的长度 size_of(String)
				length += ByteStream.getObjectSize(rechargeAccount);  //计算字段rechargeAccount的长度 size_of(String)
				length += 4;  //计算字段dealPayFeeTotal的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(submitVirtualProductInfo);  //计算字段submitVirtualProductInfo的长度 size_of(SubmitVirtualProductInfo)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段buyerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealSource_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealPayType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ip_u的长度 size_of(uint8_t)
				length += 1;  //计算字段rechargeAccount_u的长度 size_of(uint8_t)
				length += 1;  //计算字段dealPayFeeTotal_u的长度 size_of(uint8_t)
				length += 1;  //计算字段submitVirtualProductInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
