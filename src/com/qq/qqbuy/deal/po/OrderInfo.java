package com.qq.qqbuy.deal.po;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.po.ItemInfoWithPrice;
import com.qq.qqbuy.item.po.Mstock4J;
import com.qq.qqbuy.item.po.wap2.ItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetItemResponse.Stock;

/**
 * 下单po
 * 
 * @author homerwu
 * 
 */
public class OrderInfo {

	/************************************ 请求参数 *****************************/
	private String sid;
	private String itemCode;
	private int payType;
	private int addressId;
	private long uin;
	private long itemCount;
	private int redPackageId;
	private String stockAttr;
	private int stockId;
	private String buyerNote = "";
	private String expressType;
	private String priceType;

	/**
	 * 促销信息
	 */
	private int shopcouponId; // 店铺代金券id

	private int promotionId; // 促销规则id
	
	private long onlineShoppingCoupons; // 网购代金卷

	private long redBagScore;//红包积分

	private String reserved; // 主要用于pps参数
	private int orderFrom;
	private long sceneId;//场景
	/************************************ vo *****************************/
	private GetItemResponse ppItem = new GetItemResponse();
	private ItemInfoWithPrice qgoItem = new ItemInfoWithPrice();

	// pp侧下单后有值
	private GetDealDetailResponse dealDetail = new GetDealDetailResponse();
	private long dealTotalFee;
	private long superQQDiscount;
	private int discountType;
	private int superQQLevel;
	private int diamondLevel;

	/************************************ construction method *****************************/
	public OrderInfo(String sid, String itemCode, int payType, int addressId, long uin, long itemCount,
			int redPackageId, int stockId, String buyerNote, String reserved, String sa) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockId = stockId;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.stockAttr = sa;
	}

	public OrderInfo(String sid, String itemCode, int payType, int addressId, long uin, long itemCount,
			int redPackageId, String stockAttr, String buyerNote, String reserved, String expressType, String priceType) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockAttr = stockAttr;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.expressType = expressType;
		this.priceType = priceType;
	}
	
	public OrderInfo(String sid, String itemCode, int payType, int addressId, long uin, long itemCount,
			int redPackageId, String stockAttr, String buyerNote, String reserved, String expressType,
			String priceType, int shopcouponId, int promotionId, long onlineShoppingCoupons) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockAttr = stockAttr;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.expressType = expressType;
		this.priceType = priceType;
		this.shopcouponId = shopcouponId;
		this.promotionId = promotionId;
		this.onlineShoppingCoupons = onlineShoppingCoupons;
	}

	public OrderInfo(String sid, String itemCode, int payType, int addressId, long uin, long itemCount,
					 int redPackageId, String stockAttr, String buyerNote, String reserved, String expressType,
					 String priceType, int shopcouponId, int promotionId, long onlineShoppingCoupons,long redBagScore,long sceneId) {
		this.sid = sid;
		this.itemCode = itemCode;
		this.payType = payType;
		this.addressId = addressId;
		this.uin = uin;
		this.itemCount = itemCount;
		this.redPackageId = redPackageId;
		this.stockAttr = stockAttr;
		this.buyerNote = buyerNote;
		this.reserved = reserved;
		this.expressType = expressType;
		this.priceType = priceType;
		this.shopcouponId = shopcouponId;
		this.promotionId = promotionId;
		this.onlineShoppingCoupons = onlineShoppingCoupons;
		this.redBagScore = redBagScore;
		this.sceneId = sceneId;
	}

	public OrderInfo(String itemCode, int stockId, long itemCount, String sa) {
		this.itemCode = itemCode;
		this.stockId = stockId;
		this.itemCount = itemCount;
		this.stockAttr = sa;
	}

	public OrderInfo() {
	}

	/************************************ biz util *****************************/
	public void initItemInfo() throws BusinessException {
		ItemBiz biz = (ItemBiz) SpringHelper.getBean("itemBiz");
		// if("idc".equals(System.getProperty("qgo.cfg.env"))){
		// ppItem = biz.getItemFromPaipai(itemCode, false, false, true);
		// qgoItem = biz.getItemFromQgo(itemCode);
		// }else{
		ItemResponse resResult = biz.getItemFromPaipai2(itemCode, false, false, true, true);
		if (resResult != null) {
			ppItem = resResult.getPpItemInfo();
			qgoItem.setMitem(resResult.getMmItemInfo());
		}
	}

	public long caculateMppTotalDealFee() throws BusinessException {
		if (this.getQgoItem().getMitem().isSupportCOD() && getQgoItem().getMitem().isFree4Freight()) {
			return this.getDealDetail().getDealPayFeeTotal() + getQgoItem().getMitem().getAdditionalFee() * itemCount;
		} else {
			return this.getDealDetail().getDealPayFeeTotal();
		}
	}

	public Mstock4J getCurrentStock() throws BusinessException {
		if (this.ppItem.isMobileItem()) {
			if (StringUtil.isEmpty(stockAttr)) {
				Mstock4J stock = new Mstock4J();
				stock.setStockPrice(getQgoItem().getMitem().getItemPrice());
				stock.setDiscountPrice(getQgoItem().getMitem().getDiscountPrice());
				return stock;
			} else {
				for (Mstock4J stock : getQgoItem().getMitem().getStockList()) {
					if (stockAttr.equals(stock.getStockAttr())) {
						return stock;
					}
				}
			}
		} else {
			Mstock4J stock = new Mstock4J();
			if (StringUtil.isEmpty(stockAttr)) {
				stock.setStockPrice(getPpItem().getItemPrice());
				stock.setDiscountPrice(getPpItem().discountPrice);
				return stock;
			}
			for (Stock respStock : this.ppItem.stockList) {
				if (stockAttr.equals(respStock.stockAttr)) {
					stock.setStockId(respStock.stockId);
					stock.setStockAttr(respStock.stockAttr);
					stock.setStockPrice(respStock.stockPrice);
					stock.setDiscountPrice(respStock.discountPrice);
					return stock;
				}
			}
		}
		throw BusinessException.createInstance(BusinessErrorType.INVALID_STOCKATTR);
	}

	public void caculateDiscount() throws BusinessException {
		// long discount = 0;
		// discount = this.getCurrentStock().getStockPrice() -
		// this.getCurrentStock().getDiscountPrice().get(discountType);
		long discount = this.getCurrentStock().getStockDiscountPrice(discountType);// 取折扣价格
		this.superQQDiscount = discount > 0 ? discount : 0;
	}

	/************************************ setter getter *****************************/
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public long getUin() {
		return uin;
	}

	public void setUin(long uin) {
		this.uin = uin;
	}

	public long getItemCount() {
		return itemCount;
	}

	public void setItemCount(long itemCount) {
		this.itemCount = itemCount;
	}

	public GetItemResponse getPpItem() {
		return ppItem;
	}

	public void setPpItem(GetItemResponse ppItem) {
		this.ppItem = ppItem;
	}

	public ItemInfoWithPrice getQgoItem() {
		return qgoItem;
	}

	public void setQgoItem(ItemInfoWithPrice qgoItem) {
		this.qgoItem = qgoItem;
	}

	public GetDealDetailResponse getDealDetail() {
		return dealDetail;
	}

	public void setDealDetail(GetDealDetailResponse dealDetail) {
		this.dealDetail = dealDetail;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getReserved() {
		return reserved;
	}

	public void setReserved(String reserved) {
		this.reserved = reserved;
	}

	public int getRedPackageId() {
		return redPackageId;
	}

	public void setRedPackageId(int redPackageId) {
		this.redPackageId = redPackageId;
	}

	public String getBuyerNote() {
		return buyerNote;
	}

	public void setBuyerNote(String buyerNote) {
		this.buyerNote = buyerNote;
	}

	public String getStockAttr() {
		return stockAttr;
	}

	public void setStockAttr(String stockAttr) {
		this.stockAttr = stockAttr;
	}

	public String getExpressType() {
		return expressType;
	}

	public void setExpressType(String expressType) {
		this.expressType = expressType;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public long getDealTotalFee() {
		return dealTotalFee;
	}

	public void setDealTotalFee(long dealTotalFee) {
		this.dealTotalFee = dealTotalFee;
	}

	public int getStockId() {
		return stockId;
	}

	public void setStockId(int stockId) {
		this.stockId = stockId;
	}

	public long getSuperQQDiscount() {
		return superQQDiscount;
	}

	public void setSuperQQDiscount(long superQQDiscount) {
		this.superQQDiscount = superQQDiscount;
	}

	public int getDiscountType() {
		return discountType;
	}

	public void setDiscountType(int discountType) {
		this.discountType = discountType;
	}

	public int getSuperQQLevel() {
		return superQQLevel;
	}

	public void setSuperQQLevel(int superQQLevel) {
		this.superQQLevel = superQQLevel;
	}

	public int getDiamondLevel() {
		return diamondLevel;
	}

	public void setDiamondLevel(int diamondLevel) {
		this.diamondLevel = diamondLevel;
	}

	public int getOrderFrom() {
		return orderFrom;
	}

	public void setOrderFrom(int orderFrom) {
		this.orderFrom = orderFrom;
	}

	public int getShopcouponId() {
		return shopcouponId;
	}

	public void setShopcouponId(int shopcouponId) {
		this.shopcouponId = shopcouponId;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public long getOnlineShoppingCoupons() {
		return onlineShoppingCoupons;
	}

	public void setOnlineShoppingCoupons(long onlineShoppingCoupons) {
		this.onlineShoppingCoupons = onlineShoppingCoupons;
	}

	public long getRedBagScore() {
		return redBagScore;
	}

	public void setRedBagScore(long redBagScore) {
		this.redBagScore = redBagScore;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}
}
