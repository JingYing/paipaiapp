package com.qq.qqbuy.deal.po;

import java.util.List;

import com.qq.qqbuy.common.util.region.Area;
import com.qq.qqbuy.common.util.region.City;
import com.qq.qqbuy.common.util.region.Province;

/**
 * 收货地址类，用于按照特定格式输出到客户端
 * 
 * @author ethonchan
 * 
 */
public class RegionFormatter {
	
	/**
	 * 将省份信息及版本号按照客户端可以理解的格式输出
	 * @param regionVersion
	 * @param provinceList
	 * @return
	 */
	public static String toString(int regionVersion, List<Province> provinceList) {
		StringBuilder sb = new StringBuilder();
		appendObjectStart(sb, "regions");

		appendParam(sb, "version", regionVersion);
		appendComma(sb);

		appendArrayStart(sb, "provinces");

		if (provinceList != null) {
			for (int i = 0, size = provinceList.size(); i < size; i++) {
				appendProvince(sb, provinceList.get(i));
				
				if(i + 1 != size){
					appendComma(sb);
				}
			}
		}
		appendArrayEnd(sb);
		appendObjectEnd(sb);

		return sb.toString();
	}

	/**
	 * 输出省份信息
	 * @param sb
	 * @param prov
	 */
	private static void appendProvince(StringBuilder sb, Province prov) {
		appendObjectStart(sb);

		appendParam(sb, "name", prov.getProvinceName());
		appendComma(sb);

		appendParam(sb, "id", Integer.toString(prov.getProvinceId()));
		appendComma(sb);

		appendArrayStart(sb, "city1");
		List<City> cityList = prov.getCityList();
		if (cityList != null) {
			for (int i = 0, size = cityList.size(); i < size; i++) {
				appendCity(sb, cityList.get(i));
				if (i + 1 != size) {
					appendComma(sb);
				}
			}
		}
		appendArrayEnd(sb);

		appendObjectEnd(sb);
	}

	/**
	 * 输出城市信息
	 * @param sb
	 * @param city
	 */
	private static void appendCity(StringBuilder sb, City city) {
		appendObjectStart(sb);

		appendParam(sb, "name", city.getCityName());
		appendComma(sb);

		appendParam(sb, "id", Integer.toString(city.getCityId()));
		appendComma(sb);

		appendArrayStart(sb, "city2");
		List<Area> areaList = city.getAreaList();
		if (areaList != null) {
			for (int i = 0, size = areaList.size(); i < size; i++) {
				appendArea(sb, areaList.get(i));

				if (i + 1 != size) {
					appendComma(sb);
				}
			}
		}
		appendArrayEnd(sb);

		appendObjectEnd(sb);
	}

	/**
	 * 输出区信息
	 * @param sb
	 * @param area
	 */
	private static void appendArea(StringBuilder sb, Area area) {
		appendObjectStart(sb);

		appendParam(sb, "name", area.getName());
		appendComma(sb);

		appendParam(sb, "id", Integer.toString(area.getAreaId()));

		appendObjectEnd(sb);
	}

	private static void appendObjectStart(StringBuilder sb) {
		sb.append("{");
	}

	private static void appendObjectStart(StringBuilder sb, String name) {
		sb.append("\"").append(name).append("\":");
		sb.append("{");
	}

	private static void appendObjectEnd(StringBuilder sb) {
		sb.append("}");
	}

	private static void appendArrayStart(StringBuilder sb, String name) {
		sb.append("\"").append(name).append("\":");
		sb.append("[");
	}

	private static void appendArrayEnd(StringBuilder sb) {
		sb.append("]");
	}

	private static void appendParam(StringBuilder sb, String name, String value) {
		sb.append("\"" + name + "\":");
		sb.append("\"").append(value).append("\"");
	}

	private static void appendParam(StringBuilder sb, String name, int value) {
		sb.append("\"" + name + "\":");
		sb.append(value);
	}

	private static void appendComma(StringBuilder sb) {
		sb.append(",");
	}
}
