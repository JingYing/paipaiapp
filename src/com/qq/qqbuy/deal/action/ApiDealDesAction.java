package com.qq.qqbuy.deal.action;

import java.util.List;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.paipai.component.configcenter.itil.ItilReporter;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.cmdy.biz.CmdyBiz;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.client.cache.CacheClient;
import com.qq.qqbuy.common.client.cache.CacheKey;
import com.qq.qqbuy.common.constant.BusinessErrConstants;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.ItilConstants;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.biz.DealBiz;
import com.qq.qqbuy.deal.po.ConfirmOrderPo;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.deal.po.GetTakeCheapDealDetailResponse;
import com.qq.qqbuy.deal.po.OrderInfo;
import com.qq.qqbuy.deal.po.TakeCheapOrderInfo;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;

/**
 * APP一口价下单接口
 * 
 * @author matrixshi(最新修改)
 * 
 */
public class ApiDealDesAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = 1L;

	/* req */
	private String it;// itemCode
	private int ic = 1;// itemCount

	private int pt = 0; // payType
	private int adid; // addressId
	private int pkgid; // redpackageId
	private String sa;// stockAttr
	private String bn;// buyerNote
	private String rv;// reserved,for pps
	private String et;// expressType
	private String prt;// priceType
	private String source;

	/* 促销 */
	private int prId; // 促销规则id
	private int coupId; // 店铺代金券id
	private long cashId; // 网购代金卷
	private long redBagScore;//红包积分
	private long sceneId;//活动场景
	// 立即购买
	private CmdyBiz cmdyBiz;
	private ConfirmOrderVo confirmVo;
	private String itemList;
	private String internalSource;//内部来源字段（内部访问路径拼装的）-王志刚用
	private String externalSource;//外部来源字段（唤起协议中传过来的）-王志刚用

	/* resp */
	private OrderInfo orderInfo;

	/* resp */
	private GetDealDetailResponse orderResp = new GetDealDetailResponse();
	private String dataStr;

	/* biz */
	DealBiz ppDealBiz;
	BiBiz biBiz;
	private RecvAddrBiz recvAddrBiz;

	private CacheClient cacheClient; 
	
	//是否进行验证
	private static final boolean isChecke = true;
	private static Logger limitLog = LogManager.getLogger("limitolderLog");
	
	public String submit() {
			Log.run.info("app start deal,adid=" + adid + ",it=" + it + ",pt=" + pt + ",uin=" + getQq() + ",ic=" + ic
					+ ",et=" + et + ",prId=" + prId + ",coupId=" + coupId  + ",sa=" + sa + ",rv=" + rv);

			/* 商品信息 */
			orderInfo = new OrderInfo(getSid(), it, this.pt, adid, getQq(), ic, pkgid, sa, bn, rv, et, prt,
					coupId, prId, cashId,redBagScore,sceneId);
			Log.run.info(new Gson().toJson(orderInfo));
		return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_API_FAILURE;
	}
	
	
	//是否可以再下单，防刷，但有一个隐患，redis会扛很多并发请求
	public boolean issumbit(String itemId,Long wid){
		if(isChecke){
			try {
				Integer n =  (Integer) cacheClient.get(CacheKey.订单频率+itemId+wid);
		    	if(n==null){
		    		cacheClient.setex(CacheKey.订单频率+itemId+wid, 300, 1);
		    		return true;
		    	}else {
		    		if(n>=3){
		    			limitLog.info("issumbitnopass:id"+wid+", issumbitnopass:itemId:"+itemId);
		    			//Log.run.info("issumbitnopass:id"+wid+", issumbitnopass:itemId:"+itemId);
		    			return true;
		    		}else {
		    			Long ntime = (Long) cacheClient.ttl(CacheKey.订单频率+itemId+wid);
		    			cacheClient.setex(CacheKey.订单频率+itemId+wid, ntime.intValue(), n+1);
		    			return true;
		    		}
		    	}
			} catch (Exception e) {
				e.printStackTrace();
				limitLog.info("redischeckerror!");
				//Log.run.error("redischeckerror!");
				return true;
			}
				
		}else {
			return true;
		}
    	
	}
	public String submitV2() {
//		long start = System.currentTimeMillis();

		try {
			// 鉴权
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if (StringUtil.isEmpty(it + "") || (!validateUserInfo())) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "商品信息错误");
				return RST_FAILURE;
			}

			cmdyBiz.add(getQq(), it, sa, ic, true, 0);
			if (result.getErrCode() == ErrConstant.SUCCESS) {

				// 加入购物车成功后分单
				if (sa == null) {
					itemList = it + "$$" + ic;
				} else {
					itemList = it + "$" + sa + "$" + ic;
				}
				ConfirmOrderPo po = new ConfirmOrderPo(getQq(), pt, itemList, adid);
				po.setSk(getSk());
				po.setVer(2);

				confirmVo = cmdyBiz.confirmOrderCmdy(po, getSk());
				JsonConfig jsoncfg = new JsonConfig();

				String[] excludes = { "decodeAttr", "sdPrice", "disTotalPrice", "recv", "defaultAddr" };
				jsoncfg.setExcludes(excludes);
				dataStr = JSONSerializer.toJSON(confirmVo, jsoncfg).toString();

				// dataStr = obj2Json(confirmVo);
				return RST_SUCCESS;
			}

		} catch (BusinessException e) {
			setNeedAlarm(false);
			result.setErrInfo(e.getErrCode(), e.getErrMsg());
		} catch (Exception e) {
			Log.run.warn(e.getMessage(), e);
			setNeedAlarm(true);
			result.setErrInfo(BusinessErrorType.UNKNOWN_EXP.getErrCode(), BusinessErrorType.UNKNOWN_EXP.getErrMsg());
		}  
		return result.getErrCode() == ErrConstant.SUCCESS ? RST_SUCCESS : RST_API_FAILURE;
	}
	
	
	
	

	/**
	 *  拍便宜下单结算参数串，格式为：
	 *  1、订单信息
	 *  orderSr = 0,（货到付款时为快递方式11/12/13/14 在线支付时免运费为0 normal为1express为2ems为3free为0）,批价返回的promotionId，发票抬头invoiceTitle默认为“”，留言message默认为“”，批价返回店铺优惠shopCouponId，批价返回wgCouponId，批价返回balanceId
	 *  2、商品信息OrderCmdyInfo, 以"$"分隔每个商品信息，每个商品信息各项以","分隔，分别是：商品ID,库存属性,商品数量,商品类型,多价类型,红包ID,商品skuid
	 *  commStr=cid，stock，num，ComdyType，批价返回priceId，hongbao，skuId
	 *  3、店铺信息
	 *  shopInfo = shopId，shopProp
	 *  综合下单参数串
	 *  submitStr= orderSr||commStr（多个之间用$分割）||shopInfo
	 *  多个submitStr之间用$$分割
	 *  "0,2,,,,,,,||0EF6FE4B00000000000000000BEC3F38,,1,0,65536,,140737488512428||1275000334,112"
	 */
	private String orderdata;
	/**
	 * 拍便宜下单参数，收货地址信息串，格式为：
	 * 收货人,手机号码,省市县,详细地址
	 */
	private String addressStr;
	/**
	 * 拍便宜下单对象
	 */
	private TakeCheapOrderInfo takeCheapOrderInfo;
	/**
	 * 开团吆喝信息
	 */
	private String tuanMsg;
	/**
	 * 是否团团降，1：是 0：否
	 */
	private int tuan;
	/**
	 * 参团人数
	 */
	private int groupnum;
	/**
	 * 活动数量
	 */
	private int activenum;
	/**
	 * 参团Id
	 */
	private long groupId;
	/**
	 *
	 */
	private int bid = 0;
	/**
	 * 支付方式，1：货到付款 0：在线支付
	 */
	private int paytype;
	/**
	 * 支付手段，payType货到付款时，paychannel=0，在线支付时，0：财付通 2：微信支付 4：手q支付 7:银联支付
	 */
	private int paychannel;
	/**
	 * 收货地址id
	 */
	private long addressid;
	/**
	 * 地址类型，默认为0，使用手q地址时为2
	 */
	private int addrType = 0;
	/**
	 * 暂时固定为1
	 */
	private int anonymous = 1;
	/**
	 * 默认为0
	 */
	private int otherpay = 0;
	/**
	 * 区域编号
	 */
	private long regionid;
	/**
	 * 详细收货地址
	 */
	private String recvaddr;
	/**
	 * 收货人
	 */
	private String recvname;
	/**
	 * 微信绑定手机号
	 */
	private String wxpay_mobile;
	/**
	 * 收货人手机号
	 */
	private String moblie;
	/**
	 * 卖家qq
	 */
	private long shopId;
	/**
	 * 判断是否为大V团
	 */
	private int isDav = 0;
	/**
	 * 拍便宜大V团参团优惠接口返回值
	 */
	private String promoteKey= "";
	/**
	 * 拍便宜场景
	 */
	private String scene = "";
	/**
	 * 拍便宜下单接口
	 * var tuanParam = isTuan() ?
	 ('&tuan=' + tuan + "&groupnum=" + groupnum + "&activenum=" + activenum + (!!gid ? '&gid=' + gid : '')) : '';
	 var param = "";
	 if(tuanMsg != ""){
	 param = "&tuanMsg=" + tuanMsg;
	 }
	 if(isJBT != ""){
	 param += "&isJBT=1";
	 }
	 if(isDaV == 1){
	 if(g.promoteKey != ""){
	 param += "&isDaV=2";
	 }else{
	 param += "&isDaV=1";
	 }
	 }
	 if(g.promoteKey != ""){
	 param += ('&pKey=' + g.promoteKey);
	 }
	 * @return
	 */
	public String takeCheapSubmit(){
		Log.run.debug("ApiDealAction.takeCheapSubmit()");

		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_FIX_MAKEORDER_REQUEST, 1); // 上报请求

//		long start = System.currentTimeMillis();
		try {
			// // 鉴权
			if (!checkLoginAndGetSkey()) {
				result.setErrInfo(BusinessErrorType.NOT_LOGIN.getErrCode(), BusinessErrorType.NOT_LOGIN.getErrMsg());
				return RST_API_FAILURE;
			}
			
			RecvAddr recvAddrs = recvAddrBiz.listRecvAddr(getWid(), false, false, 2, getSk());
			List<ReceiverAddress> addressList = recvAddrs.getAddressList();
			
			Log.run.debug("addressList.size():"+addressList.size());
			
			ReceiverAddress nowReceiverAddress = null;//当前使用的收货地址
			for (ReceiverAddress receiverAddress : addressList) {
				if(addressid == receiverAddress.getAddressId()){
					nowReceiverAddress = receiverAddress;
					break;
				}
			}
			
			Log.run.debug("addressid:"+addressid+";nowReceiverAddress:"+nowReceiverAddress);
			
			if(null != nowReceiverAddress){
				addressStr = nowReceiverAddress.getName() + ","
						+ nowReceiverAddress.getMobile() + ","
						+ nowReceiverAddress.getProvince() + nowReceiverAddress.getCity() + nowReceiverAddress.getDistrict() + ","
						+ nowReceiverAddress.getAddress();
				Log.run.debug("ApiDealAction.takeCheapSubmit()...addressStr:" + addressStr);
			}
			/**
			 * 微信/京东登陆返回
			 */
			String openid = getPinOrId(getAppToken());
			Log.run.info("openid is : " + openid);
			/* 订单信息 */
			takeCheapOrderInfo = new TakeCheapOrderInfo(shopId,orderdata,addressStr,paytype
														,paychannel,addressid,addrType
														,anonymous,otherpay,regionid
														,recvaddr,recvname
														,null != nowReceiverAddress ? Long.parseLong(nowReceiverAddress.getMobile()) : 0L
														,null != nowReceiverAddress ? Long.parseLong(nowReceiverAddress.getMobile()) : 0L
														,tuan,groupnum,activenum
														,groupId,tuanMsg);
			ppDealBiz.takeCheapMakeOrder(getWid(),getSk(),getSourceMk(),takeCheapOrderInfo,openid,scene);
			GetTakeCheapDealDetailResponse res = takeCheapOrderInfo.getDealDetail();
			dataStr = obj2Json(res);
			Log.run.debug("ApiDealAction.takeCheapSubmit()....dataStr:" + dataStr);
			
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_FIX_MAKEORDER_SUCCESS, 1);
		} catch (BusinessException e) {
			setNeedAlarm(false);
			if (e.getErrCode() == BusinessErrConstants.ERR_APP_BIN_OVER_LIMIT)  {
				result.setErrInfo(e.getErrCode(), "商品数量超出最大范围");
			} else if(e.getErrCode()==BusinessErrConstants.ERR_APP_BID_RESULT_TIMEOUT){
				result.setErrInfo(e.getErrCode(), "此商品已下架，您不能购买");
			}else {
				result.setErrInfo(e.getErrCode(), e.getErrMsg());
			}
			//result.setErrInfo(e.getErrCode(), e.getErrMsg());

			if (BusinessErrConstants.ERROR_CODE.contains(e.getErrCode())) {
				ItilReporter.attrApiMul(ItilConstants.ITIL_APP_FIX_MAKEORDER_BUSINESS_FAILED, 1);
			} else
				ItilReporter.attrApiMul(ItilConstants.ITIL_APP_FIX_MAKEORDER_FAILED, 1);
			Log.run.warn(e.getMessage(), e);
		} catch (Exception e) {
			Log.run.warn(e.getMessage(), e);
			setNeedAlarm(true);
			result.setErrInfo(BusinessErrorType.UNKNOWN_EXP.getErrCode(), BusinessErrorType.UNKNOWN_EXP.getErrMsg());

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_FIX_MAKEORDER_FAILED, 1);
		}
		return result.getErrCode() == ErrConstant.SUCCESS ? SUCCESS : RST_API_FAILURE;
	}
	public String getIt() {
		return it;
	}

	public void setIt(String it) {
		this.it = it;
	}

	public int getIc() {
		return ic;
	}

	public void setIc(int ic) {
		this.ic = ic;
	}

	public int getPt() {
		return pt;
	}

	public void setPt(int pt) {
		this.pt = pt;
	}

	public int getAdid() {
		return adid;
	}

	public void setAdid(int adid) {
		this.adid = adid;
	}

	public int getPkgid() {
		return pkgid;
	}

	public void setPkgid(int pkgid) {
		this.pkgid = pkgid;
	}

	public String getSa() {
		return sa;
	}

	public void setSa(String sa) {
		this.sa = sa;
	}

	public String getBn() {
		return bn;
	}

	public void setBn(String bn) {
		this.bn = bn;
	}

	public String getRv() {
		return rv;
	}

	public void setRv(String rv) {
		this.rv = rv;
	}

	public String getEt() {
		return et;
	}

	public void setEt(String et) {
		this.et = et;
	}

	public String getPrt() {
		return prt;
	}

	public void setPrt(String prt) {
		this.prt = prt;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public DealBiz getPpDealBiz() {
		return ppDealBiz;
	}

	public void setPpDealBiz(DealBiz ppDealBiz) {
		this.ppDealBiz = ppDealBiz;
	}

	public GetDealDetailResponse getOrderResp() {
		return orderResp;
	}

	public void setOrderResp(GetDealDetailResponse orderResp) {
		this.orderResp = orderResp;
	}

	public String getDataStr() {
		return dataStr;
	}

	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}

	public CmdyBiz getCmdyBiz() {
		return cmdyBiz;
	}

	public void setCmdyBiz(CmdyBiz cmdyBiz) {
		this.cmdyBiz = cmdyBiz;
	}

	public ConfirmOrderVo getConfirmVo() {
		return confirmVo;
	}

	public void setConfirmVo(ConfirmOrderVo confirmVo) {
		this.confirmVo = confirmVo;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}

	public int getPrId() {
		return prId;
	}

	public void setPrId(int prId) {
		this.prId = prId;
	}

	public int getCoupId() {
		return coupId;
	}

	public void setCoupId(int coupId) {
		this.coupId = coupId;
	}

	public long getCashId() {
		return cashId;
	}

	public void setCashId(long cashId) {
		this.cashId = cashId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getInternalSource() {
		return internalSource;
	}

	public void setInternalSource(String internalSource) {
		this.internalSource = internalSource;
	}

	public String getExternalSource() {
		return externalSource;
	}

	public void setExternalSource(String externalSource) {
		this.externalSource = externalSource;
	}

	public TakeCheapOrderInfo getTakeCheapOrderInfo() {
		return takeCheapOrderInfo;
	}

	public void setTakeCheapOrderInfo(TakeCheapOrderInfo takeCheapOrderInfo) {
		this.takeCheapOrderInfo = takeCheapOrderInfo;
	}

	public String getOrderdata() {
		return orderdata;
	}

	public void setOrderdata(String orderdata) {
		this.orderdata = orderdata;
	}

	public String getAddressStr() {
		return addressStr;
	}

	public void setAddressStr(String addressStr) {
		this.addressStr = addressStr;
	}

	public String getTuanMsg() {
		return tuanMsg;
	}

	public void setTuanMsg(String tuanMsg) {
		this.tuanMsg = tuanMsg;
	}

	public int getTuan() {
		return tuan;
	}

	public void setTuan(int tuan) {
		this.tuan = tuan;
	}

	public int getGroupnum() {
		return groupnum;
	}

	public void setGroupnum(int groupnum) {
		this.groupnum = groupnum;
	}

	public int getActivenum() {
		return activenum;
	}

	public void setActivenum(int activenum) {
		this.activenum = activenum;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public int getPaytype() {
		return paytype;
	}

	public void setPaytype(int paytype) {
		this.paytype = paytype;
	}

	public int getPaychannel() {
		return paychannel;
	}

	public void setPaychannel(int paychannel) {
		this.paychannel = paychannel;
	}

	public long getAddressid() {
		return addressid;
	}

	public void setAddressid(long addressid) {
		this.addressid = addressid;
	}

	public int getAddrType() {
		return addrType;
	}

	public void setAddrType(int addrType) {
		this.addrType = addrType;
	}

	public int getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getOtherpay() {
		return otherpay;
	}

	public void setOtherpay(int otherpay) {
		this.otherpay = otherpay;
	}

	public long getRegionid() {
		return regionid;
	}

	public void setRegionid(long regionid) {
		this.regionid = regionid;
	}

	public String getRecvaddr() {
		return recvaddr;
	}

	public void setRecvaddr(String recvaddr) {
		this.recvaddr = recvaddr;
	}

	public String getRecvname() {
		return recvname;
	}

	public void setRecvname(String recvname) {
		this.recvname = recvname;
	}

	public String getWxpay_mobile() {
		return wxpay_mobile;
	}

	public void setWxpay_mobile(String wxpay_mobile) {
		this.wxpay_mobile = wxpay_mobile;
	}

	public String getMoblie() {
		return moblie;
	}

	public void setMoblie(String moblie) {
		this.moblie = moblie;
	}

	public long getShopId() {
		return shopId;
	}

	public void setShopId(long shopId) {
		this.shopId = shopId;
	}

	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}


	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public int getIsDav() {
		return isDav;
	}

	public void setIsDav(int isDav) {
		this.isDav = isDav;
	}

	public String getPromoteKey() {
		return promoteKey;
	}

	public void setPromoteKey(String promoteKey) {
		this.promoteKey = promoteKey;
	}

	public RecvAddrBiz getRecvAddrBiz() {
		return recvAddrBiz;
	}

	public void setRecvAddrBiz(RecvAddrBiz recvAddrBiz) {
		this.recvAddrBiz = recvAddrBiz;
	}

	public long getRedBagScore() {
		return redBagScore;
	}

	public void setRedBagScore(long redBagScore) {
		this.redBagScore = redBagScore;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public CacheClient getCacheClient() {
		return cacheClient;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}
	
}
