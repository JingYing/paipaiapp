package com.qq.qqbuy.deal.util;

import static com.qq.qqbuy.deal.util.DealConstant.STATE_CANCEL;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_END;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_FULL_REFUND_CANCEL;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_PAY_CONFIRM_RECV;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_PAY_OK;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_PAY_REFUND;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_REFUND_CANCEL;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_SHIPPING_OK;
import static com.qq.qqbuy.deal.util.DealConstant.STATE_SHIPPING_PREPARE;

import java.util.Map;
import java.util.Map.Entry;

import com.qq.qqbuy.common.Tuple2;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CRefundInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;

/**
 * 子单可退款金额计算器. 根据C++源码重写
 * CDealInfo中必须包含extInfo, 需要使用InfoType才能查找出extInfo
 * @author JingYing
 * @date 2015年3月4日
 */
public class RefundCalculator{
	
	private static final long 
		DEAL_PROP_HAS_WG_TICKET = 0x04000000,     	//!< 使用了网购券 
		DEAL_PROP_INCLUDE_SHIPFEE  = 0x00000008;    //!< 子单打款包含了邮费	
	private static final String 
		EXIT_DEAL_WG_TICKET_FEE = "814",          	//大单使用的网购现金券金额
		EXIT_DEAL_WG_TICKET_USE_LIMIT = "817";      //大单使用的网购现金券门槛
	
	/**
	 * 计算子订单的最小退款金额
	 * @param oDeal
	 * @param oTrade
	 * @return
	 */
	public long calcMinRefundToBuyer(CDealInfo oDeal, CTradoInfo oTrade)	{
		long dwRefundToBuyerMin = 1;//至少要退1分钱
		if (isNeedRefundTicket(oDeal))		{
			long dwWGTicketFee = getWgTicketFee(oDeal);
			dwRefundToBuyerMin = oDeal.getDealPayFeeTicket() + dwWGTicketFee;
	        if (oDeal.getDealPayFeeTicket() == 0)
	            dwRefundToBuyerMin += 1;//如果财付券为0，网购代金券会被减掉，所以至少要1分钱退款
		} else if(calcMaxRefundToBuyer(oDeal, oTrade) == 0)	{
		    dwRefundToBuyerMin = 0;
		}
		return dwRefundToBuyerMin;
	}
	
	
	private boolean isNeedRefundTicket(CDealInfo oDeal)	{
		if (oDeal.getDealPayFeeTicket() == 0 && (oDeal.getPropertymask() & DEAL_PROP_HAS_WG_TICKET)==0) //没有使用财富券和网购券
		    return false;
		boolean bIsNeedRefundTicket = true;
		for(CTradoInfo t : oDeal.getTradeList()){
			if(t.getTradeState() == STATE_PAY_REFUND || (t.getTradeState() == STATE_END && t.getBuyerFee() > 0))	{//已经退款结束
				bIsNeedRefundTicket = false;
				break;
			}
		}
		return bIsNeedRefundTicket;
	}
	
	private long getWgTicketFee(CDealInfo oDeal)	{
	    if((oDeal.getPropertymask() & DEAL_PROP_HAS_WG_TICKET)==0){//有使用网购券
	    	for(CTradoInfo t : oDeal.getTradeList())	{
	    		if (t.getExtInfo().length() > 0){
	    			Map<String,String[]> map = Util.deserializeQueryStr(t.getExtInfo());
	    			for(Entry<String,String[]> e : map.entrySet())	{
	    				if(EXIT_DEAL_WG_TICKET_FEE.equals(e.getKey()))	{
	    					return Long.parseLong(e.getValue()[0]);
	    				}
	    			}
	    		}
	    	}
	    }
	    return 0;
	}
	
	
	/**
	 * 计算子订单的最大退款金额
	 * @param oDeal
	 * @param oTrade
	 * @return
	 */
	public long calcMaxRefundToBuyer(CDealInfo oDeal, CTradoInfo oTrade)	{
		long dwRefundToBuyerMax = getItemFeeAfterAdjust(oDeal, oTrade);	
		//判断是否为最后一笔申请退款的子订单，如果是，加上邮费
		if (isHavePayShipping(oDeal, oTrade))	{
			dwRefundToBuyerMax += oDeal.getDealPayFeeShipping();
		}
		return dwRefundToBuyerMax;
	}
	
	private long getItemFeeAfterAdjust(CDealInfo oDeal, CTradoInfo oTrade)	{
		long[] couponFees = getCouponFee(oDeal, oTrade);
		long iCouponFee = couponFees[0];
		long iRealCouponFeeTotal = couponFees[1];

        if ( iRealCouponFeeTotal != 0)	{
        	Tuple2<Boolean, Long> tuple2 = CheckAdjust(oDeal);
    		if (tuple2.getA() && oTrade.getTradeId() == getMaxFeeTradeId(oDeal))	{
    			iCouponFee += tuple2.getB();
    		}
        }
		return GetItemFee(oTrade) + iCouponFee;
	}
	
	private boolean isHavePayShipping(CDealInfo oDeal, CTradoInfo oTrade)	{
		//仅处理已付款订单
		if (oTrade.getTradeState() != STATE_SHIPPING_OK &&
				oTrade.getTradeState() != STATE_PAY_OK &&
						oTrade.getTradeState() != STATE_SHIPPING_PREPARE)		{
			return false;
		}

		//以下处理发货后状态STATE_SHIPPING_OK
		//遍历所有子单的退款单，是否有退款单已包含邮费。
		for(CTradoInfo t : oDeal.getTradeList())	{
			if(t.getTradeRefundId() > 0)	{
				if((t.getTradePropertymask() & DEAL_PROP_INCLUDE_SHIPFEE)==0){//有退款单已包含邮费
					if(t.getTradeId() == oTrade.getTradeId())		{//含邮费的是当前退款单，则返回true
						return true;
					} else {//非当前退款单包含邮费，则邮费已计算，返回false
						return false;
					}
				}
			}
		}
		
		//以下处理没有退款单打上邮费标识
		//如果有子单已经确认收货，则邮费不再计入退款单
		for(CTradoInfo t : oDeal.getTradeList())	{
			if(t.getTradeState() == STATE_PAY_CONFIRM_RECV ||
					(t.getTradeState() == STATE_END && t.getBuyerFee() == 0 && t.getSellerFee() != 0))//交易成功买家金额为0卖家金额不为0，认为是确认收货
				return false;
		}
		
		//以下处理没有子单确认收货
		//只要有其它子单没申请退款，则不包含邮费
		for(CTradoInfo t : oDeal.getTradeList())	{
			if(t.getTradeState() == STATE_CANCEL || t.getTradeState() == STATE_END)	{
				continue;
			}
			
			if(t.getTradeId() != oTrade.getTradeId())	{//其它子单
				if(t.getTradeRefundId() > 0)	{//要么有退款单
					CRefundInfo refund = t.getRefundList().get(0);
					if(refund.getRefundState() == STATE_REFUND_CANCEL ||
						refund.getRefundState() == STATE_FULL_REFUND_CANCEL ||
						refund.getRefundState() == 0){//退款单取消了，则有其它子单没申请退款
						return false;
					}
				} else {//要么没申请过退款
					return false;
				}
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(1L&2L);
	}
	
	private long[] getCouponFee(CDealInfo oDeal, CTradoInfo oTrade)	{	//TODO 改写过
		long iCouponFee = 0;
	    long iRealCouponFee = oDeal.getCouponFee();

	    //计算商品上的金额
		long dwItemFee = GetItemFee(oTrade);	
		//计算使用促销之前的订单总金额
		long dwTotalFee = GetDealItemTotalFee(oDeal);
		if( 0 == dwTotalFee )
			return new long[]{0,iRealCouponFee};
        //CouponFee为店铺优惠与网购代金券之和, 如果有网购券, 需要排除掉
        if((oDeal.getPropertymask() & DEAL_PROP_HAS_WG_TICKET)==0)	        {
            long dwWgTicketFee = getWgTicketFee(oDeal);
            iRealCouponFee = iRealCouponFee + dwWgTicketFee;//因为网购券在CouponFee里是负数，所以这里是加上
        }

        //如果店铺优惠是0,直接返回0,无需再分摊
        if (iRealCouponFee == 0)
        	return new long[]{0,iRealCouponFee};

		//计算分摊到每个子订单的金额
		 iCouponFee = Math.round((double)iRealCouponFee * (double)dwItemFee/(double)dwTotalFee);
		 return new long[]{iCouponFee,iRealCouponFee};
	}
	
	//获取订单中在应用店铺活动前的订单金额(不包含运费)
	private long GetDealItemTotalFee(CDealInfo oDeal)	{
	    long dwTotalItemFee=0;
	  //计算买家申请退款的费用
	    for(CTradoInfo t : oDeal.getTradeList())	{
	    	dwTotalItemFee += GetItemFee(t);
	    }
	    return dwTotalItemFee;
	}

	private Tuple2<Boolean,Long> CheckAdjust(CDealInfo oDeal)	{	//TODO 改写过
		//检查是否有误差
		boolean bAdjust = false;
		long iCouponFee = 0;
		long iRealCouponFeeTotal = 0;
		long iAdjust = 0;
		
		for(CTradoInfo t : oDeal.getTradeList())	{
			long[] couponFees = getCouponFee(oDeal, t);
			iCouponFee += couponFees[0];
			iRealCouponFeeTotal = couponFees[1];
		}
		if (iCouponFee != iRealCouponFeeTotal) {
			bAdjust = true;
			iAdjust = iRealCouponFeeTotal - iCouponFee;
		}
		return new Tuple2<Boolean, Long>(bAdjust, iAdjust);
	}
	
	private long GetItemFee(CTradoInfo oTrade)	{
	     if( STATE_CANCEL != oTrade.getTradeState() )	     {
	    	long nItemFee = (long)(oTrade.getDealItemCount()*oTrade.getItemPrice())+oTrade.getItemAdjustPrice()-oTrade.getDiscountFee();
	    	return nItemFee < 0 ? 0 : nItemFee;
	     } else	{
	    	 return 0;
	     }
	}
	
	private long getMaxFeeTradeId(CDealInfo oDeal)	{
		long ddwTradeId = 0, dwItemFee = 0;
		for(CTradoInfo t : oDeal.getTradeList())	{
			long fee = GetItemFee(t);
			if(fee > dwItemFee)	{
				dwItemFee = fee;
				ddwTradeId = t.getTradeId();
			}
		}
		return ddwTradeId;
	}


}
