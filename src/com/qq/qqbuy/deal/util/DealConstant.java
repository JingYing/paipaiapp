package com.qq.qqbuy.deal.util;

/* @FileName: DealConstant.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2012-12-11 下午03:03:17    
 */
public interface DealConstant {
	
    public final static int ONLY_REDPACKET = 1;
    public final static int REDPACKET_AND_COUPON = 2;
    
	public final static int TYPE_COD_FEE = 0;
	public final static int TYPE_MMB_FEE = 0;
	public final static int TYPE_DISCOUNT_FEE = 0;
	
	// 运送方式：免运费
	public static final int SHIPCALC_TYPE_FREE = 0;
	// 运送方式：快递
	public static final int SHIPCALC_TYPE_EXPRESS = 1;
	// 运送方式：平邮
	public static final int SHIPCALC_TYPE_NORMAL = 2;
	// 运送方式：EMS
	public static final int SHIPCALC_TYPE_EMS = 3;
	// 运送方式：货到付款宅急送
	public static final int SHIPCALC_TYPE_COD_ZJS = 4;
	// 运送方式：货到付款顺丰
	public static final int SHIPCALC_TYPE_COD_SF = 5;
	// 运送方式：货到付款优速
	public static final int SHIPCALC_TYPE_COD_YS = 6;
	// 运送方式：货到付款圆通
	public static final int SHIPCALC_TYPE_COD_YT = 7;
	
	// 普通运费模版
	public static final int SHIPPING_TMP_TYPE_NORMAL = 0;
	// COD运费模版且买家承担COD服务费
	public static final int SHIPPING_TMP_TYPE_COD_BUYER = 1;
	// COD运费模版且卖家承担COD服务费
	public static final int SHIPPING_TMP_TYPE_COD_SELLER = 2;
	
    // 邮递类型: 0 = 无需物流, 1 = 平邮, 2 = 快递, 3 = ems (用于拍拍一口价下单接口)
	public static final int MAIL_TYPE_NONE = 0;
	public static final int MAIL_TYPE_NORMAL = 1;
	public static final int MAIL_TYPE_EXPRESS = 2;
	public static final int MAIL_TYPE_EMS = 3;
	public static final int MAIL_TYPE_COD_ZJS = 11; //货到付款方式，宅急送
	public static final int MAIL_TYPE_COD_SF = 12; //货到付款方式，顺丰
	public static final int MAIL_TYPE_COD_YS = 13; // 货到付款方式，优速
	public static final int MAIL_TYPE_COD_YT = 14; // 货到付款方式，圆通
	public static final int MAIL_TYPE_COD_JD = 16; // 货到付款方式，京东快递
	// 支付类型： 0 = 财付通, 1 = 支付中介
	public final static int PAYTYPE_OLP = 0;
	public final static int PAYTYPE_COD = 1;
	
	// 订单取消IDL接口调用使用场景
	public final static int IDL_CANCELDEAL_SCENE_USER = 1;   //用户取消 
    public final static int IDL_CANCELDEAL_SCENE_SYS = 3;    //系统取消
    
    // 订单一口价下单接口调用OrderFrom字段取值
    public final static int IDL_ORDERDEAL_FROM_WG = 3;   //微购来源，免登录态，打微购订单标
    
    // 确认订单接口的OpType的取值： 0:买家 1：卖家 2：系统
    public final static int IDL_CONFIRMDEAL_OPTYPE_BUYER = 0;  //买家
    public final static int IDL_CONFIRMDEAL_OPTYPE_SELLER = 1;  //卖家
    public final static int IDL_CONFIRMDEAL_OPTYPE_SYS = 2;  //系统
    
    //货到付款买家签收操作类型
    public final static int IDL_SIGN_TYPE_ACCEPT=0;  //签收
    public final static int IDL_SIGN_TYPE_REFUSE=1;	 //拒签	
    
    
    
    // 查询订单列表DealType的取值：0-所有的 ，1-只拉拍拍，4-只拉店铺街，5-只拉微购
    public static final int IDL_QUERYDEALLIST_DEALTYPE_ALL = 0;
    public static final int IDL_QUERYDEALLIST_DEALTYPE_PAIPAIONLY = 1;
    public static final int IDL_QUERYDEALLIST_DEALTYPE_DIANPUJIEONLY = 4;
    public static final int IDL_QUERYDEALLIST_DEALTYPE_WEIGOUONLY = 5;
    
    /*******买家付款前关闭原因***********/
    public static final int WG_DEAL_BUYER_CLOSE_0 = 0; //0. 无法联系上卖家
    public static final int WG_DEAL_BUYER_CLOSE_1 = 1; //1.卖家无诚意完成交易
    public static final int WG_DEAL_BUYER_CLOSE_2 = 2; //2.卖家缺货无法交易
    public static final int WG_DEAL_BUYER_CLOSE_3 = 3; //3.因为网银无法完成付款
    public static final int WG_DEAL_BUYER_CLOSE_4 = 4; //4.因为误拍我不想要了
    public static final int WG_DEAL_BUYER_CLOSE_5 = 5; //5.其他原因

    /******卖家付款前关闭原因***********/
    public static final int WG_DEAL_SELLER_CLOSE_0 = 6;  //6.商品缺货无法完成交易
    public static final int WG_DEAL_SELLER_CLOSE_1 = 7;  //7.买家地址无法送达
    public static final int WG_DEAL_SELLER_CLOSE_2 = 8;  //8.买家下单信息为假信息
    public static final int WG_DEAL_SELLER_CLOSE_3 = 9;  //9.买家误拍或是重拍了
    public static final int WG_DEAL_SELLER_CLOSE_4 = 10; //10.无法联系上买家
    public static final int WG_DEAL_SELLER_CLOSE_5 = 11; //11.买家未按时付款
    public static final int WG_DEAL_SELLER_CLOSE_6 = 12; //12.其他原因

    /*****卖家付款后关闭原因***********/
    public static final int WG_DEAL_SELLER_CLOSE_PAY_0 = 13; //13.商品缺货无法完成交易
    public static final int WG_DEAL_SELLER_CLOSE_PAY_1 = 14; //14.买家地址无法送达
    public static final int WG_DEAL_SELLER_CLOSE_PAY_2 = 15; //15.买家下单信息为假信息
    public static final int WG_DEAL_SELLER_CLOSE_PAY_4 = 16; //16.无法联系上买家
    public static final int WG_DEAL_SELLER_CLOSE_PAY_6 = 17; //17.其他原因
    
    /*****取消订单失败原因************/
    public static final int WG_DEAL_CANCEL_FAIL_0 = 1; //1.包大小错误
    public static final int WG_DEAL_CANCEL_FAIL_1 = 2; //2.命令号不支持/订单事件错误 
    public static final int WG_DEAL_CANCEL_FAIL_2 = 3; //3.订单状态错误 
    public static final int WG_DEAL_CANCEL_FAIL_3 = 4; //4.物品状态检查失败 
    public static final int WG_DEAL_CANCEL_FAIL_4 = 5; //5.出价失败
    public static final int WG_DEAL_CANCEL_FAIL_5 = 6; //6.系统繁忙
    public static final int WG_DEAL_CANCEL_FAIL_6 = 7; //7.拍卖商品还无人出价 
    public static final int WG_DEAL_CANCEL_FAIL_7 = 8; //8. 
    public static final int WG_DEAL_CANCEL_FAIL_8 = 9; //9.非法数据包
    public static final int WG_DEAL_CANCEL_FAIL_9 = 10; //10.上架商品时因为是手机认证,只能上架十(架上已经有十件),上架失败 
    public static final int WG_DEAL_CANCEL_FAIL_10 = 11; //11.已经有人出价,商品下架时 
    public static final int WG_DEAL_CANCEL_FAIL_11 = 12; //12.修改的商品信息已经有人参拍,新生成了商品ID 
    public static final int WG_DEAL_CANCEL_FAIL_12 = 13; //13.用户未登录 
    public static final int WG_DEAL_CANCEL_FAIL_13 = 14; //14.参数错误
    public static final int WG_DEAL_CANCEL_FAIL_14 = 66; //66.不存在的商品类目
    public static final int WG_DEAL_CANCEL_FAIL_15 = 86; //86.用户itemid + ip 刷新出价太频繁
    public static final int WG_DEAL_CANCEL_FAIL_16 = 177; //177.当前状态操作非法 
    public static final int WG_DEAL_CANCEL_FAIL_17 = 192 ; //192.商品数量为空 
    public static final int WG_DEAL_CANCEL_FAIL_18 = 193; //193.出价过低,失败 
    public static final int WG_DEAL_CANCEL_FAIL_19 = 194; //194.合理出价,但出价过低 
    public static final int WG_DEAL_CANCEL_FAIL_20 = 195; //195.买家卖家相同
    public static final int WG_DEAL_CANCEL_FAIL_21 = 196; //196.拍卖已经结束
    public static final int WG_DEAL_CANCEL_FAIL_22 = 197; //197.用户信用度不够 
    public static final int WG_DEAL_CANCEL_FAIL_23 = 198; //198.商品数量不够 
    public static final int WG_DEAL_CANCEL_FAIL_24 = 199; //199.领先者出价过低 
    public static final int WG_DEAL_CANCEL_FAIL_25 = 200; //200.领先者出价合理,只是提高了心理价 
    public static final int WG_DEAL_CANCEL_FAIL_26 = 213; //213.评价已存在 
    public static final int WG_DEAL_CANCEL_FAIL_27 = 228; //228.操作过程出错
    public static final int WG_DEAL_CANCEL_FAIL_28 = 255; //255.订单不存在 
    public static final int WG_DEAL_CANCEL_FAIL_29 = 432; //432.操作过于频繁 
    public static final int WG_DEAL_CANCEL_FAIL_30 = 484; //484.设置的橱窗数过多 
    public static final int WG_DEAL_CANCEL_FAIL_31 = 488; //488.申请的操作已经完成 
    public static final int WG_DEAL_CANCEL_FAIL_32 = 681; //681.属性出错，需编辑后再上架 
    public static final int WG_DEAL_CANCEL_FAIL_33 = 685; //685.发布商品次数过多 
    public static final int WG_DEAL_CANCEL_FAIL_34 = 709; //709.不能把女装商品修改为非女装类目 
    public static final int WG_DEAL_CANCEL_FAIL_35 = 717; //717.您没有本类目的发布权限 
    public static final int WG_DEAL_CANCEL_FAIL_36 = 731; //731.该商品必须设置一个折扣价,才能继续操作 
    public static final int WG_DEAL_CANCEL_FAIL_37 = 752; //752.spid参数错误 
    public static final int WG_DEAL_CANCEL_FAIL_38 = 763; //763.商品的库存价格不能修改成比市场价大(商品业务)
    public static final int WG_DEAL_CANCEL_FAIL_39 = 8322; //8322.没有记录(删除收货地址操作)
    
    /*******************拍拍订单状态************************/
    public static final int STATE_SUCCESS_ALL = 401; //401.交易成功(STATE_END且无退款)（包含货到付款订单）
    //订单状态-在线支付+货到付款
    public static final int STATE_COMBO_WAIT_SHIP = 802;//待发货
    public static final int STATE_COMBO_WAIT_RECV = 803;//待收货
    
    public static final int
		  //大单和子单状态
		    STATE_START = 0,            //交易流程开始. 所有状态
		    STATE_WAIT_PAY = 1,         //等待买家付款. 在线支付
		    STATE_PAY_OK = 2,           //买家已付款，等待发货. 在线支付
		    STATE_SHIPPING_OK = 3,      //卖家已发货. 在线支付. 待收货
		    STATE_END = 4,              //交易成功
    		STATE_CANCEL = 5,           //交易关闭
			STATE_HALT = 6,             //交易暂停
			STATE_PAY_CONFIRM_RECV = 7, //款项处理中(确认收货)
			STATE_PAY_REFUND = 8,       //款项处理中(有退款)
			STATE_SHIPPING_PREPARE = 9, //配货中
			
			//退款单退款流程状态
			STATE_REFUND_START = 20,                    //退款开始
			STATE_REFUND_WAIT_SHIP = 22,                //等待买家发送退货
			STATE_REFUND_WAIT_MODIFY_AFTER_SHIP = 23,   //等待买家修改退货申请(发送退货后)
			STATE_REFUND_WAIT_MODIFY= 24,               //等待买家修改退款申请
			STATE_REFUND_WAIT_CONFIRM_RECV = 25,        //等待卖家确认收货
			STATE_REFUND_WAIT_AGREE = 26,               //等待卖家同意退款
			STATE_REFUND_SUCCESS = 27,                  //退款协议达成
			STATE_REFUND_CANCEL = 29,                   //退款取消
			//退款单全额退款流程状态
			STATE_FULL_REFUND_WAIT_AGREE = 31,          //等待卖家同意全额退款
			STATE_FULL_REFUND_CANCEL = 32,              //全额退款取消
			STATE_FULL_REFUND_SUCCESS = 33,             //全额退款协议达成
    	    	    	     
			//大单货到付款状态
			//STATE_COD_START = 40,                       // 货到付款开始
			STATE_COD_WAIT_SHIP = 41,                   // 货到付款等待发货
			STATE_COD_SHIP_OK = 42,                     // 货到付款已发货
			STATE_COD_SIGN = 43,                        // 货到付款已签收
			STATE_COD_REFUSE = 44,                      // 货到付款拒签
			STATE_COD_SUCESS = 45,                      // 货到付款成功(已打款)
			STATE_COD_CANCEL = 46,                      // 货到付款取消(关闭or 拒签后关闭)
    	    	    	    	     
			//大单分期付款状态
			STATE_FQFK_START = 50,  // 等待商家确认分期付款
			STATE_FQFK_CANCEL = 51, // 未通过商家审核分期付款
			    	    	    	    	    	     
			//微购订单状态
			STATE_WG_WAIT_PAY = 60, //微购订单等待买家付款
			STATE_WG_PAY_OK = 61, //微购订单买家已付款，等待卖家发货
			STATE_WG_SHIPPING_OK = 62, //微购订单卖家已发货，等待买家确认收货
			STATE_WG_END = 63, //微购订单交易成功
			STATE_WG_CANCLE = 64, //微购订单交易关闭
			STATE_WG_PAY_REFUND = 65; //微购订单退款中
			//微购COD订单状态G_COD_WAIT_SHIP = 70, //微购COD订单
    

    
    /********************拍拍订单评价状态***********************************/
    public static final int RATE_STATE_NEED_BUYER = 100; //需买家评价
    public static final int RATE_STATE_NEED_SELLER = 101; //需卖家评价
    public static final int RATE_STATE_BUYER_DONE = 102; //买家已评
    public static final int RATE_STATE_SELLER_DONE = 103; //卖家已评
    
    /******************************订单评价角色********************************/
    public static final int EVAL_ROLE_BUYER = 0;  //买家
    public static final int EVAL_ROLE_SELLER = 1; //卖家
    
    /******************************订单评价等级********************************/
    public static final int EVAL_LEVEL_GOOD = 3;   //好评
    public static final int EVAL_LEVEL_MIDDLE = 2; //中评
    public static final int EVAL_LEVEL_BAD = 1;    //差评
    
    /*****************************订单确认收货*********************************/
    public static final String DRAW_ID_NAME = "draw_id_0"; //draw_id参数名称
    public static final String TOKEN_NAME = "token_0"; //token参数名称
    
    /*****************************订单支付类型*********************************/
    public static final int DEAL_PAY_TYPE_CODE_TENPAY = 1; //订单支付类型：在线支付
    public static final int DEAL_PAY_TYPE_CODE_OFF_LINE = 2; //订单支付类型：货到付款
    
    
	public static int COD_SHIP_ZJS = 1; // 货到付款宅急送  最高代收金额10000元
	public static int COD_SHIP_SF = 2; // 货到付款顺丰          最高代收金额10000元
	public static int COD_SHIP_YS = 3; // 货到付款优速
	public static int COD_SHIP_YT = 4; // 货到付款圆通

	public static int COD_BUYER_FEE = 0; // 0表示买家承担COD服务费
	public static int COD_SELLER_FEE = 1; // 1标识卖家承担COD服务费
	/****************************下单场景******************/
	public static int DEAL_SCENE_INITIALIZE = 1;
	public static int DEAL_SCENE_CHANGERECVADDR = 2;
	public static int DEAL_SCENE_CHANGEPROMOTIONS = 3;
	public static int DEAL_SCENE_CHANGEPAYTYPE = 4;
	public static int DEAL_SCENE_CHANGENUMBER = 5;
	/**
	 * BuyerRefundReq.EventParamBasePo.eventId的取值范围
	 * 全额退款(卖家发货前用) , 部分退款(卖家发货后用)
	 */
	public static final int 
		EVENT_APPLY_FULL_REFUND = 13  , // 买家申请全额退款
		EVENT_MODIFY_FULL_REFUND = 14  , // 买家修改全额退款
		EVENT_AGREE_FULL_REFUND = 15  , // 卖家同意全额退款
		EVENT_APPLY_REFUND = 17  , // 申请退款
		EVENT_MODIFY_REFUND = 18  , // 修改退款申请
		EVENT_REFUSE_REFUND = 19  , // 拒绝退款
		EVENT_MARK_SHIP_REFUND = 20  , // 标记发送退货
		EVENT_CONFIRM_RECV = 22  , // 确认收货
		EVENT_AGREE_REFUND = 23,	 // 同意退款
		EVENT_CANCEL_FULL_REFUND = 39  , //取消全额退款
		EVENT_CANCEL_REFUND = 40  ; // 取消退款申请
}
