package com.qq.qqbuy.deal.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.C2CDefineConstant;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.dealopr.po.DealRateState;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemStockBo;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CTradoInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;


/**
 * @author winsonwu
 * @Created 2012-12-28 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class DealUtil implements DealConstant, C2CDefineConstant {

	public static final String PP_IMG_URL_PREF = "http://image.paipai.com/";
	
	public final static String IMG_FMT_80X80 = "80x80";

	public final static String IMG_FMT_120X120 = "120x120";

	public static ItemStockBo findItemStock(ItemBO item, String itemAttr) {
		if (item != null && itemAttr != null) {
			List<ItemStockBo> itemStocks = item.getItemStockBo();
			if (itemStocks != null && itemStocks.size() > 0) {
				for (ItemStockBo itemStock : itemStocks) {
					if (itemAttr.equals(itemStock.getStockAttr())) {
						return itemStock;
					}
				}
			}
		}
		return null;
	}

	public static long getSellerUin4DealCode(String dealCode)
			throws BusinessException {
		long sellerUin = -1;
		if (!StringUtil.isEmpty(dealCode)) {
			String[] strs = dealCode.split("-");
			if (strs != null && strs.length == 3) {
				sellerUin = StringUtil.toLong(strs[0], -1);
			}
		}
		if (sellerUin > 0) {
			return sellerUin;
		}
		throw BusinessException.createInstance(
				ErrConstant.ERRCODE_INVALID_PARAMETER, "dealCode参数格式不正确");
	}

	public static String getSubDealCode4DealCode(String dealCode)
			throws BusinessException {
		String subDealCode = "";
		if (!StringUtil.isEmpty(dealCode)) {
			String[] strs = dealCode.split("-");
			if (strs != null && strs.length == 3) {
				subDealCode = strs[2];
			}
		}
		if (!StringUtil.isEmpty(subDealCode)) {
			return subDealCode;
		}
		throw BusinessException.createInstance(
				ErrConstant.ERRCODE_INVALID_PARAMETER, "dealCode参数格式不正确");
	}

	public static String genDealTradeItemPicLoc(CTradoInfo trade) {
		if (trade != null) {
			return PP_IMG_URL_PREF + trade.getItemLogo();
		}
		return "";
	}

	public static String getFullImgPath(String path) {
		return "http://image.paipai.com/" + path;
	}

	public static String getDisDealPayType(int payType) {
		switch (payType) {
		case PAY_TYPE_PAY_AGENCY:
			return "在线支付";
		case PAY_TYPE_COD:
			return "货到付款";
		default:
			return "未知";
		}
	}

	public static String getDisMailType(CDealInfo deal) {
		if (deal != null) {
			short whopayfee = deal.getWhoPayShippingfee();
			if (whopayfee == TRANSPORT_SELLER_PAY
					|| whopayfee == TRANSPORT_NO_NEED_PAY) {
				return "包邮";
			} else if (whopayfee == TRANSPORT_BUYER_PAY) {
				int mailType = (int) deal.getMailType();
				switch (mailType) {
				case POST_NORMAL:
					return "平邮";
				case POST_EXPRESS:
					return "快递";
				case POST_EMS:
					return "EMS";
				}
				if (mailType >= 10) {
					return "货到付款";
				}
			}
		}
		return "未知";
	}

	public static CDealInfo getFirstDealInfo(GetDealInfoList2Resp resp) {
		if (resp != null && resp.getResult() == 0 && resp.getTotalNum() == 1) {
			Vector<CDealInfo> dealInfos = resp.getODealInfoList();
			if (dealInfos != null && dealInfos.size() > 0) {
				return dealInfos.get(0);
			}
		}
		return null;
	}

	public static ReceiverAddress getDefaultAddr(
			List<ReceiverAddress> addrList, int adid) {
		if (addrList != null && addrList.size() > 0) {
			if (adid > 0) { // 要寻找当前选中的地址
				for (ReceiverAddress addr : addrList) {
					if (adid == addr.getAddressId()) {
						return addr;
					}
				}
			}
			return addrList.get(0);
		}
		return null;
	}

	/**
	 * 根据订单id获取卖家uin
	 * 
	 * @param dealId
	 * @return
	 */
	public static long getSellerUinFromDealId(String dealId) {
		if (StringUtil.isEmpty(dealId)) {
			return 0;
		}
		String[] ds = dealId.split("-");
		long sellerUin = -1;
		try {
			sellerUin = Long.parseLong(ds[0]);
		} catch (NumberFormatException e) {
			Log.run.error(e.getMessage(), e);
		}
		return sellerUin;
	}

	/**
	 * 
	 * @Title: parseDateToString
	 * 
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param time
	 * @param @return
	 * @param @throws Exception 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public static String parseDateToString(long time) {
		try {
			return DateUtils.formatDateToString(new Date(time * 1000L),
					DateUtils.FORMAT_YMD_HMS);
		} catch (Exception e) {
			Log.run.error("DealUtil#parseDateToString fail with exception", e);
			return "";
		}
	}

	public static String getDealRateStateDesc(int rateState) {
		switch (rateState) {
		case 1:
		case 3:
			return "未评价";
		case 2:
		case 4:
			return "已评价";
		default:
			return DealRateState.getEnumByCode(rateState).getShowMeg();
		}
	}

	/**
	 * 
	 * @Title: checkDealRateState
	 * 
	 * @Description: 校验订单评价状态值是否有效
	 * @param @param rateState
	 * @param @return 设定文件
	 * @return boolean 返回类型
	 * @throws
	 */
	public static boolean checkDealRateState(int rateState) {
		boolean flag = false;
		switch (rateState) {
		case DealConstant.RATE_STATE_NEED_BUYER:
		case DealConstant.RATE_STATE_NEED_SELLER:
		case DealConstant.RATE_STATE_BUYER_DONE:
		case DealConstant.RATE_STATE_SELLER_DONE:
			flag = true;
			break;
		default:
			break;
		}
		return flag;
	}

	/**
	 * 
	 * @Title: getPageNum
	 * 
	 * @Description: 根据总数及页面大小生成页数
	 * @param @param totalNum
	 * @param @param pageSize
	 * @param @return 设定文件
	 * @return int 返回类型
	 * @throws
	 */
	public static long getPageNum(long totalNum, int pageSize) {
		long pageNum = 0;
		pageNum = totalNum / pageSize;
		if ((totalNum % pageSize) != 0) {
			++pageNum;
		}
		return pageNum;
	}

	/**
	 * 
	 * @Title: filterDealStateDesc
	 * 
	 * @Description: 过滤订单状态描述的括号内容
	 * @param @param dealStateDesc
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public static String filterDealStateDesc(String dealStateDesc) {
		if (!StringUtil.isEmpty(dealStateDesc)) {
			int pos = dealStateDesc.indexOf("(");
			if (pos > 0) {
				dealStateDesc = dealStateDesc.substring(0, pos);
			}
		}
		return dealStateDesc;
	}

	/**
	 * 根据errCode返回errMsg
	 * 
	 * @param errCode
	 * @return
	 */
	public static String getDealErrMsg(int errCode) {
		switch (errCode) {
		case WG_DEAL_CANCEL_FAIL_0:
			return "包大小错误";
		case WG_DEAL_CANCEL_FAIL_1:
			return "命令号不支持/订单事件错误";
		case WG_DEAL_CANCEL_FAIL_2:
			return "订单状态错误 ";
		case WG_DEAL_CANCEL_FAIL_3:
			return "物品状态检查失败";
		case WG_DEAL_CANCEL_FAIL_4:
			return "出价失败";
		case WG_DEAL_CANCEL_FAIL_5:
			return "系统繁忙";
		case WG_DEAL_CANCEL_FAIL_6:
			return "拍卖商品还无人出价";
		case WG_DEAL_CANCEL_FAIL_8:
			return "非法数据包";
		case WG_DEAL_CANCEL_FAIL_9:
			return "上架商品时因为是手机认证,只能上架十件(架上已经有十件)";
		case WG_DEAL_CANCEL_FAIL_10:
			return "已经有人出价,商品下架时 ";
		case WG_DEAL_CANCEL_FAIL_11:
			return "修改的商品信息已经有人参拍,新生成了商品ID";
		case WG_DEAL_CANCEL_FAIL_12:
			return "用户未登录";
		case WG_DEAL_CANCEL_FAIL_13:
			return "参数错误";
		case WG_DEAL_CANCEL_FAIL_14:
			return "不存在的商品类目";
		case WG_DEAL_CANCEL_FAIL_15:
			return "刷新出价太频繁";
		case WG_DEAL_CANCEL_FAIL_16:
			return "当前状态操作非法 ";
		case WG_DEAL_CANCEL_FAIL_17:
			return "商品数量为空";
		case WG_DEAL_CANCEL_FAIL_18:
			return "出价过低";
		case WG_DEAL_CANCEL_FAIL_19:
			return "合理出价,但出价过低 ";
		case WG_DEAL_CANCEL_FAIL_20:
			return "买家卖家相同";
		case WG_DEAL_CANCEL_FAIL_21:
			return "拍卖已经结束";
		case WG_DEAL_CANCEL_FAIL_22:
			return "用户信用度不够 ";
		case WG_DEAL_CANCEL_FAIL_23:
			return "商品数量不够 ";
		case WG_DEAL_CANCEL_FAIL_24:
			return "领先者出价过低 ";
		case WG_DEAL_CANCEL_FAIL_25:
			return "领先者出价合理,只是提高了心理价 ";
		case WG_DEAL_CANCEL_FAIL_26:
			return "评价已存在 ";
		case WG_DEAL_CANCEL_FAIL_27:
			return "操作过程出错";
		case WG_DEAL_CANCEL_FAIL_28:
			return "订单不存在 ";
		case WG_DEAL_CANCEL_FAIL_29:
			return "操作过于频繁";
		case WG_DEAL_CANCEL_FAIL_30:
			return "设置的橱窗数过多";
		case WG_DEAL_CANCEL_FAIL_31:
			return "申请的操作已经完成";
		case WG_DEAL_CANCEL_FAIL_32:
			return "属性出错，需编辑后再上架";
		case WG_DEAL_CANCEL_FAIL_33:
			return "发布商品次数过多";
		case WG_DEAL_CANCEL_FAIL_34:
			return "不能把女装商品修改为非女装类目";
		default:
			return "";
		}
	}
	
	/**
	 * 
	 * @Title: convert80x80To120x120Img
	 * 
	 * @Description: 80x80尺寸图片url转120x120
	 * @param @param imgUrl80x80
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public static String convert80x80To120x120Img(String imgUrl80x80) {
		if (StringUtil.isBlank(imgUrl80x80)) {
			return "";
		} else if (imgUrl80x80.contains(IMG_FMT_80X80)) {
			String imgUrl120x120 = imgUrl80x80.replace(IMG_FMT_80X80,
					IMG_FMT_120X120);
			return imgUrl120x120;
		} else {
			return imgUrl80x80;
		}
	}
	
	public static String getCloseReasonDesc(int closeReason) {
		switch (closeReason) {
		case 0:
			return "无法联系上买家";
		case 1:
			return "买家误拍或重拍了";
		case 2:
			return "买家无诚意完成交易 ";
		case 3:
			return "已通过银行线下汇款";
		case 4:
			return "已通过同城见面交易";
		case 5:
			return "已通过货到付款交易";
		case 6:
			return "已通过网上银行直接汇款";
		case 7:
			return "已经缺货无法交易";
		case 8:
			return "无法联系上卖家";
		case 9:
			return "卖家无诚意完成交易 ";
		case 10:
			return "卖家缺货无法交易";
		case 11:
			return "因为网银无法完成付款";
		case 12:
			return "因为误拍我不想要了";
		case 13:
			return "不存在的商品类目";
		default:
			return "";
		}
	}
	
	/**
	 * 是否历史订单
	 * @param dealId
	 * @return
	 */
	public static boolean isHistory(String dealId)	{
		String[] arr = dealId.split("-");
		try {
			Calendar dealDate = Calendar.getInstance();
			dealDate.setTime(new SimpleDateFormat("yyyyMMdd").parse(arr[1]));
			dealDate.add(Calendar.MONTH, 3);
			return dealDate.before(Calendar.getInstance());	//TODO 待验证日期边界
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args) {
		System.out.println(isHistory("111-20150229-111"));
		System.out.println(isHistory("111-20150101-111"));
		System.out.println(isHistory("111-20141228-111"));
		System.out.println(isHistory("111-20141128-111"));
		System.out.println(isHistory("111-20141028-111"));
	}
}
