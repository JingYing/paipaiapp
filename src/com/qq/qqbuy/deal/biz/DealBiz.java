package com.qq.qqbuy.deal.biz;

import com.qq.qqbuy.deal.po.MakeEvalListPo;
import com.qq.qqbuy.deal.po.OrderInfo;
import com.qq.qqbuy.deal.po.TakeCheapOrderInfo;

public interface DealBiz {
	
	 public void makeOrder(OrderInfo orderInfo,String mk,String lskey);
	 
	 /**
	  * 买家对订单做评价
	  * @param uin
	  * @param dealId
	  * @param skey
	  * @param mk
	  * @param makeEvalListPo
	  */
	 public void makeEvalToSeller(long uin, String dealId, String skey, String mk, MakeEvalListPo makeEvalListPo);

	/**
	 * 拍便宜下单接口
	 * @param wid
	 * @param sk
	 * @param sourceMk
	 * @param takeCheapOrderInfo
	 * @param openid 微信/京东登陆返回
	 * @param scene 场景ID
	 */
	 public void takeCheapMakeOrder(Long wid, String sk, String sourceMk, TakeCheapOrderInfo takeCheapOrderInfo,String openid,String scene);

}
