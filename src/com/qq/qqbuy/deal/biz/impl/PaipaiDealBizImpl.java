package com.qq.qqbuy.deal.biz.impl;

import com.paipai.lang.MultiMap;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.deal.biz.DealBiz;
import com.qq.qqbuy.deal.po.*;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.dealopr.util.DealDetailConveter;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.IDLResult;
import com.qq.qqbuy.thirdparty.idl.deal.DealClient;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod.ConfirmV2Req;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod.OrderConfirmV2Req;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod.OrderConfirmV2Resp;
import com.qq.qqbuy.thirdparty.idl.dealpc.InfoType;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.evaluation.EvaluationClient;
import com.qq.qqbuy.thirdparty.idl.evaluation.protocol.UserMakeEval;
import com.qq.qqbuy.thirdparty.idl.pay.PaymentClient;
import com.qq.qqbuy.thirdparty.idl.pay.protocol.PayMentApiResp;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class PaipaiDealBizImpl implements DealBiz {
	
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY = "role";
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE = "mobileLife";
	public static final String CMDY_MAKEORDER_EXTINFO_APP_VALUE = "appOrder";
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE_PAIPAI = "mobileApp";
	public static final String CMDY_MAKEORDER_EXTINFO_WEIGOU_UUID = "mobileapp.uuid" ;
	private static final String TAKECHEAP_MAKEORDER_CGIURL = "http://b.paipai.com/mfixupconfirm/confirm";
	private static final String TAKECHEAP_HOST = "b.paipai.com";
	EvaluationClient evaluationClient = new EvaluationClient();
	
	/**
	 * 支持微信支付下单
	 */
	public OrderResp orderPPDeal(OrderReq po,String mk,String lskey) {
		DealClient dealClient = new DealClient();

		OrderConfirmV2Req req = new OrderConfirmV2Req();
		ConfirmV2Req data = new ConfirmV2Req();
		data.setItemId(po.getItemCode());
		data.setItemAttr(po.getItemAttr());
		data.setUin(po.getBuyerUin());
		data.setMailType(po.getMailType());
		data.setPayType(po.getPayType());
		data.setAddressid(po.getAddressid());
		data.setRegionId(po.getRegionId());
		data.setQQNum("" + po.getSellerUin()); // 卖家QQ号
		data.setBuyNum(po.getBuyNum());
		data.setBuyerNote(po.getBuyerNote());
		
		/**
		 * 设置价格类型
		 */
		data.setPriceType(po.getPriceType());
		data.setOrderFrom(po.getOrderFrom());
		data.setPromotionId(po.getPromotionId());
		data.setRedPacketId(po.getRedPacketId());
		data.setShopcouponId(po.getShopcouponId());
		data.setOnlineShoppingCoupons(po.getOnlineShoppingCoupons());
		//积分红包
		data.setRedBagScore(po.getRedBagScore());
		// 设置扩展信息:标记移动测来源
		MultiMap<String, String> extInfos = new MultiMap<String, String>();
		//extInfos.put("role", "101weixin");
		if (EnvManager.isGamma()){
			extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY,
					CMDY_MAKEORDER_EXTINFO_APP_VALUE);
		}else{
			extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_KEY,
					CMDY_MAKEORDER_EXTINFO_WEIGOU_VALUE);
		}
		extInfos.put(CMDY_MAKEORDER_EXTINFO_WEIGOU_UUID, mk);
		data.setExtInfoMap(extInfos);

		req.setReqData(data);
		//设置下单场景标识活动
		req.setSceneId(po.getSceneId());
		//如果是京东收银台下单则加上machinekey.该处可以随意写任意字符。其它下单不知道加上这个有没有影响。京东收银台暂时加上
		if(data.getPayType()==5){
			req.setMachineKey("mobileLife");
		}
		IDLResult<OrderConfirmV2Resp> rsp = dealClient.orderWg(req, lskey);
		try {
			if (rsp.isSucceed()) {
				OrderResp resp = new OrderResp();
				CDealInfo deal = PcDealQueryClient.sysGetDealInfo(rsp.getResult().getRspData().getDealId(), false, InfoType.DEAL, mk);
				resp.setDealDetail(DealDetailConveter.convertDealDetail(deal));
				return resp;
			} else {
				throw BusinessException.createInstance((int) rsp.getErrCode(), "");
			}
		} catch (ExternalInterfaceException e) {
			throw BusinessException.createInstance(Integer.parseInt(e.getInvokeResult()), e.getMessage());
		}
	}
	
//	private  OrderResp orderPPDealByIDL(OrderReq po,String mk,String lskey) throws BusinessException{
//		DealClient dealClient = new DealClient();
//		OrderFixupReq req = new OrderFixupReq();			
//		req.setSource("mqgo");
//		COrderFixupReq data = new COrderFixupReq();
//		data.setItemId(po.getItemCode());
//		data.setItemAttr(po.getItemAttr());
//		data.setUin(po.getBuyerUin());
//		data.setMailType(po.getMailType());
//		data.setPayType(po.getPayType());
//		data.setAddressid(po.getAddressid());
//		data.setPriceType(po.getPriceType());
//		data.setQQNum(""+po.getSellerUin());  //卖家QQ号
//		
//		data.setBuyNum(po.getBuyNum());
//		data.setPromotionId(po.getPromotionId());
//		data.setBuyerNote(po.getBuyerNote());
//		data.setOrderFrom(po.getOrderFrom());   //orderFrom
//		req.setOReq(data);
//		
//		IDLResult<OrderFixupResp> rsp = dealClient.order2PP(req);
//		if (rsp.isSucceed()) {
//			OrderResp resp = new OrderResp();
//			resp.setDealDetail(getDealDetail(po.getBuyerUin(), rsp.getResult().getORsp().getDealId(), mk));
//			return resp;
//		} else {
//			throw BusinessException.createInstance((int)rsp.getErrCode(),"");
//		}
//		
//	}
	
	@Override
	public void makeOrder(OrderInfo orderInfo, String mk, String lskey)  {
		OrderReq req = new OrderReq();
		req.setBuyNum((int) orderInfo.getItemCount());
		req.setBuyerNote(orderInfo.getBuyerNote());
		req.setPayType(orderInfo.getPayType());
		try {
			req.setMailType(Integer.parseInt(orderInfo.getExpressType()));
		} catch (NumberFormatException e) {
			req.setMailType(0);
		}
		req.setItemCode(orderInfo.getItemCode());
		req.setItemAttr(orderInfo.getStockAttr());
		try {
			req.setPriceType(Integer.parseInt(orderInfo.getPriceType()));
		} catch (NumberFormatException e) {
			req.setPriceType(0);
		}
		req.setRedPacketId(orderInfo.getRedPackageId());
		req.setBuyerUin(orderInfo.getUin());
		req.setAddressid(orderInfo.getAddressId());
		req.setSellerUin(ItemUtil.getSellerUin(orderInfo.getItemCode()));

		/**
		 * 促销
		 */
		req.setPromotionId(orderInfo.getPromotionId());
		req.setShopcouponId(orderInfo.getShopcouponId());
		req.setOnlineShoppingCoupons(orderInfo.getOnlineShoppingCoupons());
		req.setRedBagScore(orderInfo.getRedBagScore());
		//设置场景
		req.setSceneId(orderInfo.getSceneId());
		OrderResp orderRsp = orderPPDeal(req, mk, lskey);
		orderInfo.setDealDetail(orderRsp.getDealDetail());
		//如果不是京东收银台支付时才创建支付单。
		if(orderInfo.getPayType()!=5){
			initPayRequest(orderInfo.getDealDetail().dealCode);
		 }
	}

	private void initPayRequest(String dealCode)	{
		//调用拍拍支付中心接口添加支付记录
		PayMentApiResp payRsp = PaymentClient.initPayInfo(dealCode);
		if(payRsp.result != 0) {
			throw BusinessException.createInstance(BusinessErrorType.INVALID_PAY_INFO);
		}
	}
	
	@Override
	public void makeEvalToSeller(long uin, String dealId, String skey, String mk, MakeEvalListPo makeEvalListPo)  {
		List<UserMakeEval> evals = new ArrayList<UserMakeEval>();
		for (MakeEvalPo m : makeEvalListPo.getMakeEvalPoList()) {
			UserMakeEval u = new UserMakeEval();
			u.setStrDealId(m.getTradeId());
			u.setDwBuyerUin(makeEvalListPo.getBuyerUin());
			u.setDwSellerUin(makeEvalListPo.getSellerUin());
			u.setCRole((short)DealConstant.EVAL_ROLE_BUYER); // 买家0, 卖家1
			u.setCHideBuyerInfo((short)1);
			u.setDwDsr1(makeEvalListPo.getDsr1());
			u.setDwDsr2(makeEvalListPo.getDsr2());
			u.setDwDsr3(makeEvalListPo.getDsr3());
			// 3好评，2中评，1差评
			u.setCEvalLevel((short)m.getLevel());
			if (m.getLevel() == DealConstant.EVAL_LEVEL_BAD && m.getReasons() != null) {
				u.setVecReason(Util.toVector(m.getReasons()));
			}  else	if (m.getLevel() == DealConstant.EVAL_LEVEL_GOOD && Util.isEmpty(m.getContent())) {
				m.setContent("好评!");
			}
			u.setStrContent(m.getContent() + " [该评价来自手机拍拍客户端]");
			evals.add(u);
		}
		evaluationClient.createBefore(uin, evals.get(0).getDwSellerUin(), dealId, mk, skey);
		evaluationClient.makeEval(uin, dealId, skey, mk, evals);
	}

	/**
	 * 拍便宜下单接口
	 *  @param wid
	 * @param sk
	 * @param sourceMk
	 * @param openid 微信/京东登陆返回
	 */
	@Override
	public void takeCheapMakeOrder(Long wid, String sk, String sourceMk, TakeCheapOrderInfo orderInfo,String openid,String scene) {
		Log.run.debug("PaipaiDealBizImpl.takeCheapMakeOrder()");
		/**
		 * 提交订单
		 */
		StringBuffer url = new StringBuffer();
		String orderData = orderInfo.getOrderData();
		String addressStr = orderInfo.getAddressStr();
		String recvaddr = orderInfo.getRecvaddr();
		String recvname = orderInfo.getRecvname();
		try {
			orderData = URLEncoder.encode(orderData,"utf8");
			addressStr = URLEncoder.encode(addressStr,"utf8");
			recvaddr = URLEncoder.encode(recvaddr,"utf8");
			recvname = URLEncoder.encode(recvname,"utf8");
		} catch (UnsupportedEncodingException e) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"下单参数异常");
		}
		url.append(TAKECHEAP_MAKEORDER_CGIURL).append("?callback=cbGoPay&orderdata=").append(orderData)//
		.append("&addressStr=").append(addressStr).append("&paytype=").append(orderInfo.getPayType())//
		.append("&paychannel=").append(orderInfo.getPaychannel()).append("&addressid=").append(orderInfo.getAddressid())//
		.append("&addrType=").append(orderInfo.getAddrType()).append("&anonymous=").append(orderInfo.getAnonymous())//
		.append("&otherpay=").append(orderInfo.getOtherpay()).append("&regionid=").append(orderInfo.getRegionid())//
		.append("&recvaddr=").append(recvaddr).append("&recvname=").append(recvname)//
		.append("&wxpay_mobile=").append(orderInfo.getWxpay_mobile()).append("&moblie=").append(orderInfo.getMoblie())//
		.append("&bid=").append(0);
		/**
		 * 判断拍便宜团购
		 */
		if(orderInfo.getTuan() == 1 && orderInfo.getGroupnum() > 1){
			url.append("&tuan=").append(orderInfo.getTuan()).append("&groupnum=").append(orderInfo.getGroupnum())//
			.append("&activenum=").append("&tuanMsg=").append(orderInfo.getTuanMsg());
			long gid = orderInfo.getGid();
			if(gid > 0){
				url.append("&gid=").append(gid);
			}
		}else{
			url.append("&isYRT=1");
		}
		/**
		 * 新增场景ID
		 */
		if(!StringUtil.isEmpty(scene)){
			url.append("&scene=").append(scene);
		}
		int timeOut = 3000;
		String cookie = "wg_uin=" + wid + ";wg_skey=" + sk + ";open_id=" + openid;
		Log.run.debug("url:"+url);
		String result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		Log.run.debug(result);
		String head = "cbGoPay(";
		String tail = ")}";
		if(null == result || !result.contains(head)){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"下单异常");
		}
		int index = result.indexOf(head);
		int end = result.indexOf(tail);
		if(index >= 0 && end > 0){
			result = result.substring(index + head.length(), end);
		}
		ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		JsonNode rootNode = null;
		try {
			rootNode = jsonMapper.readValue(result, JsonNode.class);
		} catch (JsonParseException e) {
			Log.run.error("接口返回值转换json对象异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜调用外部接口异常下单失败");
		} catch (JsonMappingException e) {
			Log.run.error("json对象字段映射异常！");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜调用外部接口异常下单失败");
		} catch (IOException e) {
			Log.run.error("");
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"拍便宜调用外部接口异常下单失败");
		}
		int errorCode = 0;
		JsonNode errorNode = rootNode.path("errId");
		if(null != errorNode && !errorNode.isNull()){
			errorCode = errorNode.asInt();
		}else{
			errorCode = rootNode.path("errorId").asInt();
		}
		if(errorCode != 0){
			if (errorCode == 777888){
				throw BusinessException.createInstance(777888,"已存在重复未支付的订单");
			}else if(errorCode == 800002){
				throw BusinessException.createInstance(800002,"商品已下架拍便宜");
			}else if(errorCode == 258){
				throw BusinessException.createInstance(258,"该用户收货地址已经不存在了");
			}else if(errorCode == 33){
				throw BusinessException.createInstance(33,"抱歉，您已购的商品超过了卖家设定的最大购买数量");
			}else{
				String errMsg = rootNode.path("errMsg").asText();
				throw BusinessException.createInstance(errorCode,errMsg);
			}
		}else{
			String dealCode = rootNode.get("dealId").asText();
			long gid = rootNode.get("gid").asLong();
			orderInfo.setGid(gid);
			long sellerUin = orderInfo.getSellerUin();
			GetTakeCheapDealDetailResponse response = new GetTakeCheapDealDetailResponse();
			response.setGid(gid);
			response.setDealCode(dealCode);
			response.setSellerUin(sellerUin);
			orderInfo.setDealDetail(response);
			initPayRequest(dealCode);
		}
	}

	public static void main(String[] args) {
		long sellerUin = 1828823192l;
		long wid = 869098670l;
		String sk = "zaC52FD9EC";
		String mk = "adf01450b8d5dfa34e7991f050d0c701dafbdc9c";
		String orderdata = "0,14,,,,,,,||CACC5B1B0000000004010000458EB78E,颜色:白色,1,0,65536,,140743688402234||459001034,32";
		String addressStr = "asdfsfddsafaf，11111111111，北京平谷区，1123123123";
		int paytype = 1;
		int paychannel = 0;
		long addressid = 2;
		int addrType = 0;
		int anonymous = 1;
		int otherpay = 0;
		long regionid = 110117;
		String recvaddr = "1123123123";
		String recvname = "asdfsfddsafaf";
		long wxpay_mobile = 11111111111l;
		long moblie = 11111111111l;
		int tuan = 1;
		int groupnum = 2;
		int activenum = 0;
		long groupId = 246075;
		String openid = "";
		String scene = "";
		String tuanMsg = "一起拼团的幸福时刻里有你也有我。";
		TakeCheapOrderInfo takeCheapOrderInfo = new TakeCheapOrderInfo(sellerUin,orderdata,addressStr,paytype
				,paychannel,addressid,addrType
				,anonymous,otherpay,regionid
				,recvaddr,recvname,wxpay_mobile
				,moblie,tuan,groupnum,activenum,groupId
				,tuanMsg);
		PaipaiDealBizImpl biz = new PaipaiDealBizImpl();
		biz.takeCheapMakeOrder(wid,sk,mk,takeCheapOrderInfo,openid,scene);
	}
}
