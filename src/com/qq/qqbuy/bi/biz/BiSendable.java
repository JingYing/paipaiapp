package com.qq.qqbuy.bi.biz;

import java.io.IOException;

public interface BiSendable {
	
	void send() throws IOException;

}
