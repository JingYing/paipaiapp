package com.qq.qqbuy.bi.biz;


public interface BiBiz {
	
	/**
	 * 上传BiSendable类信息
	 * @param biSendable
	 */
	void uploadMsg(BiSendable biSendable);
}
