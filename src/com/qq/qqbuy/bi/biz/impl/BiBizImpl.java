package com.qq.qqbuy.bi.biz.impl;

import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.bi.biz.BiSendable;

public class BiBizImpl implements BiBiz{

	@Override
	public void uploadMsg(BiSendable biSendable) {
		MsgSender.addToQueue(biSendable);
	}
	
}
