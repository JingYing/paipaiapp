package com.qq.qqbuy.bi.biz;

import java.math.BigInteger;

/**
 * BI部门常用数据,全称不明
 * 
 * 分为商品DAP和店铺DAP, 格式为:消息ID(MsgId)+完整的场景ID+策略ID+商品编码或店铺ID. 消息ID和场景ID使用UINT64, java处理时要注意long越界
 * BI提供的原始的商品DAP=6781476758684696459:1125904227579747623:128:186B828600000000040100003B21A434
 * BI提供的原始的店铺DAP=3317480726929259558:1125904223284766007:2:2875808845
 * 
 * 为节省流量,缩短长度,DAP重新进行编码. 消息ID/场景ID/策略ID统一使用62进制进行处理
 * 62进制后的商品DAP=1vI213uPDK:1fxoK7hq5ae:G:A033AD9F0000000004010000242A1B94
 * 62进制后的店铺DAP=1vI213uPDK:1fxoK7hq5ae:G:2875808845
 * 
 * @author JingYing 2014-9-7
 */
public class Dap {
	private static char[] charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
	private BigInteger msgId;
	private Scene scene;
	private long policyId; 
	private String shopidOrItemcode;
	
	/**
	 * 解析dap
	 * @param dap 原始DAP或62进制DAP
	 * @return
	 */
	public static Dap parse(String dap)	{
		if(isDap10(dap))	{
			return fromDap10(dap);
		} else	{
			return fromDap62(dap);
		}
	}
	
	/**
	 * 把62进制的DAP解析成对象
	 * @param dap62
	 * @return
	 */
	public static Dap fromDap62(String dap62)	{
		if(dap62 == null)
			throw new IllegalArgumentException("DAP不能为空");
		String[] arr = dap62.split(":");
		if(arr.length != 4)
			throw new IllegalArgumentException("不合法的DAP格式:" + dap62);
		
		Dap d = new Dap();
		d.msgId = _62ToNumeric(arr[0]);
		d.scene = Scene.parse(_62ToNumeric(arr[1]).toString());
		d.policyId = _62ToNumeric((arr[2])).longValue();
		d.shopidOrItemcode = arr[3];
		return d;
	}
	
	/**
	 * 把10进制的dap解析成对象
	 * @param dap10
	 * @return
	 */
	public static Dap fromDap10(String dap10)	{
		if(dap10 == null)
			throw new IllegalArgumentException("DAP不能为空");
		String[] arr = dap10.split(":");
		if(arr.length != 4)
			throw new IllegalArgumentException("不合法的DAP格式:" + dap10);

		Dap d = new Dap();
		d.msgId = new BigInteger(arr[0]);
		d.scene = Scene.parse(arr[1]);
		d.policyId = Long.parseLong(arr[2]);
		d.shopidOrItemcode = arr[3];
		return d;
	}
	
	/**
	 * 判断是否原始的DAP格式. 前3串都为数字,即判定为10进制.特殊的62进制有时也会全为数字.
	 * @param dap
	 * @return
	 */
	public static boolean isDap10(String dap)	{
		String[] arr = dap.split(":");
		try {
			for(int i=0; i<3; i++)	{
				if(!arr[i].matches("\\d+"))	{
					return false;
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 组装DAP
	 * @param msgId
	 * @param sceneId
	 * @param policyId
	 * @param shopidOritemcode
	 */
	public Dap(String msgId, String sceneId, long policyId, String shopidOrItemcode)	{
		this.msgId = new BigInteger(msgId);
		this.scene = Scene.parse(sceneId);
		this.policyId = policyId;
		this.shopidOrItemcode = shopidOrItemcode;
	}
	
	private Dap()	{}
	
	/**
	 * 62进制的DAP
	 */
	@Override
	public String toString()	{
		return to62Str();
	}
	
	/**
	 * 十进制形式的DAP
	 * @return
	 */
	public String toDecimalStr()	{
		return String.format("%s:%s:%s:%s", msgId, scene.toString(), policyId, shopidOrItemcode);
	}
	
	/**
	 * 62进制形式的DAP
	 * @return
	 */
	public String to62Str()	{
		return String.format("%s:%s:%s:%s", 
				numericTo62(msgId),
				numericTo62(new BigInteger(scene.toString())), 
				numericTo62(new BigInteger(policyId+"")), 
				shopidOrItemcode);
	}
	
	/**
	 * 该DAP是否为店铺
	 * @return
	 */
	public boolean isShop()	{
		if(!isItem())	{
			try {
				Long.parseLong(shopidOrItemcode);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 该DAP是否是商品
	 * @return
	 */
	public boolean isItem()	{
		return shopidOrItemcode.length() == 32;
	}
	
	
	/**
	 * 将数字转化为62进制 
	 * @param number 
	 * @param length 转化成的62进制长度，不足length长度的话高位补0，否则不改变什么
	 * @return
	 */
	public String numericTo62(BigInteger num){
		BigInteger b62 = new BigInteger("62");
		if("0".equals(num))	return "0";
		
		StringBuilder sb = new StringBuilder();
		while(!"0".equals(num.toString()))	{
			BigInteger b = num.divide(b62).multiply(b62); 
		    sb.append(charset[num.subtract(b).intValue()]);
		    num = num.divide(b62);
		}
		return sb.reverse().toString();
	}
	
	/**
	 * 将62进制解码成数字
	 * @param ident62
	 * @return
	 */
	private static BigInteger _62ToNumeric(String ident62) {
		BigInteger result = new BigInteger("0");
		byte ident[] = ident62.getBytes();
		int cnt = 0;
		for ( int i=ident.length-1; i>=0; i--) {
			int num = 0;
			if ( ident[i] > 48 && ident[i] <= 57) {
				num = ident[i] - 48;
			}
			else if ( ident[i] >= 65 && ident[i] <= 90) {
				num = ident[i] - 65 + 10;
			}
			else if ( ident[i] >= 97 && ident[i] <= 122) {
				num = ident[i] - 97 + 10 + 26;
			}
			long keisu = (long) Math.pow((double)62, (double)cnt);
			result = result.add(new BigInteger(num+"").multiply(new BigInteger(keisu+"")));
			cnt++;
		}
		return result;
	}
	
	public BigInteger getMsgId() {
		return msgId;
	}

	public long getPolicyId() {
		return policyId;
	}

	public String getShopidOrItemcode(){
		return shopidOrItemcode;
	}

	public Scene getScene() {
		return scene;
	}
}
