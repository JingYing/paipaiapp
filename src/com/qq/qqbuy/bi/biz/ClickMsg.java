package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.qq.qqbuy.common.util.Util;

public class ClickMsg implements BiSendable	{
	
	/**
	 * buf << m_dwTimeStamp（空字符串）<< strSep << strP(paipai.com) << strSep <<  strToUrl（http://auction1.paipai.com/5479DB4100000000040100003D5667AF） << strSep
	   << curUrl（www.paipai.com） << strSep << strValue（wpaipai:2001|ptag：|wgtag：|deviceID:|deviceType:|sku:|shop:|category:|leaf:|MsgID:|PolicyID:|SceneID:|shopType:|skus:|latitude:|longtitude:） 
	   << strSep << strRefer（www.paipai.com） << strSep
	   << strStParam（uin:10001|visitkey:10000000000|uintype:1|pprd:） <<  strSep << strPvid (1000000000) << strSep
	   << m_strVisitKey（100000000000） << strSep <<m_strClientIP(10.0.0.1) << strSep << m_dwUin(10001) << strSep
	   << m_strServTime(时间戳1400001400) << strSep << strSkey（登陆态skey，app能取到就赋值，否则为空字符串） << strSep
	   << strWxOpenId (app能取到就赋值，否则为空字符串)<< strSep 
	 * @return
	 * @throws IOException
	 */
	private String 
			timestamp = "", strP = "paipai.com", strToUrl, 
			curUrl, strValue, strRefer, stParam, 
			strPvid, strVisitkey, strClientIp,
			dwUin, servTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), skey = "", 
			wxOpenId = "";
	
	public String assembly()	{
		String[] arr = new String[]{
			timestamp, strP, strToUrl, 
			curUrl, strValue, strRefer, stParam, 
			strPvid, strVisitkey, strClientIp,
			dwUin, servTime, skey, wxOpenId
		};
		return Util.join(arr, "$$") + "$$";
	}

	@Override
	public void send() throws IOException {
		String s = assembly();
		byte[] body = s.getBytes("utf-8");
		byte[] bs = ByteBuffer.allocate(2+4+body.length)
				.putShort((short)2)	//1pv, 2click
				.putInt(body.length)
				.put(body)
				.array();
		new PvclickSender().send(bs);
	}
	
	

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setStrP(String strP) {
		this.strP = strP;
	}

	public void setStrToUrl(String strToUrl) {
		this.strToUrl = strToUrl;
	}

	public void setCurUrl(String curUrl) {
		this.curUrl = curUrl;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

	public void setStrRefer(String strRefer) {
		this.strRefer = strRefer;
	}

	public void setStParam(String stParam) {
		this.stParam = stParam;
	}

	public void setStrPvid(String strPvid) {
		this.strPvid = strPvid;
	}

	public void setStrVisitkey(String strVisitkey) {
		this.strVisitkey = strVisitkey;
	}

	public void setStrClientIp(String strClientIp) {
		this.strClientIp = strClientIp;
	}

	public void setDwUin(String dwUin) {
		this.dwUin = dwUin;
	}

	public void setServTime(String servTime) {
		this.servTime = servTime;
	}

	public void setSkey(String skey) {
		this.skey = skey;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

}
