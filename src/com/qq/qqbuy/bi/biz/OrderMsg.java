package com.qq.qqbuy.bi.biz;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.boss.BossWebStub2;
import com.qq.qqbuy.thirdparty.idl.boss.EmptyResponse;
import com.qq.qqbuy.thirdparty.idl.boss.protocol.DataserverSyncReq;

/**
 * 下单入口:
 *  paipai 普通商详，auctionfixupconfirm
	paipai 购物车，shopcart_unixtime
	paipai 一口价标，oneaday
	paipai 扫购直接下单，sgzj_pvid（PC端的PVID）
	paipai 扫购商详下单，sgsx_pvid（PC端的PVID）
	paipai 无线侧H5下单，mfixupconfirm
	paipai 无线侧H5购物车下单，mcart
	paipai 无线侧APP 商详下单, mapp_fixupconfirm
	paipai 无线侧APP 购物车下单, mapp_shopcart

 * @author JingYing 2014-9-25
 *
 */
public class OrderMsg implements BiSendable{
	static Logger log = LogManager.getLogger(OrderMsg.class);
	
	public static final String 
				下单入口_商详页下单 = "mapp_fixupconfirm",
				下单入口_购物车下单 = "mapp_shopcart";
	
	private String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	private String dealCode, dealSubCode;
	private long buyerUin, sellerUin;
	private String itemCode;
	private long leafCatId, price, num;
	private String pprdP, pprdS = "", pprdT = "", clientIp, visitkey, orderEntrance;
	private int payType;
	private long favTime = 0;
	
	private OrderMsg()	{	}
	
	/**
	 * 构造OrderMsg
	 * @param dealcode
	 * @param dealsubcode
	 * @param buyer
	 * @param seller
	 * @param itemcode
	 * @param leafcatid
	 * @param price
	 * @param num
	 * @param pprdP
	 * @param clientIp
	 * @param reqPort 手机request的端口号
	 * @param orderEntrance 见OrderMsg.下单入口_*常量
	 * @param payType
	 * @return
	 */
	public static OrderMsg build(String dealcode, String dealsubcode, long buyer, long seller,
								String itemcode, long leafcatid, long price, long num, 
								String pprdP, String clientIp, int reqPort, String orderEntrance, 
								int payType)	{
		OrderMsg msg = new OrderMsg();
		msg.dealCode = dealcode;
		msg.dealSubCode = dealsubcode;
		msg.buyerUin = buyer;
		msg.sellerUin = seller;
		msg.itemCode = itemcode;
		msg.leafCatId = leafcatid;
		msg.price = price;
		msg.num = num;
		msg.pprdP = pprdP;
		msg.clientIp = clientIp;
		msg.visitkey = Util.genVisitkey(clientIp, reqPort)+"";
		msg.orderEntrance = orderEntrance;
		msg.payType = payType;
		return msg;
	}
	
	
	private String assembly()	{
		String[] arr = new String[]{
			time,
			dealCode,
			dealSubCode, 
			buyerUin+"",
			sellerUin+"",
			itemCode,
			leafCatId+"",
			price+"",
			num+"",
			pprdP,
			pprdS,
			pprdT,
			clientIp,
			visitkey,
			orderEntrance,
			payType+"",
			favTime+"",
		};
		return Util.join(arr, "\t");
	}
	
	public DataserverSyncReq toReq()	{
		DataserverSyncReq req = new DataserverSyncReq();
		req.setUiVersion(0);
		req.setUiTime(System.currentTimeMillis()/1000);
		req.setUiQQ(buyerUin);
		req.setStrBusiness("dwlog");
		req.setStrType("deal");
		req.setStrData(assembly());
		return req;
	}
	
	
	@Override
	public void send() throws IOException {
		BossWebStub2 stub = WebStubFactory.getBossStub2();
		DataserverSyncReq req = toReq();
		try {
			stub.invoke("boss_dataserver2", req, new EmptyResponse(req.getCmdId()), 2);
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

}
