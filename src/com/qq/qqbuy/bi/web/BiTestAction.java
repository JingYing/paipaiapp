 	
package com.qq.qqbuy.bi.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paipai.component.configagent.ConfigHelper;
import com.paipai.component.configcenter.api.set.SvcRoute;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.client.router.Router;
import com.qq.qqbuy.common.client.router.ao.AoServer;
import com.qq.qqbuy.common.client.router.tcp.TcpServer;
import com.qq.qqbuy.common.client.router.udp.UdpServer;

 /**
  * 测试入口
  * @author JingYing
  * @date 2014年12月11日
  */
public class BiTestAction extends AuthRequiredAction{
	
	static Logger log = LogManager.getLogger(BiTestAction.class);
	
	String[] p;
	BiBiz biBiz;
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	String auth, type;
	long contentLength;
	InputStream routerResult;
	
	public String routerAccess()  	{
		byte[] ret = null;
		try {
			if("ao".equals(type))	{
				ret = new AoServer().handle(readReqBody(), getHttpHeadIp());
//			} else if("http".equals(type))	{
//				ret = new HttpServer().handle(readReqBody());
			} else if("tcp".equals(type))	{
				ret = new TcpServer().handle(readReqBody());
			} else if("udp".equals(type))	{
				ret = new UdpServer().handle(readReqBody());
			}
			
			contentLength = ret.length;
			routerResult = new ByteArrayInputStream(ret);
			return SUCCESS;
		} catch (Exception e) {
			return doPrint(e.getMessage());
		}
		
	}
	
	
	
	private void assertAuth()	{
		boolean isPost = "post".equalsIgnoreCase(ServletActionContext.getRequest().getMethod());
		boolean isAuth = Router.SERVER_AUTH.equals(auth);
		if(!isPost || !isAuth)	{
			try {
				getResponse().sendError(505);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
	}
	
	public String wfadfvasdedacxvee()	{
		assertAuth();
		boolean isLogin = checkLoginAndGetSkey();
		String s = String.format("%s|%s|%s", getQq(), getSk(), isLogin);
		return doPrint(s);
	}
	
	private void auth()	{
		if(!"afadnfkdfjfiweuhrfebfskdfhspoweri".equals(auth))	{
			throw new RuntimeException();
		}
	}
	
	public String testConfig()	{
		InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(Integer.parseInt(p[0]), p[1], 0, 1);
		return doPrint(addr.getHostName() + ":" + addr.getPort());
	}
	
	public String testConfig2()	{
		//config_identify
		InetSocketAddress addr = SvcRoute.getSvcAddressBySet(p[0], 0, 0);
		return doPrint(addr.getHostName() + ":" + addr.getPort());
	}
	
	public String[] getP() {
		return p;
	}

	public void setP(String[] p) {
		this.p = p;
	}

	public BiBiz getBiBiz() {
		return biBiz;
	}

	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public InputStream getRouterResult() {
		return routerResult;
	}

	public void setRouterResult(InputStream routerResult) {
		this.routerResult = routerResult;
	}
}
