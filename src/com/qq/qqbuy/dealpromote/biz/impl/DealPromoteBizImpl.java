package com.qq.qqbuy.dealpromote.biz.impl;

import com.qq.qqbuy.cmdy.util.CmdyUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.dealpromote.biz.DealPromoteBiz;
import com.qq.qqbuy.dealpromote.po.*;
import com.qq.qqbuy.dealpromote.util.DealPromoteUtil;
import com.qq.qqbuy.item.util.DispFormater;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.PayRecommendClient;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.GetPayRecommendListResp;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;
import com.qq.qqbuy.thirdparty.idl.promote.DealPromoteClient;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromoteSaleResp;
import net.sf.json.JSONSerializer;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Vector;

public class DealPromoteBizImpl implements DealPromoteBiz {

	/**
	 *  批价接口 场景： 1 首次加载 2 切换是否货到付款 3 优惠信息变更 4 修改商品数量
	 */
	private static final String TAKECHEAP_DEALPROMOTE_CGIURL = "http://b.paipai.com/tws/price/query";
	/**
	 * 大V团优惠价钱计算
	 */
	private static final String TAKECHEAP_VDEALPROMOTE_CGIURL = "http://b.paipai.com/tuaninfo/PromotePrice";
	/**
	 * 请求商品数据
	 */
	private static final String TAKECHEAP_ORDERVIEW_CGIURL = "http://b.paipai.com/mfixupconfirm/orderview";
	private static final String TAKECHEAP_HOST = "b.paipai.com";

	@Override
	public DealFavorVo queryPromote(DealPromotePo po, ConfirmOrderVo confirmVo, String sk, String mk)
			throws BusinessException {
		// TODO Auto-generated method stub

		DealPromoteSaleResp resp = DealPromoteClient.queryPromote(po, sk, mk);

		DealFavorVo vo = new DealFavorVo();
		if (resp == null || resp.getResult() != 0) {
			if (resp == null) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
				vo.setErrorMsg("未知错误.");

				return vo;
			} else if (resp.getResult() == 1) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_PARAM_ERROR);
				vo.setErrorMsg("参数错误.");

				return vo;
			} else if (resp.getResult() == 2) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_COMBO_IDL_FAIL);
				vo.setErrorMsg("IDL套餐接口异常.");

				return vo;
			} else if (resp.getResult() == 3) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_ITEM_PRICE_IDL_FAIL);
				vo.setErrorMsg("商品批价接口异常.");

				return vo;
			} else if (resp.getResult() == 4) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_SHOP_ACTIVE_IDL_FAIL);
				vo.setErrorMsg("店铺促销接口异常.");

				return vo;
			} else if (resp.getResult() == 5) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_RED_PKG_IDL_FAIL);
				vo.setErrorMsg("红包接口异常.");

				return vo;
			} else if (resp.getResult() == 6) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_IDL_TIME_OUT);
				vo.setErrorMsg("接口超时.");

				return vo;
			} else if (resp.getResult() == 7) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_ITEM_FAVOR_ERROR);
				vo.setErrorMsg("商品优惠异常.");

				return vo;
			} else if (resp.getResult() == 8) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_DEAL_FAVOR_ERROR);
				vo.setErrorMsg("订单优惠异常.");

				return vo;
			} else if (resp.getResult() == 9) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_ACTIVE_FAVOR_ERROR);
				vo.setErrorMsg("店铺优惠异常.");

				return vo;
			} else if (resp.getResult() == 10) {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_MAX_DEAL_FEE_ERROR);
				vo.setErrorMsg("订单金额超过最大限制.");

				return vo;
			} else {
				vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
				vo.setErrorMsg("订单批价失败.");

				return vo;
			}
		}
		try {
			vo = DealPromoteUtil.convert(resp.getODealFavor().getTradeFavortList(), po, confirmVo);
		} catch (Exception e) {
			vo.setErrorCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
			vo.setErrorMsg("批价数据转换失败.");
			return vo;
		}
		return vo;
	}


	/**
	 * 拍便宜大V团批价
	 *
	 * @return
	 * @throws com.qq.qqbuy.common.exception.BusinessException
	 * @param wid
	 * @param sk
	 * @param gid
	 * @param random
	 * @param openid
	 */
	@Override
	public DealFavorVo takeCheapQueryPromote(long wid, String sk, String gid, double random,String openid) throws BusinessException {
		DealFavorVo dealVo = new DealFavorVo();
		StringBuffer url = new StringBuffer();
		url.append(TAKECHEAP_VDEALPROMOTE_CGIURL).append("callback=PromotePrice&tid=").append(gid)//
				.append("&t=").append(random);
		int timeOut = 3000;
		String cookie = "wg_uin=" + wid + ";wg_skey=" + sk + ";open_id=" + openid;
		String result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		String head = "try{fixupConfirmCallBack(";
		String tail = ");}catch(e){}";
		if(null == result || !result.startsWith(head)){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
		}
		int index = result.indexOf(head);
		int end = result.indexOf(tail);
		if(index >= 0 && end > 0){
			result = result.substring(index + head.length(), end);
		}
		ObjectMapper jsonMapper = new ObjectMapper();

		JsonNode rootNode = null;
		try {
			rootNode = jsonMapper.readValue(result, JsonNode.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		int errorCode = rootNode.path("errorId").getIntValue();
		if(errorCode != 0){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"货到付款服务费计算异常");
		}
		return dealVo;
	}

	/**
	 * 拍便宜分单接口，初始化时调用获取批价参数
	 *
	 * @param dealVo
	 * @param wid 买家qq号
	 * @param sk
	 * @param scene 场景
	 * @param isCanEdit 是否可编辑
	 * @param bid
	 * @param sid
	 * @param commlist 商品信息串
	 * @param openid
	 * @return TakeCheapDealPo
	 */
	@Override
	public String takeCheapOrderView(TakeCheapDealFavorVo dealVo, long wid, String sk, int scene, int isCanEdit, int bid, String sid, String commlist,String openid,int useRedPackage) {

		String dealStrs = "";
		try {
			commlist = URLEncoder.encode(commlist, "utf8");
		} catch (UnsupportedEncodingException e) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取团订单接口信息参数异常");
		}
		StringBuffer url = new StringBuffer();
		url.append(TAKECHEAP_ORDERVIEW_CGIURL).append("?callback=_cbOrderview&scene=").append(scene)//
				.append("&isCanEdit=").append(isCanEdit).append("&commlist=").append(commlist);
		int timeOut = 3000;
		String cookie = "wg_uin=" + wid + ";wg_skey=" + sk + ";open_id=" + openid;
		String result = "";
		if (EnvManager.isGamma()){
			result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, "10.12.199.28:80"),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		}else{
			result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		}
		Log.run.info(result);
		String head = "try{_cbOrderview(";
		String tail = ")} catch(e){}";
		if(null == result || !result.startsWith(head)){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取团订单接口信息异常");
		}
		int index = result.indexOf(head);
		int end = result.indexOf(tail);
		if(index >= 0 && end > 0){
			result = result.substring(index + head.length(), end);
		}
		ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		JsonNode rootNode = null;
		try {
			rootNode = jsonMapper.readValue(result, JsonNode.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int errorCode = 0;
		String msg = "";
		JsonNode errorNode = rootNode.path("errorId");
		if(null != errorNode && !errorNode.isNull() && errorNode.equals("")){
			errorCode = errorNode.asInt();
		}else{
			errorCode = rootNode.path("errId").asInt();
		}
		JsonNode msgNode = rootNode.path("errorMess");
		if(null != msgNode && !msgNode.isNull() && errorNode.equals("")){
			msg = msgNode.asText();
		} else{
			msg = rootNode.path("errMsg").asText();
		}
		if(errorCode != 0){
			throw BusinessException.createInstance(errorCode,msg);
		}
		long shopId = 0;
		String orderId = "";
		long payOnReciew = 0;
		String cid = "";
		String stock = "";
		String skuid = "";
		long num = 0;
		int isSupWxpay = rootNode.get("IsSupWxpay").asInt();
		int isHasScore = rootNode.get("IsHasScore").asInt();
		int sellerpropery = 0;
		long shopProp = 0;
		int sceneID = rootNode.get("sceneID").asInt();
		long onpromotion = 0;
		dealVo.setIsSupportWXPay(isSupWxpay);
		dealVo.setIsHasScore(isHasScore);
		dealVo.setSceneId(sceneID);
		Vector<TakeCheapTradeFavorVo> dealList = dealVo.getDealList();
		JsonNode orders = rootNode.path("order");
		Iterator<JsonNode> orderList = orders.getElements();
		while (orderList.hasNext()){
			TakeCheapTradeFavorVo favorVo = new TakeCheapTradeFavorVo();
			JsonNode order = orderList.next();
			String indexId = order.get("index").asText();
			orderId = indexId+"1";
			favorVo.setOrderId(orderId);
			shopId = order.get("shopId").asLong();
			favorVo.setShopId(shopId);
			int fapiao = order.get("fapiao").asInt();
			favorVo.setFapiao(fapiao);
			onpromotion = order.get("onpromotion").asLong();
			favorVo.setPromotion(onpromotion);
			shopProp = order.get("shopProp").asLong();
			favorVo.setShopProp(shopProp);
			JsonNode commodity = order.path("commodity");
			Iterator<JsonNode> items = commodity.getElements();
			Vector<TakeCheapItemPromoteResultVo> itemVos = new Vector<TakeCheapItemPromoteResultVo>();
			while (items.hasNext()){
				JsonNode item = items.next();
				TakeCheapItemPromoteResultVo itemVo = new TakeCheapItemPromoteResultVo();
				cid = item.get("cid").asText();
				itemVo.setCid(cid);
				stock = item.get("stockAttr").asText();
				itemVo.setStockAttr(stock);
				String cName = item.get("cName").asText();
				itemVo.setcName(cName);
				skuid = item.get("skuid").asText();
				payOnReciew = item.get("payOnReciew").asLong();
				favorVo.setPayOnReceive(payOnReciew);
				String image = item.get("image").asText();
				itemVo.setImage(image);
				num = item.get("num").asLong();
				itemVo.setNum(num);
				long maxBuy = item.get("maxBuy").asLong();
				itemVo.setMaxBuy(maxBuy);
				long stockNum = item.get("stockNum").asLong();
				itemVo.setStockNum(stockNum);
				long comdyType = item.get("ComdyType").asLong();
				itemVo.setComdyType(comdyType);
				itemVos.add(itemVo);
			}
			favorVo.setCommodity(itemVos);
			dealList.add(favorVo);
		}
		dealVo.setDealList(dealList);
		/**
		 * 判断是否使用红包
		 */
		if((shopProp & 1) > 0){
			sellerpropery = 1;
		}else{
			if (useRedPackage == 1 && isHasScore == 1){
				sellerpropery = 2;
			}else{
				sellerpropery = 0;
			}
		}
		dealVo.setSellerpropery(sellerpropery);
		//拼接dealStrs
		StringBuffer buffer = new StringBuffer();
		buffer.append("{\"order\":[{\"shopId\":\"").append(shopId).append("\",\"orderId\":\"").append(orderId)//
				.append("\",\"promotionId\":\"").append("").append("\",\"shopCouponId\":\"").append("")//
				.append("\",\"wgCouponId\":\"").append("").append("\",\"balanceId\":\"").append("")//
				.append("\",\"shipcardId\":\"").append("").append("\",\"comboId\":\"").append("")//
				.append("\",\"sceneId\":\"").append(sceneID).append("\",\"payOnReciew\":\"").append(0)//
				.append("\",\"onpromotion\":\"").append(onpromotion).append("\",\"sellerpropery\":\"").append(sellerpropery)//
				.append("\",\"commodity\":[{\"cid\":\"").append(cid).append("\",\"stock\":\"").append(stock)//
				.append("\",\"skuid\":\"").append(skuid).append("\",\"num\":\"").append(num).append("\",\"priceId\":\"").append(0)//
				.append("\",\"hongbao\":[]}]}],\"sn\":\"").append(System.currentTimeMillis() + "").append("\"}");
		dealStrs = buffer.toString();
		return dealStrs;
	}

	/**
	 * 拍便宜批价接口，返回app显示信息
	 * @param wid
	 * @param sk
	 * @param reqSource 请求源
	 * @param orderFrom 订单来源，团团降为10，其他为8
	 * @param sid
	 * @param dealStrs 批价参数封装对象
	 * @param openid
	 * @return TakeCheapDealFavorVo
	 * @throws BusinessException
	 */
	@Override
	public TakeCheapDealFavorVo takeCheapQuery(TakeCheapDealFavorVo dealVo,long wid, String sk, int reqSource,int orderFrom,String sid,String dealStrs,String openid,int useRedPackage,long versionCode) throws BusinessException {

		try {
			dealStrs = URLEncoder.encode(dealStrs, "utf8");
		} catch (UnsupportedEncodingException e) {
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"团订单批价信息参数异常");
		}
		StringBuffer url = new StringBuffer();
		url.append(TAKECHEAP_DEALPROMOTE_CGIURL).append("?callback=__cbQueryPrice&reqSource=").append(reqSource)//
				.append("&src=mobile").append("&orderFrom=").append(orderFrom).append("&sid=").append(sid)//
				.append("&info=").append(dealStrs) ;
		if(orderFrom == 8){//拍便宜团购缺省活动
			url.append("&dealSource=TuanTJSingle");
		}
		int timeOut = 3000;
		String cookie = "wg_uin=" + wid + ";wg_skey=" + sk + ";open_id=" + openid;
		String result = "";
		if (EnvManager.isGamma()){
			result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, "10.12.199.28:80"),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		}else{
			result = HttpUtil.get(url.toString().replace(TAKECHEAP_HOST, PaiPaiConfig.getPaipaiCommonIp()),TAKECHEAP_HOST, timeOut, timeOut, "gbk", false, false, cookie, null);
		}
		System.out.println(result);
		String head = "__cbQueryPrice(";
//		String tail = "})";
		if(null == result || !result.startsWith(head)){
			throw BusinessException.createInstance(BusinessErrorType.CALL_OUTIDL_FAIL.getErrCode(),"获取团订单接口信息异常");
		}
		int index = result.indexOf(head);
		int end = result.length()-1;
		if(index >= 0 && end > 0){
			result = result.substring(index + head.length(), end);
		}
		ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		JsonNode rootNode = null;
		try {
			rootNode = jsonMapper.readValue(result, JsonNode.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		int errorCode = rootNode.path("retCode").getIntValue();
		if(errorCode != 0){
			String msg = rootNode.path("errorMess").getTextValue();
			throw BusinessException.createInstance(errorCode,msg);
		}
		JsonNode order = rootNode.path("order");
		Iterator<JsonNode> orderList = order.getElements();
		Vector<TakeCheapTradeFavorVo> dealList = dealVo.getDealList();
		if(dealList.size() < 1){
			TakeCheapTradeFavorVo favorVo = new TakeCheapTradeFavorVo();
			dealList.add(favorVo);
		}
		TakeCheapTradeFavorVo favorVo = dealList.get(0);
		Vector<TakeCheapItemPromoteResultVo> commodityList = favorVo.getCommodity();
		if (commodityList.size() < 1){
			TakeCheapItemPromoteResultVo itemVo = new TakeCheapItemPromoteResultVo();
			commodityList.add(itemVo);
		}
		TakeCheapItemPromoteResultVo itemVo = commodityList.get(0);
		ShopBiz shopBiz = (ShopBiz) SpringHelper.getBean("shopBiz");
		while (orderList.hasNext()){
			JsonNode orderNode = orderList.next();
			long amount = orderNode.get("amount").asLong();
			dealVo.setTotalPrice(amount);
			favorVo.setAmount(amount);
			String orderId = orderNode.get("orderId").asText();
			favorVo.setOrderId(orderId);
			long shopId = orderNode.get("shopId").asLong();
			ShopInfo shopInfo = shopBiz.getShopInfo(shopId);
			favorVo.setShopId(shopId);
			favorVo.setShopName(shopInfo.getShopName());
			favorVo.setShopType(shopInfo.getShopType());
			/**
			 * 商品列表
			 */
			JsonNode commodity = orderNode.path("commodity");
			Iterator<JsonNode> elements = commodity.getElements();
			while (elements.hasNext()){
				JsonNode item = elements.next();
				long amount1 = item.get("amount").asLong();
				itemVo.setAmount(amount1);
				long priceType = item.get("curPrice").asLong();
				itemVo.setPriceType(priceType);
				String cid = item.get("cid").asText();
				itemVo.setCid(cid);
				long num = item.get("num").asLong();
				itemVo.setNum(num);
				String stock = item.get("stock").asText();
				itemVo.setStockAttr(stock);
				long redsum = item.get("redsum").asLong();
				itemVo.setRedsum(redsum);
				JsonNode prices = item.path("price");
				Iterator<String> fieldNames = prices.getFieldNames();
				while (fieldNames.hasNext()){
					String fieldName = fieldNames.next();
					if (fieldName.equals(String.valueOf(priceType))){
						long price = prices.get(fieldName).asLong();
						itemVo.setPrice(price);
					}
				}
				JsonNode priceDescs = item.path("priceDesc");
				Iterator<String> descFieldNames = priceDescs.getFieldNames();
				while (descFieldNames.hasNext()){
					String fieldName = descFieldNames.next();
					if (fieldName.equals(String.valueOf(priceType))){
						String desc = priceDescs.get(fieldName).asText();
						itemVo.setPriceDesc(desc);
					}
				}
			}
			if(useRedPackage == 1){
				/**
				 * 店铺优惠券列表
				 *"amount" : 500,
				 "available" : 1,
				 "desc" : "买满150.00元即可使用",
				 "id" : "1958118374",
				 "indate" : "2015-03-31",
				 "selected" : 1
				 */
				Vector<TakeCheapFavorExVo> shopCoupon = favorVo.getShopCoupon();
				JsonNode shopCouponNode = orderNode.path("shopCoupon");
				if (!shopCouponNode.isMissingNode() || !shopCouponNode.isNull()){
					Iterator<JsonNode> shopCouponNodes = shopCouponNode.getElements();
					while (shopCouponNodes.hasNext()){
						JsonNode promotion = shopCouponNodes.next();
						TakeCheapFavorExVo favorExVo = new TakeCheapFavorExVo();
						favorExVo.setAvailable(getInt(promotion,"available"));
						favorExVo.setDesc(getString(promotion,"desc"));
						favorExVo.setFavMoney(getLong(promotion,"amount"));
						favorExVo.setId(getLong(promotion,"id"));
						favorExVo.setSelected(getInt(promotion,"selected"));
						favorExVo.setIndate(getString(promotion,"indate"));
						shopCoupon.add(favorExVo);
					}
				}
				favorVo.setShopCoupon(shopCoupon);
				/**
				 * 网购优惠券列表
				 * "amount" : 600,
				 * "available" : 0,
				 * "desc" : "买满99.00元即可使用",
				 * "id" : "1958214089",
				 * "indate" : "2015-03-31",
				 * "msg" : "还需购买90.09元商品，才能参加此优惠",
				 * "selected" : 0
				 */
				Vector<TakeCheapFavorExVo> wgCoupon = favorVo.getWgCoupon();
				JsonNode wgCouponNode = orderNode.path("wgCoupon");
				if (!wgCouponNode.isMissingNode() || !wgCouponNode.isNull()){
					Iterator<JsonNode> wgCouponNodes = wgCouponNode.getElements();
					while (wgCouponNodes.hasNext()){
						JsonNode promotion = wgCouponNodes.next();
						TakeCheapFavorExVo favorExVo = new TakeCheapFavorExVo();
						favorExVo.setAvailable(getInt(promotion,"available"));
						favorExVo.setDesc(getString(promotion,"desc"));
						favorExVo.setFavMoney(getLong(promotion,"amount"));
						favorExVo.setId(getLong(promotion,"id"));
						favorExVo.setMsg(getString(promotion,"msg"));
						favorExVo.setSelected(getInt(promotion,"selected"));
						favorExVo.setIndate(getString(promotion,"indate"));
						wgCoupon.add(favorExVo);
					}
				}
				favorVo.setWgCoupon(wgCoupon);
				/**
				 * 积分红包列表
				 * "balance" : [
				 {
				 "amount" : 99,
				 "available" : 1,
				 "desc" : "红包积分",
				 "id" : "0",
				 "indate" : "1969-12-31",
				 "msg" : "",
				 "selected" : 1
				 }
				 ],
				 */
				Vector<TakeCheapFavorExVo> balance = favorVo.getBalance();
				JsonNode balanceNode = orderNode.path("balance");
				if (!balanceNode.isMissingNode() || !balanceNode.isNull()){
					Iterator<JsonNode> balanceNodes = balanceNode.getElements();
					while (balanceNodes.hasNext()){
						JsonNode promotion = balanceNodes.next();
						TakeCheapFavorExVo favorExVo = new TakeCheapFavorExVo();
						favorExVo.setAvailable(getInt(promotion,"available"));
						favorExVo.setDesc(getString(promotion,"desc"));
						favorExVo.setFavMoney(getLong(promotion,"amount"));
						favorExVo.setId(getLong(promotion,"id"));
						favorExVo.setMsg(getString(promotion,"msg"));
						favorExVo.setSelected(getInt(promotion,"selected"));
						favorExVo.setIndate(getString(promotion,"indate"));
						balance.add(favorExVo);
					}
				}
				favorVo.setBalance(balance);
				/**
				 * "available" : 1,
				 "desc" : "本店买满59.00元 [包快递]",
				 "favMoney" : 0,
				 "flag" : 0,
				 "id" : "15947716",
				 "mailfree" : 1,
				 "minimum" : 5900,
				 "selected" : 1,
				 "type" : 16
				 * 优惠活动信息
				 */
				Vector<TakeCheapFavorExVo> salePromotion = favorVo.getSalePromotion();
				if(versionCode > 319){
					JsonNode promotionNode = orderNode.path("salePromotion");
					if (!promotionNode.isMissingNode() || !promotionNode.isNull()){
						Iterator<JsonNode> promotionNodes = promotionNode.getElements();
						while (promotionNodes.hasNext()){
							JsonNode promotion = promotionNodes.next();
							TakeCheapFavorExVo favorExVo = new TakeCheapFavorExVo();
							favorExVo.setAvailable(getInt(promotion,"available"));
							favorExVo.setDesc(getString(promotion,"desc"));
							favorExVo.setFavMoney(getLong(promotion,"favMoney"));
							favorExVo.setFlag(getInt(promotion,"flag"));
							favorExVo.setId(getLong(promotion,"id"));
							favorExVo.setMailFree(getInt(promotion,"mailfree"));
							favorExVo.setMinimum(getLong(promotion,"minimum"));
							favorExVo.setMsg(getString(promotion,"msg"));
							favorExVo.setSelected(getInt(promotion,"selected"));
							favorExVo.setType(getLong(promotion,"type"));
							salePromotion.add(favorExVo);
						}
					}
				}
				favorVo.setSalePromotion(salePromotion);
			}
		}

		return dealVo;
	}

	protected static double getDouble(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return jobj.get(key).asDouble();
			} catch (Exception e) {
				// ignore
			}
		}
		return 0;
	}

	protected static long getLong(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return jobj.get(key).asLong();
			} catch (Exception e) {
				// ignore;
			}
		}
		return 0;
	}

	protected static int getInt(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return jobj.get(key).asInt();
			} catch (Exception e) {
				// ignore;
			}
		}
		return 0;
	}

	protected static String getString(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return jobj.get(key).asText();
			} catch (Exception e) {
				// ignore
			}
		}
		return "";
	}

	protected static JsonNode getJSONObject(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return jobj.get(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return null;
	}

	protected static ArrayNode getJSONArray(JsonNode jobj, String key) {
		if (jobj != null && jobj.has(key)) {
			try {
				return (ArrayNode) jobj.get(key);
			} catch (Exception e) {
				// ignore
			}
		}
		return null;
	}

	/**
	 * 获取推荐支付方式列表
	 *
	 * @param mk           设备码
	 * @param wid          买家qq
	 * @param userType     用户类型  用户类型：1=QQ qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿); 2=WX wx账号非绑定qq(login_type=wx切uin>39亿); 3=JD JD账号
	 * @param dealTotalFee 订单总价
	 * @param itemCodes    订单商品id串,用半角逗号分隔
	 * @param sellerIds    订单商品卖家qq号串,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param categorys    订单的商品类目,用半角逗号分隔(顺序跟itemCodes一一对应)
	 * @param supportcods  是否支持cod	串, 1=支持	2=不支持 ,用半角逗号分隔，例如1,2,1(顺序跟itemCodes一一对应)
	 * @return
	 */
	@Override
	public Vector<PayRecommend> getPayRecommends(String mk, long wid, int userType, long dealTotalFee, String itemCodes, String sellerIds, String categorys, String supportcods) {
		System.out.println("DealPromoteBizImpl.getPayRecommends()");
		Log.run.debug("wid:"+wid+";userType:"+userType+";dealTotalFee："+dealTotalFee+";itemCodes:"+itemCodes+";sellerIds:"+sellerIds+";categorys:"+categorys+";:");
		
		Vector<PayRecommend> payRecommends = new Vector<PayRecommend>();
		GetPayRecommendListResp recommendListResp = PayRecommendClient.initPayTypeInfo(mk, wid, userType, dealTotalFee, itemCodes, sellerIds, categorys, supportcods);
		if (recommendListResp != null && recommendListResp.getPayRecommendList()!= null && !recommendListResp.getPayRecommendList().isEmpty()){
			payRecommends = recommendListResp.getPayRecommendList();
		}
		return payRecommends;
	}

	// 货到付款服务费
	void getCodFee(DealFavorVo favorVo, DealPromotePo dealPo) {
		
		for (TradeFavorVo tradeFavorVo : favorVo.getDealList()){
			Vector<ShipCalcVo> shipOpts = tradeFavorVo.getShipCalcInfos();
			if (tradeFavorVo.getPayOnReceive() == 1 && shipOpts != null && shipOpts.size() > 0) {
				// 取itemCode
				String itemCode = "";
				if (tradeFavorVo.getItemPromoteResult() != null && tradeFavorVo.getItemPromoteResult().size() > 0) {
					
					ItemPromoteResultVo itemResult = tradeFavorVo.getItemPromoteResult().get(0);
					if (itemResult != null){
						itemCode = itemResult.getItemId();
					}
				}
				
				ShipCalcVo shipCalcVo = shipOpts.get(0);
				if (shipCalcVo != null
						&& (shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_ZJS
								|| shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_SF
								|| shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_YS 
								|| shipCalcVo.getMailType() == DealConstant.MAIL_TYPE_COD_YT)) {
					long pkgCmdyTotalFee = tradeFavorVo.getTradeFee();
					long pkgShipFee = 0;
					if (tradeFavorVo.getPostStat() == 0){
						pkgShipFee = shipCalcVo.getMailFee();
					}
					long pkgDealTotalFee = CmdyUtil.getCodFee2(itemCode, tradeFavorVo.getSellerUin(), pkgCmdyTotalFee + pkgShipFee, favorVo);// + shipCalcVo.getFee());
					Log.run.info("cart codFee : pkgDealTotalFee= "+pkgDealTotalFee+" --pkgCmdyTotalFee:"+pkgCmdyTotalFee);
					shipCalcVo.setFee(pkgDealTotalFee - pkgCmdyTotalFee);
					shipCalcVo.setName("货到付款: ￥" + DispFormater.priceDispFormater(shipCalcVo.getFee()) + "元");
				}
			}
		}
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		DealPromoteBizImpl biz = new DealPromoteBizImpl();
		long wid = 3115117569l;
		String sk = "za08FEA78D";
		String gid = "";
		int scene = 0;
		int isCanEdit = 1;
		int bid = 0;
		String sid = "";
		int reqSource = 2;
		int orderFrom = 10;
		String commlist = "60318D9700000000040100003B1F0EAE,自定义项:日光色,1,140741701800561";
		double random = Math.random();
		String openid = "";
		int useRedOackage = 1;
		long versionCode = 318;
		String dealStrs = "{\"order\":[{\"shopId\":\"2719435006\",\"orderId\":\"01\",\"promotionId\":\"\",\"shopCouponId\":\"\",\"wgCouponId\":\"\",\"balanceId\":\"\",\"shopbalanceId\":\"\",\"shipcardId\":\"\",\"comboId\":\"\",\"sceneId\":\"\",\"payOnReciew\":\"0\",\"onpromotion\":\"0\",\"sellerpropery\":\"0\",\"commodity\":[{\"cid\":\"FE4817A2000000000401000044210F98\",\"stock\":\"颜色:粉 + 白\",\"skuid\":\"140743362705823\",\"num\":\"1\",\"priceId\":0,\"hongbao\":[]}]}],\"sn\":\"1423212121076\"}";
//		biz.takeCheapQueryPromote(wid, sk, gid,random);
		TakeCheapDealFavorVo dealVo = new TakeCheapDealFavorVo();
		dealStrs = biz.takeCheapOrderView(dealVo, wid, sk, scene, isCanEdit, bid, sid, commlist,openid,useRedOackage);
		dealVo = biz.takeCheapQuery(dealVo, wid, sk, reqSource, orderFrom, sid, dealStrs,openid,useRedOackage,versionCode);
		String result = JSONSerializer.toJSON(dealVo).toString();
		System.out.println(result);
	}
}
