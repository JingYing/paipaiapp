package com.qq.qqbuy.dealpromote.action;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.RegionUtil;
import com.qq.qqbuy.common.RegionUtil.Node;
import com.qq.qqbuy.common.SpringHelper;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.deal.util.DealConstant;
import com.qq.qqbuy.deal.util.DealUtil;
import com.qq.qqbuy.dealpromote.biz.DealPromoteBiz;
import com.qq.qqbuy.dealpromote.po.*;
import com.qq.qqbuy.recvaddr.biz.RecvAddrBiz;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;
import com.qq.qqbuy.shippingFee.biz.SFBiz;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.shippingFee.po.Rule;
import com.qq.qqbuy.shippingFee.po.SFTplDetailInfo;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/********************************************************************
 * 
 * @author candeladiao
 * 2013-09-12
 */

/***********************
 * 测试数据**********************************
 * dealList=卖家QQ~是否参加店铺促销~套餐ID~是否货到付款~是否网购卖家~优惠1类型;优惠1ID;优惠2ID$优惠2类型;优惠3ID~
 * 商品1ID
 * ;商品1数量;商品1价格优惠类型;商品1库存属性;商品1使用红包ID$商品2ID;商品2数量;商品2价格优惠类型;商品2库存属性;商品2使用红包ID,
 * *********************IDC******************************************
 * http://candeladiao
 * -pc.tencent.com:8080/webapp_paipai/api/promote/queryPromote.xhtml?
 * dealList=1970733817
 * ~0~~0~1~~F902777500000000040100001A6362E3;1;0;%E5%86%85%E8%
 * A3%A4%E5%B0%BA%E7%A0
 * %81:%E5%9D%87%E7%A0%81|%E9%A2%9C%E8%89%B2%E5%88%86%E7%B1%BB
 * :%E5%A4%A9%E8%93%9D%E8%89%B2%20;%20 &mk=1-4E3BAACBC6BB&uk=
 * dWluPTI4OTMwMzcyMyZsc2tleT0wMDAzMDAwMDUwNmVhZWFkNGVlNTk2OWE2NzJjNDk4NjgzMzc0YTdjNjcyMmIwNjNhOTM4OGRiNDQ3ZDhmNzJiMmFlOTY5NDYwN2NiYTdkMDExNTg5ZTNmJnR5cGU9MQ
 * ==
 */

public class ApiDealPromoteAction extends PaipaiApiBaseAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * useRedPackage=1表示使用红包进行批价，useRedPackage=0表示不用红包批价
	 */
	private int useRedPackage = 0;

	private int ver = 0;

	private String dealList; // 分单后的包裹列表信息:
	
	private int sellerPayFreight = -1; //运费模板ID。大于等于10，为运费模板，其他为免运费
	
	private String cityId ; //商品的cityId 
	
	private int adid = -1;//收货地址ID

	private int reqsource = 0; // 下单场景
	
	private int scene = 1;//下单场景用于购买时的一些操作场景1：初次进入场景2：切换收货地址场景3：切换优惠活动场景4：切换支付方式场景;
	
	private int payType = 0;//支付方式
	
	private DealPromoteBiz dealPtBiz;
	
	private long dealTotalFee;
	private String itemCodes;
	private String categorys;
	private String supportcods;
	private String sellerIds;
	
	/**
	 * 支付路由
	 * 虚拟商品不走支付桥
	 * curl "http://172.25.73.14:8082/api/item/getItemBasic.xhtml?versionCode=317&ic=8F3F1A58000000000401000043BF583F&longitude=0&mk=4ee1c9531a91d9844746b136afa8a40ee678efdd&mt=android&mk2=862873024677359-30654AF2E2YV&pinid=&dap=&latitude=0&channel=%7B%22gdt_vid%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22market%22%3A%22f_baidu%22%2C%22pprd_p%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%2C%22wid%22%3A%22%22%7D&appID=1"
	 * 
	 * curl "http://172.25.73.14:8082/api/promote/payRecommendsPromote.xhtml?cityId=40000&versionCode=318&useRedPackage=1&ver=1&longitude=117.194811&sellerPayFreight=10&mk=7f526ead4745b762439d9db0a81f47fced978946&appToken=wtOwLs3k5d2DQ1W59lBzIWVz9ugQCUQiW%2FXJM9CtF5RbPZdzw9E9KdP8iOzdVMmmcco0an%2Fw93n85t8FjaG1j7hN5HaIHXSjyzehG7EoDX2Vd1jLCHjbHGGaMzOmvbVISoly%2BPuDAGjMxnkgimBcehiJdw%2Bax%2BrK8GwGXE3MgnI%2BCDv3EjEbJQMylfGVNLX3lE4QqunJznLwN9RsJN%2BpozB25QU9H08idcf5zRANXZop4bIvZPGZbabHVHezkjRxDQwptf9Tk71oL%2BpYm9HV6EDIEfC1PLDTSd6S1NrmlyeSA%3D&mt=android&mk2=865852024083667-a0934714a1fe&dealList=1000%7E36333301%7E2%7E0%7E0%7E0%7E%7EF5662A0200000000040100004B29F232%3B1%3B0%3B%3B+&reqsource=1&pinid=&payType=0&scene=1&latitude=39.117985&channel=%7B%22gdt_vid%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22market%22%3A%22f_default%22%2C%22pprd_p%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%2C%22wid%22%3A%222267985266%22%7D&appID=1"
	 * 
	 * curl "http://172.25.73.14:8082/api/promote/payRecommendsPromote.xhtml?cityId=2802&versionCode=318&ver=1&useRedPackage=1&sellerPayFreight=11&longitude=113.395671&mk=349F421B-705C-4323-B9D2-7B32DEBEB819&sence=1&appToken=wtXq%2B78x2BCgNWU%2F2uQCJ4thD9za0gEOVLxYgw5A9K8FxASJTQj7RLjeQmuFNNpr87dtB2FjiNHIqA6AWekOFIq2BEodwwn3if3R3V7TDxVc2NyWFPsjMalSkMz6wNoLpL9zIKLuXAYAWP9%2BLXg1aiCqmqCLqTGZLnReiXLIL7OWQC5SUn9mbT5Boz9Oovqnkih3UQfAOFW4Vsz3F5EL%2F5p7%2BxOMbPm1cXxf8txbQzySmL3eHpiA9KLg1lrH4VNio%2F%2F1%2FMdDD5o9fx7vm9ZgcjqKM2%2BydqvgLP&mt=ios&mk2=24c7c89b44dc9890770b4db808fb7ec3008c85fb&dealList=1000%7E2556698868%7E%7E1%7E0%7E0%7E%7EF420649800000000040100003B4E4AEA%3B1%3B0%3B%E9%A2%9C%E8%89%B2%3A%E5%BF%AB%E6%84%9F%E6%B6%A6%E6%BB%91%E6%B6%B2%E9%80%81%E8%B6%85%E8%96%842%E5%8F%AA%7C%E5%8A%9F%E6%95%88%3A%E6%88%BF%E4%BA%8B%E6%B6%A6%E6%BB%91+%E5%BF%AB%E6%84%9F%E4%B8%8D%E6%96%AD%3B+%3B&reqsource=1&adid=0&pinid=&latitude=34.793659&channel=%7B%22market%22%3A%22%7B%0A%7D%22%2C%7B%0A%7D%2C%22wid%22%3A%221517760123%22%7D&appID=1"
	 * 
	 * @return
	 */
	public String payRecommends(){
		Log.run.debug("ApiDealPromoteAction.payRecommendsPromote()1");
		JsonOutput out = new JsonOutput();
		try {

			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if (getWid() == 0 || StringUtil.isEmpty(dealList)) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				return RST_FAILURE;
			}
			//对传过来的dealList进行处理组成批价信息
			DealPromotePo dealPo = DealPromotePo.createInstance(getWid(), dealList, ver,useRedPackage);
			if (dealPo.getErrorCode() == 0) {
				
				Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>() ;
				//获取立即购买商品的编号
				String itemCode = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemId() ;
				//获取商品的信息
				FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
				
				if ( -1 != adid && -1 != sellerPayFreight && !StringUtils.isEmpty(cityId))  {
					//计算运费
					// 查询收货地址列表
					//从spring 容器中获取recvAddrBiz对象
					RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
					// 查询某QQ号收货地址列表（idl接口）
					RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
					//如果查出收货地址列表为异常则返回错误
					if (addrList == null || addrList.getAddressList() == null
							|| addrList.getAddressList().size() <= 0) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
						return RST_FAILURE;				
					}
					//从列表中取出要选择的（adid）收货地址
					ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
							addrList.getAddressList(), adid);
					//如果异常则返回错误
					if (defaultAddr == null) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
						return RST_FAILURE;					
					}
					
					if (sellerPayFreight < 10 ) {
						//如果为2表示买家承担运费。且运费计算方式不支持运费模版
						if (sellerPayFreight == 2) {
							
							//组合该商品的各运费的价格
							if (resp.getItemPo().getOItemBase().getEmsMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, resp.getItemPo().getOItemBase().getEmsMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getExpressMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, resp.getItemPo().getOItemBase().getExpressMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getNormalMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, resp.getItemPo().getOItemBase().getNormalMailPrice())); 
							}							
						} else {
							shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_FREE, 0L)); 

						}
					} else {//如果有模版
						//a获取商品数量
						Long itemNum = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemCnt() ;
						//收货地区的编号
						Long regionId = defaultAddr.getRegionId() ;
						Map<String,Rule> proviceMap = new HashMap<String,Rule>() ;
						//获取spring中sfBiz对象
						SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
						//根据模版查出该模版的费用
						SFTplDetailInfo detail = sfBiz.getSFTpl(dealPo.getDealPromoteList().get(0).getSellerUin(), sellerPayFreight);
						//获取到各目的地的费用规则
						for (int i= 0; i<detail.getRules().size();i++) {
							Rule rule = detail.getRules().get(i) ;
							for(String proviceName : rule.getRegion()) {
								proviceMap.put(proviceName, rule) ;
							}
						}
						Rule priceRule = new Rule() ;
						//解析地域xml
						Node node = RegionUtil.RegionNodeMap.get(String.valueOf(regionId));
						boolean goOn = false ;
						//获取解析的地域id与卖家在同一个城市 （同城）
						if (node.getCity1Id().equals(cityId)) {
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_ONE_CITY) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						} 
						//非同城目的地处的规则
						if (!goOn && proviceMap.keySet().contains(node.getProvinceName())){
							priceRule = proviceMap.get(node.getProvinceName()) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						}
						//如果目的没有获取到收费规则则走默认
						if (!goOn){
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_DEFAULT) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}						
						}					
					}
				}
				//下单场景。如果是1表示第一次批价
				if (this.reqsource == 1)
					dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM);
				else
					dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
				//进行批价
				DealFavorVo dealVo = dealPtBiz.queryPromote(dealPo, null, getSk(), getMk());// "@UVdu8Mcoc",// getMk());//
				dealVo.getDealList().get(0).setShipCalcInfos(shipCalcInfos) ;																		

				setErrCodeAndMsg(dealVo.getErrorCode(), dealVo.getErrorMsg());
				/**
				 * 获取推荐支付方式列表,add by wanghao start
				 */
//				String appToken = getAppToken();
				/**
				 * 用户类型
				 * 1=QQ
				 * 2=WX
				 * 3=JD
				   1、qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿)为1
				   2、wx账号非绑定qq(login_type=wx切uin>39亿)
				   3、JD账号
				 */
				int uType = 1;
				long uin = getWid();
				if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_QQ) || getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WT)){
					uType = 1;
				}
				if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WX)){
					if(uin < 3900000000L){
						uType = 1;
					}else{
						uType = 2;
					}
				}
				if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_JD)){
					uType = 3;
				}
				long dealTotalFee = dealVo.getTotalPrice();
				String itemCodes = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemId();
				String sellerIds = dealVo.getDealList().get(0).getSellerUin()+"";
				String categorys = "1";
				long payOnReceive = dealVo.getDealList().get(0).getPayOnReceive();
				int flag = (payOnReceive == 0) ? 2: 1;
				String supportcods = flag+"";
				Vector<PayRecommend> payRecommendList = dealPtBiz.getPayRecommends(getMk(),uin,uType,dealTotalFee,itemCodes,sellerIds,categorys,supportcods);
				JsonParser jsonParser = new JsonParser();
				out.setData(jsonParser.parse(new Gson().toJson(payRecommendList)));
			} else {
				out.setErrCode(dealPo.getErrorCode());
				out.setMsg(dealPo.getErrorMsg());
				Log.run.error("ApiDEalPromoteAction#newQuery1", new Exception(dealPo.getErrorMsg()));
			}
		} catch (BusinessException e) {
			out.setErrCode(e.getErrCode());
			out.setMsg(e.getErrMsg());
			Log.run.error("ApiDealPromoteAction#takeCheapQuery", e);
		} catch (Exception e) {
			Log.run.error("ApiDealPromoteAction#takeCheapQuery", e);
			out.setErrCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
			out.setMsg(e.getMessage());
		} 
		
		return doPrint(out.toJsonStr());
	}
	
	
	
	public String payRecommends1(){
		JsonOutput out = new JsonOutput();
		try {

			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			/**
			 * 用户类型
			 * 1=QQ
			 * 2=WX
			 * 3=JD
			   1、qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿)为1
			   2、wx账号非绑定qq(login_type=wx切uin>39亿)
			   3、JD账号
			 */
			int uType = 1;
			long uin = getWid();
			if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_QQ) || getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WT)){
				uType = 1;
			}
			if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_WX)){
				if(uin < 3900000000L){
					uType = 1;
				}else{
					uType = 2;
				}
			}
			if(getAppToken().startsWith(TokenUtil.LOGIN_TYPE_JD)){
				uType = 3;
			}
			Vector<PayRecommend> payRecommendList = dealPtBiz.getPayRecommends(getMk(),uin,uType,dealTotalFee,itemCodes,sellerIds,categorys,supportcods);
			out.setData(new JsonParser().parse(new Gson().toJson(payRecommendList)));
		} catch (BusinessException e) {
			out.setErrCode(e.getErrCode());
			out.setMsg(e.getErrMsg());
			Log.run.error("ApiDealPromoteAction#payRecommends1", e);
		} catch (Exception e) {
			Log.run.error("ApiDealPromoteAction#payRecommends1", e);
			out.setErrCode(ErrConstant.ERRCODE_PROMOTE_OTHER_ERROR);
			out.setMsg(e.getMessage());
		} 
		
		return doPrint(out.toJsonStr());
	}
	
	
	public String query() {
		try {
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if (getWid() == 0 || StringUtil.isEmpty(dealList)) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				return RST_FAILURE;
			}
			//对传过来的dealList进行处理组成批价信息
			DealPromotePo dealPo = DealPromotePo.createInstance(getWid(), dealList, ver,useRedPackage);
			if (dealPo.getErrorCode() == 0) {
				
				Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>() ;
				if ( -1 != adid && -1 != sellerPayFreight && !StringUtils.isEmpty(cityId))  {
					//计算运费
					// 查询收货地址列表
					//从spring 容器中获取recvAddrBiz对象
					RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
					// 查询某QQ号收货地址列表（idl接口）
					RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
					//如果查出收货地址列表为异常则返回错误
					if (addrList == null || addrList.getAddressList() == null
							|| addrList.getAddressList().size() <= 0) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
						return RST_FAILURE;				
					}
					//从列表中取出要选择的（adid）收货地址
					ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
							addrList.getAddressList(), adid);
					//如果异常则返回错误
					if (defaultAddr == null) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
						return RST_FAILURE;					
					}
					if (sellerPayFreight < 10 ) {
						//如果为2表示买家承担运费。且运费计算方式不支持运费模版
						if (sellerPayFreight == 2) {
							//获取立即购买商品的编号
							String itemCode = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemId() ;
							//获取商品的信息
							FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
							//组合该商品的各运费的价格
							if (resp.getItemPo().getOItemBase().getEmsMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, resp.getItemPo().getOItemBase().getEmsMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getExpressMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, resp.getItemPo().getOItemBase().getExpressMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getNormalMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, resp.getItemPo().getOItemBase().getNormalMailPrice())); 
							}							
						} else {
							shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_FREE, 0L)); 

						}
					} else {//如果有模版
						//a获取商品数量
						Long itemNum = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemCnt() ;
						//收货地区的编号
						Long regionId = defaultAddr.getRegionId() ;
						Map<String,Rule> proviceMap = new HashMap<String,Rule>() ;
						//获取spring中sfBiz对象
						SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
						//根据模版查出该模版的费用
						SFTplDetailInfo detail = sfBiz.getSFTpl(dealPo.getDealPromoteList().get(0).getSellerUin(), sellerPayFreight);
						//获取到各目的地的费用规则
						for (int i= 0; i<detail.getRules().size();i++) {
							Rule rule = detail.getRules().get(i) ;
							for(String proviceName : rule.getRegion()) {
								proviceMap.put(proviceName, rule) ;
							}
						}
						Rule priceRule = new Rule() ;
						//解析地域xml
						Node node = RegionUtil.RegionNodeMap.get(String.valueOf(regionId));
						boolean goOn = false ;
						//获取解析的地域id与卖家在同一个城市 （同城）
						if (node.getCity1Id().equals(cityId)) {
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_ONE_CITY) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						} 
						//非同城目的地处的规则
						if (!goOn && proviceMap.keySet().contains(node.getProvinceName())){
							priceRule = proviceMap.get(node.getProvinceName()) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						}
						//如果目的没有获取到收费规则则走默认
						if (!goOn){
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_DEFAULT) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}						
						}					
					}
				}
				//下单场景。如果是1表示第一次批价
				if (this.reqsource == 1)
					dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM);
				else
					dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
				//进行批价
				DealFavorVo dealVo = dealPtBiz.queryPromote(dealPo, null, getSk(), getMk());// "@UVdu8Mcoc",// getMk());//
				dealVo.getDealList().get(0).setShipCalcInfos(shipCalcInfos) ;																		

				setErrCodeAndMsg(dealVo.getErrorCode(), dealVo.getErrorMsg());
				/**
				 * 获取推荐支付方式列表,add by wanghao start
				 */
//				String appToken = getAppToken();
				/**
				 * 用户类型
				 * 1=QQ
				 * 2=WX
				 * 3=JD
				   1、qq账号及绑定了qq账号的微信账号(login_type=wx切uin<39亿)为1
				   2、wx账号非绑定qq(login_type=wx切uin>39亿)
				   3、JD账号
				 *
				int uType = 1;
				long uin = getWid();
				if(appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ) || appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)){
					uType = 1;
				}
				if(appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)){
					if(uin < 3900000000L){
						uType = 1;
					}else{
						uType = 2;
					}
				}
				if(appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)){
					uType = 3;
				}
				long dealTotalFee = dealVo.getTotalPrice();
				String itemCodes = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemId();
				String sellerIds = dealVo.getDealList().get(0).getSellerUin()+"";
				String categorys = "";
				long payOnReceive = dealVo.getDealList().get(0).getPayOnReceive();
				int flag = (payOnReceive == 0) ? 2: 1;
				String supportcods = flag+"";
				Vector<PayRecommend> payRecommendList = dealPtBiz.getPayRecommends(getMk(),uin,uType,dealTotalFee,itemCodes,sellerIds,categorys,supportcods);
				dealVo.setPayRecommendList(payRecommendList);
				 */
				// add for dealList output
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "errorCode", "errorMsg" };
				jsoncfg.setExcludes(excludes);
				dealList = JSONSerializer.toJSON(dealVo, jsoncfg).toString();
				return RST_SUCCESS;
			} else {
				Log.run.error("ApiDEalPromoteAction#newQuery1", new Exception(dealPo.getErrorMsg()));
				setErrCodeAndMsg(dealPo.getErrorCode(), dealPo.getErrorMsg());
				return RST_FAILURE;
			}
		} catch (BusinessException e) {
			Log.run.error("ApiCartAction#dealPromote", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiCartAction#dealPromote", e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		} finally {
			// 设置dtag
			result.setDtag(this.getDtag());
		}
	}


	/**
	 * 立即购买的新的接口
	 * @return
	 */
	public String newQuery(){
		try {
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}

			// 参数检查
			if (getWid() == 0 || StringUtil.isEmpty(dealList)||(scene!=DealConstant.DEAL_SCENE_INITIALIZE&&scene!=DealConstant.DEAL_SCENE_CHANGERECVADDR&&scene!= DealConstant.DEAL_SCENE_CHANGEPROMOTIONS&&scene!=DealConstant.DEAL_SCENE_CHANGEPAYTYPE)) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				return RST_FAILURE;
			}
			//对传过来的dealList进行处理组成批价信息
			DealPromotePo dealPo = DealPromotePo.createInstance(getWid(), dealList, ver,useRedPackage);
			//返回的数据结构体
			DealFavorVo dealVo = new DealFavorVo();
			if (dealPo.getErrorCode() == 0) {
				
				Vector<ShipCalcVo> shipCalcInfos = new Vector<ShipCalcVo>() ;
				if ( -1 != sellerPayFreight && !StringUtils.isEmpty(cityId))  {
					//计算运费
					// 查询收货地址列表
					//从spring 容器中获取recvAddrBiz对象
					RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
					// 查询某QQ号收货地址列表（idl接口）
					RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
					//如果查出收货地址列表为异常则返回错误
					if (addrList == null || addrList.getAddressList() == null
							|| addrList.getAddressList().size() <= 0) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
						return RST_FAILURE;				
					}
					//Log.run.info("defaultAddrdefaultAddrdefaultAddr++"+addrList.getAddressList().size()+addrList.getAddressList().get(0).getAddress());
					//从列表中取出要选择的（adid）收货地址
					ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
							addrList.getAddressList(), adid);
					//如果异常则返回错误码
					if (defaultAddr == null) {
						setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
						return RST_FAILURE;					
					}
					if (sellerPayFreight < 10 ) {
						//如果为2表示买家承担运费。且运费计算方式不支持运费模版
						if (sellerPayFreight == 2) {
							//获取立即购买商品的编号
							String itemCode = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemId() ;
							//获取商品的信息
							FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
							//组合该商品的各运费的价格
							if (resp.getItemPo().getOItemBase().getEmsMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, resp.getItemPo().getOItemBase().getEmsMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getExpressMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, resp.getItemPo().getOItemBase().getExpressMailPrice())); 
							}
							if (resp.getItemPo().getOItemBase().getNormalMailPrice() > 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, resp.getItemPo().getOItemBase().getNormalMailPrice())); 
							}							
						} else {
							shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_FREE, 0L)); 

						}
					} else {//如果有模版
						//a获取商品数量
						Long itemNum = dealPo.getDealPromoteList().get(0).getItemPromoteList().get(0).getItemCnt() ;
						//收货地区的编号
						Long regionId = defaultAddr.getRegionId() ;
						Map<String,Rule> proviceMap = new HashMap<String,Rule>() ;
						//获取spring中sfBiz对象
						SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
						//根据模版查出该模版的费用
						SFTplDetailInfo detail = sfBiz.getSFTpl(dealPo.getDealPromoteList().get(0).getSellerUin(), sellerPayFreight);
						//获取到各目的地的费用规则
						for (int i= 0; i<detail.getRules().size();i++) {
							Rule rule = detail.getRules().get(i) ;
							for(String proviceName : rule.getRegion()) {
								proviceMap.put(proviceName, rule) ;
							}
						}
						Rule priceRule = new Rule() ;
						//解析地域xml
						Node node = RegionUtil.RegionNodeMap.get(String.valueOf(regionId));
						boolean goOn = false ;
						//获取解析的地域id与卖家在同一个城市 （同城）
						Log.run.info("node.getCity1Id()"+node.getCity1Id()+"cityId:"+cityId);
						if (node.getCity1Id().equals(cityId)) {
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_ONE_CITY) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						} 
						//非同城目的地处的规则
						if (!goOn && proviceMap.keySet().contains(node.getProvinceName())){
							priceRule = proviceMap.get(node.getProvinceName()) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}
							if (shipCalcInfos.size() > 0) {
								goOn = true ;
							}
						}
						//如果目的没有获取到收费规则则走默认
						if (!goOn){
							priceRule = proviceMap.get(ShipCalcVo.RULE_TYPE_DEFAULT) ;
							if (priceRule!=null && priceRule.getPriceEms() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EMS, priceRule.getPriceEms() 
										+ priceRule.getPriceEmsAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceExpress() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_EXPRESS, priceRule.getPriceExpress() 
										+ priceRule.getPriceExpressAdd() * (itemNum - 1))); 
							}
							if (priceRule!=null && priceRule.getPriceNormal() >= 0) {
								shipCalcInfos.add(ShipCalcVo.getShipCalcInfo(DealConstant.SHIPCALC_TYPE_NORMAL, priceRule.getPriceNormal() 
										+ priceRule.getPriceNormalAdd() * (itemNum - 1))); 
							}						
						}					
					}
					//下单场景。如果是1表示第一次批价
					if (this.reqsource == 1)
						dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_FIRST_ORDER_CONFIRM);
					else
						dealPo.setReqSource(BizConstant.DEAL_PROMOTE_FROM_ORDER_CONFIRM);
					//进行批价
					dealVo = dealPtBiz.queryPromote(dealPo, null, getSk(), getMk());// "@UVdu8Mcoc",// getMk());//
					dealVo.getDealList().get(0).setShipCalcInfos(shipCalcInfos) ;	
					//如果不为空则将地址信息设置到返回体
					//如果是初次进入时
					Vector<ReceiverAddress> addressList = new Vector<ReceiverAddress>();
					String MobileNo = defaultAddr.getMobile();
					if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
						defaultAddr.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
					}
					addressList.add(defaultAddr);
					dealVo.setAddressList(addressList);
				/*	if(scene==DealConstant.DEAL_SCENE_INITIALIZE){
						dealVo.setAddressList(addressList);
					 }*/
					//如果是切换支付方式时则要判断是否是货到付款
					//获取spring中sfBiz对象
					SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
					//货到付款时根据批价结果计算货到付款服务费、运费和优惠费
					Vector<CodShipPo> sftplDetail = new Vector<CodShipPo>();
					//如果是货到付款
					if(payType == 1){
						long totalPrice = dealVo.getTotalPrice();
						long totalCodFee = 0;
						long totalFavFee = 0;
						long totalMailFee = 0;
						if(!dealVo.getDealList().isEmpty()){
							Vector<TradeFavorVo> dealList = dealVo.getDealList();
							for(TradeFavorVo deal : dealList){
								Vector<ItemPromoteResultVo> promoteResults = deal.getItemPromoteResult();
								long dealTotalFee = deal.getTradeFee();
								int isMailFree = 0;
								Vector<FavorExVo> shopPromotions = deal.getShopPromotions();
								if(!shopPromotions.isEmpty()){
									for(FavorExVo shopPromotion : shopPromotions){
										long checkStat = shopPromotion.getCheckStat();
										//当前促销活动选中并且促销活动为包邮活动
										if(checkStat > 0 && shopPromotion.getPostStat() > 0){
											isMailFree = 1;
											break;
										}
									}
								}
								for(ItemPromoteResultVo promoteResult : promoteResults){
									String ci = promoteResult.getItemId();
									long buyNum = promoteResult.getNum();
									CodShipPo codShipPo = sfBiz.calcCodShipfeeWithCgi(defaultAddr.getAddressId(), ci, dealTotalFee, buyNum, getWid(), getSk(), isMailFree);
									deal.setTradeFee(codShipPo.getFinalFee());
									sftplDetail.add(codShipPo);
									totalCodFee += codShipPo.getCodCountFee();
									totalMailFee += codShipPo.getFee();
									totalFavFee += codShipPo.getFavFee();
								}
							}
						}
						//货到付款购物车最终货款
						totalPrice += (totalCodFee + totalMailFee - totalFavFee);
						dealVo.setTotalPrice(totalPrice);
						dealVo.setTotalCodFee(totalCodFee);
						dealVo.setTotalMailFee(totalMailFee);
						dealVo.setTotalFavFee(totalFavFee);
						dealVo.setSftplDetail(sftplDetail);
					}else {//在线支付订单结果计算邮费信息long totalPrice = favorVo.getTotalPrice();
						long totalPrice = dealVo.getTotalPrice();
						long totalMailFee = 0;
						if(!dealVo.getDealList().isEmpty()){
							Vector<TradeFavorVo> dealList = dealVo.getDealList();
							for (TradeFavorVo deal : dealList) {
								boolean isMailFree = false;
								Vector<FavorExVo> shopPromotions = deal.getShopPromotions();
								if(!shopPromotions.isEmpty()){
									for(FavorExVo shopPromotion : shopPromotions){
										long checkStat = shopPromotion.getCheckStat();
										//当前促销活动选中并且促销活动为包邮活动
										if(checkStat > 0 && shopPromotion.getPostStat() > 0){
											isMailFree = true;
											break;
										}
									}
								}
								if (scene == DealConstant.DEAL_SCENE_CHANGEPROMOTIONS){//在线支付切换优惠活动传参带有邮费信息
									if (!isMailFree && dealPo != null && !dealPo.getMailInfos().isEmpty()){
										Map<Long, ShipCalcVo> mailInfos = dealPo.getMailInfos();
										ShipCalcVo shipCalcVo = mailInfos.get(deal.getDealId());
										if (null != shipCalcVo){
											long fee = shipCalcVo.getFee();
											totalMailFee += fee;
											deal.setTradeFee(deal.getTradeFee()+fee);
										}
									}
								}else{//其他场景在线支付初始化邮费信息
									Vector<ShipCalcVo> shipCalcInfoss = deal.getShipCalcInfos();
									if (!isMailFree && !shipCalcInfoss.isEmpty()){
										long fee = shipCalcInfoss.get(0).getFee();
										totalMailFee += fee;
										deal.setTradeFee(deal.getTradeFee()+fee);
									}
								}
							}
						}
						totalPrice += totalMailFee;
						dealVo.setTotalPrice(totalPrice);
					}
				}
				setErrCodeAndMsg(dealVo.getErrorCode(), dealVo.getErrorMsg());
				// add for dealList output
				JsonConfig jsoncfg = new JsonConfig();
				String[] excludes = { "errorCode", "errorMsg" };
				jsoncfg.setExcludes(excludes);
				dealList = JSONSerializer.toJSON(dealVo, jsoncfg).toString();

				return RST_SUCCESS;
			} else {
				Log.run.error("ApiDealPromoteAction#newQuery1", new Exception(dealPo.getErrorMsg()));
				setErrCodeAndMsg(dealPo.getErrorCode(), dealPo.getErrorMsg());
				return RST_FAILURE;
			}
		} catch (BusinessException e) {
			Log.run.error("ApiDealPromoteAction#newQuery2", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiDealPromoteAction#newQuery3", e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		} finally {
			// 设置dtag
			result.setDtag(this.getDtag());
		}
	}
	/**
	 * 拍便宜批价参数，团购信息串
	 */
	private String commlist = "";
	/**
	 * 非初始化场景的订单串，格式为：shopId~orderId~promotionId~shopCouponId~wgCouponId~balanceId~shipcardId~comboId~sceneId~payOnReciew~onpromotion~sellerpropery~cid$stock$skuid$num$priceId$hongbao
	 */
	private String dealStrs = "";
	/**
	 * 满团人数
	 */
	private int groupnum;
	/**
	 * 活动期数ID
	 */
	private int activenum;
	/**
	 * 是否是挤爆团
	 */
	private int isJBT;
	/**
	 * 吆喝信息
	 */
	private String tuanMsg;
	/**
	 * 是否大V团，1：是 0:否
	 */
	private int isDav = 0;
	/**
	 * 大V团参团接口返回值
	 */
	private String promoteKey = "";
	/**
	 * 团id
	 */
	private String gid = "";
	/**
	 * 是否团团降订单 1：是，其他值不是
	 */
	private int tuan = 1;
	/**
	 * 是否可编辑
	 */
	private int isCanEdit = 1;
	private int bid = 0;
	private int sceneId = 0;
	/**
	 * 拍便宜购买的接口
	 * 分场景：1、直接购买 2、开团购买 3、参团购买
	 * 直接购买：1、带sku，，propData.skuid为sku 2、不带sku，peoplenum为1
	 * 开团购买：1、带sku，groupnum是leastPeopNum，propData.skuid为sku 2、不带sku，peoplenum为leastPeopNum
	 * 参团购买：1、带sku 2、不带sku
	 * commlist 商品信息串:itemCode1,sku1,buyNum1,skuId1$itemCode2,sku2,buyNum2,skuId2
	 * groupnum 参团人数，直接购买时为1，开团购买时为leastPeopNum，参团购买时为参团人数
	 * payType 支付方式：1、在线支付 2、货到付款
	 * isDav 身份：1、vip isdav=1
	 * tuan=1&
	 * commlist=0EF6FE4B00000000000000000BEC374A,颜色:巧克力色,1,140737488480373&
	 * groupnum=2&
	 * _wv=1&
	 * speak=赶紧买~别等团挤爆了怪我没通知。
	 * http://b.paipai.com/tws/price/query?callback=__cbQueryPrice&info=%7B%22order%22%3A%5B%7B%22shopId%22%3A%22527517507%22%2C%22orderId%22%3A%2201%22%2C%22promotionId%22%3A%22%22%2C%22shopCouponId%22%3A%22%22%2C%22wgCouponId%22%3A%22%22%2C%22balanceId%22%3A%22%22%2C%22shipcardId%22%3A%22%22%2C%22comboId%22%3A%22%22%2C%22sceneId%22%3A%220%22%2C%22payOnReciew%22%3A%220%22%2C%22onpromotion%22%3A%220%22%2C%22sellerpropery%22%3A%220%22%2C%22commodity%22%3A%5B%7B%22cid%22%3A%224347711F0000000000433A0D00014EFC%22%2C%22stock%22%3A%22%22%2C%22skuid%22%3A%22140737488494336%22%2C%22num%22%3A%221%22%2C%22priceId%22%3A0%2C%22hongbao%22%3A%5B%5D%7D%5D%7D%5D%2C%22sn%22%3A%221419840076182%22%7D&reqSource=2&src=mobile&orderFrom=10&sid=&g_tk=1925520097&g_ty=ls
	 * http://b.paipai.com/mfixupconfirm/orderview?callback=_cbOrderview&isCanEdit=1&EncryptInfo=&Token=&scene=0&bid=0&sid=&commlist=4347711F0000000000433A0D00014EFC,,1,140737488494336&r=0.9344795399811119&&g_tk=1925520097&g_ty=ls
	 * http://b.paipai.com/recvaddr/getrecvaddrV2?callback=cbDefault&r=0.906488326843828&sid=&adid=2&type=0&reg=0&r=0.34915767796337605&g_tk=1925520097&g_ty=ls
	 *	大v团在订单接口和批价接口之前调用接口获取promoteKey，供后续使用
	 *	大V团批价参数新增dealSource参数
	 *  if(isDaV == 1){
	 dealSource = '&dealSource=TuanV';
	 if(g.promoteKey != ""){
	 dealSource = '&dealSource=TuanTJV';
	 }
	 }
	 * @return
	 */
	public String takeCheapQuery(){

		try {
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			// 参数检查
			if (getWid() == 0 || StringUtil.isEmpty(commlist)||(scene!=DealConstant.DEAL_SCENE_CHANGENUMBER && scene!=DealConstant.DEAL_SCENE_INITIALIZE&&scene!=DealConstant.DEAL_SCENE_CHANGERECVADDR&&scene!= DealConstant.DEAL_SCENE_CHANGEPROMOTIONS&&scene!=DealConstant.DEAL_SCENE_CHANGEPAYTYPE)) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
				return RST_FAILURE;
			}
			// 查询收货地址列表
			RecvAddrBiz addrBiz = (RecvAddrBiz)SpringHelper.getBean("recvAddrBiz");
			RecvAddr addrList = addrBiz.listRecvAddr(getWid(), false, false, getSk());
			if (addrList == null || addrList.getAddressList() == null
					|| addrList.getAddressList().size() <= 0) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_ADDRLIST_EMPTY, "查询用户收货地址列表为空");
				return RST_FAILURE;
			}
			ReceiverAddress defaultAddr = DealUtil.getDefaultAddr(
					addrList.getAddressList(), adid);
			//如果异常则返回错误码
			if (defaultAddr == null) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_DEAL_FINDADDR_FAIL, "查找不到收货地址" + adid);
				return RST_FAILURE;
			}
			//转换commlist为订单对象
			TakeCheapCommPo po = TakeCheapCommPo.ctreateInstaance(commlist);
			if(po != null && po.getErrorCode() != 0){
				setErrCodeAndMsg(po.getErrorCode(), po.getErrorMsg());
				return RST_FAILURE;
			}
			TakeCheapDealFavorVo dealVo = new TakeCheapDealFavorVo();
			//orderFrom = 8 表示订单是直接购买，10表示开团和参团，
			//当groupId=0时表示为开团，不为0时为参团
			int orderFrom = 8;
			if(tuan == 1 && groupnum > 1){
				orderFrom = 10;
			}
			//初始化时reqSource=2,非初始化时为3。
			//初始化时要先调用订单接口获取订单信息再批价，非初始化时直接批价
			int reqSource = 2;
			//初始化时先分单
			/**
			 * 设置userAgent包含
			 */
			String openid = getPinOrId(getAppToken());
			Log.run.info("openid is : " + openid);
			/**
			 * 获取版本号,根据版本号区分是否启用优惠活动
			 */
			long version = 318;
			try {
				String versionCode = getVersionCode();
				version = Long.parseLong(versionCode);
			} catch (Exception e) {
			}
			if (scene == DealConstant.DEAL_SCENE_INITIALIZE){
				dealStrs = dealPtBiz.takeCheapOrderView(dealVo,getWid(), getSk(), sceneId, isCanEdit, bid, "", commlist,openid,useRedPackage);
				dealVo = dealPtBiz.takeCheapQuery(dealVo,getWid(), getSk(),reqSource,orderFrom,"",dealStrs,openid,useRedPackage,version);
				//初始化时获取默认收货地址的收货地址id为后续获取运费接口提供参数
				adid = Integer.parseInt(defaultAddr.getAddressId()+"");
			}else{
				if (scene != DealConstant.DEAL_SCENE_CHANGENUMBER){//商品数量变换reqSource=2
					reqSource = 3;
				}
				dealVo = dealPtBiz.takeCheapQuery(dealVo, getWid(), getSk(), reqSource, orderFrom, "", dealStrs,openid,useRedPackage,version);
			}
			Vector<ReceiverAddress> receiverAddresses = new Vector<ReceiverAddress>();
			String MobileNo = defaultAddr.getMobile();
			if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
				defaultAddr.setMobile(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
			}
			receiverAddresses.add(defaultAddr);
			dealVo.setAddressList(receiverAddresses);
			//获取商品运费信息
			SFBiz sfBiz = (SFBiz)SpringHelper.getBean("sfBiz");
			System.out.println("收货地址ID是：" + adid);
			List<ShipCalcVo> shipCalcInfos = sfBiz.getTakeCheapShipCalcInfo(getWid(),getSk(),adid,po.getItemCode(),po.getAttrStr(),po.getBuyNum());
			dealVo.getDealList().get(0).setShipCalcVos(shipCalcInfos);
			Vector<CodShipPo> sftplDetail = new Vector<CodShipPo>();
			//如果是货到付款
			if(payType == 1){
				long totalPrice = dealVo.getTotalPrice();
				long totalCodFee = 0;
				long totalFavFee = 0;
				//货到付款时根据批价结果计算货到付款服务费、运费和优惠费
				long totalMailFee = 0;
				if(!dealVo.getDealList().isEmpty()){
					Vector<TakeCheapTradeFavorVo> dealList = dealVo.getDealList();
					for(TakeCheapTradeFavorVo deal : dealList){
						Vector<TakeCheapItemPromoteResultVo> promoteResults = deal.getCommodity();
						long dealTotalFee = deal.getAmount();
						int isMailFree = 0;
						Vector<TakeCheapFavorExVo> salePromotions = deal.getSalePromotion();
						if(!salePromotions.isEmpty()){
							for(TakeCheapFavorExVo salePromotion : salePromotions){
								int selected = salePromotion.getSelected();
								//当前促销活动选中并且促销活动为包邮活动
								if(selected > 0 && salePromotion.getMailFree() > 0){
									isMailFree = 1;
									break;
								}
							}
						}
						for(TakeCheapItemPromoteResultVo promoteResult : promoteResults){
							String ci = promoteResult.getCid();
							long buyNum = promoteResult.getNum();
							CodShipPo codShipPo = sfBiz.calcCodShipfeeWithCgi(defaultAddr.getAddressId(), ci, dealTotalFee, buyNum, getWid(), getSk(), isMailFree);
							deal.setAmount(codShipPo.getFinalFee());
							sftplDetail.add(codShipPo);
							totalCodFee += codShipPo.getCodCountFee();
							totalMailFee += codShipPo.getFee();
							totalFavFee += codShipPo.getFavFee();
						}
					}
				}
				//货到付款购物车最终货款
				totalPrice += (totalCodFee + totalMailFee - totalFavFee);
				dealVo.setTotalPrice(totalPrice);
				dealVo.setTotalCodFee(totalCodFee);
				dealVo.setTotalMailFee(totalMailFee);
				dealVo.setTotalFavFee(totalFavFee);
				dealVo.setSftplDetail(sftplDetail);
			}else {//在线支付订单结果计算邮费信息
				long totalPrice = dealVo.getTotalPrice();
				long totalMailFee = 0;
				if(!dealVo.getDealList().isEmpty()){
					Vector<TakeCheapTradeFavorVo> dealList = dealVo.getDealList();
					for (TakeCheapTradeFavorVo deal : dealList) {
						boolean isMailFree = false;
						Vector<TakeCheapFavorExVo> shopPromotions = deal.getSalePromotion();
						if(!shopPromotions.isEmpty()){
							for(TakeCheapFavorExVo shopPromotion : shopPromotions){
								int checkStat = shopPromotion.getSelected();
								//当前促销活动选中并且促销活动为包邮活动
								if(checkStat > 0 && shopPromotion.getMailFree() > 0){
									isMailFree = true;
									break;
								}
							}
						}
						List<ShipCalcVo> shipCalcInfoss = deal.getShipCalcVos();
						if (!isMailFree && !shipCalcInfoss.isEmpty()){
							long fee = shipCalcInfoss.get(0).getFee();
							totalMailFee += fee;
							deal.setAmount(deal.getAmount()+fee);
						}
					}
				}
				totalPrice += totalMailFee;
				dealVo.setTotalPrice(totalPrice);
			}
			// add for dealList output
			JsonConfig jsoncfg = new JsonConfig();
			String[] excludes = { "errorCode", "errorMsg" };
			jsoncfg.setExcludes(excludes);
			dealList = JSONSerializer.toJSON(dealVo, jsoncfg).toString();
			return RST_SUCCESS;
		} catch (BusinessException e) {
			Log.run.error("ApiDealPromoteAction#takeCheapQuery", e);
			setErrCodeAndMsg4BExp(e);
			return RST_FAILURE;
		} catch (Exception e) {
			Log.run.error("ApiDealPromoteAction#takeCheapQuery", e);
			setErrCodeAndMsg(ErrConstant.ERRCODE_FIND_THROWABLE_EXP, e.getMessage());
			return RST_FAILURE;
		} finally {
			// 设置dtag
			result.setDtag(this.getDtag());
		}
	}

	/**
	 * 测试拍便宜
	 * @return
	 */
	public String testTakeCheapQuery(){
		JsonOutput out = new JsonOutput();
		JsonParser parser = new JsonParser();
		String jsonStr = "{\n" +
				"  \"errCode\":\"0\",\n" +
				"  \"retCode\":\"0\",\n" +
				"  \"msg\":\"\",\n" +
				"  \"dtag\":\"\",\n" +
				"  \"data\":{\"addressList\":[{\"address\":\"北京北京市大兴区亦庄经济开发区荣华中路19号院朝林广场A座18层\",\"addressId\":3,\"city\":\"北京市\",\"district\":\"大兴区\",\"lastModifyTime\":\"1427009753\",\"lastUsedTime\":\"1427009736\",\"mobile\":\"135****4279\",\"name\":\"王浩\",\"phone\":\"\",\"postcode\":\"\",\"province\":\"北京\",\"regionId\":41012,\"usedCount\":0}],\"dealList\":[{\"amount\":1990,\"balance\":[],\"commodity\":[{\"amount\":1990,\"cName\":\"蛟蓓 韩剧 那年冬天风在吹 宋慧乔同款 人造珍珠耳钉\",\"cid\":\"4B4BF63200000000040100002A651981\",\"comdyType\":0,\"image\":\"http://img4.paipaiimg.com/00000000/item-55064F68-4B4BF63200000000040100002A651981.0.jpg.2.jpg\",\"maxBuy\":494,\"num\":1,\"price\":1990,\"priceDesc\":\"团购价\",\"priceType\":65536,\"redsum\":0,\"stockAttr\":\"\",\"stockNum\":494}],\"errMsg\":\"\",\"errType\":0,\"fapiao\":0,\"orderId\":\"1\",\"payOnReceive\":0,\"postStat\":0,\"promotion\":1,\"salePromotion\":[{\"available\":1,\"desc\":\"本店买满1件 送20元店铺优惠券\",\"favMoney\":0,\"flag\":1,\"id\":15961478,\"indate\":\"\",\"mailFree\":0,\"minimum\":1,\"msg\":\"\",\"selected\":1,\"type\":0},{\"available\":0,\"desc\":\"本店买满99.00元 送20元店铺优惠券 [包快递]\",\"favMoney\":0,\"flag\":0,\"id\":15961479,\"indate\":\"\",\"mailFree\":1,\"minimum\":9900,\"msg\":\"还需购买79.10元商品，才能参加此优惠\",\"selected\":0,\"type\":48},{\"available\":0,\"desc\":\"本店买满168.00元 送水晶笔 送20元店铺优惠券 [包快递]\",\"favMoney\":0,\"flag\":0,\"id\":15961480,\"indate\":\"\",\"mailFree\":1,\"minimum\":16800,\"msg\":\"还需购买148.10元商品，才能参加此优惠\",\"selected\":0,\"type\":52},{\"available\":0,\"desc\":\"本店买满298.00元 送施华洛4G水晶U盘 送20元店铺优惠券 [包快递]\",\"favMoney\":0,\"flag\":0,\"id\":15961481,\"indate\":\"\",\"mailFree\":1,\"minimum\":29800,\"msg\":\"还需购买278.10元商品，才能参加此优惠\",\"selected\":0,\"type\":52}],\"shipCalcVos\":[{\"fee\":0,\"mailFee\":0,\"mailType\":0,\"name\":\"免运费\",\"payType\":0,\"type\":0}],\"shopCoupon\":[],\"shopId\":855001931,\"shopName\":\"蛟蓓饰品官方旗舰店\",\"shopProp\":32,\"shopType\":0,\"wgCoupon\":[]}],\"isHasScore\":1,\"isSupportWXPay\":1,\"problemDealList\":[],\"queryPromoteAgain\":false,\"sceneId\":0,\"sellerpropery\":2,\"sftplDetail\":[],\"totalCodFee\":0,\"totalFavFee\":0,\"totalMailFee\":0,\"totalPrice\":1990,\"version\":2}\n" +
				"}";
		JsonElement data = parser.parse(jsonStr);
		out.setData(data);
		return doPrint(data);
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public String getDealList() {
		return dealList;
	}

	public void setDealList(String dealList) {
		this.dealList = dealList;
	}

	public void setDealPtBiz(DealPromoteBiz dealPtBiz) {
		this.dealPtBiz = dealPtBiz;
	}

	public DealPromoteBiz getDealPtBiz() {
		return dealPtBiz;
	}

	public int getReqsource() {
		return reqsource;
	}

	public void setReqsource(int reqsource) {
		this.reqsource = reqsource;
	}
	
	public static void main(String[] args) {
		DealFavorVo dealVo = new DealFavorVo();
		TradeFavorVo tradeFavorVo =new TradeFavorVo();
		tradeFavorVo.getCashCoupons().add(new FavorExVo());
		tradeFavorVo.getItemPromoteResult().add(new ItemPromoteResultVo()) ;
		tradeFavorVo.getOtherCoupons().add(new FavorExVo());
		tradeFavorVo.getShipCalcInfos().add(ShipCalcVo.getShipCalcInfo(1, 500L));
		tradeFavorVo.getShopPromotions().add(new FavorExVo());
		tradeFavorVo.getShopCoupons().add(new FavorExVo());
		dealVo.getDealList().add(tradeFavorVo) ;
		JsonConfig jsoncfg = new JsonConfig();
		String[] excludes = { "errorCode", "errorMsg" };
		jsoncfg.setExcludes(excludes);
		String dealList = JSONSerializer.toJSON(dealVo, jsoncfg).toString();
		System.out.println(dealList);
	}


	public int getSellerPayFreight() {
		return sellerPayFreight;
	}

	public void setSellerPayFreight(int sellerPayFreight) {
		this.sellerPayFreight = sellerPayFreight;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public int getAdid() {
		return adid;
	}

	public void setAdid(int adid) {
		this.adid = adid;
	}


	public int getScene() {
		return scene;
	}


	public void setScene(int scene) {
		this.scene = scene;
	}


	public int getPayType() {
		return payType;
	}


	public void setPayType(int payType) {
		this.payType = payType;
	}

	public int getGroupnum() {
		return groupnum;
	}

	public void setGroupnum(int groupnum) {
		this.groupnum = groupnum;
	}

	public int getActivenum() {
		return activenum;
	}

	public void setActivenum(int activenum) {
		this.activenum = activenum;
	}

	public int getIsJBT() {
		return isJBT;
	}

	public void setIsJBT(int isJBT) {
		this.isJBT = isJBT;
	}

	public String getTuanMsg() {
		return tuanMsg;
	}

	public void setTuanMsg(String tuanMsg) {
		this.tuanMsg = tuanMsg;
	}

	public String getCommlist() {
		return commlist;
	}

	public void setCommlist(String commlist) {
		this.commlist = commlist;
	}

	public int getIsDav() {
		return isDav;
	}

	public void setIsDav(int isDav) {
		this.isDav = isDav;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public int getTuan() {
		return tuan;
	}

	public void setTuan(int tuan) {
		this.tuan = tuan;
	}

	public int getIsCanEdit() {
		return isCanEdit;
	}

	public void setIsCanEdit(int isCanEdit) {
		this.isCanEdit = isCanEdit;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getDealStrs() {
		return dealStrs;
	}

	public void setDealStrs(String dealStrs) {
		this.dealStrs = dealStrs;
	}

	public String getPromoteKey() {
		return promoteKey;
	}

	public void setPromoteKey(String promoteKey) {
		this.promoteKey = promoteKey;
	}

	public int getUseRedPackage() {
		return useRedPackage;
	}

	public void setUseRedPackage(int useRedPackage) {
		this.useRedPackage = useRedPackage;
	}



	public int getSceneId() {
		return sceneId;
	}

	public void setSceneId(int sceneId) {
		this.sceneId = sceneId;
	}



	public long getDealTotalFee() {
		return dealTotalFee;
	}



	public void setDealTotalFee(long dealTotalFee) {
		this.dealTotalFee = dealTotalFee;
	}



	public String getItemCodes() {
		return itemCodes;
	}



	public void setItemCodes(String itemCodes) {
		this.itemCodes = itemCodes;
	}



	public String getCategorys() {
		return categorys;
	}



	public void setCategorys(String categorys) {
		this.categorys = categorys;
	}



	public String getSupportcods() {
		return supportcods;
	}



	public void setSupportcods(String supportcods) {
		this.supportcods = supportcods;
	}



	public String getSellerIds() {
		return sellerIds;
	}



	public void setSellerIds(String sellerIds) {
		this.sellerIds = sellerIds;
	}
}
