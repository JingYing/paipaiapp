package com.qq.qqbuy.dealpromote.po;

import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.shippingFee.po.CodShipPo;

import java.util.Vector;

/**
 * 
 * @author candeladiao
 * @Created 2013-09-09 <a href="mailto:289303723@qq.com">Candela</a>
 */

public class TakeCheapDealFavorVo {
	private int version = 2;
	private int errorCode = 0;
	private String errorMsg = "";
	private long totalPrice;//购物车最终价钱
	private long totalCodFee;//货到付款总服务费
	private long totalMailFee;//货到付款总邮费
	private long totalFavFee;//货到付款总优惠费
	private int isSupportWXPay ;//是否支持微信支付  0 不支持    1 支持
	private int isHasScore;//用户是否有红包
	private int sellerpropery;//卖家标识
	private int sceneId;//场景Id
	private boolean queryPromoteAgain = false; // 是否需要重新批价（有某个单的商品使用了多个红包时需重新批价）
	private Vector<TakeCheapTradeFavorVo> dealList = new Vector<TakeCheapTradeFavorVo>();//批价结果列表
	private Vector<TakeCheapTradeFavorVo> problemDealList = new Vector<TakeCheapTradeFavorVo>();//异常商品列表
	private Vector<CodShipPo> sftplDetail = new Vector<CodShipPo>();//服务费用
	private Vector<ReceiverAddress> addressList = new Vector<ReceiverAddress>();//收货地址列表
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public long getTotalCodFee() {
		return totalCodFee;
	}

	public void setTotalCodFee(long totalCodFee) {
		this.totalCodFee = totalCodFee;
	}

	public long getTotalMailFee() {
		return totalMailFee;
	}

	public void setTotalMailFee(long totalMailFee) {
		this.totalMailFee = totalMailFee;
	}

	public long getTotalFavFee() {
		return totalFavFee;
	}

	public void setTotalFavFee(long totalFavFee) {
		this.totalFavFee = totalFavFee;
	}

	public int getIsSupportWXPay() {
		return isSupportWXPay;
	}

	public void setIsSupportWXPay(int isSupportWXPay) {
		this.isSupportWXPay = isSupportWXPay;
	}

	public boolean isQueryPromoteAgain() {
		return queryPromoteAgain;
	}

	public void setQueryPromoteAgain(boolean queryPromoteAgain) {
		this.queryPromoteAgain = queryPromoteAgain;
	}

	public Vector<TakeCheapTradeFavorVo> getDealList() {
		return dealList;
	}

	public void setDealList(Vector<TakeCheapTradeFavorVo> dealList) {
		this.dealList = dealList;
	}

	public Vector<TakeCheapTradeFavorVo> getProblemDealList() {
		return problemDealList;
	}

	public void setProblemDealList(Vector<TakeCheapTradeFavorVo> problemDealList) {
		this.problemDealList = problemDealList;
	}

	public Vector<CodShipPo> getSftplDetail() {
		return sftplDetail;
	}

	public void setSftplDetail(Vector<CodShipPo> sftplDetail) {
		this.sftplDetail = sftplDetail;
	}

	public Vector<ReceiverAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(Vector<ReceiverAddress> addressList) {
		this.addressList = addressList;
	}

	public int getIsHasScore() {
		return isHasScore;
	}

	public void setIsHasScore(int isHasScore) {
		this.isHasScore = isHasScore;
	}

	public int getSellerpropery() {
		return sellerpropery;
	}

	public void setSellerpropery(int sellerpropery) {
		this.sellerpropery = sellerpropery;
	}

	public int getSceneId() {
		return sceneId;
	}

	public void setSceneId(int sceneId) {
		this.sceneId = sceneId;
	}
}
