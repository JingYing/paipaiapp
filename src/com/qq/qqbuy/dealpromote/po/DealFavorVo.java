package com.qq.qqbuy.dealpromote.po;

import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol.PayRecommend;

import java.util.Vector;

/**
 * 
 * @author candeladiao
 * @Created 2013-09-09 <a href="mailto:289303723@qq.com">Candela</a>
 */

public class DealFavorVo {
	private int version = 2;
	private int errorCode = 0;
	private String errorMsg = "";
	private long totalPrice;//购物车最终价钱
	private long totalCodFee;//货到付款总服务费
	private long totalMailFee;//货到付款总邮费
	private long totalFavFee;//货到付款总优惠费
	private int isSupportWXPay ;//是否支持微信支付  0 不支持    1 支持
	private boolean queryPromoteAgain = false; // 是否需要重新批价（有某个单的商品使用了多个红包时需重新批价）
	private Vector<TradeFavorVo> dealList = new Vector<TradeFavorVo>();
	private Vector<TradeFavorVo> problemDealList = new Vector<TradeFavorVo>();//异常商品列表
//	private DealFavor dealFavor = new DealFavor();
	private Vector<CodShipPo> sftplDetail = new Vector<CodShipPo>();//服务费用
	private Vector<ReceiverAddress> addressList = new Vector<ReceiverAddress>();//收货地址列表
//	private Vector<PayRecommend> payRecommendList = new Vector<PayRecommend>();//推荐支付方式
	
	Vector<PayRecommend> payRecommendList = new Vector<PayRecommend>();//支付桥结果
	
	
	public Vector<CodShipPo> getSftplDetail() {
		return sftplDetail;
	}

	public void setSftplDetail(Vector<CodShipPo> sftplDetail) {
		this.sftplDetail = sftplDetail;
	}

	public Vector<ReceiverAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(Vector<ReceiverAddress> addressList) {
		this.addressList = addressList;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}	

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public long getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public long getTotalCodFee() {
		return totalCodFee;
	}

	public void setTotalCodFee(long totalCodFee) {
		this.totalCodFee = totalCodFee;
	}

	public long getTotalMailFee() {
		return totalMailFee;
	}

	public void setTotalMailFee(long totalMailFee) {
		this.totalMailFee = totalMailFee;
	}

	public long getTotalFavFee() {
		return totalFavFee;
	}

	public void setTotalFavFee(long totalFavFee) {
		this.totalFavFee = totalFavFee;
	}
	
	public boolean getQueryPromoteAgain() {
		return queryPromoteAgain;
	}

	public void setQueryPromoteAgain(boolean queryPromoteAgain) {
		this.queryPromoteAgain = queryPromoteAgain;
	}

	public Vector<TradeFavorVo> getDealList() {
		return dealList;
	}

	public void setDealList(Vector<TradeFavorVo> dealList) {
		this.dealList = dealList;
	}

	public int getIsSupportWXPay() {
		return isSupportWXPay;
	}

	public void setIsSupportWXPay(int isSupportWXPay) {
		this.isSupportWXPay = isSupportWXPay;
	}

	public Vector<TradeFavorVo> getProblemDealList() {
		return problemDealList;
	}

	public void setProblemDealList(Vector<TradeFavorVo> problemDealList) {
		this.problemDealList = problemDealList;
	}

	public Vector<PayRecommend> getPayRecommendList() {
		return payRecommendList;
	}

	public void setPayRecommendList(Vector<PayRecommend> payRecommendList) {
		this.payRecommendList = payRecommendList;
	}

//	public Vector<PayRecommend> getPayRecommendList() {
//		return payRecommendList;
//	}
//
//	public void setPayRecommendList(Vector<PayRecommend> payRecommendList) {
//		this.payRecommendList = payRecommendList;
//	}

//	public DealFavor getDealFavor() {
//		return dealFavor;
//	}
//
//	public void setDealFavor(DealFavor dealFavor) {
//		this.dealFavor = dealFavor;
//	}
}
