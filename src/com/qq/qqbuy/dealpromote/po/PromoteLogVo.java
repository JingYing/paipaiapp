package com.qq.qqbuy.dealpromote.po;

public class PromoteLogVo {

	 private long type; 					// 优惠类型
	 private long favorId; 					// 优惠ID
	 private long beforePrice;				//优惠前价格
	 private long afterPrice;				//优惠后价格
	 private int favorFee;					//优惠后金额
	 private long changeStat;				//调价标识
	 private String favorDesc = new String();//优惠描述
	 private String itemId = new String();	//商品ID
	 private String attr = new String();	//商品库存
	 private long promotionPrice;			//商品促销价
	 private long num;						//商品购买数量
//	 private long skuid;					//商品skuid
	 
	 
	public long getType() {
		return type;
	}
	public void setType(long type) {
		this.type = type;
	}
	public long getFavorId() {
		return favorId;
	}
	public void setFavorId(long favorId) {
		this.favorId = favorId;
	}
	public long getBeforePrice() {
		return beforePrice;
	}
	public void setBeforePrice(long beforePrice) {
		this.beforePrice = beforePrice;
	}
	public long getAfterPrice() {
		return afterPrice;
	}
	public void setAfterPrice(long afterPrice) {
		this.afterPrice = afterPrice;
	}
	public int getFavorFee() {
		return favorFee;
	}
	public void setFavorFee(int favorFee) {
		this.favorFee = favorFee;
	}
	public long getChangeStat() {
		return changeStat;
	}
	public void setChangeStat(long changeStat) {
		this.changeStat = changeStat;
	}
	public String getFavorDesc() {
		return favorDesc;
	}
	public void setFavorDesc(String favorDesc) {
		this.favorDesc = favorDesc;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public long getPromotionPrice() {
		return promotionPrice;
	}
	public void setPromotionPrice(long promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	public long getNum() {
		return num;
	}
	public void setNum(long num) {
		this.num = num;
	}
	 
	 
	 
}
