package com.qq.qqbuy.dealpromote.po;

public class FavorExVo {
	
	private long totalType;		// 概括优惠类型
	private long type;			// 优惠类型
//	private long flag;			// 优惠标识
	private long favorId;		// 优惠ID
//	private long buyerUin;		// 买家号码
	private int favorPrice;		// 优惠面值
//	private long minimum;		// 使用门槛
	private String favorName;	// 优惠名称
	private String favorDesc;	// 优惠描述
//	private long beginTime;		// 生效时间
//	private long endTime;		// 失效时间
//	private Set<uint32_t> setApplicableScope = new HashSet<uint32_t>();// 适用店铺集合
	private long checkStat;		// 选中标识
//	private long changeStat;	// 调价标识
	private long useStat;		// 可用标识
	private long postStat;		// 包邮标识
	
//	private String itemId;		// 商品ID
//	private String attr;		// 商品库存
//	private long allowFee;		// 商品允许抵消金额
//	private int realFee;		// 实际优惠金额
//	private long skuid;		
	
	public long getType() {
		return type;
	}
	public long getTotalType() {
		return totalType;
	}
	public void setTotalType(long totalType) {
		this.totalType = totalType;
	}
	public void setType(long type) {
		this.type = type;
	}
//	public long getFlag() {
//		return flag;
//	}
//	public void setFlag(long flag) {
//		this.flag = flag;
//	}
	public long getFavorId() {
		return favorId;
	}
	public void setFavorId(long favorId) {
		this.favorId = favorId;
	}
	public int getFavorPrice() {
		return favorPrice;
	}
	public void setFavorPrice(int favorPrice) {
		this.favorPrice = favorPrice;
	}
//	public long getMinimum() {
//		return minimum;
//	}
//	public void setMinimum(long minimum) {
//		this.minimum = minimum;
//	}
	public String getFavorName() {
		return favorName;
	}
	public void setFavorName(String favorName) {
		this.favorName = favorName;
	}
	public String getFavorDesc() {
		return favorDesc;
	}
	public void setFavorDesc(String favorDesc) {
		this.favorDesc = favorDesc;
	}
//	public long getBeginTime() {
//		return beginTime;
//	}
//	public void setBeginTime(long beginTime) {
//		this.beginTime = beginTime;
//	}
//	public long getEndTime() {
//		return endTime;
//	}
//	public void setEndTime(long endTime) {
//		this.endTime = endTime;
//	}
	public long getCheckStat() {
		return checkStat;
	}
	public void setCheckStat(long checkStat) {
		this.checkStat = checkStat;
	}
	public long getUseStat() {
		return useStat;
	}
	public void setUseStat(long useStat) {
		this.useStat = useStat;
	}
	public long getPostStat() {
		return postStat;
	}
	public void setPostStat(long postStat) {
		this.postStat = postStat;
	}
//	public String getItemId() {
//		return itemId;
//	}
//	public void setItemId(String itemId) {
//		this.itemId = itemId;
//	}
//	public String getAttr() {
//		return attr;
//	}
//	public void setAttr(String attr) {
//		this.attr = attr;
//	}
//	public long getAllowFee() {
//		return allowFee;
//	}
//	public void setAllowFee(long allowFee) {
//		this.allowFee = allowFee;
//	}
//	public int getRealFee() {
//		return realFee;
//	}
//	public void setRealFee(int realFee) {
//		this.realFee = realFee;
//	}
	
}
