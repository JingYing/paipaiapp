package com.qq.qqbuy.dealpromote.util;

import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint64_t;
import com.qq.qqbuy.cmdy.po.CmdyItemVo;
import com.qq.qqbuy.common.constant.BizConstant;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.deal.po.ConfirmOrderVo;
import com.qq.qqbuy.deal.po.ConfirmPackageVo;
import com.qq.qqbuy.deal.po.ShipCalcVo;
import com.qq.qqbuy.dealpromote.po.DealFavorVo;
import com.qq.qqbuy.dealpromote.po.DealPromotePo;
import com.qq.qqbuy.dealpromote.po.FavorExVo;
import com.qq.qqbuy.dealpromote.po.ItemPromoteResultVo;
import com.qq.qqbuy.dealpromote.po.TradeFavorVo;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemPo_v2;
import com.qq.qqbuy.thirdparty.idl.item.protocol.ItemProp;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromote;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromoteFavor;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.FavorEx;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.FavorFilter;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.ItemPrice;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.ItemPromote;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.ItemPromoteFavor;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.ItemPromoteResult;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.TradeFavor;
import com.qq.qqbuy.thirdparty.idl.userinfo.ApiUserClient;
import com.qq.qqbuy.thirdparty.idl.userinfo.UserInfoConstants;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser.APIUserProfile;

//优惠类型 priceType

public class DealPromoteUtil {
	/**
	 * 批价价格类型
	 */
	
	public static final int ORDER_PROMOTE_FAVOR_PRICE_NULL 			= 1;		//多价无优惠类型
	public static final int ORDER_PROMOTE_FAVOR_PRICE_QQVIP 		= 3;		//商品优惠价格类型之QQ会员
	public static final int ORDER_PROMOTE_FAVOR_PRICE_CAIZUAN 		= 4;		//彩钻优惠类型
	public static final int ORDER_PROMOTE_FAVOR_PRICE_SHOPVIP 		= 5;		//店铺vip优惠类型
	public static final int ORDER_PROMOTE_FAVOR_PRICE_GREENVIP 		= 6;		//绿钻优惠类型
	public static final int ORDER_PROMOTE_FAVOR_PRICE_TEJIA 		= 41; 		//特价商品
	public static final int ORDER_PROMOTE_FAVOR_PRICE_REDVIP 		= 42;		//红钻商品
	public static final int ORDER_PROMOTE_FAVOR_PRICE_RECCOMMEND 	= 43;		//推荐位商品
	public static final int ORDER_PROMOTE_FAVOR_PRICE_TUANGOU 		= 44;		//团购商品
	public static final int ORDER_PROMOTE_FAVOR_PRICE_LIMIT 		= 45;		//限时折扣            
	public static final int ORDER_PROMOTE_FAVOR_PRICE_ACT 			= 46;		//大促活动
	public static final long ORDER_PROMOTE_FAVOR__REDDIAMOND      	= 0x80000000L;  //场景活动最小值，同时红钻的缺省活动类型                                                                             
	public static final long ORDER_PROMOTE_FAVOR__NBAVIP          	= 0x80000001L;  //NBA的活动类型                                                                                                      
	public static final long ORDER_PROMOTE_FAVOR__QUICKPAY          = 0x80000002L;  //快捷支付活动类型                                                                                                 
	public static final int ORDER_PROMOTE_FAVOR_COMBO 				= 60; 		//套餐优惠
	public static final int ORDER_PROMOTE_FAVOR_CMDY_REDPACKET 		= 61; 		//商品红包
	public static final int ORDER_PROMOTE_FAVOR_COUPON 				= 62; 		//优惠券
	public static final int ORDER_PROMOTE_FAVOR_FREE_MAILCARD 		= 63; 		//包邮卡
	public static final int ORDER_PROMOTE_FAVOR_SALEPROMOTION 		= 64; 		//店铺促销总概
	public static final int ORDER_PROMOTE_FAVOR_DERATE				= 65; 		//满减
	public static final int ORDER_PROMOTE_FAVOR_DISCOUNT			= 66; 		//满减（折扣）
	public static final int ORDER_PROMOTE_FAVOR_BARTER				= 67; 		//满加价换购
	public static final int ORDER_PROMOTE_FAVOR_PRESENT				= 68; 		//满送
	public static final int ORDER_PROMOTE_FAVOR_FREECARRIAGE		= 69; 		//满包邮
	public static final int ORDER_PROMOTE_FAVOR_WG_COUPON 			= 71; 		//网购平台券
	public static final int ORDER_PROMOTE_FAVOR_WEIGOUPRICE			= 101; 		//101微购物优惠
	public static final int ORDER_PROMOTE_FAVOR_WEIGOUACT			= 102; 		//102微购物优惠
	public static final int ORDER_PROMOTE_FAVOR_WEIDIAN			    = 105;      //微店价
	public static final int ORDER_PROMOTE_FAVOR_PIFA			    = 106;      //批发价
	
	/**
	 * 购物车makeOrder价格类型
	 */
	
	public static final int ORDER_CMDY_PRICE_TYPE_QQVIP			= 1;	// QQ会员
	public static final int ORDER_CMDY_PRICE_TYPE_REDDIAMOND	= 2;	// 红钻价
	public static final int ORDER_CMDY_PRICE_TYPE_RREENDIAMOND	= 3;	// 绿钻价
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND1	= 4;	// 彩钻1级
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND2	= 5;	// 彩钻2级
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND3	= 6;	// 彩钻3级
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND4	= 7;	// 彩钻4级
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND5	= 8;	// 彩钻5级
	public static final int ORDER_CMDY_PRICE_TYPE_COLORDIAMOND6	= 9;	// 彩钻6级
	public static final int ORDER_CMDY_PRICE_TYPE_COMBO			= 10;	// 套餐购买时的价格
	public static final int ORDER_CMDY_PRICE_TYPE_SHOPVIP1		= 11;	// 店铺VIP1级
	public static final int ORDER_CMDY_PRICE_TYPE_SHOPVIP2		= 12;	// 店铺VIP2级
	public static final int ORDER_CMDY_PRICE_TYPE_SHOPVIP3		= 13;	// 店铺VIP3级
	public static final int ORDER_CMDY_PRICE_TYPE_SHOPVIP4		= 14;	// 店铺VIP4级
	public static final int ORDER_CMDY_PRICE_TYPE_RECOMMEND		= 15;	// 促销特价
	public static final int ORDER_CMDY_PRICE_TYPE_TUANGOU		= 16;	// 促销团购价
	public static final int ORDER_CMDY_PRICE_TYPE_NBA			= 17;	// NBA会员专享价
	public static final int ORDER_CMDY_PRICE_TYPE_FASTPAY		= 18;	// 快捷支付价
	public static final int ORDER_CMDY_PRICE_TYPE_WEIGOU_101	= 101;
	public static final int ORDER_CMDY_PRICE_TYPE_WEIGOU_102	= 102;
	public static final int ORDER_CMDY_PRICE_TYPE_WEIDIAN		= 105;   //微店价
	public static final int ORDER_CMDY_PRICE_TYPE_PIFA			= 106;   //批发
	
	// 分单返回结果和批价返回结果转换为前端可用数据
	public static final DealFavorVo convert(Vector<TradeFavor> tradeList, DealPromotePo promotePo, ConfirmOrderVo confirmVo) {
		if (tradeList == null || tradeList.size() == 0) {
			return null;
		}
		
		DealFavorVo vo = new DealFavorVo();
		
		long totalPrice = 0;
		boolean wxSupportFlag = true ;
		for (TradeFavor tradeFavor : tradeList){
			TradeFavorVo tradeVo = new TradeFavorVo();
			
			// 从请求参数设置订单的货到付款状态 和促销标识
			for (DealPromote dealPromote : promotePo.getDealPromoteList()) {
				if (dealPromote.getDealId() == tradeFavor.getDealId()) {
					tradeVo.setPayOnReceive(dealPromote.getPayOnReciew());
					tradeVo.setPromotion(dealPromote.getMarketStat());
				}
			}
			
			tradeVo.setDealId(tradeFavor.getDealId());
			tradeVo.setSellerUin(tradeFavor.getSellerUin());
			tradeVo.setTradeFee(tradeFavor.getTradeFee());
			tradeVo.setFavorFee(tradeFavor.getFavorFee());
			tradeVo.setErrType(tradeFavor.getErrType());
			tradeVo.setErrMsg(tradeFavor.getErrMsg());
			tradeVo.setPostStat(tradeFavor.getPostStat());
			APIUserProfile profile = ApiUserClient.getUserSimpleInfo(tradeFavor.getSellerUin());
			int isSupportWXPay = 0;
			if (profile != null) {
				int sellerPropForWeiXin = 0 ;
				int sellerPropForQQGou = 0 ;
				uint16_t keyForWeiXin = new uint16_t(UserInfoConstants.NEW_USER_PROP_BOS_WEIXIN_PAY);
				uint16_t keyForQQGou = new uint16_t(UserInfoConstants.NEW_USER_PROP_BOS_MEMBER_PRIVILEGE);
				
				if (profile.getProp().get(keyForWeiXin) != null) {
					sellerPropForWeiXin = profile.getProp().get(keyForWeiXin).intValue();
				}
				if (profile.getProp().get(keyForQQGou) != null) {
					sellerPropForQQGou = profile.getProp().get(keyForQQGou).intValue();
				}
				if (sellerPropForQQGou == 0 && sellerPropForWeiXin == 1) {
					isSupportWXPay = 1 ;
				} else {
					isSupportWXPay = 0 ;
				}
			}
			tradeVo.setIsSupportWXPay(isSupportWXPay) ;
			totalPrice += tradeFavor.getTradeFee();
			wxSupportFlag = wxSupportFlag&&(isSupportWXPay==1) ;
			// 转换订单维度可选优惠集合
			for (DealPromoteFavor dealFavor : tradeFavor.getDealPromoteFavor()){
				for (FavorEx favorEx : dealFavor.getFavorExList()){
					
					FavorExVo favorExVo = new FavorExVo();
					
					favorExVo.setTotalType(favorEx.getTotalType());
					favorExVo.setType(favorEx.getType());
					if (favorEx.getTotalType() == 0) {
						favorExVo.setTotalType(favorEx.getType());
					}
					favorExVo.setFavorId(favorEx.getFavorId());
					favorExVo.setFavorPrice(favorEx.getFavorPrice());
					
					favorExVo.setFavorName(favorEx.getFavorName());
					favorExVo.setFavorDesc(favorEx.getFavorDesc());
					
					favorExVo.setCheckStat(favorEx.getCheckStat());
					favorExVo.setPostStat(favorEx.getPostStat());
					favorExVo.setUseStat(favorEx.getUseStat());
					
					switch (new Long(favorExVo.getTotalType()).intValue()){
						case ORDER_PROMOTE_FAVOR_COUPON: //店铺优惠券
						{
							// 优惠金额大于0 且订单总价大于0.01元 且优惠券可用
							if (favorEx.getFavorPrice() > 0 && favorEx.getUseStat() == 1) {
								String favorName = String.format("%s元店铺优惠券", new Long(favorEx.getFavorPrice() / 100).toString());
								
								if (favorEx.getMinimum() > 0) {
									favorName += String.format(" 满%s元可用", new Long(favorEx.getMinimum() / 100).toString());
								} else {
									favorName += " 无使用限制";
								}
								favorExVo.setFavorName(favorName);
								favorExVo.setFavorDesc(favorName);
							
								tradeVo.getShopCoupons().add(favorExVo);
							} else {
								tradeVo.getOtherCoupons().add(favorExVo);
							}
							break;
						}
						
						case ORDER_PROMOTE_FAVOR_SALEPROMOTION: //店铺促销
						case ORDER_PROMOTE_FAVOR_DERATE:
						case ORDER_PROMOTE_FAVOR_DISCOUNT:
						case ORDER_PROMOTE_FAVOR_BARTER:
						case ORDER_PROMOTE_FAVOR_PRESENT:
						case ORDER_PROMOTE_FAVOR_FREECARRIAGE:
						{
							if (favorEx.getUseStat() == 1) {
							tradeVo.getShopPromotions().add(favorExVo);
							} else {
								tradeVo.getOtherCoupons().add(favorExVo);
							}
							break;
						}
						
						case ORDER_PROMOTE_FAVOR_WG_COUPON: // 网购代金券
						{
							if (favorEx.getFavorPrice() > 0 && favorEx.getUseStat() == 1) {
								String favorName = String.format("%s元代金券", new Long(favorEx.getFavorPrice() / 100).toString());
								
								if (favorEx.getMinimum() > 0) {
									favorName += String.format(" 满%s元可用", new Long(favorEx.getMinimum() / 100).toString());
								} else {
									favorName += " 无使用限制";
								}
								favorExVo.setFavorName(favorName);
								favorExVo.setFavorDesc(favorName);
							
								tradeVo.getCashCoupons().add(favorExVo);
							} else {
								tradeVo.getOtherCoupons().add(favorExVo);
							}
							break;
						}
						
						default:
						{
							tradeVo.getOtherCoupons().add(favorExVo);
							break;
						}
					}
				}
			}
			
			
			// 分单结果列表中的当前订单
			ConfirmPackageVo currentPkgVo = null;
			if (confirmVo != null) {
				for (ConfirmPackageVo pkgVo : confirmVo.getNormalPackages()){
//					if (pkgVo.getSellerUin() == tradeFavor.getSellerUin()){
					if (pkgVo.getDealId() == tradeFavor.getDealId()) {
						currentPkgVo = pkgVo;
						
						// 设置当前店铺
						tradeVo.setShopName(pkgVo.getShopName());
						
//						// 分单取到的是店铺属性位
//						if ((pkgVo.getShopType() & BizConstant.SHOP_B2C_SELLER) > 0) {
//							tradeVo.setShopType(1);// 网购卖家
//						} else {
//							tradeVo.setShopType(0);// 拍拍卖家
//						}
						tradeVo.setShopType(pkgVo.getShopType());
						
						// 设置运送方式
						tradeVo.setShipCalcInfos(pkgVo.getShipCalcInfos());
						
					}
				}
			} else if (tradeVo.getPayOnReceive() == 1){ // 货到付款且不是从分单结果调用批价(即用户在终端操作触发的批价)
				
				ShipCalcVo calcVo = promotePo.getMailInfos().get(tradeFavor.getDealId());
				tradeVo.getShipCalcInfos().add(calcVo);
			}
			
			// 转换商品批价结果
			for (ItemPromoteResult itemResult : tradeFavor.getItemPromoteResultList()){
				ItemPromoteResultVo itemResultVo = new ItemPromoteResultVo();
				
				itemResultVo.setItemId(itemResult.getItemId());
				itemResultVo.setAttr(itemResult.getAttr());
				itemResultVo.setTotalMoney(itemResult.getTotalMoney());
				
				itemResultVo.setPrice(itemResult.getPrice());
				if (itemResult.getPrice() > itemResult.getPromotionPrice()) {
					itemResultVo.setPrice(itemResult.getPromotionPrice());
				}
				itemResultVo.setAllowFee(itemResult.getAllowFee());
				
				// 设置商品主图
				if (currentPkgVo != null){
					for (CmdyItemVo itemVo : currentPkgVo.getItems()){
						if (itemVo.getItemCode().equals(itemResult.getItemId()) 
								&& itemVo.getItemAttr().equals(itemResult.getAttr())) {
							itemResultVo.setItemName(itemVo.getItemTitle());
							itemResultVo.setMainLogo(itemVo.getMainLogoUrl());
							
						}
					}
				}
				
				itemResultVo.setNum(itemResult.getNum());
				itemResultVo.setPriceDesc("原价");
				// 根据商品维度可选优惠集合，设置当前商品可选优惠列表
				for (ItemPromoteFavor itemFavor : tradeFavor.getItemPromoteFavor()){
					//if (itemFavor.getItemId().equals(itemResult.getItemId())) 不能单独以itemId标识一个商品
					//if (itemFavor.getSkuid() == itemResult.getSkuid())// itemPromoteResultList里sku字段为空
					if (itemFavor.getItemId().equals(itemResult.getItemId()) 
							&& itemFavor.getAttr().equals(itemResult.getAttr()))
					{
						// 当前商品使用的红包数量
						int redPackageCount = 0;
						
						// 商品优惠
						for (FavorEx favorEx : itemFavor.getFavorExList()){
							
							FavorExVo favorExVo = new FavorExVo();
							
							favorExVo.setTotalType(favorEx.getTotalType());
							favorExVo.setType(favorEx.getType());
							if (favorEx.getTotalType() == 0) {
								favorExVo.setTotalType(favorEx.getType());
							}
							favorExVo.setFavorId(favorEx.getFavorId());
							
							favorExVo.setFavorName(favorEx.getFavorName());
							favorExVo.setFavorDesc(favorEx.getFavorDesc());
							
							favorExVo.setFavorPrice(favorEx.getFavorPrice());
							favorExVo.setCheckStat(favorEx.getCheckStat());
							favorExVo.setPostStat(favorEx.getPostStat());
							favorExVo.setUseStat(favorEx.getUseStat());
							
							if (favorExVo.getTotalType() == ORDER_PROMOTE_FAVOR_CMDY_REDPACKET){
								if (itemResultVo.getAllowFee() > 0 && favorEx.getUseStat() == 1) {// 过滤面值不足的
									if (favorEx.getCheckStat() == 1) {
										redPackageCount++;
									}
									
									String favorName = favorExVo.getFavorName();
									if (favorEx.getFavorPrice() > 0) {
										favorName = String.format("%s元红包", new Long(favorEx.getFavorPrice() / 100).toString());
									}
									
									favorExVo.setFavorName(favorName);
									favorExVo.setFavorDesc(favorName);
									
									itemResultVo.getRedPackages().add(favorExVo);
								} else {
									itemResultVo.getOtherFavorList().add(favorExVo);
								}
							} else {
								itemResultVo.getOtherFavorList().add(favorExVo);
							}
						}
						
						// 若所使用红包数量大于2个，设置重新批价标识位
						if (redPackageCount > 1) {
							vo.setQueryPromoteAgain(true);
						}
						
						// 商品价格描述
						for (ItemPrice itemPrice : itemFavor.getItemPriceList()) {
							if (itemPrice.getCheckStat() == 1) {
								itemResultVo.setPriceType(itemPrice.getType());
								
								switch (new Long(itemPrice.getType()).intValue()) {
									
									case ORDER_PROMOTE_FAVOR_PRICE_QQVIP:		//商品优惠价格类型之QQ会员
									{
										itemResultVo.setPriceDesc("QQ会员价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_QQVIP);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_CAIZUAN:		//彩钻优惠类型
									{
										itemResultVo.setPriceDesc("彩钻价");
										
										switch (new Long(itemPrice.getLevel()).intValue()){
											case 1:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND1);
												break;
											}
											
											case 2:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND2);
												break;
											}
											
											case 3:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND3);
												break;
											}
											
											case 4:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND4);
												break;
											}
											
											case 5:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND5);
												break;
											}
											
											case 6:
											{
												itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_COLORDIAMOND6);
												break;
											}
											
											default:
											{
												break;
											}
										}
										
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_SHOPVIP:		//店铺vip优惠类型
									{
										itemResultVo.setPriceDesc("店铺VIP价");
										switch (new Long(itemPrice.getLevel()).intValue()){
										case 1:
										{
											itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_SHOPVIP1);
											break;
										}
										
										case 2:
										{
											itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_SHOPVIP2);
											break;
										}
										
										case 3:
										{
											itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_SHOPVIP3);
											break;
										}
										
										case 4:
										{
											itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_SHOPVIP4);
											break;
										}
										
										default:
										{
											break;
										}
									}
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_GREENVIP:	//绿钻优惠类型
									{
										itemResultVo.setPriceDesc("绿钻价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_RREENDIAMOND);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_TEJIA: 		//特价商品
									{
										itemResultVo.setPriceType(ORDER_PROMOTE_FAVOR_PRICE_RECCOMMEND);
										
										itemResultVo.setPriceDesc("特价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_RECOMMEND);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_REDVIP:		//红钻商品
									{
										itemResultVo.setPriceDesc("红钻价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_REDDIAMOND);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_RECCOMMEND:	//推荐位商品
									{
										itemResultVo.setPriceDesc("促销价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_RECOMMEND);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_TUANGOU:		//团购商品
									{
										itemResultVo.setPriceDesc("团购价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_TUANGOU);
										break;
									}
									case ORDER_PROMOTE_FAVOR_PRICE_LIMIT:		//限时折扣   
									{
										itemResultVo.setPriceType(ORDER_PROMOTE_FAVOR_PRICE_RECCOMMEND);
										
										itemResultVo.setPriceDesc("促销价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_RECOMMEND);
										break;
									}         
									case ORDER_PROMOTE_FAVOR_WEIDIAN:			//微店价
									{										
										itemResultVo.setPriceDesc("微店价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_WEIDIAN);
										break;
									}
									
									case ORDER_PROMOTE_FAVOR_PIFA:			//批发价 
									{										
										itemResultVo.setPriceDesc("批发价");
										itemResultVo.setOrderPriceType(ORDER_CMDY_PRICE_TYPE_PIFA);
										break;
									}
									
									case ORDER_PROMOTE_FAVOR_PRICE_NULL:		//多价无优惠类型
									default:
									{
										if(StringUtils.isNotBlank(itemPrice.getName())){
											itemResultVo.setPriceDesc(itemPrice.getName());
										}else{
											itemResultVo.setPriceDesc("原价");
										}
										break;
									}
								}
							}
						}
					}
				}
				
				tradeVo.getItemPromoteResult().add(itemResultVo);
			}
			
			vo.getDealList().add(tradeVo);
		}
		if (wxSupportFlag) {
			vo.setIsSupportWXPay(1) ;
		} else {
			vo.setIsSupportWXPay(0) ;
		}
		vo.setTotalPrice(totalPrice);
		
		return vo;
	}
	
	// 购物车下单，将分单返回结果转换成批价请求参数
	public static final DealPromotePo convert(long buyerUin, long payType, ConfirmOrderVo confirmVo,int useRedPackage) {
		if (confirmVo == null) {
			return null;
		}
		
		DealPromotePo promotePo = new DealPromotePo();
		promotePo.setBuyerUin(buyerUin);
		for (int i = 0; i < confirmVo.getNormalPackages().size(); i++) {
			ConfirmPackageVo pkgVo = confirmVo.getNormalPackages().elementAt(i);
			
			DealPromote dealPromote = new DealPromote();
			
			dealPromote.setDealId(pkgVo.getDealId());
			dealPromote.setSeqId(System.currentTimeMillis() / 1000);
			
			dealPromote.setSellerUin(pkgVo.getSellerUin());
			dealPromote.setBuyerUin(buyerUin);
			dealPromote.setSceneId(0);
			
			dealPromote.setPayOnReciew(payType);
			if ((pkgVo.getShopType() & BizConstant.SHOP_B2C_SELLER) > 0) {
				dealPromote.setUserPropery(1);// 网购卖家
			} else {
				/**
				 * 当useRedPackage == 1,批价使用红包
				 */
				if(useRedPackage == 1 && DealPromotePo.hasRedPackageUser(buyerUin)){
					dealPromote.setUserPropery(2);// 拍拍卖家
				}else{
					dealPromote.setUserPropery(0);// 拍拍卖家
				}
			}

			promotePo.getShopTypes().put(pkgVo.getSellerUin(), (long)pkgVo.getShopType());
			
//			int marketStat = pkgVo.getPromotionRules().size() > 0 ? 1 : 0;
			dealPromote.setMarketStat(pkgVo.getPromotion());
//			dealPromote.setComBoId(dealStr[2]);
			
			// 商品列表
			for (CmdyItemVo itemVo : pkgVo.getItems()){
				ItemPromote itemPromote = new ItemPromote();
				
				itemPromote.setItemId(itemVo.getItemCode());
				itemPromote.setItemCnt(itemVo.getBuyNum());
				itemPromote.setPriceType(0);//itemVo.getMostLowPrice().getPriceType());
				itemPromote.setProperty(itemVo.getItemAttr());
				
				//2015-04-29劉本龍
				if(itemVo.getSceneId() != 0){
					FavorFilter favorFilter = new FavorFilter();
					favorFilter.setType(DealPromoteUtil.ORDER_CMDY_PRICE_TYPE_SHOPVIP1);
					favorFilter.getFavorIdList().add(new uint64_t(itemVo.getSceneId()));
					itemPromote.getFavorFilterList().add(favorFilter);
				}
				
				dealPromote.getItemPromoteList().add(itemPromote);
			}
			
			promotePo.getDealPromoteList().add(dealPromote);
		}
		
		return promotePo;
	}
	
	
	// 购物车下单，将批价返回结果转换成二次批价请求参数
	public static final DealPromotePo convert(long buyerUin, long payType, DealFavorVo favorVo,int useRedPackage) {
		if (favorVo == null) {
			return null;
		}
		
		DealPromotePo promotePo = new DealPromotePo();
		promotePo.setBuyerUin(buyerUin);
		for (int i = 0; i < favorVo.getDealList().size(); i++) {
			TradeFavorVo tradeFavorVo = favorVo.getDealList().get(i);
			
			DealPromote dealPromote = new DealPromote();
			
			dealPromote.setDealId(tradeFavorVo.getDealId());
			dealPromote.setSeqId(System.currentTimeMillis() / 1000);
			
			dealPromote.setSellerUin(tradeFavorVo.getSellerUin());
			dealPromote.setBuyerUin(buyerUin);
			dealPromote.setSceneId(0);
			dealPromote.setPayOnReciew(payType);
			
			if ((tradeFavorVo.getShopType() & BizConstant.SHOP_B2C_SELLER) > 0) {
				dealPromote.setUserPropery(1);// 网购卖家
			} else {
				/**
				 * 当useRedPackage == 1,批价使用红包
				 */
				if(useRedPackage == 1 && DealPromotePo.hasRedPackageUser(buyerUin)){
					dealPromote.setUserPropery(2);// 拍拍卖家
				}else{
					dealPromote.setUserPropery(0);// 拍拍卖家
				}
			}
			dealPromote.setMarketStat(1);
			
			// 订单优惠列表-现金券
			for (FavorExVo favorExVo : tradeFavorVo.getCashCoupons()) {
				if (favorExVo.getCheckStat() == 1) {
					FavorFilter favorFilter = new FavorFilter();
					
					favorFilter.setType(favorExVo.getType());
					favorFilter.getFavorIdList().add(new uint64_t(favorExVo.getFavorId()));
					dealPromote.getFavorFilterList().add(favorFilter);
				}
			}
			
			// 订单优惠列表-店铺券
			for (FavorExVo favorExVo : tradeFavorVo.getShopCoupons()) {
				if (favorExVo.getCheckStat() == 1) {
					FavorFilter favorFilter = new FavorFilter();
					
					favorFilter.setType(favorExVo.getType());
					favorFilter.getFavorIdList().add(new uint64_t(favorExVo.getFavorId()));
					dealPromote.getFavorFilterList().add(favorFilter);
				}
			}
			
			// 订单优惠列表-促销规则
			for (FavorExVo favorExVo : tradeFavorVo.getShopPromotions()) {
				if (favorExVo.getCheckStat() == 1) {
					FavorFilter favorFilter = new FavorFilter();
					
					favorFilter.setType(favorExVo.getType());
					favorFilter.getFavorIdList().add(new uint64_t(favorExVo.getFavorId()));
					dealPromote.getFavorFilterList().add(favorFilter);
				}
			}
			
			// 商品列表
			for (ItemPromoteResultVo itemVo : tradeFavorVo.getItemPromoteResult()){
				ItemPromote itemPromote = new ItemPromote();
				
				itemPromote.setItemId(itemVo.getItemId());
				itemPromote.setItemCnt(itemVo.getNum());
				itemPromote.setPriceType(itemVo.getPriceType());
				itemPromote.setProperty(itemVo.getAttr());
				
//				// 商品优惠-红包:只选择一个
//				for (FavorExVo favorExVo : itemVo.getRedPackages()) {
//					if (favorExVo.getCheckStat() == 1) {
//						FavorFilter favorFilter = new FavorFilter();
//						
//						favorFilter.setType(favorExVo.getType());
//						favorFilter.getFavorIdList().add(new uint64_t(favorExVo.getFavorId()));
//						itemPromote.getFavorFilterList().add(favorFilter);
//						break;
//					}
//				}
				
				dealPromote.getItemPromoteList().add(itemPromote);
			}
			
			promotePo.getDealPromoteList().add(dealPromote);
		}
		
		return promotePo;
	}
	
	public static final boolean getPromotionInfoOfItem(String itemCode){
		FetchItemInfoResp resp = ItemClient.fetchItemInfo(itemCode);
		
		ItemPo_v2 itemPo = null;
		ItemProp bitSet = null;
		boolean ret = false;
		if (resp != null && resp.getResult() == 0
			&& (itemPo = resp.getItemPo()) != null
			&& (bitSet = itemPo.getOItemProp()) != null) {
			
			//INDEX_PROP_HAS_PROMOT_INFO = 27, //商品带有促销信息
			//INDEX_PROP_PROMOTIONAL_EVENT_JOIN = 34, //参加促销活动,活动没开始(满立减活动) skydu
			//INDEX_PROP_PROMOTIONAL_EVENT_START = 35, //促销活动开始满立减活动) skydu
			//INDEX_PROP_PROMOTION_EVENT_NO=44, //不参加优惠促销的标记anthonywei 2010-10-18
			//INDEX_PROP_HAS_PROPMOTION=100, //商品是否有促销标记位，仅用于itempo_v2接口有效
			//INDEX_PROP_HAS_NEWACTIVE=101, //商品参加促销活动(非场景价)的标记位，应用在新的活动分库存价格系统中
			//INDEX_PROP_HAS_SCENE_ACTIVE=102, //商品参加促销活动(场景价)的标记位，应用在新的活动分库存价格系统中
			
			String tmpBitSet = bitSet.getBitProp().toString();
			tmpBitSet = tmpBitSet.replace("{", "");
			tmpBitSet = tmpBitSet.replace("}", "");
			String[] array = tmpBitSet.split(",");
			if (array != null) {
				for (String key : array) {
					if (key != null) {
					    int keyProp = (int) StringUtil.toLong(key
						    .trim(), 0);
					    
					    if (keyProp == 35 || keyProp == 100 || keyProp == 101 || keyProp == 102)
					    {
					    	ret = true;
					    }
					}
				}
			}
		}
		
		return ret;
	}
}
