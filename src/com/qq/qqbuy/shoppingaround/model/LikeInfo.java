package com.qq.qqbuy.shoppingaround.model;
/**
 * 
 * @ClassName: LikeInfo 
 * @Description:个人点赞记录
 * @author lhn
 * @date 2015-3-4 下午4:03:37 
 *
 */
public class LikeInfo {
	  private Long id;
	  private int itemtype;
	  private Long liketime;
	  private Long wid;
	  private String itemid;
	  private int likenum;
	  private int status;
	  
	  
	  
	  
	  
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getItemtype() {
		return itemtype;
	}
	public void setItemtype(int itemtype) {
		this.itemtype = itemtype;
	}
	public Long getLiketime() {
		return liketime;
	}
	public void setLiketime(Long liketime) {
		this.liketime = liketime;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public int getLikenum() {
		return likenum;
	}
	public void setLikenum(int likenum) {
		this.likenum = likenum;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	  
	  
	  
}
