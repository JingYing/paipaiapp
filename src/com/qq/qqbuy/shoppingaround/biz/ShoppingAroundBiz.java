package com.qq.qqbuy.shoppingaround.biz;

import java.util.List;

import com.qq.qqbuy.shoppingaround.common.CurrentPage;
import com.qq.qqbuy.shoppingaround.model.Comment;

/**
 * 
 * @ClassName: ShoppingAroundBiz 
 * @Description: 业务接口
 * @author lhn
 * @date 2015-3-4 下午3:48:46 
 *
 */
public interface ShoppingAroundBiz {
	/**
	 * @param wid 
	 * 
	 * @Title: setLike 
	 * @Description: 点赞
	 * @param @param itemid
	 * @param @param itemtype    设定文件 
	 * @return void    返回类型 
	 * @throws
	 */
	String setLike(String itemid, int itemtype, Long wid);
	/**
	 * @param iwd 
	 * 
	 * @Title: cancelLike 
	 * @Description: 取消点赞
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @return    设定文件 
	 * @return String    返回类型 
	 * @throws
	 */
	String cancelLike(String itemid, int itemtype, Long iwd);
	/**
	 * 
	 * @Title: setLikeNum 
	 * @Description:查出点赞的总数
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @return    设定文件 
	 * @return String    返回类型 
	 * @throws
	 */
	String getLikeNum(String itemid, int itemtype);
	/**
	 * @param pagesize 页行数
	 * @param pagenum 页码
	 * @Title: getCommentList 
	 * @Description: 获取评论列表
	 * @param @param itemid
	 * @param @param itemtype
	 * @param @return    设定文件 
	 * @return List<Comment>    返回类型 
	 * @throws
	 */
	CurrentPage<Comment> getCommentList(String itemid, int itemtype, int pagenum, int pagesize);

}
