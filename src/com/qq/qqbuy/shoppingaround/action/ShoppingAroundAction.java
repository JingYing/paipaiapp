package com.qq.qqbuy.shoppingaround.action;

import java.util.List;

import com.google.gson.Gson;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.shoppingaround.biz.ShoppingAroundBiz;
import com.qq.qqbuy.shoppingaround.common.CurrentPage;
import com.qq.qqbuy.shoppingaround.model.Comment;
/**
 * 
 * @ClassName: ShoppingAroundAction 
 * @Description: 购物圈的action类 
 * @author lhn
 * @email: lianghaining1@jd.com
 * @date 2015-3-4 下午2:34:02 
 *
 */
public class ShoppingAroundAction extends PaipaiApiBaseAction {
		private ShoppingAroundBiz shoppingAroundBiz;
		private int itemtype = 0;//业务点类型
		private String itemid;//业务单体ID
		private int islike = 0;//是点赞还是取消点赞（0：点赞，1：取消点赞） 
		
		//以下为评论的一些field
		private int pagenum;//第几页
		private int pagesize = 10 ;//一列多少条，默认10条
		//输出信息
		JsonOutput out = new JsonOutput();
		/**
		 * 
		 * @Title: setLike 
		 * @Description: 点赞取消点赞功能 
		 * @param @return 无    
		 * @return String 一个json格式数据
		 * @throws
		 */
		public String setLike(){
			//判断是否登录
			if (checkLogin()) {
					//判断是否是取消点赞
					if(islike==0){
						try {
							String result =	this.shoppingAroundBiz.setLike(itemid,itemtype,getWid());
							Log.run.info("-----------点赞结束记录----------"+result);
							if(result.equals("10")){
								out.setErrCode("0");
								out.setMsg("您是第一个点赞的哦！");
							}else if(result.equals("20")) {
								out.setErrCode("0");
								out.setMsg("点赞成功！");
							}else if(result.equals("110")) {
								out.setErrCode(result);
								out.setMsg("您24内只能点一次，请明天再来吧！");
							}else {
								out.setErrCode("0");
								out.setMsg("点赞成功,但数据有异常！");
							}
							
						} catch (Exception e) {
							out.setErrCode("255");
							out.setMsg("点赞失败，请求数据异常！");
						}
						
					}else {
						try {
							//如果是取消点赞，则减去被赞数
							String result =	this.shoppingAroundBiz.cancelLike(itemid,itemtype,getWid());
							if(result.equals("10")){
								out.setErrCode("0");
								out.setMsg("取消点赞成功");
							}else {
								out.setErrCode(result);
								out.setMsg("取消点赞异常");
							}
						} catch (Exception e) {
								out.setErrCode("255");
								out.setMsg("取消点赞失败，请求数据异常！");
						}
						
						
					}
			}else {//没有登录则
							out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
							out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			}
			return doPrint(out.toJsonStr());
		}
		
		/**
		 * 
		 * @Title: getLikeNum 
		 * @Description:返回点赞的个数
		 * @param @return    设定文件 
		 * @return String    返回类型 
		 * @throws
		 */
		public String  getLikeNum(){
			try {
				String result =	this.shoppingAroundBiz.getLikeNum(itemid,itemtype);
				String data ="{\"likesnum\":\""+result+"\"}";
				out.setData(new Gson().toJsonTree(data));
				out.setErrCode(0);
				out.setMsg("success");
			} catch (Exception e) {
				out.setErrCode(255);
				out.setMsg("查询数据异常");
			}
			return doPrint(out.toJsonStr());
		}
		
		/**
		 * 
		 * @Title: getCommnetList 
		 * @Description: 获取评论列表
		 * @param @return  
		 * @return String   返回Json格式数据
		 * @throws
		 */
		public String getCommentList(){
			//无需鉴权
				 try {
					 CurrentPage<Comment> commentlist = this.shoppingAroundBiz.getCommentList(itemid,itemtype,pagenum,pagesize);
					 if(commentlist!=null){
						 out.setData(new Gson().toJsonTree(commentlist));
						 out.setMsg("success");
					 }
				 } catch (Exception e) {
					 out.setErrCode(ErrConstant.ERRCODE_DAO_SQL_EXECUTION_FAIL);
					 out.setMsg("获取评论数据失败");
				}
			
			return doPrint(out.toJsonStr());
		}
		
		
		//------------------------------------get set -------------------------------------------
		public ShoppingAroundBiz getShoppingAroundBiz() {
			return shoppingAroundBiz;
		}

		public void setShoppingAroundBiz(ShoppingAroundBiz shoppingAroundBiz) {
			this.shoppingAroundBiz = shoppingAroundBiz;
		}

		public int getItemtype() {
			return itemtype;
		}

		public void setItemtype(int itemtype) {
			this.itemtype = itemtype;
		}

		public String getItemid() {
			return itemid;
		}

		public void setItemid(String itemid) {
			this.itemid = itemid;
		}

		public int getIslike() {
			return islike;
		}

		public void setIslike(int islike) {
			this.islike = islike;
		}

		public JsonOutput getOut() {
			return out;
		}

		public void setOut(JsonOutput out) {
			this.out = out;
		}

		public int getPagenum() {
			return pagenum;
		}

		public void setPagenum(int pagenum) {
			this.pagenum = pagenum;
		}

		public int getPagesize() {
			return pagesize;
		}

		public void setPagesize(int pagesize) {
			this.pagesize = pagesize;
		}
		
}
