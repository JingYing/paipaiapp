package com.qq.qqbuy.discover.biz.impl;

import java.nio.ByteBuffer;

/**
 * 0x2 + syshead(固定长度) + data + 0x3
 * @author JingYing 2014-9-3
 *
 */
public class STPolicyMsg {
	
	private STPolicySysHead sysHead;
	private byte[] data;
	
	public STPolicyMsg(STPolicySysHead sysHead, byte[] data) {
		this.sysHead = sysHead;
		this.data = data;
	}

	public byte[] toByteArray()	{
		int len = 1 + STPolicySysHead.length() + data.length + 1;		//计算消息体总长度
		sysHead.setAllMsglength(len);	
		return ByteBuffer.allocate(len)
			.put((byte)0x2)
			.put(sysHead.toByteArray())
			.put(data)
			.put((byte)0x3)
			.array();
	}
	
	/**
	 * 从BI返回的消息体中, 剥离最前面的2, 最后的3, 再剥离STPolicySysHead, 剩下的就是protobuf可以解析的byte[]
	 * @param bytes
	 */
	public static byte[] parseProtobufFromResp(byte[] bytes)	{
		int targetByteLen = bytes.length - 1 - STPolicySysHead.length() - 1;
		byte[] dest = new byte[targetByteLen];
		System.arraycopy(bytes, 1 + STPolicySysHead.length(), dest, 0, targetByteLen);
		return dest;
	}
	
	/**
	 * 从BI返回的消息体中, 解析STPolicySysHead
	 * @param bytes
	 * @return
	 */
	public static byte[] parseSysheadFromResp(byte[] bytes)	{
		byte[] dest = new byte[STPolicySysHead.length()];
		System.arraycopy(bytes, 1, dest, 0, STPolicySysHead.length());
		return dest;
	}

}
