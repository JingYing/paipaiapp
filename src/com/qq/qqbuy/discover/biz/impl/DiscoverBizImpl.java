package com.qq.qqbuy.discover.biz.impl;

import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_机器码;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_消息ID策略ID;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_纬度;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_经度;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.ECHO_PARAM_设备类型;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.SCENE_发现页商品推荐;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.SCENE_大场景ID;
import static com.qq.qqbuy.discover.biz.impl.BIConstant.SCENE_店铺推荐小场景ID;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.paipai.component.configagent.ConfigHelper;
import com.qq.qqbuy.bi.biz.Dap;
import com.qq.qqbuy.bi.biz.Scene;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.Uint32;
import com.qq.qqbuy.common.client.http.HttpClient;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.log.PacketLogParser;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.ItemCode;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.discover.biz.DiscoverBiz;
import com.qq.qqbuy.discover.biz.ItemVo;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyBody;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyCmd;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyEchoParam;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyHead;
import com.qq.qqbuy.discover.biz.impl.EcDataPolicyProtocV1.CProPolicyItem;
import com.qq.qqbuy.discover.util.Constants;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.idl.item.ItemClient;
import com.qq.qqbuy.thirdparty.idl.item.protocol.FetchItemInfoResp;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
public class DiscoverBizImpl implements DiscoverBiz{
	static Logger log = LogManager.getLogger(DiscoverBizImpl.class);

	private static final String MART_ACTIVE_SHOW_URL = "http://opr.paipai.com/mart/activeshow";
	
	private ShopBiz shopBiz;
	private Random rand = new Random();
	
	@Override
	public List<ItemVo> discoverItem(String clientIp, String deviceType, String mk, 
										String longitude, String latitude, long wid, 
										int[] poolIds, int pageSize) throws Exception	{
		
		if(poolIds == null || poolIds.length == 0)	{
			poolIds = new int[]{10000};		//如果poolIds无值, 则对sceneId只传主场景+小场景+10000
		}
		
		List<CProPolicyBody> bodys = new ArrayList<CProPolicyBody>();
		for(int poolId : poolIds)	{
			CProPolicyBody b = CProPolicyBody.newBuilder()
			.setUllSceneID(new Scene(SCENE_大场景ID, SCENE_发现页商品推荐, poolId).longValue())
			.setUiItemNum(pageSize).setUiPageNo(1).setUiPageSize(pageSize)
			.setUiParamNum(4)
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_机器码).setStrValue(mk).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_设备类型).setStrValue(deviceType).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_经度).setStrValue(longitude==null?"":longitude).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_纬度).setStrValue(latitude==null?"":latitude).build())
			.build();
			bodys.add(b);
		}
		
		CProPolicyHead head = CProPolicyHead.newBuilder()
		.setUiVersion(1)
		.setUiUin((int)wid)
		.setUiClientIP((int)Util.convertIp(clientIp))
		.setUiBodyNum(bodys.size()).build();
		final CProPolicyCmd cmd = CProPolicyCmd.newBuilder()
				.setProHead(head).addAllProBody(bodys).build();
		return parseItemFromCmd(wrapAndSend(cmd));
	}
	
	private List<ItemVo> parseItemFromCmd(CProPolicyCmd cmd)	{
		String msgId = "0";
		long policyId = 0L;
		int scene1 = 0, scene2 = 0;
		int count = cmd.getProBody(0).getUiParamNum();
		if(count > 0)	{
			for(CProPolicyEchoParam p : cmd.getProBody(0).getProParamList())	{
				if(ECHO_PARAM_消息ID策略ID.equals(p.getStrKey()))	{
					String[] arr = p.getStrValue().split(":");
					msgId = arr[0];
					Scene scene = Scene.parse(arr[1]);
					scene1 = scene.getI1();
					scene2 = scene.getI2();
					policyId = Long.parseLong(arr[2]);
					break;
				}
			}
		}
		
		List<CProPolicyItem> items = cmd.getProBody(0).getProItemList();
		List<ItemVo> vos = new ArrayList<ItemVo>();
		for(CProPolicyItem i : items)	{
			try {
				if(i.getUllItemID() == 0)	continue;	//proItem中有为空的情况
				long sellerUin = new Uint32(i.getUiSellUin()).longValue();	//把超出int值的负数还原为long 
				ItemVo vo = new ItemVo();
				vo.itemId = i.getUllItemID();
				vo.sellerUin = sellerUin;
				vo.score = i.getUiScore();
				vo.tabId = i.getUiReserved(0);
				vo.poolId = i.getUiReserved(5);
				String sceneId = new Scene(scene1, scene2, i.getUiReserved(5)).toString();
				Dap dap = new Dap(msgId, sceneId, policyId, new ItemCode(sellerUin, i.getUllItemID()).toString());
				vo.dap = dap.to62Str();
				vos.add(vo);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		return vos;
	}
	
	
	@Override
	public List<Dap> discoverShop(String clientIp, String deviceType, String mk, 
									String longitude, String latitude, long wid, 
									int pageSize)  throws IOException	{
		CProPolicyBody body = CProPolicyBody.newBuilder()
				.setUllSceneID(new Scene(SCENE_大场景ID, SCENE_店铺推荐小场景ID, 0).longValue())
				.setUiItemNum(pageSize).setUiPageNo(1).setUiPageSize(pageSize)
				.setUiParamNum(4)
				.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_机器码).setStrValue(mk).build())
				.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_设备类型).setStrValue(deviceType).build())
				.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_经度).setStrValue(longitude==null?"":longitude).build())
				.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_纬度).setStrValue(latitude==null?"":latitude).build())
				.build();
		CProPolicyHead head = CProPolicyHead.newBuilder()
				.setUiVersion(1)
				.setUiUin((int)wid)
				.setUiClientIP((int)Util.convertIp(clientIp))
				.setUiBodyNum(1).build();
		final CProPolicyCmd request = CProPolicyCmd.newBuilder()
			.setProHead(head).addProBody(body).build();
		CProPolicyCmd response = wrapAndSend(request);
		
		String msgId = null;
		Scene sce = null;
		long policyId = 0;
		List<Dap> shopDaps = new ArrayList<Dap>();
		if(response.getProBody(0).getUiParamNum() > 0)	{
			for(CProPolicyEchoParam p : response.getProBody(0).getProParamList())	{
				if(ECHO_PARAM_消息ID策略ID.equals(p.getStrKey()))	{
					String[] arr = p.getStrValue().split(":");
					msgId = arr[0];
					sce = Scene.parse(arr[1]);
					policyId = Long.parseLong(arr[2]);
					break;
				}
			}
			for(CProPolicyItem p : response.getProBody(0).getProItemList())	{
				if(p.getUllItemID() != 0)	{
					shopDaps.add(new Dap(msgId, sce.toString(), policyId, p.getUllItemID()+""));
				}
			}
		} else	{
			Log.run.error("uiParamNum=" + response.getProBody(0).getUiParamNum());
		}
		return shopDaps;
	}
	
	
	/**
	 * 把protobuf封装成BI消息格式, 发送, 接收, 解析出BI消息, 再解析出protobuf
	 * 另外加入
	 * @param request
	 * @return CProPolicyCmd 解析后的protobuf
	 * @throws IOException
	 */
	public CProPolicyCmd wrapAndSend(final CProPolicyCmd request) throws IOException	{
		String ipport = "10.209.16.111:8684";
		if(EnvManager.isIdc())	{
			InetSocketAddress addr = ConfigHelper.getSvcAddressBySet(8080, "pp_main_srv", 0, rand.nextInt(100));
			ipport = addr.getHostName() + ":" + addr.getPort();
		} 
		STPolicyMsg2 msg = new STPolicyMsg2(request.toByteArray());
		DatagramPacket packet = new UdpClient().sendAndReceive(ipport, msg.toByteArray(), new PacketLogParser() {

			@Override
			public String parseRequest(byte[] req) {
				return request.toString().replace("\n", "\t");	//直接将protobuf的string类型传给解析器,不需要二次解析
			}

			@Override
			public String parseResponse(byte[] response) throws Exception {
				CProPolicyCmd cmd = CProPolicyCmd.parseFrom(STPolicyMsg2.parseProtobufFromResp(response));
				return cmd.toString().replace("\n", "\t");
			}

			@Override
			public String getServiceName() {
				return "pp_main_srv";
			}
		}, null);
		
		return CProPolicyCmd.parseFrom(STPolicyMsg2.parseProtobufFromResp(packet.getData()));
	}
	
	
	@Override
	public JsonElement discoverAct() throws IOException {
		String url = "http://www.paipai.com/sinclude/app_activity_list.js";
		return fromPpms(url).getAsJsonObject().get("data");
	}

	@Override
	public JsonElement discoverBanner() throws IOException  {
		String url = "http://www.paipai.com/sinclude/app_discover_banner.js";
		return fromPpms(url).getAsJsonObject();
	}
	
	@Override
	public JsonElement discoverSets() throws IOException  {
		String url = "http://www.paipai.com/sinclude/app_discover_setting.js";
		return fromPpms(url).getAsJsonObject();
	}
	
	private JsonElement fromPpms(String url) throws IOException	{
		HttpResp resp = new HttpClient().get(url, null, new PacketLogParser() {
			
			@Override
			public String parseResponse(byte[] response) throws Exception {
				if(response == null)	return null;
				return new String(response, "GBK");
			}
			
			@Override
			public String parseRequest(byte[] request) throws Exception {
				return null;
			}
			
			@Override
			public String getServiceName() {
				return null;
			}
		});
		BufferedInputStream is = null;
		if("gzip".equalsIgnoreCase(resp.getRespHead().getContentEncoding()))	{
			is = new BufferedInputStream(new GZIPInputStream(new ByteArrayInputStream(resp.getResponse())));
		} else	{
			is = new BufferedInputStream(new ByteArrayInputStream(resp.getResponse()));
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i;
		while((i=is.read()) != -1)	{
			baos.write(i);
		}
		String s = new String(baos.toByteArray(), "GBK");
		JsonObject json;
		try {
			String s1 = s.replaceAll(";/[*].*[*]/","");
			json = new JsonParser().parse(s1).getAsJsonObject();
		} catch (JsonSyntaxException e) {
			String s2 = s.substring(s.indexOf("{"), s.lastIndexOf("}") + 1);
			json = new JsonParser().parse(s2).getAsJsonObject();
		}
		return json;
		
	}

	/**
	 * @author wanghao
	 * @param poolIds 获取商品的商品池ID数组
	 * @param pageSize 显示商品总数量
	 * @return 返回商品列表（只包含基本信息）
	 */
	@Override
	public List<ItemVo> discoverRecommendItems(String clientIp,
			String deviceType, String mk, String longtitude, String latitude,
			long wid, int[] poolIds, int pageSize) throws Exception {
		//组装 protocol buffer协议（谷歌的一个协议）
		//CProPolicyBody是提供接口的人生成的
		List<CProPolicyBody> bodys = new ArrayList<CProPolicyBody>();
		for(int poolId : poolIds)	{
			CProPolicyBody body1 = CProPolicyBody.newBuilder()
			.setUllSceneID(new Scene(SCENE_大场景ID, SCENE_发现页商品推荐, poolId).longValue())
			.setUiItemNum(pageSize).setUiPageNo(1).setUiPageSize(pageSize)
			.setUiParamNum(4)
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_机器码).setStrValue(mk).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_设备类型).setStrValue(deviceType).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_经度).setStrValue(longtitude).build())
			.addProParam(CProPolicyEchoParam.newBuilder().setStrKey(ECHO_PARAM_纬度).setStrValue(latitude).build())
			.build();
			bodys.add(body1);
		}
		
		CProPolicyHead head = CProPolicyHead.newBuilder()
			.setUiVersion(1)
			.setUiUin((int)wid)
			.setUiClientIP((int)Util.convertIp(clientIp))
			.setUiBodyNum(bodys.size()).build();
		
		CProPolicyCmd cmd = CProPolicyCmd.newBuilder()
			.setProHead(head).addAllProBody(bodys).build();
		
		CProPolicyCmd response = wrapAndSend(cmd);
		return parseItemFromCmd(response);
	}

	@Override
	public JsonArray recommendGoodsPPY(Long wid, String clientIp, int pageSize)
			throws Exception {
		// TODO Auto-generated method stub
		JsonArray jsonArray = new JsonArray() ;
		try {
			String url = Constants.DISCOVER_SHOW_URL.replace(Constants.DISCOVER_BI_HOSTS, PaiPaiConfig.getPaipaiCommonIp()).replace("$SF", Constants.RECOMMEND_GOODS_PPY_SF)
					.replace("$HID", Constants.RECOMMEND_GOODS_PPY_HID).replace("$MID", Constants.RECOMMEND_GOODS_PPY_MID)
					.replace("$TYPE", Constants.RECOMMEND_GOODS_PPY_TYPE).replace("$UIN", String.valueOf(wid))
					.replace("$IP", clientIp).replace("$PAGESIZE", String.valueOf(pageSize)) ;
			String strResult = HttpUtil.get(url,Constants.DISCOVER_BI_HOSTS, Constants.connectTimeout, Constants.readTimeout,
		            "GBK", false);
			JsonParser jp = new JsonParser();
			JsonObject jsonResult = (JsonObject)jp.parse(strResult);
			if (jsonResult.get("retCode").getAsString().equals("0")) {
				JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
				for (int i=0;i<jsonArrayTmp.size();i++) {
					JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
					JsonObject jsonObject = new JsonObject() ;
					jsonObject.addProperty("itemCode", jsonObjectTmp.get("strCommodityId").getAsString());					
					jsonObject.addProperty("itemName", jsonObjectTmp.get("strTitle").getAsString());
					jsonObject.addProperty("price", jsonObjectTmp.get("dwPrice").getAsNumber());
					jsonObject.addProperty("activePrice", jsonObjectTmp.get("dwActivePrice").getAsNumber());
					jsonObject.addProperty("itemPic", jsonObjectTmp.get("strItemPic").getAsString());
					jsonObject.addProperty("tuanNum", jsonObjectTmp.get("dwTuanNum").getAsNumber());
					
					try {
						String[] split = jsonObjectTmp.get("strDap").getAsString().split(":");
						Dap dap = new Dap(split[0], split[1], Long.parseLong(split[2]), split[3]);
						String to62Str = dap.to62Str();
						jsonObject.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));		
					} catch (NumberFormatException e) {
						Log.run.error(e.getMessage(), e);
					} catch (Exception e) {
						Log.run.error(e.getMessage(), e);
					}
					
					
					jsonArray.add(jsonObject);
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.run.info(ex.toString()) ;
		}
		return jsonArray;
	}	

	@Override
	public JsonArray recommendGoodsGlobal(Long wid, String clientIp,
			int pageSize) throws Exception {
		// TODO Auto-generated method stub
		JsonArray jsonArray = new JsonArray() ;
		try {
			String url = Constants.DISCOVER_SHOW_URL.replace(Constants.DISCOVER_BI_HOSTS, PaiPaiConfig.getPaipaiCommonIp()).replace("$SF", Constants.RECOMMEND_GOODS_GLOBAL_SF)
					.replace("$HID", Constants.RECOMMEND_GOODS_GLOBAL_HID).replace("$MID", Constants.RECOMMEND_GOODS_GLOBAL_MID)
					.replace("$TYPE", Constants.RECOMMEND_GOODS_GLOBAL_TYPE).replace("$UIN", String.valueOf(wid))
					.replace("$IP", clientIp).replace("$PAGESIZE", String.valueOf(pageSize)) ;
			
			String strResult = HttpUtil.get(url,Constants.DISCOVER_BI_HOSTS, Constants.connectTimeout, Constants.readTimeout,
		            "GBK", false);
			JsonParser jp = new JsonParser();
			JsonObject jsonResult = (JsonObject)jp.parse(strResult);
			if (jsonResult.get("retCode").getAsString().equals("0")) {
				JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
				for (int i=0;i<jsonArrayTmp.size();i++) {
					JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
					JsonObject jsonObject = new JsonObject() ;
					String itemCode = jsonObjectTmp.get("strCommodityId").getAsString();
					jsonObject.addProperty("itemCode", itemCode);		
					jsonObject.addProperty("itemName", jsonObjectTmp.get("strTitle").getAsString());
					jsonObject.addProperty("price", jsonObjectTmp.get("dwPrice").getAsNumber());
					jsonObject.addProperty("activePrice", jsonObjectTmp.get("dwActivePrice").getAsNumber());
					jsonObject.addProperty("itemPic", jsonObjectTmp.get("strItemPic").getAsString());
					
					try {
						String[] split = jsonObjectTmp.get("strDap").getAsString().split(":");
						Dap dap = new Dap(split[0], split[1], Long.parseLong(split[2]), split[3]);
						String to62Str = dap.to62Str();
						jsonObject.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));		
					} catch (NumberFormatException e) {
						Log.run.error(e.getMessage(), e);
					} catch (Exception e) {
						Log.run.error(e.getMessage(), e);
					}
					
					Long buyNum = 0l ;
					FetchItemInfoResp  resp = ItemClient.fetchItemInfo(itemCode);
					if (resp != null && resp.getResult() == 0) {
						buyNum = resp.getItemPo().getOItemNum().getToalBuyNum() ;
					}					
					jsonObject.addProperty("buyNum", buyNum);
					jsonArray.add(jsonObject);
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.run.info(ex.toString()) ;
		}
		return jsonArray;
	}

	@Override
	public JsonArray recommendShopWD(Long wid, String clientIp,
			int pageSize, String mk) throws Exception {
		// TODO Auto-generated method stub
		JsonArray jsonArray = new JsonArray() ;
		try {
			String url = Constants.DISCOVER_SHOW_URL.replace(Constants.DISCOVER_BI_HOSTS, PaiPaiConfig.getPaipaiCommonIp())
					.replace("$SF", Constants.RECOMMEND_SHOP_WD_SF)
					.replace("$HID", Constants.RECOMMEND_SHOP_WD_HID).replace("$MID", Constants.RECOMMEND_SHOP_WD_MID)
					.replace("$TYPE", Constants.RECOMMEND_SHOP_WD_TYPE).replace("$UIN", String.valueOf(wid))
					.replace("$IP", clientIp).replace("$PAGESIZE", String.valueOf(pageSize)) ;
			String strResult = HttpUtil.get(url, Constants.DISCOVER_BI_HOSTS, Constants.connectTimeout, Constants.readTimeout,
		            "GBK", false);
			
			Log.run.info("recommendShopWD...strResult::"+strResult) ;
			
			JsonParser jp = new JsonParser();
			JsonObject jsonResult = (JsonObject)jp.parse(strResult);
			
			if (jsonResult.get("retCode").getAsString().equals("0")) {
				JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
				for (int i=0;i<jsonArrayTmp.size();i++) {
					JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
					JsonObject jsonObject = new JsonObject() ;					
					Long shopId = jsonObjectTmp.get("ddwSellerUin").getAsLong();
					jsonObject.addProperty("shopId", shopId);
					jsonObject.addProperty("shopLogo", jsonObjectTmp.get("strShopPic").getAsString());
					jsonObject.addProperty("shopName", jsonObjectTmp.get("strShopName").getAsString());
					
					try {
						String[] split = jsonObjectTmp.get("strDap").getAsString().split(":");
						Dap dap = new Dap(split[0], split[1], Long.parseLong(split[2]), split[3]);
						String to62Str = dap.to62Str();
						jsonObject.addProperty("dap", to62Str.substring(0, to62Str.lastIndexOf(":")));		
					} catch (NumberFormatException e) {
						Log.run.error(e.getMessage(), e);
					} catch (Exception e) {
						Log.run.error(e.getMessage(), e);
					}
					
					jsonObject.addProperty("newItemCount", shopBiz.getNewItemCountAndCache(shopId, mk));	//使用缓存
					//查询最新的4个商品
					Map<String, String> filter2 = new HashMap<String, String>();
					filter2.put(CVItemFilterConstants.ITEM_FILTER_KEY_STATE, 
							CVItemFilterConstants.ITEM_FILTER_STATE_SELLING);	// 只查询出售中的商品
					ShopItems items = shopBiz.getShopComdyList(shopId, 
							mk, 0, 4, 
							CVItemFilterConstants.C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC, 
							filter2);
					
					int minSize = Math.min(4, items.getItems().size()) ;
					JsonArray jsonArrayTmp2 = new JsonArray() ;
					for (int j=0 ; j< minSize; j++) {
						JsonObject jsonObjectTemp2 = new JsonObject() ;
						ShopSimpleItem  simpleItem = items.getItems().get(j) ;
						jsonObjectTemp2.addProperty("itemCode", simpleItem.getItemId());
						jsonObjectTemp2.addProperty("itemName", simpleItem.getTitle());
						jsonObjectTemp2.addProperty("soldNum", simpleItem.getSoldNum());
						jsonObjectTemp2.addProperty("price", simpleItem.getPrice());
						jsonObjectTemp2.addProperty("itemPic", simpleItem.getImage120());
						jsonArrayTmp2.add(jsonObjectTemp2) ;
					}
					jsonObject.add("itemList", jsonArrayTmp2);
					jsonArray.add(jsonObject);
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.run.info(ex.toString()) ;
		}
		return jsonArray;
	}
	
	/**
	 * p_jdhongle(勒宏)
	 * p_jdjianjin(金健) 
	 */
	@Override
	public JsonObject recommendMyFavShop(Long wid, String clientIp, int pageSize, String mk) throws Exception {
		JsonArray favShop = new JsonArray();
		JsonArray jsonObjectDap = new JsonArray();
		JsonObject jsonObjectResult = new JsonObject();
		
		try {
			String url = Constants.DISCOVER_SHOW_URL.replace(Constants.DISCOVER_BI_HOSTS, PaiPaiConfig.getPaipaiCommonIp())
					.replace("$SF", "1417487190")
					.replace("$HID", Constants.RECOMMEND_SHOP_WD_HID).replace("$MID", "1013")//新分配的场景
					.replace("$TYPE", Constants.RECOMMEND_SHOP_WD_TYPE).replace("$UIN", String.valueOf(wid))
					.replace("$IP", clientIp).replace("$PAGESIZE", String.valueOf(pageSize)) ;
			Log.run.debug("recommendMyFavShop_url："+url);
			String strResult = HttpUtil.get(url, Constants.DISCOVER_BI_HOSTS, Constants.connectTimeout, Constants.readTimeout,
		            "GBK", false);
			
			Log.run.debug("recommendMyFavShop_strResult："+strResult);
			
			JsonParser jp = new JsonParser();
			JsonObject jsonResult = (JsonObject)jp.parse(strResult);
			
			if (jsonResult.get("retCode").getAsString().equals("0")) {
				JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
				
				for (int i=0;i<jsonArrayTmp.size();i++) {
					JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
					
					String dap62WithNoIc = "";
					try {
						String[] split = jsonObjectTmp.get("strDap").getAsString().split(":");
						Dap dap = new Dap(split[0], split[1], Long.parseLong(split[2]), split[3]);
						String to62Str = dap.to62Str();
						dap62WithNoIc = to62Str.substring(0, to62Str.lastIndexOf(":"));
					} catch (NumberFormatException e) {
						Log.run.error(e.getMessage(), e);
					} catch (Exception e) {
						Log.run.error(e.getMessage(), e);
					}
					
					jsonObjectDap.add(new JsonPrimitive(dap62WithNoIc));
					favShop .add(new JsonPrimitive(jsonObjectTmp.get("ddwSellerUin").getAsLong()));
				}
			}
		} catch(Exception ex) {
			Log.run.error(ex.getMessage(), ex) ;
		}
		
		jsonObjectResult.add("dap", jsonObjectDap);
		jsonObjectResult.add("sub", favShop);
		return jsonObjectResult;
	}

	/**
	 * 获取海外购推荐商品
	 *
	 * @param activeId
	 * @param poolId
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	@Override
	public JsonArray discoverMartItems(long activeId, int poolId, int pageSize,int sortType) throws Exception {
		JsonArray jsonArray = new JsonArray() ;
		StringBuffer sb = new StringBuffer();
		sb.append(MART_ACTIVE_SHOW_URL);
		sb.append("?ids=").append(activeId).append("&sort=").append(sortType).append("&ctl=").append(poolId).append(":").append(pageSize);
		String url = sb.toString();
		String strResult = HttpUtil.get(url , Constants.connectTimeout, Constants.readTimeout,
				"GBK", true);
		String result = strResult.substring(strResult.indexOf("try{activeshow_callback(") + 26, strResult.indexOf("})") + 1);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonResult = (JsonObject)jsonParser.parse(result);
		if(jsonResult.get("errCode").getAsInt() != 0){
			return jsonArray;
		}
		JsonArray tabs = jsonResult.get("data").getAsJsonArray()//
				.get(0).getAsJsonObject()//
				.get("vecAreas").getAsJsonArray()//
				.get(0).getAsJsonObject()//
				.get("vecTabs").getAsJsonArray();
		for (int i = 0; i < tabs.size(); i++) {
			JsonObject obj = new JsonObject();
			JsonObject tab = tabs.get(i).getAsJsonObject();
			String itemCode = tab.get("strCommodityId").getAsString();
			String price = tab.get("dwPrice").getAsString();
			String activePrice = tab.get("dwActivePrice").getAsString();
			String shopName = tab.get("strShopName").getAsString();
			String title = tab.get("strTitle").getAsString();
			long shopId = tab.get("ddwSellerUin").getAsLong();
			String itemUrl = tab.get("strDetailUrl").getAsString();
			String uploadPicUrl1 = tab.get("mapCustomData").getAsJsonObject().get("uploadPicUrl1").getAsString();
			obj.addProperty("itemCode",itemCode);
			obj.addProperty("price",price);
			obj.addProperty("activePrice",activePrice);
			obj.addProperty("shopName",shopName);
			obj.addProperty("title",title);
			obj.addProperty("shopId",shopId);
			obj.addProperty("url",itemUrl);
			obj.addProperty("uploadPicUrl1", uploadPicUrl1);
			jsonArray.add(obj);
		}
		return jsonArray;
	}

	public ShopBiz getShopBiz() {
		return shopBiz;
	}

	public void setShopBiz(ShopBiz shopBiz) {
		this.shopBiz = shopBiz;
	}

	public static void main(String[] args) {
		JsonArray jsonArray = new JsonArray() ;
//		String url = Constants.DISCOVER_SHOW_URL.replace("$SF", "1416471665")
//				.replace("$HID", "3002").replace("$MID", "1006")
//				.replace("$TYPE", "0").replace("$UIN", "469524051")
//				.replace("$IP", "127.0.0.1").replace("$PAGESIZE", "5") ;
//		System.out.println(url );
		StringBuffer sb = new StringBuffer();
		sb.append(MART_ACTIVE_SHOW_URL);
		sb.append("?ids=").append(35090).append("&sort=").append(0).append("&ctl=").append("28008:100");
		String url = sb.toString();
		System.out.println(url);
		String strResult = HttpUtil.get(url , Constants.connectTimeout, Constants.readTimeout,
				"GBK", false);
		System.out.println(strResult);
		String result = strResult.substring(strResult.indexOf("try{activeshow_callback(") + 26, strResult.indexOf("})") + 1);
		System.out.println(result);
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonResult = (JsonObject)jsonParser.parse(result);
		if(jsonResult.get("errCode").getAsInt() != 0){
			String errorMsg = jsonResult.get("errMsg").getAsString();
			return;
		}
		JsonArray tabs = jsonResult.get("data").getAsJsonArray()//
				.get(0).getAsJsonObject()//
				.get("vecAreas").getAsJsonArray()//
				.get(0).getAsJsonObject()//
				.get("vecTabs").getAsJsonArray();
		JsonArray jsons = new JsonArray();
		for (int i = 0; i < tabs.size(); i++) {
			JsonObject obj = new JsonObject();
			JsonObject tab = tabs.get(i).getAsJsonObject();
			String itemCode = tab.get("strCommodityId").getAsString();
			String shopName = tab.get("strShopName").getAsString();
			String title = tab.get("strTitle").getAsString();
			long shopId = tab.get("ddwSellerUin").getAsLong();
			String itemurl = tab.get("strDetailUrl").getAsString();
			String uploadPicUrl1 = tab.get("mapCustomData").getAsJsonObject().get("uploadPicUrl1").getAsString();
			obj.addProperty("itemCode",itemCode);
			obj.addProperty("shopName",shopName);
			obj.addProperty("title",title);
			obj.addProperty("shopId",shopId);
			obj.addProperty("url",itemurl);
			obj.addProperty("uploadPicUrl1",uploadPicUrl1);
			jsons.add(obj);
			System.out.println(tab.get("strShopName").getAsString());
		}
		/*JsonParser jp = new JsonParser();
		JsonObject jsonResult = (JsonObject)jp.parse(strResult);
		if (jsonResult.get("retCode").getAsString().equals("0")) {
			JsonArray jsonArrayTmp = jsonResult.get("data").getAsJsonObject().get("111").getAsJsonArray() ;
			for (int i=0;i<jsonArrayTmp.size();i++) {
				JsonObject jsonObjectTmp = jsonArrayTmp.get(i).getAsJsonObject() ;
				JsonObject jsonObject = new JsonObject() ;
				jsonObject.addProperty("strTitle", jsonObjectTmp.get("strTitle").getAsString());
				jsonObject.addProperty("dwPrice", jsonObjectTmp.get("dwPrice").getAsNumber());
				jsonObject.addProperty("dwActivePrice", jsonObjectTmp.get("dwActivePrice").getAsNumber());
				jsonObject.addProperty("strItemPic", jsonObjectTmp.get("strItemPic").getAsString());
				jsonObject.addProperty("dwTuanNum", jsonObjectTmp.get("dwTuanNum").getAsNumber());
				jsonArray.add(jsonObject);
			}
		}*/
//		System.out.println(jsonArray);
	}

}
