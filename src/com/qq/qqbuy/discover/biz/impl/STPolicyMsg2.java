package com.qq.qqbuy.discover.biz.impl;

import java.nio.ByteBuffer;

/**
 * 0x2 + 包总长度  + data + 0x3
 * @author JingYing 2014-9-3
 *
 */
public class STPolicyMsg2 {
	
	private byte[] data;
	
	public STPolicyMsg2(byte[] data) {
		this.data = data;
	}

	/**
	 * 封装报文信息
	 * @return
	 */
	public byte[] toByteArray()	{
		int len = 1 + 4 + data.length + 1;		//计算消息体总长度
		return ByteBuffer.allocate(len)
			.put((byte)0x2)
			.putInt(len)
			.put(data)
			.put((byte)0x3)
			.array();
	}
	
	/**
	 * 从BI返回的消息体中, 剥离最前面的2, 最后的3, 再剥离长度, 剩下的就是protobuf可以解析的byte[]
	 * @param bytes
	 */
	public static byte[] parseProtobufFromResp(byte[] bytes)	{
		int targetByteLen = bytes.length - 1 - 4 - 1;
		byte[] dest = new byte[targetByteLen];
		System.arraycopy(bytes, 1 + 4, dest, 0, targetByteLen);
		return dest;
	}
}
