package com.qq.qqbuy.favorite.biz.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.common.util.ImageUrlUtil;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.favorite.po.FavoriteItemPo;
import com.qq.qqbuy.favorite.po.FavoriteItemsPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetailPo;
import com.qq.qqbuy.favorite.po.FavoriteShopDetails;
import com.qq.qqbuy.favorite.po.FavoriteUpdatePo;
import com.qq.qqbuy.thirdparty.idl.favorite.FavoriteClient;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.AddFavShopResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavShopListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.DelFavShopListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.FavFilterPo;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.FavItemPo;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.FavShopListPo;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.FavShopPo;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavItemListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavItemListResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopDetailResp;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopListReq;
import com.qq.qqbuy.thirdparty.idl.favorite.protocol.GetFavShopListResp;
import com.qq.qqbuy.thirdparty.idl.item.protocol.eval.EvalStat;
import com.qq.qqbuy.thirdparty.idl.shop.ShopClient;
import com.qq.qqbuy.thirdparty.idl.stat.StatClient;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdResp;

public class FavoriteBizImpl implements FavoriteBiz
{

	static Logger log = LogManager.getLogger(FavoriteBizImpl.class);
	
    @Override
    public FavoriteUpdatePo addFavItemList(String[] itemcodes, long uin, String clientIp, String sk, String reserve)
    {
        FavoriteUpdatePo ret = new FavoriteUpdatePo();
        ret.setItems(itemcodes) ;
        // 1、参数检查
        if (uin > 10000 && sk != null
                && sk.length() == 10 && itemcodes != null
                && itemcodes.length>0)
        {
            // 2\ 初使化参数
//            AddFavItemListReq req = new AddFavItemListReq();
//            req.setAddItemIdList(addItemIdList);
//            req.setMechineKey(mk);
//            req.setUin(uin);
//            // 3、这里要把skey传到stub里面去
//            req.setInReserve(sk);

            AddFavItemListResp resp = FavoriteClient.addFavItemList(itemcodes, uin, clientIp, sk, reserve);

            // 4、组装返回数据
            if (resp != null && resp.result == 0)
            {
                Vector<uint32_t> results = null;
                if ((results = resp.getAddResult()) != null)
                {
                    List<Long> items = new ArrayList<Long>(results.size());
                    for (uint32_t item : results)
                    {
                        items.add(item.longValue());
                    }
                    ret.setResult(items);
                }
                ret.setTotalCount(resp.getTotalCount());
            } else
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrMsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }
        return ret;
    }

    @Override
    public CommonPo delFavItemList(long uin, String mk, String sk,
            Vector<String> delItemIdList)
    {
        CommonPo ret = new CommonPo();

        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && delItemIdList != null
                && !delItemIdList.isEmpty())
        {
            // 2\ 初使化参数
            DelFavItemListReq req = new DelFavItemListReq();
            req.setDelItemIdList(delItemIdList);
            req.setMechineKey(mk);
            req.setUin(uin);
            // 3、这里要把skey传到stub里面去
            req.setInReserve(sk);

            DelFavItemListResp resp = FavoriteClient.delFavItemList(req);

            // 4、组装返回数据
            if (resp == null || resp.result != 0)
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrMsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }
        return ret;
    }
    
    @Override
    public List<FavoriteItemPo> getAllFavItem(long uin, String mk, String sk)	{
    	int ps = 40;	//貌似不能取太大值,否则AO
		FavoriteItemsPo po = this.getFavItemList(uin, mk, sk, 0, ps);
		
		int totalPage = (int)po.getTotal()/ps;
    	if(po.getTotal() % ps > 0)	{
    		totalPage += 1;
    	}
    	int maxPage = totalPage-1;
    	if(maxPage < 0)			maxPage = 0;
		List<FavoriteItemPo> all = po.getFavItemList();
		for(int i=1; i<=maxPage; i++)	{
			all.addAll(this.getFavItemList(uin, mk, sk, i, ps).getFavItemList());
		}
		return all;
    }
    
    @Override
    public FavoriteItemsPo getFavItemList(long uin, String mk, String sk,
            int startPage, int pageSize)
    {
    	
        FavoriteItemsPo ret = new FavoriteItemsPo();

        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && startPage >= 0 && pageSize > 0)
        {
            // 2、初使化参数
            GetFavItemListReq req = new GetFavItemListReq();
            FavFilterPo filter = new FavFilterPo();
            req.setMechineKey(mk);
            filter.setUin(uin);
            filter.setStartPage(startPage + 1);
            filter.setPageSize(pageSize);
            req.setFavItemFiter(filter);
            // 3、这里要把skey传到stub里面去
            req.setInReserve(sk);

            GetFavItemListResp resp = FavoriteClient.getFavItemList(req);

            // 4、组装返回数据
            if (resp != null && resp.result == 0)
            {
                Vector<FavItemPo> favItemList = null;
                if (resp.getFavItemList() != null
                        && (favItemList = resp.getFavItemList()
                                .getFavItemList()) != null)
                {
                    List<FavoriteItemPo> favItems = new ArrayList<FavoriteItemPo>(
                            favItemList.size());
                    for (FavItemPo item : favItemList)
                    {
                        if(item == null )
                        {
                            continue;
                        }
                        
                        FavoriteItemPo favItem = new FavoriteItemPo();
                        favItem.setAddTime(item.getAddTime());
                        String itemCode = item.getItemId() ;
                        favItem.setItemId(itemCode);
                        GetFavoritesByIdResp statResp = StatClient.findFavCount(Long.parseLong(itemCode.substring(itemCode.length()-16), 16)) ;
        				Map<uint32_t,Integer>  storeMap = statResp.getStatData() ;
        				uint32_t key = new uint32_t(1) ;
        				if (storeMap == null || storeMap.get(key) == null) {
        					favItem.setStoreNum(0) ;
        				} else {
        					favItem.setStoreNum(storeMap.get(key)) ;
        				}
                        favItem.setItemPic(ImageUrlUtil.genImageUrl(item.getItemPic(),ImageUrlUtil.DEFAULTFormat160));
                        favItem.setItemPic120(ImageUrlUtil.genImageUrl(item.getItemPic(), ImageUrlUtil.DEFAULTFormat120));
                        favItem.setItemProperty(item.getItemProperty());
                        favItem.setItemTitle(item.getItemTitle());
                        favItem.setOldPrice(item.getOldPrice());
                        favItem.setPrice(item.getPrice());
                        favItem.setState(item.getState());
                        favItems.add(favItem);
                    }
                    ret.setFavItemList(favItems);
                    ret.setTotal(resp.getFavItemList().getTotal());
                }
            } else
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrMsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }
        return ret;
    }

    @Override
    public CommonPo addShop2Fav(long uin, String mk, String sk, long shopId, String reserve)
    {
        CommonPo ret = new CommonPo();
        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && shopId > 10000)
        {
            AddFavShopResp resp = FavoriteClient
                    .addFavShop(uin, shopId, mk, sk, reserve);
            // 4、组装返回数据
            if (resp == null || resp.result != 0)
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrmsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }

        return ret;
    }
    
    @Override
    public boolean isShopInFav(long uin, String mk, String sk, long shopId)		{
    	FavoriteShopDetailPo p = getShopIdFavInfo(uin, mk, sk, shopId);
    	return p.getUserId() == uin ? true : false;
    }

    @Override
    public FavoriteShopDetailPo getShopIdFavInfo(long uin, String mk,
            String sk, long shopId)
    {
        FavoriteShopDetailPo ret = new FavoriteShopDetailPo();
        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && shopId > 10000)
        {
            GetFavShopDetailResp resp = FavoriteClient.getFavShopDetail(uin,
                    shopId, mk, sk);
            // 4、组装返回数据
            FavShopPo po = null;
            if (resp == null || resp.result != 0
                    || (po = resp.getFavShop()) == null)
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrmsg();
            } else
            {
                ret = transferShopPo(po);
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }

        return ret;
    }

    private FavoriteShopDetailPo transferShopPo(FavShopPo po)
    {
        FavoriteShopDetailPo ret = new FavoriteShopDetailPo();
        if (po != null)
        {
            ret.setAddTime(po.getAddTime());
            ret.setShopId(po.getShopId());
            ret.setUserId(po.getUserId());
            ret.setFavoriteHeat(po.getFavoriteHeat());
            ret.setSellerNickName(po.getSellerNickName());
            ret.setShopCredit(po.getShopCredit());
            ret.setShopGoodRate(po.getShopGoodRate());
            ret.setShopLogoPos(ImageUrlUtil.genImageUrlFromLogo(po.getUserId(),po.getShopLogoPos()));
            ret.setShopName(po.getShopName());
        }
        return ret;
    }

    @Override
    public CommonPo delShopItemList(long uin, String mk, String sk,
            Vector<Long> delShopIdList)
    {
        CommonPo ret = new CommonPo();

        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && delShopIdList != null
                && !delShopIdList.isEmpty())
        {
            // 2\ 初使化参数
            DelFavShopListReq req = new DelFavShopListReq();
            req.setDelShopIdList(delShopIdList);
            req.setMechineKey(mk);
            req.setUin(uin);
            // 3、这里要把skey传到stub里面去
            req.setInReserve(sk);

            DelFavShopListResp resp = FavoriteClient.delFavShopList(req);

            // 4、组装返回数据
            if (resp == null || resp.result != 0)
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrMsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }
        return ret;
    }

    @Override
    public FavoriteShopDetails getFavShopList(long uin, String mk, String sk,
            int startPage, int pageSize)
    {
    	
    	Log.run.debug("uin:" + uin + ",mk:" + mk + ",sk:" + sk + ",startPage:" + startPage + ",pageSize:" + pageSize);
        FavoriteShopDetails ret = new FavoriteShopDetails();
        ShopClient shopClient = new ShopClient();
        // 1、参数检查
        if (uin > 10000 && StringUtils.isNotBlank(mk) && sk != null
                && sk.length() == 10 && startPage >= 0 && pageSize > 0)
        {
            // 2\ 初使化参数
            GetFavShopListReq req = new GetFavShopListReq();
            FavFilterPo filter = new FavFilterPo();
            req.setMechineKey(mk);
            filter.setUin(uin);
            filter.setStartPage(startPage + 1);
            filter.setPageSize(pageSize);
            req.setFavShopFiter(filter);
            // 3、这里要把skey传到stub里面去
            req.setInReserve(sk);

            GetFavShopListResp resp = FavoriteClient.getFavShopList(req);

            // 4、组装返回数据
            if (resp != null && resp.result == 0)
            {
                FavShopListPo po = resp.getFavShopList();
                Vector<FavShopPo> favItemList = null;
                if (po != null && (favItemList = po.getFavShopList()) != null)
                {
                    List<FavoriteShopDetailPo> favItems = new ArrayList<FavoriteShopDetailPo>(
                            favItemList.size());
                    for (FavShopPo item : favItemList)
                    {
                        FavoriteShopDetailPo favItem = transferShopPo(item);
                        // 查询店铺评价信息 add by wanghao start
                		try {
                			EvalStat stat = shopClient.getEval(item.getShopId());
        					long totalEval = stat.getDsrTotal() != 0 ? stat.getDsrTotal() : 1;
        					long goodDescriptionMatch = stat.getDsr1Total();
        					long attitudeOfService = stat.getDsr2Total();
        					long speedOfDelivery = stat.getDsr3Total();
        					// 计算评分
        					favItem.setTotalEval((int) (Double.parseDouble(String.format("%.1f",
        							(goodDescriptionMatch + attitudeOfService + speedOfDelivery) * 1.0 / (3 * totalEval))) * 10));
            			} catch (ExternalInterfaceException e) {
            				log.error(e.getMessage()+":收藏店铺评分获取失败！", e);
            				favItem.setTotalEval(0);
            			}
                		//add by wanghao over
                        favItems.add(favItem);
                    }
                    ret.setItems(favItems);
                    ret.setTotal(po.getTotal());
                    
                    Log.run.debug("ret.getItems().size()::"+ret.getItems().size());
                }
            } else
            {
                ret.errCode = resp.getResult();
                ret.msg = resp.getErrMsg();
            }
        } else
        {
            ret.errCode = ErrConstant.ERRCODE_INVALID_PARAMETER;
            ret.msg = "参数校验不合法";
        }
        return ret;
    }

}
