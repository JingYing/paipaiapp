package com.qq.qqbuy.favorite.po;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.common.po.CommonPo;

public class FavoriteItemsPo extends CommonPo
{

    /**
     * 商品信息
     *
     * 版本 >= 0
     */
     private List<FavoriteItemPo> favItemList = new ArrayList<FavoriteItemPo>();

    /**
     * 总数
     *
     * 版本 >= 0
     */
     private long total;

    public List<FavoriteItemPo> getFavItemList()
    {
        return favItemList;
    }

    public void setFavItemList(List<FavoriteItemPo> favItemList)
    {
        this.favItemList = favItemList;
    }

    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    @Override
    public String toString()
    {
        return "FavoriteItemsPo [favItemList=" + favItemList + ", total="
                + total + ", errCode=" + errCode + ", msg=" + msg
                + ", retCode=" + retCode + "]";
    }
}
