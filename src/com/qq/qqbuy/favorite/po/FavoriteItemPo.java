package com.qq.qqbuy.favorite.po;

public class FavoriteItemPo
{
    /**
     * 商品Id
     *
     * 版本 >= 0
     */
     private String itemId = new String();

    /**
     * 收藏时的价格
     *
     * 版本 >= 0
     */
     private long oldPrice;

    /**
     * 当前价格
     *
     * 版本 >= 0
     */
     private long price;

    /**
     * 商品名称
     *
     * 版本 >= 0
     */
     private String itemTitle = new String();

    /**
     * 商品图片
     *
     * 版本 >= 0
     */
     private String itemPic = new String();
     
     /**
      * 商品图片120x120
      */
     private String itemPic120 = new String();

    /**
     * 当前状态
     *
     * 版本 >= 0
     */
     private long state;

    /**
     * 收藏时间
     *
     * 版本 >= 0
     */
     private long addTime;
     /**
      * 商品收藏数
      */
     private int storeNum ;

    /**
     * 属性字段
     *
     * 版本 >= 0
     */
     private String itemProperty = new String();

    public String getItemId()
    {
        return itemId;
    }

    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public long getOldPrice()
    {
        return oldPrice;
    }

    public void setOldPrice(long oldPrice)
    {
        this.oldPrice = oldPrice;
    }

    public long getPrice()
    {
        return price;
    }

    public void setPrice(long price)
    {
        this.price = price;
    }

    public String getItemTitle()
    {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle)
    {
        this.itemTitle = itemTitle;
    }

    public String getItemPic()
    {
        return itemPic;
    }

    public void setItemPic(String itemPic)
    {
        this.itemPic = itemPic;
    }
    
    public String getItemPic120() 
    {
		return itemPic120;
	}

	public void setItemPic120(String itemPic120) 
	{
		this.itemPic120 = itemPic120;
	}

	public long getState()
    {
        return state;
    }

    public void setState(long state)
    {
        this.state = state;
    }

    public long getAddTime()
    {
        return addTime;
    }

    public void setAddTime(long addTime)
    {
        this.addTime = addTime;
    }

    public String getItemProperty()
    {
        return itemProperty;
    }

    public void setItemProperty(String itemProperty)
    {
        this.itemProperty = itemProperty;
    }
    
    

    public int getStoreNum() {
		return storeNum;
	}

	public void setStoreNum(int storeNum) {
		this.storeNum = storeNum;
	}

	@Override
    public String toString()
    {
        return "FavoriteItemPo [addTime=" + addTime + ", itemId=" + itemId
                + ", itemPic=" + itemPic + ", itemPic120=" + itemPic120 +
                ", itemProperty=" + itemProperty + ", storeNum=" + storeNum
                + ", itemTitle=" + itemTitle + ", oldPrice=" + oldPrice
                + ", price=" + price + ", state=" + state + "]";
    }
}
