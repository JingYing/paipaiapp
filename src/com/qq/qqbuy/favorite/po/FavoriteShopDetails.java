package com.qq.qqbuy.favorite.po;

import java.util.ArrayList;
import java.util.List;

import com.qq.qqbuy.common.po.CommonPo;

public class FavoriteShopDetails extends CommonPo
{

    private List<FavoriteShopDetailPo> items = new ArrayList<FavoriteShopDetailPo>();

    /**
     * 总数
     * 
     * 版本 >= 0
     */
    private long total;

    public long getTotal()
    {
        return total;
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public List<FavoriteShopDetailPo> getItems()
    {
        return items;
    }

    public void setItems(List<FavoriteShopDetailPo> items)
    {
        this.items = items;
    }

}
