package com.qq.qqbuy.tenpay.biz.impl;

import java.io.StringReader;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.tenpay.biz.TenpayServiceAdapter;

/**
 * 
 * C2C 支付实现
 * 
 * 这里要求的参数有如下：
 * 
 * @param ver
 *            版本号,ver默认值是1.0。目前版本ver取值应为2.0
 * @param charset
 *            1 UTF-8, 2 GB2312, 默认为1 UTF-8
 * @param bank_type
 *            银行类型:财付通支付填0
 * @param desc
 *            商品描述,32个字符以内
 * @param purchaser_id
 *            用户(买方)的财付通帐户(QQ 或EMAIL)。若商户没有传该参数，则在财付通支付页面，买家需要输入其财付通帐户。
 * @param bargainor_id
 *            商户号,由财付通统一分配的10位正整数(120XXXXXXX)号
 * @param sp_billno
 *            商户系统内部的定单号,32个字符内、可包含字母
 * @param total_fee
 *            总金额,以分为单位,不允许包含任何字、符号
 * @param fee_type
 *            现金支付币种,目前只支持人民币,默认值是1-人民币
 * @param notify_url
 *            接收财付通通知的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
 * @param callback_url
 *            交易完成后跳转的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
 * @param attach
 *            商户附加信息,可做扩展参数，255字符内
 * @param time_start
 *            订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。
 *            时区为GMT+8 beijing。该时间取自商户服务器
 * @param time_expire
 *            订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。
 *            时区为GMT+8 beijing。该时间取自商户服务器
 * 
 * @param spid
 *            商户申请的spid
 * 
 *            * @param sp_time_stamp 拍拍时间戳 linux时间戳
 * 
 * @param price
 *            产品价格，以分为单位
 * 
 * @param transport_fee
 *            物流费用，以分为单位
 * 
 * @param fee1
 *            现金支付金额，以分为单位
 * 
 * @param fee2
 *            代金券金额
 * 
 * @param fee3
 *            其它费用
 * 
 * @return tokenid
 * 
 * @author wendyhu
 * 
 */
public class MutilC2CServiceTenpayServiceImpl extends TenpayServiceAdapter
{
    public MutilC2CServiceTenpayServiceImpl()
    {
        hostUrl = "https://wap.tenpay.com/cgi-bin/mpayv1.0/mpay_merge_init.cgi";// 请求的url
        encoding = "gbk";// 回复的字符编码
    }

    /**
     * C2C支付初始化接口，返回tokenid<br/>
     * 
     * @param ver
     *            版本号,ver默认值是1.0。目前版本ver取值应为2.0
     * @param charset
     *            1 UTF-8, 2 GB2312, 默认为1 UTF-8
     * @param bank_type
     *            银行类型:财付通支付填0
     * @param desc
     *            商品描述,32个字符以内
     * @param purchaser_id
     *            用户(买方)的财付通帐户(QQ 或EMAIL)。若商户没有传该参数，则在财付通支付页面，买家需要输入其财付通帐户。
     * @param bargainor_id
     *            商户号,由财付通统一分配的10位正整数(120XXXXXXX)号
     * @param sp_billno
     *            商户系统内部的定单号,32个字符内、可包含字母
     * @param total_fee
     *            总金额,以分为单位,不允许包含任何字、符号
     * @param fee_type
     *            现金支付币种,目前只支持人民币,默认值是1-人民币
     * @param notify_url
     *            接收财付通通知的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
     * @param callback_url
     *            交易完成后跳转的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
     * @param attach
     *            商户附加信息,可做扩展参数，255字符内
     * @param time_start
     *            订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。
     *            时区为GMT+8 beijing。该时间取自商户服务器
     * @param time_expire
     *            订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。
     *            时区为GMT+8 beijing。该时间取自商户服务器
     * 
     * @param sign_sp_id
     *            商户申请的sign_sp_id
     * 
     * @param sp_time_stamp
     *            拍拍时间戳 linux时间戳
     * 
     * @param price
     *            产品价格，以分为单位
     * 
     * @param transport_fee
     *            物流费用，以分为单位
     * 
     * @param fee1
     *            现金支付金额，以分为单位
     * 
     * @param fee2
     *            代金券金额
     * 
     * @param fee3
     *            其它费用
     * 
     * 
     * 
     * @return tokenid
     * 
     * @return tokenid
     */
    @Override
    protected boolean validator(Map<String, String> reqParam)
    {
        if (hostUrl == null || hostUrl.trim().length() < 10)
            return false;

        if (reqParam != null && reqParam.get("ver") != null
                && reqParam.get("charset") != null
                && reqParam.get("spid") != null
                && reqParam.get("callback_url") != null
                && reqParam.get("sale_plat") != null
                && reqParam.get("request_no") != null
                && reqParam.get("attach") != null
                && reqParam.get("request1") != null)
            return true;

        return false;
    }

    /**
     * 解析tokenid
     * 
     * @param xmlContent
     * @return
     */
    protected String parseContent(String xmlContent)
    {
        String tokenId = "";
        SAXBuilder builder = new SAXBuilder();
        try
        {
            Document doc = builder.build(new StringReader(xmlContent));
            Element root = doc.getRootElement();

            // 如果成功则有这个字段
            tokenId = StringUtil.removeInvalidWML(root.getChildText("token_id")
                    .trim().replaceAll("</?[^>]+>", "")
                    .replaceAll("&nbsp;", ""));

            if (tokenId != null && tokenId.length() != 0)
            {
                return tokenId;
            } else
            {
                String errinfo = StringUtil.removeInvalidWML(root.getChildText(
                        "err_info").trim().replaceAll("</?[^>]+>", "")
                        .replaceAll("&nbsp;", ""));
                logger.error("parseContent error for [" + xmlContent
                        + "] err:[" + errinfo + "]");
                return "";
            }

        } catch (Exception e)
        {
            logger.error("parseContent error for [" + xmlContent + "]", e);
        }
        return tokenId;
    }

    // /**
    // * 从财付通获取付款的token号码
    // *
    // * @param reqParam
    // * 去财付通获取token的参数
    // *
    // * @return 返回token信息
    // */
    // public String getToken(Map<String, String> reqParam)
    // {
    // String tokenId = "";
    // logger.info("###getToken param:[ " + reqParam.toString() + "]");
    // long begin = System.nanoTime();
    //        
    // //1、发送并获取http回包
    // String xmlContent = getRespXmlContent(reqParam);
    // long getContentTime = System.nanoTime();
    //        
    // //2、解析回包token
    // tokenId = parseContent(xmlContent);
    // long parseTime = System.nanoTime();
    //        
    // logger.info("###getToken param:[ " + reqParam.toString() +" ] result:[ "
    // + xmlContent + " ] token:[ " + tokenId + " ] tc:[ " +
    // (parseTime - begin) + " ] pc:[ " + (parseTime - getContentTime) +
    // " ] gc:[ " + (getContentTime - begin)
    // + " ] " );
    //        
    // return tokenId;
    // }
}
