package com.qq.qqbuy.tenpay.biz.impl;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.MD5Coding;
import com.qq.qqbuy.common.util.MD5Util;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.dealopr.util.DealDetailConveter;
import com.qq.qqbuy.tenpay.biz.IPayBiz;
import com.qq.qqbuy.tenpay.biz.ITenpayService;
import com.qq.qqbuy.tenpay.po.PayInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.PcDealQueryClient;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.CDealInfo;
import com.qq.qqbuy.thirdparty.idl.dealpc.protocol.GetDealInfoList2Resp;
import com.qq.qqbuy.thirdparty.idl.pay.PaymentClient;
import com.qq.qqbuy.thirdparty.idl.pay.protocol.PayMentApiResp;
import com.qq.qqbuy.thirdparty.openapi.po.GetDealDetailResponse;
import com.qq.qqbuy.thirdparty.openapi.po.PaymentApiRequest;
import com.qq.qqbuy.virtualgoods.biz.VirtualBiz;
import com.qq.qqbuy.virtualgoods.model.Order;
import com.qq.qqbuy.virtualgoods.model.OrderProduct;

/**
 * @ClassName: PayBizImpl
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author wendyhu
 * @date 2012-12-21 下午04:17:15
 */
public class PayBizImpl implements IPayBiz {
	/**
     * 
     */
	private ITenpayService c2cPayBiz = new C2CServiceTenpayServiceImpl();
	
	/**
	 * 统一支付平台
	 */
//	private ITenpayService uppPayBiz = new UppServiceTenpayServiceImpl();

	private ITenpayService c2cMutilPayBiz = new MutilC2CServiceTenpayServiceImpl();
	private VirtualBiz virtualBiz ;

	/**
	 * 过期时间
	 */
	private long expireTime = 60 * 60 * 24 * 15 * 1000;

	/**
	 * h5支付页面的url
	 */
	private String H5TenpayFormat = "http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_gate.cgi?gate_sp_id=%s&token_id=%s";

	private String MH5TenpayFormatParam = "spid=%s&token_id=%s";

	private String MH5TenpayFormatUrl = "https://wap.tenpay.com/cgi-bin/mpayv1.0/mpay_merge_gate.cgi";

	//银联支付流水单获取cgi接口
	private static final String UNIONPAY_URL  = "http://pay.paipai.com/index.php?c=pay&m=app&dealid=";

	private static final String UNIONPAY_HOST  = "pay.paipai.com";

//	private static final int DRAW_SEQ_REFUND = 112;

	private static final int DRAW_SEQ_RECV = 113;

	private static final int TENPAY_ID_LENGTH = 28;

	private static final int SPID_LENGTH = 10;

	private static final int MAX_DRAW_SEQ = 1000000000;

	private static final SimpleDateFormat FORMAT_YYMMDD = new SimpleDateFormat("yyMMdd");

	/**
	 * 财付通h5确认收货页面的url
	 */
	private static final String CfmrcvH5TenpayPre = "https://www.tenpay.com/app/mpay/mobile_auth.cgi?";

	private static final String MD5_KEY = "tencent*20090925@cft_paipai";

	public String getMH5TenpayFormatParam() {
		return MH5TenpayFormatParam;
	}

	public void setMH5TenpayFormatParam(String mH5TenpayFormatParam) {
		MH5TenpayFormatParam = mH5TenpayFormatParam;
	}

	public String getMH5TenpayFormatUrl() {
		return MH5TenpayFormatUrl;
	}

	public void setMH5TenpayFormatUrl(String mH5TenpayFormatUrl) {
		MH5TenpayFormatUrl = mH5TenpayFormatUrl;
	}

	/**
	 * c2c_spid
	 */
	private String spid = "1000000101";

	public ITenpayService getC2cMutilPayBiz() {
		return c2cMutilPayBiz;
	}

	public void setC2cMutilPayBiz(ITenpayService c2cMutilPayBiz) {
		this.c2cMutilPayBiz = c2cMutilPayBiz;
	}

	public String getH5TenpayFormat() {
		return H5TenpayFormat;
	}

	public void setH5TenpayFormat(String h5TenpayFormat) {
		H5TenpayFormat = h5TenpayFormat;
	}

	public String getSpid() {
		return spid;
	}

	public void setSpid(String spid) {
		this.spid = spid;
	}

	public ITenpayService getC2cPayBiz() {
		return c2cPayBiz;
	}

	public void setC2cPayBiz(ITenpayService c2cPayBiz) {
		this.c2cPayBiz = c2cPayBiz;
	}

	public long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(long expireTime) {
		this.expireTime = expireTime;
	}

	/**
	 * 
	 * @Title: getPayInfo
	 * @Description: 初使化订单并获取订单信息
	 * @param source
	 * @param uin
	 *            : 买家uin
	 * @param sellerUin
	 *            : 卖家uin
	 * @param dealCode
	 *            : 订单code
	 * @return 设定文件
	 * @return GetDealDetailResponse 返回类型
	 * @throws
	 */
	private GetDealDetailResponse getPayInfo(long uin, String source,
			long sellerUin, String dealCode, String sid, String lskey) {
		PaymentApiRequest payReq = new PaymentApiRequest();
		payReq.setType(1);
		payReq.setSource(source);
		payReq.setUin(uin);
		payReq.setDealCode(dealCode);
		PayMentApiResp payRsp = PaymentClient.initPayInfo(dealCode);
		
		Log.run.info("---------PayBizImpl.getPayInfo()-------payRsp:--"+payRsp.toString());
		
		
		if (payRsp != null && payRsp.result == 0) {
			// GrayUtil gray= new DefaultGrayUtil();
			GetDealDetailResponse dealRsp = null;
			// if(gray.isAllowGray()){
			
			Log.run.info("---------财付通--------------PayBizImpl.getPayInfo4upp()  uin::"+uin+"  dealCode:"+dealCode);
			
			GetDealInfoList2Resp res = PcDealQueryClient.queryByDealId(uin,dealCode, 1);
			
			Log.run.info("-----------财付通194行--------------PayBizImpl.getPayInfo4upp()res::"+res.toString());
			
			
			if (res != null && res.getResult() == 0) {
				Vector<CDealInfo> dealList = res.getODealInfoList();
				
				//输出大订单info信息
				for (int i = 0 ; i < dealList.size() ; i ++) {
					Log.run.info("-------------------------cDealInfo["+i+"]: " + dealList.get(i));
				}
				
				
				if (dealList != null && dealList.size() == 1) {
					//这取得是第一个订单
					dealRsp = DealDetailConveter.convertDealDetail(res
							.getODealInfoList().get(0));
					return dealRsp;
				}
			}
			// else{
			// throw
			// BusinessException.createInstance(BusinessErrorType.INVALID_PP_DEAL,res==null?"call pc dealidl fail":res.getErrMsg());
			// }
			// }else{
			// // 1、查询拍拍订单
			// GetDealDetailRequest dealreq = new GetDealDetailRequest();
			// dealreq.setSellerUin(sellerUin);
			// dealreq.setUin(sellerUin);
			// dealreq.setDealCode(dealCode);
			// dealreq.setListItem(1);// 列出商品信息
			// // dealreq.setSid(sid);
			// // dealreq.setLskey(lskey);
			// dealRsp = OpenApi.getProxy().getDealDetail(
			// dealreq);
			// if (dealRsp != null && dealRsp.isSucceed())
			// {
			// return dealRsp;
			// }
			// }

		}
		return null;
	}
	
	
	
	/**
	 * 统一支付平台
	 * 
	 * @Title: getPayInfo
	 * @Description: 初使化订单并获取订单信息
	 * @param source
	 * @param uin
	 *            : 买家uin
	 * @param sellerUin
	 *            : 卖家uin
	 * @param dealCode
	 *            : 订单code
	 * @return 设定文件
	 * @return GetDealDetailResponse 返回类型
	 * @throws
	 */
	private CDealInfo getPayInfo4upp(long uin, long sellerUin, String dealCode) {
		PayMentApiResp payRsp = PaymentClient.initPayInfo(dealCode);
		
//		Log.key.info("-----------统一支付--------------PayBizImpl.getPayInfo4upp()payRsp::"+payRsp.toString());
		
		Log.run.info("-----------统一支付--------------PayBizImpl.getPayInfo4upp()payRsp::"+payRsp+"----"+payRsp.toString()+"payRsp.result:"+payRsp.result);
		if (payRsp != null && payRsp.result == 0) {
			
			GetDealInfoList2Resp res = PcDealQueryClient.queryByDealId(uin,dealCode, 1);
			Log.run.info("-----------统一支付--------------PayBizImpl.getPayInfo4upp()res::"+res+"----"+res.toString()+"res.result:"+res.getResult());
			if (res != null && res.getResult() == 0) {
				Vector<CDealInfo> dealList = res.getODealInfoList();
				if (dealList != null && dealList.size() == 1) {
					return res.getODealInfoList().get(0);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @Title: isQQbuyDeal
	 * @Description: 判断订单是否是qq网购的订单
	 * @param propertyMask
	 * @return 设定文件
	 * @return boolean 返回类型
	 * @throws
	 */
	private boolean isQQbuyDeal(String propertyMask) {
		boolean ret = false;
		if (StringUtil.isNotBlank(propertyMask)) {
			String temp = "_" + propertyMask + "_";
			if (temp.indexOf("_8_") > -1)
				ret = true;
		}
		return ret;
	}

	@Override
	public PayTokenInfo getPayToken(long uin, long sellerUin, String dealCode,
			String payDesc, String source, String attach, String callBackUrl,
			String notifyUrl, String clientPayBackUrl, String sid, String lskey) {
		StringBuilder buf = new StringBuilder();
		buf.append("PayBizImpl#getPayToken uin:").append(uin).append(
				" sellerUin:").append(sellerUin).append(" dealCode:").append(
				dealCode).append(" payDesc:").append(payDesc)
				.append(" source:").append(source).append(" callBackUrl:")
				.append(callBackUrl);
		Log.run.info(buf.toString());
		PayTokenInfo ret = new PayTokenInfo();
		if (StringUtil.isNotBlank(dealCode) && uin > 10000 && sellerUin > 10000) {
			// String
			// uuString=DateUtils.formatDateToString(DateUtils.parseStringToDateNoExp("2012-12-13 16:50:10",DateUtils.FORMAT_YMD_HMS),DateUtils.FORMAT_yMdHms);
			GetDealDetailResponse dealRsp = getPayInfo(uin, source, sellerUin,dealCode, sid, lskey);
			
			Log.run.info("------财付通---------------------订单信息dealRsp："+dealRsp.toString());
			
			if (dealRsp != null && dealRsp.isSucceed()) {
				//判断如果为微信支付订单，直接返回
				Log.run.info("[getPayToken]dealPayType="+dealRsp.getDealPayType()+",uin="+uin);
				if(dealRsp.getDealPayType().equals("WXPAY")){
					ret.errCode = ErrConstant.INVALID_PAYTYPE_WXPAY;
					ret.msg = "不支持微信支付订单";
					return ret;
				}
				//判断支付
				if("JDPAY".equals(dealRsp.getDealPayType())){
					ret.errCode = ErrConstant.INVALID_PAYTYPE_JDPAY;
					ret.msg = "不支持京东收银台支付订单";
					return ret;
				}
				Date dt = DateUtils.parseStringToDateNoExp(dealRsp.createTime,
						DateUtils.FORMAT_YMD_HMS);
				String timeStart = "", endStart = "";
				try {
					timeStart = DateUtils.formatDateToString(dt,
							DateUtils.FORMAT_yMdHms);
					endStart = DateUtils.formatDateToString(new Date(dt
							.getTime()
							+ expireTime), DateUtils.FORMAT_yMdHms);
				} catch (Exception e) {
				}
				Map<String, String> mp2 = new TreeMap<String, String>();
				mp2.put("ver", "2.0");
				mp2.put("charset", "1");
				mp2.put("fee_type", "1");
				// 测试将订单号换成支付单号能否成功
//				mp2.put("sp_billno", dealCode);
				mp2.put("sp_billno", dealRsp.PayId+"");
				
				
				//mp2.put("sp_billno", dealRsp.tenpayCode);
				mp2.put("bank_type", "0");
				// 支付描述为空则使用商品列表第一个商品名称
				if (StringUtil.isEmpty(payDesc)) {
					if (dealRsp.getItemList().get(0).itemName != null
							&& dealRsp.getItemList().get(0).itemName.length() > 32)
						mp2.put("desc", dealRsp.getItemList().get(0).itemName
								.substring(0, 31));
					else
						mp2.put("desc", dealRsp.getItemList().get(0).itemName);
				} else {
					if (payDesc != null && payDesc.length() > 32)
						mp2.put("desc", payDesc.substring(0, 31));
					else
						mp2.put("desc", payDesc);
				}

				mp2.put("fee1", "" + dealRsp.dealPayFeeTotal);
				mp2.put("fee2", "0");
				mp2.put("fee3", "0");
				mp2.put("price", ""
						+ (dealRsp.dealPayFeeTotal - dealRsp.freight));
				mp2.put("total_fee", "" + dealRsp.dealPayFeeTotal);
				mp2.put("transport_fee", "" + dealRsp.freight);
				mp2.put("notify_url", notifyUrl);
				mp2
						.put("sp_time_stamp", "" + System.currentTimeMillis()
								/ 1000);

				// 如果是qq网购的订单，则使用现金券，命名为“shiwu_m_qbuy”——nedzhang
				if (isQQbuyDeal(dealRsp.propertymask)) {
					mp2.put("goods_tag", "shiwu_m_qbuy");
				} else {
					mp2.put("goods_tag", "tenpay_cl_01");
				}
				
				Log.run.info("------财付通-----------------财付通付款单号transaction_id【dealRsp.tenpayCode】： " + dealRsp.tenpayCode);
				
				mp2.put("transaction_id", dealRsp.tenpayCode);
				mp2.put("time_start", timeStart);
				mp2.put("time_expire", endStart);
				mp2.put("bargainor_id", "" + sellerUin);

				// 若有传attach参数，则拼在最后
				String attach1 = uin + "~" + sellerUin + "~" + dealCode + "~"
						+ dealRsp.dealPayFeeTotal + "~" + dealRsp.tenpayCode;

				if (!StringUtil.isEmpty(attach)) {
					attach1 += "~" + attach;
				}

				mp2.put("attach", attach1);
				mp2.put("purchaser_id", "" + uin);
				mp2.put("callback_url", callBackUrl);
				mp2.put("sign_sp_id", spid);
//				mp2.put("sign_sp_id", dealRsp.PayId+"");//改为payid
				
				String token = c2cPayBiz.getToken(mp2);
				if (StringUtil.isNotBlank(token)) {
					ret.setToken(token);
					/**
					 * 获取加密key
					 */
					String getTokenUrl = String.format(H5TenpayFormat,
							"1000000101", ret.getToken());
					ret
							.setH5PayUrl(getTokenUrl
									+ "&sign="
									+ MD5Coding.encode2HexStr((getTokenUrl
											+ "&key=" + c2cPayBiz
											.getMd5Key(null)).getBytes()));
					ret.setTenPayUrl(clientPayBackUrl);
				}
				Log.run.info(buf.toString() + " mp:" + mp2 + " token:" + token);
			} else {
				ret.errCode = ErrConstant.INVALID_PAY_INFO;
				ret.msg = "订单接口调用失败";
			}
		} else {
			ret.errCode = ErrConstant.INVALID_PAY_INFO;
			ret.msg = "支付中心调用失败";
		}

		Log.run.info(buf.toString() + " ret:" + ret);
		return ret;
	}
	
	/**
	 * 统一支付平台
	 */
	@Override
	public JsonObject getPayToken4upp(long wid , long sellerUin, String dealCode,
			String payDesc, String attach, String callBackUrl,
			String notifyUrl, String clientPayBackUrl, String sid, String lskey,String ip
			, String appId, String client, String clientVersion, String osVersion, String screen,String uuid, String payType,String pin) {
		StringBuilder buf = new StringBuilder();
		JsonObject wxPayInfo = new JsonObject();
		Log.run.debug("PayBizImpl.getPayToken4upp()...payType:" + payType + ",dealCode:" + dealCode + ",sellerUin:" + sellerUin+",wid:"+wid);
		
		if (StringUtil.isNotBlank(dealCode) && wid > 10000 && sellerUin > 10000) {
			Map<String, String> param = null;
//			if("v_wx".equals(payType)){//虚拟支付
			if(payType.startsWith("v_wx")){//虚拟支付
				Order virtualOrder = virtualBiz.getVirtualOrder(dealCode);
				
				Log.run.debug(new Gson().toJsonTree(virtualOrder).toString());
				
				if(null != virtualOrder){
					try {
						Log.key.info("---------------00-0");
						param = spliceParam4v_wx(virtualOrder, ip, wid, payType);
					} catch (Exception e) {
						Log.run.error(e.getMessage(), e);
					}
				}else{
					wxPayInfo.addProperty("errCode", ErrConstant.INVALID_PAY_INFO);
					wxPayInfo.addProperty("msg", "订单接口调用失败");
				}
			}else{
				
				//先查出该订单的信息，是用于验证订单的支付方式
				GetDealDetailResponse dealRsp =null;
				//获取订单列表
				GetDealInfoList2Resp res = PcDealQueryClient.queryByDealId(wid, dealCode, 1);
				if (res != null && res.getResult() == 0) {
					//查出大订单列表。该处应该只有一条数据
					Vector<CDealInfo> dealList = res.getODealInfoList();
					if (dealList != null && dealList.size() == 1) {
						//这取得是第一个订单
						dealRsp = DealDetailConveter.convertDealDetail(res
								.getODealInfoList().get(0));
					}
				}
				
				Log.run.info("====================>dealRsp:"+dealRsp);
				if(dealRsp!=null){
					//判断财付通。该字符串暂时没有。先用着
					if("TENPAY".equals(dealRsp.getDealPayType())){
						wxPayInfo.addProperty("errCode", ErrConstant.INVALID_PAYTYPE_TENPAY);
						wxPayInfo.addProperty("msg", "不支持财付通支付订单");
						return wxPayInfo;
					}
				}
				CDealInfo cDealInfo = null;
				//增加以下判断 如果是微信支付则走原有接口，如果是京东，则少走一部接口
				if("wx".equals(payType)){
					cDealInfo = getPayInfo4upp(wid, sellerUin,dealCode);
				}else if ("jdsyt".equals(payType)){
					if (res != null && res.getResult() == 0) {
						Vector<CDealInfo> dealList = res.getODealInfoList();
						if (dealList != null && dealList.size() == 1) {
							cDealInfo= res.getODealInfoList().get(0);
						}
					}
				}
				Log.run.info("===================>cDealInfo:"+cDealInfo);
				if (cDealInfo != null) {
					if("wx".equals(payType)){
						param = spliceParam(cDealInfo, ip, wid);//微信支付
					}else if ("jdsyt".equals(payType)){//京东收银台
						Log.run.info("------------------jdsyt begin -----------------");
						param = spliceJdParam(cDealInfo,ip,wid,  appId,  client,  clientVersion,  osVersion,screen,uuid,dealCode,pin);
					}
				} else {
					wxPayInfo.addProperty("errCode", ErrConstant.INVALID_PAY_INFO);
					wxPayInfo.addProperty("msg", "订单接口调用失败");
				}
			}
			
			wxPayInfo = getWXResp(param, wid, payType);
		} else {
			wxPayInfo.addProperty("errCode", ErrConstant.INVALID_PAY_INFO);
			wxPayInfo.addProperty("msg", "支付中心调用失败");
		}

		Log.key.info(buf.toString() + " wxPayInfo:" + wxPayInfo);
		return wxPayInfo;
	}
	
	private boolean validator(Map<String, String> reqParam) {
            return true;
    }
	
	public static void main(String[] args) {
		
		JsonObject wxPayInfo = new JsonObject();
		wxPayInfo.addProperty("errCode", ErrConstant.DEAL_TOO_MANY);
		wxPayInfo.addProperty("errC222ode", ErrConstant.DEAL_TOO_MANY);
		
		System.out.println(wxPayInfo.get("errCode").getAsString());
		
		wxPayInfo.remove("errsssddCod1111e");
		
		System.out.println(wxPayInfo.toString());

		long wid = 3115117569l;
		String sk = "lduYRBd*ja*fLiDCQ9QNksh4jjq30ZboGc9vjcPL6RM_";
		ArrayList<PayInfo> payInfos = new ArrayList<PayInfo>();
		PayInfo payInfo = new PayInfo();
		payInfo.dealCode = "2317723118-20150410-10144195";
		payInfo.sellerUin = 15685582585l;
		payInfos.add(payInfo);
		PayBizImpl payBiz = new PayBizImpl();
		payBiz.getUnionPayTn(wid,sk,payInfos);
	}
	private JsonObject getWXResp(Map<String, String> reqParam,long uin, String payType) {
		
		Log.key.debug("PayBizImpl.getWXResp()------begin----reqParam:" + reqParam.toString());
    	
        // 1、检查参数是否合法
        if (!validator(reqParam)){
            return null;
        }

        String key = "";
        // 2、获取sign签名
//        if("v_wx".equals(payType)){//虚拟微信支付
        if(payType.startsWith("v_wx")){//虚拟微信支付
        	key = "9f8f9e801455ac377ded54e089bed163";
        }else{
        	key = "83f9821dc8a952ce4eefdab603e43de5";
        }
        
        Log.key.info("getWXResp:"+key);
        
        String sign = MD5Util.md5Sign(reqParam, "sign_key", key, "gb2312");
        reqParam.put("sign", sign);
        
        Log.key.info("PayBizImpl.getWXResp()---sign: " + sign);

        // 3、发送并获取http回包
//        String xmlContent = HttpUtil.post4upp("http://pay.paipai.com/cgi-bin/unipay/api/1.1/payment/gateway", 
//        		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
        
        
        //这是统一支付平台gamma环境地址
//        String xmlContent = HttpUtil.post4upp("http://gamma.pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
//        		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
        
        
        
        String ipPort = PaiPaiConfig.getPaipaiCommonIp();
        Log.key.debug("ipPort: "+ipPort);
        
        //这是统一支付平台IDC环境地址
        //现在改为内网直连，不再需要代理--2014-12-22  by  刘本龙
       // String xmlContent = HttpUtil.post4upp("http://" + ipPort + "/cgi-bin/unipay_pp/api/payment/gateway", 
        //		reqParam, 10000, 15000, "gb2312", "gb2312", false ,"uin=" + uin,"pay.paipai.com",true);
        String xmlContent = "";
        //如果是gamma则走测试的
        if(EnvManager.isGamma()){
        	Log.run.info("xmlContentisGamma---");
        	xmlContent = HttpUtil.post4upp("http://gamma.pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
            		reqParam, 10000, 15000, "gb2312", "gb2312", true ,"uin=" + uin,"pay.paipai.com",true);
        }else {
        	xmlContent = HttpUtil.post4upp("http://" + ipPort + "/cgi-bin/unipay_pp/api/payment/gateway", 
            		reqParam, 10000, 15000, "gb2312", "gb2312", false ,"uin=" + uin,"pay.paipai.com",true);
		}
        
        Log.key.info("PayBizImpl.getWXResp()---xmlContent: " + xmlContent);

        // 4、解析回包token
        JsonObject wxPayInfo = new JsonObject();
        if("jdsyt".equals(payType)){
        	 // 4、解析回包token
             wxPayInfo = parseJdContent(xmlContent);
        }else {
        	 wxPayInfo = parseContent(xmlContent);
		}

        Log.key.info("PayBizImpl.getWXResp()---wxPayInfo: " + wxPayInfo.toString());
        Log.payMent.info("getWXResp param:[ " + reqParam.toString()
                + " ] result:[ " + xmlContent + " ] ") ;
        return wxPayInfo;
    }

	/**
	 * 
	
	 * @Title: parseJdContent 
	
	 * @Description: 解析京东支付
	
	 * @param @param xmlContent
	 * @param @return    设定文件 
	
	 * @return JsonObject    返回类型 
	
	 * @throws
	 */
	private JsonObject parseJdContent(String xmlContent) {
		Log.run.info("-----------PayBizImpl.parseJdContent()");
		try {
			JsonObject wxPayInfo = new JsonObject();
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(xmlContent));
			Element root = doc.getRootElement();
			// 如果成功则有这个字段
			if (root != null && root.getChildText("Status") != null){
			    String Status = StringUtil.removeInvalidWML(root.getChildText(
			            "Status").trim().replaceAll("</?[^>]+>", "")
			            .replaceAll("&nbsp;", ""));
			    Log.run.info("Status:"+Status);
			}
			
			String Message = root.getChildText("Message");
			Log.run.info("Message："+Message);
			String Sign = root.getChildText("Sign");
			Log.run.info("Sign:"+Sign);
			
			//拼接返回的URL
			List<Element> children = root.getChildren("Form");
			for (Element e : children) {
				  String  URL="";
				if (e != null && e.getChildText("Url") != null){
				    URL = StringUtil.removeInvalidWML(
				    		e.getChildText("Url").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
				    Log.run.info("URL:"+URL);
				}
				
				List<Element> children2 = e.getChild("Parameters").getChildren("Parameter");
				
//				if(children2.size() > 0)URL += "?";
				Map<String, String> map = new HashMap<String, String>();
				for (Element child : children2) {
					String Name = "";
					String Value = "";
					if (child != null && child.getChildText("Name") != null){
					    Name = StringUtil.removeInvalidWML(
					    		child.getChildText("Name").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
					    Log.run.info("Name:"+Name);
					}
					if (child != null && child.getChildText("Value") != null){
					    Value = StringUtil.removeInvalidWML(
					    		child.getChildText("Value").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
					    Log.run.info("Value:"+Value);
					}
					
//					URL += Name + "=" + Value +"&";
					map.put(Name, Value);
				}
				
				URL = URL+"?appId="+ map.get("appId") +"&payId="+map.get("payId");
				wxPayInfo.addProperty("appid", map.get("appId"));
				wxPayInfo.addProperty("payId", map.get("payId"));
				wxPayInfo.addProperty("payurl",URL);
				wxPayInfo.addProperty("callback","http://www.paipai.com");
			}
			
			return wxPayInfo;
		} catch (JDOMException e) {
			Log.key.error("-----------PayBizImpl.parseJdContent()" + e.getMessage());
		} catch (IOException e) {
			Log.key.error("-----------PayBizImpl.parseJdContent()" + e.getMessage());
		}
        return null;
    }

	/**
     * 解析tokenid
     * 
     * @param xmlContent
     * @return
     */
	private JsonObject parseContent(String xmlContent){
		
		Log.key.info("-----------PayBizImpl.parseContent()");
		
		try {
//			WXPayInfo wxPayInfo = new WXPayInfo();
			JsonObject wxPayInfo = new JsonObject();
//			String URL = "";
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(new StringReader(xmlContent));
			Element root = doc.getRootElement();
			// 如果成功则有这个字段
			if (root != null && root.getChildText("Status") != null){
			    String Status = StringUtil.removeInvalidWML(root.getChildText(
			            "Status").trim().replaceAll("</?[^>]+>", "")
			            .replaceAll("&nbsp;", ""));
			    System.out.println("Status:"+Status);
			}
			
			String Message = root.getChildText("Message");
			System.out.println("Message："+Message);
			String Sign = root.getChildText("Sign");
			System.out.println("Sign:"+Sign);
			
			//拼接返回的URL
			List<Element> children = root.getChildren("Form");
			for (Element e : children) {
//				if (e != null && e.getChildText("Url") != null){
//				    URL = StringUtil.removeInvalidWML(
//				    		e.getChildText("Url").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//				    System.out.println("URL:"+URL);
//				}
				
				List<Element> children2 = e.getChild("Parameters").getChildren("Parameter");
				
//				if(children2.size() > 0)URL += "?";
				Map<String, String> map = new HashMap<String, String>();
				for (Element child : children2) {
					String Name = "";
					String Value = "";
					if (child != null && child.getChildText("Name") != null){
					    Name = StringUtil.removeInvalidWML(
					    		child.getChildText("Name").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
					    System.out.println("Name:"+Name);
					}
					if (child != null && child.getChildText("Value") != null){
					    Value = StringUtil.removeInvalidWML(
					    		child.getChildText("Value").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
					    System.out.println("Value:"+Value);
					}
					
//					URL += Name + "=" + Value +"&";
					map.put(Name, Value);
				}
				
				
				wxPayInfo.addProperty("appid", map.get("appid"));
				wxPayInfo.addProperty("noncestr", map.get("noncestr"));
				wxPayInfo.addProperty("package1", map.get("package"));
				wxPayInfo.addProperty("partnerid", map.get("partnerid"));
				wxPayInfo.addProperty("prepayid", map.get("prepayid"));
				wxPayInfo.addProperty("sign", map.get("sign"));
				wxPayInfo.addProperty("timestamp", map.get("timestamp"));
			}
			
			return wxPayInfo;
		} catch (JDOMException e) {
			Log.key.error("-----------PayBizImpl.parseContent()" + e.getMessage());
		} catch (IOException e) {
			Log.key.error("-----------PayBizImpl.parseContent()" + e.getMessage());
		}
        return null;
    }
//	/**
//     * 解析tokenid
//     * 京东收银台
//     * @param xmlContent
//     * @return
//     */
//	private WXPayInfo parseContentJD(String xmlContent){
//		
//		Log.key.info("-----------PayBizImpl.parseContentJD()");
//		try {
//			WXPayInfo jdPayInfo = new WXPayInfo();
////			String URL = "";
//			SAXBuilder builder = new SAXBuilder();
//			Document doc = builder.build(new StringReader(xmlContent));
//			Element root = doc.getRootElement();
//			// 如果成功则有这个字段
//			if (root != null && root.getChildText("Status") != null){
//			    String Status = StringUtil.removeInvalidWML(root.getChildText(
//			            "Status").trim().replaceAll("</?[^>]+>", "")
//			            .replaceAll("&nbsp;", ""));
//			    System.out.println("Status:"+Status);
//			}
//			
//			String Message = root.getChildText("Message");
//			System.out.println("Message："+Message);
//			String Sign = root.getChildText("Sign");
//			System.out.println("Sign:"+Sign);
//			
//			//拼接返回的URL
//			List<Element> children = root.getChildren("Form");
//			for (Element e : children) {
////				if (e != null && e.getChildText("Url") != null){
////				    URL = StringUtil.removeInvalidWML(
////				    		e.getChildText("Url").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
////				    System.out.println("URL:"+URL);
////				}
//				
//				List<Element> children2 = e.getChild("Parameters").getChildren("Parameter");
//				
////				if(children2.size() > 0)URL += "?";
//				Map<String, String> map = new HashMap<String, String>();
//				for (Element child : children2) {
//					String Name = "";
//					String Value = "";
//					if (child != null && child.getChildText("Name") != null){
//					    Name = StringUtil.removeInvalidWML(
//					    		child.getChildText("Name").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//					    System.out.println("Name:"+Name);
//					}
//					if (child != null && child.getChildText("Value") != null){
//					    Value = StringUtil.removeInvalidWML(
//					    		child.getChildText("Value").trim().replaceAll("</?[^>]+>", "").replaceAll("&nbsp;", ""));
//					    System.out.println("Value:"+Value);
//					}
//					
////					URL += Name + "=" + Value +"&";
//					map.put(Name, Value);
//				}
//				
//				
//				jdPayInfo.setAppid(map.get("appid"));
//				jdPayInfo.setNoncestr(map.get("noncestr"));
//				jdPayInfo.setPackage1(map.get("package"));
//				jdPayInfo.setPartnerid(map.get("partnerid"));
//				jdPayInfo.setPrepayid(map.get("prepayid"));
//				jdPayInfo.setSign(map.get("sign"));
//				jdPayInfo.setTimestamp(map.get("timestamp"));
//				
////				URL = URL.substring(0, URL.length()-1);
//				
//			}
//			
//			return jdPayInfo;
//		} catch (JDOMException e) {
//			Log.key.error("-----------PayBizImpl.parseContentJD()" + e.getMessage());
//		} catch (IOException e) {
//			Log.key.error("-----------PayBizImpl.parseContentJD()" + e.getMessage());
//		}
//        return null;
//    }
	
	
	
	/**
	 * 拼接参数
	 * @param cDealInfo
	 * @param ip
	 * @param wid
	 * @return
	 */
	private Map<String, String> spliceParam4v_wx(Order order,String ip,long wid, String payType) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		OrderProduct orderProduct = order.getOrderProductList().get(0);
		// 请求参数
		Map<String, String> paramMap = new TreeMap<String, String>();
		
		paramMap.put("_input_charset", "GBK");
		paramMap.put("channel", "0");
		paramMap.put("partner", "200001");
		paramMap.put("payment_method", "mobile");
		paramMap.put("payment_provider", "wx");
		paramMap.put("provider_partner", "1219840001");
		paramMap.put("registration_type", "simplified");
		paramMap.put("render_style", "wx_app");
		paramMap.put("sign_encode", "false");
		paramMap.put("sign_type", "MD5");
		paramMap.put("trade_number", "1");
		// 请求参数trade0
		Map<String, String> paramTradeMap = new TreeMap<String, String>();
		String partner_trade_no = order.getFsellerUin().toString()+"-"+
				sdf.format(order.getFdealCreateTime())+"-"+
				order.getFdealId();
		paramTradeMap.put("partner_trade_no", partner_trade_no);
		
		
//		"&attach=partner_define_extra_info" + //附加字段
//		"&extra_info=appid%3dwx6e04fccd996dc0d1" + //扩展字段
		
		paramTradeMap.put("payment_type", "direct");
		paramTradeMap.put("provider_partner", "1219840001");
		paramTradeMap.put("coupon_code", "0");
		String itemName = orderProduct.getFitemName();
		itemName = itemName.replace("\u0000", "");
		paramTradeMap.put("item_name", itemName.trim());
		paramTradeMap.put("item_type", "physical");
		paramTradeMap.put("automatic_delivery", "false");//????
		paramTradeMap.put("buyer_uin", order.getFbuyerUin().toString());
		paramTradeMap.put("seller_uin", order.getFsellerUin().toString());
		paramTradeMap.put("buyer_uid", order.getFbuyerUin().toString());
		paramTradeMap.put("seller_uid", order.getFsellerUin().toString());
		paramTradeMap.put("buyer_pay_account", order.getFbuyerUin().toString());
		paramTradeMap.put("seller_pay_account", "1219840001");
		paramTradeMap.put("total_amount", String.valueOf(order.getFdealPayFeeTotal()));
		paramTradeMap.put
("item_amount", String.valueOf(order.getFdealPayFeeTotal()));
		paramTradeMap.put("freight_amount", "0");
		paramTradeMap.put("attach", "partner_define_extra_info");
		if("v_wx_wd".equals(payType)){//拍拍微店虚拟微信支付
			paramTradeMap.put("extra_info", "appid%3dwx33b21f044ebf5163");
		}else{
			paramTradeMap.put("extra_info", "appid%3dwx6e04fccd996dc0d1");
		}
//		paramTradeMap.put("attach", "client:app,via:wx,buyeruin:"+order.getFbuyerUin().toString()+",paytype:5,spid:1219840001,payid:606057537,appid:wxd8488ee8497bb6de,noncestr:hllfgbjedfdb,timestamp:1416991257");
//		paramTradeMap.put("extra_info", "appid=wxd8488ee8497bb6de");
		paramTradeMap.put("receive_name", order.getFreceiveName());
		paramTradeMap.put("receive_zip", "");
		paramTradeMap.put("receive_address", order.getFreceiveAddr());
		paramTradeMap.put("receive_phone", order.getFreceiveMobile());
		paramTradeMap.put("receive_mobile", order.getFreceiveMobile());
		paramTradeMap.put("client_ip", "10.28.72.91");
		// trade0参数转换为字符串
		String trade0 = getUrlParam(paramTradeMap);
		paramMap.put("trade0", trade0);
		return paramMap;
	}
	
	public String getUrlParam(Map map){
		if(map.size()==0){
			return null;
		}
		StringBuffer param = new StringBuffer();
		Iterator iterator = map.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry entry = (Map.Entry)iterator.next();
			param.append(entry.getKey());
			param.append("=");
			param.append(entry.getValue());
			param.append("&");
		}
		param.delete(param.length()-1, param.length());
		return param.toString();
	}
	
//	/**
//	 * 拼接参数--虚拟微信支付
//	 * @param cDealInfo
//	 * @param ip
//	 * @param wid
//	 * @return
//	 */
//	private Map<String, String> spliceParam4v_wx(Order virtualOrder, String ip, long wid) {
//		
//		Map<String, String> reqParam = new TreeMap<String, String>();
//		Date fdealCreateTime = virtualOrder.getFdealCreateTime();
//		SimpleDateFormat  format = new SimpleDateFormat("yyyyMMdd");
//
//		reqParam.put("partner", "200001");//业务编号[测试号]--魏超到支付平台申请
//		reqParam.put("sign_type", "MD5");//签名方式 目前仅支持MD5
//		reqParam.put("sign_encode", "false");//签名是否需要urencode
//		reqParam.put("_input_charset", "GBK");//字符集
////		    	reqParam.put("_new_encode", "true");
//		reqParam.put("payment_provider", "wx");// 	支付服务商;
//		reqParam.put("provider_partner", "1219840001");// 	商户编号	bigint(20) unsigned	支付服务商商户号(spid), 
//		reqParam.put("payment_method", "mobile");// 	支付方式
//		reqParam.put("channel", "0");//	银行编码	String	支付服务商定义的银行编码【如果选择网银直连或快捷支付则必选】
////		    	reqParam.put("ptkey", "@rECyVl8B9");
//		reqParam.put("registration_type", "simplified");//	注册类型【complete 完全    	simplified 简化注册】
//		reqParam.put("render_style", "wx_app");//	渲染风格
//		reqParam.put("trade_number", "1");
//		reqParam.put("trade0", 
//				"partner_trade_no=" + virtualOrder.getFsellerUin() + "-" + format.format(fdealCreateTime) + "-" + virtualOrder.getFdealId() +//业务订单号
//				"&payment_type=direct" + //支付类型 [direct直付交易,medium 中介交易]
//				"&provider_partner=1219840001" + //商户编号
//				"&coupon_code=0" + //优惠券标签 
//				"&item_name=" + virtualOrder.getOrderProductList().get(0).getFitemName() + //商品名称---取第一个商品名称
//				/*
//				 * 现在不考虑虚拟商品，这里暂时写死
//				 */
//				"&item_type=virtual" + //商品类型[physical 实物商品    virtual  虚拟商品]
//				"&automatic_delivery=false" + //是否自动发货[统一填false]
//				/*
//				 * 京东用户传pin【这个华宇会提供接口，在APPtoken中解析出pin】   paipai用户传wid
//				 */
//				"&buyer_uin=" + wid + //买家编号[业务方内部用户ID]
////				"&buyer_uin=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
//				"&seller_uin=" + virtualOrder.getFsellerUin() + //卖家编号
//				
//				"&buyer_uid=" + wid + //买家编号[业务方内部用户ID]
////				"&buyer_uid=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
//				"&seller_uid=" + virtualOrder.getFsellerUin() + //卖家编号
//				"&buyer_pay_account=" + wid + //买家账号[与buyer_uid相同]
//				"&seller_pay_account=1219840001" + //支付宝传收款账号[商户编号 ]
//				
//				"&total_amount=" + virtualOrder.getFdealPayFeeTotal() + //订单金额
//				"&item_amount=" + virtualOrder.getFdealPayFeeTotal() + //商品金额
//				"&freight_amount=0" +  //运费金额
//				
//				"&attach=client:app,via:wx,buyeruin:"+virtualOrder.getFbuyerUin().toString()+",paytype:5,spid:1219840001,payid:606057537,appid:wxd8488ee8497bb6de,noncestr:hllfgbjedfdb,timestamp:1416991257" +
////				"&attach=partner_define_extra_info" + //附加字段
//    			"&extra_info=appid%3Dwxd8488ee8497bb6de" + //扩展字段
//				
//				"&receive_name="  + //收货人姓名
//				"&receive_zip=" + //收货人邮编
//				"&receive_address="  + //收货人地址
//				"&receive_phone="  + //收货人电话
//				"&receive_mobile="+ //收货人手机
//				
//				"&client_ip=" + ip //用户下单ip
//			);
//		
//		return reqParam;
//	}
	
	/**
	 * 拼接参数
	 * @param cDealInfo
	 * @param ip
	 * @param wid
	 * @return
	 */
	private Map<String, String> spliceParam(CDealInfo cDealInfo,String ip,long wid) {
		Map<String, String> reqParam = new TreeMap<String, String>();
		
		reqParam.put("partner", "100001");//业务编号[测试号]--魏超到支付平台申请
		reqParam.put("sign_type", "MD5");//签名方式 目前仅支持MD5
		reqParam.put("sign_encode", "false");//签名是否需要urencode
		reqParam.put("_input_charset", "GBK");//字符集
//		    	reqParam.put("_new_encode", "true");
		reqParam.put("payment_provider", "wx");// 	支付服务商;
		reqParam.put("provider_partner", "1219840101");// 	商户编号	bigint(20) unsigned	支付服务商商户号(spid), 
		reqParam.put("payment_method", "mobile");// 	支付方式
		reqParam.put("channel", "0");//	银行编码	String	支付服务商定义的银行编码【如果选择网银直连或快捷支付则必选】
//		    	reqParam.put("ptkey", "@rECyVl8B9");
		reqParam.put("registration_type", "simplified");//	注册类型【complete 完全    	simplified 简化注册】
		reqParam.put("render_style", "wx_app");//	渲染风格
		reqParam.put("trade_number", "1");
		reqParam.put("trade0", 
				"partner_trade_no=" + cDealInfo.getDisDealId() + "-" +  cDealInfo.getPayId() +//业务订单号
				"&payment_type=direct" + //支付类型 [direct直付交易,medium 中介交易]
				"&provider_partner=1219840101" + //商户编号
				"&coupon_code=0" + //优惠券标签 
				"&item_name=" + cDealInfo.getTradeList().get(0).getItemName() + //商品名称---取第一个商品名称
				/*
				 * 现在不考虑虚拟商品，这里暂时写死
				 */
				"&item_type=virtual" + //商品类型[physical 实物商品    virtual  虚拟商品]
				"&automatic_delivery=false" + //是否自动发货[统一填false]
				/*
				 * 京东用户传pin【这个华宇会提供接口，在APPtoken中解析出pin】   paipai用户传wid
				 */
				"&buyer_uin=" + wid + //买家编号[业务方内部用户ID]
//				"&buyer_uin=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
				"&seller_uin=" + cDealInfo.getSellerUin() + //卖家编号
				
				"&buyer_uid=" + wid + //买家编号[业务方内部用户ID]
//				"&buyer_uid=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
				"&seller_uid=" + cDealInfo.getSellerUin() + //卖家编号
				"&buyer_pay_account=" + wid + //买家账号[与buyer_uid相同]
				"&seller_pay_account=1219840101" + //支付宝传收款账号[商户编号 ]
				
				"&total_amount=" + cDealInfo.getDealPayFeeTotal() + //订单金额
				"&item_amount=" + (cDealInfo.getDealPayFeeTotal() - cDealInfo.getDealPayFeeShipping()) + //商品金额
				"&freight_amount=" + cDealInfo.getDealPayFeeShipping() + //运费金额
				
				"&attach=partner_define_extra_info" + //附加字段
    			"&extra_info=appid%3dwx6e04fccd996dc0d1" + //扩展字段
				
				"&receive_name=" + cDealInfo.getReceiveName() + //收货人姓名
				"&receive_zip=" + cDealInfo.getReceivePostcode() + //收货人邮编
				"&receive_address=" + cDealInfo.getReceiveAddr() + //收货人地址
				"&receive_phone=" + cDealInfo.getReceiveTel() + //收货人电话
				"&receive_mobile=" + cDealInfo.getStrReceiveMobile() + //收货人手机
				
				"&client_ip=" + ip //用户下单ip
			);
		
		return reqParam;
	}

	/**
	 * 京东收银台参数构造
	 * @param cDealInfo
	 * @param ip
	 * @param wid
	 * @return
	 */
	private Map<String, String> spliceJdParam(CDealInfo cDealInfo, String ip,
			Long wid, String appId, String client, String clientVersion, String osVersion, String screen,String uuid,String dealCode,String pin) {
		Log.run.info("--------------------spliceJdParam----------------begin");
		Map<String, String> reqParam = new TreeMap<String, String>();
		//附加信息
		Map<String ,String> extra_infoMap = new TreeMap<String, String>();
		//该值已经不用传了，王群处已经配置
		//extra_infoMap.put("appId", "pay_test");
		extra_infoMap.put("client", client);
		extra_infoMap.put("osVersion", (osVersion==null||"".equals(osVersion))?clientVersion:osVersion);
		extra_infoMap.put("clientVersion", clientVersion);
		extra_infoMap.put("screen",(screen==null||"".equals(screen))?"200*800": screen);
		extra_infoMap.put("uuid", uuid);
		extra_infoMap.put("backUrl", "http://www.paipai.com");
		//该值已经不用传了，王群处已经配置
		//extra_infoMap.put("appKey", "123456");
		//实物为84 ， 虚拟 与投资金银类为85目前没有没有虚拟
		extra_infoMap.put("orderType", "84");
		extra_infoMap.put("pin", pin+"");
		reqParam.put("partner", "100001");//业务编号[测试号]--魏超到支付平台申请
		reqParam.put("sign_type", "MD5");//签名方式 目前仅支持MD5
		reqParam.put("sign_encode", "false");//签名是否需要urencode
		reqParam.put("_input_charset", "GBK");//字符集
//		    	reqParam.put("_new_encode", "true");
		reqParam.put("payment_provider", "jdpay");// 	支付服务商;
		reqParam.put("provider_partner", "1000000001");// 	商户编号	bigint(20) unsigned	支付服务商商户号(spid), 
		reqParam.put("payment_method", "mobile");// 	支付方式
		reqParam.put("channel", "0");//	银行编码	String	支付服务商定义的银行编码【如果选择网银直连或快捷支付则必选】
//		    	reqParam.put("ptkey", "@rECyVl8B9");
		reqParam.put("registration_type", "simplified");//	注册类型【complete 完全    	simplified 简化注册】
		reqParam.put("render_style", "html5");//	渲染风格
		reqParam.put("trade_number", "1");
		try {
			reqParam.put("trade0", 
					"partner_trade_no=" + cDealInfo.getDisDealId() + "-" +  cDealInfo.getPayId() +//业务订单号
					"&provider_trade_no="+ cDealInfo.getDealCftPayid()+//
					//"&provider_trade_no="+ dealCode.split("-")[2].toString()+//交易流水号
					//"&provider_trade_no="+ cDealInfo.getDisDealId() + "-" +  cDealInfo.getPayId()+//交易流水号
					"&payment_type=direct" + //支付类型 [direct直付交易,medium 中介交易]
					"&provider_partner=1000000001" + //商户编号
					"&coupon_code=0" + //优惠券标签 
					"&item_name=" + cDealInfo.getTradeList().get(0).getItemName() + //商品名称---取第一个商品名称
					/*
					 * 现在不考虑虚拟商品，这里暂时写死
					 */
					"&item_type=physical" + //商品类型[physical 实物商品    virtual  虚拟商品]
					"&automatic_delivery=false" + //是否自动发货[统一填false]
					/*
					 * 京东用户传pin【这个华宇会提供接口，在APPtoken中解析出pin】   paipai用户传wid
					 */
					"&buyer_uin=" + wid + //买家编号[业务方内部用户ID]
//				"&buyer_uin=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
					"&seller_uin=" + cDealInfo.getSellerUin() + //卖家编号
					
					"&buyer_uid=" + wid + //买家编号[业务方内部用户ID]
//				"&buyer_uid=" + cDealInfo.getBuyerUin() + //买家编号[业务方内部用户ID]
					"&seller_uid=" + cDealInfo.getSellerUin() + //卖家编号
					"&buyer_pay_account=" + wid + //买家账号[与buyer_uid相同]
					"&seller_pay_account=1000000001" + //支付宝传收款账号[商户编号 ]
					
					"&total_amount=" + cDealInfo.getDealPayFeeTotal() + //订单金额
					"&item_amount=" + (cDealInfo.getDealPayFeeTotal() - cDealInfo.getDealPayFeeShipping()) + //商品金额
					"&freight_amount=" + cDealInfo.getDealPayFeeShipping() + //运费金额
					
					"&extra_info="+URLEncoder.encode(getUrlParam(extra_infoMap),"utf-8") + //扩展字段
					"&receive_name=" + cDealInfo.getReceiveName() + //收货人姓名
					"&receive_zip=" + cDealInfo.getReceivePostcode() + //收货人邮编
					"&receive_address=" + cDealInfo.getReceiveAddr() + //收货人地址
					"&receive_phone=" + cDealInfo.getReceiveTel() + //收货人电话
					"&receive_mobile=" + cDealInfo.getStrReceiveMobile() + //收货人手机
					"&provider_partner_no="+ cDealInfo.getDisDealId() + "-" +  cDealInfo.getPayId()+//交易流水号
					
					"&client_ip=" + ip //用户下单ip
				);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Log.run.info("--------------------spliceJdParamend----------------end:"+reqParam);
		return reqParam;
	
	}
	
	@Override
	public PayTokenInfo getCfmRcvToken(long uin, String tenpayCode,
			String dealCode, long minSubDealCode, long sellerFee,
			String callbackUrl, String lskey) {

		PayTokenInfo ret = new PayTokenInfo();
		ret.setCallbackUrl(callbackUrl);
		
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("uin", uin + "");
		params.put("trans_num", "1");
		params.put("trans_id_0", tenpayCode);
		params.put("draw_id_0", genDrawId(tenpayCode, minSubDealCode));
		params.put("deal_id_0", dealCode);
		params.put("buyer_fee_0", "0");
		params.put("seller_fee_0", sellerFee + "");
		params.put("channel", "1");
		params.put("remark", "");
		params.put("return_url", callbackUrl);
		String timestamp = (System.currentTimeMillis() / 1000) + "";
		params.put("timestamp", timestamp);
		String sign = map2StringV2(params);
		sign += MD5_KEY;
		sign = MD5Coding.encode2HexStr(sign.getBytes());
		params.put("sign", sign);
		params.put("lskey", lskey);
		params.put("magic_key", "");
		params.put("tag", "");

		String urlParams = map2StringV2(params);
		ret.setH5PayUrl(CfmrcvH5TenpayPre + urlParams);
		try {
			ret.setH5PayUrl(URLEncoder.encode(ret.getH5PayUrl(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
		}
		return ret;
	}

	/**
	 * 
	 * @Title: genDrawId
	 * 
	 * @Description: 生成drawid draw_id生成规则：前缀112/113+spid(10)+日期（YYMMDD)+序列号(9)
	 *               112 2000000101 101011 000000017）
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public String genDrawId(String tenpayCode, long minTradeId) {
		StringBuilder sb = new StringBuilder();
		sb.append(DRAW_SEQ_RECV).append(genSPID(tenpayCode));
		String currDate = "";
		try {
			currDate = DateUtils.formatDateToString(new Date(), FORMAT_YYMMDD);
		} catch (Exception e) {
			Log.run.error("PayBizImpl#genDrawId format date to string error",e);
		}
		sb.append(currDate);
		
		long drawSeq = minTradeId % MAX_DRAW_SEQ;

		return String.format("%s%09d", sb.toString(), drawSeq);
	}

	private String genSPID(String tenPayCode) {
		if (StringUtil.isNotBlank(tenPayCode)
				&& tenPayCode.length() == TENPAY_ID_LENGTH) {
			return tenPayCode.substring(0, SPID_LENGTH);
		}
		return "";
	}

	@Override
	public CommonPo callback(Map<String, String> urlParm) {
		return null;
	}

	@Override
	public CommonPo notify(Map<String, String> urlParm) {
		return null;
	}

	/**
	 * 获取银联支付订单的流水单号信息列表
	 *
	 * @param wid
	 * @param sk
	 * @param payInfos
	 * @return
	 */
	@Override
	public JsonObject getUnionPayTn(long wid, String sk, List<PayInfo> payInfos) {

		JsonObject unionpay = new JsonObject();
		JsonArray tnList = new JsonArray();
		int size = 0;
		int readTimeout = 15000;
		int connectTimeout = 10000;
		StringBuffer sb = new StringBuffer();
		String cookie = "wg_uin=" + wid + ";wg_skey=" + sk;
		Log.run.info("cookie is : " + cookie);
		//目前只支持单笔支付
		if (payInfos != null && (size = payInfos.size()) > 0 && size <= 5 && wid > 10000) {
			PayInfo payInfo = payInfos.get(0);
			JsonObject tn = new JsonObject();
			if (payInfo != null){
				String result = "";
				String urlStr = "";
				if(EnvManager.isGamma()) {
					//172.27.28.234, 8080
					urlStr = sb.append(UNIONPAY_URL).append(payInfo.dealCode).toString();
					result = HttpUtil.get(urlStr.replace(UNIONPAY_HOST,"10.187.15.84:80"), UNIONPAY_HOST, connectTimeout, readTimeout, "gbk", false, false, cookie,null);
				}else{
					urlStr = sb.append(UNIONPAY_URL).append(payInfo.dealCode).toString();
					result = HttpUtil.get(urlStr.replace(UNIONPAY_HOST, PaiPaiConfig.getPaipaiCommonIp()), UNIONPAY_HOST, connectTimeout, readTimeout, "gbk", false, false, cookie, null);
				}
				System.out.println(result);
				ObjectMapper jsonMapper = new ObjectMapper();
				JsonNode rootNode = null;
				try {
					rootNode = jsonMapper.readValue(result, JsonNode.class);
					int errorCode = rootNode.path("errno").getIntValue();
					if(errorCode == 0){
						String tnstr = rootNode.path("data").getTextValue();
						tn.addProperty("tn",tnstr);
						tnList.add(tn);
					}else if(errorCode == 2){
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","请登录之后再支付！");
					}else if(errorCode == 3){
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","获取订单信息失败，请稍候重试");
					}else if(errorCode == 5){
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","该订单已支付无需再支付");
					}else if(errorCode == 14){
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","出问题啦，请稍后重试");
					}else if(errorCode == 15){
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","订单信息不对哦");
					}else{
						unionpay.addProperty("errCode",errorCode);
						unionpay.addProperty("msg","出问题啦，请稍后重试");
					}
				} catch (IOException e) {
					e.printStackTrace();
					unionpay.addProperty("errCode",14);
					unionpay.addProperty("msg","出问题啦，请稍后重试");
				}
			}
		} else {
			unionpay.addProperty("errCode", ErrConstant.INVALID_PAY_INFO);
			if (size > 5) {
				unionpay.addProperty("errCode", ErrConstant.DEAL_TOO_MANY);
				unionpay.addProperty("msg", "最大只支持5笔合单支付，你可以到订单中心一笔一笔支付!");
			} else {
				unionpay.addProperty("msg", "支付参数有问题!");
			}
		}
		unionpay.add("tnList",tnList);
		return unionpay;
	}

	protected String map2String(Map<String, String> params) {
		if (params != null && params.size() > 0) {
			StringBuffer sbf = new StringBuffer();
			for (Entry<String, String> entry : params.entrySet()) {
				if (entry != null && !StringUtil.isEmpty(entry.getKey())
						&& !StringUtil.isEmpty(entry.getValue())) {
					sbf.append("&").append(entry.getKey()).append("=").append(
							entry.getValue());
				}
			}
			return (sbf.length() > 1) ? sbf.substring(1) : sbf.toString();
		}
		return "";
	}
	
	protected String map2StringV2(Map<String, String> params) {
		if (params != null && params.size() > 0) {
			StringBuffer sbf = new StringBuffer();
			for (Entry<String, String> entry : params.entrySet()) {
				if (entry != null && !StringUtil.isEmpty(entry.getKey()))
						//&& !StringUtil.isEmpty(entry.getValue()))
				{
					sbf.append("&").append(entry.getKey()).append("=").append(
							entry.getValue());
				}
			}
			return (sbf.length() > 1) ? sbf.substring(1) : sbf.toString();
		}
		return "";
	}

	/**
	 * 
	 * @Title: buildPayInfoParam
	 * @Description: 构建合单支付的子单
	 * @param payInfo
	 * @param notifyUrl
	 * @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	private String buildPayInfoParam(GetDealDetailResponse payInfo,
			String notifyUrl) {
		String ret = "";
		Map<String, String> params = new HashMap<String, String>();
		try {
			String payDesc = "";
			if ((payDesc = payInfo.getItemList().get(0).itemName) != null
					&& payDesc.length() > 32)
				payDesc = payDesc.substring(0, 31);

			params.put("sale_plat", "10"); // 请求来源，填10
			params.put("ver", "1.0"); // 当前应该填1.0
			params.put("charset", "1"); // 1表示 UTF-8, 2 表示GB2312, 默认为1
			params.put("desc", payDesc); // 商品描述,32个字符以内
			params.put("bank_type", "0"); // 财付通支付，填0
			params.put("purchaser_id", "" + payInfo.buyerUin); // 用户(买方)的财付通帐户(QQ
			// 或EMAIL)
			params.put("bargainor_id", "" + payInfo.sellerUin); // 卖家财付通帐号(QQ
			// 由于手机财付通不支持改价，为了支持改价，这里将订单号参数替换为财付通支付单号
			params.put("sp_billno", payInfo.dealCode); //
			// 商户系统内部的定单号,32个字符内的字母和数字。B账户走旧流程，使用原来的商户订单号
			//params.put("sp_billno", payInfo.tenpayCode);
			params.put("total_fee", "" + payInfo.dealPayFeeTotal); // 总金额,以分为单位,不允许包含任何字、符号
			params.put("fee_type", "1"); // 现金支付币种,固定填1
			params.put("notify_url", URLEncoder.encode(notifyUrl, "UTF-8")); // 接收财付通通知的URL,需给绝对路径，128字符内。格式如:http://cl.tenpay.com/tenpay.asp
			params.put("attach", "m~" + payInfo.dealCode + "~"
					+ payInfo.buyerUin);
			// //商户附加信息,可做扩展参数，128字符内。在支付成功后原样返回给notify_url
			params.put("transaction_id", payInfo.tenpayCode); // 财付通交易单号，拍拍生成

			params.put("sign_sp_id", spid); // 签名商户号

			params.put("sp_time_stamp", "" + System.currentTimeMillis() / 1000); // 拍拍时间戳,
			// C2C必填，B2C不填
			params.put("price", ""
					+ (payInfo.dealPayFeeTotal - payInfo.freight)); // 产品价格，以分为单位
			params.put("transport_fee", "" + payInfo.freight); // 物流费用，以分为单位
			params.put("procedure_fee", "0"); // 手续费用，以分为单位
			params.put("fee1", "" + payInfo.dealPayFeeTotal); // 现金支付金额，以分为单位
			params.put("fee2", "0"); // 代金券金额，以分为单位
			params.put("fee3", "0"); // 其它费用，以分为单位
			return map2String(params);
		} catch (Throwable t) {
			Log.key.warn("构建BC合单支付的子单出现未知异常.", t);
		}
		return ret;
	}

	@Override
	public PayTokenInfo getPayToken(long uin, String source,
			List<PayInfo> payInfos, String attach, String callBackUrl,
			String notifyUrl, String clientPayBackUrl, String sid, String lskey) {
		StringBuilder buf = new StringBuilder();
		buf.append("PayBizImpl#getPayToken uin:").append(uin).append(
				" payInfos:").append(payInfos).append(" source:")
				.append(source).append(" callBackUrl:").append(callBackUrl);
		Log.key.info(buf.toString());
		PayTokenInfo ret = new PayTokenInfo();
		int size = 0;
		/**
		 * 合单支付支付参数
		 */
		StringBuffer sb = new StringBuffer();
		Map<String, String> params = new TreeMap<String, String>();
		if (payInfos != null && (size = payInfos.size()) > 0 && size <= 5
				&& uin > 10000) {
			// 当只有一笔时就走单笔支付流程
			
			Log.run.info("------财付通-----------payInfos.size()："+size);
			
			if (size == 1) {
				PayInfo payInfo = payInfos.get(0);
				if (payInfo != null)
					return getPayToken(uin, payInfo.sellerUin,
							payInfo.dealCode, null, source, attach,
							callBackUrl, notifyUrl, clientPayBackUrl, sid,
							lskey);
			}
			int i = 0;
			// 生成每一笔订单的参数
			for (PayInfo payInfo : payInfos) {
				// 1、初使化支付id
				if (payInfo != null && payInfo.sellerUin > 10000
						&& StringUtils.isNotBlank(payInfo.dealCode)) {
					GetDealDetailResponse dealRsp = getPayInfo(uin, source,
							payInfo.sellerUin, payInfo.dealCode, sid, lskey);
					sb.append(payInfo.dealCode).append("~");
					if (dealRsp != null && dealRsp.isSucceed()) {
						//判断微信支付
						if("WXPAY".equals(dealRsp.getDealPayType())){
							ret.errCode = ErrConstant.INVALID_PAYTYPE_WXPAY;
							ret.msg = "不支持微信支付订单";
							return ret;
						}
						//判断支付是否是京东收银台支付
						if("JDPAY".equals(dealRsp.getDealPayType())){
							ret.errCode = ErrConstant.INVALID_PAYTYPE_JDPAY;
							ret.msg = "不支持京东收银台支付订单";
							return ret;
						}
						
						// 2、生成财付通支付参数
						String paramItem = buildPayInfoParam(dealRsp, notifyUrl);
						if (StringUtils.isNotBlank(paramItem)) {
							++i;
							params.put("request" + i, paramItem);
						}
					}
				}

			}

			params.put("spid", spid); // spid:商户号,
			// 如果包含C2C的子订单，那么使用C2C的Spid，否则使用B2C的spid
			params.put("callback_url", callBackUrl); // callback_url:支付完成后的回调URL，需给绝对路径，128字符内
			params.put("ver", "1.0");
			params.put("charset", "1"); // charset:字符集，1=UTF8, 2=GB2312, 默认为1
			params.put("sale_plat", "10"); // sale_plat:请求来源，填10
			params.put("request_no", "" + i); // request_no:请求笔数，必须与requst1...requstn的数目一致，最大10笔
			String tenpayAttach = "";// attach字段不能超过255个
			if (attach != null)
				tenpayAttach = attach;
			else
				tenpayAttach = sb.toString();
			if (tenpayAttach != null) {
				if (tenpayAttach.length() > 128)
					params.put("attach", tenpayAttach.substring(0, 127)); // 财付通源串返回，用于解决登录态问题
				else
					params.put("attach", tenpayAttach); // 财付通

			} else {
				params.put("attach", "mpaipai"); // 财付通源串返回，用于解决登录态问题
			}

			String token = c2cMutilPayBiz.getToken(params);

			if (StringUtil.isNotBlank(token)) {
				ret.setToken(token);
				/**
				 * 获取加密key
				 */
				String getTokenParam = String.format(MH5TenpayFormatParam,
						"1000000101", ret.getToken());
				ret
						.setH5PayUrl(MH5TenpayFormatUrl
								+ "?"
								+ getTokenParam
								+ "&sign="
								+ MD5Coding.encode2HexStr((getTokenParam
										+ "&key=" + c2cMutilPayBiz
										.getMd5Key(null)).getBytes()));
				ret.setTenPayUrl(clientPayBackUrl);
			} else {
				ret.msg = c2cMutilPayBiz.getErrMsg();
			}
			Log.key.info(buf.toString() + " params:" + params + " token:"
					+ token);
		} else {
			ret.errCode = ErrConstant.INVALID_PAY_INFO;
			if (size > 5) {
				ret.errCode = ErrConstant.DEAL_TOO_MANY;
				ret.msg = "最大只支持5笔合单支付，你可以到订单中心一笔一笔支付!";
			} else {
				ret.msg = "支付参数有问题!";
			}
		}
		if (StringUtil.isBlank(ret.getToken()) && StringUtil.isBlank(ret.msg)) {
			ret.msg = "获取支付信息失败!";
			ret.errCode = ErrConstant.INVALID_PAY_INFO;
		}

		Log.key.info(buf.toString() + " ret:" + ret);
		return ret;
	}
	
	
	
	/**
	 * 统一支付平台
	 */
	@Override
	public JsonObject getPayToken4upp(long wid, List<PayInfo> payInfos, String attach, String callBackUrl,
			String notifyUrl, String clientPayBackUrl, String sid, String lskey,String ip
			, String appId, String client, String clientVersion, String osVersion, String screen,String uuid, String payType,String pin) {

		JsonObject wxPayInfo = new JsonObject();
		int size = 0;
		//目前只支持单笔支付
		if (payInfos != null && (size = payInfos.size()) > 0 && size <= 5 && wid > 10000) {
			PayInfo payInfo = payInfos.get(0);
			if (payInfo != null)
				return getPayToken4upp(wid,  payInfo.sellerUin,
						payInfo.dealCode, null, attach,
						callBackUrl, notifyUrl, clientPayBackUrl, sid,
						lskey,ip, appId,  client,  clientVersion,  osVersion,  screen, uuid, payType,pin);
		} else {
			wxPayInfo.addProperty("errCode", ErrConstant.INVALID_PAY_INFO);
			if (size > 5) {
				wxPayInfo.addProperty("errCode", ErrConstant.DEAL_TOO_MANY);
				wxPayInfo.addProperty("msg", "最大只支持5笔合单支付，你可以到订单中心一笔一笔支付!");
			} else {
				wxPayInfo.addProperty("msg", "支付参数有问题!");
			}
		}

		Log.key.info( "---------- PayBizImpl.getPayToken4upp():" + wxPayInfo.toString());
		return wxPayInfo;
	}
	/**
	 * 京东收银台支付
	 */
//	@Override
//	public WXPayInfo getJdPayToken(Long wid, long uin, List<PayInfo> payInfos,
//			String attach, String callBackUrl, String notifyUrl,
//			String clientPayBackUrl, String sid, String lskey, String ip, String appId, String client, String clientVersion, String  osVersion, String screen,String uuid) {
//		Log.key.info("---------------------京东支付进入PayBizImpl.getJdPayToken-------------");
//		 //构造出返回的信息类
//		WXPayInfo jdPayInfo = new WXPayInfo();
//		int size=0;
//		//检查支付定单以及uid是否存在以及合法
//		if (payInfos != null && (size = payInfos.size()) > 0 && size <= 5 && uin > 10000) {
//			//只有一笔订单时
//			PayInfo payInfo = payInfos.get(0);
//			if (payInfo != null)
//				//处理该支付信息
//				return getPayTokenByJd(wid,uin, payInfo.sellerUin,
//						payInfo.dealCode, null, attach,
//						callBackUrl, notifyUrl, clientPayBackUrl, sid,
//						lskey,ip, appId,  client,  clientVersion,  osVersion,  screen,uuid);
//		}else {
//			jdPayInfo.errCode = ErrConstant.INVALID_PAY_INFO;
//			if (size > 5) {
//				jdPayInfo.errCode = ErrConstant.DEAL_TOO_MANY;
//				jdPayInfo.msg = "最大只支持5笔合单支付，你可以到订单中心一笔一笔支付!";
//			} else {
//				jdPayInfo.msg = "支付参数有问题!";
//			}
//		}
//		return jdPayInfo;
//	}
	/**
	 * 调用接口向服务端发送信息
	 * @param wid
	 * @param uin
	 * @param sellerUin
	 * @param dealCode
	 * @param object
	 * @param attach
	 * @param callBackUrl
	 * @param notifyUrl
	 * @param clientPayBackUrl
	 * @param sid
	 * @param lskey
	 * @param ip
	 * @return
	 */
//	private WXPayInfo getPayTokenByJd(Long wid, long uin, long sellerUin,
//			String dealCode, Object object, String attach, String callBackUrl,
//			String notifyUrl, String clientPayBackUrl, String sid,
//			String lskey, String ip, String appId, String client, String clientVersion, String  osVersion, String screen ,String uuid) {
//
//		
//		Log.key.info("------京东收银台支付---1375行--------PayBizImpl.getJdPayToken()");
//		
//		StringBuilder buf = new StringBuilder();
//		WXPayInfo jdPayInfo = new WXPayInfo();
//		
//		if (StringUtil.isNotBlank(dealCode) && uin > 10000 && sellerUin > 10000) {
//			//先查出该订单的信息，是用于验证订单的支付方式
//			GetDealDetailResponse dealRsp =null;
//			//获取订单列表
//			GetDealInfoList2Resp res = PcDealQueryClient.queryByDealId(uin,dealCode, 1);
//			if (res != null && res.result == 0) {
//				//查出大订单列表。该处应该只有一条数据
//				Vector<CDealInfo> dealList = res.getODealInfoList();
//				
//				//输出大订单info信息
////				for (int i = 0 ; i < dealList.size() ; i ++) {
////					Log.run.info("-------------------------cDealInfo["+i+"]: " + dealList.get(i));
////				}
//				
//				
//				if (dealList != null && dealList.size() == 1) {
//					//这取得是第一个订单
//					dealRsp = DealDetailConveter.convertDealDetail(res
//							.getODealInfoList().get(0));
//				}
//			}
//			
//			if(dealRsp!=null){
//				//判断财付通。该字符串暂时没有。先用着
//				if("TENPAY".equals(dealRsp.getDealPayType())){
//					jdPayInfo.errCode = ErrConstant.INVALID_PAYTYPE_TENPAY;
//					jdPayInfo.msg = "不支持财付通支付订单";
//					return jdPayInfo;
//				}
//				//判断京东收银台。该字符串暂时没有。先用着
//				if("WXPAY".equals(dealRsp.getDealPayType())){
//					jdPayInfo.errCode = ErrConstant.INVALID_PAYTYPE_WXPAY;
//					jdPayInfo.msg = "不支持微信支付订单";
//					return jdPayInfo;
//				}
//				
//			}
//			CDealInfo cDealInfo = getPayInfo4upp(uin, sellerUin,dealCode, sid, lskey);
//			
//			Log.key.info("------京东收银台支付---------------------订单信息dealRsp："+cDealInfo.toString());
//			
//			if (cDealInfo != null) {
//				
//				Map<String, String> param = spliceJdParam(cDealInfo,ip,wid,  appId,  client,  clientVersion,  osVersion,screen,uuid);
//				
//				Log.key.info("------京东收银台支付--参数信息---------------------："+param.toString());
//				
////				String URL = uppPayBiz.getToken(mp2);
//				jdPayInfo = getJDResp(param,uin);
//				
//			} else {
//				jdPayInfo.errCode = ErrConstant.INVALID_PAY_INFO;
//				jdPayInfo.msg = "订单接口调用失败";
//			}
//		} else {
//			jdPayInfo.errCode = ErrConstant.INVALID_PAY_INFO;
//			jdPayInfo.msg = "支付中心调用失败";
//		}
//
//		Log.key.info(buf.toString() + " jdPayInfo:" + jdPayInfo);
//		return jdPayInfo;
//	}

	public VirtualBiz getVirtualBiz() {
		return virtualBiz;
	}

	public void setVirtualBiz(VirtualBiz virtualBiz) {
		this.virtualBiz = virtualBiz;
	}

//	private WXPayInfo getJDResp(Map<String, String> reqParam, long uin) {
//		
//		Log.key.info("PayBizImpl.getJDResp()------begin----");
//    	
//        // 1、检查参数是否合法
//        if (!validator(reqParam)){
//            return null;
//        }
//
//        // 2、获取sign签名
//        String sign = MD5Util.md5Sign(reqParam, "sign_key", "83f9821dc8a952ce4eefdab603e43de5", "gb2312");
//        reqParam.put("sign", sign);
//        
//        Log.key.info("PayBizImpl.getJDResp()---sign: " + sign);
//        String xmlContent="" ;
//        // 3、发送并获取http回包
////        String xmlContent = HttpUtil.post4upp("http://pay.paipai.com/cgi-bin/unipay/api/1.1/payment/gateway", 
////        		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
//        
//        
//        //这是统一支付平台gamma环境地址
////        String xmlContent = HttpUtil.post4upp("http://gamma.pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
////        		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
//        
//        //这是统一支付平台IDC环境地址
//        /*String xmlContent = HttpUtil.post4upp("http://pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
//        		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);*/
//       if (EnvManager.isGAMMAEnvCurrent() ) {
//       	  System.out.println("正常 到了gamma环境了");
//    	   xmlContent = HttpUtil.post4upp("http://gamma.pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
//           		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
//        	//return SEARCH_HOST_NAME;
//        } else if(EnvManager.isIDCEnvCurrent()){
//        	System.out.println("正常 到了IDC了");
//        	xmlContent = HttpUtil.post4upp("http://pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
//            		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
//        }else {
//        	//这是统一支付平台IDC环境地址
//        	System.out.println("else 到了IDC了");
//            xmlContent = HttpUtil.post4upp("http://pay.paipai.com/cgi-bin/unipay_pp/api/payment/gateway", 
//           		reqParam, 10000, 15000, "gb2312", "gb2312", true,"uin=" + uin,null,true);
//		}
//        
//        Log.key.info("PayBizImpl.getJDResp()---xmlContent: " + xmlContent);
//
//        // 4、解析回包token
//        WXPayInfo jdPayInfo = parseContentJD(xmlContent);
//
//        Log.key.info("PayBizImpl.getJDResp()---jdPayInfo: " + jdPayInfo.toString());
//        
//        return jdPayInfo;
//    }

	
	
}
