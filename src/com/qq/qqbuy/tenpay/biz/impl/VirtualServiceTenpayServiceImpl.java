package com.qq.qqbuy.tenpay.biz.impl;

import java.util.Map;

import com.qq.qqbuy.tenpay.biz.TenpayServiceAdapter;



/**
 * 
 * Vb2c支付实现
 * 
 * 这里要求的参数有如下：
 * 
 * 
 * @param ver
 *            版本号,ver默认值是1.0。目前版本ver取值应为2.0
 * @param charset
 *            1 UTF-8, 2 GB2312, 默认为1 UTF-8
 * @param bank_type
 *            银行类型:财付通支付填0
 * @param desc
 *            商品描述,32个字符以内
 * @param purchaser_id
 *            用户(买方)的财付通帐户(QQ 或EMAIL)。若商户没有传该参数，则在财付通支付页面，买家需要输入其财付通帐户。
 * @param bargainor_id
 *            商户号,由财付通统一分配的10位正整数(120XXXXXXX)号
 * @param sp_billno
 *            商户系统内部的定单号,32个字符内、可包含字母
 * @param total_fee
 *            总金额,以分为单位,不允许包含任何字、符号
 * @param fee_type
 *            现金支付币种,目前只支持人民币,默认值是1-人民币
 * @param notify_url
 *            接收财付通通知的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
 * @param callback_url
 *            交易完成后跳转的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
 * @param attach
 *            商户附加信息,可做扩展参数，255字符内
 * @param time_start
 *            订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。
 *            时区为GMT+8 beijing。该时间取自商户服务器
 * @param time_expire
 *            订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。
 *            时区为GMT+8 beijing。该时间取自商户服务器
 *            
 * @param spid
 *            商户申请的spid
 * 
 * 
 * @author wendyhu
 * 
 */
public class VirtualServiceTenpayServiceImpl extends TenpayServiceAdapter
{
    /**
     * Vb2c 支付初始化接口，返回tokenid<br/>
     * 
     * @param ver
     *            版本号,ver默认值是1.0。目前版本ver取值应为2.0
     * @param charset
     *            1 UTF-8, 2 GB2312, 默认为1 UTF-8
     * @param bank_type
     *            银行类型:财付通支付填0
     * @param desc
     *            商品描述,32个字符以内
     * @param purchaser_id
     *            用户(买方)的财付通帐户(QQ 或EMAIL)。若商户没有传该参数，则在财付通支付页面，买家需要输入其财付通帐户。
     * @param bargainor_id
     *            商户号,由财付通统一分配的10位正整数(120XXXXXXX)号
     * @param sp_billno
     *            商户系统内部的定单号,32个字符内、可包含字母
     * @param total_fee
     *            总金额,以分为单位,不允许包含任何字、符号
     * @param fee_type
     *            现金支付币种,目前只支持人民币,默认值是1-人民币
     * @param notify_url
     *            接收财付通通知的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
     * @param callback_url
     *            交易完成后跳转的URL,需给绝对路径，255字符内 格式如:http://wap.tenpay.com/tenpay.asp
     * @param attach
     *            商户附加信息,可做扩展参数，255字符内
     * @param time_start
     *            订单生成时间，格式为yyyymmddhhmmss，如2009年12月25日9点10分10秒表示为20091225091010。
     *            时区为GMT+8 beijing。该时间取自商户服务器
     * @param time_expire
     *            订单失效时间，格式为yyyymmddhhmmss，如2009年12月27日9点10分10秒表示为20091227091010。
     *            时区为GMT+8 beijing。该时间取自商户服务器
     *            
     * @param sign_sp_id
     *            商户申请的sign_sp_id
     * 
     * @return tokenid
     */
    @Override
    protected boolean validator(Map<String, String> reqParam)
    {
        if(hostUrl == null || hostUrl.trim().length()< 10 )
            return false;
       
        if (reqParam != null && reqParam.get("ver") != null
                && reqParam.get("charset") != null
                && reqParam.get("bank_type") != null
                && reqParam.get("desc") != null
                && reqParam.get("purchaser_id") != null
                && reqParam.get("bargainor_id") != null
                && reqParam.get("transaction_id") != null
                && reqParam.get("sp_billno") != null
                && reqParam.get("total_fee") != null
                && reqParam.get("fee_type") != null
                && reqParam.get("notify_url") != null
                && reqParam.get("callback_url") != null
                && reqParam.get("attach") != null
                && reqParam.get("time_start") != null
                && reqParam.get("time_expire") != null
                && reqParam.get("sign_sp_id") != null)
            return true;

        return false;
    }

}
