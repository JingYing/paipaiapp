package com.qq.qqbuy.tenpay.biz;

import java.util.List;
import java.util.Map;

import com.google.gson.JsonObject;
import com.qq.qqbuy.common.po.CommonPo;
import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.tenpay.biz.impl.WXPayInfo;
import com.qq.qqbuy.tenpay.po.PayInfo;

/**
 * 
 * @ClassName: IPayBiz
 *  
 * @Description: 订单支付接口 
 * @author wendyhu 
 * @date 2012-12-21 下午04:04:31
 */
public interface IPayBiz
{
    
    /**
     * 
     * @Title: getPayToken
     * 
     * @Description:获取支付token  
     * @param uin： 用户qq号码
     * @param sellerUin: 卖家qq号码
     * @param dealCode：订单code
     * @param payDesc: 支付描述，建议用商品名
     * @param source: 来源 @see TenpayConstant.SOURCE_FOR_*
     * @param source: 附加字段，用来传递业务参数
     * @param callBackUrl: 回跳url
     * @param notifyUrl: 后台通知url
     * @param clientPayBackUrl: 客户端收到支付成功后的通知url
     * 
     * @return    设定文件 
     * 
     * @return PayTokenInfo  返回类型 
     * @throws
     */
    public PayTokenInfo getPayToken(long uin, long sellerUin, String dealCode, String payDesc, String source, String attach,String callBackUrl, String notifyUrl, String clientPayBackUrl,String sid,String lskey);
    
    /**
     * 
     * @Title: getPayToken4upp
     * 
     * @Description:获取支付token  
     * @param uin： 用户qq号码
     * @param sellerUin: 卖家qq号码
     * @param dealCode：订单code
     * @param payDesc: 支付描述，建议用商品名
     * @param source: 来源 @see TenpayConstant.SOURCE_FOR_*
     * @param source: 附加字段，用来传递业务参数
     * @param callBackUrl: 回跳url
     * @param notifyUrl: 后台通知url
     * @param clientPayBackUrl: 客户端收到支付成功后的通知url
     * 
     * @return    设定文件 
     * 
     * @return PayTokenInfo  返回类型 
     * @throws
     */
    public JsonObject getPayToken4upp(long wid, long sellerUin, String dealCode, String payDesc, String attach,String callBackUrl, String notifyUrl, String clientPayBackUrl,String sid,String lskey,String ip, String appId, String client, String clientVersion, String osVersion, String screen,String uuid, String payType,String pin);
    
    
    
    /**
     * 
	 * 统一支付平台
	 *
     * @Title: getPayToken
     * 
     * @Description:获取支付token  
     * @param uin： 用户qq号码
     * @param payInfos: 订单信息
     * @param source: 来源 @see TenpayConstant.SOURCE_FOR_*
     * @param callBackUrl: 回跳url
     * @param notifyUrl: 后台通知url
     * @param clientPayBackUrl: 客户端收到支付成功后的通知url
     * 
     * @return    设定文件 
     * 
     * @return PayTokenInfo  返回类型 
     * @throws
     */
    public PayTokenInfo getPayToken(long uin, String source, List<PayInfo> payInfos, String attach, String callBackUrl, String notifyUrl, String clientPayBackUrl,String sid,String lskey);
    
    
    /**
     * 
     * @Title: getPayToken--统一支付平台
     * 
     * @Description:获取支付token  
     * @param uin： 用户qq号码
     * @param payInfos: 订单信息
     * @param source: 来源 @see TenpayConstant.SOURCE_FOR_*
     * @param callBackUrl: 回跳url
     * @param notifyUrl: 后台通知url
     * @param clientPayBackUrl: 客户端收到支付成功后的通知url
     * 
     * @return    设定文件 
     * 
     * @return PayTokenInfo  返回类型 
     * @throws
     */
    public JsonObject getPayToken4upp(long wid,
			List<PayInfo> payInfos, String attach, String callBackUrl,
			String notifyUrl, String clientPayBackUrl, String sid, String lskey,String ip
			, String appId, String client, String clientVersion, String osVersion, String screen,String uuid,String payType,String pin);
    
    
    
    /**
     * 
     * @Title: getCfmRcvToken
     *
     * @Description: 确认订单获取Token
     * @param @param uin
     * @param @param tenPayCode
     * @param @param dealCode
     * @param @param minSubDealCode
     * @param @param callBackUrl
     * @param @param lskey
     * @param @return  设定文件
     * @return PayTokenInfo  返回类型
     * @throws
     */
    public PayTokenInfo getCfmRcvToken(long uin, String tenpayCode, String dealCode, long minSubDealCode, long sellerFee, String callbackUrl, String lskey);
    
    /**
     * 
     * @Title: callback 
     * 
     * @Description: 页面回跳调用 
     * 
     * @param urlParm 第三方支付传过来的参数
     * @return    设定文件 
     * @return CommonPo    返回类型 
     * @throws
     */
    public CommonPo callback(Map<String,String> urlParm);
    
    
    /**
     * 
     * @Title: notify 
     * 
     * @Description: 后台回调的接口 
     * @param urlParm 第三方支付传过来的参数
     * @return    设定文件 
     * @return CommonPo    返回类型 
     * @throws
     */
    public CommonPo notify(Map<String,String> urlParm);

    /**
     * 获取银联支付订单的流水单号信息列表
     * @param wid
     * @param sk
     * @param payInfos
     * @return
     */
    public JsonObject getUnionPayTn(long wid, String sk, List<PayInfo> payInfos);

//    /**
//     * @param object 
//     * @param i 
//     * @param string3 
//     * @param string2 
//     * @param string 
//     * 
//     * @Title: getJdPayToken
//     * --京东收银台支付
//     * @Description:获取支付token  
//     * @param uin： 用户qq号码
//     * @param payInfos: 订单信息
//     * @param source: 来源 @see TenpayConstant.SOURCE_FOR_*
//     * @param callBackUrl: 回跳url
//     * @param notifyUrl: 后台通知url
//     * @param clientPayBackUrl: 客户端收到支付成功后的通知url
//     * 
//     * @return    设定文件 
//     * 
//     * @return PayTokenInfo  返回类型 
//     * @throws
//     */
//	public WXPayInfo getJdPayToken(Long wid, long qq, List<PayInfo> payInfos,
//			String attach, String callbackUrl, String notifyUrl,
//			String clientPayBackUrl, String sid, String lk, String ip, String appId, String client, String clientVersion, String osVersion, String screen,String uuid );
    
}
