package com.qq.qqbuy.tenpay.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.google.gson.JsonObject;
import com.paipai.component.configcenter.itil.ItilReporter;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.ItilConstants;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.Base64;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.tenpay.biz.IPayBiz;
import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.tenpay.po.PayInfo;

/**
 * api支付action
 * 
 * @author wendyhu
 * 
 */
public class ApiTenpayAction extends AuthRequiredAction 
{
    private static final long serialVersionUID = -1498512477267390691L;

    // long uin, long sellerUin, String dealCode, String payDesc, String source,
    // String callBackUrl)
    // *********************START API支付请求参数***********************************//
    private long sellerUin = 0;
    private String dealCode = "";
    private String payDesc = "";
    private String source = "";
    private JsonOutput out = new JsonOutput();

    // 多笔支付
    // 格式"dealCode~sellerUin|dealCode~sellerUin"
    private String deals = "";
    // *********************END API支付请求参数***********************************//

    // *********************START支付成功后财付通的回跳参数***********************************//
    private String msg = "";

    // *********************END支付成功后财付通的回跳参数***********************************//
    
    // *********************START确认收货获取Token请求参数*******************************//
    /**
     * 财付通支付ID
     */
    private String tenpayCode;
    
    /**
     * 最小子单号
     */
    private long minSubDealCode;
    
    /**
     * 卖家收款金额
     */
    private long sellerFee;
    // *********************END确认收货获取Token请求参数********************************//
    
    /**
     * 支付类
     */
    private IPayBiz payBiz;
    
    //以下为京东收银台的参数
    private String osVersion="";//os版本号
    private String screen="";//屏幕宽度
    private String jsonStr = "{}";
    private String payType;//支付方式[微信：wx;京东收银台：jdsyt]
    

	/**
     * 
     * @Title: validateGetTokenInput
     * 
     * @Description: 判断获取token的请求参数是否合法
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean validateGetTokenInput()
    {
        if ((sellerUin > 10000) && StringUtil.isNotBlank(dealCode)
                && StringUtil.isNotBlank(payDesc)
                && StringUtil.isNotBlank(source))
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * 
     * @Title: validateGetTokensInput
     * 
     * @Description: 判断获取token的请求参数是否合法
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean validateGettksInput()
    {
//        if (StringUtil.isNotBlank(deals) && StringUtil.isNotBlank(source))
    	if (StringUtil.isNotBlank(deals))//source没有实际用到，所以不需要传递。--刘本龙--2014.9.19
        {
            return true;
        } else
        {
            return false;
        }
    }
    
    /**
     * 
     * @Title: validateGetCfmRcvTkInput
     *
     * @Description: 校验获取确认收货token的请求参数
     * @param @return  设定文件
     * @return boolean  返回类型
     * @throws
     */
    private boolean validateGetCfmRcvTkInput()
    {
    	if (StringUtil.isBlank(dealCode) 
    			|| StringUtil.isBlank(tenpayCode)
                || sellerFee <= 0 || minSubDealCode <= 0) 
    	{
			return false;
		}
    	return true;
    }

    public String getToken()
    {
        long start = System.currentTimeMillis();
        PayTokenInfo ret = new PayTokenInfo();

        String basePath = getRequest().getScheme() + "://"
                + getRequest().getServerName();
        int port = getRequest().getServerPort();
        if (port != 80)
        {
            basePath += ":" + port;
        }
        basePath += getRequest().getContextPath();

        String callbackUrl = basePath + "/api/pay/callback.xhtml";
        String clientTenPayUrl = "pay/retback.xhtml";

        if (!validateGetTokenInput())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
            return SUCCESS;
        }

        // 鉴权
        if (!checkLogin())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
            return SUCCESS;
        }

        ret = payBiz.getPayToken(getQq(), sellerUin, dealCode, payDesc, source,
                null, callbackUrl, callbackUrl, clientTenPayUrl,getSid(),getLk());
        
        //微信支付类型则返回
        if(null!=ret && ret.errCode == ErrConstant.INVALID_PAYTYPE_WXPAY){
        	setErrCodeAndMsg(ErrConstant.ERRCODE_COMMON_NOTSUPPORT_WXPAY, "不支持微信支付订单");
        }
        
        if (null != ret && ret.errCode == 0
                && StringUtil.isNotBlank(ret.getToken()))
        {
            String loginKey = "uin=" + getQq() + "&session_id=" + getLk();
            String h5Url = ret.getH5PayUrl() + "&login_key="
                    + Base64.encode(loginKey.getBytes());
            ret.setH5PayUrl(h5Url);
            ret.setCallbackUrl(callbackUrl);
            JsonConfig jsoncfg = new JsonConfig();
            String[] excludes =
            { "size", "empty" };
            jsoncfg.setExcludes(excludes);
            jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
        }
        return SUCCESS;
    }

    public String gettks()
    {
    	Log.run.info("-------财付通---------ApiTenpayAction.gettks()---------");
    	
    	ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_REQUEST, 1);
        long start = System.currentTimeMillis();
        PayTokenInfo ret = new PayTokenInfo();

        String basePath = getRequest().getScheme() + "://"
                + getRequest().getServerName();
        int port = getRequest().getServerPort();
        if (port != 80)
        {
            basePath += ":" + port;
        }
        basePath += getRequest().getContextPath();

        String callbackUrl = basePath + "/api/pay/callback.xhtml";
        String clientTenPayUrl = "pay/retback.xhtml";

        // 参数校验
        if (!validateGettksInput())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
            return SUCCESS;
        }

        // 鉴权
        if (!checkLogin())
        {
            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
            return SUCCESS;
        }

        List<PayInfo> payInfos = new ArrayList<PayInfo>();
        // 2、分隔出订单以及卖家uin
        String[] dealCodes = deals.split("\\|");
        if (dealCodes != null && dealCodes.length > 0)
        {
            for (String dealItem : dealCodes)
            {
                String[] dealCodeInfo = dealItem.split("\\~");
                if (dealCodeInfo != null && dealCodeInfo.length >= 2)
                {
                    PayInfo payInfo = new PayInfo();
                    payInfo.dealCode = dealCodeInfo[0].trim();
                    if ("ios".equals(getMt())) {
                    	payInfo.sellerUin = StringUtil.toLong(dealCodeInfo[0].trim().split("-")[0]
                                .trim(), 0);//ios sellerUin错误，兼容处理
                    } else {
                    	payInfo.sellerUin = StringUtil.toLong(dealCodeInfo[1]
                                 .trim(), 0);
                    }
                   
                    payInfos.add(payInfo);
                }
            }
        }
        if (payInfos.size() > 0)
            ret = payBiz.getPayToken(getQq(), source, payInfos, null,
                    callbackUrl, callbackUrl, clientTenPayUrl,getSid(),getLk());

        if (null != ret)
        {
            JsonConfig jsoncfg = new JsonConfig();
            String[] excludes =
            { "size", "empty" };
            if (ret.errCode == 0 && StringUtil.isNotBlank(ret.getToken()))
            {
                String loginKey = "uin=" + getQq() + "&session_id=" + getLk();
                String h5Url = ret.getH5PayUrl() + "&login_key="
                        + Base64.encode(loginKey.getBytes());
                ret.setH5PayUrl(h5Url);
                ret.setCallbackUrl(callbackUrl);
                jsoncfg.setExcludes(excludes);
            }
            if (ret.errCode != 0)
            {
                result.setErrCode((int) ret.errCode);
                if(ret.msg != null)
                    result.setMsg(ret.msg);
                ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_FAILED, 1);
            }
            jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
        }
        ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_SUCCESS, 1);
        return SUCCESS;
    }
    
    
    
    
    
    /**
     * 统一支付平台
     * @author liubenlong3
     * @return
     */
    public String unifiedPaymentPlatform()
    {
    	try {
			Log.run.debug("-------统一支付平台---------ApiTenpayAction.unifiedPaymentPlatform()---------");
			
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_REQUEST, 1);
			JsonObject wxPayInfo = new JsonObject();

			String basePath = getRequest().getScheme() + "://" + getRequest().getServerName();
			int port = getRequest().getServerPort();
			if (port != 80) {
			    basePath += ":" + port;
			}
			basePath += getRequest().getContextPath();

			String callbackUrl = basePath + "/api/pay/uppCallback.xhtml";
			String clientTenPayUrl = "pay/uppRetback.xhtml";

			// 参数校验
			if (!validateGettksInput()) {
			    out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
			    out.setMsg("参数校验不合法");
			    return returnResult(out, getPayType());
			}

			// 鉴权
			if (!checkLogin()) {
			    out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
			    out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			    return returnResult(out, getPayType());
			}

			List<PayInfo> payInfos = new ArrayList<PayInfo>();
			// 2、分隔出订单以及卖家uin
			String[] dealCodes = deals.split("\\|");
			if (dealCodes != null && dealCodes.length > 0) {
			    for (String dealItem : dealCodes) {
			        String[] dealCodeInfo = dealItem.split("\\~");
			        if (dealCodeInfo != null && dealCodeInfo.length >= 2) {
			            PayInfo payInfo = new PayInfo();
			            payInfo.dealCode = dealCodeInfo[0].trim();
			            payInfo.sellerUin = StringUtil.toLong(dealCodeInfo[1].trim(), 0);
			            payInfos.add(payInfo);
			        }
			    }
			}
			
			if (payInfos.size() > 0){
				wxPayInfo = payBiz.getPayToken4upp(getWid(), payInfos, null,
			            callbackUrl, callbackUrl, clientTenPayUrl,getSid(),getLk(),getRequest().getRemoteAddr()
			            ,getAppID(),getMt(),getVersionCode(), osVersion, screen,getMk(), getPayType(),getPinByTooken(getAppToken()));
			}
			    
			
			if (null != wxPayInfo) {
			    if (wxPayInfo.has("errCode") && wxPayInfo.get("errCode").getAsInt() != 0) {
			        out.setErrCode(wxPayInfo.get("errCode").getAsInt());
			        if(wxPayInfo.has("msg") && StringUtil.isNotBlank(wxPayInfo.get("msg").getAsString()))
			        	out.setMsg(wxPayInfo.get("msg").getAsString());
			        ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_FAILED, 1);
			    }
			    
				wxPayInfo.remove("errCode");
				wxPayInfo.remove("msg");
			    out.setData(wxPayInfo);
			}

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_SUCCESS, 1);
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
		}
    	
    	return returnResult(out, getPayType());
    }

    /**
     * 银联支付
     * @return
     */
    public String unionpay(){

        try {
            ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_REQUEST, 1);
            JsonObject tn = new JsonObject();
            // 参数校验
            if (!validateGettksInput()) {
                out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
                out.setMsg("参数校验不合法");
                return doPrint(out.toJsonStr());
            }
            // 鉴权
            if (!checkLoginAndGetSkey()) {
                out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL);
                out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
                return doPrint(out.toJsonStr());
            }
            List<PayInfo> payInfos = new ArrayList<PayInfo>();
            // 2、分隔出订单以及卖家uin
            String[] dealCodes = deals.split("\\|");
            if (dealCodes != null && dealCodes.length > 0) {
                for (String dealItem : dealCodes) {
                    String[] dealCodeInfo = dealItem.split("\\~");
                    if (dealCodeInfo != null && dealCodeInfo.length >= 2) {
                        PayInfo payInfo = new PayInfo();
                        payInfo.dealCode = dealCodeInfo[0].trim();
                        payInfo.sellerUin = StringUtil.toLong(dealCodeInfo[1].trim(), 0);
                        payInfos.add(payInfo);
                    }
                }
            }
            if (payInfos.size() > 0){
                tn = payBiz.getUnionPayTn(getWid(),getSk(),payInfos);
            }
            if (null != tn) {
                if (tn.has("errCode") && tn.get("errCode").getAsInt() != 0) {
                    out.setErrCode(tn.get("errCode").getAsInt());
                    if(tn.has("msg") && StringUtil.isNotBlank(tn.get("msg").getAsString()))
                        out.setMsg(tn.get("msg").getAsString());
                    ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_FAILED, 1);
                }
                tn.remove("errCode");
                tn.remove("msg");
                out.setData(tn);
            }
            ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_SUCCESS, 1);
        } catch (Exception e) {
            Log.run.error(e.getMessage(), e);
        }
        return doPrint(out.toJsonStr());
    }

    private String returnResult(JsonOutput out, String payType){
//    	if("v_wx".equals(getPayType())){//虚拟支付
    	if(getPayType().startsWith("v_wx")){//虚拟支付--微店也要用的
	    	String callback = getRequest().getParameter("callback");
	    	if(!StringUtil.isBlank(callback)){
	    		callback = CoderUtil.decodeXssFilter(AntiXssHelper.htmlAttributeEncode(callback));
	    		return doPrint(callback + "(" + out.toJsonStr() + ")");
	    	}
	    }
	    
    	return doPrint(out.toJsonStr());
    }
    
    
    /**
     *  京东收银台支付接口
     *  @author lianghaining1
     *  
     */
//    public String jdCashierPay(){
//    	
//    	Log.run.debug("--------京东收银台支付---------ApiTenpayAction.jdCashierPay()--------------------------");
//    	ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_REQUEST, 1);
//        long start = System.currentTimeMillis();
//        WXPayInfo wxPayInfo = new WXPayInfo();
//
//        String basePath = getRequest().getScheme() + "://" + getRequest().getServerName();
//        int port = getRequest().getServerPort();
//        if (port != 80) {
//            basePath += ":" + port;
//        }
//        basePath += getRequest().getContextPath();
//
//        String callbackUrl = basePath + "/api/pay/uppCallback.xhtml";
//        String clientTenPayUrl = "pay/uppRetback.xhtml";
//
//        // 参数校验
//        if (!validateGettksInput()) {
//            setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
//            return SUCCESS;
//        }
//
//        // 鉴权
//        if (!checkLogin()) {
//            setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, "鉴权失败");
//            return SUCCESS;
//        }
//        //如果以上没有错误则对传入了订单号进行处理为传入接品参数进行准备工作
//        //准备一 个支付列表
//        List<PayInfo> payInfos = new ArrayList<PayInfo>();
//        // 2、分隔出订单以及卖家uin
//        String[] dealCodes = deals.split("\\|");
//        if (dealCodes != null && dealCodes.length > 0) {
//            for (String dealItem : dealCodes) {
//            	//为了取出订单中的订单号以及卖家UID
//                String[] dealCodeInfo = dealItem.split("\\~");
//                if (dealCodeInfo != null && dealCodeInfo.length >= 2) {
//                    PayInfo payInfo = new PayInfo();
//                    //为对支付 类装载数据
//                    payInfo.dealCode = dealCodeInfo[0].trim();
//                    payInfo.sellerUin = StringUtil.toLong(dealCodeInfo[1].trim(), 0);
//                    payInfos.add(payInfo);
//                }
//            }
//        }
//        //如果获取的列表中有值则进行支付
//        if (payInfos.size() > 0){
//        	wxPayInfo = payBiz.getJdPayToken(getWid(),getQq(), payInfos, null,
//                    callbackUrl, callbackUrl, clientTenPayUrl,getSid(),getLk(),getRequest().getRemoteAddr(),getAppID(),getMt(),getVersionCode(), osVersion, screen,getMk());
//        }
//        if (null != wxPayInfo) {
//            JsonConfig jsoncfg = new JsonConfig();
//            
//            if (wxPayInfo.errCode != 0) {
//                result.setErrCode((int) wxPayInfo.errCode);
//                if(wxPayInfo.msg != null)
//                    result.setMsg(wxPayInfo.msg);
//                ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_FAILED, 1);
//            }
//            
//            jsonStr = JSONSerializer.toJSON(wxPayInfo, jsoncfg).toString();
//        }
//
//        long timeCost = System.currentTimeMillis() - start;
//        perflog(genPerfLogString("ApiTenpayAction.jdCashierPay()",
//                InterfaceType.TYPE_ACTION, TYPE_ANDROID, timeCost));
//        runlog("ApiTenpayAction.jdCashierPay() qq:" + getQq() + " deals:" + deals + " wxPayInfo:" + wxPayInfo);
//        reportJmc(Module.S_QGO_API_TENPAY, "gettks", timeCost, result.getErrCode() == ErrConstant.SUCCESS);
//        
//        ItilReporter.attrApiMul(ItilConstants.ITIL_APP_TENPAY_SUCCESS, 1);
//        return SUCCESS;
//    }
    
    /**
     * 确认收货获取财付通token
     * 
     * @return
     */
    public String getCfmRcvTk() {
    	long start = System.currentTimeMillis();
    	
    	// 参数校验
    	if (!validateGetCfmRcvTkInput()) {
    		setErrCodeAndMsg(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数校验不合法");
            return SUCCESS;
		}
    	
    	// 鉴权
    	if (!checkLogin()) {
    		setErrCodeAndMsg(ErrConstant.ERRCODE_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
            return SUCCESS;
		}
    	
    	PayTokenInfo ret = new PayTokenInfo();

        String basePath = getRequest().getScheme() + "://"
                + getRequest().getServerName();
        int port = getRequest().getServerPort();
        if (port != 80)
        {
            basePath += ":" + port;
        }
        basePath += getRequest().getContextPath();

        String callbackUrl = basePath + "/api/pay/callback.xhtml";
        ret = payBiz.getCfmRcvToken(getQq(), tenpayCode, dealCode, minSubDealCode, sellerFee, callbackUrl, getLk());
        if (null != ret) {
            JsonConfig jsoncfg = new JsonConfig();
            String[] excludes = { "size", "empty" };
            if (ret.errCode == 0 && StringUtil.isNotBlank(ret.getH5PayUrl())) {
                jsoncfg.setExcludes(excludes);
            }
            if (ret.errCode != 0) {
                result.setErrCode((int) ret.errCode);
                if(ret.msg != null)
                    result.setMsg(ret.msg);
            }
            jsonStr = JSONSerializer.toJSON(ret, jsoncfg).toString();
        }
    	
    	return SUCCESS;
    }

    /**
     * 
     * @Title: callback
     * @Description: 这个暂时还没有什么作用
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public String callback()
    {
    	Log.run.info("-------财付通---------ApiTenpayAction.callback()---------**cft callback msg:" + msg);//lbl 2014.9.12
        return SUCCESS;
    }

    /**
     * 
     * @Title: retback
     * @Description: B账号财付通支付成功后的回跳
     * @return String 返回类型
     * 
     * @throws
     */
    public String retback()
    {
    	Log.run.info("-------财付通---------ApiTenpayAction.retback()---------**cft retback msg:" + msg);//lbl 2014.9.12
        return SUCCESS;
    }
    
    
    
    public String uppCallback()
    {
    	Log.run.info("-------统一支付---------ApiTenpayAction.callback()---------**cft callback msg:" + msg);//lbl 2014.9.12
    	

        return SUCCESS;
    }

    /**
     * 
     * @throws
     */
    public String uppRetback()
    {
    	Log.run.info("-------统一支付---------ApiTenpayAction.retback()---------**cft retback msg:" + msg);//lbl 2014.9.12
        return SUCCESS;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //---------------get set方法----------------------------
    public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}
	
	public String getDeals()
    {
    	return deals;
    }
    
    public void setDeals(String deals)
    {
    	this.deals = deals;
    }

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public String getDealCode()
    {
        return dealCode;
    }

    public void setDealCode(String dealCode)
    {
        this.dealCode = dealCode;
    }

    public String getPayDesc()
    {
        return payDesc;
    }

    public void setPayDesc(String payDesc)
    {
        this.payDesc = payDesc;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public IPayBiz getPayBiz()
    {
        return payBiz;
    }

    public void setPayBiz(IPayBiz payBiz)
    {
        this.payBiz = payBiz;
    }

    public String getJsonStr()
    {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr)
    {
        this.jsonStr = jsonStr;
    }
    
    public String getTenpayCode() {
		return tenpayCode;
	}

	public void setTenpayCode(String tenpayCode) {
		this.tenpayCode = tenpayCode;
	}

	public long getMinSubDealCode() {
		return minSubDealCode;
	}

	public void setMinSubDealCode(long minSubDealCode) {
		this.minSubDealCode = minSubDealCode;
	}

	public long getSellerFee() {
		return sellerFee;
	}

	public void setSellerFee(long sellerFee) {
		this.sellerFee = sellerFee;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
}
