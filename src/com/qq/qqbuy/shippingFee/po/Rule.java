package com.qq.qqbuy.shippingFee.po;

import java.util.List;

public class Rule {

    private List<String> region;

    private long property;

    private long priceNormal;

    private long priceExpress;

    private long priceEms;

    private long priceNormalAdd;

    private long priceExpressAdd;

    private long priceEmsAdd;

    public List<String> getRegion() {
        return region;
    }

    public void setRegion(List<String> region) {
        this.region = region;
    }

    public long getProperty() {
        return property;
    }

    public void setProperty(long property) {
        this.property = property;
    }

    public long getPriceNormal() {
        return priceNormal;
    }

    public void setPriceNormal(long priceNormal) {
        this.priceNormal = priceNormal;
    }

    public long getPriceExpress() {
        return priceExpress;
    }

    public void setPriceExpress(long priceExpress) {
        this.priceExpress = priceExpress;
    }

    public long getPriceEms() {
        return priceEms;
    }

    public void setPriceEms(long priceEms) {
        this.priceEms = priceEms;
    }

    public long getPriceNormalAdd() {
        return priceNormalAdd;
    }

    public void setPriceNormalAdd(long priceNormalAdd) {
        this.priceNormalAdd = priceNormalAdd;
    }

    public long getPriceExpressAdd() {
        return priceExpressAdd;
    }

    public void setPriceExpressAdd(long priceExpressAdd) {
        this.priceExpressAdd = priceExpressAdd;
    }

    public long getPriceEmsAdd() {
        return priceEmsAdd;
    }

    public void setPriceEmsAdd(long priceEmsAdd) {
        this.priceEmsAdd = priceEmsAdd;
    }
    
    
}
