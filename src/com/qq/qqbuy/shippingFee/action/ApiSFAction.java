package com.qq.qqbuy.shippingFee.action;

import java.util.ArrayList;
import java.util.List;

import com.paipai.component.configcenter.itil.ItilReporter;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;

import com.qq.qqbuy.common.constant.ItilConstants;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.shippingFee.biz.SFBiz;
import com.qq.qqbuy.shippingFee.po.CodShipPo;
import com.qq.qqbuy.shippingFee.po.SFTplDetailInfo;

public class ApiSFAction extends PaipaiApiBaseAction {

    private long sellerUin;
    
    private int freightId;
    
    private SFBiz sfBiz;
    
    private String datastr;
    
    private String ci;
    
    private long rid;
    
    private long cityId;
    
    private long dealTotalFee;
    
    private long shipCodId;
    
    private long buyNum;
    
    private int isMailFree;
    
    /**
     * 用来批量计算商品服务费的参数，格式为itemcode~dealTotalFee~buyNum~rid~isMailFree,itemcode~dealTotalFee~buyNum~rid~isMailFree
     * 分别对应：商品code~批价之后的商品总价~商品数量~收货地址id~是否参加店铺包邮活动0：否  1：是
     */
    private String itemList;
    
    public String get(){
        long start = System.currentTimeMillis();
        try {

            if(sellerUin < 10000){
                throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "sellerUin必须大于10000");
            }
            if(freightId < 10){
                throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "freightId必须大于10");
            }
            
            SFTplDetailInfo detail = sfBiz.getSFTpl(sellerUin, freightId);
            if (detail != null) {
                datastr = obj2Json(detail);
            }
            return RST_SUCCESS;
        } catch (BusinessException e) {
            setErrCodeAndMsg4BExp(e, "sellerUin=" + sellerUin + " freightId=" + freightId);
            return RST_API_FAILURE;
        } catch (Throwable e) {
            setErrcodeAndMsg4Throwable(e, "sellerUin=" + sellerUin + " freightId=" + freightId);
            return RST_API_FAILURE;
        } finally {
            result.setDtag(dtag);
        }
    }
    
    /**
     * 购物车获取服务费
     * @return
     */
    public String getMultiCod(){
    	ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_REQUEST, 1); // 上报请求
    	List<CodShipPo> poList = new ArrayList<CodShipPo>();
    	long start = System.currentTimeMillis();
		try {
			if(null == itemList || "".equals(itemList.trim())){
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "itemList格式不正确");
	    	}
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			String[] itemParams = itemList.split(",");
			for(String  itemParam : itemParams){
				String[] params = itemParam.split("~");
				if(params.length != 5){
					throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "itemList格式不正确");
				}
				for(int i = 0;i < 5;i++){
					long sellerUin = ItemUtil.getSellerUin(params[0]);
					if (sellerUin < 10000) {
						throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "sellerUin必须大于10000");
					}
					ci = params[0];
					dealTotalFee = Long.parseLong(params[1]);
					buyNum = Long.parseLong(params[2]);
					rid = Long.parseLong(params[3]);
					isMailFree = Integer.parseInt(params[4]);
				}
				CodShipPo po = sfBiz.calcCodShipfeeWithCgi(rid, ci, dealTotalFee, buyNum, getWid(), getSk(),isMailFree);
				poList.add(po);
			}
			datastr = obj2Json(poList);
			Log.run.info("ApiSFAction#getMultiCod:" + datastr);
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "sellerUin=" + sellerUin + ",shipCodId=" + shipCodId + ",rid=" + rid + ",cityId="
					+ cityId + ",totalFee=" + dealTotalFee + ",buyNum=" + buyNum + ",innerCode=" + e.getInnerErrCode()
					+ ",innerMsg=" + e.getInnerErrMsg());

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_BUSINESS_FAILED, 1);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "sellerUin=" + sellerUin + ",shipCodId=" + shipCodId + ",rid=" + rid
					+ ",cityId=" + cityId + ",totalFee=" + dealTotalFee + ",buyNum=" + buyNum);

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_FAILED, 1);
			return RST_API_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}
    
	public String getCod() {
		ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_REQUEST, 1); // 上报请求

		long start = System.currentTimeMillis();
		try {
			if (sellerUin < 10000) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "sellerUin必须大于10000");
			}

//			CodShipPo po = sfBiz.calcCodShipfeeV2(rid, sellerUin, cityId, dealTotalFee, shipCodId, buyNum);
			if (!checkLoginAndGetSkey()) {
				setErrCodeAndMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL, ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
				return RST_FAILURE;
			}
			CodShipPo po = sfBiz.calcCodShipfeeWithCgi(rid, ci, dealTotalFee, buyNum, getWid(), getSk(),isMailFree);
			if (po != null) {
				datastr = obj2Json(po);
			}
			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_SUCCESS, 1);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "sellerUin=" + sellerUin + ",shipCodId=" + shipCodId + ",rid=" + rid + ",cityId="
					+ cityId + ",totalFee=" + dealTotalFee + ",buyNum=" + buyNum + ",innerCode=" + e.getInnerErrCode()
					+ ",innerMsg=" + e.getInnerErrMsg());

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_BUSINESS_FAILED, 1);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "sellerUin=" + sellerUin + ",shipCodId=" + shipCodId + ",rid=" + rid
					+ ",cityId=" + cityId + ",totalFee=" + dealTotalFee + ",buyNum=" + buyNum);

			ItilReporter.attrApiMul(ItilConstants.ITIL_APP_CART_CODCALC_FAILED, 1);
			return RST_API_FAILURE;
		} finally {
			result.setDtag(dtag);
		}
	}

    public long getSellerUin() {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin) {
        this.sellerUin = sellerUin;
    }

    public int getFreightId() {
        return freightId;
    }

    public void setFreightId(int freightId) {
        this.freightId = freightId;
    }

    public SFBiz getSfBiz() {
        return sfBiz;
    }

    public void setSfBiz(SFBiz sfBiz) {
        this.sfBiz = sfBiz;
    }

    public String getDatastr() {
        return datastr;
    }

    public void setDatastr(String datastr) {
        this.datastr = datastr;
    }

	public long getRid() {
		return rid;
	}

	public void setRid(long rid) {
		this.rid = rid;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getDealTotalFee() {
		return dealTotalFee;
	}

	public void setDealTotalFee(long dealTotalFee) {
		this.dealTotalFee = dealTotalFee;
	}

	public long getShipCodId() {
		return shipCodId;
	}

	public void setShipCodId(long shipCodId) {
		this.shipCodId = shipCodId;
	}

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public int getIsMailFree() {
		return isMailFree;
	}

	public void setIsMailFree(int isMailFree) {
		this.isMailFree = isMailFree;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}
}
