package com.qq.qqbuy.login.aciton;

import javax.servlet.http.Cookie;

import com.qq.qqbuy.login.biz.JdLoginBiz;
import org.apache.commons.lang3.StringUtils;

import net.sf.json.JSONObject;

import com.jd.wlogin.api.ClientInfo;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.po.LoginResult;
import com.qq.qqbuy.common.util.StrMD5;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.login.biz.ApiLoginBiz;
import com.qq.qqbuy.login.biz.LoginToDayBiz;
import com.qq.qqbuy.login.po.AppToken;
import com.qq.qqbuy.login.po.JDSdkAppToken;
import com.qq.qqbuy.login.po.LoginToDay;
import com.qq.qqbuy.login.po.QQSdkAppToken;
import com.qq.qqbuy.login.po.UnifromLoginPo;
import com.qq.qqbuy.login.po.WXSdkAppToken;
import com.qq.qqbuy.login.po.WtLoginAppToken;

public class ApiLoginAction extends PaipaiApiBaseAction {

	private String type;// 登陆方式 qq,jd,wt
//	private String mk;// 设备唯一标示
	private String mt;// 设备类型
	private String appID;// APP唯一标识
	private String loginToken;// 登陆的详细信息
	private String verifyCode;// 防止数据被篡改的校验串其他信息合成一个字符串，然后坐md5 出来的值
	private String userInfo;// 用户信息
	private String longitude;// 经度
	private String latitude;// 纬度
	private String versionCode;// App版本号
	private String datastr;// 返回的token串
	private String appToken ;//登出时的APPTOKEN
	private String channel;//广告渠道参数
	private String nickName ;//用户昵称
	private String figureUrl ;//用户头像
	private String returnUserInfo = "{}";//返回的用户信息
	private String tokenResult = "{}";
	private Long wid ;//拍拍用户ID
	private String sk ;//用户sk
	private ApiLoginBiz apiLoginBiz;
	private LoginToDayBiz loginToDayBiz ;
	/**
	 * JD登陆
	 */
	private JdLoginBiz jdLoginBiz;

	public String getToken() {
		long start = System.currentTimeMillis();
		try {
			validateParam();
			JSONObject jsonObject = null;
			try {
				jsonObject = JSONObject.fromObject(loginToken);
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "loginToken error");
			}
			String ip = getHttpHeadIp() ;
			int port = getRequest().getRemotePort() ;
			if (ip != null && ip.length() > 6) {
				this.setCp(ip);
			} else {
				this.setCp("127.0.0.1");
			}
			LoginToDay loginToDay = new LoginToDay() ;
			if (TokenUtil.LOGIN_TYPE_WT.equals(type)) {								
				WtLoginAppToken appToken = new WtLoginAppToken();
				this.setUk(jsonObject.getString("uk"));
				if (StringUtil.isEmpty(this.getUk())) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "uk is empty");
				}
				if (!parseUK()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "uk is error");
				}
				LoginResult result = this.getVerifyCodeService()
						.checkLoginForPt(this.getQq(), this.getCp(), null,
								this.getLk());
				Log.run.info("checkLogin in ApiLoginAction for uk " + result);
				if (result.getRetCode() != 0) {
					throw BusinessException
					.createInstance(BusinessErrorType.NOT_LOGIN,
							"checklogin for wtLogin in ApiLoginAction is error:" + result.getRetCode());
				}	
				String nikeName = StringUtils.isEmpty(result.getNickName())?String.valueOf(this.getQq()):result.getNickName() ;
				UnifromLoginPo loginPo = this.apiLoginBiz.getWidFromWTIdl(getMk(),
						this.getQq(), this.getLk(), this.getLk(), nikeName);	//openId暂时先放lk			
				sk = loginPo.getLsk() ;
				wid = loginPo.getWid();
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLk(this.getLk());
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setQq(wid);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.wtTokenToStr(appToken);
				returnUserInfo = userInfo ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;				
			}
			// QQ OPENSDK 校验，后续添加
			else if (TokenUtil.LOGIN_TYPE_QQ.equals(type)) {
				QQSdkAppToken appToken = new QQSdkAppToken();
				String openId = jsonObject.getString("openid");
				String expiresIn = jsonObject.getString("expires_in");
				String accessToken = jsonObject.getString("access_token");
				if (StringUtil.isEmpty(openId) || StringUtil.isEmpty(expiresIn)
						|| StringUtil.isEmpty(accessToken)) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				String checkResult = this.apiLoginBiz.checkQQLogin(accessToken,	openId);				
				JSONObject checkResultObject = JSONObject
						.fromObject(checkResult);
				if (checkResultObject.getInt("ret") != 0) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call checkQQLogin  error " + checkResult);
				}
				String getUinResult = this.apiLoginBiz.getUinFromQQTencentb(
						accessToken, openId);
				Log.run.info("checkLogin in ApiLoginAction for uin "
						+ getUinResult);
				JSONObject getUinResultObject = JSONObject
						.fromObject(getUinResult);
				if (getUinResultObject.getInt("ret") != 0) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call getUinFromQQTencentb  error " + getUinResult);
				}
				Long uin = getUinResultObject.getLong("uin");
				UnifromLoginPo loginPo = this.apiLoginBiz.getWidFromQQIdl(getMk(),
						uin, openId, nickName);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk();
				appToken.setAccessToken(accessToken);
				appToken.setAppID(appID);
				appToken.setExpiresIn(expiresIn);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setOpenId(openId);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.qqTokenToStr(appToken);
				returnUserInfo = checkResult ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// JD OPENSDK 校验 后续添加
			else if (TokenUtil.LOGIN_TYPE_JD.equals(type)) {
				JSONObject userInfoObject ;
				try {
					userInfoObject = JSONObject.fromObject(userInfo);
				} catch (Exception ex) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "userInfo error "+ userInfo);
				}
				JDSdkAppToken appToken = new JDSdkAppToken();
				String pin = jsonObject.getString("pin");
				String a2 = jsonObject.getString("a2");
				String client = userInfoObject.getString("client");
				String osVersion = userInfoObject.getString("osVersion");
				String clientVersion = userInfoObject.getString("clientVersion");
				String appName = userInfoObject.getString("appName");
				String networkType = userInfoObject.getString("networkType");
				
				if (StringUtil.isEmpty(pin) || StringUtil.isEmpty(a2)) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				ClientInfo clientInfo = new ClientInfo() ;
				clientInfo.setAppName(appName) ;
				clientInfo.setClient(client) ;
				clientInfo.setClientIp(this.getCp()) ;
				clientInfo.setClientVersion(clientVersion) ;
				clientInfo.setNetworkType(networkType);
				clientInfo.setOsVersion(osVersion) ;
				clientInfo.setUuid(getMk()) ;
				clientInfo.setArea("") ;
				clientInfo.setScreen("") ;
				this.apiLoginBiz.checkJDLogin(pin, a2, clientInfo);		
				UnifromLoginPo loginPo = this.apiLoginBiz.getWidFromJDIdl(getMk(),pin);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk() ;
				appToken.setA2(a2) ;
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setPin(pin);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.jdTokenToStr(appToken);
				returnUserInfo = this.apiLoginBiz.getJDUserInfo(pin) ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// wx OPENSDK 校验 后续添加
			else if (TokenUtil.LOGIN_TYPE_WX.equals(type)) {
				WXSdkAppToken appToken = new WXSdkAppToken();
				String idTokenResult = "" ;
				String code = "" ;
				String accessToken = "" ;
				String openid = "" ;
				if (jsonObject.containsKey("code")) {
					code = jsonObject.getString("code");				
					if (StringUtil.isEmpty(code)) {
						throw BusinessException.createInstance(
								BusinessErrorType.PARAM_ERROR, "参数不能为空");
					}
					idTokenResult = this.apiLoginBiz.getIDTokenForWXSdk(code) ;				
					JSONObject idTokenResultObject = JSONObject
							.fromObject(idTokenResult);
					
					if (idTokenResultObject.get("errcode") != null ) {
						throw BusinessException.createInstance(
								BusinessErrorType.CALL_OUTIDL_FAIL,
								"call getIDTokenForWXSdk  error,errCode= " + idTokenResultObject.get("errcode")
								+ ",errMsg=" + idTokenResultObject.get("errmsg"));
					}
					accessToken = idTokenResultObject.getString("access_token");
					openid = idTokenResultObject.getString("openid");
				} else {
					accessToken = jsonObject.getString("access_token");
					openid = jsonObject.getString("openid");
					idTokenResult = loginToken ;
				}
				
				String wxUserInfo = this.apiLoginBiz.getWXUserInfo(accessToken, openid);				
				JSONObject wxUserInfoObject = JSONObject
						.fromObject(wxUserInfo);
				if (wxUserInfoObject.get("errcode") != null ) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call getWXUserInfo  error,errCode= " + wxUserInfoObject.get("errcode")
							+ ",errMsg=" + wxUserInfoObject.get("errmsg"));
				}
				String unionid = wxUserInfoObject.getString("unionid");				
				UnifromLoginPo loginPo = this.apiLoginBiz.getWidFromWXIdl(getMk(),
						openid, accessToken, unionid);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk();
				appToken.setCode(code);
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				appToken.setOpenId(openid) ;
				appToken.setAccessToken(accessToken) ;
				appToken.setUnionid(unionid) ;
				datastr = TokenUtil.wxTokenToStr(appToken);
				returnUserInfo = wxUserInfo ;
				tokenResult = idTokenResult ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// PAIPAI 根据WID sk 换取登陆态
			else if (TokenUtil.LOGIN_TYPE_PP.equals(type)) {
				AppToken appToken = new AppToken();
				//Long wid = 0L;
				//String sk = "" ;
				if (!jsonObject.containsKey("wid") || !jsonObject.containsKey("sk")) {					
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				try {
					wid = jsonObject.getLong("wid") ;
					sk = jsonObject.getString("sk") ;					
				} catch (Exception ex) {
					ex.printStackTrace() ;
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数错误");
				}					
				if (!this.apiLoginBiz.checkLoginForWidSk(wid, sk, getMk())) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"checkLogin error");
				}	
				
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);				
				datastr = TokenUtil.appTokenToStr(appToken);
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			long vkInt = Util.genVisitkey(this.getCp(), port) ;
			String vk = String.valueOf(vkInt) ;
			loginToDay.setExt1(vk) ;
			this.getResponse().addCookie(new Cookie("uin",String.valueOf(wid))) ;
			this.getResponse().addCookie(new Cookie("wg_uin",String.valueOf(wid))) ;
			this.getResponse().addCookie(new Cookie("skey",String.valueOf(sk))) ;
			this.getResponse().addCookie(new Cookie("wg_skey",String.valueOf(sk))) ;
			this.getResponse().addCookie(new Cookie("visitkey",String.valueOf(vk))) ;	
			try {				
				/**
				 * 登录成功日志
				 */
				Log.loginSuc.info(start + "\t" + getAppID() + "\t" + loginToDay.getWid() + "\t" + loginToDay.getLoginType() + "\t" 
						+ loginToDay.getLoginIp() + "\t" + loginToDay.getMk() + "\t" + loginToDay.getMt() + "\t"
						+ loginToDay.getLongitude() + "\t" + loginToDay.getLatitude() + "\t" + loginToDay.getExt1()) ;
				//微小店接入了该登录入口，appId=2.只对我们appID=1 的入库和发券
				if ("1".equals(loginToDay.getAppID())) {
					this.loginToDayBiz.writeLoginToDay(loginToDay) ;
				}
				
			} catch (Exception ex) {
				Log.run.info("ApiLoginAction#getToken#loginToDay=" + ex.toString() + ",wid=" + loginToDay.getWid()
						+ ",loginDay=" + loginToDay.getLoginDay()) ;
				//ex.printStackTrace() ;
			}
			return RST_SUCCESS;
		} catch (BusinessException e) {
			/*
			 * 登录失败日志
			 */
			Log.loginErr.info(start + "\t" + getAppID() + "\t" + getType() + "\t" 
					+ getCp() + "\t" + getMk() + "\t" + getMt() + "\t"
					+ getLongitude() + "\t" + getLatitude() + "\t" + getVerifyCode() 
					+ "\t" + getUserInfo() + "\t" + getChannel() + "\t" + e.toString()) ;
			
			setErrCodeAndMsg4BExp(e, "type=" + type + ",mk=" + getMk() + ",mt=" + mt
					+ ",loginToken=" + loginToken + ",userInfo=" + userInfo
					+ ",verifyCode=" + verifyCode + ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			/*
			 * 登录失败日志
			 */
			Log.loginErr.info(start + "\t" + getAppID() + "\t" + getType() + "\t" 
					+ getCp() + "\t" + getMk() + "\t" + getMt() + "\t"
					+ getLongitude() + "\t" + getLatitude() + "\t" + getVerifyCode() 
					+ "\t" + getUserInfo() + "\t" + getChannel() + "\t" + e.toString()) ;
			
			setErrcodeAndMsg4Throwable(e, "type=" + type + ",mk=" + getMk() + ",mt="
					+ mt + ",loginToken=" + loginToken + ",userInfo="
					+ userInfo + ",verifyCode=" + verifyCode + ",longitude="
					+ longitude + ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			return RST_API_FAILURE;
		}  
	}

	/**
	 * 退出登陆入口方法
	 * @return
	 */
	public String logout() {
		long start = System.currentTimeMillis();		
		try {
			Long result = 0L ;
			//参数校验
			if (StringUtil.isEmpty("mk") || StringUtil.isEmpty(appToken) || appToken.length() < 10) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "param is error");
    		}
			//type校验
    		if (!appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)
    				&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)
    				&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
    			throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "type is nonsupport");
    		} 
    		//qq登出
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
    			QQSdkAppToken qqLoginToken = TokenUtil.strToQqToken(appToken);   
        		if (null == qqLoginToken) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "qqLoginToken is null");
        		}        		
        		if (-1 != qqLoginToken.getDeadline() && qqLoginToken.getDeadline() < System.currentTimeMillis()) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "jdLoginToken expire time");
        		}
        		result = apiLoginBiz.loginOutForQQSdk(getMk(), qqLoginToken.getWid(), qqLoginToken.getLsk()) ;        		
    		}  
    		//jd登出
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
    			JDSdkAppToken jdLoginToken = TokenUtil.strToJdToken(appToken);   
        		if (null == jdLoginToken) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "jdLoginToken is null");
        		}        		
        		if (-1 != jdLoginToken.getDeadline() && jdLoginToken.getDeadline() < System.currentTimeMillis()) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "jdLoginToken expire time");
        		}
        		result = apiLoginBiz.loginOutForQQSdk(getMk(), jdLoginToken.getWid(), jdLoginToken.getLsk()) ;        		
    		}  
    		//wt登出
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)) {    			

    			WtLoginAppToken wtLoginToken = TokenUtil.strToWtToken(appToken);       			

        		if (null == wtLoginToken) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wtLoginToken is null");
        		}        		
        		if (-1 != wtLoginToken.getDeadline() && wtLoginToken.getDeadline() < System.currentTimeMillis()) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wtLoginToken expire time");
        		}
        		result = apiLoginBiz.loginOutForQQSdk(getMk(), wtLoginToken.getWid(), wtLoginToken.getLsk()) ; 

    		}
    		//wx登出
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
    			WXSdkAppToken wxLoginToken = TokenUtil.strToWxToken(appToken);   
        		if (null == wxLoginToken) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wxLoginToken is null");
        		}        		
        		if (-1 != wxLoginToken.getDeadline() && wxLoginToken.getDeadline() < System.currentTimeMillis()) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wxLoginToken expire time");
        		}
        		result = apiLoginBiz.loginOutForQQSdk(getMk(), wxLoginToken.getWid(), wxLoginToken.getLsk()) ;        		
    		} 
    		//paipai登出
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
    			AppToken appLoginToken = TokenUtil.strToAppToken(appToken);   
        		if (null == appLoginToken) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wxLoginToken is null");
        		}        		
        		if (-1 != appLoginToken.getDeadline() && appLoginToken.getDeadline() < System.currentTimeMillis()) {
        			throw BusinessException.createInstance(
    						BusinessErrorType.PARAM_ERROR, "wxLoginToken expire time");
        		}
        		result = apiLoginBiz.loginOutForQQSdk(getMk(), appLoginToken.getWid(), appLoginToken.getLsk()) ;        		
    		} 
			datastr = String.valueOf(result) ;			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "appToken=" + appToken + ",mk=" + getMk() + ",mt=" + mt
					+ ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "appToken=" + appToken + ",mk=" + getMk() + ",mt=" + mt
					+ ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}

	/**
	 * 京东侧登陆
	 * @return
	 */
	public String getJdToken() {
		long start = System.currentTimeMillis();
		try {
			validateParam();
			JSONObject jsonObject = null;
			try {
				jsonObject = JSONObject.fromObject(loginToken);
			} catch (Exception ex) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "loginToken error");
			}
			String ip = getRequest().getRemoteAddr();
			if (ip != null && ip.length() > 6) {
				this.setCp(ip);
			} else {
				this.setCp("127.0.0.1");
			}
			LoginToDay loginToDay = new LoginToDay() ;
			if (TokenUtil.LOGIN_TYPE_WT.equals(type)) {
				WtLoginAppToken appToken = new WtLoginAppToken();
				this.setUk(jsonObject.getString("uk"));
				if (StringUtil.isEmpty(this.getUk())) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "uk is empty");
				}
				if (!parseUK()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "uk is error");
				}
				LoginResult result = this.getVerifyCodeService()
						.checkLoginForPt(this.getQq(), this.getCp(), null,
								this.getLk());
				Log.run.info("checkLogin in ApiLoginAction for uk " + result);
				if (result.getRetCode() != 0) {
					throw BusinessException
							.createInstance(BusinessErrorType.NOT_LOGIN,
									"checklogin for wtLogin in ApiLoginAction is error:" + result.getRetCode());
				}
				String nikeName = StringUtils.isEmpty(result.getNickName())?String.valueOf(this.getQq()):result.getNickName() ;
				UnifromLoginPo loginPo = jdLoginBiz.getWidFromWTIdl(getMk(),
						this.getQq(), this.getLk(), this.getLk(), nikeName);	//openId暂时先放lk
				sk = loginPo.getLsk() ;
				wid = loginPo.getWid();
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLk(this.getLk());
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setQq(wid);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.wtTokenToStr(appToken);
				returnUserInfo = userInfo ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// QQ OPENSDK 校验，后续添加
			else if (TokenUtil.LOGIN_TYPE_QQ.equals(type)) {
				QQSdkAppToken appToken = new QQSdkAppToken();
				String openId = jsonObject.getString("openid");
				String expiresIn = jsonObject.getString("expires_in");
				String accessToken = jsonObject.getString("access_token");
				if (StringUtil.isEmpty(openId) || StringUtil.isEmpty(expiresIn)
						|| StringUtil.isEmpty(accessToken)) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				String checkResult = jdLoginBiz.checkQQLogin(accessToken,	openId);
				JSONObject checkResultObject = JSONObject
						.fromObject(checkResult);
				if (checkResultObject.getInt("ret") != 0) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call checkQQLogin  error " + checkResult);
				}
				String getUinResult = jdLoginBiz.getUinFromQQTencentb(
						accessToken, openId);
				Log.run.info("checkLogin in ApiLoginAction for uin "
						+ getUinResult);
				JSONObject getUinResultObject = JSONObject
						.fromObject(getUinResult);
				if (getUinResultObject.getInt("ret") != 0) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call getUinFromQQTencentb  error " + getUinResult);
				}
				Long uin = getUinResultObject.getLong("uin");
				UnifromLoginPo loginPo = jdLoginBiz.getWidFromQQIdl(getMk(),
						uin, openId, nickName);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk();
				appToken.setAccessToken(accessToken);
				appToken.setAppID(appID);
				appToken.setExpiresIn(expiresIn);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setOpenId(openId);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.qqTokenToStr(appToken);
				returnUserInfo = checkResult ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// JD OPENSDK 校验 后续添加
			else if (TokenUtil.LOGIN_TYPE_JD.equals(type)) {
				JSONObject userInfoObject ;
				try {
					userInfoObject = JSONObject.fromObject(userInfo);
				} catch (Exception ex) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "userInfo error "+ userInfo);
				}
				JDSdkAppToken appToken = new JDSdkAppToken();
				String pin = jsonObject.getString("pin");
				String a2 = jsonObject.getString("a2");
				String client = userInfoObject.getString("client");
				String osVersion = userInfoObject.getString("osVersion");
				String clientVersion = userInfoObject.getString("clientVersion");
				String appName = userInfoObject.getString("appName");
				String networkType = userInfoObject.getString("networkType");

				if (StringUtil.isEmpty(pin) || StringUtil.isEmpty(a2)) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				ClientInfo clientInfo = new ClientInfo() ;
				clientInfo.setAppName(appName) ;
				clientInfo.setClient(client) ;
				clientInfo.setClientIp(this.getCp()) ;
				clientInfo.setClientVersion(clientVersion) ;
				clientInfo.setNetworkType(networkType);
				clientInfo.setOsVersion(osVersion) ;
				clientInfo.setUuid(getMk()) ;
				clientInfo.setArea("") ;
				clientInfo.setScreen("") ;
				jdLoginBiz.checkJDLogin(pin, a2, clientInfo);
				UnifromLoginPo loginPo = jdLoginBiz.getWidFromJDIdl(getMk(),pin);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk() ;
				appToken.setA2(a2) ;
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setPin(pin);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.jdTokenToStr(appToken);
				returnUserInfo = jdLoginBiz.getJDUserInfo(pin) ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			// wx OPENSDK 校验 后续添加
			else if (TokenUtil.LOGIN_TYPE_WX.equals(type)) {
				WXSdkAppToken appToken = new WXSdkAppToken();
				String code = jsonObject.getString("code");
				if (StringUtil.isEmpty(code)) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "参数不能为空");
				}
				String idTokenResult = jdLoginBiz.getIDTokenForWXSdk(code) ;
				JSONObject idTokenResultObject = JSONObject
						.fromObject(idTokenResult);

				if (idTokenResultObject.get("errcode") != null ) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call getIDTokenForWXSdk  error,errCode= " + idTokenResultObject.get("errcode")
									+ ",errMsg=" + idTokenResultObject.get("errmsg"));
				}
				String accessToken = idTokenResultObject.getString("access_token");
				String openid = idTokenResultObject.getString("openid");
				String wxUserInfo = jdLoginBiz.getWXUserInfo(accessToken, openid);
				JSONObject wxUserInfoObject = JSONObject
						.fromObject(wxUserInfo);
				if (wxUserInfoObject.get("errcode") != null ) {
					throw BusinessException.createInstance(
							BusinessErrorType.CALL_OUTIDL_FAIL,
							"call getWXUserInfo  error,errCode= " + wxUserInfoObject.get("errcode")
									+ ",errMsg=" + wxUserInfoObject.get("errmsg"));
				}
				String unionid = wxUserInfoObject.getString("unionid");

				UnifromLoginPo loginPo = jdLoginBiz.getWidFromWXIdl(getMk(),
						openid, accessToken, unionid);
				wid = loginPo.getWid() ;
				sk = loginPo.getLsk();
				appToken.setCode(code);
				appToken.setAppID(appID);
				appToken.setLatitude(latitude);
				appToken.setLongitude(longitude);
				appToken.setLsk(sk);
				appToken.setMk(getMk());
				appToken.setMt(mt);
				appToken.setType(type);
				appToken.setVersionCode(versionCode);
				appToken.setWid(wid);
				datastr = TokenUtil.wxTokenToStr(appToken);
				returnUserInfo = wxUserInfo ;
				loginToDay = this.loginToDayBiz.convertToLoginToDay(appToken, this.getCp()) ;
			}
			this.getResponse().addCookie(new Cookie("uin",String.valueOf(wid))) ;
			this.getResponse().addCookie(new Cookie("wg_uin",String.valueOf(wid))) ;
			this.getResponse().addCookie(new Cookie("skey",String.valueOf(sk))) ;
			this.getResponse().addCookie(new Cookie("wg_skey",String.valueOf(sk))) ;
			try {
				/**
				 * 登录成功日志
				 */
				Log.loginSuc.info(start + "\t" + getAppID() + "\t" + loginToDay.getWid() + "\t" + loginToDay.getLoginType() + "\t"
						+ loginToDay.getLoginIp() + "\t" + loginToDay.getMk() + "\t" + loginToDay.getMt() + "\t"
						+ loginToDay.getLongitude() + "\t" + loginToDay.getLatitude()) ;

				this.loginToDayBiz.writeLoginToDay(loginToDay) ;

			} catch (Exception ex) {
				Log.run.info("ApiLoginAction#getToken#loginToDay=" + ex.toString() + ",wid=" + loginToDay.getWid()
						+ ",loginDay=" + loginToDay.getLoginDay()) ;
				//ex.printStackTrace() ;
			}
			return RST_SUCCESS;
		} catch (BusinessException e) {
			/*
			 * 登录失败日志
			 */
			Log.loginErr.info(start + "\t" + getAppID() + "\t" + getType() + "\t"
					+ getCp() + "\t" + getMk() + "\t" + getMt() + "\t"
					+ getLongitude() + "\t" + getLatitude() + "\t" + getVerifyCode()
					+ "\t" + getUserInfo() + "\t" + getChannel() + "\t" + e.toString()) ;

			setErrCodeAndMsg4BExp(e, "type=" + type + ",mk=" + getMk() + ",mt=" + mt
					+ ",loginToken=" + loginToken + ",userInfo=" + userInfo
					+ ",verifyCode=" + verifyCode + ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			return RST_API_FAILURE;
		} catch (Throwable e) {
			/*
			 * 登录失败日志
			 */
			Log.loginErr.info(start + "\t" + getAppID() + "\t" + getType() + "\t"
					+ getCp() + "\t" + getMk() + "\t" + getMt() + "\t"
					+ getLongitude() + "\t" + getLatitude() + "\t" + getVerifyCode()
					+ "\t" + getUserInfo() + "\t" + getChannel() + "\t" + e.toString()) ;

			setErrcodeAndMsg4Throwable(e, "type=" + type + ",mk=" + getMk() + ",mt="
					+ mt + ",loginToken=" + loginToken + ",userInfo="
					+ userInfo + ",verifyCode=" + verifyCode + ",longitude="
					+ longitude + ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			return RST_API_FAILURE;
		}
	}

	/**
	 * 京东侧退出登陆入口方法
	 * @return
	 */
	public String jdlogout() {
		long start = System.currentTimeMillis();
		try {
			Long result = 0L ;
			//参数校验
			if (StringUtil.isEmpty("mk") || StringUtil.isEmpty(appToken) || appToken.length() < 10) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "param is error");
			}
			//type校验
			if (!appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)
					&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)&&!appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
				throw BusinessException.createInstance(
						BusinessErrorType.PARAM_ERROR, "type is nonsupport");
			}
			//qq登出
			if (appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
				QQSdkAppToken qqLoginToken = TokenUtil.strToQqToken(appToken);
				if (null == qqLoginToken) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "qqLoginToken is null");
				}
				if (-1 != qqLoginToken.getDeadline() && qqLoginToken.getDeadline() < System.currentTimeMillis()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "jdLoginToken expire time");
				}
				result = jdLoginBiz.loginOutForQQSdk(getMk(), qqLoginToken.getWid(), qqLoginToken.getLsk()) ;
			}
			//jd登出
			if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
				JDSdkAppToken jdLoginToken = TokenUtil.strToJdToken(appToken);
				if (null == jdLoginToken) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "jdLoginToken is null");
				}
				if (-1 != jdLoginToken.getDeadline() && jdLoginToken.getDeadline() < System.currentTimeMillis()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "jdLoginToken expire time");
				}
				result = jdLoginBiz.loginOutForQQSdk(getMk(), jdLoginToken.getWid(), jdLoginToken.getLsk()) ;
			}
			//wt登出
			if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)) {

				WtLoginAppToken wtLoginToken = TokenUtil.strToWtToken(appToken);

				if (null == wtLoginToken) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "wtLoginToken is null");
				}
				if (-1 != wtLoginToken.getDeadline() && wtLoginToken.getDeadline() < System.currentTimeMillis()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "wtLoginToken expire time");
				}
				result = jdLoginBiz.loginOutForQQSdk(getMk(), wtLoginToken.getWid(), wtLoginToken.getLsk()) ;

			}
			//wx登出
			if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
				WXSdkAppToken wxLoginToken = TokenUtil.strToWxToken(appToken);
				if (null == wxLoginToken) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "wxLoginToken is null");
				}
				if (-1 != wxLoginToken.getDeadline() && wxLoginToken.getDeadline() < System.currentTimeMillis()) {
					throw BusinessException.createInstance(
							BusinessErrorType.PARAM_ERROR, "wxLoginToken expire time");
				}
				result = jdLoginBiz.loginOutForQQSdk(getMk(), wxLoginToken.getWid(), wxLoginToken.getLsk()) ;
			}
			datastr = String.valueOf(result) ;
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "appToken=" + appToken + ",mk=" + getMk() + ",mt=" + mt
					+ ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "appToken=" + appToken + ",mk=" + getMk() + ",mt=" + mt
					+ ",longitude=" + longitude
					+ ",latitude=" + latitude + ",appID=" + appID + ",channel=" + channel
					+ ",versionCode=" + versionCode);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}
	}

	/**
	 * 登陆校验
	 * 
	 * @throws BusinessException
	 */
	private void validateParam() throws BusinessException {
		if (StringUtil.isEmpty(type) || StringUtil.isEmpty(getMk())
				|| StringUtil.isEmpty(mt) || StringUtil.isEmpty(loginToken)
				|| StringUtil.isEmpty(verifyCode) || StringUtil.isEmpty(appID) 
				|| StringUtil.isEmpty(versionCode)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "参数不能为空");
		}
		if (!TokenUtil.LOGIN_TYPE_JD.equals(type)
				&& !TokenUtil.LOGIN_TYPE_QQ.equals(type)
				&& !TokenUtil.LOGIN_TYPE_WT.equals(type)
				&& !TokenUtil.LOGIN_TYPE_WX.equals(type)
				&& !TokenUtil.LOGIN_TYPE_PP.equals(type)) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "type error");
		}
		StringBuffer sb = new StringBuffer();
		sb.append(type).append(getMk()).append(mt).append(loginToken)
				.append(userInfo).append(longitude).append(latitude)
				.append(appID).append(versionCode).append(TokenUtil.LOGIN_KEY);
		StrMD5 md5 = new StrMD5(sb.toString());
		if (!verifyCode.equals(md5.getResult())) {
			throw BusinessException.createInstance(
					BusinessErrorType.PARAM_ERROR, "verifyCode error");
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

//	public String getMk() {
//		return mk;
//	}
//
//	public void setMk(String mk) {
//		this.mk = mk;
//	}

	public String getLoginToken() {
		return loginToken;
	}

	public void setLoginToken(String loginToken) {
		this.loginToken = loginToken;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(String userInfo) {
		this.userInfo = userInfo;
	}

	public String getDatastr() {
		return datastr;
	}

	public void setDatastr(String datastr) {
		this.datastr = datastr;
	}

	public String getMt() {
		return mt;
	}

	public void setMt(String mt) {
		this.mt = mt;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public ApiLoginBiz getApiLoginBiz() {
		return apiLoginBiz;
	}

	public void setApiLoginBiz(ApiLoginBiz apiLoginBiz) {
		this.apiLoginBiz = apiLoginBiz;
	}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getFigureUrl() {
		return figureUrl;
	}

	public void setFigureUrl(String figureUrl) {
		this.figureUrl = figureUrl;
	}

	public String getReturnUserInfo() {
		return returnUserInfo;
	}

	public void setReturnUserInfo(String returnUserInfo) {
		this.returnUserInfo = returnUserInfo;
	}

	public Long getWid() {
		return wid;
	}

	public void setWid(Long wid) {
		this.wid = wid;
	}

	public String getSk() {
		return sk;
	}

	public void setSk(String sk) {
		this.sk = sk;
	}

	public LoginToDayBiz getLoginToDayBiz() {
		return loginToDayBiz;
	}

	public void setLoginToDayBiz(LoginToDayBiz loginToDayBiz) {
		this.loginToDayBiz = loginToDayBiz;
	}

	public JdLoginBiz getJdLoginBiz() {
		return jdLoginBiz;
	}

	public void setJdLoginBiz(JdLoginBiz jdLoginBiz) {
		this.jdLoginBiz = jdLoginBiz;
	}
	
	

	public String getTokenResult() {
		return tokenResult;
	}

	public void setTokenResult(String tokenResult) {
		this.tokenResult = tokenResult;
	}

	public static void main(String[] args) {
		long time = 0l;
	}
}
