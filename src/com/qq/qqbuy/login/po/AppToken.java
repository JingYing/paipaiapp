package com.qq.qqbuy.login.po;

import java.util.Random;

public class AppToken {

	/**
	 * 登陆类型
	 */
	private String type = "";
	/**
	 * 手机标识
	 */
	private String mk = "";
	/**
	 * 设备类型
	 */
	private String mt = "";
	/**
	 * 经度
	 */
	private String longitude = "" ;
	/**
	 * 维度
	 */
	private String latitude = "" ;
	/**
	 * 拍拍多个APP的唯一识别码
	 */
	private String appID = "";
	/**
	 * App版本
	 */
	private String versionCode = "";
	/**
	 * 拍拍用户ID
	 */
	private Long wid;

	/**
	 * 拍拍用户登录标识
	 */
	private String lsk = "";
	/**
	 * 拍拍定义过期时间，-1为永不过期
	 */
	private Long deadline = -1l;	
	/**
	 * 盐
	 */
	private Long salt = new Random(System.nanoTime()).nextLong();
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMk() {
		return mk;
	}
	public void setMk(String mk) {
		this.mk = mk;
	}
	public String getMt() {
		return mt;
	}
	public void setMt(String mt) {
		this.mt = mt;
	}	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAppID() {
		return appID;
	}
	public void setAppID(String appID) {
		this.appID = appID;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}	
	public String getLsk() {
		return lsk;
	}
	public void setLsk(String lsk) {
		this.lsk = lsk;
	}
	public Long getDeadline() {
		return deadline;
	}
	public void setDeadline(Long deadline) {
		this.deadline = deadline;
	}
	public Long getSalt() {
		return salt;
	}
	public void setSalt(Long salt) {
		this.salt = salt;
	}
	
}
