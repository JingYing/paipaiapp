package com.qq.qqbuy.login.po;

public class JDUserInfo {

	private String r ;
	private String ex ;
	private JDUserInfoMsg msg = new JDUserInfoMsg();
	private Boolean execOk ;
	public String getR() {
		return r;
	}

	public void setR(String r) {
		this.r = r;
	}

	public String getEx() {
		return ex;
	}

	public void setEx(String ex) {
		this.ex = ex;
	}

	

	public JDUserInfoMsg getMsg() {
		return msg;
	}

	public void setMsg(JDUserInfoMsg msg) {
		this.msg = msg;
	}

	public Boolean getExecOk() {
		return execOk;
	}

	public void setExecOk(Boolean execOk) {
		this.execOk = execOk;
	}

	
}
