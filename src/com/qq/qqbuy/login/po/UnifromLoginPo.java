package com.qq.qqbuy.login.po;

public class UnifromLoginPo {

	/**
	 * 拍拍用户标识
	 */
	private Long wid ;
	/**
	 * 拍拍登陆标识
	 */
	private String lsk ;
	/**
	 * 昵称
	 */
	private String nickName ;
	public Long getWid() {
		return wid;
	}
	public void setWid(Long wid) {
		this.wid = wid;
	}
	public String getLsk() {
		return lsk;
	}
	public void setLsk(String lsk) {
		this.lsk = lsk;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
}
