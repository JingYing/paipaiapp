package com.qq.qqbuy.login.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.CommonConstants;
import com.qq.qqbuy.login.dao.LoginToDayDao;
import com.qq.qqbuy.login.po.LoginToDay;

public class LoginToDayDaoImpl implements LoginToDayDao {

	private JdbcTemplate jdbcTemplate;

	@Override
	public void insertLoginToDay(LoginToDay loginToDay) throws Exception {
		// TODO Auto-generated method stub
		jdbcTemplate
				.update("insert into loginToDay (wid,loginDay,loginType,loginTimes,insertTime,updateTime,mk"
						+ ",mt,longitude,latitude,appID,versionCode,loginIp,ntext,ext1,ext2,ext3) values ("
						+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
						new Object[] { loginToDay.getWid(),
								loginToDay.getLoginDay(),
								loginToDay.getLoginType(),
								loginToDay.getLoginTimes(),
								loginToDay.getInsertTime(),
								loginToDay.getUpdateTime(), loginToDay.getMk(),
								loginToDay.getMt(), loginToDay.getLongitude(),
								loginToDay.getLatitude(),
								loginToDay.getAppID(),
								loginToDay.getVersionCode(),
								loginToDay.getLoginIp(),
								loginToDay.getAppendInfo(),
								loginToDay.getExt1(), loginToDay.getExt2(),
								loginToDay.getExt3() });
	}

	@Override
	public void updateLoginToDay(LoginToDay loginToDay) throws Exception {
		// TODO Auto-generated method stub
		jdbcTemplate
				.update("update loginToDay set loginType = ?, loginTimes = loginTimes + 1, updateTime = ?, mk = ?, mt = ?, "
						+ "longitude = ?, latitude = ?, appID = ?, versionCode = ?, loginIp = ?, ntext = CONCAT(ntext,?), ext1 = ?, ext2 = ?, "
						+ "ext3 = ? " + " where wid=? and loginDay = ? ",
						new Object[] {
								loginToDay.getLoginType(),
								loginToDay.getUpdateTime(),
								loginToDay.getMk(),
								loginToDay.getMt(),
								loginToDay.getLongitude(),
								loginToDay.getLatitude(),
								loginToDay.getAppID(),
								loginToDay.getVersionCode(),
								loginToDay.getLoginIp(),
								CommonConstants.STR_PARTITION2
										+ loginToDay.getAppendInfo(),
								loginToDay.getExt1(), loginToDay.getExt2(),
								loginToDay.getExt3(), loginToDay.getWid(),
								loginToDay.getLoginDay() });
	}

	@Override
	public LoginToDay findByWidDay(long wid, String loginDay) throws Exception {
		// TODO Auto-generated method stub
		List<LoginToDay> list = jdbcTemplate.query(
				"select * from loginToDay where wid=? and loginDay = ? ",
				new Object[] { wid, loginDay }, new LoginToDayRowMapper());
		if (list!= null && list.size() >0) {
			return list.get(0) ;
		} else {
			return null ;
		}
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	class LoginToDayRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			LoginToDay loginToDay = new LoginToDay();
			loginToDay.setAppID(rs.getString("appID"));
			loginToDay.setExt1(rs.getString("ext1"));
			loginToDay.setExt2(rs.getString("ext2"));
			loginToDay.setExt3(rs.getString("ext3"));
			loginToDay.setInsertTime(rs.getLong("insertTime"));
			loginToDay.setLatitude(rs.getString("latitude"));
			loginToDay.setLoginDay(rs.getString("loginDay"));
			loginToDay.setLoginIp(rs.getString("loginIp"));
			loginToDay.setLoginTimes(rs.getInt("loginTimes"));
			loginToDay.setLoginType(rs.getString("loginType"));
			loginToDay.setLongitude(rs.getString("longitude"));
			loginToDay.setMk(rs.getString("mk"));
			loginToDay.setMt(rs.getString("mt"));
			loginToDay.setNtext(rs.getString("ntext"));
			loginToDay.setUpdateTime(rs.getLong("updateTime"));
			loginToDay.setVersionCode(rs.getString("versionCode"));
			loginToDay.setWid(rs.getLong("wid"));
			return loginToDay;
		}
	}

	@Override
	public List<LoginToDay> findByinsertTime(long beforeTime, long endTime)
			throws Exception {
		// TODO Auto-generated method stub		
		return jdbcTemplate.query(
				"select * from loginToDay where insertTime > ? and insertTime < ? group by wid",
				new Object[] { beforeTime, endTime }, new LoginToDayRowMapper());
	}

	@Override
	public LoginToDay findoldLogBywid(long wid) throws Exception {
		List<LoginToDay> logList = jdbcTemplate.query(
				"select * from loginToDay where wid =? order  by insertTime",
				new Object[] { wid}, new LoginToDayRowMapper());
		Log.run.info("findoldLogBywid"+logList.toString());
		if(logList!=null&&logList.size()>0){
			Log.run.info("findoldLogBywid"+logList.get(0).getWid());
			return logList.get(0);
		}else {
			return null;
		}
		
	}
}
