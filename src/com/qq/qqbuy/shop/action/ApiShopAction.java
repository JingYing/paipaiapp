package com.qq.qqbuy.shop.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.action.AuthRequiredAction;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.favorite.biz.FavoriteBiz;
import com.qq.qqbuy.shop.biz.OrderStyleAdapter;
import com.qq.qqbuy.shop.biz.ShopBiz;
import com.qq.qqbuy.shop.po.ShopCategoryItem;
import com.qq.qqbuy.shop.po.ShopInfo;
import com.qq.qqbuy.shop.po.ShopItems;
import com.qq.qqbuy.shop.po.ShopSimpleItem;
import com.qq.qqbuy.thirdparty.http.search.ShopItemSearchParam;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;
import com.qq.qqbuy.thirdparty.idl.shop.CVItemFilterConstants;
import com.qq.qqbuy.thirdparty.idl.stat.StatClient;
import com.qq.qqbuy.thirdparty.idl.stat.protocol.GetFavoritesByIdResp;
import com.qq.qqbuy.user.biz.UserBiz;
import com.qq.qqbuy.user.po.User;

/**
 * 
 * @ClassName: ShopAction
 * 
 * @Description: 店铺相关api
 * @author wendyhu
 * @date 2013-03-09 下午03:25:21
 */
public class ApiShopAction extends AuthRequiredAction {

	static Logger log = LogManager.getLogger(ApiShopAction.class);
	
	//微店商品价格接口
	protected final String WEIDIAN_DECORATED_URL = "http://party.paipai.com/cgi-bin/v2/wxprice_favor_query";
		
	ShopBiz shopBiz;
	UserBiz userBiz;
	FavoriteBiz favBiz;
	Long shopId;

	/**
	 * 排序方式,默认为 15：定时上架时间逆序
	 */
	private int pageIndex = 0, startIndex = 0, pageSize = 5;

	/**
	 * 操作类型： 1：要查询店铺促销信息
	 */
	private int opt = 0;
	private String key, cid, otype;
	private JsonOutput out = new JsonOutput();
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	/**
	 * 详情页面
	 * @return
	 * @throws BusinessException 
	 */
	public String info() {
		long wid = getWidFromToken(getAppToken());
		ShopInfoPageVo vo = new ShopInfoPageVo();
		ShopInfo s = shopBiz.getShopInfo(shopId);
		if(!"0".equals(s.getSellerUin()))	{
			String MobileNo = s.getMobileNo();
			if(StringUtil.isNotBlank(MobileNo) && MobileNo.trim().length() == 11){
				s.setMobileNo(StringUtil.substring(MobileNo, 0, 3) + "****" + StringUtil.substring(MobileNo, 7, 11));
			}
			
			vo.shopInfo = s;
		}
		
		try {
			User seller = userBiz.getUserDetailInfo(shopId);
			if(hasWeidian(seller.getProperties()) || hasWeixiaodian(seller.getProperties())/**&& shopBiz.isDecorated(shopId)*/)	{
				vo.hasWeidian = true;
				vo.weidianUrl = "http://wd.paipai.com/mshop/" + shopId;
			} else	{
				vo.hasWeidian = false;
				vo.weidianUrl = "";
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		try {
			Map<uint32_t,Integer> statData = StatClient.findFavCount(shopId).getStatData() ;
			Integer i = statData.get(new uint32_t(2));
			if(i != null)
				vo.likeCount = i.intValue(); 
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
			
		if(checkLoginAndGetSkey())	{	//登陆后才能查看以下信息
			vo.isInFav = favBiz.isShopInFav(wid, getMk(), getSk(), shopId);
		}
		
		if(!vo.hasWeidian)	{	//未找到微店时再请求店铺商品列表, 以节约查询时间
			vo.categoryItem = infoPage_catItems(wid);
		}
		
		out.setData(vo.toJsonTree());
		return doPrint(out);
	}
	
	/**
	 * 0_1|2_1|65_1|66_1|76_1|111_1|108_1|41_1|106_1|74_1|117_1|48_1|114_1|29_1|28_1
	 * 92_1且不含50_?, 则代表有微店
	 * @param prop
	 * @return
	 */
	private boolean hasWeidian(String prop)	{
		boolean has92_1 = false, has50 = false;
		String[] arr = prop.split("[|]");
		for(String s : arr)	{
			if("92_1".equals(s))	has92_1 = true;
			if(s.startsWith("50_"))	has50 = true;
		}
		return has92_1 && !has50;
	}
	
	/**
	 * 有95则有微小店
	 * @param prop
	 * @return
	 */
	private boolean hasWeixiaodian(String prop)	{
		for(String s : prop.split("[|]"))	{
			if(s.startsWith("95_"))
				return true;
		}
		return false;
	}
	
//	private Map<ShopCategoryItem, ShopItems> infoPage_catItems(long wid)	{
//		Map<ShopCategoryItem, ShopItems> cat_items = new LinkedHashMap<ShopCategoryItem, ShopItems>();
//		List<ShopCategoryItem> cats = shopBiz.getShopCategoryListByUin(shopId, getMk());
//		for(ShopCategoryItem c : cats)	{
//			Map<String, String> filter = new HashMap<String, String>();
//			filter.put("shopcategory", c.getCategoryId() + "");
//			filter.put("state", "1110");	// 只查询出售中的商品
//			ShopItems items = shopBiz.getShopComdyList(shopId, getMk(), 
//										0, 5, 15, filter);	//取第一页5个,按UP_TIME_DESC = 7排序.
//			cat_items.put(c, items);
//		}
//		return cat_items;
//	}
	
	
	private Map<ShopCategoryItem, List<ShopItem>> infoPage_catItems(long wid)	{
		Map<ShopCategoryItem, List<ShopItem>> cat_items = new LinkedHashMap<ShopCategoryItem, List<ShopItem>>();
		List<ShopCategoryItem> cats = shopBiz.findShopCategory(shopId, getMk());
		for(ShopCategoryItem c : cats)	{
			Pager<ShopItem> pager = shopBiz.findShopItemCgi(shopId, c.getCategoryId()+"", null, 
										ShopItemSearchParam.ORDERSTYLE_最新发布降序, 1, 5);	//取最新的5个商品
			cat_items.put(c, pager.getElements());
		}
		return cat_items;
	}
	
	
	/**
	 * 查询店铺内类目列表
	 * @deprecated 此接口已发布, 要保留. 但返回格式不标准,推荐category2.
	 */
	public String category() {
		try {
			List<ShopCategoryItem> list = shopBiz.findShopCategory(shopId, getMk());
			JsonObject json = new JsonObject();
			json.add("items", gson.toJsonTree(list));
			return doPrint(json);		//该接口没有使用标准JSON返回格式
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return doPrint(null);
		}
	}
	
	/**
	 * 与category接口相比, 使用了标准JSON返回格式
	 * @return
	 */
	public String category2()	{
		List<ShopCategoryItem> list = shopBiz.findShopCategory(shopId, getMk());
		return doPrint(out.setData(gson.toJsonTree(list)));
	}
	
	/**
	 * 店内商品搜索
	 * @deprecated 旧实现,使用IDL, 价格不准.
	 */
	public String productList2() {
		Map<String, String> filter = new HashMap<String, String>();
		filter.put("state", "1110");		// 只查询出售中的商品
		if (Util.isNotEmpty(cid)) {
			filter.put(CVItemFilterConstants.ITEM_FILTER_KEY_SHOPCATEGORY, cid);
		}
		if(Util.isNotEmpty(key))	{
			filter.put(CVItemFilterConstants.ITEM_FILTER_KEY_TITLE, key);
		}
		if(--pageIndex < 0)	pageIndex = 0;
		ShopItems ret = shopBiz.getShopComdyList(shopId, getMk(), new Long(pageIndex), new Long(pageSize),
										Long.parseLong(otype), filter);	
		
		JsonArray itemArr = new JsonArray();
		for(ShopSimpleItem s : ret.getItems())	{
			JsonObject item = new JsonObject();
			item.addProperty("itemid", s.getItemId());
			item.addProperty("title", s.getTitle());
			item.addProperty("price", s.getPrice());
			item.addProperty("soldnum", s.getSoldNum());
			item.addProperty("picurl", s.getImage());
			itemArr.add(item);
		}
		JsonObject json = new JsonObject();
		json.addProperty("total", ret.getFindNum());
		json.addProperty("totalNum", ret.getTotalNum());
		json.add("items", itemArr);
		
		return doPrint(out.setData(json));
	}
	
	public String productList() {
		try {
			Integer.parseInt(cid);
		} catch (NumberFormatException e) {
			cid = "";	//旧接口中, APP提交的CID有误, 需要过滤而不能报错
		}
		Pager<ShopItem> pager = shopBiz.findShopItemCgi(shopId, 
								Util.isEmpty(cid) ? null : cid, 
								Util.isEmpty(key) ? null : key, 
								OrderStyleAdapter.adapt(otype), pageIndex, pageSize);
		
		JsonArray itemArr = new JsonArray();
		for(ShopItem s : pager.getElements())	{
			JsonObject item = new JsonObject();
			item.addProperty("itemid", s.getCommId());
			item.addProperty("title", s.getTitle());
			item.addProperty("price", s.getPrice());
			item.addProperty("soldnum", s.getSaleNum());
			item.addProperty("picurl", s.getImgLL());
			itemArr.add(item);
		}
		
		JsonObject json = new JsonObject();
		json.addProperty("total", pager.getElements().size());
		json.addProperty("totalNum", pager.getTotalCount());
		json.add("items", itemArr);
		return doPrint(out.setData(json));
	}
	
	/**
	 * 测试白名单查询
	 * @return
	 */
	public String isDecorated(){ 
		boolean flag = shopBiz.isDecorated(shopId);
		return doPrint(flag ? "resulut:true" : "resulut:false");
	}

	public ShopBiz getShopBiz() {
		return shopBiz;
	}

	public void setShopBiz(ShopBiz shopBiz) {
		this.shopBiz = shopBiz;
	}

	public FavoriteBiz getFavBiz() {
		return favBiz;
	}

	public void setFavBiz(FavoriteBiz favBiz) {
		this.favBiz = favBiz;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOpt() {
		return opt;
	}

	public void setOpt(int opt) {
		this.opt = opt;
	}

	public JsonOutput getOut() {
		return out;
	}

	public void setOut(JsonOutput out) {
		this.out = out;
	}
	
	
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getOtype() {
		return otype;
	}

	public void setOtype(String otype) {
		this.otype = otype;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setUserBiz(UserBiz userBiz) {
		this.userBiz = userBiz;
	}
}
