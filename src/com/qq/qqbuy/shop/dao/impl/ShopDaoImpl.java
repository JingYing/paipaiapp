package com.qq.qqbuy.shop.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.qq.qqbuy.shop.dao.ShopDao;

public class ShopDaoImpl implements ShopDao{

	private JdbcTemplate jdbcTemplate;
	
	@Override
	@SuppressWarnings("rawtypes")
	public boolean isDecoratedByUin(long uin) {
		boolean flag = false;
		String sql = "select uin from pp_shop_whitelist where uin=?";
		List list = jdbcTemplate.queryForList(sql, new Object[]{"uin"}, Long.class);
		if(null != list && !list.isEmpty()){
			flag = true;
		}
		return flag;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
