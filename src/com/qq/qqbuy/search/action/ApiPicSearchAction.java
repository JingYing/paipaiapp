package com.qq.qqbuy.search.action;


import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.search.biz.PicSearchBiz;

import java.io.File;

/**
 * @author wanghao
 */
public class ApiPicSearchAction extends PaipaiApiBaseAction {

	private PicSearchBiz picSearchBiz;

	/**
	 * 上传文件域
	 */
	private File image;
	/**
	 * 上传文件类型
	 */
	private String imageContentType;
	/**
	 * 上传文件名称
	 */
	private String imageFileName;
	/**
	 * 图片截取横坐标
	 */
	private String xpoint;
	/**
	 * 图片截取纵坐标
	 */
	private String ypoint;
	/**
	 * 图片截取宽度
	 */
	private String wide;
	/**
	 * 图片截取高度
	 */
	private String height;
	/**
	 * 返回app数据
	 */
	private String itemList;
	/**
	 * 返回结果编码
	 */
	private String encode = "utf8";
	/**
	 * 颜色，RGB格式的Red
	 * @return
	 */
	private String red = "";
	/***
	 * 颜色RGB格式的Green
	 */
	private String green = "";
	/**
	 * 颜色RGB格式的Blue
	 */
	private String blue = "";
	/**
	 * 链接超时时间
	 * @return
	 */
	private int connectTimeOut = 10000;
	/**
	 * 读取超时时间
	 * @return
	 */
	private int readTimeOut = 10000;

	/**
	 * 接口请求返回数据量
	 * @return
	 */
	private int num = 50;

	/**
	 * 类别信息（包是1，衣服是2，鞋子是3）
	 */
	private long category;

	/**
	 * 拍照购
	 * @return
	 */
	public String list(){

		boolean isNeedProxy = true;
		try {
			if(image == null || !image.exists()){
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "请先上传图片");
			}
			String name = image.getName();
			Log.run.info("Image name is : " + name + "--" + imageFileName + "---" + imageContentType);
			JsonArray items = picSearchBiz.picSearchItems(image,imageFileName,imageContentType,isNeedProxy,encode,connectTimeOut,readTimeOut,num);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg(e.getErrCode(), e.getErrMsg());
			return RST_API_FAILURE;
		}
	}

	/**
	 * 根据颜色搜索商品列表
	 * @return
	 */
	public String colorList(){

		boolean isNeedProxy = true;
		try {
			if(StringUtil.isBlank(red)  || StringUtil.isBlank(green)|| StringUtil.isBlank(blue)){
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "请选择颜色");
			}
			Log.run.info("Color is : " + red+green+blue );
			JsonArray items = picSearchBiz.colorSearchItems(red,green,blue, isNeedProxy, encode,connectTimeOut,readTimeOut,num);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg(e.getErrCode(), e.getErrMsg());
			return RST_API_FAILURE;
		}
	}

	/**
	 * 拍图购分类搜索接口
	 */
	public String cateList(){
		boolean isNeedProxy = true;
		JsonOutput out = new JsonOutput();
		try {
			if(image == null || !image.exists()){
				out.setErrCode(BusinessErrorType.PARAM_ERROR + "");
				out.setMsg("请先上传图片");
			}
			String name = image.getName();
			Log.run.info("Image name is : " + name + "--" + imageFileName + "---" + imageContentType);
			JsonArray items = picSearchBiz.cateSearchItems(category, image, imageFileName, imageContentType, isNeedProxy, encode, connectTimeOut, readTimeOut,num);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			JsonParser parser = new JsonParser();
			JsonElement data = parser.parse(itemList);
			out.setData(data);
		} catch (BusinessException e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
			e.printStackTrace();
		}
		return doPrint(out.toJsonStr());
	}

	public PicSearchBiz getPicSearchBiz() {
		return picSearchBiz;
	}

	public void setPicSearchBiz(PicSearchBiz picSearchBiz) {
		this.picSearchBiz = picSearchBiz;
	}

	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	public String getImageContentType() {
		return imageContentType;
	}

	public void setImageContentType(String imageContentType) {
		this.imageContentType = imageContentType;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getEncode() {
		return encode;
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}

	public String getXpoint() {
		return xpoint;
	}

	public void setXpoint(String xpoint) {
		this.xpoint = xpoint;
	}

	public String getYpoint() {
		return ypoint;
	}

	public void setYpoint(String ypoint) {
		this.ypoint = ypoint;
	}

	public String getWide() {
		return wide;
	}

	public void setWide(String wide) {
		this.wide = wide;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}

	public String getRed() {
		return red;
	}

	public void setRed(String red) {
		this.red = red;
	}

	public String getGreen() {
		return green;
	}

	public void setGreen(String green) {
		this.green = green;
	}

	public String getBlue() {
		return blue;
	}

	public void setBlue(String blue) {
		this.blue = blue;
	}

	public int getConnectTimeOut() {
		return connectTimeOut;
	}

	public void setConnectTimeOut(int connectTimeOut) {
		this.connectTimeOut = connectTimeOut;
	}

	public int getReadTimeOut() {
		return readTimeOut;
	}

	public void setReadTimeOut(int readTimeOut) {
		this.readTimeOut = readTimeOut;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public long getCategory() {
		return category;
	}

	public void setCategory(long category) {
		this.category = category;
	}
}
