package com.qq.qqbuy.search.po;

import com.qq.qqbuy.common.util.StringUtil;

/**
 * @author winsonwu
 * @Created 2012-11-26 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchItem {

    /**
     * 主图 URL
     */
    private String mainImg;

    /**
     * 主图URL60*60
     */
    private String mainImg60;
    /**
     * 主图 URL
     */
    private String mainImg80;
    /**
     * 主图 URL120*120
     */
    private String mainImg120;
    /**
     * 主图 URL160*160
     */
    private String mainImg160;
    /**
     * 主图 URL
     */
    private String mainImg200;
    /**
     * 主图 URL
     */
    private String mainImg300;

    /**
     * 标题
     */
    private String title;

    /**
     * PC商品详情链接
     */
    private String link;

    /**
     * 商品ID
     */
    private String itemCode;

    /**
     * 店铺QQ号
     */
    private long sellerUin;

    /**
     * 卖家昵称(店铺名)
     */
    private String sellerNickName;

    /**
     * 卖家信用值
     */
    private long sellerCredit;

    /**
     * 卖家等级
     */
    private long sellerLevel;
    /**
     * 邮递费用，注意可能为文字“免邮费”或者以元为单位的邮递费用值，如“10.00”（已处理为2位小数）
     * express与express2字段详细含义如下: 1) 如果是免运费：express为"免运费"，express2无意义 2)
     * 如果express和express2都有价格数据：express表示平邮价格 express2表示快递价格 3)
     * 如果express有价格数据，express2为空串，则express表示快递价格
     */
    private String express;
    /**
     * 邮递费用2.请参见express
     */
    private String express2;

    /**
     * 平邮价格
     */
    private String mailPrice;

    /**
     * 快递价格
     */
    private String expressPrice;

    /**
     * EMS价格
     */
    private String emsPrice;

    /**
     * 商品价格价格，以元为单位，如“22.00”（已处理为2位小数）
     */
    private String price;

    /**
     * 市场价格
     */
    private String marketPrice;

    /**
     * 店铺所在地
     */
    private String shopAddr;

    /**
     * 已售件数
     */
    private long soldNum;

    /**
     * 人气
     */
    private long favNum;

    /**
     * 商家认证状态
     */
    private String userValidStatus;

    /**
     * 先行赔付状态
     */
    private String legend1Status;

    /**
     * 7天退货状态
     */
    private String legend2Status;

    /**
     * 诚保代充状态
     */
    private String legend3Status;

    /**
     * 假一赔三状态
     */
    private String legend4Status;

    /**
     * 货到付款状态
     */
    private String codStatus;

    /**
     * 红包商品状态
     */
    private String redPacketStatus;

    /**
     * 自动发货状态
     */
    private String autoShipStatus;

    /**
     * 裳品廊状态
     */
    private String validClothStatus;

    /**
     * QQ会员价格,以元为单位，已处理为两位小数的格式，如“12.00”
     */
    private String qqVipPrice;

    /**
     * 商品属性串，如"6e20:b|6eba:800|6e17:6|6ec0:200000"
     */
    private String propertyString;

    /**
     * 是否支持彩钻
     */

    private boolean isSupportColorDiamond;
    /**
     * 1-2级彩钻价
     */
    private String vipDiamondPrice;

    /**
     * 支持的红包面额
     */
    private int redPacket;
    /**
     * 商品属性字段，同请求中property字段定义
     */
    private long commProperty;

    /**
     * 手拍商品价格,保留2位小数
     */
    private String qgoPrice;
    /**
     * 是否手拍商品
     */
    private boolean qgoItem;

    // 1=商品是QQ商城的
    private byte commPrpertyQQShop;

    private byte promotion;
    /**
     * 免邮
     */
    private boolean freeMail;

    /**
     * 商品活动标识，43 ： 闪购， 44 ：拍便宜
     */
    private long activePriceType;
    /**
     * 拍便宜商品开团链接
     */
    private String takeCheapLink;

    /**
     * 搜索优化新增已浏览和已下单个性化数据
     * 0、普通商品，1、已购买过的商品，2、已浏览过的商品
     * @return
     */
    private int personalizedTypeTag;

    /**
     * 拍便宜商品标识
     */
    private int istakeCheap;

    /**
     * 闪购商品标识
     */
    private int isFlashSale;

    public byte getPromotion() {
	return promotion;
    }

    public void setPromotion(byte promotion) {
	this.promotion = promotion;
    }

    public String getMainImg() {
	return mainImg;
    }

    public void setMainImg(String mainImg) {
	this.mainImg = mainImg;
    }

    public String getMainImg60() {
	return mainImg60;
    }

    public void setMainImg60(String mainImg60) {
	this.mainImg60 = mainImg60;
    }

    public String getMainImg80() {
	return mainImg80;
    }

    public void setMainImg80(String mainImg80) {
	this.mainImg80 = mainImg80;
    }

    public String getMainImg120() {
	return mainImg120;
    }

    public void setMainImg120(String mainImg120) {
	this.mainImg120 = mainImg120;
    }

    public String getMainImg160() {
	return mainImg160;
    }

    public void setMainImg160(String mainImg160) {
	this.mainImg160 = mainImg160;
    }

    public String getMainImg200() {
	return mainImg200;
    }

    public void setMainImg200(String mainImg200) {
	this.mainImg200 = mainImg200;
    }

    public String getMainImg300() {
	return mainImg300;
    }

    public void setMainImg300(String mainImg300) {
	this.mainImg300 = mainImg300;
    }

    public String getTitle() {
	return title;
    }

    public String getShortTitle() {
	try {
	    if (StringUtil.isNotEmpty(title)
		    && title.getBytes("GB2312").length >= 40) {
		StringBuilder shortTitle = new StringBuilder(StringUtil
			.bSubstring(title, 37));
		shortTitle.append("...");
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getLink() {
	return link;
    }

    public void setLink(String link) {
	this.link = link;
    }

    public String getItemCode() {
	return itemCode;
    }

    public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
    }

    public long getSellerUin() {
	return sellerUin;
    }

    public void setSellerUin(long sellerUin) {
	this.sellerUin = sellerUin;
    }

    public String getSellerNickName() {
	return sellerNickName;
    }

    public void setSellerNickName(String sellerNickName) {
	this.sellerNickName = sellerNickName;
    }

    public long getSellerCredit() {
	return sellerCredit;
    }

    public void setSellerCredit(long sellerCredit) {
	this.sellerCredit = sellerCredit;
    }

    public long getSellerLevel() {
	return sellerLevel;
    }

    public void setSellerLevel(long sellerLevel) {
	this.sellerLevel = sellerLevel;
    }

    public String getMailPrice() {
	return mailPrice;
    }

    public void setMailPrice(String mailPrice) {
	this.mailPrice = mailPrice;
    }

    public String getExpressPrice() {
	return expressPrice;
    }

    public void setExpressPrice(String expressPrice) {
	this.expressPrice = expressPrice;
    }

    public String getEmsPrice() {
	return emsPrice;
    }

    public void setEmsPrice(String emsPrice) {
	this.emsPrice = emsPrice;
    }

    public String getPrice() {
	return price;
    }

    public void setPrice(String price) {
	this.price = price;
    }

    public String getMarketPrice() {
	return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
	this.marketPrice = marketPrice;
    }

    public String getShopAddr() {
	return shopAddr;
    }

    public void setShopAddr(String shopAddr) {
	this.shopAddr = shopAddr;
    }

    public long getSoldNum() {
	return soldNum;
    }

    public void setSoldNum(long soldNum) {
	this.soldNum = soldNum;
    }

    public long getFavNum() {
	return favNum;
    }

    public void setFavNum(long favNum) {
	this.favNum = favNum;
    }

    public String getUserValidStatus() {
	return userValidStatus;
    }

    public void setUserValidStatus(String userValidStatus) {
	this.userValidStatus = userValidStatus;
    }

    public String getLegend1Status() {
	return legend1Status;
    }

    public void setLegend1Status(String legend1Status) {
	this.legend1Status = legend1Status;
    }

    public String getLegend2Status() {
	return legend2Status;
    }

    public void setLegend2Status(String legend2Status) {
	this.legend2Status = legend2Status;
    }

    public String getLegend3Status() {
	return legend3Status;
    }

    public void setLegend3Status(String legend3Status) {
	this.legend3Status = legend3Status;
    }

    public String getLegend4Status() {
	return legend4Status;
    }

    public void setLegend4Status(String legend4Status) {
	this.legend4Status = legend4Status;
    }

    public String getCodStatus() {
	return codStatus;
    }

    public void setCodStatus(String codStatus) {
	this.codStatus = codStatus;
    }

    public String getRedPacketStatus() {
	return redPacketStatus;
    }

    public void setRedPacketStatus(String redPacketStatus) {
	this.redPacketStatus = redPacketStatus;
    }

    public String getAutoShipStatus() {
	return autoShipStatus;
    }

    public void setAutoShipStatus(String autoShipStatus) {
	this.autoShipStatus = autoShipStatus;
    }

    public String getValidClothStatus() {
	return validClothStatus;
    }

    public void setValidClothStatus(String validClothStatus) {
	this.validClothStatus = validClothStatus;
    }

    public String getQqVipPrice() {
	return qqVipPrice;
    }

    public void setQqVipPrice(String qqVipPrice) {
	this.qqVipPrice = qqVipPrice;
    }

    public String getPropertyString() {
	return propertyString;
    }

    public void setPropertyString(String propertyString) {
	this.propertyString = propertyString;
    }

    public boolean isSupportColorDiamond() {
	return isSupportColorDiamond;
    }

    public void setSupportColorDiamond(boolean isSupportColorDiamond) {
	this.isSupportColorDiamond = isSupportColorDiamond;
    }

    public int getRedPacket() {
	return redPacket;
    }

    public void setRedPacket(int redPacket) {
	this.redPacket = redPacket;
    }

    public String getVipDiamondPrice() {
	return vipDiamondPrice;
    }

    public void setVipDiamondPrice(String vipDiamondPrice) {
	this.vipDiamondPrice = vipDiamondPrice;
    }

    public long getCommProperty() {
	return commProperty;
    }

    public void setCommProperty(long commProperty) {
	this.commProperty = commProperty;
    }

    public String getExpress() {
	return express;
    }

    public void setExpress(String express) {
	this.express = express;
    }

    public String getExpress2() {
	return express2;
    }

    public void setExpress2(String express2) {
	this.express2 = express2;
    }

    public String getQgoPrice() {
	return qgoPrice;
    }

    public void setQgoPrice(String qgoPrice) {
	this.qgoPrice = qgoPrice;
    }

    public boolean isQgoItem() {
	return qgoItem;
    }

    public void setQgoItem(boolean qgoItem) {
	this.qgoItem = qgoItem;
    }

    public byte getCommPrpertyQQShop() {
	return commPrpertyQQShop;
    }

    public void setCommPrpertyQQShop(byte commPrpertyQQShop) {
	this.commPrpertyQQShop = commPrpertyQQShop;
    }

    public boolean isFreeMail() {
	return freeMail;
    }

    public void setFreeMail(boolean freeMail) {
	this.freeMail = freeMail;
    }

    public long getActivePriceType() {
        return activePriceType;
    }

    public void setActivePriceType(long activePriceType) {
        this.activePriceType = activePriceType;
    }

    public String getTakeCheapLink() {
        return takeCheapLink;
    }

    public void setTakeCheapLink(String takeCheapLink) {
        this.takeCheapLink = takeCheapLink;
    }

    public int getPersonalizedTypeTag() {
        return personalizedTypeTag;
    }

    public void setPersonalizedTypeTag(int personalizedTypeTag) {
        this.personalizedTypeTag = personalizedTypeTag;
    }

    public int getIstakeCheap() {
        return istakeCheap;
    }

    public void setIstakeCheap(int istakeCheap) {
        this.istakeCheap = istakeCheap;
    }

    public int getIsFlashSale() {
        return isFlashSale;
    }

    public void setIsFlashSale(int isFlashSale) {
        this.isFlashSale = isFlashSale;
    }
}
