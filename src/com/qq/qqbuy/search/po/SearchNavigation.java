package com.qq.qqbuy.search.po;
/**
 * 搜索
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class SearchNavigation {
	
	private String listURL;
	private String classFilterURL;
	private String backURL;

	public String getListURL() {
		return listURL;
	}

	public void setListURL(String listURL) {
		this.listURL = listURL;
	}

	public String getClassFilterURL() {
		return classFilterURL;
	}

	public void setClassFilterURL(String classFilterURL) {
		this.classFilterURL = classFilterURL;
	}

	public String getBackURL() {
		return backURL;
	}

	public void setBackURL(String backURL) {
		this.backURL = backURL;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchNavigation [backURL=").append(backURL).append(
				", classFilterURL=").append(classFilterURL)
				.append(", listURL=").append(listURL).append("]");
		return builder.toString();
	}

}

