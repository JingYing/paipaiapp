package com.qq.qqbuy.search.po;

/**
 * 聚类项. 可以是类目聚类，也可以是属性聚类.
 * 
 * @author rickwang
 */
public class ClusterItem implements java.io.Serializable {
	
	private static final long serialVersionUID = -2242146844228754684L;

	/**
     * 聚类ID. 当类目聚类时为类目ID，当属性聚类时为属性ID
     */
    private long clusterId;
    
    /**
     * 聚类名称. 当类目聚类时为类目名称, 当属性聚类时为属性项名称
     */
    private String clusterName;
    
    /**
     * 该聚类的商品数
     */
    private long clusterCount;
    
    /**
     * 聚类URL
     */
    private String clusterUrl;
    
    /**
     * 选中该节点后的path参数。从clusterUrl中截取出来
     */
    private String path;

	public long getClusterId() {
		return clusterId;
	}

	public void setClusterId(long clusterId) {
		this.clusterId = clusterId;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public long getClusterCount() {
		return clusterCount;
	}

	public void setClusterCount(long clusterCount) {
		this.clusterCount = clusterCount;
	}

	public String getClusterUrl() {
		return clusterUrl;
	}

	public void setClusterUrl(String clusterUrl) {
		this.clusterUrl = clusterUrl;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
    
}