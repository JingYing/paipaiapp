package com.qq.qqbuy.search.util;

/**
 * @author winsonwu
 * @Created 2012-12-20
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchConstant {
	
	public static String SEARCH_DTYPE_JSONP = "jsonp";
	
	public static String SEARCH_DTYPE_JSON = "json";
	
	public static String SEARCH_DTYPE_XML = "xml";
	
	public static String SEARCH_CHARSET_GBK = "gbk";
	
	public static String SEARCH_CHARSET_UTF8 = "utf-8";
	
	public static String SEARCH_PROPERTY_CLUSTER = "cluster";
	/**
     * 搜索来源，表明搜索来自移动电商
     */
    public static final int SF_QGO = 30;
	
	/**
     * 时间升序
     */
    public static final int SEARCH_SORT_TIME_A = 4;
    
    /**
     * 时间降序
     */
    public static final int SEARCH_SORT_TIME_D = 5;
    
    /**
     * 价格升序
     */
    public static final int SEARCH_SORT_PRICE_A = 6;
    
    /**
     * 价格降序
     */
    public static final int SEARCH_SORT_PRICE_D = 7;
    
    /**
     * 信用升序
     */
    public static final int SEARCH_SORT_CREDIT_A = 8;
    
    /**
     * 信用降序
     */
    public static final int SEARCH_SORT_CREDIT_D = 9;
    
    /**
     * 推荐升序
     */
    public static final int SEARCH_SORT_RECOMMEND_A = 12;
    
    /**
     * 推荐降序
     */
    public static final int SEARCH_SORT_RECOMMEND_D = 13;
    
    /**
     * 收藏量降序
     */
    public static final int SEARCH_SORT_STORE_NUM_D = 21 ;
    
    /**
     * 收藏时间降序
     */
    public static final int SEARCH_SORT_STORE_TIME_D = 23 ;      
    
    /**
     * 销量降序
     */
    public static final int SEARCH_SORT_SOLD_COUNT_D = 24;
    
    
    /**
     * 默认排序方式,保持和Pc端一直，排序规则以此为准
     * 6价格升序,7价格降序, 8卖家信用升序, 9卖家信用降序, 21收藏量降序,23收藏时间降序， 24销量降序, 26商品创建时间降序。15:默认排序
	*（后面的是不常用的，pc端也没有这个的排序规则，但是他们的接口人反馈接口目前是支持的，这里只是列出来，需要的话再测试。）【60卖家考试区排序，78自营卖场默认排序，88 cps默认排序，89 cps商品销售量排序，94商品发布时间排序，4商品下架时间升序, 5商品下架时间降序】
     */
    public static final int SEARCH_SORT_DEFAULT = 15;
    
    /////////////////////////////////////////////////////////////
    //
    // 商品过滤条件
    //
    /////////////////////////////////////////////////////////////
    
    /**
     * 大卖场商品过滤
     */
    public static final long PROP_BIG_STORE = 1;
    
    /**
     * 店铺推荐位商品过滤
     */
    public static final long PROP_SHOP_RECOMMEND = 2;
    
    /**
     * Qzone商品过滤
     */
    public static final long PROP_QZONE = 4;
    
    /**
     * 网游7*24小时过滤
     */
    public static final long PROP_ONLINE_GAME = 8;
    
    /**
     * 免运费商品过滤
     */
    public static final long PROP_SHIP_FREE = 16;
    
    /**
     * 移动电商的商品
     */
    public static final long PROP_QGO = 32;
    
    /**
     * 红包商品过滤
     */
    public static final long PROP_RED_PACKET = 64;
    
    /**
     * 优质商品过滤(默认)
     */
    public static final long PROP_HIGH_QUALITY = 128;
    
    /**
     * 会员商品过滤
     */
    public static final long PROP_QQVIP = 256;
    
    /**
     * 7天包退换诚保商品过滤
     */
    public static final long PROP_7DAY_INTEGRITY = 512;
    
    /**
     * 14天包换诚保商品过滤
     */
    public static final long PROP_14DAY_INTEGRITY = 1024;
    
    /**
     * QQVideo商品过滤
     */
    public static final long PROP_QQVIDEO = 2048;
    
    /**
     * 收藏商品过滤
     */
    public static final long PROP_COLLECTION = 4096;
    
    /**
     * 裳品廊认证商品
     */
    public static final long PROP_SHANG_PIN_LANG = 0x2000;
    
    /**
     * 新手机商品
     */
    public static final long PROP_NEW_MOBILE = 0x4000;
    
    /**
     * 闪电发货商品
     */
    public static final long PROP_DELIVER_QUICKLY = 0x8000;
    
    /**
     * 橱窗商品
     */
    public static final long PROP_WINDOW = 0x10000;
    
    /**
     * 假1赔3商品
     */
    public static final long PROP_1FAKE_3INDEMNITY = 0x20000;
    
    /**
     * 手机“官“字
     */
    public static final long PROP_GUAN_MOBILE = 0x40000;
    
    /**
     * 促销信息
     */
    public static final long PROP_PROMOTION_MSG = 0x80000;
    
    /**
     * 快冲入口白名单
     */
    public static final long PROP_FAST_CHARGE_WHITE = 0x100000;
    
    /**
     * 商品有赠品
     */
    public static final long PROP_WITH_GIFT = 0x200000;
    
    /**
     * 商品有积分
     */
    public static final long PROP_WITH_SCORE = 0x400000;
    
    /**
     * 折扣
     */
    public static final long PROP_DISCOUNT = 0x800000;
    
    /**
     * 尚品会
     */
    public static final long PROP_SHANG_PIN_HUI = 0x1000000;
    
    /**
     * 有发票商品
     */
    public static final long PROP_WITH_INVOICE = 0x2000000;
    
    /**
     * 参加促销活动的商品
     */
    public static final long PROP_PROMOTION_ACTIVITY = 0x4000000;
    
    /**
     * 彩钻商品
     */
    public static final long PROP_COLORFUL_DIAMOND = 0x8000000;
    /**
     * QQ网购和拍拍商品
     */
    public static final long PROP_PAIPAI_AND_WANGGOU = 0x10000000000L;

    /**
     * 运费险
     */
    public static final long PROP_REIGHT_INSURANCE = 2305843009213693952L;

}
