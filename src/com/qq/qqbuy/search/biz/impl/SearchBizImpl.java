package com.qq.qqbuy.search.biz.impl;

import java.util.Map;

import com.qq.qqbuy.common.Pager;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.search.biz.SearchBiz;
import com.qq.qqbuy.search.po.SearchPo;
import com.qq.qqbuy.search.util.SearchConverter;
import com.qq.qqbuy.thirdparty.http.search.SearchClient;
import com.qq.qqbuy.thirdparty.http.search.ShopItemSearchParam;
import com.qq.qqbuy.thirdparty.http.search.po.SearchCondition;
import com.qq.qqbuy.thirdparty.http.search.po.SearchResult;
import com.qq.qqbuy.thirdparty.http.search.po.ShopItem;

public class SearchBizImpl implements SearchBiz {
	
	private SearchClient searchClient = new SearchClient();

	@Override
	public SearchPo searchList(SearchCondition condition,Long wid,int versionCode) {
		validateSearchCondition(condition);
		SearchResult searchResult = searchClient.search(condition,wid,versionCode);
		return SearchConverter.convert(searchResult);
	}
	
	@Override
	public SearchPo searchMoreCluster(SearchCondition condition,int versionCode)  {
		// 参数校验
		validateSearchCondition(condition);
		String[] fields = new String[] {"hotclass", "Path", "Property"};
		SearchResult searchResult = searchClient.search4Filter(condition,0L,fields,versionCode);
		SearchPo po = SearchConverter.convert(searchResult);
		return po;
	}
	
	protected boolean validateSearchCondition(SearchCondition condition) throws BusinessException {
		if (condition == null) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_INVALID_PARAMETER, "搜索请求没有参数");
		}
		Map<String, String> errorMsgs = condition.validate();
		if (errorMsgs != null && errorMsgs.size() > 0) {
			throw BusinessException.createInstance(ErrConstant.ERRCODE_INVALID_PARAMETER, "搜索请求参数问题: " + errorMsgs.keySet().toString());
		}
		
		return true;
	}

}
