package com.qq.qqbuy.item.util;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.item.po.PromotionItem;
import com.qq.qqbuy.thirdparty.idl.cms.protocol.TempInfo;

/**
 * 6月大促管理工具类
 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
 */
public class JunePromotionManager {

	private static Set<String> PROMOTION_ITEM_POOL = new HashSet<String>();// 大促商品池
	
	private static Map<String ,PromotionItem> PROMOTION_SPECIAL_ITEM_POOL = new HashMap<String,PromotionItem>();//大促特价商品池
	private static String TEM_FOR_JUNE_PROMOTION = "wap_june_promotion_item";// 储存大促商品的模板id
	private static String TIME_START_JUNE_PROMOTION = "2013-06-04 00:00:00";// 大促开始时间
	private static String TIME_END_JUNE_PROMOTION = "2013-06-09 00:00:00";// 大促结束时间
	private static String TEM_FOR_SPECIAL_ITEM_UNE_PROMOTION = "wap_june_special_item";// 大促特价商品的模板id

	private JunePromotionManager() {

	}

	static {
		if (validateActEffective()) {
			init();
		}else{
			PROMOTION_ITEM_POOL = null;
		}
		if(validateSepcialActEffective()){
			initSpecialItem();
		}
	}

	/**
	 * 初始化商品池，从模板中解析商品存入内存
	 * 
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private static void initSpecialItem() {
		try {
//			TempInfo tem = TemplateManager.getTemplateInfo(
//					TEM_FOR_SPECIAL_ITEM_UNE_PROMOTION, 1, "");
			TempInfo tem = null;
			if (tem != null && StringUtil.isNotEmpty(tem.getContent())) {
				String[] itemPool = tem.getContent().split("\r\n");
				if (itemPool != null && itemPool.length > 0) {
					for (String item : itemPool) {
						if(StringUtil.isNotEmpty(item)){
							String[] items=item.split(",");
							if(items!= null && items.length>=2){
								PromotionItem promotionItem=new PromotionItem();
								promotionItem.setIc(items[1]);
								promotionItem.setBeginTime(items[0]);
								PROMOTION_SPECIAL_ITEM_POOL.put(items[1],promotionItem);
							}
						}
					}
					Log.run.info("JunePromotionManager==>initSpecialItem ,PROMOTION_ITEM_POOL size["+PROMOTION_ITEM_POOL.size()+"]");
				}
			}
		} catch (Exception e) {
			Log.run.error("JunePromotionManager==>initSpecialItem error", e);
		}
	}
	/**
	 * 初始化商品池，从模板中解析商品存入内存
	 * 
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private static void init() {
		if(!validateActEffective()){
			return;
		}
		try {
//			TempInfo tem = TemplateManager.getTemplateInfo(
//					TEM_FOR_JUNE_PROMOTION, 1, "");
			TempInfo tem = null;
			if (tem != null && StringUtil.isNotEmpty(tem.getContent())) {
				String[] itemPool = tem.getContent().split("\r\n");
				if (itemPool != null && itemPool.length > 0) {
					for (String item : itemPool) {
						if (ItemUtil.isValidItemCode(item)) {
							PROMOTION_ITEM_POOL.add(item);
						}
					}
					Log.run.info("JunePromotionManager==>init ,PROMOTION_ITEM_POOL size["+PROMOTION_ITEM_POOL.size()+"]");
				}
			}
		} catch (Exception e) {
			Log.run.error("JunePromotionManager==>init error", e);
		}
	}

	/**
	 * 校验商品是否在大促商品池内
	 * 
	 * @param ic
	 * @return
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static boolean validateItemExist(String ic) {
		return validateActEffective() && PROMOTION_ITEM_POOL != null
				&& PROMOTION_ITEM_POOL.contains(ic);
	}
	
	/**
	 * 校验商品是否在大促商品池内
	 * 
	 * @param ic
	 * @return
	 * @date:2013-5-27
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public static PromotionItem validateSpecialItem(String ic) {
		if( PROMOTION_SPECIAL_ITEM_POOL != null){
			PromotionItem item=PROMOTION_SPECIAL_ITEM_POOL.get(ic);
			return item;
		}
		return null;
	}
	
    /**
     * 确保只在活动期间才有效
     * @return
     * @date:2013-5-27
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
	public static boolean validateActEffective() {
		try {
			Date now = new Date();
			Date end = DateUtils.parseStringToDate(TIME_END_JUNE_PROMOTION,
					DateUtils.FORMAT_YMD_HMS);
			Date start = DateUtils.parseStringToDate(TIME_START_JUNE_PROMOTION,
					DateUtils.FORMAT_YMD_HMS);
			return now.before(end) &&
				(now.after(start) || EnvManager.isNotIdc());
		} catch (Exception e) {
			Log.run.error("JunePromotionManager==>validateActEffective  error", e);
		}
		return false;
	}
	 /**
     * 确保只在活动期间才有效
     * @return
     * @date:2013-5-27
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
	public static boolean validateSepcialActEffective() {
		try {
			Date now = new Date();
			Date end = DateUtils.parseStringToDate(TIME_END_JUNE_PROMOTION,
					DateUtils.FORMAT_YMD_HMS);
			return now.before(end) ;
		} catch (Exception e) {
			Log.run.error("JunePromotionManager==>validateActEffective  error", e);
		}
		return false;
	}
	
	public static void main(String args[]){
		JunePromotionManager.validateActEffective();
	}
}
