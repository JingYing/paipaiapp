package com.qq.qqbuy.item.constant;

public enum ItemExtend
{
  SPID(0, "附属信息spid"), 

  QCC_QID(1, "QCC qid"), 

  JNET_CARDID(2, "骏网卡id"), 

  INFO_CFTHB(3, "财付通红包标识"), 

  QQTMC_ITEM(4, "QQ特卖场物品业务代码"), 

  JNET_CARDINFO(5, "骏网卡密信息"), 

  PPHB(6, "拍拍卖家红包标识"), 

  QQSHOWID(7, "QQShow物品ID"), 

  BANKTMC(8, "银行活动标识"), 

  FRIENDS_MARK(9, "合作伙伴标识，现在使用的是芒果网"), 

  AUTO_CARD(10, "自动发货卡密信息"), 

  WULIU_SELLER(11, "卖家发货物流信息"), 

  WULIU_BUYER(12, "买家发退货物流信息"), 

  DEAL_ETCINFO_FLAG(13, "第三方合作信息,用于标记API信息"), 

  CARD_FIRST_VISITTIME(14, "卡密信息首次查看时间"), 

  CARD_HINTINFO(15, "卡密查看提示信息,卖家设置"), 

  QQVIDEO_ID(16, "qq video id"), 

  QCC_PRESENT(17, "QCC赠送人"), 

  OUFEI_CARDID(18, "欧飞卡ID"), 

  OUFEI_CARDINFO(19, "欧飞卡信息"), 

  KA91_CARDID(20, "91卡ID"), 

  KA91_CARDINFO(21, "91卡信息"), 

  FRIEND_TYPE(22, "合作信息"), 

  WISH_BASEINFO(23, "QQ Wish BaseInfo"), 

  WISH_NOTEINFO(24, "QQ Wish MsgInfo"), 

  CARD_CHARGETYPE(25, "充值方式"), 

  CARD_ATTENTION(26, "充值注意事项,最多可以有3个"), 

  ISD_ID(27, ""), 

  EXPECT_RECVTIME(28, ""), 

  FLASH_SEND(29, ""), 

  PROMOTE_INFO(30, ""), 

  TO_NAME(31, ""), 

  SEND_NAME(32, ""), 

  FIRST_PRICE(33, ""), 

  MEMBER_PRICE(34, ""), 

  REDBRICK_DISCOUNT(35, ""), 

  GREENBRICK_DISCOUNT(36, ""), 

  AUTOCARD_ID(37, ""), 

  AUTOCARD_INFO(38, ""), 

  SIZETABLE_ID(39, ""), 

  COLOR_DIAMOND(40, "彩钻价格，用|号线隔开"), 

  SCENE_PRICE(99, "场景价格(新价格体系) "), 

  SALE_PROMOTION(100, "是否有促销信息的标识位，主要针对满立减满立送的促销优惠活动"), 

  COMDY_DOWN_REASON(128, ""), 

  ITEM_STOCK_ID(129, ""), 

  DEAL_COMDY_ATTR_STOCK(130, ""), 

  DEAL_SEND_GOOD_NO(131, ""), 

  ITEM_EMS_PRICE(132, ""), 

  ITEM_NEW_RED_PAG(133, ""), 

  ITEM_DEAL_MSG(134, ""), 

  ITEM_REBATE_RATE(135, ""), 

  ITEM_PROMOTION_DESC(136, ""), 

  ITEM_ACCESSORY_DESC(137, ""), 

  ITEM_PROMOTION_TYPE(138, ""), 

  ITEM_PRODUCT_ATTR(139, ""), 

  ITEM_TEJIA_TYPE(140, ""), 

  ITEM_HAS_RULE_INFO(141, ""), 

  REFUND_FIRST_ACTION_TIME(142, ""), 

  NOTSAVEDB_START(1024, ""), 

  VISITKEY(1025, ""), 

  BUYER_REMARK(1026, ""), 

  BIN_NUMLIMIT(1027, ""), 

  PPHB_ID(1028, ""), 

  DEAL_GEN_LOG(1029, ""), 

  DEAL_MODIFYPRICE_LOG(1030, ""), 

  DEAL_SOURCE_FLAG(1031, ""), 

  TMP_GENUINE_PRODUCT(1032, ""), 

  DEAL_GEN_TIMESTAMP(1033, ""), 

  MAX(2048, ""), 

  COMDY_LOGO1(3001, ""), 

  COMDY_LOGO2(3002, ""), 

  COMDY_LOGO3(3003, ""), 

  COMDY_LOGO4(3004, ""), 

  EXID_COMDY_STOCK_IMG(3005, "商品库存图片列表"), 

  COMDY_ATTR_OFFSET(20000, ""), 

  ERROR_EXTEND_ITEM(-1, "未知的商品扩展属性");

  private final int code;
  private final String showMeg;

  private ItemExtend(int code, String showMeg)
  {
    this.code = code;
    this.showMeg = showMeg;
  }

  public static ItemExtend getItemExtend(int code)
  {
    for (ItemExtend extend : values()) {
      if (extend.code == code)
        return extend;
    }
    return ERROR_EXTEND_ITEM;
  }

  public int getCode() {
    return this.code;
  }

  public String getShowMeg() {
    return this.showMeg;
  }
}