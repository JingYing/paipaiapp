package com.qq.qqbuy.item.po;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.constant.ItemConstantForWap;
public class ItemStock {
	private StockAttribute rootStockAttr = new StockAttribute();
	private HashMap<String, StockAttribute> mp = new HashMap<String, StockAttribute>();
	private Map<String,List<SaleAttrPair>> viewSkuSaleAttrPo= new LinkedHashMap<String,List<SaleAttrPair>>();
	private Map<String,List<SaleAttrPair>> viewSkuSaleAttrPoForQuickBuy= new LinkedHashMap<String,List<SaleAttrPair>>();
	private Map<String,List<SaleAttrPair>> twoStepAttrPo1= new LinkedHashMap<String,List<SaleAttrPair>>();
	private Map<String,List<Mstock4J>> twoStepAttrPo2= new LinkedHashMap<String,List<Mstock4J>>();
	private Map<String, List<Mstock4J>> oneStepAttrPo = new LinkedHashMap<String, List<Mstock4J>>();
	private int displayType;
	
	

	//最小价格
	public long minPrice;
	//最大价格
	public long maxPrice;
	
	public int maxstep=0;
	public int maxchoosestep=0;
	
	private int stockCount;

	/**
	 * 解析商品库存，得出颜色：红，白，蓝；尺码：大、中、小
	 * @param rsp
	 */
	public ItemStock(List<Mstock4J> itemRs) {
		try{		
		if ( itemRs != null && itemRs.size()>0) {
			minPrice=maxPrice=itemRs.get(0).getStockPrice();
			for (int i = 0; i < itemRs.size(); i++) {
							
				Mstock4J stock = itemRs.get(i);
                if(stock.getStockCount()>0){
                	stockCount++;
				}
	            if(stock.getStockPrice()>=maxPrice){
	            	maxPrice=stock.getStockPrice();
				}
	            if(stock.getStockPrice()<=minPrice){
	            	minPrice=stock.getStockPrice();
				}
				if ( stock.getStockAttr() != null && stock.getStockAttr().length() > 0 ) {
				
					String[] attr = StringUtil.split(stock.getStockAttr(), "|");
					if ( attr != null ) {
						for (int j = 0; j < attr.length; j++) {
							String[] tmp = StringUtil.split(attr[j], ":");
							if ( tmp != null && tmp.length == 2 ) {						
								if(!viewSkuSaleAttrPo.containsKey(tmp[0])){
									List<SaleAttrPair> values=new ArrayList<SaleAttrPair>();
									values.add(getSubStockPair(stock,tmp[1]));
									viewSkuSaleAttrPo.put(tmp[0], values);
								}else{
									List<SaleAttrPair> values =viewSkuSaleAttrPo.get(tmp[0]);
									SaleAttrPair saleAttrPair=getSubStockPair(stock,tmp[1]);
									if(values!=null && !values.contains(saleAttrPair)){
									 values.add(saleAttrPair);
									}
								}
							}
						}
					}
				}
			}
		}
		}catch(Exception e){
			Log.run.error("init stock failed!",e);
		}
	}

	/**
	 * 解析属性串，
	 * 从颜色：红|尺码：大 ，颜色：红|尺码：小，得到红色|大、中、小的形式
	 * @param itemRes
	 * @return
	 * @date:2013-2-26
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public  void  parseItemStockForQuickBuy(List<Mstock4J>  itemRes) {
		for (Mstock4J stock : itemRes) {
			if (StringUtil.isNotEmpty(stock.getStockAttr() )&& stock.getStockCount()>0) {//只展示有库存的
				String[] attr = StringUtil.split(stock.getStockAttr(), "|");
				String firstAttrKey = null;
				String displayAttr = null;//页面展示的属性，只展示库存属性串中第二个属性值
				if (attr != null && attr.length>0 && attr[0]!=null) {
					String[] tmp = StringUtil.split(attr[0], ":");
					if (tmp != null && tmp.length == 2) {
							firstAttrKey = attr.length==1?tmp[0]:tmp[1];//商品只有一个库存时，以属性名作为KEY；有多个库存时，以属性值为KEY						
					}					
						String att=attr.length>1?attr[1]:attr[0];	
						String[] attTmp = StringUtil.split(att, ":");
						displayAttr = (attTmp != null && attTmp.length == 2)?attTmp[1]:attr[0];			
										
					if (!viewSkuSaleAttrPoForQuickBuy.containsKey(firstAttrKey)) {
						List<SaleAttrPair> values=new ArrayList<SaleAttrPair>();
						values.add(getSubStockPair(stock,displayAttr));
						viewSkuSaleAttrPoForQuickBuy.put(firstAttrKey, values);
					} else {					
						List<SaleAttrPair> values =viewSkuSaleAttrPoForQuickBuy.get(firstAttrKey);
						SaleAttrPair saleAttrPair=getSubStockPair(stock,displayAttr);
						if(values!=null && !values.contains(saleAttrPair)){
						 values.add(saleAttrPair);
						}
					}
				}
			}
		}
	}

	/**
	 * 提取单个项库存属性信息
	 * @param stock
	 * @param attr
	 * @return
	 * @date:2013-2-26
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private SaleAttrPair getSubStockPair(Mstock4J stock,String attr ){
		SaleAttrPair subStock=new SaleAttrPair(attr);
		subStock.setStockId(stock.getStockId());
		subStock.setStockAttr(stock.getStockAttr());
		subStock.setCurrState(stock.getStockCount()>0?1:0);
		return subStock;
	}
	
	/**
	 * 提取单个项库存属性信息
	 * @param stock
	 * @param attr
	 * @return
	 * @date:2013-2-26
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private SaleAttrPair getSubStockPair(Mstock4J stock,String stockDisplay,String stockAttr ){
		SaleAttrPair subStock=new SaleAttrPair(stockDisplay);
		subStock.setStockId(stock.getStockId());
		subStock.setStockAttr(stockAttr);
		subStock.setCurrState(stock.getStockCount()>0?1:0);
		return subStock;
	}

	/**
	 * 商详页属性选择页的处理
	 * 分为一页展示和二页展示
	 * @param mstock4JList 库存列表
	 * @return
	 * @date:2013-3-12
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public  void parseSelectItemStock(List<Mstock4J> mstock4JList) {
		
		if(needTwoStepToSelectAttr()){
			displayType=2;
			processTwoStep(mstock4JList);		
		}else{
			displayType=1;
			processOneStep(mstock4JList);			
		}		
	}
	/**
	 * 一步下单选择属性
	 * @param mstock4JList
	 * @date:2013-3-14
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	private void processOneStep(List<Mstock4J> mstock4JList){
		//Map<String, List<Mstock4J>> viewSkuSaleAttrPo = new LinkedHashMap<String, List<Mstock4J>>();			
		for (Mstock4J stock : mstock4JList) {
			if (StringUtil.isNotEmpty(stock.getStockAttr() )&& stock.getStockCount()>0) {//只展示有库存的
				String[] attr = StringUtil.split(stock.getStockAttr(), "|");
				String firstAttrKey = null;
				if (attr != null && attr.length>1 && attr[0]!=null) {
					String[] tmp = StringUtil.split(attr[0], ":");
					if (tmp != null && tmp.length == 2) {
						firstAttrKey = attr[0];
						if (!oneStepAttrPo.containsKey(firstAttrKey)) {
							List<Mstock4J> values = new ArrayList<Mstock4J>();
							values.add(stock);
							oneStepAttrPo.put(firstAttrKey, values);
						} else {
							List<Mstock4J> values = oneStepAttrPo
									.get(firstAttrKey);						
							if (values != null && !values.contains(stock)) {
								values.add(stock);
							}
						}
					}			
				}
			}
		}	
	}
	/**
	 * 二步下单选择属性
	 * @param mstock4JList
	 * @date:2013-3-14
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
    private void processTwoStep(List<Mstock4J> mstock4JList){
    	for (Mstock4J stock : mstock4JList) {
			if (StringUtil.isNotEmpty(stock.getStockAttr() )&& stock.getStockCount()>0) {//只展示有库存的
				String[] attr = StringUtil.split(stock.getStockAttr(), "|");
				String attrKey2 = null;
				String attrKey1 = null;
				if (attr != null && attr.length>=3) {
					    attrKey1 = attr[0];
						attrKey2 = attr[0]+"|"+attr[1];
						String attrValue1=genStep1AttrDisplay(attr);
						stock.setTwoStepDisplayAttrValue(genStep2AttrDisplay(stock.getStockAttr()));
						if (!twoStepAttrPo2.containsKey(attrKey2)) {
							List<Mstock4J> values = new ArrayList<Mstock4J>();
							values.add(stock);
							twoStepAttrPo2.put(attrKey2, values);
						} else {
							List<Mstock4J> values = twoStepAttrPo2
									.get(attrKey2);						
							if (values != null && !values.contains(stock)) {
								values.add(stock);
							}
						}
						if (!twoStepAttrPo1.containsKey(attrKey1)) {
							List<SaleAttrPair> values = new ArrayList<SaleAttrPair>();
							values.add(getSubStockPair(stock,attrValue1,attrKey2));
							twoStepAttrPo1.put(attrKey1, values);
						} else {
							List<SaleAttrPair> values = twoStepAttrPo1
									.get(attrKey1);	
							SaleAttrPair saleAttrPair=getSubStockPair(stock,attrValue1,attrKey2);
							if (values != null && !values.contains(saleAttrPair)) {
								values.add(saleAttrPair);
							}
						}
								
				}
			}
		}
	}
	public ItemStock() {
		super();
	}
	/**
	 * 取总库存，根库存
	 * @return
	 */
	public StockAttribute getRootStockAttr() {
		return rootStockAttr;
	}

	/**
	 * 取某一个库存
	 * @param key
	 * @return
	 */
	public StockAttribute getItemStockAttr(String key) {
		return mp.get(key);
	}
	
	public HashMap<String, StockAttribute> getStockMap() {
		return mp;
	}
	
	public String getStepAttrList(StockAttribute stockAttr) {
		if ( stockAttr != null ) {
			return getStepAttrList(stockAttr.key);
		}
		return "";
	}
	
	public String getStepAttrList(String key) {
		ArrayList<StockAttribute> lst = new ArrayList<StockAttribute>();
		StockAttribute sa = mp.get(key);
		while ( sa != null && !sa.parentKey.equals("r") ) {
			lst.add(sa);
			sa = mp.get(sa.parentKey);
		}
		
		String content = "";
		int j = 1;
		if ( lst.size() > 0 ) {
			for (int i = lst.size() -1 ; i >=0 ; i++) {
				sa = lst.get(i);
				content += j+"."+sa.name+":"+sa.attrName+"<br/>";
				j++;
			}
		}
		return content;
	}
	
	/**
	 * 确定是否需要分步选择下单
	 * 当库存个数大于25，且商品属性个数不小于3，并且所有属性的值个数大于1时，分步选择属性
	 * @param mstock4JList
	 * @return
	 * @date:2013-3-13
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
    private boolean needTwoStepToSelectAttr(){  
//    	if("idc".equals(System.getProperty("qgo.cfg.env"))){
//    		return false;    
//    	}
     	if(stockCount>ItemConstantForWap.MIN_STOCK_COUNT_FOR_TWO_STEP && viewSkuSaleAttrPo!=null && viewSkuSaleAttrPo.size()>=3){
     		int i=0;
    		for(Map.Entry<String,List<SaleAttrPair>> entry:viewSkuSaleAttrPo.entrySet()){
    			i++;
    			if(i>2&&entry.getValue()!=null && entry.getValue().size()<=1){//有属性个数为1时直接下单
    				return false;
    			}
    		}
    		return true;   		
    	}
    	return false;    	
    }
    /**
     * 生成属性选择第二页的展示值，为踢除前两个属性后的库存属性
     * @param stockAttr
     * @return
     * @date:2013-3-14
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    private String genStep2AttrDisplay(String stockAttr) {
		if(StringUtils.isNotEmpty(stockAttr)){
			String[] attr = StringUtil.split(stockAttr, "|");
			if (attr != null && attr.length>2 && stockAttr.length()>=attr[0].length()+attr[1].length()+1) {
				StringBuilder displayAtt=new StringBuilder();
				String[] stocks=StringUtil.split(stockAttr.substring(attr[0].length() +attr[1].length()+ 1), "|");
				if(stocks!=null){
					for(String s:stocks){
						String[] att=s.split(":");
						if(att!=null && att.length==2){
							displayAtt.append(att[1]).append("|");
						}
					}
					if(displayAtt.toString().endsWith("|")){
						return displayAtt.substring(0,displayAtt.length()-1);
					}
					return displayAtt.toString();
				}
				
			}
		}
			return stockAttr;		
		
	}
    /**
     *产生库存选择第 一页的展示值，为第二个属性的值
     * @param attr
     * @return
     * @date:2013-3-14
     * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
     * @version 1.0
     */
    private String genStep1AttrDisplay(String[] attr) {
		if (attr != null && attr.length>2 ) {
				String [] att1Value=attr[1].split(":");
				return (att1Value!=null &&att1Value.length==2)?att1Value[1]:attr[1];
		}
			return "";		
		
	}
	public Map<String, List<SaleAttrPair>> getViewSkuSaleAttrPo() {
		return viewSkuSaleAttrPo;
	}


	public void setViewSkuSaleAttrPo(
			Map<String, List<SaleAttrPair>> viewSkuSaleAttrPo) {
		this.viewSkuSaleAttrPo = viewSkuSaleAttrPo;
	}
	public Map<String, List<SaleAttrPair>> getViewSkuSaleAttrPoForQuickBuy() {
		return viewSkuSaleAttrPoForQuickBuy;
	}
	public void setViewSkuSaleAttrPoForQuickBuy(
			Map<String, List<SaleAttrPair>> viewSkuSaleAttrPoForQuickBuy) {
		this.viewSkuSaleAttrPoForQuickBuy = viewSkuSaleAttrPoForQuickBuy;
	}
	public HashMap<String, StockAttribute> getMp() {
		return mp;
	}
	public void setMp(HashMap<String, StockAttribute> mp) {
		this.mp = mp;
	}

	public int getDisplayType() {
		return displayType;
	}
	public void setDisplayType(int displayType) {
		this.displayType = displayType;
	}
	public long getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(long minPrice) {
		this.minPrice = minPrice;
	}
	public long getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(long maxPrice) {
		this.maxPrice = maxPrice;
	}
	public int getMaxstep() {
		return maxstep;
	}
	public void setMaxstep(int maxstep) {
		this.maxstep = maxstep;
	}
	public int getMaxchoosestep() {
		return maxchoosestep;
	}
	public void setMaxchoosestep(int maxchoosestep) {
		this.maxchoosestep = maxchoosestep;
	}
	public void setRootStockAttr(StockAttribute rootStockAttr) {
		this.rootStockAttr = rootStockAttr;
	}
	
	public int getStockCount() {
		return stockCount;
	}
	public void setStockCount(int stockCount) {
		this.stockCount = stockCount;
	}
	public Map<String, List<SaleAttrPair>> getTwoStepAttrPo1() {
		return twoStepAttrPo1;
	}
	public void setTwoStepAttrPo1(Map<String, List<SaleAttrPair>> twoStepAttrPo1) {
		this.twoStepAttrPo1 = twoStepAttrPo1;
	}
	public Map<String, List<Mstock4J>> getTwoStepAttrPo2() {
		return twoStepAttrPo2;
	}
	public void setTwoStepAttrPo2(Map<String, List<Mstock4J>> twoStepAttrPo2) {
		this.twoStepAttrPo2 = twoStepAttrPo2;
	}
	public Map<String, List<Mstock4J>> getOneStepAttrPo() {
		return oneStepAttrPo;
	}
	public void setOneStepAttrPo(Map<String, List<Mstock4J>> oneStepAttrPo) {
		this.oneStepAttrPo = oneStepAttrPo;
	}
	
}
