package com.qq.qqbuy.item.po;

import java.util.Vector;

public class PPItem4J {
	/**
	 * 商品价格
	 * 
	 * 版本 >= 0
	 */
	private int itemPrice;

	/**
	 * 商品的市场价格
	 * 
	 * 版本 >= 0
	 */
	private int marketPrice;

	/**
	 * 商品的邮寄费用
	 * 
	 * 版本 >= 0
	 */
	private int mailFee;

	/**
	 * 商品彩钻折扣
	 * 
	 * 版本 >= 0
	 */
	private Vector<Integer> discount = new Vector<Integer>();

	/**
	 * 运费模版id
	 * 
	 * 版本 >= 0
	 */
	private int freightId;

	/**
	 * 运费模版
	 * 
	 * 版本 >= 0
	 */
	private PPFreight4J freight = new PPFreight4J();

	/**
	 * 库存信息
	 * 
	 * 版本 >= 0
	 */
	private Vector<PPStock4J> stockList = new Vector<PPStock4J>();


	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public int getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(int marketPrice) {
		this.marketPrice = marketPrice;
	}

	public int getMailFee() {
		return mailFee;
	}

	public void setMailFee(int mailFee) {
		this.mailFee = mailFee;
	}

	public Vector<Integer> getDiscount() {
		return discount;
	}

	public void setDiscount(Vector<Integer> discount) {
		this.discount = discount;
	}

	public int getFreightId() {
		return freightId;
	}

	public void setFreightId(int freightId) {
		this.freightId = freightId;
	}

	public PPFreight4J getFreight() {
		return freight;
	}

	public void setFreight(PPFreight4J freight) {
		this.freight = freight;
	}

	public Vector<PPStock4J> getStockList() {
		return stockList;
	}

	public void setStockList(Vector<PPStock4J> stockList) {
		this.stockList = stockList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PPItem4J [discount=").append(discount).append(
				", freight=").append(freight).append(", freightId=").append(
				freightId).append(", itemPrice=").append(itemPrice).append(
				", mailFee=").append(mailFee).append(", marketPrice=").append(
				marketPrice).append(", stockList=").append(stockList).append(
				"]");
		return builder.toString();
	}

}
