/*jadclipse*/// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.

package com.qq.qqbuy.item.po;

import java.util.Vector;

import com.paipai.lang.uint32_t;

public class ItemStockBo
{

    public ItemStockBo()
    {
        stockId = "";
        stockAttr = "";
        stockDesc = "";
        stockLocalCode = "";
    }

    public String getStockId()
    {
        return stockId;
    }

    public void setStockId(String stockId)
    {
        this.stockId = stockId;
    }

    public String getStockAttr()
    {
        return stockAttr;
    }

    public void setStockAttr(String stockAttr)
    {
        this.stockAttr = stockAttr;
    }

    public String getStockDesc()
    {
        return stockDesc;
    }

    public void setStockDesc(String stockDesc)
    {
        this.stockDesc = stockDesc;
    }

    public String getStockLocalCode()
    {
        return stockLocalCode;
    }

    public void setStockLocalCode(String stockLocalCode)
    {
        this.stockLocalCode = stockLocalCode;
    }

    public long getSellerUin()
    {
        return sellerUin;
    }

    public void setSellerUin(long sellerUin)
    {
        this.sellerUin = sellerUin;
    }

    public long getSoldCount()
    {
        return soldCount;
    }

    public void setSoldCount(long soldCount)
    {
        this.soldCount = soldCount;
    }

    public long getStockPrice()
    {
        return stockPrice;
    }

    public void setStockPrice(long stockPrice)
    {
        this.stockPrice = stockPrice;
    }

    public long getStockCount()
    {
        return stockCount;
    }

    public void setStockCount(long stockCount)
    {
        this.stockCount = stockCount;
    }

    public long getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(long createTime)
    {
        this.createTime = createTime;
    }

    public long getLastModifyTime()
    {
        return lastModifyTime;
    }

    public void setLastModifyTime(long lastModifyTime)
    {
        this.lastModifyTime = lastModifyTime;
    }

    public long getIndex()
    {
        return index;
    }

    public void setIndex(long index)
    {
        this.index = index;
    }

    public Vector<uint32_t> getColorPrice()
    {
        return colorPrice;
    }

    public void setColorPrice(Vector<uint32_t> colorPrice)
    {
        this.colorPrice = colorPrice;
    }

    public long getActivityPrice()
    {
        return activityPrice;
    }

    public void setActivityPrice(long activityPrice)
    {
        this.activityPrice = activityPrice;
    }

    public Vector<uint32_t> getShopVipPrice()
    {
        return shopVipPrice;
    }

    public void setShopVipPrice(Vector<uint32_t> shopVipPrice)
    {
        this.shopVipPrice = shopVipPrice;
    }



    private String stockId;
    private String stockAttr;
    private String stockDesc;
    private String stockLocalCode;
    private long sellerUin;
    private long soldCount;
    private long stockPrice;
    private long stockCount;
    private long createTime;
    private long lastModifyTime;
    private long index;
    /**
	 * 商品vip价格
	 */
	private long itemShopVipPrice ;
	/**
	 * 商品彩钻价格
	 */
	private long itemColorPrice ;
	
	/**
	 * 微店价
	 */
	private long weiShopPrice ;
    /**
     * 彩钻价格，如果有彩钻价格则vector不为空
     * 
     * 版本 >= 0
     */
    private Vector<uint32_t> colorPrice = new Vector<uint32_t>();
    /**
     * 库存属性颜色带图片时显示图片URL
     */
    private String colorPicUrl;
    /**
     * 活动价格
     * 
     * 版本 >= 0
     */
    private long activityPrice;
    /**
	 * 最终价格
	 */
	private long resultPrice ;
	/**
	 * 最终价格说明
	 */
	private int resultPriceType ;

    /**
     * 店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，
     * 黄金会员价，白金会员价，钻石会员价
     * 
     * 版本 >= 1
     */
    private Vector<uint32_t> shopVipPrice = new Vector<uint32_t>();

	

	public long getItemShopVipPrice() {
		return itemShopVipPrice;
	}

	public void setItemShopVipPrice(long itemShopVipPrice) {
		this.itemShopVipPrice = itemShopVipPrice;
	}

	public long getItemColorPrice() {
		return itemColorPrice;
	}

	public void setItemColorPrice(long itemColorPrice) {
		this.itemColorPrice = itemColorPrice;
	}

	public long getWeiShopPrice() {
		return weiShopPrice;
	}

	public void setWeiShopPrice(long weiShopPrice) {
		this.weiShopPrice = weiShopPrice;
	}

	public long getResultPrice() {
		return resultPrice;
	}

	public void setResultPrice(long resultPrice) {
		this.resultPrice = resultPrice;
	}

	public int getResultPriceType() {
		return resultPriceType;
	}

	public void setResultPriceType(int resultPriceType) {
		this.resultPriceType = resultPriceType;
	}

	public String getColorPicUrl() {
		return colorPicUrl;
	}

	public void setColorPicUrl(String colorPicUrl) {
		this.colorPicUrl = colorPicUrl;
	}

	
}

/*
 * DECOMPILATION REPORT
 * 
 * Decompiled from: E:\MyProject\test_openApi\lib\com.paipai.api-2.0.0.jar Total
 * time: 37 ms Jad reported messages/errors: Exit status: 0 Caught exceptions:
 */