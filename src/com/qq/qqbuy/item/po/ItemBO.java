package com.qq.qqbuy.item.po;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.item.constant.ItemExtend;
import com.qq.qqbuy.item.constant.ItemState;
import com.qq.qqbuy.item.po.eval.EvalPo;

public class ItemBO {
	
	private Long leafClassId ;
	/**
	 * 版本字段
	 *//*
	private int v = 1;*/
	/**
	 * 商品编码，商品在拍拍上标识的唯一编码
	 */
	private String itemCode;
	/**
	 * 商品vip价格
	 */
	private long itemShopVipPrice ;
	/**
	 * 微店价
	 */
	private long weiShopPrice ;
	/**
	 * 商品彩钻价格
	 */
	private long itemColorPrice ;
	/**
	 * 该商品是否被收藏  0：未收藏  1：已收藏
	 */
	private int isStore = 0;
	/**
	 * 最终价格
	 */
	private long resultPrice ;
	/**
	 * 最终价格说明
	 */
	private int resultPriceType;
	/**
	 * 该商品是否可买  0：可购买  1：不可购买
	 */
	private int isNotSoldComdy = 0 ;
	/**
	 * 商品链接地址
	 */
	private String itemUrl;
	/**
	 * 商品的微店商品地址
	 */
	private String itemWeiDian ;
	/**
	 * 商品所属店铺的微店地址
	 */
	private String shopWeiDian ;

	/**
	 * 商品名称
	 */
	private String itemName;

	/**
	 * 商家对商品的编码，商家自行保证该编码的唯一性， 否则根据该编码查询可能出错。
	 */
	private String itemLocalCode;

	/**
	 * 商品状态,所有状态代码及含义请看本文档页后说明
	 */
	private ItemState itemState = ItemState.getItemState(2);

	/**
	 * 商品状态的说明
	 */
	private String stateDesc;

	/**
	 * 商品的运费模板id
	 */
	private long freightId;

	/**
	 * 货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 */
	private long codFreightId;
	/**
	 * 当前活动
	 */
	private List<String> currentActivity = new ArrayList<String>() ;	


	public List<String> getCurrentActivity() {
		return currentActivity;
	}

	public void setCurrentActivity(List<String> currentActivity) {
		this.currentActivity = currentActivity;
	}

	/**
	 * 商铺推荐商品
	 */
	private List<ShopItemInfo> shopRecommendItem = new ArrayList<ShopItemInfo>();

	/**
	 * 卖家昵称
	 */
	private String sellerName;

	/******************************/


	/**
	 * 商品的商家自定义属性
	 */
	// private String customAttr;

	/**
	 * 商品的重量
	 */
	// private long weight;
	// private String itemTags;

	/**
	 * 每次加价值
	 */
	// private long incrementPrice;
	// private long protectPrice;
	// private long depositPrice;

	// /**
	// * 商品的详情内容
	// */
	// private String detailInfo;

	// /**
	// * 发布时间
	// */
	// private long createTime;
	//
	// /**
	// * 最后修改时间
	// */
	// private long lastModifyTime;
	//
	// /**
	// * 上次上架时间
	// */
	// private long lastToSaleTime;
	//
	// /**
	// * 上次下架时间
	// */
	// private long lastToStoreTime;
	//
	// /**
	// * 重上架次数
	// */
	// @Deprecated
	// private long reloadCount;
	
	/**
	 * 商品最后上架
	 */
	private long beginTime;
	

	/**
	 * 商品结束时间
	 */
	private long endTime;

	/**
	 * 商品的种类id（店铺自定义分类）
	 */
	private String categoryId;

	/**
	 * 商品的类目id
	 */
	private long classId;

	// /**
	// * 商品上架后卖N天后下架（单位以秒计，目前系统N只支持7天和14天两个值）
	// */
	// private long validDuration;
	//
	// /**
	// * 商品详情页面主题
	// */
	// private Theme theme;
	//
	// /**
	// * 是否为橱窗商品 1是 0否
	// */
	// private byte windowItem;
	//
	// /**
	// * 是否假一赔三 1是 0否
	// */
	// private byte guaranteeCompensation;
	//
	// /**
	// * 是否7天包退 1是 0否
	// */
	// private byte guarantee7Days;
	//
	// /**
	// * 是否14天包换 1是 0否
	// */
	// private byte guarantee14Days;
	//
	// /**
	// * 是否为二手商品 1是 0否
	// */
	// private byte secondHandItem;
	// private SellType sellType;
	//
	// private byte recommendItem;
	//    
	// private long productId;
	//
	// /**
	// * 商品的尺码表Id
	// */
	// private long sizeTableId;
	//
	// private long codId;
	/***************************/

	

	/**
	 * 商品库存总数量
	 */
	private long stockCount;

	public List<ShopItemInfo> getShopRecommendItem() {
		return shopRecommendItem;
	}

	public void setShopRecommendItem(List<ShopItemInfo> shopRecommendItem) {
		this.shopRecommendItem = shopRecommendItem;
	}

	/**
	 * 商品的邮寄费用
	 */
	private long mailPrice;

	/**
	 * 商品的快递费用
	 */
	private long expressPrice;

	/**
	 * 商品的EMS费用
	 */
	private long emsPrice;

	/**
	 * 商品图片连接
	 */
	private String picLink;
	/**
	 * 商品图片连接
	 */
	/*private String picLink80;*/

	/**
	 * 商品图片连接
	 */
	private List<String> extPicsLink = new ArrayList();
	/**
	 * 商品原始图片连接
	 */
	private List<String> originalExtPicsLink = new ArrayList();

	/**
	 * 商品销售单价
	 */
	private long itemPrice;

	/**
	 * 卖家或者买家承担运费的情况 1 卖家承担运费 2 买家承担运费 3 同城交易，无需运费 大于或等于10
	 * 买家承担运费，表示支持运费模板，该值即为运费模板ID
	 */
	private long sellerPayFreight;

	/**
	 * 购买时的数量限制
	 */
	private long buyLimit;

	 /**
	 * 销售的商品数量
	 */
	private long soldTotalCount;
	

	/**
	 * 近期购买商品数量
	 */
	private long buyNum;

	/**
	 * 购买商品的总数量
	 */
	private long totalBuyNum;

	/**
	 * 近期下单的订单次数
	 */
	private long buyCount;

	/**
	 * 下单的订单总次数
	 */
	private long totalBuyCount;

	/**
	 * 国家id
	 */
	private long countryId;

	/**
	 * 省份id
	 */
	private long provinceId;

	/**
	 * 城市id
	 */
	private long cityId;

	/**
	 * 商品的市场价格
	 */
	private long marketPrice;

	 /**
	 * 是否提供保修服务 1是 0否
	 */
	 private byte guaranteeRepair;
	
	// /**
	// * 是否提供发票 1是 0否
	// */
	// private byte invoiceItem;

	/**
	 * 访问的次数
	 */
	private long visitCount;

	/**
	 * 卖家QQ号
	 */
	private long sellerUin;

	private List<ItemStockBo> itemStockBo = new ArrayList();

	private Map<ItemExtend, String> extendInfo = new Hashtable();

	private List<ItemAttrBO> parsedAttrList = new ArrayList();

	/**
	 * 商品的属性组合串 格式如：key1_value1|key1_value1|key1_value1|.....
	 * 例如：13_1|18_1|422_1|.... 40=团购标识 6=推荐位商品 26=今日特价
	 */
	/*
	 * INDEX_PROP_AUTO_CARD_HINT_INFO = 0, //卡密提示信息 INDEX_PROP_DEPOSIT_SELL = 1,
	 * //抵押金拍卖，正在实现中。。。 INDEX_PROP_DISCOUNT_SELL = 2, //折扣商品
	 * INDEX_PROP_CASH_ON_DELIVERY = 3, //货到付款商品 INDEX_PROP_CITY_SELL = 4,
	 * //购买地域限制商品,暂未实现 INDEX_PROP_MOBILE_AUTH = 5, //手机认证用户发布的商品
	 * INDEX_PROP_RECOMM_AUTH = 6, //推荐位商品 INDEX_PROP_VIRTUAL_ITEM = 7, //虚拟商品
	 * INDEX_PROP_QZONE_ITEM = 8, //QZONE商品 INDEX_PROP_JUNWANG_ITEM = 9, //骏网商品
	 * INDEX_PROP_AUDIT_FIRST = 10, //先审后发 INDEX_PROP_AUCTION_HALL = 11,
	 * //拍卖大厅商品 INDEX_PROP_LOW_QUALITY = 12, //低质商品,搜索隐藏 INDEX_PROP_QQ_TMC = 13,
	 * //QQ 特卖场商品 INDEX_PROP_SPECIAL_AUTH = 14, //特权商品，双价商品 INDEX_PROP_AUTO_SEND
	 * = 15, //自动发货商品，又名预存卡密 INDEX_PROP_GOOD_EVAL = 16, //是否有评价
	 * INDEX_PROP_LEAVE_MSG = 17, //是否有留言 INDEX_PROP_14_DAYS_SECURITY = 18,
	 * //14天包退 INDEX_PROP_7_DAYS_SECURITY = 19, //7天包换 INDEX_PROP_QQVIDEO = 20,
	 * //有QQvideo的商品 INDEX_PROP_CLASS_GIRLS = 21, //女装商品 2008-06-03 woodzheng
	 * INDEX_PROP_CLASS_MOBILE = 22, //官字商品 2008-06-03 woodzheng
	 * INDEX_PROP_SEND_FAST = 23, //闪电发货商品(诚保代充) 2008-07-21 woodzheng
	 * INDEX_PROP_SHOP_WINDOW = 24, //橱窗商品 2008-07-21 woodzheng
	 * INDEX_PROP_GENUINE = 25, //正品保证，假一赔三 2008-09-23 leywang
	 * INDEX_PROP_TAKE_WHEN_BIDED = 26, //拍下即扣商品 2008-10-19 woodzheng (one a day
	 * item) INDEX_PROP_HAS_PROMOT_INFO = 27, //商品带有促销信息 2008-12-01 leywang
	 * INDEX_PROP_HAS_STOCK_RECORD = 28, //商品有库存记录 2009-06-01 leywang
	 * 注意:仅限商品模块dao层使用,其它地方请勿引用 INDEX_PROP_HAS_STOCK_ID = 29,
	 * //商品无库存记录,但是有货号,此时需要从扩展表取出货号 leywang INDEX_RPOP_CANT_BUY = 30, //禁止购买
	 * leywang INDEX_PROP_NOUSERD = 31, // 暂时不用
	 * 
	 * //Fproperty1(使用中) INDEX_PROP_THROUGH_TRAIN =32, //直通车属性skydu add
	 * INDEX_PROP_THROUGH_OPENAPI_USER=33, //通过OPEN_API发布的商品skydu
	 * INDEX_PROP_PROMOTIONAL_EVENT_JOIN = 34, //参加促销活动,活动没开始(满立减活动) skydu
	 * INDEX_PROP_PROMOTIONAL_EVENT_START = 35, //促销活动开始满立减活动) skydu
	 * INDEX_PROP_SEARCH_SORT_DUBIOUS=36, //搜索排序可疑标识skydu 2010-05-25
	 * INDEX_PROP_NO_IMAGE_ITEM=37, //商品没有主图skydu 2010-07-09
	 * INDEX_PROP_NOT_RESET_CURRENT_SALES=38, // 不清除当期销售量标记billjiang 2010-08-09
	 * INDEX_PROP_ITEM_AUCTION3=39, // Auction3标识 billjiang 2010-08-10
	 * INDEX_PROP_ITEM_TUANGOU=40, //团购标识 billjiang 2010-08-10
	 * INDEX_PROP_HAS_RELATEDITEMS=41, //关联商品billjiang 2010-08-24
	 * INDEX_PROP_HAS_RELATEDCOMBOS=42, //优惠套餐billjiang 2010-10-12
	 * INDEX_PROP_THROUGH_TRAIN_V2=43, //直通车商品(新版V2)billjiang 2010-10-12
	 * INDEX_PROP_PROMOTION_EVENT_NO=44, //不参加优惠促销的标记anthonywei 2010-10-18
	 * INDEX_PROP_TAKE_WHEN_ORDERED=45, // 下单即扣标识(如果商品有此标识,则用户下单即扣商品数,类似今日特价)
	 * billjiang 2010-12-20 INDEX_PROP_THROUGH_TRAIN_QZONE=46, // 直通车空间直投广告商品标记位
	 * onionxie 2012-01-10 INDEX_PROP_MICROSELLER_LOCK_STOCK=47, // 禁止改商品库存的标记位
	 * INDEX_PROP_THROUGH_TRAIN_REDRABIT=48, //直通车赤兔广告合作投放商品
	 * INDEX_PROP_IMPORT_PRODUCT=49, //进口商品标 INDEX_PROP_FORBID_REVISE_PRICE=50,
	 * //禁止修改价格的标记位 INDEX_PROP_FORBID_REVISE_SHIPMODE=51, //禁止修改运费模式
	 * INDEX_PROP_PRE_SELL=52, //预售商品的标记位
	 * 
	 * 
	 * //Fproperty2(待使用)//商品组内部使用 INDEX_PROP_SELLER_RECOMMAND = 65,
	 * //卖家推荐标记位，占用此标记位，但不使用有问题咨询anthonywei INDEX_PROP_HAS_SHOP_VIP = 66,
	 * //商品有店铺活动的标记位，仅用于itempo_v2接口有效，其他地方不使用，请咨询anthonywei
	 * //另外此标记和96号标记位不可能同事出现，此标记位不会表现出来 INDEX_PROP_7_DAYS_FREE_SECURITY = 67,
	 * //7天免邮包退可以咨询anthonywei INDEX_PROP_FAST_SNED_ITEM = 68, //快速发货的商品
	 * 
	 * //Fproperty3(待使用)//基础组内部使用 INDEX_PROP_SHOP_VIP=96,
	 * //店铺VIP反标识(如果商品有此标识，则不支持店铺VIP) INDEX_PROP_SHOPWINDOW_TEMP=97, //橱窗推荐临时属性位
	 * INDEX_PROP_PLATFORM_CARD=98, //平台卡易售商品 INDEX_PROP_ONLINE_GAME=99,
	 * //网游api商品 INDEX_PROP_HAS_PROPMOTION=100, //商品是否有促销标记位，仅用于itempo_v2接口有效
	 * INDEX_PROP_HAS_NEWACTIVE=101, //商品参加促销活动(非场景价)的标记位，应用在新的活动分库存价格系统中
	 * INDEX_PROP_HAS_SCENE_ACTIVE=102, //商品参加促销活动(场景价)的标记位，应用在新的活动分库存价格系统中
	 * 
	 * 
	 * //Fproperty4(待使用)//外组使用 INDEX_PROP_MOBILE_Q_BUY=128, //手机Q购标记anthonywei
	 * 2010-01-06 INDEX_PROP_MART_RECOMMEND=129, //卖场推荐位属性(boss使用) billjiang
	 * 2010-01-10 INDEX_PROP_ITEM_CPC=130, //BOSS侧点击竞价商品 anthonywei 2011-04-06
	 * INDEX_PROP_ITEM_CPS=131, //CPS for boss INDEX_PROP_MOBILE_O2O =132,
	 * //移动电商的O2O标记 INDEX_PROP_MOBILE_MICROBUY =133, //移动电商微购商品标记
	 * 
	 * //Fdetail_property(使用中) (这些index不能用在查询过滤器) INDEX_PROP_REPAIR = 160, //保修
	 * INDEX_PROP_INVOICE = 161, //发票 INDEX_PROP_PRICEADD = 162, //加价类型
	 * INDEX_PROP_PASS_AUTH = 163, //通过商品审核 INDEX_PROP_LIMIT_DEPOSIT = 164,
	 * //抵押金拍卖限制条件(bit, 上架; nobit, 价格) INDEX_PROP_HAS_RELOAD = 165, //用户设置了自动上架
	 * INDEX_PROP_COMDY_GRAY = 166, //商品灰度属性skydu INDEX_PROP_COMDY_SPH = 167,
	 * //尚品会 INDEX_PROP_ITEM_ACCESSORY = 169, //配件 INDEX_PROP_ITEM_INSTALLMENT =
	 * 170, //分期付款 INDEX_PROP_ITEM_BUYER_CAN_TALK = 171, //表示订单是否要求买单提供信息
	 * INDEX_PROP_ITEM_BUYER_MUST_TALK = 172, //表示订单是否要求买单提供信息,买家必须回答
	 * INDEX_PROP_COMDY_B2C_NEW = 173, //B2C_NEW INDEX_PROP_ITEM_COUPON = 174,
	 * // 购物券 INDEX_PROP_ITEM_HAS_EXT = 175, //是否存在扩展信息 add by leywang
	 * 
	 * //最大值 INDEX_PROP_MAX = 256,
	 */
	private List<Integer> properties = new ArrayList<Integer>();

	// /**
	// * 商品属性
	// */
	private long itemProperty;
/*
	*//**
	 * 推荐搭配商品编码，多个以‘|’号隔开
	 */
	private String relatedItems;

	// *********************版本大于1的字段***********************/

	/**
	 * 图片url
	 */
	private Vector<String> vecImg;

	/**
	 * 可以使用的红包面值最小为多少
	 */
	private long redPrice;

	/**
	 * 彩钻价格，如果有彩钻价格则vector不为空
	 * 
	 * 版本 >= 0
	 */
	private Vector<uint32_t> colorPrice = new Vector<uint32_t>();

	/**
	 * 活动价格
	 * 
	 * 版本 >= 0
	 */
	private long activityPrice;
	
	private String activityDes ;

	/**
	 * 店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，
	 * 黄金会员价，白金会员价，钻石会员价
	 * 
	 * 版本 >= 1
	 */
	private Vector<uint32_t> shopVipPrice = new Vector<uint32_t>();

	/**
	 * 是否支持购物车，1：支持，0：不支持
	 */
	private int cartFlag = 0;

	/**
	 * 是否支持立即购买，1：支持，0：不支持
	 */
	private int buyNowFlag = 0;

	/**
	 * 商品评论总数
	 */
	private long commentCount = 0;

	/**
	 * 店铺评分
	 */
	private int shopMark = 0;
	/**
	 * 货品符合度
	 */
	private int goodDescriptionMatch;
	/**
	 * 收藏数
	 */
	private int storeNum;
	public int getStoreNum() {
		return storeNum;
	}

	public void setStoreNum(int storeNum) {
		this.storeNum = storeNum;
	}

	/**
	 * 服务质量
	 */
	private int attitudeOfService;
	public int getGoodDescriptionMatch() {
		return goodDescriptionMatch;
	}

	public void setGoodDescriptionMatch(int goodDescriptionMatch) {
		this.goodDescriptionMatch = goodDescriptionMatch;
	}

	public int getAttitudeOfService() {
		return attitudeOfService;
	}

	public void setAttitudeOfService(int attitudeOfService) {
		this.attitudeOfService = attitudeOfService;
	}

	public int getSpeedOfDelivery() {
		return speedOfDelivery;
	}

	public void setSpeedOfDelivery(int speedOfDelivery) {
		this.speedOfDelivery = speedOfDelivery;
	}

	/**
	 * 物流速度
	 */
    private int speedOfDelivery;
	/**
	 * 店铺类型，0：拍拍，1：网购
	 */
	private int shopType;
	
	/**
	 * 是否一口价商品，1：一口价，0：不是
	 */
	private int fixOrderFlag = 0;
	
	/**
	 * 大促
	 */
	private int promotion = 0;
	
	/**
	 * 是否参加促销标识
	 */
	private long marketStat;	
	
	/**
	 * 商品评价信息
	 */
	private EvalPo evalPo = new EvalPo();
	/**
	 * 店铺Logo
	 */
	private String shopLogo ;
	/**
	 * 店铺名称
	 */
	private String shopName ;

	public EvalPo getEvalPo() {
		return evalPo;
	}

	public void setEvalPo(EvalPo evalPo) {
		this.evalPo = evalPo;
	}	

	public int getPromotion() {
	    return promotion;
	}

	public void setPromotion(int promotion) {
	    this.promotion = promotion;
	}

	public int getBuyNowFlag() {
		return buyNowFlag;
	}

	public void setBuyNowFlag(int buyNowFlag) {
		this.buyNowFlag = buyNowFlag;
	}

	public int getCartFlag() {
		return cartFlag;
	}

	public void setCartFlag(int cartFlag) {
		this.cartFlag = cartFlag;
	}

	public Vector<uint32_t> getColorPrice() {
		return colorPrice;
	}

	public void setColorPrice(Vector<uint32_t> colorPrice) {
		this.colorPrice = colorPrice;
	}

	public long getActivityPrice() {
		return activityPrice;
	}

	public void setActivityPrice(long activityPrice) {
		this.activityPrice = activityPrice;
	}

	public Vector<uint32_t> getShopVipPrice() {
		return shopVipPrice;
	}

	public void setShopVipPrice(Vector<uint32_t> shopVipPrice) {
		this.shopVipPrice = shopVipPrice;
	}

	/*public int getV() {
		return v;
	}

	public void setV(int v) {
		this.v = v;
	}
*/
	public Vector<String> getVecImg() {
		return vecImg;
	}

	public void setVecImg(Vector<String> vecImg) {
		this.vecImg = vecImg;
	}

	public long getRedPrice() {
		return redPrice;
	}

	public void setRedPrice(long redPrice) {
		this.redPrice = redPrice;
	}

	public static void main(String[] args) {
		new ItemBO().toString();
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemLocalCode() {
		return itemLocalCode;
	}

	public void setItemLocalCode(String itemLocalCode) {
		this.itemLocalCode = itemLocalCode;
	}

	public ItemState getItemState() {
		return itemState;
	}

	public void setItemState(ItemState itemState) {
		this.itemState = itemState;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public long getFreightId() {
		return freightId;
	}

	public void setFreightId(long freightId) {
		this.freightId = freightId;
	}

	public long getCodFreightId() {
		return codFreightId;
	}

	public void setCodFreightId(long codFreightId) {
		this.codFreightId = codFreightId;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public long getStockCount() {
		return stockCount;
	}

	public void setStockCount(long stockCount) {
		this.stockCount = stockCount;
	}

	public long getMailPrice() {
		return mailPrice;
	}

	public void setMailPrice(long mailPrice) {
		this.mailPrice = mailPrice;
	}

	public long getExpressPrice() {
		return expressPrice;
	}

	public void setExpressPrice(long expressPrice) {
		this.expressPrice = expressPrice;
	}

	public long getEmsPrice() {
		return emsPrice;
	}

	public void setEmsPrice(long emsPrice) {
		this.emsPrice = emsPrice;
	}

	public String getPicLink() {
		return picLink;
	}
	

	public void setPicLink(String picLink) {
		this.picLink = picLink;
	}

	public List<String> getExtPicsLink() {
		return extPicsLink;
	}

	public void setExtPicsLink(List<String> extPicsLink) {
		this.extPicsLink = extPicsLink;
	}

	public long getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}

	public long getSellerPayFreight() {
		return sellerPayFreight;
	}

	public void setSellerPayFreight(long sellerPayFreight) {
		this.sellerPayFreight = sellerPayFreight;
	}

	public long getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(long buyLimit) {
		this.buyLimit = buyLimit;
	}

	public long getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}

	public long getTotalBuyNum() {
		return totalBuyNum;
	}

	public void setTotalBuyNum(long totalBuyNum) {
		this.totalBuyNum = totalBuyNum;
	}

	public long getBuyCount() {
		return buyCount;
	}

	public void setBuyCount(long buyCount) {
		this.buyCount = buyCount;
	}

	public long getTotalBuyCount() {
		return totalBuyCount;
	}

	public void setTotalBuyCount(long totalBuyCount) {
		this.totalBuyCount = totalBuyCount;
	}

	// public String getRegionInfo()
	// {
	// return regionInfo;
	// }
	//
	//
	// public void setRegionInfo(String regionInfo)
	// {
	// this.regionInfo = regionInfo;
	// }

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(long marketPrice) {
		this.marketPrice = marketPrice;
	}

	public long getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(long visitCount) {
		this.visitCount = visitCount;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}

	public List<ItemStockBo> getItemStockBo() {
		return itemStockBo;
	}

	public void setItemStockBo(List<ItemStockBo> itemStockBo) {
		this.itemStockBo = itemStockBo;
	}

	public Map<ItemExtend, String> getExtendInfo() {
		return extendInfo;
	}

	public void setExtendInfo(Map<ItemExtend, String> extendInfo) {
		this.extendInfo = extendInfo;
	}

	public List<ItemAttrBO> getParsedAttrList() {
		return parsedAttrList;
	}

	public void setParsedAttrList(List<ItemAttrBO> parsedAttrList) {
		this.parsedAttrList = parsedAttrList;
	}


	public long getItemProperty() {
		return itemProperty;
	}

	public void setItemProperty(long itemProperty) {
		this.itemProperty = itemProperty;
	}

	public String getRelatedItems() {
		return relatedItems;
	}

	public void setRelatedItems(String relatedItems) {
		this.relatedItems = relatedItems;
	}

	/*@Override
	public String toString() {
		return "ItemBO [buyCount=" + buyCount + ", buyLimit=" + buyLimit
				+ ", buyNum=" + buyNum + ", categoryId=" + categoryId
				+ ", cityId=" + cityId + ", classId=" + classId
				+ ", countryId=" + countryId + ", emsPrice=" + emsPrice
				+ ", expressPrice=" + expressPrice + ", extPicsLink="
				+ extPicsLink + ", extendInfo=" + extendInfo + ", freightId="
				+ freightId + ", codFreightId=" + codFreightId + ", itemCode="
				+ itemCode + ", itemLocalCode=" + itemLocalCode + ", itemName="
				+ itemName + ", itemPrice=" + itemPrice + ", itemStockBo="
				+ itemStockBo + ", mailPrice=" + mailPrice + ", marketPrice="
				+ marketPrice + ", parsedAttrList=" + parsedAttrList
				+ ", picLink=" + picLink + ", properties=" + properties
				+ ", provinceId=" + provinceId + ", relatedItems="
				+ relatedItems + ", sellerName=" + sellerName
				+ ", sellerPayFreight=" + sellerPayFreight + ", sellerUin="
				+ sellerUin + ", stockCount=" + stockCount + ", totalBuyCount="
				+ totalBuyCount + ", totalBuyNum=" + totalBuyNum
				+ ", visitCount=" + visitCount + ", commentCount="
				+ commentCount + ", shopMark=" + shopMark + "]";
	}*/

	
	public List<String> getOriginalExtPicsLink() {
		return originalExtPicsLink;
	}

	public void setOriginalExtPicsLink(List<String> originalExtPicsLink) {
		this.originalExtPicsLink = originalExtPicsLink;
	}

	public long getSoldTotalCount() {
		return soldTotalCount;
	}

	public List<Integer> getProperties() {
		return properties;
	}

	public void setProperties(List<Integer> properties) {
		this.properties = properties;
	}

	public void setSoldTotalCount(long soldTotalCount) {
		this.soldTotalCount = soldTotalCount;
	}

	public long getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(long commentCount) {
		this.commentCount = commentCount;
	}

	public int getShopMark() {
		return shopMark;
	}

	public void setShopMark(int shopMark) {
		this.shopMark = shopMark;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getFixOrderFlag() {
		return fixOrderFlag;
	}

	public void setFixOrderFlag(int fixOrderFlag) {
		this.fixOrderFlag = fixOrderFlag;
	}

	public long getMarketStat() {
		return marketStat;
	}

	public void setMarketStat(long marketStat) {
		this.marketStat = marketStat;
	}


	
	public String getActivityDes() {
		return activityDes;
	}

	public void setActivityDes(String activityDes) {
		this.activityDes = activityDes;
	}

	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getIsStore() {
		return isStore;
	}

	public void setIsStore(int isStore) {
		this.isStore = isStore;
	}

	public String getItemUrl() {
		return itemUrl;
	}

	public void setItemUrl(String itemUrl) {
		this.itemUrl = itemUrl;
	}
	
	public int getIsNotSoldComdy() {
		return isNotSoldComdy;
	}

	public void setIsNotSoldComdy(int isNotSoldComdy) {
		this.isNotSoldComdy = isNotSoldComdy;
	}
	

	public String getItemWeiDian() {
		return itemWeiDian;
	}

	public void setItemWeiDian(String itemWeiDian) {
		this.itemWeiDian = itemWeiDian;
	}

	public String getShopWeiDian() {
		return shopWeiDian;
	}

	public void setShopWeiDian(String shopWeiDian) {
		this.shopWeiDian = shopWeiDian;
	}

	public long getItemShopVipPrice() {
		return itemShopVipPrice;
	}

	public void setItemShopVipPrice(long itemShopVipPrice) {
		this.itemShopVipPrice = itemShopVipPrice;
	}

	public long getWeiShopPrice() {
		return weiShopPrice;
	}

	public void setWeiShopPrice(long weiShopPrice) {
		this.weiShopPrice = weiShopPrice;
	}

	public long getItemColorPrice() {
		return itemColorPrice;
	}

	public void setItemColorPrice(long itemColorPrice) {
		this.itemColorPrice = itemColorPrice;
	}

	public long getResultPrice() {
		return resultPrice;
	}

	public void setResultPrice(long resultPrice) {
		this.resultPrice = resultPrice;
	}

	public int getResultPriceType() {
		return resultPriceType;
	}

	public void setResultPriceType(int resultPriceType) {
		this.resultPriceType = resultPriceType;
	}

	public long getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public Long getLeafClassId() {
		return leafClassId;
	}

	public void setLeafClassId(Long leafClassId) {
		this.leafClassId = leafClassId;
	}

	public byte getGuaranteeRepair() {
		return guaranteeRepair;
	}

	public void setGuaranteeRepair(byte guaranteeRepair) {
		this.guaranteeRepair = guaranteeRepair;
	}
	
	
}