package com.qq.qqbuy.item.po;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class StockAttribute {
	public ItemStock stock;
	public String key = "";
	/**
	 * 具体的属性名
	 */
	public String attrName;
	/**
	 * 属性类别名称
	 */
	public String name;
	/**
	 * 下一级的名称，颜色、尺寸等
	 */
	public String nextName;
	/**当前属性商品的价格*/
	public long price;
	
	public  Vector<Integer> discountPrice = new Vector<Integer>();
	/**
	 * 库存
	 */
	public long stockCount = 0;
	/**第几层节点0为根节点*/
	public int step = 0;
	public int choosestep=0;
	/**父节点KEY*/
	public String parentKey = "";
	/**上一步选择的KEY*/
	public String preKey="";
	/**子结点列表*/
	public List<StockAttribute> lst = new ArrayList<StockAttribute>();
	private boolean isGetLeaf = true;
	/**叶子结点列表*/
	public List<StockAttribute> leafLst = new ArrayList<StockAttribute>();
	
	/**
	 * 是否根节点
	 * @return
	 */
	public boolean isRoot() {
		return step <= 0;
	}
	
	public boolean hasStock() {
		return stockCount > 0;
	}
	
	public boolean hasChild() {
		return lst.size() > 0;
	}
	
	/**
	 * 获取折扣价
	 * @param level
	 * @return
	 */
	public int getDiscoutPrice(int level){
		if(this.discountPrice.size() < level || level <= 0){
			return (int) this.price;
		}else{
			return discountPrice.get(level-1);
		}
	}
	
	/**
	 * 获取叶子结点个数
	 * @return
	 */
	public int getLeafCount() {
		if ( !isGetLeaf ) {
			initLeafList();
		}
		return leafLst.size();
	}
	
	/**
	 * 获取叶子结点列表
	 * @return
	 */
	public List<StockAttribute> getLeafList() {
		if ( !isGetLeaf ) {
			initLeafList();
		}
		return leafLst;
	}
	
	/**
         * 获取有库存的叶子结点个数
         * @return
         */
        public int getHasStockLeafCount() {
                if ( !isGetLeaf ) {
                        initLeafList();
                }
                int hasStockLeafCount = 0;
                for (StockAttribute sa : leafLst)
                {
                    if (sa.hasStock())
                    {
                        hasStockLeafCount ++;
                    }
                }
                return hasStockLeafCount;
        }
	
	private void initLeafList() {
		for (int i = 0; i < lst.size(); i++) {
			StockAttribute child = lst.get(i);
			dfsLeaf(child);
		}
		isGetLeaf = true;
	}
	
	private void dfsLeaf(StockAttribute child) {
		if ( child.hasChild() ) {
			List<StockAttribute> clst = child.lst;
			for (int i = 0; i < clst.size(); i++) {
				dfsLeaf(clst.get(i));
			}
		} else {
			leafLst.add(child);
		}
	}
	
	public String getNextAttrStr() {
		ArrayList<StockAttribute> hasLst = new ArrayList<StockAttribute>();
		String content = "";
		for (int i = 0; i < lst.size(); i++) {
			StockAttribute next = lst.get(i);
			if ( next.hasStock() ) {
				hasLst.add(next);
			}
		}
		
		if ( hasLst.size() > 0 ) {
			content += "(";
			for (int i = 0; i < hasLst.size() - 1; i++) {
				StockAttribute next = hasLst.get(i);
				content += next.attrName+"/";
			}
			StockAttribute next1 = hasLst.get(hasLst.size() - 1);
			content += next1.attrName+")";
		}
		
		return content;
	}
	
	/**完整名称路径如:红色 X*/
	public String getStockNameStr() {
		ArrayList<StockAttribute> lst = new ArrayList<StockAttribute>();
		StockAttribute sa = this;
		while ( sa != null && !sa.key.equals("r") ) {
			lst.add(sa);
			sa = stock.getStockMap().get(sa.parentKey);
		}
		
		String content = "";
		if ( lst.size() > 0 ) {
			for (int i = lst.size() -1 ; i >=1 ; i--) {
				sa = lst.get(i);
				content +=sa.attrName+"/";
			}
			sa = lst.get(0);
			content += sa.attrName;
		}
		return content;
	}
	
	/**完整属性路径如:颜色:红色|尺码:*/
	public String getStockAttrStr() {
		ArrayList<StockAttribute> lst = new ArrayList<StockAttribute>();
		StockAttribute sa = this;
		while ( sa != null && !sa.key.equals("r") ) {
			lst.add(sa);
			sa = stock.getStockMap().get(sa.parentKey);
		}
		
		String content = "";
		if ( lst.size() > 0 ) {
			for (int i = lst.size() -1 ; i >=1 ; i--) {
				sa = lst.get(i);
				content +=sa.name+":"+sa.attrName+"|";
			}
			sa = lst.get(0);
			content += sa.name+":"+sa.attrName;
		}
		return content;
	}
	
	public String getStepAttrList() {
		ArrayList<StockAttribute> lst = new ArrayList<StockAttribute>();
		StockAttribute sa = this;
		while ( sa != null && !sa.key.equals("r") ) {
			lst.add(sa);
			sa = stock.getStockMap().get(sa.parentKey);
		}
		
		String content = "";
		int j = 1;
		if ( lst.size() > 0 ) {
			for (int i = lst.size() -1 ; i >=0 ; i--) {
				sa = lst.get(i);
				content += j+"."+sa.name+":"+sa.attrName+"<br/>";
				j++;
			}
		}
		return content;
	}
	
	public Vector<Integer> getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Vector<Integer> discountPrice) {
		this.discountPrice = discountPrice;
	}

	@Override
	public String toString() {
		return "StockAttribute [attrName=" + attrName + ", choosestep="
				+ choosestep + ", discountPrice=" + discountPrice
				+ ", isGetLeaf=" + isGetLeaf + ", key=" + key + ", leafLst="
				+ leafLst + ", lst=" + lst + ", name=" + name + ", nextName="
				+ nextName + ", parentKey=" + parentKey + ", preKey=" + preKey
				+ ", price=" + price + ", step=" + step + ", stock=" + stock
				+ ", stockCount=" + stockCount + "]";
	}


}
