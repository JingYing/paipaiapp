package com.qq.qqbuy.item.po;

public class ItemInfoWithPrice {
	/**
	 * 手机拍拍商品信息
	 * 
	 * 版本 >= 0
	 */
	private MItem4J mitem = new MItem4J();

	/**
	 * 拍拍商品信息
	 * 
	 * 版本 >= 0
	 */
	private PPItem4J ppItem = new PPItem4J();

	public MItem4J getMitem() {
		return mitem;
	}

	public void setMitem(MItem4J mitem) {
		this.mitem = mitem;
	}

	public PPItem4J getPpItem() {
		return ppItem;
	}

	public void setPpItem(PPItem4J ppItem) {
		this.ppItem = ppItem;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemInfoWithPrice [mitem=").append(mitem).append(
				", ppItem=").append(ppItem).append("]");
		return builder.toString();
	}



}
