package com.qq.qqbuy.item.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qq.qqbuy.bi.biz.BiBiz;
import com.qq.qqbuy.common.ChannelParam;
import com.qq.qqbuy.common.JsonOutput;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.exception.BusinessErrorType;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.biz.ItemBiz;
import com.qq.qqbuy.item.biz.PPItemBiz;
import com.qq.qqbuy.item.po.ItemBO;
import com.qq.qqbuy.item.po.ItemBOBasic;
import com.qq.qqbuy.item.po.ItemBOExtInfo;
import com.qq.qqbuy.item.po.ItemBOPrice;
import com.qq.qqbuy.item.po.ItemJsonInfo;
import com.qq.qqbuy.item.po.ItemStockBo;
import com.qq.qqbuy.item.po.eval.EvalPo;
import com.qq.qqbuy.item.util.ItemUtil;
import com.qq.qqbuy.thirdparty.idl.item.price.ItemPriceClient;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.CommodityPrice;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.GetCommodityListPriceResp;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.ScenePrice;
import com.qq.qqbuy.thirdparty.idl.item.price.protocol.SkuPrice;

public class ApiItemAction extends PaipaiApiBaseAction {

	public String ic, dap;

	public boolean needParseAttr;

	public boolean needExtendInfo;

	public PPItemBiz ppitemBiz;
	public ItemBiz itemBiz;
	private BiBiz biBiz;
	public String itemDetail ;
	public String currentActive ;
	public String datastr;

	/** 评论 **/
	private int pn = 1;// 页码
	private int ps;// 页大小
	private int his;// 是否需要历史信息
	private int lev;// 评论等级
	private int rep;// 是否需要回复
	private long suin;// 卖家
	private int ver = 0;
	// 来源
	private int s = 0;

	// 网络类型
	private String nt = "";
	InputStream categoryStream;		//商品类目文件
	long contentLength;		//文件大小

	
	//以下为看了又看的参数
	private int type = 0;//请求类型，0:商品类，1：素材，2：店铺, 3：固定商品池混排,4: 拍便宜,5:店铺内推荐
	private int num = 12;//请求结果的个数默认12
	private String itemId;//商品的ID如果type为0时这个不能为空，必填。
	private String uin;//QQ如果为空则取尝试从cookie中取
	/**
	 * 该字段为保留字段，可以设置一些特定的显示 格式如下：k:v,k:v,...
	 *其中k为参数名,v为参数值，个数不限。目前可用参数如下：
	 *	pageno: 当前query请求页号, 从1开始
	 *	pagesize: 当前query每页商品个数
	 *	itemnum: 店铺推荐时需要该店铺商品的个数，默认为0 
	 *	shopid:店铺内推荐的店铺id
	 *	itemid:店铺内推荐的商品id
	 *	needwdprice:是否需要微店价格，仅当type=0,3,5生效
	 *	1需要微店价格,其他不需要
	 */
	private String Reserved;//该参数暂时不用。目前业务暂时用不到。
	
	public int getS() {
		return s;
	}

	public void setS(int s) {
		this.s = s;
	}

	public String getNt() {
		return nt;
	}

	public void setNt(String nt) {
		this.nt = nt;
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public String getItem() {
		try {
			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}
			
			Log.run.info("start checkLogin sid:" + getSid() + " ,uk:" + getUk() + " ,sk:" + getSk() + " ,appToken:" + getAppToken() + ",mk," + getMk());

			ItemBO item = null;			
			/**
			 * 获得商品基本信息
			 */
			item = ppitemBiz.fetchItemInfo(ic, true);
			/**
			 * 获得商品图文详情
			 */
			String detail = ppitemBiz.getItemDetail(ic);
			itemDetail = detail.replaceAll("\r", "").replaceAll("\n", "").replaceAll("\\\\","\\\\\\\\").replaceAll("\"", "\\\\\"")
					.replaceAll("/", "\\\\/");
			/**
			 * 获得商品评论
			 */
			EvalPo evalPo = itemBiz.getItemEval(ic, 0, 1, 10, 0, 0, 3);
			item.setEvalPo(evalPo) ;
			/**
			 * 获得商品店铺信息
			 */
			ppitemBiz.fetchCmtShopInfo4Item(ic, item, this.getMk());	
			
			Long sellerUin = item.getSellerUin() ;
			
			/**
			 * 登陆后价格处理
			 * 自己不能买自己的商品
			 */
			if (checkLoginAndGetSkey()) {
				this.ppitemBiz.handlePrice(item, sellerUin, getWid(), getSk()) ;
			}

			this.ppitemBiz.computePrice(item) ;
			
			this.ppitemBiz.wxPrice(item,sellerUin,ic);
			
			/**
			 * 该商品是否被收藏
			 * 这个接口待开发。暂时设为未收藏
			 */
			//item.setIsStore(0) ;
				// 鉴权
		    if (!checkLoginAndGetSkey()) {
					item.setIsStore(0) ;
				}else{
					//获取商品是否被收藏
					String pprdP = null;
					try {
						pprdP = new ChannelParam(getChannel()).getPprdP();
					} catch (Exception e) {
						Log.run.error("addFavItems#getChannel:" + e.getMessage());
					}
					boolean isFavorite = this.ppitemBiz.getIsFavorite(ic,getQq(), getHttpHeadIp(), getSk(), pprdP);
					Log.run.info("-0--------isFavorite--------"+isFavorite);
					item.setIsStore(isFavorite==true?1:0);
				}

			/**
			 * 获得商品对应的店铺的活动信息
			 */
			currentActive = ppitemBiz.getCurrentActive(sellerUin) ;
			if (item != null) {				
				// 非wifi情况下返回300*300的图片
				if (nt != null && !"WIFI".equalsIgnoreCase(nt)) {
					if (item.getPicLink() != null)
						item.setPicLink(item.getPicLink().replace("400x400", "300x300"));
					List<String> pics = item.getExtPicsLink();
					if (pics != null && pics.size() > 0) {
						List<String> pics2 = new ArrayList<String>();
						for (String pic : pics) {
							if (pic != null)
								pics2.add(pic.replace("400x400", "300x300"));
						}
						if (!pics2.isEmpty()) {
							item.setExtPicsLink(pics2);
						}
					}
				}	
				datastr = obj2Json(item);
			}
			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "ic=" + ic + ",needParseAttr=" + needParseAttr + ",needExtendInfo"
					+ needExtendInfo);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic + ",needParseAttr=" + needParseAttr + ",needExtendInfo"
					+ needExtendInfo);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}
	
	public String getItemExtInfo() {
		try {
			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}
			ItemBOExtInfo item = null;			
			/**
			 * 获得商品基本信息
			 */
			item = ppitemBiz.getItemExtInfo(ic);
			/**
			 * 获得商品图文详情
			 */
			String detail = ppitemBiz.getItemDetail(ic);
			itemDetail = detail.replaceAll("\r", "").replaceAll("\n", "").replaceAll("\\\\","\\\\\\\\").replaceAll("\"", "\\\\\"")
					.replaceAll("/", "\\\\/");
			/**
			 * 获得商品评论
			 */
			EvalPo evalPo = itemBiz.getItemEvalFroBest(ic, 0, 1, 100, 0, 0, 3);
			item.setEvalPo(evalPo) ;
			/**
			 * 获得商品店铺信息
			 */
			ppitemBiz.fetchCmtShopInfo4Item(ic, item, this.getMk());	
			
			long sellerUin = ItemUtil.getSellerUin(ic);				
			/**
			 * 该商品是否被收藏
			 */
			//item.setIsStore(0) ;
				// 鉴权
		    if (!checkLoginAndGetSkey()) {
					item.setIsStore(0) ;
				}else{
					//获取商品是否被收藏
					String pprdP = null;
					try {
						pprdP = new ChannelParam(getChannel()).getPprdP();
					} catch (Exception e) {
						Log.run.error("addFavItems#getChannel:" + e.getMessage());
					}
					boolean isFavorite = this.ppitemBiz.getIsFavorite(ic,getQq(), getHttpHeadIp(), getSk(), pprdP);
					item.setIsStore(isFavorite==true?1:0);
				}
			
			/**
			 * 获得商品对应的店铺的活动信息
			 */
			currentActive = ppitemBiz.getCurrentActive(sellerUin) ;
			if (item != null) {	
				datastr = obj2Json(item);
			}
			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "ic=" + ic );
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic );
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}
	
	
	/**
	 * 判断商品是否被收藏
	 * @return
	 */
	public String checkItemIsStore() {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtil.isBlank(ic)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
		    	out.setMsg("参数校验不合法");
			}
			
			// 鉴权
		    if (!checkLoginAndGetSkey()) {
		    	out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
		    	out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			}else{
				//获取商品是否被收藏
				String pprdP = null;
				try {
					pprdP = new ChannelParam(getChannel()).getPprdP();
				} catch (Exception e) {
					Log.run.error("addFavItems#checkItemIsStore:" + e.getMessage());
				}
				boolean isStore = this.ppitemBiz.getIsFavorite(ic,getQq(), getHttpHeadIp(), getSk(), pprdP);
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("isStore", isStore);
				out.setData(jsonObject);
			}
		} catch (BusinessException e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
			e.printStackTrace() ;
		} catch (Throwable e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
			e.printStackTrace() ;
		}  
		
		return doPrint(out.toJsonStr());
	}
	
	/**
	 * 判断商品是否被收藏
	 * @return
	 */
	public String checkDulItemIsStore() {
		JsonOutput out = new JsonOutput();
		try {
			if (StringUtil.isBlank(ic)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
		    	out.setMsg("参数校验不合法");
			}
			
			// 鉴权
		    if (!checkLoginAndGetSkey()) {
		    	out.setErrCode(ErrConstant.ERRCODE_CHECKLOGIN_FAIL + "");
		    	out.setMsg(ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG);
			}else{
				//获取商品是否被收藏
				String pprdP = null;
				try {
					pprdP = new ChannelParam(getChannel()).getPprdP();
				} catch (Exception e) {
					Log.run.error("addFavItems#checkDulItemIsStore:" + e.getMessage(), e);
				}
				String[] split = ic.split(",");
				JsonObject jsonObject = new JsonObject();
				for (String sp_ic : split) {
					boolean isStore = this.ppitemBiz.getIsFavorite(sp_ic,getQq(), getHttpHeadIp(), getSk(), pprdP);
					jsonObject.addProperty(sp_ic, isStore);
				}
				
				out.setData(jsonObject);
			}
		} catch (BusinessException e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
	    	e.printStackTrace();
		} catch (Throwable e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
	    	e.printStackTrace();
		}  
		
		return doPrint(out.toJsonStr());
	}
	
	public String getItemPrice() {
		try {
			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}
			ItemBOPrice item = null;			
			/**
			 * 获得商品基本信息
			 */
			item = ppitemBiz.getItemPrice(ic);			
			Long sellerUin = item.getSellerUin() ;			
			/**
			 * 登陆后价格处理
			 * 自己不能买自己的商品
			 */
			if (checkLoginAndGetSkey()) {
				this.ppitemBiz.handlePrice(item, sellerUin, getWid(), getSk()) ;
			}			
			this.ppitemBiz.computePrice(item) ;			
			this.ppitemBiz.wxPrice(item,sellerUin,ic);	
			datastr = obj2Json(item);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}
	
	public String getItemPrice2() {
		try {
			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}
			ItemBOPrice item = null;	
			item = ppitemBiz.getItemPrice(ic);			
			Long sellerUin = item.getSellerUin() ;	
			
			//设置商品默认价格
			item.setResultPrice(item.getItemPrice()) ;
			item.setResultPriceType(0) ;
			
			if (item.getActivityPrice() != 0 && item.getActivityPrice() <= item.getResultPrice()) {
				item.setResultPrice(item.getActivityPrice()) ;
				item.setResultPriceType(1) ;
			}
			
			/**
			 * 登陆后价格处理
			 * 自己不能买自己的商品
			 */
			long userUin = 0;			
			if (checkLoginAndGetSkey()) {
				userUin = getWid();
				if (userUin == sellerUin) {
					item.setIsNotSoldComdy(1) ;
					item.setStockCount(0) ;
					item.setCartFlag(0) ;
					List<ItemStockBo> itemStockBos = item.getItemStockBo() ;
					for (int i = 0 ; i < itemStockBos.size(); i++) {
						itemStockBos.get(i).setStockCount(0) ;
					}
				}
			}
			//最优价格计算(已SKU为KEY，放MAP中)
			Vector<String> idList = new Vector<String>();
			idList.add(ic) ;
			GetCommodityListPriceResp resp = ItemPriceClient.getItemPrice(sceneId, 1, userUin, sellerUin, idList);
			Map<String,ScenePrice> priceMap = new HashMap<String, ScenePrice>() ;
			for(CommodityPrice commodityPrice: resp.getCommodityPriceList()) {
				for (SkuPrice skuPrice: commodityPrice.getSkuPriceList()) {
					priceMap.put(skuPrice.getDesc(), skuPrice.getRecommendedScenePrice()) ;
				}
			}
			
			for (int i = 0 ; i < item.getItemStockBo().size(); i++) { 
				ItemStockBo itemStockBo = item.getItemStockBo().get(i) ;
				//库存默认价格
				itemStockBo.setResultPrice(itemStockBo.getStockPrice()) ;
				itemStockBo.setResultPriceType(0) ;
				if (itemStockBo.getActivityPrice() != 0 && itemStockBo.getActivityPrice() <= itemStockBo.getStockPrice()) {
					itemStockBo.setResultPrice(itemStockBo.getActivityPrice()) ;
					itemStockBo.setResultPriceType(1) ;
				}
				//取推荐价格
				String stockAttr = itemStockBo.getStockAttr() ;
				ScenePrice scenePrice = priceMap.get(stockAttr) ;				
				if (scenePrice != null ) {				
					itemStockBo.setResultPrice(scenePrice.getPrice());
					itemStockBo.setResultPriceType((int)scenePrice.getId()) ;
				}
				//商品价格默认取SKU最便宜价格
				if (itemStockBo.getResultPrice() <= item.getResultPrice()) {
					item.setResultPrice(itemStockBo.getResultPrice()) ;
					item.setResultPriceType(itemStockBo.getResultPriceType()) ;
				}
			}
			datastr = obj2Json(item);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}
	
	/**
	 * 获得商品基本信息
	 * @return
	 */	
	public String getItemBasic() {
		try {
			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}			
			ItemBOBasic item = null;			
			/**
			 * 获得商品基本信息
			 */
			item = ppitemBiz.getItemBasic(ic) ;
			if (item != null) {				
				// 非wifi情况下返回300*300的图片，将主图及轮播图替换为300*300的图片
				if (nt != null && !"WIFI".equalsIgnoreCase(nt)) {
					if (item.getPicLink() != null){
						item.setPicLink(item.getPicLink().replace("400x400", "300x300"));
					}
					List<String> pics = item.getExtPicsLink();
					if (pics != null && pics.size() > 0) {
						List<String> pics2 = new ArrayList<String>();
						for (String pic : pics) {
							if (pic != null)
								pics2.add(pic.replace("400x400", "300x300"));
						}
						if (!pics2.isEmpty()) {
							item.setExtPicsLink(pics2);
						}
					}
				}	
				datastr = obj2Json(item);
			}
			
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg4BExp(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		} catch (Throwable e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			e.printStackTrace() ;
			return RST_API_FAILURE;
		}  
	}
	

	/**
	 * 商品评价页
	 * 
	 * @return
	 */
	public String getCmt() {
		try {

			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}

			EvalPo evalPo = itemBiz.getItemEval(ic, suin, pn, 10, 0, 0, 0);
			if (evalPo != null) {
				datastr = obj2Json(evalPo);
			}
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			return RST_API_FAILURE;
		} catch (Exception e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			return RST_API_FAILURE;
		}  
	}

	public String getDetail() {
		try {

			if (StringUtil.isEmpty(ic)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "ic不能为空");
			}

			String detail = ppitemBiz.getItemDetail(ic);
			datastr = detail.replaceAll("\r", "").replaceAll("\n", "").replaceAll("\\\\","\\\\\\\\").replaceAll("\"", "\\\\\"")
					.replaceAll("/", "\\\\/");
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			return RST_API_FAILURE;
		} catch (Exception e) {
			setErrcodeAndMsg4Throwable(e, "ic=" + ic);
			return RST_API_FAILURE;
		}  
	}
	
	/**
	 * 获取全量商品的目录,以json文件方式提供
	 * @return
	 * @throws Exception
	 */
	public String category() 	{
		try {
			URL url = Thread.currentThread().getContextClassLoader().getResource("res/category.json");
			File f = new File(url.toURI());
			contentLength = f.length();
			categoryStream = new FileInputStream(f);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @throws UnsupportedEncodingException 
	 * 
	
	 * @Title: getRecoItem 
	
	 * @Description:获取看了又看json
	
	 * @param @return    设定文件 
	
	 * @return String    返回类型 
	
	 * @throws
	 */
	public String getRecoItem() throws UnsupportedEncodingException {
		// 生成一个输入的json对象
		JsonOutput out = new JsonOutput();
			//验证参数
			  if(type==0&&(itemId==null||"".equals(itemId))){
				  out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER+"");
				  out.setMsg("推荐类型为0时，商品ID不能为空！");
				  return doPrint(out.toJsonStr());
			  }
			// 请求的cgi地址
		String cgiurl = "http://bi.paipai.com/json/show?sf=1416471665&nojsonp=1&hid=3002&mid=1006&low=0"
				+ "&uin="
				+ (uin == null ? "" : uin)
				+ "&type="
				+ type
				+ "&q=9527|" + num + "|" + itemId + "|";
			String recoresult = "";
			try {
				recoresult = HttpUtil
						.get(cgiurl, 10000, 10000, "gb2312", true);
			} catch (Exception e) {
				// 如果有异常。返回调用失败
				out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
				out.setMsg("看了又看商品推荐接口调用失败！");
				return doPrint(out.toJsonStr());
			}
			//将返回的数据整理成统一的输出格式
			if(recoresult!=null&&!"".equals(recoresult)){
				JsonParser recoJsonParser = new JsonParser();
				JsonElement recoJsonElement = recoJsonParser.parse(recoresult);
				//将数据取出。并拼接出html
				JsonElement dataJsonElement = recoJsonElement.getAsJsonObject().get("data").getAsJsonObject().get("9527");
				//构造一个显示对象。方便显示操作
				Log.run.info(dataJsonElement.getAsJsonArray().size());
				ItemJsonInfo itemj = null;
				//google的gson对象
				Gson gson = new Gson(); 
				StringBuffer sb  = new StringBuffer();
				sb.append("<div class=\"kk_text\">");
				sb.append("<hr> <span class=\"kk_text_span\">");
			    sb.append(" <img src=\"tb.png\"/>看了又看");   
			    sb.append("</span></hr>");       
			    sb.append("</div>");        
			    sb.append("<div class=\"product_div\">");    
			    sb.append("<ul class=\"product_list\">");
				for(int i=0;i<dataJsonElement.getAsJsonArray().size();i++){
					JsonElement jt = dataJsonElement.getAsJsonArray().get(i);
					//取出每一条数据
					String json = jt.toString();
					//准备输出对象
					itemj = new ItemJsonInfo();
					//将json转成itmej对象
					itemj= gson.fromJson(json, ItemJsonInfo.class);
					if(i%2==0){
				    sb.append("<li>");
				    sb.append("<a class=\"product_list_a_left\" >");
				    sb.append("<div class=\"product_list_commodity\">");
				    sb.append(" <img  class=\"product_list_commodity_img\" src=\""+itemj.getStrItemPic()+"\">");
				    sb.append("<div class=\"product_list_commodity_bottom\">");
				    sb.append("<div class=\"product_title\" >");
				    sb.append(itemj.getStrTitle());
				    sb.append("</div>");
				    sb.append("<div class=\"product_price\">");
				    sb.append("<div class=\"product_price_red\">");
				    sb.append("￥");
				    sb.append(paipaiPriceFormat(Long.valueOf(itemj.getDwPrice()==null?"0":itemj.getDwPrice())));
				    sb.append("</div>");
				    sb.append("<div class=\"product_price_num\">");
				    sb.append("月销量"+itemj.getDwOrderNum()+"件");
				    sb.append("</div>");
				    sb.append("</div>");
				    sb.append("</div>");
				    sb.append("</div>");
				    sb.append("</a>");
				    sb.append("</li>");
					}else {
					sb.append("  <li><a class=\"product_list_a_right\" >");	
					sb.append(" <div class=\"product_list_commodity\">");
					sb.append(" <img  class=\"product_list_commodity_img\" src=\""+itemj.getStrItemPic()+"\">");
					sb.append("<div class=\"product_list_commodity_bottom\">");
					sb.append("<div class=\"product_title\" >");
					sb.append(itemj.getStrTitle());
					sb.append("</div>");
					sb.append(" <div class=\"product_price\">");
					sb.append("<div class=\"product_price_red\">￥"+paipaiPriceFormat(Long.valueOf(itemj.getDwPrice()==null?"0":itemj.getDwPrice()))+"</div>");
					sb.append("<div class=\"product_price_num\">月销量"+itemj.getDwOrderNum()+"件</div>");
					sb.append("</div>");
					sb.append("</div>");
					sb.append("</div>");
					sb.append("</a></li>");
					}
				}
				sb.append("</ul></div>");
				//测试接口
				recoJsonElement.getAsJsonObject().addProperty("recoItem", new String(sb.toString().getBytes(),"utf-8"));
				out.setData(recoJsonElement.getAsJsonObject().get("recoItem"));
				out.setErrCode(recoJsonElement.getAsJsonObject().get("retCode").getAsString());
				out.setMsg(recoJsonElement.getAsJsonObject().get("errMsg").getAsString());
			}else {
				out.setMsg("没有可推荐的数据！");
				out.setErrCode("0");
			}
			return doPrint(out.toJsonStr());
	}
	/***********************
	 * app专享商品列表接口相关
	 **********************/
	/**
	 * app专享itemIds
	 */
	private String itemIds;
	/**
	 * 接口返回结果
	 */
	private String itemList;
	/**
	 * app专享商品列表接口
	 * 根据ppms配置的itemIds获取app端需要的商品信息
	 */
	public String exclusive(){
		try {
			if (StringUtil.isEmpty(itemIds)) {
                throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "itemIds不能为空");
            }
			boolean flag = checkLoginAndGetSkey();
			String[] ids = itemIds.split(",");
			JsonArray items = new JsonArray();
			for (String id : ids){
				try {
					JsonObject item = ppitemBiz.exclusiveItemAndCache(id,flag,getWid(),getSk());
					items.add(item);
				} catch (Exception e) {
					Log.run.error("获取app专享商品信息出错",e);
				}
			}
			Log.run.info("ApiItemAction#exclusive:" + items);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg(e.getErrCode(),e.getErrMsg());
			return RST_API_FAILURE;
		}
	}

	/**
	 * 根据ItemIds批量获取商品基本信息
	 * @return
	 */
	public String batch(){
		try {
			if (StringUtil.isEmpty(itemIds)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "itemIds不能为空");
			}
			String[] ids = itemIds.split(",");
			JsonArray items = new JsonArray();
			for (String id : ids){
				try {
					JsonObject item = ppitemBiz.getItemBaseInfoByItemCode(id);
					if(null != item && !item.isJsonNull()){
						items.add(item);
					}
				} catch (Exception e) {
					Log.run.error("获取商品基本信息出错",e);
				}
			}
			Log.run.info("ApiItemAction#batch:" + items);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg(e.getErrCode(),e.getErrMsg());
			return RST_API_FAILURE;
		}
	}
	
	
	/**
	 * 根据ItemIds批量获取商品基本信息
	 * 计算微店价，速度比较慢，不建议使用
	 * @return
	 */
	@Deprecated
	public String batchSlow(){
		try {
			if (StringUtil.isEmpty(itemIds)) {
				throw BusinessException.createInstance(BusinessErrorType.PARAM_ERROR, "itemIds不能为空");
			}
			boolean flag = checkLoginAndGetSkey();
			String[] ids = itemIds.split(",");
			JsonArray items = new JsonArray();
			for (String id : ids){
				try {
					JsonObject item = ppitemBiz.getItemBaseInfoByItemCodeSlow(id, flag, getWid(),getSk());
					if(null != item && !item.isJsonNull()){
						items.add(item);
					}
				} catch (Exception e) {
					Log.run.error("获取商品基本信息出错",e);
				}
			}
			Log.run.info("ApiItemAction#batch:" + items);
			itemList = new GsonBuilder().serializeNulls().create().toJson(items);
			return RST_SUCCESS;
		} catch (BusinessException e) {
			setErrCodeAndMsg(e.getErrCode(),e.getErrMsg());
			return RST_API_FAILURE;
		} catch (Exception e) {
			Log.run.error(e.getMessage(), e);
			return RST_API_FAILURE;
		}
	}

	/**
	 * 商品价格场景
	 */
	private long sceneId = 0;
	/**
	 * userType：核心参数，代表用户类型，
	 *     取值如下：1代表买家，2代表卖家，3代表平台用户，
	 *     当前系统只支持1，其他取值会返回系统错误，必填
	 */
	private long userType = 1;
	/**
	 * 卖家qq
	 */
	private long sellerUin;
	/**
	 *详新的价格IDL使用
	 * @return
	 */
	public String priceList(){

		JsonOutput out = new JsonOutput();
		try {
			if (StringUtil.isBlank(itemIds)) {
				out.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER + "");
				out.setMsg("参数校验不合法");
			}
			long userUin = 0;
			// 鉴权
			if (checkLoginAndGetSkey()) {
				userUin = getWid();
			}
			String[] ids = itemIds.split(",");
			Vector<String> idList = new Vector<String>(Arrays.asList(ids));
			GetCommodityListPriceResp resp = ItemPriceClient.getItemPrice(sceneId, userType, userUin, sellerUin, idList);
			JsonParser parser = new JsonParser();
			JsonElement data = parser.parse(obj2Json(resp));
			out.setData(data);
		} catch (BusinessException e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
			e.printStackTrace();
		} catch (Throwable e) {
			out.setErrCode(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP + "");
			e.printStackTrace();
		}

		return doPrint(out.toJsonStr());
	}

	/**
     * 拍拍价格转换（后台提供的都是整数，如：1.01元的物品，返回的价格是101）
     *
     * @param num
     * @return
     */
    public static String paipaiPriceFormat(long num) {
        double doubleNum = (num * 1.0) / 100;
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(doubleNum);
    }
	public InputStream getCategoryStream() {
		return categoryStream;
	}

	public void setCategoryStream(InputStream categoryStream) {
		this.categoryStream = categoryStream;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}

	public String getIc() {
		return ic;
	}

	public void setIc(String ic) {
		this.ic = ic;
	}

	public boolean isNeedParseAttr() {
		return needParseAttr;
	}

	public void setNeedParseAttr(boolean needParseAttr) {
		this.needParseAttr = needParseAttr;
	}

	public boolean isNeedExtendInfo() {
		return needExtendInfo;
	}

	public void setNeedExtendInfo(boolean needExtendInfo) {
		this.needExtendInfo = needExtendInfo;
	}

	public PPItemBiz getPpitemBiz() {
		return ppitemBiz;
	}

	public void setPpitemBiz(PPItemBiz ppitemBiz) {
		this.ppitemBiz = ppitemBiz;
	}

	public ItemBiz getItemBiz() {
		return itemBiz;
	}

	public void setItemBiz(ItemBiz itemBiz) {
		this.itemBiz = itemBiz;
	}

	public int getPn() {
		return pn;
	}

	public void setPn(int pn) {
		this.pn = pn;
	}

	public int getPs() {
		return ps;
	}

	public void setPs(int ps) {
		this.ps = ps;
	}

	public int getHis() {
		return his;
	}

	public void setHis(int his) {
		this.his = his;
	}

	public int getLev() {
		return lev;
	}

	public void setLev(int lev) {
		this.lev = lev;
	}

	public int getRep() {
		return rep;
	}

	public void setRep(int rep) {
		this.rep = rep;
	}

	public long getSuin() {
		return suin;
	}

	public void setSuin(long suin) {
		this.suin = suin;
	}

	public String getDatastr() {
		return datastr;
	}

	public void setDatastr(String datastr) {
		this.datastr = datastr;
	}
	
	public String getCurrentActive() {
		return currentActive;
	}

	public void setCurrentActive(String currentActive) {
		this.currentActive = currentActive;
	}

	public String getItemDetail() {
		return itemDetail;
	}

	public void setItemDetail(String itemDetail) {
		this.itemDetail = itemDetail;
	}
	
	public void setBiBiz(BiBiz biBiz) {
		this.biBiz = biBiz;
	}
	
	public String getDap() {
		return dap;
	}

	public void setDap(String dap) {
		this.dap = dap;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getReserved() {
		return Reserved;
	}

	public void setReserved(String reserved) {
		Reserved = reserved;
	}

	public String getUin() {
		return uin;
	}

	public void setUin(String uin) {
		this.uin = uin;
	}

	public String getItemList() {
		return itemList;
	}

	public void setItemList(String itemList) {
		this.itemList = itemList;
	}

	public String getItemIds() {
		return itemIds;
	}

	public void setItemIds(String itemIds) {
		this.itemIds = itemIds;
	}

	public static void main(String[] args) {
		ApiItemAction api = new ApiItemAction() ;
		ItemBO item = new ItemBO();
		System.out.println(api.obj2Json(item));
		System.out.println(Long.MAX_VALUE);
		System.setProperty("user.timezone", "Asia/Shanghai");  
		      TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");  
		      TimeZone.setDefault(tz);  
		      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		      String times = format.format(new Date(1418101200*1000L));  
		      System.out.print("日期格式---->" + times);  
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}

	public long getUserType() {
		return userType;
	}

	public void setUserType(long userType) {
		this.userType = userType;
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}
}
