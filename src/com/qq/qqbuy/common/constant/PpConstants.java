package com.qq.qqbuy.common.constant;

/**
 * 
 * @ClassName: PpConstants
 * @Description: 拍拍接口调用的一些常量
 * @author wendyhu
 * @date 2013-4-9 上午10:42:24
 */
public class PpConstants
{

    /**
     * 拍拍分配给移动电商这边的调用接口ID
     */
    public static final long PP_BUSINESSID = 10008L;
    /**
     * 拍拍分配给移动电商这边调用接口的来源
     */
    public static final String PP_SOURCE = "o2o";
}
