package com.qq.qqbuy.common;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.interceptor.ExceptionMappingInterceptor;
import com.qq.qqbuy.bi.web.BiAction;
import com.qq.qqbuy.common.client.cache.CacheClient;
import com.qq.qqbuy.common.client.cache.CacheKey;
import com.qq.qqbuy.common.client.log.AbstractLogItem;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.filter.TimerAndTokenInterceptor;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.Util;
import com.qq.qqbuy.gchoice.biz.FirstPageManager;

public class LogServelt extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public LogServelt() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * 项目启动时执行，用于每天凌晨触发日志，以避免没有日志产生时昨天的日志文件名不修改的BUG
	 * @author liubenlong3
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
//		triggerLog();
		if(EnvManager.isNotIdc() || "172.25.73.14".equals(Util.getLocalIp()))	
			return;
		
		ServletContext servletContext = this.getServletContext();  
        WebApplicationContext context =   
                WebApplicationContextUtils.getWebApplicationContext(servletContext);  
        CacheClient cacheClient = (CacheClient)context.getBean("cacheClient");
        Timer timer = new Timer();
        
        miaosha(cacheClient, timer);//每隔两分钟执行一次
        paipianyi(cacheClient, timer);//每隔两分钟执行一次
        paipianyi317(cacheClient, timer);//每隔两分钟执行一次
        shangou(cacheClient, timer);//从第二天开始10点执行 ，执行5次
        shangou317(cacheClient, timer);//从第二天开始10点执行 ，执行5次
        
//        new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				FirstPageManager.shangou(redisTemplate);//第一天执行
//			}
//		}).start();

        
	}

	
	/**
	 * 新首页闪购  每天10点更新
	 */
	private void miaosha(final CacheClient cacheClient, Timer timer) {
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if(isdo("miaosha", cacheClient)){
					FirstPageManager.miaosha(cacheClient);
					cacheClient.del("miaosha_lock");
				}						
			}
		}, new Date(), 2 * 60 * 1000L);
	}
	
	/**
	 * paipianyi
	 */
	private void paipianyi(final CacheClient cacheClient, Timer timer) {
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if(isdo("paipianyi", cacheClient)){
					FirstPageManager.paipianyi(cacheClient);
					cacheClient.del("paipianyi_lock");
				}
			}
		}, new Date(), 2 * 60 * 1000L);
	}
	
	/**
	 * paipianyi
	 * 317及以上版本
	 */
	private void paipianyi317(final CacheClient cacheClient, Timer timer) {
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if(isdo("paipianyi317", cacheClient)){
					FirstPageManager.paipianyi317(cacheClient);
					cacheClient.del("paipianyi317_lock");
				}
			}
		}, new Date(), 2 * 60 * 1000L);
	}
	
	/**
	 * shangou
	 */
	private void shangou(final CacheClient cacheClient, Timer timer) {
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if(isdo("shangou", cacheClient)){
					FirstPageManager.shangou(cacheClient);
					cacheClient.del(CacheKey.同步标记 + "shangou_lock");
				}
			}
		}, new Date(), 2 * 60 * 1000L);

////
//		GregorianCalendar calendar = new GregorianCalendar();
//		calendar.add(Calendar.DAY_OF_MONTH, 1);
//		calendar.set(Calendar.HOUR_OF_DAY, 10);
//		calendar.set(Calendar.MINUTE, 0);
//		
//		
//		timer.schedule(new TimerTask() {
//			
//			@Override
//			public void run() {
//				if(isdo("shangou", redisTemplate)){
//					FirstPageManager.shangou(redisTemplate);
//					redisTemplate.del(RedisKey.同步标记 + "shangou_lock");
//				}
//			}
//		}, calendar.getTime(), 24 * 60 * 60 * 1000L);
//		
//		
//		calendar.set(Calendar.MINUTE, 10);
//		
//		timer.schedule(new TimerTask() {
//			
//			@Override
//			public void run() {
//				if(isdo("shangou", redisTemplate)){
//					FirstPageManager.shangou(redisTemplate);
//					redisTemplate.del(RedisKey.同步标记 + "shangou_lock");
//				}
//			}
//		}, calendar.getTime(), 24 * 60 * 60 * 1000L);
//		
//		calendar.set(Calendar.MINUTE, 20);
//		
//		timer.schedule(new TimerTask() {
//			
//			@Override
//			public void run() {
//				if(isdo("shangou", redisTemplate)){
//					FirstPageManager.shangou(redisTemplate);
//					redisTemplate.del(RedisKey.同步标记 + "shangou_lock");
//				}
//			}
//		}, calendar.getTime(), 24 * 60 * 60 * 1000L);
	}

	
	/**
	 * shangou
	 * 317及以上版本
	 */
	private void shangou317(final CacheClient cacheClient, Timer timer) {
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				if(isdo("shangou317", cacheClient)){
					FirstPageManager.shangou317(cacheClient);
					cacheClient.del(CacheKey.同步标记 + "shangou317_lock");
				}
			}
		}, new Date(), 2 * 60 * 1000L);
	}
	
	
	/**
	 * 判断是否需要更新缓存
	 */
	private boolean isdo(String sign, CacheClient cacheClient){
		try {
			Calendar now = new GregorianCalendar();
			
			long setnx2 = cacheClient.setnx(CacheKey.同步标记 + sign + "_lock", "lock");
			Object object = cacheClient.get(CacheKey.同步标记 + sign + "_time");
			LogManager.getLogger("triggerLog").debug(" LogServelt.isdo()" + sign + ";object:" + object + ";setnx2:" + setnx2);
			
			if(setnx2 == 1){//抢到锁
				if(null != object && StringUtil.isNotBlank(object.toString())){//不为空，判断是否过期
//					String time = new String((byte[])object, "utf-8");
					if(now.getTimeInMillis() - Long.parseLong(object.toString()) > 0){//过期
						cacheClient.setex(CacheKey.同步标记 + sign + "_time", 120 , now.getTimeInMillis() + "");
						return true;
					}
				}else{
					cacheClient.setex(CacheKey.同步标记 + sign + "_time", 120 , now.getTimeInMillis() + "");
					return true;
				}
			}else{
				if(null != object && StringUtil.isNotBlank(object.toString())){//不为空，判断是否过期
//					String time = new String((byte[])object, "utf-8");
					if(now.getTimeInMillis() - Long.parseLong(object.toString()) > 0){//过期
						cacheClient.del(CacheKey.同步标记 + sign + "_lock");
					}
				}else{
					cacheClient.del(CacheKey.同步标记 + sign + "_lock");
				}
			}
			return false;
		} catch (Exception e) {
			LogManager.getLogger("triggerLog").error(e.getMessage(), e);
			return false;
		}
		
//		try {
//			Object object = redisTemplate.getObject(sign + "_sign");
//			
//			LogManager.getLogger("triggerLog").debug(" LogServelt.isdo()" + sign + ";object:" + object);
//			Calendar now = new GregorianCalendar();
//			
//			
//			if(null != object && StringUtil.isNotBlank(object.toString())){//不为空，判断是否过期
//				String time = new String((byte[])object, "utf-8");
//				if(now.getTimeInMillis() - Long.parseLong(time) > 0){//过期
//					now.add(Calendar.MINUTE, 2);//2分钟
//					long setnx = redisTemplate.setnx(sign + "_sign", now.getTimeInMillis() + "");
//					if(setnx == 1){//不存在
//						LogManager.getLogger("triggerLog").debug(sign + " 获得锁，开始执行。setnx:" + setnx);
//						//执行。。。。
//						return true;
//					}
//				}
//			}else{
//				now.add(Calendar.MINUTE, 2);//2分钟
//				long setnx = redisTemplate.setnx(sign + "_sign", now.getTimeInMillis() + "");
//				if(setnx == 1){//不存在
//					LogManager.getLogger("triggerLog").debug(sign + " 获得锁，开始执行。setnx:" + setnx);
//					//执行。。。。
//					return true;
//				}
//			}
//			
//			return false;
//		} catch (Exception e) {
//			LogManager.getLogger("triggerLog").error(e.getMessage(), e);
//			return false;
//		}
	}
	
	
	/**
	 * 出发日志，避免当天没有日志消息不会产生新的日志文件
	 */
	private void triggerLog() {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 30);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				try {
					LogManager.getLogger("triggerLog").debug("触发日志开始");
					
					LogManager.getLogger("h5CallApp").info("");
					LogManager.getLogger("run").debug("");
					LogManager.getLogger("idlRun").debug("");
					LogManager.getLogger("access").debug("");
					LogManager.getLogger("bizexp").warn("");
					LogManager.getLogger("redpacketActive").info("");
					LogManager.getLogger("budan").info("");
					LogManager.getLogger("send").info("");
					LogManager.getLogger("common").info("");
					LogManager.getLogger("makeorder").debug("");
					AbstractLogItem.log.debug("");
					AbstractLogItem.log2.debug("");
					BiAction.log.debug("");
					BiAction.log2.debug("");
					LogManager.getLogger(ExceptionMappingInterceptor.class).debug("");
					LogManager.getLogger(TimerAndTokenInterceptor.class).debug("");
					
					LogManager.getLogger("triggerLog").debug("触发日志结束");
				} catch (Exception e) {
					LogManager.getLogger("triggerLog").debug("触发日志出错 " + e.getMessage() , e);
				}
			}
		}, calendar.getTime(), 24*60*60*1000L);
	}

	
}
