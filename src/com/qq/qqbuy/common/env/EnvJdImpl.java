package com.qq.qqbuy.common.env;

/**
 * 使用配置文件中的env.id来控制运行环境
 * @author JingYing
 * @date 2015年1月6日
 */
public class EnvJdImpl implements Env{
	
	private String envId;

	@Override
	public boolean isLocal() {
		return "local".equals(envId);
	}

	@Override
	public boolean isGamma() {
		return "jd_test".equals(envId);
	}

	@Override
	public boolean isIdc() {
		return "jd_idc".equals(envId);
	}

	@Override
	public boolean isJd() {
		return true;
	}

	@Override
	public boolean isPaipai() {
		return false;
	}

	@Override
	public void setEnvId(String id) {
		this.envId = id;
	}

	@Override
	public String getEnvId() {
		return envId;
	}

}
