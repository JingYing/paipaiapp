package com.qq.qqbuy.common.env;

import com.qq.qqbuy.common.ConfigHelper;


/**
 * 根据properties配置文件判断是京东/拍拍/gamma/idc
 * @author JingYing
 * @date 2014年12月30日
 */
public class EnvManager	{
	private static Env env;
	
    static	{
	    String envId = ConfigHelper.getProperty("env.id");
	    String envImpl = ConfigHelper.getProperty("env.impl");
	    if(envId == null || envImpl == null)
	    	throw new RuntimeException("properties配置文件中没找到env.id或env.impl");
    	try {
			env = (Env)Class.forName(envImpl).newInstance();
			env.setEnvId(envId);
		} catch (Exception e) {
			throw new UnsupportedOperationException("未识别的env.impl" + envImpl);
		}
    }
    
    /**
     * 获得环境变量标识符
     * @return
     */
    public static String getEnvId()	{
    	return env.getEnvId();
    }
    
    /**
     * 是否本地开发环境
     * @return
     */
    public static boolean isLocal()    {
    	return env.isLocal();
    }
    
    /**
     * 是否gamma环境
     * @return
     */
    public static boolean isGamma()    {
    	return env.isGamma();
    }

    /**
     * 是否IDC环境
     * @return
     */
    public static boolean isIdc()	{
    	return env.isIdc();
    }
   
    public static boolean isNotIdc()	{
    	return !env.isIdc();
    }
    
    /**
     * 是否部署在京东
     * @return
     */
    public static boolean isJd()	{
    	return env.isJd();
    }
    
    /**
     * 是否部署在拍拍
     * @return
     */
    public static boolean isPaipai()	{
    	return env.isPaipai();
    }
}
