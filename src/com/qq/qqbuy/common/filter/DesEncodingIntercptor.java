package com.qq.qqbuy.common.filter;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.util.SecretUtils;

public class DesEncodingIntercptor extends AbstractInterceptor {

	@Override
	public String intercept(ActionInvocation invocation) throws Exception {

		ActionContext actionContext = invocation.getInvocationContext();     
		 HttpServletRequest request= (HttpServletRequest) actionContext.get(StrutsStatics.HTTP_REQUEST);   
		 System.out.println("Encoding Intercept...");  
		 Log.run.info(new Gson().toJson(request.getParameterMap()));
		 //对所有的的request内容进行转码
		 Iterator iter=request.getParameterMap().values().iterator();  
		 while (iter.hasNext()){
			 String [] params = (String[]) iter.next();
			 for(int i =0 ;i<params.length;i++){
				 Log.run.info("DesEncodingIntercptor"+params[i]);
				 byte[] msg =SecretUtils.decryptMode(params[i].getBytes());
				 Log.run.info("DesEncodingIntercptor"+new String(msg));
				 params[i] = new String(msg);
				 
			 }
		 }

		return invocation.invoke();
	
	}

}
