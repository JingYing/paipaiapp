package com.qq.qqbuy.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 
 * @ClassName: ReflectGetValue 
 * @Description: 反射取值用于取Ao代理返回值
 * @author lhn
 * @date 2015-5-12 下午4:43:25 
 *
 */
public class ReflectGetValue {
    /**
     * 取出 属性和值
     * @param obj
     * @param f //取哪一个属性的值，如果为空则取全部值
     * @return
     * @throws Exception
     */
    public static Map<Object,Object> getFileValue(Object obj,String f) throws Exception{
    	
        Map<Object, Object> map = new HashMap<Object, Object>();
        Class<?> cls = obj.getClass();
        Class<?> supcls = obj.getClass().getSuperclass();
        //查出父类的属性(由于有一些Ao的属性在其父类中)
        Field [] supfields = supcls.getDeclaredFields();
        Field [] fields = cls.getDeclaredFields();
        Field [] allFields  = new Field[supfields.length+fields.length];
       /* Method [] methods = cls.getDeclaredMethods();
        Method [] supmethods = supcls.getDeclaredMethods();
        Method [] allMethods = new Method[methods.length+supmethods.length];*/
        //将父类与子类的的属性复制至一个新的属性中
        System.arraycopy(supfields,0,allFields,0,supfields.length);	
        System.arraycopy(fields,0,allFields,supfields.length,fields.length);
        //将父子类方法合并
   /*     System.arraycopy(supmethods,0,allMethods,0,supmethods.length);	
        System.arraycopy(methods,0,allMethods,supmethods.length,methods.length);*/
        for(Field field:allFields){
        	//抑制Java对其的检查()
    		field.setAccessible(true) ;
        	//如果为空则全部
        	if(f==null||"".equals(f)){
		            String fldtype = field.getType().getSimpleName();
		            String getMetName = pareGetName(field.getName());
		            String result ="";
		          //有一些类中并没有get方法。直接通过field.get获取
		            /*if(!checkMethod(allMethods,getMetName)){
		                continue;
		            }
		            Method method = cls.getMethod(getMetName, null);
		            Object object = method.invoke(obj, new Object[]{});*/
		            Object object =field.get(obj);
		            if(null != object){
		                if(fldtype.equals("Date")){
		                    result = fmlDate((Date)object);
		                }
		                result = String.valueOf(object);
		            }
		            map.put(field.getName(), result);
        	}else {
				if(f.equals(field.getName())){
					String fldtype = field.getType().getSimpleName();
		            String getMetName = pareGetName(field.getName());
		            String result ="";
		            //有一些类中并没有get方法。直接通过field.get获取
		            /*if(!checkMethod(allMethods,getMetName)){
		                continue;
		            }
		            Method method = cls.getMethod(getMetName, null);
		            Object object = method.invoke(obj, new Object[]{});*/
		            Object object =field.get(obj);
		            if(null != object){
		                if(fldtype.equals("Date")){
		                    result = fmlDate((Date)object);
		                }
		                result = String.valueOf(object);
		            }
		            map.put(field.getName(), result);
				}
			}
        }
        
        return map;
    }
    /**
     * 设置bean 属性值
     * @param map
     * @param bean
     * @throws Exception
     */
    public static void setFieldValue(Map<Object, Object> map, Object bean) throws Exception{
        Class<?> cls = bean.getClass();
        Method methods[] = cls.getDeclaredMethods();
        Field fields[] = cls.getDeclaredFields();
        
        for(Field field:fields){
            String fldtype = field.getType().getSimpleName();
            String fldSetName = field.getName();
            String setMethod = pareSetName(fldSetName);
            if(!checkMethod(methods, setMethod)){
                continue;
            }
            Object value = map.get(fldSetName);
            System.out.println(value.toString());
            Method method = cls.getMethod(setMethod, field.getType());
            System.out.println(method.getName());
            if(null != value){
                if("String".equals(fldtype)){
                    method.invoke(bean, (String)value);
                }else if("Double".equals(fldtype)){
                    method.invoke(bean, (Double)value);
                }else if("int".equals(fldtype)){
                    int val = Integer.valueOf((String)value);
                    method.invoke(bean, val);
                }
            }
            
        }    
    }
    /**
     * 拼接某属性get 方法
     * @param fldname
     * @return
     */
    public static String pareGetName(String fldname){
        if(null == fldname || "".equals(fldname)){
            return null;
        }
        String pro = "get"+fldname.substring(0,1).toUpperCase()+fldname.substring(1);
        return pro;
    }
    /**
     * 拼接某属性set 方法
     * @param fldname
     * @return
     */
    public static String pareSetName(String fldname){
        if(null == fldname || "".equals(fldname)){
            return null;
        }
        String pro = "set"+fldname.substring(0,1).toUpperCase()+fldname.substring(1);
        return pro;
    }
    /**
     * 判断该方法是否存在
     * @param methods
     * @param met
     * @return
     */
    public static boolean checkMethod(Method methods[],String met){
        if(null != methods ){
            for(Method method:methods){
                if(met.equals(method.getName())){
                    return true;
                }
            }
        }        
        return false;
    }
    /**
     * 把date 类转换成string
     * @param date
     * @return
     */
    public static String fmlDate(Date date){
        if(null != date){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);
            return sdf.format(date);
        }
        return null;
    }
}

