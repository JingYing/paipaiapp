package com.qq.qqbuy.common.client.ao;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.exception.ExternalInterfaceException;

public class AoConnDefaultImpl implements AoConn {

	@Override
	public int connect(AsynWebStub stub, Object req, Object resp) throws ExternalInterfaceException {
		try {
			return stub.invoke(req, resp);
		} catch (AsynWebStubException e) {
			throw new ExternalInterfaceException(e);
		}
	}

}
