package com.qq.qqbuy.common.client.degrade;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qq.qqbuy.common.client.AbstractHttpConnAdvice;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.http.RespHead;
import com.qq.qqbuy.common.util.Util;

public class HttpMockResponseAdvice extends AbstractHttpConnAdvice{
	private static Logger log = LogManager.getLogger();
	
	private byte[] defaultResp;
	private LevelAppraisal level = new LevelAppraisal();
	
	public HttpMockResponseAdvice(byte[] defaultResp) {
		this.defaultResp = defaultResp;
	}

	@Override
	public Object aroundConnect(MethodInvocation invocation, String url) throws Throwable {
		String uri = Util.truncateParam(url);
		if(!level.shouldDegrade(uri, Thread.currentThread().getStackTrace()))	
			return invocation.proceed();
		if(defaultResp == null)			
			defaultResp = CacheRespPool.get(uri);
		if(defaultResp == null)	{
			return invocation.proceed();
		} else	{
			log.debug(uri + "服务返回缓存");
			RespHead head = new RespHead();
			head.setDate(System.currentTimeMillis());
			head.setContentLength(defaultResp.length);
			HttpResp resp = new HttpResp();
			resp.setResponse(defaultResp);
			resp.setResponseCode(304);
			return resp;
		}	
	}

}
