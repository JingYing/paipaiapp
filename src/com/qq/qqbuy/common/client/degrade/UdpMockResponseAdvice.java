package com.qq.qqbuy.common.client.degrade;

import java.net.DatagramPacket;

import org.aopalliance.intercept.MethodInvocation;

import com.qq.qqbuy.common.client.AbstractUdpConnAdvice;

public class UdpMockResponseAdvice extends AbstractUdpConnAdvice{
	
	private String serviceName;
	private byte[] defaultResp;
	
	private LevelAppraisal level = new LevelAppraisal();
	
	public UdpMockResponseAdvice(String serviceName, byte[] defaultResp) {
		this.serviceName = serviceName;
		this.defaultResp = defaultResp;
	}

	@Override
	public Object aroundSend(MethodInvocation invocation, String host, int port) throws Throwable {
		return invocation.proceed();
	}

	@Override
	public Object aroundSendAndReceive(MethodInvocation invocation, String host, int port)
			throws Throwable {
		String serviceId = genKey4CacheResp(host, port, serviceName);
		if(!level.shouldDegrade(serviceId, Thread.currentThread().getStackTrace()))	
			return invocation.proceed();
		if(defaultResp == null)	{
			byte[] cache = CacheRespPool.get(serviceId);
			if(cache == null)	return invocation.proceed();
			return new DatagramPacket(cache, cache.length);
		} else	{
			return new DatagramPacket(defaultResp, defaultResp.length);
		}
	}

}
