package com.qq.qqbuy.common.client.degrade;

public interface Mockable {
	
	/**
	 * 当一个AO服务被降级时,调用AsynWebStub.invoke(req,resp)时,不会远程调用AO服务,而是按原样返回resp.
	 * 如果多个地方使用了同一个response,而且在降级时都要使用特殊的response,可以让*Resp实现该接口.具体调用见AoMockResponseAdvice类
	 * @return 
	 */
	Object generateMockResponse();
}
