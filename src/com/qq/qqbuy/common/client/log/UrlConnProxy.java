package com.qq.qqbuy.common.client.log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

/**
 * <pre>
 * 监控httpUrlConnection.getInputStream(). 并记录日志.
 * 调用方式:
 * HttpURLConnection conn;
 * ....
 * InputStream is = new UrlConnProxy(conn).getInputStream();
 * 或
 * InputStream is = new UrlConnProxy(conn, true, "utf-8").getInputStream();
 * </pre>
 * @author JingYing 2014-10-20
 * @deprecated 使用com.qq.qqbuy.common.client.http.HttpClient统一调用第3方http服务
 */
public class UrlConnProxy {
	
	private static int MAX_READ = 10000;	//最大读取多少字节的响应体, 记到日志中, 防止日志过大
	private HttpURLConnection conn;
	private PacketLogParser logParser;
	
	/**
	 * 使用urlConnProxy监控http调用
	 * 默认不读取解析响应体
	 * @param conn
	 */
	public UrlConnProxy(HttpURLConnection conn)	{
		this.conn = conn;
	}
	
	/**
	 * 使用urlConnProxy监控http调用. 如果要自定义控制日志解析过程, 使用public UrlConnProxy(HttpURLConnection conn, PacketLogParser parser)
	 * @param conn
	 * @param consumeResponse 是否让urlConnProxy读取response? 
	 * 			是的话会记录响应体到日志中, 并准确记录响应体读取时长, 但在消息体较大时,会加大内存占用,额外增加读取总时间
	 * @param respCharset 使用什么字符编码解析响应体
	 */
	public UrlConnProxy(HttpURLConnection conn, boolean consumeResponse, String respCharset)	{
		this.conn = conn;
		final boolean c = consumeResponse;
		final String r = respCharset;
		this.logParser = new PacketLogParser() {
			
			@Override
			public String parseResponse(byte[] response) throws Exception {
				if(c && response != null)	{
					try {
						return new String(response, r);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						return null;
					}
				} else	{
					return null;
				}
			}
			
			@Override
			public String parseRequest(byte[] request) throws Exception {
				return "";
			}
			
			@Override
			public String getServiceName() {
				return null;
			}
		};
	}
	
	/**
	 * 自定义日志记录
	 * @param conn
	 * @param parser
	 */
	public UrlConnProxy(HttpURLConnection conn, PacketLogParser parser)	{
		this.conn = conn;
		this.logParser = parser;
	}
	
	/**
	 * 使用BufferedInputStream预读并reset流, 将连接信息记录到日志中
	 * 注: 调用getInputStream()最好不要调用HttpURLConnection.getResponse()或getHeadField(). 因为调用此类API时连接已建立, UrlConnProxy就无法将建立连接时间记录进来, 只能记录流读取时间.
	 * @return
	 * @throws IOException
	 */
	public InputStream getInputStream() throws IOException	{
		HttpLogItem item = new HttpLogItem();
		item.setInvokeStack(Thread.currentThread().getStackTrace());
		item.setUrl(conn.getURL().toString());
		String hostHead = conn.getRequestProperty("Host");
		if(hostHead != null && !"".equals(hostHead))
			item.setHost(hostHead);		//首选头信息中的host
		else
			item.setHost(conn.getURL().getHost());
	
		long start = System.currentTimeMillis();
		BufferedInputStream is = null;
		try {
			conn.connect();		//尝试connect建立TCP连接, 如果调用urlconnProxy之前, httpurlconn已经建立TCP连接, 则此处无效,下面的connectMillis也无效.
			item.setConnectMillis(System.currentTimeMillis() - start);
			is = new BufferedInputStream(conn.getInputStream());
			item.setContentType(conn.getContentType());
			item.setContentLength(conn.getContentLength());
			item.setRespCode(conn.getResponseCode() + "");
			item.setConnectCostMillis(System.currentTimeMillis() - start);
			
			if(logParser != null){ 					
				long s = System.currentTimeMillis();
				try {
					item.setReqBody(logParser.parseRequest(null));	//此处无法取得urlconnection.outputstream,传null
					byte[] resp = readAndReset(is, conn.getContentLength());
					if(resp.length > 0)	{
						item.setRespText(logParser.parseResponse(resp));
					}
					item.setReadBodyCostMillis(System.currentTimeMillis() - s);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			item.setConnectCostMillis(System.currentTimeMillis() - start);
			item.setException(e);
			throw new IOException(conn.getURL().toString(), e);
		} finally	{
			item.log();
		}
		return is;
	}
	
	/**
	 * 读取流,并reset流
	 * @param is
	 * @param contentLength
	 * @return
	 * @throws IOException
	 */
	private byte[] readAndReset(BufferedInputStream is, int contentLength) throws IOException	{
		int available = contentLength > 0 ? contentLength : is.available();
		if(available > 0 && is.markSupported())	{
			ByteArrayOutputStream baos = new ByteArrayOutputStream(2048);
			is.mark(Integer.MAX_VALUE);
			byte[] buf = new byte[1024];
			int len = 0;
			long totalLen = 0L;
			while(true)	{
				len = is.read(buf);
				if(len == -1)	{
					break;
				} else	{
					totalLen += len;
					if(totalLen > MAX_READ)	{						//只读取这么多字节, 读太多的话在日志中没什么意义, 还有可能报out memory
						baos.write(buf, 0, len < MAX_READ ? len : MAX_READ);
						baos.write("  ...too long, truncate".getBytes());
						break;
					} else	{
						baos.write(buf, 0, len);
					}
				}
			}
			is.reset();
			return baos.toByteArray();
		}
		return new byte[0];
	}

}
