package com.qq.qqbuy.common.client;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public abstract class AbstractUdpConnAdvice implements MethodInterceptor{
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		if("send".equals(invocation.getMethod().getName()))	{
			String host = (String)invocation.getArguments()[0];
			int port = (Integer)invocation.getArguments()[1];
			return aroundSend(invocation, host, port);
		} else if("sendAndReceive".equals(invocation.getMethod().getName()))	{
			String host = (String)invocation.getArguments()[0];
			int port = (Integer)invocation.getArguments()[1];
			return aroundSendAndReceive(invocation, host, port);
		} else	{
			return invocation.proceed();
		}
	}
	
	public abstract Object aroundSend(MethodInvocation invocation, String host, int port) throws Throwable;
	
	public abstract Object aroundSendAndReceive(MethodInvocation invocation, String host, int port) throws Throwable;
	
	
	protected String genKey4CacheResp(String host, int port, String serviceName)	{
		if(serviceName == null)	{
			return String.format("udp://%s:%s", host, port);
		} else	{
			return String.format("udp://%s", String.valueOf(serviceName));
		}
	}
	
	protected String genKey4Ump(String host, int port, String serviceName)	{
		return String.format("udp://%s:%s(%s)", host, port, String.valueOf(serviceName));
	}

}
