package com.qq.qqbuy.common.client.router.ao;

public class UinSkey {
	public long uin;
	public String skey;
	public boolean isLogin;
	public UinSkey(long uin ,String skey, boolean isLogin)	{
		this.uin = uin;
		this.skey = skey;
		this.isLogin = isLogin;
	}
}
