package com.qq.qqbuy.common.client.http;

import java.util.List;
import java.util.Map;

public class HttpResp {

	private int responseCode; // http响应码
	private RespHead respHead;	//http响应头
	private byte[] response; // 响应体
	

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public byte[] getResponse() {
		return response;
	}

	public void setResponse(byte[] response) {
		this.response = response;
	}

	public RespHead getRespHead() {
		return respHead;
	}

	public void setRespHead(RespHead respHead) {
		this.respHead = respHead;
	}
}
