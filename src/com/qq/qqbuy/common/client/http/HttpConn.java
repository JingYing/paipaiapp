package com.qq.qqbuy.common.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;

public interface HttpConn {
	
	HttpResp connect(String method, String url, ReqHead head, InputStream body) throws IOException;
	
	void setProxy(Proxy proxy);
	
}
