package com.qq.qqbuy.common.client.http;

import java.net.Proxy;

/**
 * 发起http请求时的连接策略
 * @author JingYing
 * @date 2015年4月15日
 */
public interface ConnStrategy {
	
	/**
	 * 修改最终的url和请求头host
	 * @param originalUrl
	 * @return 1:url 2:请求头host
	 */
	String[] changeUrlAndHost(String originalUrl);
	
	/**
	 * 是否使用代理服务器
	 * @param url
	 * @return 代理服务器地址
	 */
	Proxy chooseProxy(String url);
	
}
