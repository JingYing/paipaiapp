package com.qq.qqbuy.common.client.cache;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import redis.clients.jedis.JedisPool;

import com.jd.jim.cli.Cluster;
import com.qq.qqbuy.common.env.EnvManager;

/**
 * 使用FactoryBean控制CacheClient初始化, 防止在拍拍机房下初始化jimdb而报错.
 * @author JingYing
 * @date 2015年4月13日
 */
public class CacheClientFactoryBean implements FactoryBean, ApplicationContextAware	{

	private ApplicationContext applicationContext;
	
	@Override
	public Object getObject() throws Exception {
		CacheClient client = new CacheClient();
		if(EnvManager.isJd())	{
			Cluster jimClient = (Cluster)applicationContext.getBean("jimClient");
			client.setImpl(new JimOperImpl(jimClient));
		} else if(EnvManager.isPaipai())	{
			JedisPool jedisPool = (JedisPool)applicationContext.getBean("jedisPool");
			client.setImpl(new RedisOperImpl(jedisPool));
		} else	{
			throw new RuntimeException("不识别的运行环境:" + EnvManager.getEnvId());
		}
		return client;
	}

	@Override
	public Class getObjectType() {
		return CacheClient.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
