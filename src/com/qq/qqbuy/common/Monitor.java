package com.qq.qqbuy.common;

import com.jd.ump.profiler.CallerInfo;
import com.jd.ump.profiler.proxy.Profiler;
import com.qq.qqbuy.common.env.EnvManager;


/**
 * 上报至UMP统一监控平台.为避免到处在代码中引用UMP,降低与UMP耦合,做下简单封装
 */
//TODO 迁移到京东后再启用 
public class Monitor {

	public static final String SYSTEM_KEY = "paipaiapp";
	
	/**
	 * 注册JVM和系统心跳监控, 在spring中初始化一次即可
	 */
	public static void initHeartbeatAndJvm()	{
		if(EnvManager.isJd())	{
			Profiler.registerJVMInfo(SYSTEM_KEY);
			Profiler.InitHeartBeats(SYSTEM_KEY);
		}
	}
	
	
	/**
	 * 开启监控method. 为避免到处在代码中引用UMP, 做下简单封装
	 * <pre>使用方法:
	 * public void methodA()	{
	 * 	Profiler.registerInfo(key, enableHeartbeat, enableTP);
	 * 	.....do sth
	 * 	Profiler.registerInfoEnd((CallerInfo)callerInfo);
	 * }</pre>
	 * @param key 监控第三方接口时格式如下:
	 * http协议: http://www.example.com/**.html 不包含参数
	 * tcp协议:  tcp://127.0.0.1:8080(服务名或配置中心key) 后面包含服务名或配置中心key便于排查
	 * udp协议:  udp://127.0.0.1:8008(服务名或配置中心key) 后面包含服务名或配置中心key便于排查, 只能监控带响应包的接口
	 * ao服务:   ao://0x12345678(Req类名)
	 * boss服务: boss://0x12345678(Req类名)
	 * 
	 * 监控我方的服务时格式如下:
	 * http协议: api/deal/query.action  不需要包含域名和参数
	 * 
	 * @return
	 */
	public static Object registerInfo(String key)	{
		if(EnvManager.isJd())	{
			try {
				return Profiler.registerInfo(key, false, true); //不监控方法心跳, 只监控方法执行时间
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
	
	/**
	 * 结束监控method. 为避免到处在代码中引用UMP, 做下简单封装
	 * @param callerInfo Profiler.registerInfo的返回值
	 */
	public static void registerInfoEnd(Object callerInfo)	{
		if(EnvManager.isJd())	{
			try {
				if(callerInfo != null)	{
					Profiler.registerInfoEnd((CallerInfo)callerInfo);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 出现异常时记录.
	 * @param callerInfo
	 */
	public static void functionError(Object callerInfo)	{
		if(EnvManager.isJd())	{
			if(callerInfo != null)
				Profiler.functionError((CallerInfo)callerInfo);
		}
	}
	
	/**
	 * 立即报警. 自动生成key,key=调用栈中的上一级调用者的包名+方法名
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(String detail)	{
		if(EnvManager.isJd())	{
			StackTraceElement ele = Thread.currentThread().getStackTrace()[2];
			String key = ele.getClassName() + "." + ele.getMethodName() + "()";
			Profiler.businessAlarm(key, System.currentTimeMillis(), detail);
		}
	}

	/**
	 * 立即报警. 自动生成key,key=调用栈中的上一级调用者的包名+方法名
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(int callerIndex, String detail)	{
		if(EnvManager.isJd())	{
			StackTraceElement ele = Thread.currentThread().getStackTrace()[callerIndex];
			String key = ele.getClassName() + "." + ele.getMethodName() + "()";
			Profiler.businessAlarm(key, System.currentTimeMillis(), detail);
		}
	}
	
	/**
	 * 立即报警
	 * @param key 在http://ump.360buy.com上注册的自定义监控点的key,为空或仅含有空格则不会报警. 一般是包名+方法名
	 * @param time 该条信息产生的时间(ms)
	 * @param detail 报警信息详细描述,允许的最大长度为512字符,为空或仅含有空格则不会报警 
	 */
	public static void alarm(String key, long time, String detail)	{
		if(EnvManager.isJd())	{
			Profiler.businessAlarm(key, time, detail);
		}
	}
	
	
}
