package com.qq.qqbuy.common.exception;

import com.qq.qqbuy.common.constant.ErrConstant;

// 不建议在此处重新定义ErrCode，会导致不好管理，推荐使用ErrConstant
//此处使用ErrConstant中的errCode。
//@Deprecated注释影响原有代码
public enum BusinessErrorType {

    SUCCESSFUL(ErrConstant.SUCCESS, "successful"),
    // 系统错误
    PARAM_ERROR(ErrConstant.ERRCODE_INVALID_PARAMETER, "参数错误"), 
    CALL_OUTIDL_FAIL(ErrConstant.IDL_FAILURE, "调用外部接口失败"), 
    HTTP_REQUEST_FAILED(ErrConstant.HTTP_FAILURE, "HTTP请求失败"), 
    UNKNOWN_EXP(ErrConstant.ERRCODE_ACTION_UNKNOW_EXP, "未知异常"), 
    NOT_LOGIN(ErrConstant.ERRCODE_CHECKLOGIN_FAIL,ErrConstant.ERRCODE_JSON_CHECKLOGIN_FAIL_MSG),
    UNKNOWN_ERROR(0x00FF, "未知系统错误"),
    
    // OpenAPI相关
    CALL_OPENAPI_XML_IS_NULL((int)ErrConstant.INERRCODE_OPENAPI_XML_IS_NULL,"调用 OpenApi失败"), 
    CALL_OPENAPI_PARSE_XML_FAILED((int)ErrConstant.INERRCODE_OPENAPI_PARSE_XML_FAILED,"调用 OpenApi失败"), 
    CALL_OPENAPI_XML_DOM_FAILED((int)ErrConstant.INERRCODE_OPENAPI_XML_DOM_FAILED,"调用 OpenApi失败"), 
    CALL_OPENAPI_HTTP_IO_FAILED((int)ErrConstant.INERRCODE_OPENAPI_HTTP_IO_FAILED,"调用 OpenApi失败"), 
    CALL_OPENAPI_BIZ_FAILED((int)ErrConstant.INERRCODE_OPENAPI_BIZ_FAILED,"调用 OpenApi失败"), 


    // 业务错误
    NULL_SKULIST(0x1000, "没有找到对应的SKULIST"),
    
    // 订单相关
    INVALID_ITEMINFO(ErrConstant.INVALID_ITEMINFO, "商品信息不全"),
    INVALID_ITEMCOUNT(ErrConstant.INVALID_ITEMCOUNT, "商品数量无效"),
    INVALID_ADDRESS(ErrConstant.INVALID_ADDRESS, "收货地址无效"),
    INVALID_PP_DEAL(ErrConstant.INVALID_PP_DEAL, "拍拍订单生成失败"),
    INVALID_PAY_INFO(ErrConstant.INVALID_PAY_INFO, "支付中心调用失败"),
    COD_DEAL_FAILURE(ErrConstant.COD_DEAL_FAILURE, "货到付款订单生成失败"),
    PC_COD_DEAL_FAILURE(ErrConstant.PC_COD_DEAL_FAILURE, "PC订单生成失败"),
    INVALID_PAY_TYPE(ErrConstant.INVALID_PAY_TYPE, "支付类型错误"),
    INVALID_STOCKATTR(ErrConstant.INVALID_STOCKATTR, "商品属性无效"),
    INVALID_DEAL_NOTEXIST(ErrConstant.DEAL_NOT_EXIST, "该订单不存在"),
    COD_MSG_FAILURE(ErrConstant.COD_MSG_FAILURE, "确认订单短信失败"),
    
    // 活动相关
    INVALID_ACTIVITY_LIST(ErrConstant.INVALID_ACTIVITY_LIST, "未参加活动"),
    INVALID_ACTIVITY_DETAIL(ErrConstant.INVALID_ACTIVITY_DETAIL, "无法找打活动信息"),
    
    // 商品相关
    LOAD_ITEM_FAILED(ErrConstant.ERRCODE_ITEMWAP2_NOT_FOUND, "获取商品信息失败"),
    LOAD_ITEM_EXT_FAILED(ErrConstant.ERRCODE_ITEMWAP2_EXTINFO_FAIL, "获取商品信息失败"),
    LOAD_ITEM_CMT_FAILED(ErrConstant.ERRCODE_ITEM_EVAL_FAIL, "获取商品评价失败"),
    ERRCODE_ITEM_DETAIL_FAIL(0x0005, "商详获取失败"),
    ITEM_ID_ILLEGAL(ErrConstant.ERRCODE_ITEMWAP2_ITEM_ID_ILLEGAL,"SHANGP商品id不合法"),

    
    //支付
    INVALID_TOKEN(ErrConstant.INVALID_TOKEN, "支付失败"),
    
    //物流
    WULIU_DEAL_NOT_HAS_WULIUINFO(ErrConstant.ERRCODE_MSG_COMPANY_NAME_IS_NULL, "订单不包括物流信息"),
    WULIU_COMPANY_NAME_IS_NULL(ErrConstant.ERRCODE_MSG_COMPANY_NAME_IS_NULL, "物流公司名称为空"),
    WULIU_CANNOT_FIND_COMPANYID(ErrConstant.ERRCODE_MSG_CANNOT_FIND_COMPANYID, "查找不到物流公司名称"),
    WULIU_GET_FAILED(ErrConstant.ERRCODE_MSG_GETWULIU_FAIL, "物流商异常"),  //物流商异常
    //收货地址相关
    ADDR_CANNOT_FIND(ErrConstant.ERRCODE_QUERY_ADDR_FAIL, "查找不到地址"),
    ADDR_ADD_LIMIT(ErrConstant.ERRCODE_ADD_ADDR_LIMIT, "收货地址最多不能超10个");
    
    private int errCode;
    private String errMsg;

    private BusinessErrorType(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

}
