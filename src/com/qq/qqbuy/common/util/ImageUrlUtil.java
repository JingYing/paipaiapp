package com.qq.qqbuy.common.util;

/**
 * 
 * @ClassName: ImageUrlUtil 
 * @Description: 图片url生成
 * @author wendyhu 
 * @date 2013-3-9 下午01:23:27
 */
public class ImageUrlUtil
{
    /**
     * 图片格式
     */
    public static String DEFAULTFormat = "300x300";
    
    public static String DEFAULTFormat80 = "80x80";
    
    public static String DEFAULTFormat120 = "120x120";
    
    public static String DEFAULTFormat160 = "160x160";
    
    public static String DEFAULTFormat200 = "200x200";
    
    /**
     * 
     * @Title: genImageUrl 
     * 
     * @Description: 生成图片url
     * @param url 原图url
     * @param imageFormat 图片格式
     * @return    设定文件 
     * @return String    返回类型 
     * @throws
     */
    public static String genImageUrl(String url , String imageFormat)
    {
        int len = 0;
        if(url != null && (len = url.length()) > 10)
        {
            len--;
            int pos = url.lastIndexOf("/");
            if(pos > 1 && pos < len)
                pos = url.indexOf(".",pos + 1);
            if(pos > 1 && pos < len)
                pos = url.indexOf(".",pos + 1 );
            if(pos > 1 && pos < len)
                return url.substring(0,pos) + "." + imageFormat + url.substring(pos);
        }
        return url;
    }
    
    /**
     * 
     * @Title: genImageUrlFromLogo 
     * @Description: 根据主图生成图片url
     * @param uin 用户qq号码
     * @param url 主图url
     * @return    设定文件 
     * @return String    返回类型 
     * @throws
     */
    public static String genImageUrlFromLogo(long uin, String url)
    {
        if(url != null)
        {
            url = "http://img" + uin%8  + ".paipaiimg.com/" + url;
        }
        else
        {
            url = "http://pai.qpic.cn/paipai/default.jpg";
        }
        return url;
    }
    
    /**
     * 
     * @Title: genImageUrlFromLogo 
     * @Description: 根据主图生成图片url
     * @param uin
     * @param url
     * @return    设定文件 
     * @return String    返回类型 
     * @throws
     */
    public static String genImageUrlFromItem(long uin, String item)
    {
        String url = "";
        if(item != null)
        {
            url = "http://img" + uin%8  + ".paipaiimg.com/item-00598B88-" + item + ".0.jpg";
        }
        else
        {
            url = "http://pai.qpic.cn/paipai/default.jpg";
        }
        return url;
    }
    
    
}
