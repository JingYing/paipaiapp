package com.qq.qqbuy.common.util;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class PageUtil {
	private final static String regxpForHtml = "<([^>]*)>";
	public static String URLUTF8Encode(String s) {
		try {
			return java.net.URLEncoder.encode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {

		}
		return "";
	}
	
	public static String URLUTF8Decode(String s) {
		try {
			return java.net.URLDecoder.decode(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return "";
	}

	/**
	 * http://www.xxxx.com/g/s?aid=xxx&bid=xxxxxx或者是aid=xxxxx&bid=xxxx
	 * @param s
	 * @return
	 */
	public static String URLEncodeParams(String s) {
		if ( s == null || s.length() == 0 ) {
			return "";
		}
		
		String index = "";
		String params = "";
		if ( s.contains("?") ) {
			index = s.substring(0,s.indexOf('?') + 1);
			params = s.substring(s.indexOf('?') + 1);
		} else {
			params = s;
		}
		
		String params1 = "";
		String[] s1 = StringUtil.split(params, "&");
		if ( s1 != null ) {
			for (int i = 0; i < s1.length; i++) {
				String[] s2 = StringUtil.split(s1[i], "=");
				if ( s2 != null && s2.length == 2 ) {
					params1 += s2[0]+"="+PageUtil.URLUTF8Encode(s2[1]);
					if ( i != s1.length - 1 ) {
						params1 += "&";
					}
				}
			}
		}
		if ( params1.length() == 0 ) {
			params1 = params;
		}
		return index + params1;
	}
	
	public static boolean hasChinese(String s) {
		for (int i = 0; i < s.length(); i++) {
			if ( isChinese(s.charAt(i)) ) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
		|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
		|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
		|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
		|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
		|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
			return true;
		}
		return false;
	}
	
	public static String unEscapeHtml(String str) {
		if ( str == null ) {
			return "";
		}
		str = str.replace("&lt;","<");
		str = str.replace("&gt;",">");
		str = str.replace("nbsp;", "");
		str = str.replace("&amp;", "");
		str = str.replace("&quot;", "\"");
		return str;
	}
	public static String filterHtml(String str) {
		if ( str == null ) {
			return "";
		}
		
		Pattern pattern = Pattern.compile(regxpForHtml);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		while (result1) {
			matcher.appendReplacement(sb, "");
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
	
	public static String excapeHtml(String html) {
		if ( html == null ) {
			return "";
		}
	        html = StringUtils.replace(html, "&", "&amp;");// 替换跳格

		    html = StringUtils.replace(html, "'", "&apos;");
		    html = StringUtils.replace(html, "\"", "&quot;");
		    html = StringUtils.replace(html, "\t", "&nbsp;&nbsp;");// 替换跳格
		    //html = StringUtils.replace(html, " ", "&nbsp;");// 替换空格
		    html = StringUtils.replace(html, "<", "&lt;");
		    html = StringUtils.replace(html, ">", "&gt;");
		   return html;
	}
	
	
	
	/**
	 * 将单位为分的转换为元展示
	 * @param money
	 * @return
	 */
	public static String showMoney(int money) {		
		BigDecimal b1 = new BigDecimal(money);
		DecimalFormat df = new DecimalFormat("0.00");
		
		return df.format(b1.divide(new BigDecimal(100)));
	}
	
	
	
	public static String trimDetail(String s) {
		if ( s == null || s.length() == 0 ) {
			return "";
		}
		return s.replaceAll("(^\\s*)|(\\s*$)", "");
	}
	/**
	 * 替换全角中文冒号
	 * @param s
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public static String processChineseColon(String s) {
		if ( s == null || s.length() == 0 ) {
			return "";
		}
		return s.replaceAll("：", ":");
	}
	public static String repaceImgUrl(String imgUrl){
		String[] array = StringUtil.split(imgUrl, ".");
		String url = "";
		for(int i = 0;i < array.length;i++){
		if(i == 0){
		  url = url +  array[i];
		}else if(i == array.length - 2){
		   url = url  + "." + "80x80";
		} else{
		   url = url  + "." + array[i];
		}
		}
		return url;
	}
	
	public static void main(String[] args) {
		//String s = "in?dfdsafdas";
		//System.out.println(s.substring(0,s.indexOf('?')+1));
		//System.out.println(""+repaceImgUrl("http://img7.paipaiimg.com/item-0DD60347-5744F6320000000000573ADE0769D403.3.300x300.jpg"));
		String s = "\n\ndfdad\n\n";
		System.out.print(trimDetail(s));
	}
}
