package com.qq.qqbuy.common.util;

import java.net.URLDecoder;
import java.net.URLEncoder;

import com.qq.qqbuy.common.Log;

/**
 * @author winsonwu
 * @Created 2012-3-28
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CoderUtil {
	
	public static String encodeUtf8(String content) {
		return encode(content, "utf-8");
	}
	
	public static String decodeUtf8(String content) {
		return decode(content, "utf-8");
	}
	
	protected static String encode(String content, String charset) {
		if (!StringUtil.isEmpty(content)) {
			try {
				return URLEncoder.encode(content, charset);
			} catch (Exception e) {
				Log.run.warn("encode content with " + charset + " fail!, content:" + content);
			}
		}
		return "";
	}
	
	protected static String decode(String content, String charset) {
		if (!StringUtil.isEmpty(content)) {
			try {
				return URLDecoder.decode(content, charset);
			} catch (Exception e) {
				Log.run.warn("decode content with " + charset + " fail!, content: " + content);
			}
		}
		return "";
	}
	
	/**
     * 暂时用于处理url作为参数经过防止XXS攻击后将&amp;替换成&amp;amp;的情况
     * @param content
     * @return
     */
    public static String decodeWML(String content) {
    	if (!StringUtil.isEmpty(content)) {
    		return content.replaceAll("&amp;amp;", "&amp;")
    					  .replaceAll("&amp;gt;", "&gt;")
    					  .replaceAll("&amp;lt;", "&lt;")
    					  .replaceAll("&amp;apos;", "&apos;")
    					  .replaceAll("&amp;quot;", "&quot;")
    					  .replaceAll("&amp;nbsp;", "&nbsp;")
    					  .replaceAll("&amp;#61;", "&#61;")
    					  .replaceAll("&amp;#96;", "&#96;")
    					  .replaceAll("&amp;#39;", "&#39;");
    	}
    	return content;
    }
    
	/**
	 * 防止过滤一些加密串使用到字符，当前是等号(=,&#61;)
	 */
	public static String decodeXssFilter(String content) {
		if (!StringUtil.isEmpty(content)) {
			return content.replaceAll("&amp;amp;", "&amp;")
					.replaceAll("&amp;gt;", "&gt;")
					.replaceAll("&amp;lt;", "&lt;")
					.replaceAll("&amp;apos;", "&apos;")
					.replaceAll("&amp;quot;", "&quot;")
					.replaceAll("&amp;nbsp;", "&nbsp;")
					.replaceAll("&amp;#61;", "&#61;")
					.replaceAll("&amp;#96;", "&#96;")
					.replaceAll("&amp;#39;", "&#39;")
					.replaceAll("&#61;", "=");
		}
		return content;
	}
	
	/**
	 * 网络字节序转换
	 */
	public static String htonls(long v) {
		return String.format("%02X%02X%02X%02X", new Object[] {
				Byte.valueOf((byte) (int) (v >>> 0)),
				Byte.valueOf((byte) (int) (v >>> 8)),
				Byte.valueOf((byte) (int) (v >>> 16)),
				Byte.valueOf((byte) (int) (v >>> 24)) });
	}
	
    public static void main(String[] args) {
    	String content = "A&amp;amp;b&amp;nbsp;c&amp;quot;d&amp;apos;g&amp;gt;gg&amp;lt;jjj&amp;amp;";
    	System.out.println(decodeWML(content));
    }
	
}
