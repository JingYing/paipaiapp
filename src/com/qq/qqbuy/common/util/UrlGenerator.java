package com.qq.qqbuy.common.util;

import com.qq.qqbuy.common.util.StringUtil;

/**
 * 和URL相关的构造
 * @author winsonwu
 * @Created 2012-3-19
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class UrlGenerator {
	
	protected static String genUrlParamStr(String... params) {
		StringBuilder sbf = new StringBuilder();
		if (params != null && params.length % 2 == 0) {  //参数必须是成对的
			for (int i=0; i<params.length-1; i+=2) {
				if (StringUtil.isEmpty(params[i+1])) {
					continue;
				}
				sbf.append("&amp;");
				sbf.append(params[i]);
				sbf.append("=");
				sbf.append(params[i+1]);
			}
			if (sbf.length() >= 1) {
				return "?" + sbf.substring(5, sbf.length());
			}
		}
		
		return sbf.toString();
	}
	
	protected static boolean appendSid(StringBuilder sbf, String sid, boolean isFirst) {
		if (!StringUtil.isEmpty(sid)) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("sid=").append(sid);
			return false;
		}
		return isFirst;
	}
	
	protected static boolean appendUploadTime(StringBuilder sbf, String uploadTime, boolean isFirst) {
		if (!StringUtil.isEmpty(uploadTime)) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("uploadTime=").append(uploadTime);
			return false;
		}
		return isFirst;
	}
	protected static boolean appendPn(StringBuilder sbf, int pn, boolean isFirst) {
		if (pn > 0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("pn=").append(pn);
			return false;
		}
		return isFirst;
	}
	
	protected static boolean appendDay(StringBuilder sbf, int day, boolean isFirst) {
		if (day > 0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("day=").append(day);
			return false;
		}
		return isFirst;
	}
	
	protected static boolean appendPs(StringBuilder sbf, int ps, boolean isFirst) {
		if (ps > 0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("ps=").append(ps);
			return false;
		}
		return isFirst;
	}
	
	protected static boolean appendVw(StringBuilder sbf, int vw, boolean isFirst) {
		if (vw >= 0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("vw=").append(vw);
			return false;
		}
		return isFirst;
	}
	
	protected static boolean appendRegionId(StringBuilder sbf, long regionId, boolean isFirst) {
		if (regionId > 0) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("rid=").append(regionId);
			isFirst = false;
		}
		return isFirst;
	}
	
	protected static boolean appendBackUrl(StringBuilder sbf, String backUrl, boolean isFirst) {
		if (!StringUtil.isEmpty(backUrl)) {
			sbf.append(isFirst ? "?" : "&amp;");
			sbf.append("bkurl=").append(backUrl);
			isFirst = false;
		}
		return isFirst;
	}
	
}
