package com.qq.qqbuy.common.util;

/**
 * 根据商品ID解析出 卖家QQ和商品真是ID
 * 商品ID的字符串: BF716790 00000000 040100003996D47A
		第一组8位为卖家QQ按一定顺序转成16进制,转换规则:BF716790转成906771BF 
		第二组8位为0
		第三组16位为商品ID的16进制,如不够16位前面补0.
		解析后卖家QQ为2422698431, 商品真实ID为288511852094608506
 * @return
 */
public class ItemCode {
	private long sellerUin, itemId;
	
	/**
	 * 根据itemcode反解成对象
	 * @param itemCode
	 */
	public ItemCode(String itemCode)	{
		if(itemCode.length() != 32)
			throw new IllegalArgumentException("不识别的商品编码:" + itemCode);
		String uin = new StringBuilder()
			.append(itemCode.substring(6, 8))
			.append(itemCode.substring(4, 6))
			.append(itemCode.substring(2, 4))
			.append(itemCode.substring(0, 2))
			.toString();
		this.sellerUin = Long.parseLong(uin, 16);
		this.itemId = Long.parseLong(itemCode.substring(16,32), 16);
	}
	
	 /**
     * 根据卖家uin和商品真是ID 生成商品 ID
     * @param sellerUin
     * @param originalItemId
     * @return
     */
	public ItemCode(long sellerUin, long originalItemId)	{
		this.sellerUin = sellerUin;
		this.itemId = originalItemId;
	}
	
	@Override
	public String toString()	{
		String 
			hexUin = Long.toHexString(sellerUin),
			hexItemId = Long.toHexString(itemId);
		
		StringBuilder zeros = new StringBuilder();
		for(int i=0;i < 8-hexUin.length();i++)	{
			zeros.append("0");
		}
		hexUin = zeros.toString() + hexUin;	//卖家QQ转换成16进制后, 如果不够8位, 前面补0
		
		StringBuilder sb = new StringBuilder()
			.append(hexUin.substring(6, 8))
			.append(hexUin.substring(4, 6))
			.append(hexUin.substring(2, 4))
			.append(hexUin.substring(0, 2))
			.append("00000000");
		
		for(int i=0; i < 16-hexItemId.length(); i++)	{
			sb.append("0");
		}
		sb.append(hexItemId);
		
		return sb.toString().toUpperCase();
	}

	public long getSellerUin() {
		return sellerUin;
	}

	public long getItemId() {
		return itemId;
	}
}
