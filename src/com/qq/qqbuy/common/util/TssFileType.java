package com.qq.qqbuy.common.util;

public enum TssFileType {
	item,

	caoption,

	shop;

	public static String getTssFileTypeName(TssFileType fileType) {
		if (fileType == item)
			return "item";
		if (fileType == caoption) {
			return "caoption";
		}
		return "shop";
	}

	public static boolean isValidateType(String value) {
		if (value == null) {
			return false;
		}

		return value.length() == 4;
	}
}