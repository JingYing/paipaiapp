package com.qq.qqbuy.common.util.region;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.qq.qqbuy.common.util.StringUtil;

/**
 * 收货地址工具类
 * @author ethonchan
 *
 */
public class RegionUtil {
	
	public static RegionHolder parseRegionFile(File xmlFile){
		RegionHolder holder = new RegionHolder();
		
		SAXBuilder builder = new SAXBuilder();
		try {
			Document doc = builder.build(xmlFile);
			Element root = doc.getRootElement();
			List provincelst = root.getChildren("province");
			
			for (Iterator piter = provincelst.iterator(); piter.hasNext();) {
				Element province = (Element) piter.next();
				Province pro = new Province();
				pro.provinceName = province.getAttributeValue("name");
				pro.provinceId = StringUtil.convertInt(province.getAttributeValue("id"), 0);
				holder.provinceList.add(pro);
				holder.provinceMap.put(pro.provinceId,pro);
				List citylst = province.getChildren("city1");
				for (Iterator citer = citylst.iterator(); citer.hasNext();) {
					Element city = (Element) citer.next();
					City c = new City();
					c.cityId = StringUtil.convertInt(city.getAttributeValue("id"), 0);
					c.cityName = city.getAttributeValue("name");
					c.provinceId = pro.provinceId;
					pro.cityList.add(c);
					holder.cityMap.put(c.cityId, c);
					List arealst = city.getChildren("city2");
					for (Iterator aiter = arealst.iterator(); aiter.hasNext();) {
						Element area = (Element) aiter.next();
						Area a = new Area();
						a.areaId = StringUtil.convertInt(area.getAttributeValue("id"), 0);
						a.name = area.getAttributeValue("name");
						c.areaList.add(a);
						a.cityId = c.cityId;
						a.provinceId = pro.provinceId;
						holder.areaMap.put(a.areaId, a);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return	holder;
	}
}
