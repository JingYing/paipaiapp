package com.qq.qqbuy.common.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.QQBuyLogin;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.po.LoginResult;
import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.Base64;
import com.qq.qqbuy.common.util.MD5Coding;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.TenpayLogin;
import com.qq.qqbuy.common.util.TokenUtil;
import com.qq.qqbuy.common.util.UKItem;
import com.qq.qqbuy.common.util.WeiXinLoginKey;
import com.qq.qqbuy.common.util.WeiXinParser;
import com.qq.qqbuy.common.util.url.UrlCodeUtil;
import com.qq.qqbuy.login.po.AppToken;
import com.qq.qqbuy.login.po.JDSdkAppToken;
import com.qq.qqbuy.login.po.QQSdkAppToken;
import com.qq.qqbuy.login.po.WXSdkAppToken;
import com.qq.qqbuy.login.po.WtLoginAppToken;
import com.qq.qqbuy.verifycode.biz.VerifyCodeService;

/**
 * 需要鉴权的Action基类. 会对输入参数进行登录态校验
 * 
 * @author rickwang
 */
@SuppressWarnings("serial")
public abstract class AuthRequiredAction extends BasicAction {
	
	/**
	 * iosMK格式正则
	 */
	private static final String REGEX_MK_IOS = "^[0-9A-F]{8}(-[0-9A-F]{4}){3}-[0-9A-F]{12}$";
	
	/**
	 * androidMK格式正则
	 */
	private static final String REGEX_MK_ANDROID = "^[0-9a-f]{40,}$";
	
    /**
     * 这里的uk是由(base64(uin=??&lskey[|skey|sid]=??&type=0[1|2])组成的。如果前端传了uk过来，
     * 那么这里验证登陆就会以uk为准， 并解析出里面的信息并填写到对应的qq号等其它字段里)
     */
    private String uk = "";// login token;
    /**
     * 如果穿了appToken过来，则按新的登陆方式校验
     */
    private String appToken = "";//拍拍 新的login token 规范
   /**
    * 拍拍用户唯一标示
    */
    private Long wid  ;
    private String ppType ;//登录类型 wt qq wx
    private String mt ;//设备类型
    private String longitude ;//经度
    private String latitude ;//纬度
    private String appID ;//拍拍多个APP的唯一识别码
    private String versionCode ;//App版本
    private String channel; //广告渠道参数
    private String sourceMk ;//源mk
	

	

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMt() {
		return mt;
	}

	public void setMt(String mt) {
		this.mt = mt;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getPpType() {
		return ppType;
	}

	public void setPpType(String ppType) {
		this.ppType = ppType;
	}

	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	public Long getWid() {
	return wid;
}

public void setWid(Long wid) {
	this.wid = wid;
}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	/**
     * 以下这些参数用在调用qq网购后台服务需要
     */
    private long qq;// 默认的qq号参数
    private String mk = "";// 默认的mobileKey参数 (手机码)
    private String mk2 = "";//京东体系下的设备唯一编号
    private String pinid = "";
    private String cp = "";// 默认的clientIp参数 (用户登陆ip)
    private String osVersion = "";//操作系统版本
    
    private String sk = "";// web站点的skey
    private String qua = "";// 默认的Q-UA参数
    private String skTemp = "" ;//生成sk的中间参数

    /**
     * 以下的参数是用在不同的验证登陆态场景
     */
    private String lk = "";// 默认的lskey参数 (登陆后的token)
    private String sid = "";// 无线的sid
    private String request_token = "";// 财付通的登陆key

    /**
     * 增加对微信登陆态的支持
     */
    private String tsk = "";

    /**
     * 用户昵称
     */
    protected String nickName = "";

    
    public String getSkTemp() {
		return skTemp;
	}

	public void setSkTemp(String skTemp) {
		this.skTemp = skTemp;
	}

	/**
     * 登陆类型
     */
    private int loginType = QQBuyLogin.NO_LOGIN_TYPE;

    protected int getLoginType() {
	return loginType;
    }

    public String getNickName() {
	return nickName;
    }

    public void setNickName(String nickName) {
	this.nickName = nickName;
    }

    public String getTsk() {
	return tsk;
    }

    public void setTsk(String tsk) {
	this.tsk = tsk;
    }

    /**
     * 无线sid验证之后会返回一个错误码。这个错误码要由业务来处理 200
     * 有效用户的sid，response里面会有uin、a8、crtTime、token。 201
     * 有效游客的sid，response里面会有uin、a8、crtTime、token，但a8、crtTime、token均为默认值。 300
     * sid安全检查需要输入验证码。
     * 
     */
    protected int code = 0;

    protected String selfUrl;

    private String pgid; // 页面ID

    // private String ptag; //来源页面tag

    public String getPgid() {
	return AntiXssHelper.htmlAttributeEncode(pgid);
    }

    public String getRequest_token() {
	return request_token;
    }

    public void setRequest_token(String requestToken) {
	request_token = requestToken;
	if (request_token != null && request_token.length() > 10
		&& request_token.length() < 100) {
	    sid = QQBuyLogin.CFT_PRE + request_token;
	    Log.run.info("checkLogin for tenpay request_token:" + request_token
		    + " sid:");
	}
    }

    public void setPgid(String pgid) {
	this.pgid = AntiXssHelper.htmlAttributeEncode(pgid);
    }

    public String getMachineKey() {
	return mk;
    }

    public void setMachineKey(String machineKey) {
	this.mk = machineKey;
    }

    public String getSk() {
	return sk;
    }

    public void setSk(String sk) {
	this.sk = sk;
    }

    public String getQua() {
	return qua;
    }

    public void setQua(String qua) {
	this.qua = qua;
    }

    public String getSid() {
	// Log.run.debug(sid);
	if (StringUtils.isEmpty(sid)) {
	    sid = "00";
	}
	if (sid.contains("=")) {
	    try {
		String temp = URLEncoder.encode(sid, "utf-8");
		return temp;
	    } catch (Exception e) {
		return sid;
	    }
	}
	// return sid;
	return QQBuyLogin.isWeixinSid(sid) ? sid : AntiXssHelper
		.htmlAttributeEncode(sid);
    }

    public String getPureSid() {
	// Log.run.debug(sid);
	if (StringUtils.isEmpty(sid)) {
	    sid = "00";
	}
	return sid;
	// return QQBuyLogin.isWeixinSid(sid) ? sid : AntiXssHelper
	// .htmlAttributeEncode(sid);
    }

    public void setSid(String sid) {
	// Log.run.debug(this.sid + ":" + sid);
	if (QQBuyLogin.isWeixinSid(sid))
	    this.sid = sid;
	else
	    // this.sid = AntiXssHelper.htmlAttributeEncode(sid);
	    this.sid = sid;
    }

    public String getUk() {
	return uk;
    }

    public void setUk(String uk) {
	this.uk = uk;
    }

    public String getCp() {
	return cp;
    }

    public void setCp(String cp) {
	this.cp = cp;
    }

    /**
     *远程验证码服务
     */
    private VerifyCodeService verifyCodeService;

    public long getQq() {
	return qq;
    }

    public void setQq(long qq) {
	this.qq = qq;
    }

    public String getLk() {
	return lk;
    }

    public void setLk(String lk) {
	this.lk = lk;
    }

    public String getMk() {
    	return mk;
    }

    public void setMk(String mk){
    	this.mk = mk;
    }

    public VerifyCodeService getVerifyCodeService() {
	return verifyCodeService;
    }

    public void setVerifyCodeService(VerifyCodeService verifyCodeService) {
	this.verifyCodeService = verifyCodeService;
    }

    public String getSelfUrl() {
	return selfUrl;
    }

    public void setSelfUrl(String selfUrl) {
	this.selfUrl = selfUrl;
    }

    /**
     * 默认的检查用户是否已经登陆的接口
     * 
     * (前提条件是用户的登陆信息都是通过http的参数传过来的，并且格式为qq=xx&ls=xx&mk=xx&cp=xx)
     * 
     * 这个接口将会从http请求里获取如下参数: qq(qq号码) lk(lskey,也就是登陆后返回的lskey)
     * 
     * mk(moblieKey,手机标识) cp(client ip 用户登陆ip)，sk(skey web和微信是用的这个登陆态)，
     * sid(无线的wap&h5使用的登陆态)
     * 
     * @return
     */
    protected boolean checkLogin() {
	Log.run.info("start checkLogin sid:" + sid + " ,uk:" + uk + " ,sk:" + sk + " ,appToken:" + appToken + ",mk," + mk);
	boolean ret = false;
	sourceMk = mk ;
	// 1、获取ip并组装参数
	String ip = getRequest().getRemoteAddr();
	if (cp == null || cp.length() < 7) {
	    if (ip != null && ip.length() > 6)
		cp = ip;
	    else
		cp = "127.0.0.1";
	}
	//0、优先token校验，token is not empty 是新接入方式的依据
	if (StringUtils.isNotEmpty(appToken)) {
		return checkLoginForToken();
	}
	// 1、这里是验证客户端的uk,是通过wtlogin登陆，然后通过ptlogin验证，验证的字段为lskey,skey,qq,clientip
	if ((uk != null && uk.length() > 10 && uk.length() < 400)) {

	    return checkLoginForUk();
	}

	// 2、对微信的tsk处理 tsk里面包含了sk,qq号码，所以这里要解析出sk和qq。 因此也是用ptlogin来验证
	if (tsk != null && tsk.length() > 10 && tsk.length() < 200) {
	    return checkLoginForWeixinTsk();
	}

	// 3、对于微信sid的处理，里面包含了skey,qq号码，因此也是用ptlogin来验证
	if (QQBuyLogin.isWeixinSid(sid)) {
	    return checkLoginForWeiXinSid();

	}

	// 4、对财付通的request_token处理 ，是用财付通的http接口来验证
	if (request_token != null && request_token.length() > 10
		&& request_token.length() < 100) {
	    return checkLoginForCftToken();
	}

	// 5、对财付通的request_token处理 ，是用财付通的http接口来验证
	if (QQBuyLogin.isCftSid(sid)) {
	    return checkLoginForCftSid();
	}

	// 6、处理无线sid的,这里是通过手机wap或者触屏登陆，然后再通过无线的sid验证服务来验证，验证的字段为sid
	// 并且还要从cookie中取出其它ng_st_sid_v2，ng_c_sid，sid三个字段
	if (sid != null && sid.length() > 10) {
	    if (sid.contains("%")) {
		try {
		    sid = URLDecoder.decode(sid, "utf-8");
		    Log.run.warn("sid decode:" + sid);
		} catch (Exception e) {
		    Log.run.warn("sid error", e);
		}
	    }
	    return checkForSid();
	}
	return ret;
    }

    /**
     * 
     * @Title: checkForSid
     * 
     * @Description: 无线sid的检查
     * 
     *               处理无线sid的,这里是通过手机wap或者触屏登陆，然后再通过无线的sid验证服务来验证，验证的字段为sid //
     *               并且还要从cookie中取出其它ng_st_sid_v2，ng_c_sid，s
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkForSid() {
	boolean ret = false;
	loginType = QQBuyLogin.SID_LOGIN_TYPE;

	// 6.1、对cookie的处理。要从cookie中取出ng_st_sid_v2，ng_c_sid，sid三个字段
	HttpServletRequest request = getRequest();
	Cookie[] cookies = request != null ? request.getCookies() : null;
	String sidFromQCookie = "", sidFromCookie = "", statSid = "";
	;
	int sidCookieCheck = 3;
	if (cookies != null && cookies.length > 0) {
	    for (Cookie cookie : cookies) {
		if (cookie != null) {
		    // 6.1.1提高性能，当 找到想要的值后就退出遍历
		    if (sidCookieCheck == 0)
			break;

		    if (cookie.getName().equals(
			    QQBuyLogin.SID_STATSID_COOKIE_KEY)
			    && (statSid = cookie.getValue()) != null) {
			sidCookieCheck--;
			continue;
		    }
		    if (cookie.getName().equals(
			    QQBuyLogin.SID_SIDFROMCOOKIE_COOKIE_KEY)
			    && (sidFromCookie = cookie.getValue()) != null) {
			sidCookieCheck--;
			continue;
		    }
		    if (cookie.getName().equals(
			    QQBuyLogin.SID_SIDFROMQCOOKIE_COOKIE_KEY)
			    && (sidFromQCookie = cookie.getValue()) != null) {
			sidCookieCheck--;
			continue;
		    }
		}
	    }
	}

	// 6.2 检查登陆态是否Ok
	LoginResult result = verifyCodeService.checkLoginForSid(cp,
		getPureSid(), statSid, sidFromCookie, sidFromQCookie);
	Log.run.info("checkLogin for sid " + result);
	if (result.getRetCode() == 0) {
	    // 6.3 生成网购验证登陆态的sk和mk
	    code = result.getCode();
	    nickName = result.getNickName();
	    qq = result.getUin();
	    if (nickName == null && nickName.length() < 1)
		nickName = "" + qq;
	    initLoginParam(sid);
	    ret = true;
	} else {
	    ret = false;
	}
	return ret;
    }

    /**
     * 
     * @Title: checkLoginForCftSid
     * 
     * @Description: 我们自己生成的cft的sid
     * 
     *               sid里面包含了财付通的request_token。 对财付通的request_token处理
     *               ，是用财付通的http接口来验证
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkLoginForCftSid() {
	boolean ret = false;
	loginType = QQBuyLogin.CFT_LOGIN_TYPE;

	// 5.1 去财付通检查登陆态是否ok
	TenpayLogin TenpayLogin = new TenpayLogin();
	if (TenpayLogin.checkLogin(sid.substring(QQBuyLogin.CFT_PRE_LENGTH))) {
	    // 4.2 生成财付通的sid 生成网购验证登陆态的sk和mk
	    try {
		nickName = URLDecoder
			.decode(TenpayLogin.getUserName(), "utf-8");
	    } catch (UnsupportedEncodingException e) {
	    }
	    qq = StringUtil.toLong(TenpayLogin.getUserId(), 0);
	    initLoginParam(sid);
	    if (nickName == null && nickName.length() < 1)
		nickName = "" + qq;
	    ret = true;
	}
	Log.run.info("checkLogin for cft sid:" + sid + " ret:" + ret
		+ " nickName:" + nickName + " qq:" + qq);
	return ret;
    }

    /**
     * 
     * @Title: checkLoginForCftToken
     * 
     * @Description: 财付通的登陆token检查
     * 
     *               对财付通的request_token处理 ，是用财付通的http接口来验证
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkLoginForCftToken() {
	boolean ret = false;
	loginType = QQBuyLogin.CFT_LOGIN_TYPE;

	// 4.1 去财付通检查登陆态是否ok
	TenpayLogin TenpayLogin = new TenpayLogin();
	ret = TenpayLogin.checkLogin(request_token);
	if (ret) {
	    // 4.2 生成财付通的sid 生成网购验证登陆态的sk和mk
	    try {
		nickName = URLDecoder
			.decode(TenpayLogin.getUserName(), "utf-8");
	    } catch (UnsupportedEncodingException e) {
	    }
	    qq = StringUtil.toLong(TenpayLogin.getUserId(), 0);
	    sid = QQBuyLogin.CFT_PRE + request_token;
	    initLoginParam(sid);
	    if (nickName == null && nickName.length() < 1)
		nickName = "" + qq;
	}
	Log.run.info("checkLogin for cft request_token:" + request_token
		+ " ret:" + ret + " nickName:" + nickName + " qq:" + qq
		+ " sid:" + sid);
	return ret;
    }

    /**
     * 
     * @Title: checkLoginForWeiXinSid
     * 
     * @Description: 我们自己生成的微信sid的检查 sid里面包含了sk和qq
     *               对于微信sid的处理，里面包含了skey,qq号码，因此也是用ptlogin来验证
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkLoginForWeiXinSid() {
	boolean ret = false;
	loginType = QQBuyLogin.WEIXIN_LOGIN_TYPE;
	try {
	    // 3.1 解析登陆状信息sk,qq号码
	    String weixinSid = new String(Base64.decode(sid
		    .substring(QQBuyLogin.WEIXIN_PRE_LENGTH)));
	    String[] array = weixinSid.split("\\+");
	    if (array != null && array.length == 2) {
		qq = Long.parseLong(array[0], 20);
		sk = array[1];
		Log.run.info("checkLogin from weixin tsk=" + tsk + " qq:" + qq
			+ " sk:" + sk + " sid:" + weixinSid);
	    }

	    // 3.2 检查登陆态是否Ok
	    LoginResult result = verifyCodeService.checkLoginForPt(qq, cp, sk,
		    lk);
	    Log.run.info("checkLogin for weixin " + result);
	    if (result.getRetCode() == 0) {
		// 3.3 生成网购验证登陆态的sk和mk
		nickName = result.getNickName();
		if (nickName == null && nickName.length() < 1)
		    nickName = "" + qq;
		initLoginParam(sid);
		ret = true;
	    } else {
		ret = false;
	    }
	} catch (Exception e) {

	}
	return ret;
    }

    /**
     * 
     * @Title: checkLoginForWeixinTsk
     * 
     * @Description: 微信tsk的登陆态检查
     * 
     *               对微信的tsk处理 tsk里面包含了sk,qq号码，所以这里要解析出sk和qq。 因此也是用ptlogin来验证
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkLoginForWeixinTsk() {
	loginType = QQBuyLogin.WEIXIN_LOGIN_TYPE;

	// 2.1 解析登陆状信息sk,qq号码
	long uin = 0;
	String skey = "";
	String weixinSid = "";
	WeiXinLoginKey key = WeiXinParser.parse(tsk);
	if (key != null && (uin = key.getUin()) >= 10000
		&& (skey = key.getSkey()) != null && skey.length() > 5) {
	    qq = uin;
	    sk = skey;
	    weixinSid = QQBuyLogin.WEIXIN_PRE
		    + Base64.encode((Long.toString(qq, 20) + "+" + sk)
			    .getBytes());
	    Log.run.info("checkLogin from weixin tsk=" + tsk + " qq:" + qq
		    + " sk:" + sk + " weixinSid:" + weixinSid);
	}
	// 2.2 检查登陆态是否Ok
	LoginResult result = verifyCodeService.checkLoginForPt(qq, cp, sk, lk);
	Log.run.info("checkLogin for weixin " + result);
	if (result.getRetCode() == 0) {
	    // 2.3 生成微信的sid 生成网购验证登陆态的sk和mk
	    sid = weixinSid;
	    nickName = result.getNickName();
	    if (nickName == null && nickName.length() < 1)
		nickName = "" + qq;
	    initLoginParam(sid);
	    return true;
	} else {
	    return false;
	}
    }

    private boolean checkLoginForToken() { 
    	try {
    		if (StringUtil.isEmpty(appToken) || appToken.length() < 10 || StringUtils.isEmpty(mk)) {
    			return false ;
    		}
    		if (!appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_WT) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_JD) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)&& !appToken.startsWith(TokenUtil.LOGIN_TYPE_PP) ) {
    			return false ;
    		}    		
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)) {
    			WtLoginAppToken wtLoginToken = TokenUtil.strToWtToken(appToken);   
        		if (null == wtLoginToken) {
        			return false ;
        		}        		
        		if (-1 != wtLoginToken.getDeadline() && wtLoginToken.getDeadline() < System.currentTimeMillis()) {
        			return false ;
        		}
        		LoginResult result = verifyCodeService.checkLoginForPt(wtLoginToken.getQq(), cp, null, wtLoginToken.getLk());
        		Log.run.info("checkLogin for appToken " + result);
        		if (result.getRetCode() == 0) {
        		    nickName = result.getNickName();
        		    Boolean result2 = verifyCodeService.checkLoginForQQSdk(mk, wtLoginToken.getWid(), wtLoginToken.getLsk()) ;
        		    if (result2) {
        		    // 1.3 生成网购验证登陆态的sk和mk
        		    	this.setPpType(TokenUtil.LOGIN_TYPE_WT);
        		    	this.setWid(wtLoginToken.getQq()) ;
        		    	this.setQq(wtLoginToken.getQq()) ;//兼容之前的接口
        		    	this.setLk(wtLoginToken.getLk()) ;//兼容之前接口
        		    	this.setSk(wtLoginToken.getLsk()) ;   
        		    	//initLoginParam(wtLoginToken.getLk());
        		    	return true;
        		    } else {
        		    	return false;
        		    }
        		} else {
        		    return false;
        		}
    		} 
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
    			JDSdkAppToken jdLoginToken = TokenUtil.strToJdToken(appToken);   
        		if (null == jdLoginToken) {
        			Log.run.info("checkLogin for appToken ,jdLoginToken is null");
        			return false ;
        		}        		
        		if (-1 != jdLoginToken.getDeadline() && jdLoginToken.getDeadline() < System.currentTimeMillis()) {
        			Log.run.info("checkLogin for appToken ,jdLoginToken time expires");
        			return false ;
        		}
        		Boolean result = verifyCodeService.checkLoginForQQSdk(mk, jdLoginToken.getWid(), jdLoginToken.getLsk()) ;
        		if (result) {
        			this.setPpType(TokenUtil.LOGIN_TYPE_JD);
         		    this.setWid(jdLoginToken.getWid()) ;
         		    this.setQq(jdLoginToken.getWid()) ;//兼容之前的接口
         		    this.setSk(jdLoginToken.getLsk()) ;
         		    return true;
        		} else {
        			return false ;
        		}
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
    			QQSdkAppToken qqLoginToken = TokenUtil.strToQqToken(appToken);   
        		if (null == qqLoginToken) {
        			Log.run.info("checkLogin for appToken ,qqLoginToken is null");
        			return false ;
        		}        		
        		if (-1 != qqLoginToken.getDeadline() && qqLoginToken.getDeadline() < System.currentTimeMillis()) {
        			Log.run.info("checkLogin for appToken ,qqLoginToken time expires");
        			return false ;
        		}
        		Boolean result = verifyCodeService.checkLoginForQQSdk(mk, qqLoginToken.getWid(), qqLoginToken.getLsk()) ;
        		if (result) {
        			this.setPpType(TokenUtil.LOGIN_TYPE_QQ);
         		    this.setWid(qqLoginToken.getWid()) ;
         		    this.setQq(qqLoginToken.getWid()) ;//兼容之前的接口
         		    this.setSk(qqLoginToken.getLsk()) ;
         		    return true;
        		} else {
        			return false ;
        		}
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
    			WXSdkAppToken wxLoginToken = TokenUtil.strToWxToken(appToken);   
        		if (null == wxLoginToken) {
        			Log.run.info("checkLogin for appToken ,wxLoginToken is null");
        			return false ;
        		}        		
        		if (-1 != wxLoginToken.getDeadline() && wxLoginToken.getDeadline() < System.currentTimeMillis()) {
        			Log.run.info("checkLogin for appToken ,wxLoginToken time expires");
        			return false ;
        		}
        		Boolean result = verifyCodeService.checkLoginForQQSdk(mk, wxLoginToken.getWid(), wxLoginToken.getLsk()) ;
        		if (result) {
        			this.setPpType(TokenUtil.LOGIN_TYPE_WX);
         		    this.setWid(wxLoginToken.getWid()) ;
         		    this.setQq(wxLoginToken.getWid()) ;//兼容之前的接口
         		    this.setSk(wxLoginToken.getLsk()) ;
         		    return true;
        		} else {
        			return false ;
        		}
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
    			AppToken appLoginToken = TokenUtil.strToAppToken(appToken);   
        		if (null == appLoginToken) {
        			Log.run.info("checkLogin for appToken ,ppLoginToken is null");
        			return false ;
        		}        		
        		if (-1 != appLoginToken.getDeadline() && appLoginToken.getDeadline() < System.currentTimeMillis()) {
        			Log.run.info("checkLogin for appToken ,wxLoginToken time expires");
        			return false ;
        		}
        		Boolean result = verifyCodeService.checkLoginForQQSdk(mk, appLoginToken.getWid(), appLoginToken.getLsk()) ;
        		if (result) {
        			this.setPpType(TokenUtil.LOGIN_TYPE_PP);
         		    this.setWid(appLoginToken.getWid()) ;
         		    this.setQq(appLoginToken.getWid()) ;//兼容之前的接口
         		    this.setSk(appLoginToken.getLsk()) ;
         		    return true;
        		} else {
        			return false ;
        		}
    		}
    		return false ;    		
    		
    	} catch (Exception ex) {
    		Log.run.error("checkLoginForToken error:", ex);
    		return false ;
    	}
    	
    	
    }
    
    /**
     * 
     * @Title: checkLoginForUk
     * 
     * @Description: 客户端的登陆态检查
     * 
     * 
     * 
     *               
     *               这里是验证客户端的uk,是通过wtlogin登陆，然后通过ptlogin验证，验证的字段为lskey,skey,qq,clientip
     * 
     * @return 设定文件
     * @return boolean 返回类型
     * @throws
     */
    private boolean checkLoginForUk() {
	loginType = QQBuyLogin.PT_LOGIN_TYPE;
	// 1.1、通过UK解析出里面的uin和lk和cp
	parseUK();

	// 1.2 检查登陆态是否Ok
	LoginResult result = verifyCodeService.checkLoginForPt(qq, cp, sk, lk);

	Log.run.info("checkLogin for uk " + result);
	if (result.getRetCode() == 0) {
	    // 1.3 生成网购验证登陆态的sk和mk
		this.setPpType(TokenUtil.LOGIN_TYPE_WT_SK) ;
	    nickName = result.getNickName();
	    initLoginParam(lk);
	    this.setWid(qq) ;
	    return true;
	} else {
	    return false;
	}
    }

    // 将UK加密串进行解密，数据存到qq、lk、cp三个字段中，其中cp会设置固定值“127.0.0.1”
    protected boolean parseUK() {
	boolean ret = false;
	if (uk != null && uk.length() > 10) {
	    try {
		UKItem ukItem = encodeUk(uk);
		long uin = UrlCodeUtil.getLong(ukItem.getName(), 0);
		String lskey = ukItem.getLskey();
		Log.run.info("uin========" + uin + ",lskey========" + lskey) ;
		if (uin > 1000 && lskey != null && lskey.length() > 9) {
		    qq = uin;
		    lk = lskey;
		    if (cp != null && cp.length() < 7)
			cp = "127.0.0.1"; // winson：如果此处为null，则鉴权会不通过
		    ret = true;
		}
	    } catch (Exception e) {
		Log.run.error("login parseUK fail uk:" + uk, e);
	    }
	}
	return ret;
    }
    
    private static UKItem encodeUk(String uk)
    {
        UKItem item = new UKItem();
        if(uk != null && uk.length() > 10)
        {
            try{
                String data = new String(Base64.decode(uk));
                item.setName(UrlCodeUtil
                        .getString(data, QQBuyLogin.LOGIN_PARAM_UIN));
                item.setLskey(UrlCodeUtil.getString(data,
                        QQBuyLogin.LOGIN_PARAM_LSKEY));
            }catch(Exception e)
            {
                
            }
        }
        return item;
    }

    protected boolean checkLoginAndGetSkey() {
    	boolean ret = checkLogin();
    	// 如果登陆成功，那么就生成一个skey
    	if (ret) {
    		sk = getPPSkey();
    		if (StringUtil.isEmpty(sk)) {
    			ret = false ;
    		}		
    	}
    	return ret;
    }
    
    protected String getPPSkey() { 
    	try {
    		if (this.ppType.equals(TokenUtil.LOGIN_TYPE_WT_SK)) {
    			String authCode = lk;
    		    int type = 1;
    		    if (sid != null && sid.length() > 5) {
    		    	authCode = sid;
    		    	type = 2;
    		    }
    		    String skey = verifyCodeService.getSkey(qq, authCode, skTemp, mk, type);
    		    if (StringUtil.isNotBlank(skey))
    				return skey;
    		    else
    				return null ;
    		} else if (this.ppType.equals(TokenUtil.LOGIN_TYPE_WT)) {
    			return sk ;
    		} else if (this.ppType.equals(TokenUtil.LOGIN_TYPE_QQ)) {
    			return sk ;
    		} else if (this.ppType.equals(TokenUtil.LOGIN_TYPE_JD)) {
    			return sk ; 
    		} else if (this.ppType.equals(TokenUtil.LOGIN_TYPE_WX)) {
    			return sk ; 
    		} else if (this.ppType.equals(TokenUtil.LOGIN_TYPE_PP)) {
    			return sk ; 
    		} else {
    			return null ;
    		}
    	} catch (Exception ex) {
    		Log.run.warn("get sk error ", ex) ;
    		return null ;
    	}
    	
    }

    /**
     * 
     * @Title: initLoginParam
     * 
     * @Description: 生成登陆sk和mk，使得网购的idl服务能够验证登陆通过
     * @param key
     *            void 返回类型
     * 
     * @throws
     */
    protected void initLoginParam(String key) {    	
	if (mk == null || mk.trim().length() != 17) {
	    mk = "11"
		    + MD5Coding.encode2HexStr((qq + key).getBytes())
			    .toLowerCase().substring(0, 15);
	}
	if (StringUtils.isEmpty(sourceMk)){
		sourceMk = mk ;
	}
	if (skTemp == null || skTemp.length() != 10) {
		skTemp = "@"
		    + MD5Coding.encode2HexStr((qq + mk).getBytes())
			    .toLowerCase().substring(0, 8) + "@";
	}
    }

    protected boolean validateUserInfo() {
	if (StringUtil.isEmpty(getMachineKey()) || getWid() <= 0) {
	    result.setErrCode(ErrConstant.ERRCODE_INVALID_PARAMETER);
	    result.setMsg("无法取得用户信息");
	    return false;
	}
	return true;
    }
    
    /**
     * 根据appToken活动用户wid
     * @param appToken
     * @return
     * @throws BusinessException
     */
    public Long getWidFromToken(String appToken) {
		// TODO Auto-generated method stub
		try {
    		if (StringUtil.isEmpty(appToken) || appToken.length() < 10) {
    			Log.run.info("getWidFromToken appToken is error validate" ) ;
    			return 0L ;
    		}
    		if (!appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_WT) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_JD) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_WX) && !appToken.startsWith(TokenUtil.LOGIN_TYPE_PP) ) {
    			Log.run.info("getWidFromToken appToken is error Type" ) ;
    			return 0L ;
    		}    		
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WT)) {
    			WtLoginAppToken wtLoginToken = TokenUtil.strToWtToken(appToken);   
        		if (null == wtLoginToken) {
        			Log.run.info("getWidFromToken wtToken error null" ) ;
        			return 0L ;
        		} 
        		return wtLoginToken.getWid() ;
    		} 
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
    			JDSdkAppToken jdLoginToken = TokenUtil.strToJdToken(appToken);   
    			if (null == jdLoginToken) {
    				Log.run.info("getWidFromToken jdLoginToken error null" ) ;
        			return 0L ;
        		} 
        		return jdLoginToken.getWid() ;
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_QQ)) {
    			QQSdkAppToken qqLoginToken = TokenUtil.strToQqToken(appToken);   
    			if (null == qqLoginToken) {
    				Log.run.info("getWidFromToken qqLoginToken error null" ) ;
        			return 0L ;
        		} 
        		return qqLoginToken.getWid() ;
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
    			WXSdkAppToken wxLoginToken = TokenUtil.strToWxToken(appToken);   
    			if (null == wxLoginToken) {
    				Log.run.info("getWidFromToken wxLoginToken error null" ) ;
        			return 0L ;
        		} 
        		return wxLoginToken.getWid() ;
    		}
    		if (appToken.startsWith(TokenUtil.LOGIN_TYPE_PP)) {
    			AppToken appLoginToken = TokenUtil.strToAppToken(appToken);   
    			if (null == appLoginToken) {
    				Log.run.info("getWidFromToken appLoginToken error null" ) ;
        			return 0L ;
        		} 
        		return appLoginToken.getWid() ;
    		}
    		
    	} catch (Exception ex) {
    		Log.run.info("getWidFromToken exception e =" + ex.toString()) ;
			return 0L ;
    	}    	
    	return 0L ;
    }
    /**
     * 
    
     * @Title: getPinByTooken 
    
     * @Description: 从token中获取pin只有京东登录时才会有 
    
     * @param @param appToken
     * @param @return    设定文件 
    
     * @return String    返回类型 
    
     * @throws
     */
   public String getPinByTooken(String appToken){
	   if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {
			JDSdkAppToken jdLoginToken;
			try {
				jdLoginToken = TokenUtil.strToJdToken(appToken);
				if (null == jdLoginToken) {
					Log.run.info("getWidFromToken jdLoginToken error null" ) ;
					return "" ;
				} 
				return jdLoginToken.getPin();
			} catch (Exception e) {
				Log.run.info("getWidFromToken exception e =" + e.toString()) ;
				return "" ;
			}   
	   }
	   return null;
   }
   /**
    * JD登录获取pin,wx登录获取openId
    * @param appToken
    * @return
    */
   public String getPinOrId(String appToken){
	   if (appToken.startsWith(TokenUtil.LOGIN_TYPE_JD)) {			
			try {
				JDSdkAppToken jdLoginToken = TokenUtil.strToJdToken(appToken);
				if (null == jdLoginToken) {
					Log.run.info("getPinFromToken jdLoginToken error null" ) ;
					return "" ;
				} 
				return jdLoginToken.getPin();
			} catch (Exception e) {
				Log.run.info("getPinFromToken exception e =" + e.toString()) ;
				return "" ;
			}   
	   }
	   if (appToken.startsWith(TokenUtil.LOGIN_TYPE_WX)) {
		   try {
			    WXSdkAppToken wxSdkAppToken = TokenUtil.strToWxToken(appToken);
				if (null == wxSdkAppToken) {
					Log.run.info("getOpenIdFromToken wxLoginToken error null" ) ;
					return "" ;
				} 
				return wxSdkAppToken.getOpenId() ;
			} catch (Exception e) {
				Log.run.info("getOpenIdFromToken exception e =" + e.toString()) ;
				return "" ;
			}  
	   }
	   return "";
   }
	public String getSourceMk() {
		return sourceMk;
	}

	public void setSourceMk(String sourceMk) {
		this.sourceMk = sourceMk;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMk2() {
		return mk2;
	}

	public void setMk2(String mk2) {
		this.mk2 = mk2;
	}

	@Override
	public void validate() {
		if(null != mk && ("0".equals(mk) || mk.matches(REGEX_MK_IOS) || mk.matches(REGEX_MK_ANDROID))){
			super.validate();
    	}else{
    		BusinessException exception = BusinessException.createInstance(ErrConstant.ERRCODE_INVALID_PARAMETER,"获取设备信息失败");
    		setErrCodeAndMsg4BExp(exception);
    	}
	}

	public String getPinid() {
		return pinid;
	}

	public void setPinid(String pinid) {
		this.pinid = pinid;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

}
