
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.common.po;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *移动电商中的公共bo
 * 
 *@date 2011-09-29 10:15::57
 * 
 *@since version:0
 */
public class QQBuyServiceComm implements ICanSerializeObject
{
    /**
     * 版本本
     * 
     * 版本 >= 0
     */
    private long version = 1;

    /**
     * 来源
     * 
     * 版本 >= 0
     */
    private String source = new String();

    /**
     * 登陆token
     * 
     * 版本 >= 0
     */
    private String lskey = new String();

    /**
     * 机器标识
     * 
     * 版本 >= 0
     */
    private String mobileKey = new String();

    /**
     * 用户uin
     * 
     * 版本 >= 0
     */
    private long uin;

    /**
     * loginAppId
     * 
     * 版本 >= 0
     */
    private long loginAppId;

    /**
     * client ip
     * 
     * 版本 >= 0
     */
    private String ip = new String();

    /**
     * 登陆skey
     * 
     * 版本 >= 0
     */
    private String skey = new String();

    /**
     * 登陆sid
     * 
     * 版本 >= 0
     */
    private String sid = new String();
    
    /** add by wendyhu 下面几个字段是从cookie中取得的。新的sid协议需要以下字段
    /**
     * 
     * 从cookie 或者qcookie中获取，cookie name 是 ng_st_sid_v2
     */
    private String statSid = "";
    
    /**
     * 从cookie中获取，cookie name 是ng_c_sid
     */
    private String sidFromCookie = "";
    
    /**
     * 从qcookie中获取，cookie name 是sid
     */
    private String sidFromQCookie = "";
    
    /**
     * URL 里面的参数名字是g_key
     */
    private String g_key = "";
    
    /**
     * vkey,URL里面的参数名字是vkey
     */
    private String vkey = "";
    
    /**
     * 返回信息
     */
    private String retMsg = "";
    
    /**
     * 生成微信的sid,不传到后台验证
     */
    private String weixinSid = "";
    
    
    protected byte[] a8;//a8信息
    protected byte[] crtTime;//crtTime;
    
    public String getWeixinSid()
    {
        return weixinSid;
    }


    public void setWeixinSid(String weixinSid)
    {
        this.weixinSid = weixinSid;
    }


    public byte[] getA8()
    {
        return a8;
    }

    
    public String getStatSid()
    {
        return statSid;
    }


    public void setStatSid(String statSid)
    {
        this.statSid = statSid;
    }


    public String getSidFromCookie()
    {
        return sidFromCookie;
    }


    public void setSidFromCookie(String sidFromCookie)
    {
        this.sidFromCookie = sidFromCookie;
    }


    public String getSidFromQCookie()
    {
        return sidFromQCookie;
    }


    public void setSidFromQCookie(String sidFromQCookie)
    {
        this.sidFromQCookie = sidFromQCookie;
    }


    public String getG_key()
    {
        return g_key;
    }


    public void setG_key(String gKey)
    {
        g_key = gKey;
    }


    public String getVkey()
    {
        return vkey;
    }


    public void setVkey(String vkey)
    {
        this.vkey = vkey;
    }


    public void setA8(byte[] a8)
    {
        this.a8 = a8;
    }

    public byte[] getCrtTime()
    {
        return crtTime;
    }

    public void setCrtTime(byte[] crtTime)
    {
        this.crtTime = crtTime;
    }

    public String getRetMsg()
    {
        return retMsg;
    }

    public void setRetMsg(String retMsg)
    {
        this.retMsg = retMsg;
    }

    public int serialize(ByteStream bs) throws Exception
    {
        bs.pushUInt(getClassSize());
        bs.pushUInt(version);
        bs.pushString(source);
        bs.pushString(lskey);
        bs.pushString(mobileKey);
        bs.pushUInt(uin);
        bs.pushUInt(loginAppId);
        bs.pushString(ip);
        bs.pushString(skey);
        bs.pushString(sid);
        if(version >= 1)
        {
            bs.pushString(statSid);
            bs.pushString(sidFromCookie);
            bs.pushString(sidFromQCookie);
            bs.pushString(g_key);
            bs.pushString(vkey);
        }
        return bs.getWrittenLength();
    }

    public int unSerialize(ByteStream bs) throws Exception
    {
        long size = bs.popUInt();
        int startPosPop = bs.getReadLength();
        if (size == 0)
            return 0;
        version = bs.popUInt();
        source = bs.popString();
        lskey = bs.popString();
        mobileKey = bs.popString();
        uin = bs.popUInt();
        loginAppId = bs.popUInt();
        ip = bs.popString();
        skey = bs.popString();
        sid = bs.popString();
        if(version >= 1)
        {
            statSid = bs.popString();
            sidFromCookie = bs.popString();
            sidFromQCookie = bs.popString();
            g_key = bs.popString();
            vkey = bs.popString();
        }

        /********************** 为了支持多个版本的客户端 ************************/
        int needPopBytes = (int) size - (bs.getReadLength() - startPosPop);
        for (int i = 0; i < needPopBytes; i++)
            bs.popByte();
        /********************** 为了支持多个版本的客户端 ************************/

        return bs.getReadLength();
    }

    /**
     * 获取版本本
     * 
     * 此字段的版本 >= 0
     * 
     * @return version value 类型为:long
     * 
     */
    public long getVersion()
    {
        return version;
    }

    /**
     * 设置版本本
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:long
     * 
     */
    public void setVersion(long value)
    {
        this.version = value;
    }

    /**
     * 获取来源
     * 
     * 此字段的版本 >= 0
     * 
     * @return source value 类型为:String
     * 
     */
    public String getSource()
    {
        return source;
    }

    /**
     * 设置来源
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:String
     * 
     */
    public void setSource(String value)
    {
        if (value != null)
        {
            this.source = value;
        } else
        {
            this.source = new String();
        }
    }

    /**
     * 获取登陆token
     * 
     * 此字段的版本 >= 0
     * 
     * @return lskey value 类型为:String
     * 
     */
    public String getLskey()
    {
        return lskey;
    }

    /**
     * 设置登陆token
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:String
     * 
     */
    public void setLskey(String value)
    {
        if (value != null)
        {
            this.lskey = value;
        } else
        {
            this.lskey = new String();
        }
    }

    /**
     * 获取机器标识
     * 
     * 此字段的版本 >= 0
     * 
     * @return mobileKey value 类型为:String
     * 
     */
    public String getMobileKey()
    {
        return mobileKey;
    }

    /**
     * 设置机器标识
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:String
     * 
     */
    public void setMobileKey(String value)
    {
        if (value != null)
        {
            this.mobileKey = value;
        } else
        {
            this.mobileKey = new String();
        }
    }

    /**
     * 获取用户uin
     * 
     * 此字段的版本 >= 0
     * 
     * @return uin value 类型为:long
     * 
     */
    public long getUin()
    {
        return uin;
    }

    /**
     * 设置用户uin
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:long
     * 
     */
    public void setUin(long value)
    {
        this.uin = value;
    }

    /**
     * 获取loginAppId
     * 
     * 此字段的版本 >= 0
     * 
     * @return loginAppId value 类型为:long
     * 
     */
    public long getLoginAppId()
    {
        return loginAppId;
    }

    /**
     * 设置loginAppId
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:long
     * 
     */
    public void setLoginAppId(long value)
    {
        this.loginAppId = value;
    }

    /**
     * 获取client ip
     * 
     * 此字段的版本 >= 0
     * 
     * @return ip value 类型为:String
     * 
     */
    public String getIp()
    {
        return ip;
    }

    /**
     * 设置client ip
     * 
     * 此字段的版本 >= 0
     * 
     * @param value
     *            类型为:String
     * 
     */
    public void setIp(String value)
    {
        if (value != null)
        {
            this.ip = value;
        } else
        {
            this.ip = new String();
        }
    }
    
    
    /**
     * 获取登陆skey
     * 
     * 此字段的版本 >= 0
     * @return skey value 类型为:String
     * 
     */
    public String getSkey()
    {
        return skey;
    }


    /**
     * 设置登陆skey
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:String
     * 
     */
    public void setSkey(String value)
    {
        if (value != null) {
                this.skey = value;
        }else{
                this.skey = new String();
        }
    }


    /**
     * 获取登陆sid
     * 
     * 此字段的版本 >= 0
     * @return sid value 类型为:String
     * 
     */
    public String getSid()
    {
        return sid;
    }


    /**
     * 设置登陆sid
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:String
     * 
     */
    public void setSid(String value)
    {
        if (value != null) {
                this.sid = value;
        }else{
                this.sid = new String();
        }
    }

    /**
     * 计算类长度 用于告诉解包者，该类只放了这么长的数据
     * 
     */
    protected int getClassSize()
    {
        int length = getSize() - 4;
        try
        {

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return length;
    }

    /**
     * 计算类长度 这个是该类的实际长度，在序列化时bytestream会调用这个方法
     * 
     */
    public int getSize()
    {
        int length = 4;
        try
        {
            length = 4; // size_of(QQBuyServiceComm)
            length += 4; // 计算字段version的长度 size_of(uint32_t)
            length += ByteStream.getObjectSize(source); // 计算字段source的长度
                                                        // size_of(String)
            length += ByteStream.getObjectSize(lskey); // 计算字段lskey的长度
                                                       // size_of(String)
            length += ByteStream.getObjectSize(mobileKey); // 计算字段mobileKey的长度
                                                           // size_of(String)
            length += 4; // 计算字段uin的长度 size_of(uint32_t)
            length += 4; // 计算字段loginAppId的长度 size_of(uint32_t)
            length += ByteStream.getObjectSize(ip); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(skey); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(sid); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(statSid); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(sidFromCookie); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(sidFromQCookie); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(g_key); // 计算字段ip的长度 size_of(String)
            length += ByteStream.getObjectSize(vkey); // 计算字段ip的长度 size_of(String)
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return length;
    }

    /**
     ********************以下信息是每个版本的字段********************
     */

    /**
     *下面是生成toString()方法 此方法用于调试时打开* 如果要打开此方法，请加入commons-lang-2.4.jar 并导入 import
     * org.apache.commons.lang.builder.ToStringBuilder; import
     * org.apache.commons.lang.builder.ToStringStyle;
     * 
     */
    @Override
    public String toString()
    {
        return "version=" + version + " source=" + source + " lskey=" + lskey
                + " mobileKey=" + mobileKey + " uin=" + uin + " loginAppId="
                + loginAppId + " ip=" + ip + " skey=" + skey + " sid=" + sid
                + " statSid=" + statSid
                + " sidFromCookie=" + sidFromCookie + " sidFromQCookie=" + sidFromQCookie
                + " g_key=" + g_key + " vkey=" + vkey + " retMsg:" + retMsg;
    }
}
