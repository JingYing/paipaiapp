package com.qq.qqbuy.common.po;

import java.util.Comparator;

import com.qq.qqbuy.common.util.StringUtil;


/**
 * 用于封装展示和真实传递的值，如用在下拉框中的值
 * @author winsonwu
 * @Created 2012-4-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Pair {
	
	private String name;
	private String code;
	
	public Pair(String name, String code) {
		this.name = name;
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	public static final Comparator<Pair> PAIR_CODE_INT_COMPARATOR = new Comparator<Pair>() {
    	@Override
    	public int compare(Pair o1, Pair o2) {
    		if (o1 == null && o2 == null) {
    			return 0;
    		}
    		if (o1 != null && o2 != null) {
    			int i1 = StringUtil.toInt(o1.getCode(), -1);
    			int i2 = StringUtil.toInt(o2.getCode(), -2);
    			return i1-i2;
    		}
    		return -1;
    	}
    };
    
    public static final Comparator<Pair> PAIR_NAME_STRING_COMPARATOR = new Comparator<Pair>() {
    	@Override
    	public int compare(Pair o1, Pair o2) {
    		if (o1 == null && o2 == null) {
    			return 0;
    		}
    		if (o1 != null && o2 != null) {
    			return o1.getName().compareTo(o2.getName());
    		}
    		return -1;
    	};
    };

	@Override
	public String toString() {
		return "Pair [code=" + code + ", name=" + name + "]";
	}
    
}