package com.qq.qqbuy.recvaddr.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.recvaddr.po.RecvAddr;


public interface RecvAddrBiz {
	
	/**
	 * 
	 * @author:tylerliu
	 * @date:2012-12-11
	 * @desc:查询收货地址列表
	 * @param:sk  //sk
	 * @param:mk  //machineKey
	 * @param:qq  //买家QQ
	 * @param:regionVersion	//	收货地址版本号 
	 * @return:RecvAddr 处理结果
	 */	
	public RecvAddr listRecvAddr(long qq, boolean needMask, boolean needName,String sk) throws BusinessException ;
	
	/**
	 * 罗列用户的收货地址
	 * @param qq	用户QQ
	 * @param needMask	是否打码
	 * @param needName	是否需要用户姓名，如果需要会在地址后方用括号附上
	 * @param regionVersion	regionVersion版本号，默认为0
	 * @return
	 * @throws BusinessException
	 * @author ethonchan
	 */
	public RecvAddr listRecvAddr(long qq, boolean needMask, boolean needName, int regionVersion,String sk) throws BusinessException ;
	/**
	 * 修改默认收货地址
	 * @param qq
	 * @param sk
	 * @param addressId
	 * @return
	 * @throws BusinessException
	 */
	public RecvAddr modifyDefaultRecvAddr(long qq, String sk, long addressId) throws BusinessException ;
	
	/**
	 * 罗列用户的收货地址
	 * @param qq	用户QQ
	 * @param needMask	是否打码
	 * @param needName	是否需要用户姓名，如果需要会在地址后方用括号附上
	 * @param regionVersion	regionVersion版本号，默认为0
	 * @return
	 * @throws BusinessException
	 * @author ethonchan
	 */
	public RecvAddr listRecvAddrV2(long qq, boolean needMask, boolean needName, int regionVersion,String sk) throws BusinessException ;
	
	public boolean addRecvAddr(long buyerUin,
			long regionId, String name, String address, String mobile,
			String phone, String postcode,String sk) throws BusinessException;
			
	public boolean modifyRecvAddr(long buyerUin,
			long addressId, long regionId, String name, String address,
			String mobile, String phone, String postcode,String sk) throws BusinessException;

//	public ReceiverAddress getRecvAddr(long buyerUin,long addressId,String sid,String lskey) throws BusinessException ;
	
	public boolean deleteRecvAddr(long buyerUin,long addressId,String sk) throws BusinessException ;
	public long getRecvAddrCount(long qq,String sk) throws BusinessException ;
}

