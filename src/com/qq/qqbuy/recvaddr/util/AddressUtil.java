package com.qq.qqbuy.recvaddr.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.common.util.region.RegionManager;
import com.qq.qqbuy.common.util.region.RegionManagerV2;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.thirdparty.idl.addr.protocol.ApiRecvaddr;




public class AddressUtil 
{
	public final static String OPFLAG_ADD = "add";
	public final static String OPFLAG_MODIFY = "modify";
	
    /**
     * 把地址转换成字符串供页面展示用。
     * 
     * @param address
     * @return e.g.
     *         广东深圳市南山区南海大道1079号数码大厦A座2层科技公司(曾小姐 12342980)
     */
    public static String address2String(ReceiverAddress addr, boolean needMask, boolean needName) {
    	if ( addr == null) {
    		return "";
    	}
    	String address = RegionManager.getAddressByRegionId(addr.regionId) + addr.address;
    	address = AntiXssHelper.htmlAttributeEncode(address);

    	String mobile;
    	String phone;
        if (needMask) {
            address = addMask2Address(address);

            mobile = addMask2MobilePhone(addr.mobile);

            phone = addMask2Phone(addr.phone);
        }else{
            mobile =addr.mobile;
            phone =addr.phone;
        }
    	
        String s = address;
        if (needName){
            s = address + "(" + addr.name + " "
            + (StringUtil.isEmpty(mobile) ? phone : mobile) + ")";
        }
        return s;
    }
    
    /**
     * 把收货地址转换成字符串供页面展示用
     * @param addr
     * @param needMask
     * @param needName
     * @return e.g.
     *         广东深圳市南山区南海大道1079号数码大厦A座2层科技公司(曾小姐 12342980)
     */
    public static String address2StringV2(ReceiverAddress addr, boolean needMask, boolean needName){
    	if ( addr == null) {
    		return "";
    	}
    	String address = RegionManagerV2.getAddressByRegionId(addr.regionId) + addr.address;
    	String mobile;
    	String phone;
        if (needMask) {
            address = addMask2Address(address);

            mobile = addMask2MobilePhone(addr.mobile);

            phone = addMask2Phone(addr.phone);
        }else{
            mobile =addr.mobile;
            phone =addr.phone;
        }
    	
        String s = address;
        if (needName){
            s = address + "(" + addr.name + " "
            + (StringUtil.isEmpty(mobile) ? phone : mobile) + ")";
        }
        return s;
    }
    
    /**
     * 地址打码
     * @param address
     * @return
     */
    public static String addMask2Address(String address) {
    	if(StringUtil.isEmpty(address)) {
    		return "";
    	}
    	String newAddress = "";
    	if(address.length() > 5) {
    		newAddress = address.substring(0, (address.length() - 4)) + "****";
    	} else {
    		newAddress = address;
    	}
    	return newAddress;
    }
    
    /**
     * 手机号码打码
     * @param mobilePhone
     * @return
     */
    public static String addMask2MobilePhone(String mobilePhone) {
    	if(StringUtil.isEmpty(mobilePhone)) {
    		return "";
    	}
    	String newMobile = "";
    	if(mobilePhone.length() >= 11) {
    		newMobile = mobilePhone.substring(0, 3) + "****" + mobilePhone.substring(7);
    	} else {
    		newMobile = mobilePhone;
    	}
    	return newMobile;
    	
    }
    
    /**
     * 座机号码打码
     * @param phoneNum
     * @return
     */
    public static String addMask2Phone(String phoneNum) {
    	if(StringUtil.isEmpty(phoneNum)) {
    		return "";
    	}
    	String newNum = "";
    	if(phoneNum.length() > 5) {
    		newNum = phoneNum.substring(0, (phoneNum.length() - 4)) + "****";
    	} else {
    		newNum = phoneNum;
    	}
    	return newNum;
    }

    /**
     * 屏蔽字符串中间几位以*的代替
     * @param strSrc 屏蔽的字符串号
     * @param from 屏蔽起始位置（从０开始）
     * @param end　屏蔽结束位置
     * @return
     */
    public static String maskString(String strSrc, int from, int end)
    {
        String reStr = "";
        if (strSrc == null || strSrc.length()==0 || from < 0 || from > end)
        {
            return strSrc;
        }

        // 当屏蔽结束位置大于等于字符串长度时，则只保留字符串头和尾中间全以*屏蔽
        if (end >= strSrc.length() - 1)
        {
            //头
            String _h = strSrc.substring(0, 1);
            //尾
            String _f = strSrc.substring(strSrc.length() - 1);
            //中间几位？
            int _c = strSrc.length() - 2;
            String _cc = "";
            for (int i = 0; i < _c; i++)
            {
                _cc += "*";
            }
            //拼装
            return _h + _cc + _f;
        }

        int len = end - from + 1;
        if (from != 0)
        {
            reStr += strSrc.substring(0, from);
        }
        do
        {
            reStr += "*";
            len--;
        }
        while (len > 0);
        reStr += strSrc.substring(end + 1, strSrc.length());

        return reStr;
    }
    
    /**
     * 屏蔽ＱＱ中间几位以*的代替
     * @param qq 屏蔽的QQ号
     * @return
     */
    public static String maskQQ(String qq)
    {
        return  AddressUtil.maskString(qq, 3, 5);
    }
    
    public static List<ReceiverAddress> idlAddr2PoAddr(Vector<ApiRecvaddr> apiAddrs) {
    	List<ReceiverAddress> addrs = new LinkedList<ReceiverAddress>();
    	if (apiAddrs != null) {
    		for (ApiRecvaddr apiAddr : apiAddrs) {
    			addrs.add(idlAddr2PoAddr(apiAddr));
    		}
    	}
    	return addrs;
    }
    
    public static ReceiverAddress idlAddr2PoAddr(ApiRecvaddr apiAddr) {
    	ReceiverAddress addr = new ReceiverAddress(apiAddr.getAddrId(), apiAddr.getRegionId(), apiAddr.getRecvName(),
				apiAddr.getRecvProvince(), apiAddr.getRecvCity(), apiAddr.getRecvDistric(), apiAddr.getRecvAddress(),
				apiAddr.getPostCode(), apiAddr.getRecvPhone(), apiAddr.getRecvMobile(), apiAddr.getUsedCount(),
				"" + apiAddr.getLastUsedTime(), "" + apiAddr.getLastModifyTime()); 
    	return addr;
    }
    
    public static void main(String[] args)
    {
        String qq = "30305461";
        String desSrc = AddressUtil.maskQQ(qq);
        System.out.println(qq + "/" + desSrc);
        
        qq = "12345";
        desSrc = AddressUtil.maskQQ(qq);
        System.out.println(qq + "/" + desSrc);
        
        qq = "1234567891";
        desSrc = AddressUtil.maskQQ(qq);
        System.out.println(qq + "/" + desSrc);
    }
}
