package com.qq.qqbuy.vb2c.action;

import java.util.List;

import com.paipai.util.string.StringUtil;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.action.PaipaiApiBaseAction;
import com.qq.qqbuy.common.constant.ErrConstant;

import com.qq.qqbuy.common.exception.BusinessException;

import com.qq.qqbuy.common.util.AntiXssHelper;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.vb2c.biz.Vb2cMobileBiz;
import com.qq.qqbuy.vb2c.po.MobileAmount;
import com.qq.qqbuy.vb2c.po.ValidateMobilePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cChecker;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class WMobileAction extends PaipaiApiBaseAction {

	private static final long serialVersionUID = -5789758627560817146L;
	
	public static final int FROM_TYPE_CHARGEBTN = 1;
	
	/************************* 输入参数 ****************************/
	private String m; //mobile,充值的手机号码
	private int am; //amount, 充值的面额，"以元为单位" 
	private String s; //充值来源
	private int from; // 页面点击来源：1=通过点击“立即充值”跳转来的。
	
	/************************* 输出参数 ***************************/
	private List<MobileAmount> amounts;
	
	private String tenpayUrl;
	
	// 临时变量
	private ValidateMobilePo validateMobilePo;
	
	/************************ 注入变量 ***************************/
	private Vb2cMobileBiz mobileBiz;
	
	protected boolean needsAlaram()
    {
    	return (result.getErrCode()!=0 && (result.getErrCode()!=ErrConstant.ERRCODE_INVALID_PARAMETER) && (result.getErrCode()!=ErrConstant.ERRCODE_BIZ_VB2C_MOBILE_SHORTAGE));
    }
	
	// 话费充值
	public String buy() {
		long start = System.currentTimeMillis();
		try {
			String backUrl = getActionPath("/w/vb2c/buyMobile.xhtml", "w", "m", m, "am", am, "from", from, "s", s); 
			boolean validateRet = validateMDetailInput(m, am);
			//
			if (from == 1) {
				// 通过立即充值来的
				if (validateRet) {
					//处理登录态问题
					String checkResult = checkLogin(backUrl);
					if(checkResult != null){
						return checkResult;
					}					
					validateMobilePo.setSid(getSid());
					validateMobilePo.setMkey(getMachineKey());
					validateMobilePo.setSkey(getSk());
					validateMobilePo.setUin(getQq());
					
					//处理下单逻辑
					try {
						String cftCommonAttach = TenpayVb2cUtil.genWap2Vb2cCommonCtfAttach(getSid(), getPgid(), getIcfa(), getO_icfa(), getGcfa());
						
						Vb2cOrderDealOutPo po = mobileBiz.orderDeal(VersionType.VERSION_WAP2, validateMobilePo, getQq(), s, cftCommonAttach);
						tenpayUrl = po.getTenpayUrl();
						return "cft-pay";
					}
					catch (BusinessException e) 
					{
						result.setErrCode(e.getErrCode());
						result.setMsg(e.getErrMsg());
						Log.run.warn("vb2c mobile order deal get Token fail!");
					}
					amounts = MobileAmount.getMobileAmountOptions();
					return "mobile-detail";
				}
			}
			
			// 初始化话费充值页面的展示数据
			amounts = MobileAmount.getMobileAmountOptions();
			getLoginInfo(backUrl);
			return "mobile-detail";
		}
		catch (Exception e) {
			result.setErrCode(ErrConstant.ERRCODE_VB2C_CALL_IDL_FAIL);
			result.setMsg("Wap2.0话费充值出现异常");
			Log.run.warn("Wap2.0话费充值出现异常", e);
			return "mobile-detail";
		} finally {
			result.setDtag(dtag);
			long timeCost = System.currentTimeMillis() - start;
		}
	}
	
	/********************************************************** Validate输入参数 *******************************************************************/
	protected boolean validateMDetailInput(String mobile, int amount) {
		if (StringUtil.isEmpty(mobile) && amount == 0) {
			return true; //不带参数，代表刚刚进入话费充值页面。
		}
		
		validateMobilePo = Vb2cChecker.checkMobileAmount(result, mobile, amount);
		if (result.getErrCode() != 0) {
			return false;
		}
		
		return true;
	}
	
	/********************************************************** 和Jsp页面交互的变量 ****************************************************************/
	public List<MobileAmount> getAmounts() {
		return amounts;
	}
	
	public String getTenpayUrl() {
		return tenpayUrl;
	}
	
	/************************************************************ Getter And Setter Method ********************************************************/
	public String getM() {
		return m;
	}

	public void setM(String m) {
		this.m = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(m));
	}

	public int getAm() {
		return am;
	}

	public void setAm(int am) {
		this.am = am;
	}

	public Vb2cMobileBiz getMobileBiz() {
		return mobileBiz;
	}

	public void setMobileBiz(Vb2cMobileBiz mobileBiz) {
		this.mobileBiz = mobileBiz;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = CoderUtil.decodeWML(AntiXssHelper.xmlAttributeEncode(s));
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

}
