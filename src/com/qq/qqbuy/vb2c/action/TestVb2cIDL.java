package com.qq.qqbuy.vb2c.action;

import com.qq.qqbuy.common.util.JSONUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSubmitResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameItemResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.MobileSubmitResp;

/**
 * @author winsonwu
 * @Created 2012-6-1 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TestVb2cIDL
{

    private static String vb2cTag = "mWgTest";
    private static boolean isShowContent = false;

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // QQ号：2292983637 密码：qgo123456
        // 查询网游产品信息
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 1 index
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 1 0
        // 查询网游商品信息
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 2 productId
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 2
        // C269EC1E0000000000000000000F730A (1元面额的Q币充值)
        // 话费充值下单
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 3 buyerUin mobile
        // amount machineKey skey uin
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 3 381936231
        // 15818691951 1000 112a9e6bcbbb18127 @4077d36e@ 381936231
        // 网游充值下单
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 4 uin buyCount
        // account index machineKey skey
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 4 381936231 2
        // 381936231 0 112a9e6bcbbb18127 @4077d36e@
        // 查询虚拟订单详情
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 5 buyerUin dealId
        // machineKey skey uin
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 5 381936231
        // '1012762941|20120605|1190531581' 112a9e6bcbbb18127 @4077d36e@
        // 381936231 //话费
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 5 381936231
        // '408345088|20120605|1190516869' 112a9e6bcbbb18127 @4077d36e@
        // 381936231 //七雄争霸
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 5 381936231
        // '1210187501201207031000129635' 112a9e6bcbbb18127 @4077d36e@ 381936231
        // //DEV
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 5 381936231
        // '1210187501201207041209309883' 112a9e6bcbbb18127 @4077d36e@ 381936231
        // //GAMMA

        // 查询虚拟订单列表
        // java -Dfile.encoding=utf-8 -jar testVb2cIDL.jar 6 381936231 1 5
        // 112a9e6bcbbb18127 @4077d36e@ 1 1

        // args = new String[]{"1", "0", "1"}; //查询网购产品信息
        // args = new String[]{"2", "C269EC1E0000000000000000000F730A", "1"};
        // //查询网游商品信息
        // args = new String[]{"3", "93889065", "15014082808", "1000",
        // "113918bf06ec624f2", "@ef09bfcc@", "93889065", "1"}; //话费充值下单
        args = new String[]
        { "3", "381936231", "15818691951", "1000", "112a9e6bcbbb18127",
                "@4077d36e@", "381936231", "1" }; // 话费充值下单
        // args = new String[]{"4", "381936231", "2", "381936231", "0",
        // "112a9e6bcbbb18127", "@4077d36e@", "1"}; //网游充值下单
        // args = new String[]{"5", "381936231",
        // "408345088|20120605|1190516869", "112a9e6bcbbb18127", "@4077d36e@",
        // "381936231", "1"};
        // args = new String[]{"6", "381936231", "1", "5", "112a9e6bcbbb18127",
        // "@4077d36e@", "381936231", "1", "1"};

        // System.getProperties().put("qgo.cfg.env", "local");
        System.getProperties().put("qgo.cfg.env", "dev");
        int type = Integer.parseInt(args[0]);
        try
        {
            isShowContent = Integer.parseInt(args[args.length - 1]) == 1;
        } catch (Exception e)
        {
            System.out.println("isShowContent set fail!");
        }
        switch (type)
        {
        case 1:
            queryGameProductInfo(args);
            break;
        case 2:
            queryGameItemInfo(args);
            break;
        case 3:
            mobileOrderDeal(args);
            break;
        // case 4:
        // gameOrderDeal(args);
        // break;
        case 5:
            queryDealInfo(args);
            break;
        case 6:
            queryDealList(args);
            break;
        }
        System.out.println("Done!");
    }

    private static void queryDealList(String[] args)
    {
        long buyerUin = StringUtil.toLong(args[1], 0);
        int pn = StringUtil.toInt(args[2], 1);
        int ps = StringUtil.toInt(args[3], 5);
        String mkey = args[4];
        String skey = args[5];
        GetDealListExResp resp = Vb2cClient.queryDealList(buyerUin, pn, ps, 0,
                mkey, skey, "");
        System.out.println(JSONUtil.getJsonStr(resp));
    }

    private static void queryGameProductInfo(String[] args)
    {
        String[] gameNames = new String[]
        { "七雄争霸", "盛大点券", "征途", "魔兽世界", "穿越火线", "劲舞团", "诛仙前传", "跑跑卡丁车",
                "天龙八部3", "网易一卡通", "地下城与勇士", "完美世界国际版" };
        String gameName = gameNames[Integer.parseInt(args[1])];
        long start = System.currentTimeMillis();
        GetGameProductResp resp = Vb2cClient.queryGameProductInfo(gameName);
        System.out.println("TimeCost：" + (System.currentTimeMillis() - start));
        if (isShowContent)
        {
            System.out.println(JSONUtil.getJsonStr(resp));
        }
    }

    private static void queryGameItemInfo(String[] args)
    {
        String productId = args[1];
        long start = System.currentTimeMillis();
        GetGameItemResp gameItemResp = Vb2cClient.queryGameItemInfo(productId);
        System.out.println("TimeCost：" + (System.currentTimeMillis() - start));
        if (isShowContent)
        {
            System.out.println(JSONUtil.getJsonStr(gameItemResp));
        }
    }

    private static void mobileOrderDeal(String[] args)
    {
        long buyerUin = Long.parseLong(args[1]);
        String mobile = args[2];
        long amount = Long.parseLong(args[3]);
        String machineKey = args[4];
        String skey = args[5];
        long uin = Long.parseLong(args[6]);
        long start = System.currentTimeMillis();
        MobileSubmitResp mobileSubmitResp = Vb2cClient.mobileOrderDeal(
                buyerUin, mobile, amount, vb2cTag, machineKey, skey, uin);
        System.out.println("TimeCost：" + (System.currentTimeMillis() - start)
                + "result:" + mobileSubmitResp.getResult());
        if (isShowContent)
        {
            System.out.println(JSONUtil.getJsonStr(mobileSubmitResp));
        }
    }

    // private static void gameOrderDeal(String[] args) {
    // long uin = Long.parseLong(args[1]);
    // long buyCount = Long.parseLong(args[2]);
    // String account = args[3];
    // GameOrderDeal currGod =
    // GameOrderDeal.getGameOrderDealByIndex(Integer.parseInt(args[4]));
    // String machineKey = args[5];
    // String skey = args[6];
    // long start = System.currentTimeMillis();
    // GameSubmitResp gameSubmitResp = Vb2cClient.gameOrderDeal(uin,
    // currGod.itemId, buyCount, currGod.sectionName, currGod.sectionCode,
    // currGod.serverName,
    // currGod.serverCode, account, vb2cTag, machineKey, skey, uin);
    // System.out.println("TimeCost：" + (System.currentTimeMillis() - start));
    // if (isShowContent) {
    // System.out.println(JSONUtil.getJsonStr(gameSubmitResp));
    // }
    // }

    private static void queryDealInfo(String[] args)
    {
        long buyerUin = Long.parseLong(args[1]);
        String dealId = args[2];
        String machineKey = args[3];
        String skey = args[4];
        long uin = Long.parseLong(args[5]);
        long start = System.currentTimeMillis();
        GetDealInfoResp dealInfoResp = Vb2cClient.queryDealInfo(buyerUin,
                dealId, machineKey, skey, uin);
        System.out.println("TimeCost：" + (System.currentTimeMillis() - start));
        if (isShowContent)
        {
            System.out.println(JSONUtil.getJsonStr(dealInfoResp));
        }
    }

}
