package com.qq.qqbuy.vb2c.po;

/**
 * @author winsonwu
 * @Created 2012-6-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ValidateMobilePo extends ValidateVb2cPo {

	private String mobile; 
	private int amountByYuan;
	
	private MobileAmount mobileAmount;
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getAmountByYuan() {
		return amountByYuan;
	}

	public void setAmountByYuan(int amountByYuan) {
		this.amountByYuan = amountByYuan;
	}

	public MobileAmount getMobileAmount() {
		return mobileAmount;
	}

	public void setMobileAmount(MobileAmount mobileAmount) {
		this.mobileAmount = mobileAmount;
	}
	
}
