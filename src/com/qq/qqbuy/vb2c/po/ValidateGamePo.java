package com.qq.qqbuy.vb2c.po;

import java.util.List;

import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.Product;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.SectionServer;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.Server;

/**
 * 校验网游所有参数后的结果，缓存校验过程中的临时结果
 * 检查gameId的时候，currGame、products、sectionServers会被赋值
 * 检查amount、chargeType的时候，currProduct会被赋值
 * 检查buyCount的时候，无赋值
 * 检查商品ProductId的时候，itemId、itemPrice会被赋值
 * 检查sectionCode的时候，currSectionServer、currSectionName会被赋值
 * 检查serverCode的时候，currServer、currServerName会被复制
 * @author winsonwu
 * @Created 2012-5-24
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class ValidateGamePo extends ValidateVb2cPo {
	
	private int gameId;
	private int amount;
	private String chargeType;
	private int buyCount;
	private String sectionCode;
	private String serverCode;
	
	private OnlineGame currGame;
	private List<Product> products;
	private List<SectionServer> sectionServers;
	
	private Product currProduct;
	private SectionServer currSectionServer;
	private String currSectionName;
	
	private Server currServer;
	private String currServerName;
	
	private String itemId;
	private int itemPrice;

	public ValidateGamePo(int gameId) {
		this.gameId = gameId;
	}
	
	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	public int getBuyCount() {
		return buyCount;
	}

	public void setBuyCount(int buyCount) {
		this.buyCount = buyCount;
	}

	public OnlineGame getCurrGame() {
		return currGame;
	}

	public void setCurrGame(OnlineGame currGame) {
		this.currGame = currGame;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<SectionServer> getSectionServers() {
		return sectionServers;
	}

	public void setSectionServers(List<SectionServer> sectionServers) {
		this.sectionServers = sectionServers;
	}

	public Product getCurrProduct() {
		return currProduct;
	}

	public void setCurrProduct(Product currProduct) {
		this.currProduct = currProduct;
	}

	public SectionServer getCurrSectionServer() {
		return currSectionServer;
	}

	public void setCurrSectionServer(SectionServer currSectionServer) {
		this.currSectionServer = currSectionServer;
	}

	public Server getCurrServer() {
		return currServer;
	}

	public void setCurrServer(Server currServer) {
		this.currServer = currServer;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getSectionCode() {
		return sectionCode;
	}

	public void setSectionCode(String sectionCode) {
		this.sectionCode = sectionCode;
	}

	public String getServerCode() {
		return serverCode;
	}

	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}

	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	public String getCurrSectionName() {
		return currSectionName;
	}

	public void setCurrSectionName(String currSectionName) {
		this.currSectionName = currSectionName;
	}

	public String getCurrServerName() {
		return currServerName;
	}

	public void setCurrServerName(String currServerName) {
		this.currServerName = currServerName;
	}

	@Override
	public String toString() {
		return "ValidateGamePo [amount=" + amount + ", buyCount=" + buyCount
				+ ", chargeType=" + chargeType + ", currGame=" + currGame
				+ ", currProduct=" + currProduct + ", currSectionName="
				+ currSectionName + ", currSectionServer=" + currSectionServer
				+ ", currServer=" + currServer + ", currServerName="
				+ currServerName + ", gameId=" + gameId + ", itemId=" + itemId
				+ ", itemPrice=" + itemPrice + ", products=" + products
				+ ", sectionCode=" + sectionCode + ", sectionServers="
				+ sectionServers + ", serverCode=" + serverCode + "]";
	}
	
}
