package com.qq.qqbuy.vb2c.po;

import java.util.List;

/**
 * 用于在Json生成的时候设置格式
 * 
 * @author winsonwu
 * @Created 2011-10-21
 */
public class OnlineGameList
{
    private String firstLetter;
    private List<OnlineGame> gameList;

    public OnlineGameList(String firstLetter, List<OnlineGame> gameList)
    {
        this.firstLetter = firstLetter;
        this.gameList = gameList;
    }

    public String getFirstLetter()
    {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter)
    {
        this.firstLetter = firstLetter;
    }

    public List<OnlineGame> getGameList()
    {
        return gameList;
    }

    public void setGameList(List<OnlineGame> gameList)
    {
        this.gameList = gameList;
    }

}
