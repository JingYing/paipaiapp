package com.qq.qqbuy.vb2c.po;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @ClassName: Vb2cDealInfos 
 * @Description: 虚拟订单列表
 * @author wendyhu 
 * @date 2013-4-15 上午10:01:07
 */
public class Vb2cDealInfos
{

    /**
     * 订单总数
     * 
     * 版本 >= 0
     */
    private long totalNum;

    /**
     * 订单数据
     */
    List<Vb2cDealInfo> items = new ArrayList<Vb2cDealInfo>();

    public long getTotalNum()
    {
        return totalNum;
    }

    public void setTotalNum(long totalNum)
    {
        this.totalNum = totalNum;
    }

    public List<Vb2cDealInfo> getItems()
    {
        return items;
    }

    public void setItems(List<Vb2cDealInfo> items)
    {
        this.items = items;
    }

}
