package com.qq.qqbuy.vb2c.util;

/**
 * 用于区分访问webapp_qqbuy接口的不同来源
 * @author winsonwu
 * @Created 2012-6-14
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public enum VersionType {

	VERSION_UNKNOWK(0, "Unknow", ""),
	VERSION_ANDROID(1, "Android", "api"),
	VERSION_IOS(2, "Ios", "api"),
	VERSION_WAP2(3, "Wap2", "w"),
	VERSION_TOUCH(4, "Touch", "t"),
	VERSION_WAP1(5, "Wap1", "w");
	
	private static VersionType[] types = new VersionType[] {VERSION_ANDROID, VERSION_IOS, VERSION_WAP2, VERSION_TOUCH, VERSION_WAP1};

	private int type;
	private String name;
	private String dispType;  //转发Action的type字段的值
	
	private VersionType(int type, String name, String searchDtag) {
		this.type = type;
		this.name = name;
		this.dispType = searchDtag;
	}
	
	public static VersionType find(int type) {
		for (VersionType tp : types) {
			if (type == tp.getType()) {
				return tp;
			}
		}
		return VERSION_UNKNOWK;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDispType() {
		return dispType;
	}

	public void setDispType(String dispType) {
		this.dispType = dispType;
	}

}
