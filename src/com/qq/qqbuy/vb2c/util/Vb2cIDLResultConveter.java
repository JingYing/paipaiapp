package com.qq.qqbuy.vb2c.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfoList;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductByAmount;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductList;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSectionServer;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSectionServerList;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExRespBo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductResp;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfo;
import com.qq.qqbuy.vb2c.po.game.Product;
import com.qq.qqbuy.vb2c.po.game.ProductInfo;
import com.qq.qqbuy.vb2c.po.game.Section;
import com.qq.qqbuy.vb2c.po.game.SectionServer;
import com.qq.qqbuy.vb2c.po.game.Server;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.Vector;
//import java.util.Map.Entry;
//
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductByAmount;
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductInfo;
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameProductList;
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSectionServer;
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSectionServerList;
//import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductResp;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.Product;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.ProductInfo;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.Section;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.SectionServer;
//import com.qq.qqbuy.thirdparty.openapi.protocol.GetGameProductResponse.Server;

/**
 * 将VB2C部分的IDL接口返回数据结构转换成类似之前OpenAPI的数据结构。
 * 
 * @author winsonwu
 * @Created 2012-6-6 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cIDLResultConveter
{

    public static List<Vb2cDealInfo> convert(GetDealListExRespBo resp)
    {
        List<Vb2cDealInfo> ret = new ArrayList<Vb2cDealInfo>();
        if (resp != null)
        {
            DealInfoList dealList = resp.getDealList();
            if (dealList != null && dealList.getDealInfoList() != null
                    && dealList.getDealInfoList().size() > 0)
            {
                for (DealInfo deal : dealList.getDealInfoList())
                {
                    Vb2cDealInfo po = new Vb2cDealInfo();
                    po.setDealGenTime(deal.getDealGenTime());
                    po.setDealId(deal.getDealId());
                    po.setDealState(deal.getDealState());
                    po.setDealStateDesc(deal.getDealStateDesc());
                    po.setDealType(deal.getDealType());
                    po.setItemName(deal.getItemName());
                    po.setNum(deal.getNum());
                    po.setPayFee(deal.getPayFee());
                    po.setSellerName(deal.getSellerName());
                    po.setSellerSpid(deal.getSellerSpid());
                    po.setSvrPhone(deal.getSvrPhone());
                    Map<String, String> extInfos = deal.getExtInfo();
                    String account = "";
                    if (extInfos != null)
                    {
                        account = extInfos.get("Account");
                        if (StringUtils.isNotBlank(account))
                            po.setAccount(account);
                        long faceValue = StringUtil.toLong(extInfos
                                .get("FaceValue"), 0);
                        if (faceValue > 0)
                            po.setFee(faceValue);
                    }
                    ret.add(po);
                }
            }
        }

        return ret;
    }

    public static Vb2cDealInfo convert(DealInfo deal)
    {
        Vb2cDealInfo po = new Vb2cDealInfo();
        if (deal != null)
        {
            po.setDealGenTime(deal.getDealGenTime());
            po.setDealId(deal.getDealId());
            po.setDealState(deal.getDealState());
            po.setDealStateDesc(deal.getDealStateDesc());
            po.setDealType(deal.getDealType());
            po.setItemName(deal.getItemName());
            po.setNum(deal.getNum());
            po.setPayFee(deal.getPayFee());
            po.setSellerName(deal.getSellerName());
            po.setSellerSpid(deal.getSellerSpid());
            po.setSvrPhone(deal.getSvrPhone());
            Map<String, String> extInfos = deal.getExtInfo();
            String account = "";
            if (extInfos != null)
            {
                account = extInfos.get("Account");
                if (StringUtils.isNotBlank(account))
                    po.setAccount(account);
                long faceValue = StringUtil
                        .toLong(extInfos.get("FaceValue"), 0);
                if (faceValue > 0)
                    po.setFee(faceValue);
            }
        }

        return po;
    }

    public static List<Product> getGameProductList(
            GetGameProductResp gameProductResp)
    {
        if (gameProductResp != null)
        {
            GameProductList pdtList = gameProductResp.getGamePdtList();
            if (pdtList != null)
            {
                Vector<GameProductByAmount> products = pdtList
                        .getGameProductList();
                return convertProducts(products);
            }
        }

        return new ArrayList<Product>();
    }

    public static List<SectionServer> getGameSectionServer(
            GetGameProductResp gameProductResp)
    {
        if (gameProductResp != null)
        {
            GameSectionServerList gssList = gameProductResp.getGssList();
            if (gssList != null)
            {
                Vector<GameSectionServer> sectionServers = gssList.getGssList();
                return convertSectionServers(sectionServers);
            }
        }
        return new ArrayList<SectionServer>();
    }

    // GameProductByAmount是多个一个面额的聚拢。
    private static List<Product> convertProducts(
            Vector<GameProductByAmount> products)
    {
        List<Product> pos = new ArrayList<Product>();
        if (products != null && products.size() > 0)
        {
            for (GameProductByAmount product : products)
            {
                int amount = (int) product.getAmount();
                Vector<GameProductInfo> productInfos = product.getPdtInfo();
                if (productInfos != null && productInfos.size() > 0)
                {
                    for (GameProductInfo productInfo : productInfos)
                    {
                        Product po = new Product();
                        po.setAmount(amount);
                        ProductInfo infoPo = new ProductInfo();
                        infoPo.setChargeAce(productInfo.getChargeAce());
                        infoPo.setChargeNum(convertChargeNum(productInfo
                                .getChargeNum()));
                        infoPo.setChargeType(productInfo.getChargeType());
                        infoPo.setProductId(productInfo.getProductId());
                        infoPo.setProductName(productInfo.getProductName());
                        infoPo.setProductPic(productInfo.getProductPic());
                        po.setProductInfo(infoPo);
                        pos.add(po);
                    }
                }
            }
        }

        return pos;
    }

    private static List<SectionServer> convertSectionServers(
            Vector<GameSectionServer> sectionServers)
    {
        List<SectionServer> pos = new ArrayList<SectionServer>();
        if (sectionServers != null && sectionServers.size() > 0)
        {
            for (GameSectionServer sectionServer : sectionServers)
            {
                SectionServer po = new SectionServer();
                Section sectionPo = new Section(sectionServer.getSectionName(),
                        sectionServer.getSectionCode());
                po.setSection(sectionPo);
                Map<String, String> serverMap = sectionServer.getServerList();
                if (serverMap != null && serverMap.size() > 0)
                {
                    List<Server> serverPos = new ArrayList<Server>();
                    for (Entry<String, String> serverEntry : serverMap
                            .entrySet())
                    {
                        Server serverPo = new Server(serverEntry.getKey(),
                                serverEntry.getValue());
                        serverPos.add(serverPo);
                    }
                    po.setServerList(serverPos);
                }
                pos.add(po);
            }
        }

        return pos;
    }

    private static List<Integer> convertChargeNum(String chargeNum)
    {
        List<Integer> ret = new ArrayList<Integer>();
        if (!StringUtil.isEmpty(chargeNum))
        {
            String[] strs = chargeNum.split(",");
            for (String str : strs)
            {
                ret.add(StringUtil.toInt(str.trim(), 0));
            }
        }
        return ret;
    }

}
