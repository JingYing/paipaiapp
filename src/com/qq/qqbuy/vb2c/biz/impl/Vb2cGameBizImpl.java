package com.qq.qqbuy.vb2c.biz.impl;

import java.util.List;
import java.util.Map;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.ErrConstant;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.po.Pair;
import com.qq.qqbuy.common.util.CoderUtil;
import com.qq.qqbuy.item.util.DispFormater;
import com.qq.qqbuy.thirdparty.idl.vb2c.Vb2cClient;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.DealInfo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSubmitResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.vb2c.biz.TenpayVb2cBiz;
import com.qq.qqbuy.vb2c.biz.Vb2cGameBiz;
import com.qq.qqbuy.vb2c.po.OnlineGame;
import com.qq.qqbuy.vb2c.po.TenpayPo;
import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.WGameDetailOutPo;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cUtil;
import com.qq.qqbuy.vb2c.util.VersionType;
import com.qq.qqbuy.vb2c.util.WGameDetailIndexer;

/**
 * @author winsonwu
 * @Created 2012-4-25
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cGameBizImpl implements Vb2cGameBiz {
	
	/*********************** 注入变量 ************************/
	private TenpayVb2cBiz tenpayVb2cBiz;
	
	public Vb2cOrderDealOutPo orderDeal(VersionType type, ValidateGamePo validateGamePo, long qq, String account, String s, String commonCftAttach) throws BusinessException {
		Vb2cOrderDealOutPo po = new Vb2cOrderDealOutPo();
		
		int gameId = validateGamePo.getGameId();
		int amount = validateGamePo.getAmount();
		String chargeType = validateGamePo.getChargeType();
		int buyCount = validateGamePo.getBuyCount();
		OnlineGame game = validateGamePo.getCurrGame();
		String gameItemName = game == null ? "" : game.getItemName(); 
		
		String sectionCode = validateGamePo.getSectionCode();
		String serverCode = validateGamePo.getServerCode();
		String tSectionName = validateGamePo.getCurrSectionName();
		String tServerName = validateGamePo.getCurrServerName();
		String itemId = validateGamePo.getItemId();
		
		String sid = validateGamePo.getSid();
		String mkey = validateGamePo.getMkey();
		String skey = validateGamePo.getSkey();
		long uin = validateGamePo.getUin();

		// 调IDL接口下单
		GameSubmitResp gameSubmitResp = Vb2cClient.gameOrderDeal(uin, itemId, buyCount, tSectionName, sectionCode, tServerName, 
				serverCode, account, s, mkey, skey, uin);
		if (gameSubmitResp == null || gameSubmitResp.getResult() != 0 || gameSubmitResp.getOrderResult() == null) {
			String msg = gameSubmitResp == null ? "GameSubmitResp为null" : "" + gameSubmitResp.getResult();
			//throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GAME_ORDER_DEAL_FAIL, "下单失败，请重试[" + msg + "]");
		}
//        // 调OpenAPI下单
//        ChargeGameResponse chargeGameResp = OpenApi.getProxy()
//                .onlineGameOrderDeal(qq, itemId, buyCount, account, tSectionName,
//                        sectionCode, tServerName, serverCode, s);
//        if (chargeGameResp == null || !chargeGameResp.isSucceed())
//        {
//        	String msg = OpenApi.genOpenApiErrorMsg(chargeGameResp);
//        	throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_GAME_ORDER_DEAL_FAIL, "下单失败，请重试[" + msg + "]");
//        }

//		// 调IDL接口获取订单详情
		GetDealInfoResp dealInfoResp = Vb2cClient.queryDealInfo(uin, gameSubmitResp.getOrderResult().getDealId(), mkey, skey, uin);
		if (dealInfoResp == null || dealInfoResp.getResult() != 0 || dealInfoResp.getDealInfo() == null) {
			String msg = dealInfoResp == null ? "DealInfoResp为null" : "" + dealInfoResp.getResult();
			//throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL, "获取下单详情失败，请重试[" + msg + "]");
		}
		DealInfo dealInfo = dealInfoResp.getDealInfo();
		
//        // 获取订单详情
//        GetBuyerDRDealDetailResponse dealDetailResp = OpenApi.getProxy().queryBuyerDirectRechargeDealDetail(qq, chargeGameResp.dealId);
//        if (dealDetailResp == null || !dealDetailResp.isSucceed())
//        {
//        	String msg = OpenApi.genOpenApiErrorMsg(dealDetailResp);
//        	throw BusinessException.createInstance(ErrConstant.ERRCODE_BIZ_VB2C_QUERY_DEAL_DETAIL_FAIL, "获取下单详情失败，请重试[" + msg + "]");
//        }
        
        Map<String, String> reqParam = TenpayVb2cUtil.genParamter4Game(type, /*dealDetailResp.deal*/dealInfo, commonCftAttach, uin, amount, 
        		gameId, buyCount, account, gameItemName, chargeType, tSectionName, tServerName, sectionCode, serverCode);
        TenpayPo tenpayPo = tenpayVb2cBiz.genTenPayUrl(reqParam, sid);
        
        po.setTenpayUrl(tenpayPo.getTenpayUrl());
		po.setLoginBargainorId(ConfigHelper.getStringByKey(PropertiesConstant.VB2C_LOGIN_BARGAINOR_ID, ""));
		po.setTokenId(tenpayPo.getTokenId());
		po.setTotalFee(/*dealDetailResp.deal.payFee*/(int)dealInfo.getPayFee());
		return po;
	}
	
	public WGameDetailOutPo wap2detail(ValidateGamePo gamePo, int currIndex, String chargeType, String account, String confirmAccount, String wp, String wp1) {
		WGameDetailOutPo gameDetailPo = new WGameDetailOutPo();
		gameDetailPo.setGameInfo(gamePo);
		List<Pair> options = Vb2cUtil.genWap2GameDetailOptions(currIndex, gamePo);
		Log.run.debug("options:" + options.toString());
		gameDetailPo.setOptions(options);
		gameDetailPo.setNextIndex(WGameDetailIndexer.toNext(currIndex, gamePo));
		gameDetailPo.setPreIndex(WGameDetailIndexer.toPre(currIndex, gamePo));
		gameDetailPo.setEncodeChargeType(CoderUtil.encodeUtf8(chargeType));
		gameDetailPo.setEncodeAccount(CoderUtil.encodeUtf8(account));
		gameDetailPo.setEncodeConfirmAccount(CoderUtil.encodeUtf8(confirmAccount));
		gameDetailPo.setEncodewp(CoderUtil.encodeUtf8(wp));
		gameDetailPo.setEncodewp1(CoderUtil.encodeUtf8(wp1));
		String totalFee = DispFormater.priceDispFormater(gamePo.getItemPrice() * gamePo.getBuyCount());
		gameDetailPo.setTotalFee(totalFee);
		return gameDetailPo;
	}
	
	public TenpayVb2cBiz getTenpayVb2cBiz() {
		return tenpayVb2cBiz;
	}

	public void setTenpayVb2cBiz(TenpayVb2cBiz tenpayVb2cBiz) {
		this.tenpayVb2cBiz = tenpayVb2cBiz;
	}
	
}
