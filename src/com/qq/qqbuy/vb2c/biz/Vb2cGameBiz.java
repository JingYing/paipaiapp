package com.qq.qqbuy.vb2c.biz;

import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.vb2c.po.ValidateGamePo;
import com.qq.qqbuy.vb2c.po.Vb2cOrderDealOutPo;
import com.qq.qqbuy.vb2c.po.WGameDetailOutPo;
import com.qq.qqbuy.vb2c.util.VersionType;

/**
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public interface Vb2cGameBiz {
	
	/**
	 * 网游下单逻辑
	 * @param type
	 * @param validateGameProductPo
	 * @param account
	 * @param s
	 * @param commonCftAttach
	 * @return
	 * @throws BusinessException
	 */
	public Vb2cOrderDealOutPo orderDeal(VersionType type, ValidateGamePo validateGamePo, long qq, String account, String s, String commonCftAttach) throws BusinessException;
	
	/**
	 * 生成Wap2.0页面详情页面所需要的数据结构
	 * @param gamePo
	 * @param currIndex
	 * @param chargeType
	 * @param account
	 * @param confirmAccount
	 * @return
	 */
	public WGameDetailOutPo wap2detail(ValidateGamePo gamePo, int currIndex, String chargeType, String account, String confirmAccount, String wp, String wp1);
	
}
