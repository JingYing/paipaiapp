package com.qq.qqbuy.vb2c.biz;

import com.qq.qqbuy.tenpay.biz.impl.PayTokenInfo;
import com.qq.qqbuy.vb2c.po.MobileLocationPo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfo;
import com.qq.qqbuy.vb2c.po.Vb2cDealInfos;

/**
 * 
 * @ClassName: Vb2cQueryDealBiz
 * @Description: 查询虚拟订单
 * 
 * @author wendyhu
 * @date 2013-4-12 下午04:05:06
 */
public interface Vb2cDealBiz
{

    /**
     * 
     * @Title: getDealList
     * @Description: 查询虚拟订单列表
     * @param buyerUin
     *            用户qq号码
     * @param pn
     *            起始页 从1开始
     * @param ps
     *            每页的大小，默认为50
     * @param state
     *            获取订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
     * @param mkey
     *            机器码
     * @param skey
     *            登陆skey
     * @param dealType
     *            查询的订单类型 1是话费，3是网游
     * @return 设定文件
     * @return List<Vb2cDealInfo> 返回类型
     * @throws
     */
    public Vb2cDealInfos getDealList(long buyerUin, int pn, int ps, int state,
            String mkey, String skey, String dealType);

    /**
     * 
     * @Title: getDealItem
     * @Description: 查询订单详情
     * @param buyerUin
     *            买家UIN
     * @param dealId
     *            订单id
     * @param mkey
     *            机器码
     * @param skey
     *            登陆态
     * @param uin
     *            操作者
     * @return 设定文件
     * @return Vb2cDealInfo 返回类型
     * @throws
     */
    public Vb2cDealInfo getDealItem(long buyerUin, String dealId, String mkey,
            String skey, long uin);


    /***
     * 
     * @Title: getToken 
     * @Description: TODO(这里用一句话描述这个方法的作用) 
     * @param buyerUin
     * @param dealId
     * @param spid
     * @param payFee
     * @param account
     * @param fee
     * @return    设定文件 
     * @return PayTokenInfo    返回类型 
     * @throws
     */
    public PayTokenInfo getToken(long buyerUin, String dealId, String mkey,
            String skey);
    
    
    /**
     * 
     * @Title: getMobileLocation
     *
     * @Description: 获取手机号归属信息
     * @param @param mo 手机号
     * @param @return  设定文件
     * @return MobileLocationPo  返回类型
     * @throws
     */
    public MobileLocationPo getMobileLocation(String mo);

}
