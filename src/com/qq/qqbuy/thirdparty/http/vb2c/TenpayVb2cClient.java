package com.qq.qqbuy.thirdparty.http.vb2c;

import java.util.Map;
import java.util.Map.Entry;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.exception.BusinessException;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.vb2c.util.TenpayVb2cUtil;
import com.qq.qqbuy.vb2c.util.Vb2cConstant;

/**
 * 虚拟充值：http方式通知虚拟中心财付通支付是否成功。
 * 
 * @author winsonwu
 * @Created 2012-4-18 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class TenpayVb2cClient  {

	public static boolean tenpayNotifyVb2c(String ver, String charset, String bankType,
			String bankBillno, String payResult, String payInfo,
			String purchaseAlias, String bargainorId, String transactionId,
			String spBillno, String totalFee, String feeType, String attach,
			String timeEnd, String sign_sp_id, String sign) throws BusinessException {

		String url = genNotifyUrl(ver, charset, bankType,
				bankBillno, payResult, payInfo, purchaseAlias, bargainorId, transactionId,
				spBillno, totalFee, feeType, attach, timeEnd, sign_sp_id, sign);
		Log.run.debug("Vb2cTenpayNotify-Http URL: " + url);
		
		boolean isNeedProxy = EnvManager.isIdc()|| EnvManager.isGamma();
        Log.run.debug("isNeedProxy: " + isNeedProxy);
        
        int retryNums = ConfigHelper.getIntByKey(PropertiesConstant.VB2C_TENPAY_NOTIFY_RETRY_COUNTS, Vb2cConstant.VB2C_TENPAY_NOTIFY_DEFAULT_RETRY_COUNTS);
        for (int i=0; i<retryNums; i++) {
        	String encoding = getDefaultEncoding(charset);
        	Log.run.debug("encoding: " + encoding);
        	String notifyRet = HttpUtil.get(url, 5000, 5000, encoding, isNeedProxy);
        	if (!StringUtil.isEmpty(notifyRet)) {
        		Log.run.debug("notify vb2c http result: " + notifyRet);
        		// TODO winson 注意检查虚拟中心的请求是否有内容回来已经内容格式
        		return true;
        	}
        }

		return false;
	}
	
	private static final String getDefaultEncoding(String shortCharset) {
		if (StringUtil.isEmpty(shortCharset)) {
			return "utf-8";
		}
		if ("1".equals(shortCharset)) {
			return "utf-8";
		}
		if ("2".equals(shortCharset)) {
			return "GB2312";
		}
		return "utf-8";
	}

	private static String genNotifyUrl(String ver, String charset, String bankType,
			String bankBillno, String payResult, String payInfo,
			String purchaseAlias, String bargainorId, String transactionId,
			String spBillno, String totalFee, String feeType, String attach,
			String timeEnd, String sign_sp_id, String sign) {
		String vb2cNotifyUrlPrefix = Vb2cConstant.VB2C_TENPAY_NOTIFY_DETAULT_URL;
		StringBuffer sbf = new StringBuffer(vb2cNotifyUrlPrefix);
		Map<String, String> param = TenpayVb2cUtil.genParamMap(ver, charset, bankType,
				bankBillno, payResult, payInfo, purchaseAlias, bargainorId, transactionId,
				spBillno, totalFee, feeType, attach, timeEnd, sign_sp_id);
		sbf.append("?").append(genParamUrl(param));
		sbf.append("&").append("sign=").append(sign);
		return sbf.toString();
	}
	
	private static String genParamUrl(Map<String, String> param) {
    	if (param != null && param.size() > 0) {
    		StringBuffer sbf = new StringBuffer();
    		for (Entry<String, String> entry : param.entrySet()) {
    			if (!StringUtil.isEmpty(entry.getKey())) {
    				sbf.append("&").append(entry.getKey()).append("=").append(entry.getValue());
    			}
    		}
    		if (sbf.length() > 1) {
    			return sbf.substring(1); //去掉最前面的&
    		}
    	}
    	return "";
    }

}
