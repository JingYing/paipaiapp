package com.qq.qqbuy.thirdparty.http.vb2c;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.HttpUtil;
import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.thirdparty.http.vb2c.po.MobileStockInfo;

/**
 * 调网购的js接口得到话费充值面额是否缺货
 * @author winsonwu
 * @Created 2012-4-6
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class MobileShortageClient  {
	
	private static String defaultPrefix = "http://virtual.paipai.com/extinfo/GetMobileProductInfo";
	private static String callbackName = "vb2cMobileCheckShortage";
	private static int readTimeout = 5000;// 读超时时间
    private static int connectTimeout = 5000;// 连接超时时间
	
	public static boolean checkMobileShortage(String mobile, int amount) {
		String urlPrefix = ConfigHelper.getStringByKey(PropertiesConstant.VB2C_CHECK_MOBILE_SHORTAGE_URL_PREFIX, defaultPrefix);
		StringBuffer sbf = new StringBuffer(urlPrefix);
		sbf.append("?");
		sbf.append("mobile=").append(mobile);
		sbf.append("&");
		sbf.append("amount=").append(amount);
		sbf.append("&");
		sbf.append("callname=").append(callbackName);
		sbf.append("&");
		sbf.append("seed=").append(System.currentTimeMillis());
		
		String url = sbf.toString();
		boolean isNeedProxy = EnvManager.isIdc() || EnvManager.isGamma();
		Log.run.debug("isNeedProxy: " + isNeedProxy);
		String content = HttpUtil.get(url, connectTimeout, readTimeout, "GBK", isNeedProxy);
		if (StringUtil.isEmpty(content)) {
			Log.run.warn("MobileShortageClient check shortage fail, can not get content by http request");
			return true;
		}
		
		String jsonContent = getJsonContent(content);
		MobileStockInfo info = deseriable(jsonContent);
		if (info != null) {
			return !(info.getStock() > 0);
		}
		
		return true;
	}
	
	private static MobileStockInfo deseriable(String jsonContent) {
		if (!StringUtil.isEmpty(jsonContent)) {
			JSONObject jsObj = JSONObject.fromObject(jsonContent);
			MobileStockInfo info = new MobileStockInfo();
			info.setMobile(jsObj.getString("mobile"));
			info.setProvince(jsObj.getString("province"));
			info.setIsp(jsObj.getString("isp"));
			info.setStock(jsObj.getInt("stock"));
			info.setAmount(jsObj.getInt("amount"));
			return info;
		}
		Log.run.warn("MobileShortageClient describle jsonContent fail, with jsonContent is: " + jsonContent);
		return null;
	}
	
	private static String getJsonContent(String content) {
		if (!StringUtil.isEmpty(content)) {
			String _doc = StringUtils.substringAfter(content, "(");
	        _doc = StringUtils.substringBeforeLast(_doc, ");}");
	        return _doc == null ? "" : _doc;
		}
		return content;
	}
	
}
