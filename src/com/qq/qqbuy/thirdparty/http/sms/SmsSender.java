package com.qq.qqbuy.thirdparty.http.sms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.qq.qqbuy.common.ConfigHelper;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.PaiPaiConfig;
import com.qq.qqbuy.common.constant.PropertiesConstant;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.common.util.StringUtil;

/**
 * 短信下发
 * @author millionchen
 *
 */
public class SmsSender implements PropertiesConstant {
    /**
     * 下行短信
     * @param mobile 手机号码
     * @param content 短信内容
     * @return
     */
    public static int sendSms(int buType, long buId, String mobile, String upStation, String content)
    {
        int ret = -100;
        try
        {
            //            HessianProxyFactory factory = new HessianProxyFactory();
            //            factory.setReadTimeout(ConfigUtil.getIntByKey("mt_timeout", 3000));
            //            String url = ConfigUtil.getStringByKey("mt_send_service_url", "");
            //            SmsSendService smsSendService = (SmsSendService) factory.create(SmsSendService.class, url);
            //            ret = smsSendService.sendSM(buType, buId, mobile, upStation, content);
            Map<String, String> params = new HashMap<String, String>();
            int connectTimeout = ConfigHelper.getIntByKey("mt_timeout", 3000);
            int readTimeOut = ConfigHelper.getIntByKey("mt_timeout", 3000);
            String resultJsonString = "";
            params.clear();
            params.put("buType", String.valueOf(buType));
            params.put("buId", String.valueOf(buId));
            params.put("mobile", mobile);
            params.put("station", upStation);
            params.put("content", content);
            String strUrls = "";
			if (EnvManager.isIdc()) {
				//从配置中心中获取
				strUrls = PaiPaiConfig.getSendMsgServerUrl();
			} else {
				strUrls = ConfigHelper.getStringByKey("mt_send_service_url", "");
			}
            String[] listUrls = StringUtil.split(strUrls, ",");
            // 暂时不用AutoSwitch自动选择路由，先用轮询方式
            for (String url : listUrls)
            {
                try
                {
                    resultJsonString = post(url, params, connectTimeout, readTimeOut);
//                    ret = JsonUtils.fromJsonString(resultJsonString, IntegerResponse.class).body;
                    break;
                }
                catch (IOException e)
                {
                	Log.run.error("sendSms invoke exception:" + url, e);
                }
            }
        }
        catch (Exception ex)
        {
            Log.run.error(String.format("sendSms(%s, %s, %s, %s, %s)",buType, buId, mobile, upStation, content), ex);
        }
        return ret;
    }
    
    public static String post(String urlStr, Map<String, String> params, int connectTimeout, int readTimeOut)
            throws IOException
    {
        String charset = "UTF-8";
        HttpURLConnection huc = null;
        try
        {
            URL url;
            String paramStr = StringUtil.trim(toQueryString(params, charset));
            url = new URL(urlStr);
            huc = (HttpURLConnection) url.openConnection();
            huc.setConnectTimeout(connectTimeout);
            huc.setReadTimeout(readTimeOut);
            huc.setRequestMethod("POST");
            // TEST ONLY
            //huc.setRequestProperty("Host", "rest.paipai.com");
            huc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            if (!StringUtil.isEmpty(paramStr))
            {
                huc.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(huc.getOutputStream(), charset);
                wr.write(paramStr);
                wr.flush();
            }
            InputStream is = huc.getInputStream();
            byte[] buf = new byte[1024];
            ByteArrayOutputStream bos = new ByteArrayOutputStream(2048);
            int length = -1;
            while ((length = is.read(buf)) >= 0)
            {
                bos.write(buf, 0, length);
            }
            buf = bos.toByteArray();
            return new String(buf, charset);
        }
        finally
        {
            if (huc != null)
                huc.disconnect();
        }
    }
    
    public static String toQueryString(Map<String, String> params, String charset) throws UnsupportedEncodingException
    {
        if (null == params || params.size() == 0)
        {
            return null;
        }
        StringBuilder str = new StringBuilder("");
        for (Map.Entry<String, String> e : params.entrySet())
        {
            appendParam(e.getKey(), e.getValue(), str, "&", charset);
        }
        return str.toString();
    }
    
    private static StringBuilder appendParam(String key, String value, StringBuilder sb, String ampersand,
            String charset) throws UnsupportedEncodingException
    {
        if (sb.length() > 0)
        {
            sb.append(ampersand);
        }
        sb.append(URLEncoder.encode(key, charset));
        sb.append("=");
        if (value != null)
            sb.append(URLEncoder.encode(value, charset));
        return sb;
    }

    public static void main(String args[])
    {
        int i = -100;
        i = SmsSender.sendSms(88, 10000111, "13728667921", null, "测试下行Hessian接口");
        System.out.println(i == 0 ? "短信下发成功！" : "短信发下失败");
    }
}

