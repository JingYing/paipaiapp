package com.qq.qqbuy.thirdparty.http.search;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.qq.qqbuy.common.util.Util;

public class ShopItemSearchParam {
	
	public static final int 
		ORDERSTYLE_价格降序 = 7,
		ORDERSTYLE_价格升序 = 6,
		ORDERSTYLE_信用降序 = 9,
		ORDERSTYLE_人气降序 = 21,
		ORDERSTYLE_销量升序 = 23,
		ORDERSTYLE_销量降序 = 24,
		ORDERSTYLE_综合分排序 = 77,
		ORDERSTYLE_默认排序 = 80,
		ORDERSTYLE_最新发布降序 = 94;
	
	private String 
		earmark = "0",		//是否对关键字加<em>标签
		dtype = "json", 
		charset = "utf-8",
		ac = "1",		//??不明
		sf = "2003",	//2003:手机, 2004:pc
		categoryId, 	//店内类目ID
		keyword,
		shopId;		//店铺ID,必填
	
	private int pageNum, pageSize;	//pageNum:起始页1 
	
	private int orderStyle;	//见ORDERSTYLE_*
	
	/**
	 * 序列化成a=1&b=2的形式
	 * @return
	 */
	public String serialize()	{
		StringBuilder sb = new StringBuilder()
			.append("dtype=").append(dtype)
			.append("&sf=").append(sf)
			.append("&earmark=").append(earmark)
			.append("&charset=").append(charset)
			.append("&shop=").append(shopId);
		if(categoryId != null)
			sb.append(".").append(categoryId);
		if(Util.isNotEmpty(keyword))	{
			try {
				sb.append("&KeyWord=").append(URLEncoder.encode(keyword, charset));
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
		if(orderStyle != 0)	
			sb.append("&ac=").append(ac).append("&OrderStyle=").append(orderStyle);
		if(pageNum != 0)	
			sb.append("&PageNum=").append(pageNum);
		if(pageSize != 0)	
			sb.append("&PageSize=").append(pageSize);
		return sb.toString();
	}

	public String getDtype() {
		return dtype;
	}

	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getAc() {
		return ac;
	}

	public void setAc(String ac) {
		this.ac = ac;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public int getOrderStyle() {
		return orderStyle;
	}

	public void setOrderStyle(int orderStyle) {
		this.orderStyle = orderStyle;
	}

	public String getSf() {
		return sf;
	}

	public void setSf(String sf) {
		this.sf = sf;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}	
}
