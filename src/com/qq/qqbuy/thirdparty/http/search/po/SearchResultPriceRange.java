package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;

/**
 * 结果价格区间
 * @author winsonwu
 * @Created 2012-3-13
 * <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class SearchResultPriceRange implements Serializable {

	private static final long serialVersionUID = -3923023589019653633L;

	/**
     * 当前价低价
     */
    private int lowLimit;

    /**
     * 当前价高价
     */
    private int highLimit;

    /**
     * 比例
     */
    private int ratio;

    public int getLowLimit() {
        return lowLimit;
    }

    public void setLowLimit(int lowLimit) {
        this.lowLimit = lowLimit;
    }

    public int getHighLimit() {
        return highLimit;
    }

    public void setHighLimit(int highLimit) {
        this.highLimit = highLimit;
    }

    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }
}
