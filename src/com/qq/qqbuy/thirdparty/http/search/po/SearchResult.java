package com.qq.qqbuy.thirdparty.http.search.po;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Description: 搜索结果
 * Date: 11-10-27
 * Time: 上午11:42
 *
 * @author <a href="mailto:huangfengjing@gmail.com">Ivan</a>
 * @version 1.0
 */
public class SearchResult implements Serializable {

	private static final long serialVersionUID = -418625886440510695L;

	/**
     * 通用错误状态码
     */
    public static final int ERR_RET_CODE = -1;

    /**
     * 正常状态码
     */
    public static final int SUCCESS_RET_CODE = 0;

    /**
     * 聚类字段名
     */
    public static final String CLUSTER_FIELD_NAME = "cluster";

    /**
     * 兄弟聚类字段名
     */
    public static final String BRO_CLUSTER_FIELD_NAME = "brothercluster";

    /**
     * 父聚类字段名
     */
    public static final String FATHER_CLUSTER_FIELD_NAME = "fathercluster";

    /**
     * 祖父类字段名
     */
    public static final String GRAND_FATHER_CLUSTER_FIELD_NAME = "grandfathercluster";

    /**
     * 返回码
     */
    private int retCode = SUCCESS_RET_CODE;

    /**
     * 返回信息
     */
    private String msg;

    /**
     * dtag 用于校验请求时的 dtag
     */
    private String dtag;

    /**
     * 搜索结果总数量
     */
    private int totalNum;

    /**
     * 优质店铺json信息
     */
    private String shopList;

    /**
     * 热门聚类信息
     */
    private LinkedHashMap<String, List<SearchResultCluster>> hotClasses = new LinkedHashMap<String, List<SearchResultCluster>>();

    /**
     * 类目信息
     */
    private List<SearchResultClass> classes = new ArrayList<SearchResultClass>();

    /**
     * 路径信息
     */
    private List<SearchResultPath> resultPaths = new ArrayList<SearchResultPath>();

    /**
     * 价格区间信息
     */
    private List<SearchResultPriceRange> resultPriceRanges = new ArrayList<SearchResultPriceRange>();

    /**
     * 属性聚类信息
     */
    private LinkedHashMap<String, List<SearchResultCluster>> propertyClusters = new LinkedHashMap<String, List<SearchResultCluster>>();

    /**
     * 商品信息
     */
    private List<SearchResultItem> resultItems = new ArrayList<SearchResultItem>();

    public int getRetCode() {
        return retCode;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDtag() {
        return dtag;
    }

    public void setDtag(String dtag) {
        this.dtag = dtag;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public LinkedHashMap<String, List<SearchResultCluster>> getHotClasses() {
        return hotClasses;
    }

    public void setHotClasses(LinkedHashMap<String, List<SearchResultCluster>> hotClasses) {
        this.hotClasses = hotClasses;
    }

    public List<SearchResultClass> getClasses() {
        return classes;
    }

    public void setClasses(List<SearchResultClass> classes) {
        this.classes = classes;
    }

    public List<SearchResultPath> getResultPaths() {
        return resultPaths;
    }

    public void setResultPaths(List<SearchResultPath> resultPaths) {
        this.resultPaths = resultPaths;
    }

    public List<SearchResultPriceRange> getResultPriceRanges() {
        return resultPriceRanges;
    }

    public void setResultPriceRanges(List<SearchResultPriceRange> resultPriceRanges) {
        this.resultPriceRanges = resultPriceRanges;
    }

    public LinkedHashMap<String, List<SearchResultCluster>> getPropertyClusters() {
        return propertyClusters;
    }

    public void setPropertyClusters(LinkedHashMap<String, List<SearchResultCluster>> propertyClusters) {
        this.propertyClusters = propertyClusters;
    }

    public List<SearchResultItem> getResultItems() {
        return resultItems;
    }

    public void setResultItems(List<SearchResultItem> resultItems) {
        this.resultItems = resultItems;
    }

    /**
     * 判断搜索发生错误，注意：如果返回结果为空也被认为发生错误（如果要准备判断是否结果为空，请使用：@see SearchResult#isEmpty）
     * @return 如果搜索发生错误，返回 TRUE，否则返回 FALSE
     */
    public boolean isError() {
        return retCode < 0;
    }

    /**
     * 判断搜索结果是否为空（并没有发生错误）
     * @return 如果搜索结果为空，返回　TRUE，否则返回 FALSE
     */
    public boolean isEmpty() {
        return resultItems.isEmpty();
    }

    public String getShopList() {
        return shopList;
    }

    public void setShopList(String shopList) {
        this.shopList = shopList;
    }
}