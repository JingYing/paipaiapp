package com.qq.qqbuy.thirdparty.http.search.po;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: 搜索条件
 * Date: 11-10-26
 * Time: 下午7:44
 *
 * @author <a href="mailto:huangfengjing@gmail.com">Ivan</a>
 * @version 1.0
 */
public class SearchCondition {

    /**
     * 系统参数
     */
    private SearchSysParams sysParam = new SearchSysParams();

    /**
     * 业务参数
     */
    private SearchBizParams bizParam = new SearchBizParams();
    
	public SearchSysParams getSysParam() {
		return sysParam;
	}

	public void setSysParam(SearchSysParams sysParam) {
		this.sysParam = sysParam;
	}

	public SearchBizParams getBizParam() {
		return bizParam;
	}

	public void setBizParam(SearchBizParams bizParam) {
		this.bizParam = bizParam;
	}

	public Map<String, String> validate() {
		Map<String, String> map = new HashMap<String, String>();
		map.putAll(sysParam.validate());
		map.putAll(bizParam.validate());
		return map;
	}
    
    public Map<String, String> toQueryMap() {
    	Map<String, String> map = new HashMap<String, String>();
    	map.putAll(sysParam.toQueryMap());
    	map.putAll(bizParam.toQueryMap());
    	return map;
    }
    
}
