package com.qq.qqbuy.thirdparty.openapi.impl;

import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import com.qq.qqbuy.thirdparty.openapi.OpenApiMethod;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoRequest;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoResponse;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoResponse.ComdyStockInfo;
import com.qq.qqbuy.thirdparty.openapi.po.GetComdyStockInfoResponse.ComdyStockInfo.Stock;

/**
 * 查询商品库存信息method
 * 
 * @author homerwu
 * 
 */
public class GetComdyStockInfoMethod extends
        OpenApiMethod<GetComdyStockInfoRequest, GetComdyStockInfoResponse>
{

    @Override
    protected void init()
    {
        super.signRequired = false;
        super.serviceModelName = "oneaday";
        super.serviceActionName = "getComdyStockInfoList";
    }

    @Override
    protected void handleResponse(final GetComdyStockInfoResponse res,
            long clientErrorCode, final String clientErrorMessage,
            final Document xmlDoc)
    {
        res.common.clientErrorCode = clientErrorCode;
        res.common.clientErrorMessage = clientErrorMessage;
        if (clientErrorCode != 0)
        {
            return;
        }
        xml2Response(res, xmlDoc);
    }

    @SuppressWarnings("unchecked")
    private void xml2Response(final GetComdyStockInfoResponse resp,
            final Document xml)
    {
        try
        {
            if (xml != null)
            {
                Element elmRoot = xml.getRootElement();
                resp.common.serverErrorCode = Long.parseLong(elmRoot
                        .getChildText("errorCode"));
                resp.common.serverErrorMessage = elmRoot
                        .getChildText("errorMessage");
                if (resp.common.serverErrorCode != 0)
                {
                    return;
                }
                Element itemListNode = elmRoot.getChild("itemList");
                List itemlist = itemListNode.getChildren("item");
                Element elm = null;
                Iterator iter = itemlist.iterator();
                while (iter.hasNext())
                {
                    ComdyStockInfo res = resp.new ComdyStockInfo();
                    elm = (Element) iter.next();
                    res.itemCode = elm.getChildText("itemCode");
                    res.itemName = elm.getChildText("itemName");
                    res.stockNum = Long.parseLong(elm.getChildText("stockNum"));
                    res.soldNum = Long.parseLong(elm.getChildText("soldNum"));
                    res.limitBuyNum = Long.parseLong(elm.getChildText("limitBuyNum"));
                    res.remainNum = Long.parseLong(elm
                            .getChildText("remainNum"));
                    res.uploadTime = elm.getChildText("uploadtime");
                    res.price = Util.getChildLong(elm, "price", 0);
                    res.property = elm.getChildText("property");
                    res.extInfo = elm.getChildText("extInfo");

                    Element stockListNode = elm.getChild("stocks");
                    List stocklist = stockListNode.getChildren("stock");
                    Element elmNode = null;
                    Iterator itr = stocklist.iterator();
                    while (itr.hasNext())
                    {
                        Stock stock = res.new Stock();
                        elmNode = (Element) itr.next();
                        stock.stockId = elmNode.getChildText("stockId");
                        stock.stockLocalCode = elmNode
                                .getChildText("stockLocalCode");
                        stock.attr = elmNode.getChildText("attr");
                        stock.stockNum = Long.parseLong(elmNode
                                .getChildText("stockNum"));
                        stock.soldNum = Long.parseLong(elmNode
                                .getChildText("soldNum"));
                        stock.remainNum = Long.parseLong(elmNode
                                .getChildText("remainNum"));
                        stock.price = Util.getChildLong(elmNode, "price", 0);
                        stock.desc = elmNode.getChildText("desc");
                        res.stockList.add(stock);
                    }
                    resp.items.add(res);
                }
            } else
            {
                setResponseError(resp, ErrorInfo.ERRNO_XML_IS_NULL, true);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            setResponseError(resp, ErrorInfo.ERRNO_PARSE_XML_FAILED, true);
        }
    }

    @Override
    protected GetComdyStockInfoResponse createResponse()
    {
        return new GetComdyStockInfoResponse();
    }
}