package com.qq.qqbuy.thirdparty.openapi.po;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.qq.qqbuy.common.util.StringUtil;
import com.qq.qqbuy.item.constant.ItemConstants.ItemProperty;
import com.qq.qqbuy.item.po.ItemAttrBO;
import com.qq.qqbuy.item.po.ItemPriceExtPo4J;
import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

/**
 * 查询单个商品详细信息response
 * @see http://pop.paipai.com/bin/view/Main/getItem
 * @author homerwu
 *
 */
public class GetItemResponse extends OpenApiResponse {
	
	/**
	 * 是否是缓存的数据
	 */
	public boolean isCache = false;
	
	
	/**
	 * 货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 */
	public long codShipTempId = 0;
	
	
	/**
	 * itemCode:商品编码，商品在拍拍上标识的唯一编码
	 */
	public String itemCode = null;
	
	/**
	 *itemLocalCode:商家对商品的编码，商家自行保证该编码的唯一性。
	 */
	public String itemLocalCode = null;
	
	/**
	 * itemName:商品名称
	 */
	public String itemName=null;
	
	/**
	 * itemState:商品状态
	 */
	public String itemState=null;
	
	/**
	 * stateDesc:商品状态说明
	 */
	public String stateDesc=null;
	
	/**
	 * itemProperty:商品属性
	 */
	public String itemProperty = null;
	
	/**
     * properties:商品的属性组合串,格式如：key1_value1|key1_value1|
     */
    public Map<Integer,Integer> properties = new HashMap<Integer,Integer>();
	
	/**
	 * stockCount:商品库存数量
	 */
	public long stockCount;
	
	/**
	 * itemPrice:商品单价
	 */
	public long itemPrice;
	
	/**
	 * marketPrice:市场价
	 */
	public long marketPrice;
	
	/**
	 * expressPrice:快递费用
	 */
	public long expressPrice;
	
	/**
	 * emsPrice:EMS费用
	 */
	public long emsPrice;
	
	/**
	 * mailPrice:邮寄费用
	 */
	public long mailPrice;
	
	/**
	 * categoryId:种类id
	 */
	public String categoryId=null;
	
	/**
	 * classId:类目id
	 */
	public String classId=null;
	
	/**
	 * cityId:城市id
	 */
	public long cityId;
	
	/**
	 * provinceId:省份id
	 */
	public long provinceId;
	
	/**
	 * countryId:国家id
	 */
	public long countryId;
	
	/**
	 * attr:属性串编码
	 */
	public String attr=null;
	
	/**
	 * customAttr:商家自定义属性
	 */
	public String customAttr=null;
	
	/**
	 * parsedAttrList:商家自定义属性说明列表
	 */
	public ArrayList<ParsedAttr> parsedAttrList = new ArrayList<ParsedAttr>();
	
	public List<ItemAttrBO> parsedAttrListForNew=new ArrayList<ItemAttrBO>();
	
    /**
     * extendList:商品扩展属性列表
     */
    public ArrayList<ExtendAttr> extendList = new ArrayList<ExtendAttr>();
	
	/**
	 * buyLimit:购买数量限制
	 */
	public long buyLimit;
	
	/**
	 * detailInfo:商品详情内容
	 */
	public String detailInfo=null;
	
	/**
	 * freightId:商品的运费模板id,当>=10时为有效运费模版
	 */
	public String freightId;
	
	/**
	 * guarantee14Days:是否14天包换  1是 0否
	 */
	public long guarantee14Days;
	
	/**
	 * guarantee7Days:是否7天包退   1是 0否
	 */
	public long guarantee7Days;
	
	/**
	 * guaranteeCompensation:是否假一赔三    1是 0否
	 */
	public long guaranteeCompensation;
	
	/**
	 * guaranteeRepair:是否提供保修服务 1是 0否
	 */
	public long guaranteeRepair;
	
	/**
	 * invoiceItem:是否提供发票 1是 0否
	 */
	public long invoiceItem;
	
	/**
	 * createTime:发布时间
	 */
	public String createTime;
	
	/**
	 * lastModifyTime:最后修改时间
	 */
	public String lastModifyTime;
	
	/**
	 * lastToSaleTime:上次上架时间
	 */
	public String lastToSaleTime;
	
	/**
	 * lastToStoreTime:上次下架时间
	 */
	public String lastToStoreTime;
	
	/**
	 * payType:支持的付款方式
	 */
	public String payType;
	
	/**
	 * picLink:商品主图连接
	 */
	public String picLink;
	
	/**
	 * 商品图片连接列表
	 */
	public List<String> picLinks;
	/**
	 * qqvipDiscount:QQ会员折扣 
	 */
	public long qqvipDiscount;
	
	/**
	 * qqvipItem:是否QQ会员店商品
	 */
	public long qqvipItem;
	
	/**
	 * recommendItem:是否推荐商品 1是 0否
	 */
	public long recommendItem;
	
	/**
	 * regionInfo:地区信息
	 */
	public String regionInfo;
	
	/**
	 * reloadCount:重上架次数
	 */
	public long reloadCount;
	
	/**
	 * secondHandItem:是否为二手商品  1是 0否
	 */
	public long secondHandItem;
	
	/**
	 * sellerPayFreight:是否为卖家承担运费 1是 0否
	 */
	public long sellerPayFreight;
	
	/**
	 * sellerName:店铺名称
	 */
	public String sellerName;
	
	/**
	 * sellerUin:商家qq号
	 */
	public long sellerUin;
	
	/**
	 * theme:商品详情页面主题
	 */
	public String theme;
	
	/**
	 * validDuration:商品上架后卖多少天后下架，单位以秒计
	 */
	public long validDuration;
	
	/**
	 * visitCount:访问的次数
	 */
	public long visitCount;
	
	/**
	 * soldCount:近期销售商品数量
	 */
	public long soldCount;
	
	/**
	 * soldTotalCount:销售的商品数量
	 */
	public long soldTotalCount;
	
	/**
	 * soldTimes:近期销售的订单次数
	 */
	public long soldTimes;
	
	/**
	 * soldTotalTimes:销售订单的总次数
	 */
	public long soldTotalTimes;
	
	/**
	 * buyNum:近期购买商品数量
	 */
	public long buyNum;
	
	/**
	 * totalBuyNum:购买商品的总数量
	 */
	public long totalBuyNum;
	
	/**
	 * buyCount:近期下单的订单次数
	 */
	public long buyCount;
	
	/**
	 * totalBuyCount:下单的订单总次数
	 */
	public long totalBuyCount;
	
	/**
	 * weight:商品的重量
	 */
	public long weight;
	
	/**
	 * windowItem:是否为橱窗商品 1是 0否
	 */
	public long windowItem;
	
	/**
	 * sizeTableId:商品的尺码表Id
	 */
	public long sizeTableId;
	
	/**
	 * stockList:商品的库存列表
	 */
	public ArrayList<Stock> stockList = new ArrayList<Stock>();
	/**
	 * 折后价
	 */
	public Vector<Integer> discountPrice = new Vector<Integer>();
	
	/**
	 * colorDiamond：商品彩钻折扣字符串，彩钻价格，用|号线隔开
	 * 例如：9900|9900|9800|9800|9700|9700
	 * 表示：1级|2级|3级|4级|5级|5级以上
	 */
	public String colorDiamond;
	
	
	private ItemPriceExtPo4J oItemPriceExt = new ItemPriceExtPo4J();
	/**
	 * 是否支持促销
	 */
	private boolean supportPromotion;
    /**
     * 拍拍正常价
     */
	private long normalPrice;
	
	@Override
	public boolean needsAlarm() {
		boolean requireAlarm = super.needsAlarm();
	    if (requireAlarm) {
	        // 过滤不需要作为接口调用失败上报的情况
	        if (common.clientErrorCode == 0) {
	            
	            // 注意下面的错误码排序是按照情况出现频率由高到低以提升系统判断效率
	            if (common.serverErrorCode == 8193) { // 商品不存在
	                requireAlarm = false;
	            }
	        }
	    }
	    return requireAlarm;
	}
	public boolean isMobileItem() {
		if ( properties != null ) {
			Integer mobileItem = properties.get(128);
			if ( mobileItem != null && mobileItem == 1 ) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 是否支持pc货到付款
	 * @return
	 * @date:2013-4-10
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 * @version 1.0
	 */
	public boolean isSupportCodForPP() {
		if ( properties != null ) {
			if ( properties.get(ItemProperty.SIMP_PROP_CASH_ON_DELIVERY.getIndex()) != null 
					&& properties.get(ItemProperty.SIMP_PROP_CASH_ON_DELIVERY.getIndex()) == 1 ) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isColorDimaond() {
		if (StringUtil.isEmpty(colorDiamond)) {
			return false;
		}
		return true;
	}
	/**
	 * 通过等级获取折扣减免价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockDiscountPrice(int level){
		if(discountPrice == null){
			  return 0;	
		}
			if(discountPrice.size() >= level){ 
				return (int)itemPrice - discountPrice.get(level-1);
			}			
		  return 0;		
	}
	/**
	 * 通过等级获取折扣后价格
	 * @param level
	 * @return
	 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
	 */
	public long getStockAfterDiscountPrice(int level){
		if(discountPrice == null){
			  return itemPrice;	
		}
			if(discountPrice.size() >=level){ 
				return discountPrice.get(level-1);
			}			
		  return itemPrice;		
	}

	public class Stock implements java.io.Serializable {
		
		/**
		 * stockId:商品的库存id
		 */
		public String stockId=null;
		
		/**
		 * stockLocalCode:商品的商家自定义库存id
		 */
		public String stockLocalCode=null;
		
		/**
		 * stockAttr:商品的库存属性串
		 */
		public String stockAttr=null;
		
		/**
		 * stockDesc:商品的库存备注
		 */
		public String stockDesc=null;
		
		/**
		 * soldCount:商品的该库存对应的销售数量
		 */
		public long soldCount;
		
		/**
		 * stockPrice:商品的库存的价格
		 */
		public long stockPrice;
		
		/**
		 * stockCount:商品的库存数量
		 */
		public long stockCount;
		
		/**
		 * createTime:商品的库存创建时间
		 */
		public String createTime=null;
		
		/**
		 * lastModifyTime:商品的库存最后修改时间
		 */
		public String lastModifyTime=null;
		/**
		 * 折后价
		 */
		public Vector<Integer> discountPrice = new Vector<Integer>();
		private ItemPriceExtPo4J oItemPriceExt = new ItemPriceExtPo4J();
		
		private long normalPrice = 0;	

		
		public Vector<Integer> getDiscountPrice() {
			return discountPrice;
		}
		public void setDiscountPrice(Vector<Integer> discountPrice) {
			this.discountPrice = discountPrice;
		}
		/**
		 * 通过等级获取折扣减免价格
		 * @param level
		 * @return
		 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
		 */
		public long getStockDiscountPrice(int level){
			if(discountPrice == null){
				return 0;
			}
				if(discountPrice.size() >= level){ 
					return (int)stockPrice - discountPrice.get(level-1);
				}			
			  return 0;		
		}
		/**
		 * 通过等级获取折扣后价格
		 * @param level
		 * @return
		 * @author:<a href="mailto:jiaqiangxu@tencent.com">JiaqiangXu</a>
		 */
		public long getStockAfterDiscountPrice(int level){
			if(discountPrice == null){
				return stockPrice;
			}
				if(discountPrice.size() >= level){ 
					return discountPrice.get(level-1);
				}			
			  return stockPrice;		
		}
		public ItemPriceExtPo4J getoItemPriceExt() {
			return oItemPriceExt;
		}
		public void setoItemPriceExt(ItemPriceExtPo4J oItemPriceExt) {
			this.oItemPriceExt = oItemPriceExt;
		}
		public long getNormalPrice() {
			return normalPrice;
		}
		public void setNormalPrice(long normalPrice) {
			this.normalPrice = normalPrice;
		}

	}
	
	public class ParsedAttr implements java.io.Serializable{
		
		/**
		 * attrId:商品的属性串编码id
		 */
		public String attrId=null;
		
		/**
		 * attrName:商品的属性串编码名称
		 */
		public String attrName=null;
		
		/**
		 * attrOptionList:商品的属性串列表
		 */
		public ArrayList<AttrOption> attrOptionList = new ArrayList<AttrOption>();
		
		public String getAttrOptionDisplay(){
			StringBuilder s=new StringBuilder();
			if(attrOptionList!=null){
				for(int i=0;i<attrOptionList.size();i++){
					AttrOption attrOption=attrOptionList.get(i);
					if(attrOption!=null && StringUtil.isNotEmpty(attrOption.attrOptionName)){
						s.append(attrOption.attrOptionName);
						if(i!=attrOptionList.size()-1){
							s.append("、");
						}
					}					
				}
			}
			return s.toString();
		}
		
		
		public class AttrOption implements java.io.Serializable {
			/**
			 * attrOptionId:商品的属性串编码值id
			 */
			public String attrOptionId=null;
			
			/**
			 * attrOptionName:商品的属性串编码值名称
			 */
			public String attrOptionName=null;
		}
	}
	
	public class ExtendAttr implements java.io.Serializable{
        /**
         * extendCode:商品扩展属性编码id
         */
        public long extendCode;
        
        /**
         * extendName:商品扩展属性名称
         */
        public String extendName = null ;
        
        /**
         * showMeg:商品扩展属性名称说明
         */
        public String showMeg; 
        
        /**
         * extendValue:商品扩展属性 值
         */
        public String extendValue; 
        
        
	}
	

	public boolean isCache() {
		return isCache;
	}


	public void setCache(boolean isCache) {
		this.isCache = isCache;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getItemLocalCode() {
		return itemLocalCode;
	}


	public void setItemLocalCode(String itemLocalCode) {
		this.itemLocalCode = itemLocalCode;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getItemState() {
		return itemState;
	}


	public void setItemState(String itemState) {
		this.itemState = itemState;
	}


	public String getStateDesc() {
		return stateDesc;
	}


	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}


	public String getItemProperty() {
		return itemProperty;
	}


	public void setItemProperty(String itemProperty) {
		this.itemProperty = itemProperty;
	}


	public Map<Integer, Integer> getProperties() {
		return properties;
	}


	public void setProperties(Map<Integer, Integer> properties) {
		this.properties = properties;
	}


	public long getStockCount() {
		return stockCount;
	}


	public void setStockCount(long stockCount) {
		this.stockCount = stockCount;
	}


	public long getItemPrice() {
		return itemPrice;
	}


	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}


	public long getMarketPrice() {
		return marketPrice;
	}


	public void setMarketPrice(long marketPrice) {
		this.marketPrice = marketPrice;
	}


	public long getExpressPrice() {
		return expressPrice;
	}


	public void setExpressPrice(long expressPrice) {
		this.expressPrice = expressPrice;
	}


	public long getEmsPrice() {
		return emsPrice;
	}


	public void setEmsPrice(long emsPrice) {
		this.emsPrice = emsPrice;
	}


	public long getMailPrice() {
		return mailPrice;
	}


	public void setMailPrice(long mailPrice) {
		this.mailPrice = mailPrice;
	}


	public String getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}


	public String getClassId() {
		return classId;
	}


	public void setClassId(String classId) {
		this.classId = classId;
	}


	public long getCityId() {
		return cityId;
	}


	public void setCityId(long cityId) {
		this.cityId = cityId;
	}


	public long getProvinceId() {
		return provinceId;
	}


	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}


	public long getCountryId() {
		return countryId;
	}


	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}


	public String getAttr() {
		return attr;
	}


	public void setAttr(String attr) {
		this.attr = attr;
	}


	public String getCustomAttr() {
		return customAttr;
	}


	public void setCustomAttr(String customAttr) {
		this.customAttr = customAttr;
	}


	public ArrayList<ParsedAttr> getParsedAttrList() {
		return parsedAttrList;
	}


	public void setParsedAttrList(ArrayList<ParsedAttr> parsedAttrList) {
		this.parsedAttrList = parsedAttrList;
	}


	public ArrayList<ExtendAttr> getExtendList() {
		return extendList;
	}


	public void setExtendList(ArrayList<ExtendAttr> extendList) {
		this.extendList = extendList;
	}


	public long getBuyLimit() {
		return buyLimit;
	}


	public void setBuyLimit(long buyLimit) {
		this.buyLimit = buyLimit;
	}


	public String getDetailInfo() {
		return detailInfo;
	}


	public void setDetailInfo(String detailInfo) {
		this.detailInfo = detailInfo;
	}


	public String getFreightId() {
		return freightId;
	}


	public void setFreightId(String freightId) {
		this.freightId = freightId;
	}


	public long getGuarantee14Days() {
		return guarantee14Days;
	}


	public void setGuarantee14Days(long guarantee14Days) {
		this.guarantee14Days = guarantee14Days;
	}


	public long getGuarantee7Days() {
		return guarantee7Days;
	}


	public void setGuarantee7Days(long guarantee7Days) {
		this.guarantee7Days = guarantee7Days;
	}


	public long getGuaranteeCompensation() {
		return guaranteeCompensation;
	}


	public void setGuaranteeCompensation(long guaranteeCompensation) {
		this.guaranteeCompensation = guaranteeCompensation;
	}


	public long getGuaranteeRepair() {
		return guaranteeRepair;
	}


	public void setGuaranteeRepair(long guaranteeRepair) {
		this.guaranteeRepair = guaranteeRepair;
	}


	public long getInvoiceItem() {
		return invoiceItem;
	}


	public void setInvoiceItem(long invoiceItem) {
		this.invoiceItem = invoiceItem;
	}


	public String getCreateTime() {
		return createTime;
	}


	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}


	public String getLastModifyTime() {
		return lastModifyTime;
	}


	public void setLastModifyTime(String lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}


	public String getLastToSaleTime() {
		return lastToSaleTime;
	}


	public void setLastToSaleTime(String lastToSaleTime) {
		this.lastToSaleTime = lastToSaleTime;
	}


	public String getLastToStoreTime() {
		return lastToStoreTime;
	}


	public void setLastToStoreTime(String lastToStoreTime) {
		this.lastToStoreTime = lastToStoreTime;
	}


	public String getPayType() {
		return payType;
	}


	public void setPayType(String payType) {
		this.payType = payType;
	}


	public String getPicLink() {
		return picLink;
	}


	public void setPicLink(String picLink) {
		this.picLink = picLink;
	}


	public List<String> getPicLinks() {
		return picLinks;
	}


	public void setPicLinks(List<String> picLinks) {
		this.picLinks = picLinks;
	}


	public long getQqvipDiscount() {
		return qqvipDiscount;
	}


	public void setQqvipDiscount(long qqvipDiscount) {
		this.qqvipDiscount = qqvipDiscount;
	}


	public long getQqvipItem() {
		return qqvipItem;
	}


	public void setQqvipItem(long qqvipItem) {
		this.qqvipItem = qqvipItem;
	}


	public long getRecommendItem() {
		return recommendItem;
	}


	public void setRecommendItem(long recommendItem) {
		this.recommendItem = recommendItem;
	}


	public String getRegionInfo() {
		return regionInfo;
	}


	public void setRegionInfo(String regionInfo) {
		this.regionInfo = regionInfo;
	}


	public long getReloadCount() {
		return reloadCount;
	}


	public void setReloadCount(long reloadCount) {
		this.reloadCount = reloadCount;
	}


	public long getSecondHandItem() {
		return secondHandItem;
	}


	public void setSecondHandItem(long secondHandItem) {
		this.secondHandItem = secondHandItem;
	}


	public long getSellerPayFreight() {
		return sellerPayFreight;
	}


	public void setSellerPayFreight(long sellerPayFreight) {
		this.sellerPayFreight = sellerPayFreight;
	}


	public String getSellerName() {
		return sellerName;
	}


	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	public long getSellerUin() {
		return sellerUin;
	}


	public void setSellerUin(long sellerUin) {
		this.sellerUin = sellerUin;
	}


	public String getTheme() {
		return theme;
	}


	public void setTheme(String theme) {
		this.theme = theme;
	}


	public long getValidDuration() {
		return validDuration;
	}


	public void setValidDuration(long validDuration) {
		this.validDuration = validDuration;
	}


	public long getVisitCount() {
		return visitCount;
	}


	public void setVisitCount(long visitCount) {
		this.visitCount = visitCount;
	}


	public long getSoldCount() {
		return soldCount;
	}


	public void setSoldCount(long soldCount) {
		this.soldCount = soldCount;
	}


	public long getSoldTotalCount() {
		return soldTotalCount;
	}


	public void setSoldTotalCount(long soldTotalCount) {
		this.soldTotalCount = soldTotalCount;
	}


	public long getSoldTimes() {
		return soldTimes;
	}


	public void setSoldTimes(long soldTimes) {
		this.soldTimes = soldTimes;
	}


	public long getSoldTotalTimes() {
		return soldTotalTimes;
	}


	public void setSoldTotalTimes(long soldTotalTimes) {
		this.soldTotalTimes = soldTotalTimes;
	}


	public long getBuyNum() {
		return buyNum;
	}


	public void setBuyNum(long buyNum) {
		this.buyNum = buyNum;
	}


	public long getTotalBuyNum() {
		return totalBuyNum;
	}


	public void setTotalBuyNum(long totalBuyNum) {
		this.totalBuyNum = totalBuyNum;
	}


	public long getBuyCount() {
		return buyCount;
	}


	public void setBuyCount(long buyCount) {
		this.buyCount = buyCount;
	}


	public long getTotalBuyCount() {
		return totalBuyCount;
	}


	public void setTotalBuyCount(long totalBuyCount) {
		this.totalBuyCount = totalBuyCount;
	}


	public long getWeight() {
		return weight;
	}


	public void setWeight(long weight) {
		this.weight = weight;
	}


	public long getWindowItem() {
		return windowItem;
	}


	public void setWindowItem(long windowItem) {
		this.windowItem = windowItem;
	}


	public long getSizeTableId() {
		return sizeTableId;
	}


	public void setSizeTableId(long sizeTableId) {
		this.sizeTableId = sizeTableId;
	}


	public ArrayList<Stock> getStockList() {
		return stockList;
	}


	public void setStockList(ArrayList<Stock> stockList) {
		this.stockList = stockList;
	}


	public String getColorDiamond() {
		return colorDiamond;
	}


	public void setColorDiamond(String colorDiamond) {
		this.colorDiamond = colorDiamond;
	}


	@Override
    public String toString() {
        return "GetItemResponse [itemCode=" + itemCode + ", itemLocalCode=" + itemLocalCode + ", itemName=" + itemName
                        + ", itemState=" + itemState + ", stateDesc=" + stateDesc + ", itemProperty=" + itemProperty
                        + ", properties=" + properties + ", stockCount=" + stockCount + ", itemPrice=" + itemPrice
                        + ", marketPrice=" + marketPrice + ", expressPrice=" + expressPrice + ", emsPrice=" + emsPrice
                        + ", mailPrice=" + mailPrice + ", categoryId=" + categoryId + ", classId=" + classId
                        + ", cityId=" + cityId + ", provinceId=" + provinceId + ", countryId=" + countryId + ", attr="
                        + attr + ", customAttr=" + customAttr + ", parsedAttrList=" + parsedAttrList + ", extendList="
                        + extendList + ", buyLimit=" + buyLimit + ", detailInfo=" + detailInfo + ", freightId="
                        + freightId + ", guarantee14Days=" + guarantee14Days + ", guarantee7Days=" + guarantee7Days
                        + ", guaranteeCompensation=" + guaranteeCompensation + ", guaranteeRepair=" + guaranteeRepair
                        + ", invoiceItem=" + invoiceItem + ", createTime=" + createTime + ", lastModifyTime="
                        + lastModifyTime + ", lastToSaleTime=" + lastToSaleTime + ", lastToStoreTime="
                        + lastToStoreTime + ", payType=" + payType + ", picLink=" + picLink + ", picLinks=" + picLinks
                        + ", qqvipDiscount=" + qqvipDiscount + ", qqvipItem=" + qqvipItem + ", recommendItem="
                        + recommendItem + ", regionInfo=" + regionInfo + ", reloadCount=" + reloadCount
                        + ", secondHandItem=" + secondHandItem + ", sellerPayFreight=" + sellerPayFreight
                        + ", sellerName=" + sellerName + ", sellerUin=" + sellerUin + ", theme=" + theme
                        + ", validDuration=" + validDuration + ", visitCount=" + visitCount + ", soldCount="
                        + soldCount + ", soldTotalCount=" + soldTotalCount + ", soldTimes=" + soldTimes
                        + ", soldTotalTimes=" + soldTotalTimes + ", buyNum=" + buyNum + ", totalBuyNum=" + totalBuyNum
                        + ", buyCount=" + buyCount + ", totalBuyCount=" + totalBuyCount + ", weight=" + weight
                        + ", windowItem=" + windowItem + ", sizeTableId=" + sizeTableId + ", stockList=" + stockList
                        + ", toString()=" + super.toString() + "]";
    }


	public Vector<Integer> getDiscountPrice() {
		return discountPrice;
	}


	public void setDiscountPrice(Vector<Integer> discountPrice) {
		this.discountPrice = discountPrice;
	}
	public ItemPriceExtPo4J getoItemPriceExt() {
		return oItemPriceExt;
	}
	public void setoItemPriceExt(ItemPriceExtPo4J oItemPriceExt) {
		this.oItemPriceExt = oItemPriceExt;
	}
	public long getCodShipTempId() {
		return codShipTempId;
	}
	public void setCodShipTempId(long codShipTempId) {
		this.codShipTempId = codShipTempId;
	}
	public boolean isSupportPromotion() {
		return supportPromotion;
	}
	public void setSupportPromotion(boolean supportPromotion) {
		this.supportPromotion = supportPromotion;
	}
	public long getNormalPrice() {
		return normalPrice;
	}
	public void setNormalPrice(long normalPrice) {
		this.normalPrice = normalPrice;
	}
	public List<ItemAttrBO> getParsedAttrListForNew() {
		return parsedAttrListForNew;
	}
	public void setParsedAttrListForNew(List<ItemAttrBO> parsedAttrListForNew) {
		this.parsedAttrListForNew = parsedAttrListForNew;
	}

}
