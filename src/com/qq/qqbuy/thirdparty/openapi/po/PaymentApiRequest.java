package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiRequest;

public class PaymentApiRequest extends OpenApiRequest {
	

	
	/**
	 * source:来源
	 */
	public void setSource(String source) {
		this.argList.addParam("source", source);
	}
	
	/**
	 * dealCode:订单号
	 */
	public void setDealCode(String dealCode) {
		this.argList.addParam("dealCode", dealCode);
	}
	
	/**
	 * type:订单类型
	 */
	public void setType(long type) {
		this.argList.addParam("type", type);
	}
	
	/**
	 * reserve:保留字段
	 */
	public void setReserve(String reserve) {
		this.argList.addParam("reserve", reserve);
	}
}
