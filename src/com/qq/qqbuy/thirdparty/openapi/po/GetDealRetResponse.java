package com.qq.qqbuy.thirdparty.openapi.po;

import com.qq.qqbuy.thirdparty.openapi.OpenApiResponse;

/**
 * 查询抢购结果response
 * @author homerwu
 *
 */
public class GetDealRetResponse extends OpenApiResponse {
	
	/**
	 * pos:订单处理状态，
	 * 0 订单已经被处理 ,0xFFFFFFFF订单正在被处理
	 */
	public long pos;

	/**
	 * dealRet:订单处理结果
	 * 0订单成功生成；非0订单生成失败
	 */
	public long dealRet;
	
	/**
	 * dealCode:订单号
	 * 只有dealRet为0时才会有订单号
	 */
	public String dealCode;

	@Override
	public String toString() {
		return "GetDealRetResponse [dealCode=" + dealCode + ", dealRet="
				+ dealRet + ", pos=" + pos + "]";
	}
	
}
