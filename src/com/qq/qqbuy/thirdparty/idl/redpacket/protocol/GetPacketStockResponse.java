//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPacketStockResp.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *获取红包批次回复
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class GetPacketStockResponse  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 获取批次者uin
	 *
	 * 版本 >= 0
	 */
	 private long RespUin;

	/**
	 * 版本 >= 0
	 */
	 private short RespUin_u;

	/**
	 * 红包批次列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<PacketStock> RespPacketStock = new Vector<PacketStock>();

	/**
	 * 版本 >= 0
	 */
	 private short RespPacketStock_u;

	/**
	 * 红包批次总数
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockTotal;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockTotal_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(RespUin);
		bs.pushUByte(RespUin_u);
		bs.pushObject(RespPacketStock);
		bs.pushUByte(RespPacketStock_u);
		bs.pushUInt(PacketStockTotal);
		bs.pushUByte(PacketStockTotal_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		RespUin = bs.popUInt();
		RespUin_u = bs.popUByte();
		RespPacketStock = (Vector<PacketStock>)bs.popVector(PacketStock.class);
		RespPacketStock_u = bs.popUByte();
		PacketStockTotal = bs.popUInt();
		PacketStockTotal_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取获取批次者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return RespUin value 类型为:long
	 * 
	 */
	public long getRespUin()
	{
		return RespUin;
	}


	/**
	 * 设置获取批次者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRespUin(long value)
	{
		this.RespUin = value;
		this.RespUin_u = 1;
	}

	public boolean issetRespUin()
	{
		return this.RespUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RespUin_u value 类型为:short
	 * 
	 */
	public short getRespUin_u()
	{
		return RespUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRespUin_u(short value)
	{
		this.RespUin_u = value;
	}


	/**
	 * 获取红包批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RespPacketStock value 类型为:Vector<PacketStock>
	 * 
	 */
	public Vector<PacketStock> getRespPacketStock()
	{
		return RespPacketStock;
	}


	/**
	 * 设置红包批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PacketStock>
	 * 
	 */
	public void setRespPacketStock(Vector<PacketStock> value)
	{
		if (value != null) {
				this.RespPacketStock = value;
				this.RespPacketStock_u = 1;
		}
	}

	public boolean issetRespPacketStock()
	{
		return this.RespPacketStock_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RespPacketStock_u value 类型为:short
	 * 
	 */
	public short getRespPacketStock_u()
	{
		return RespPacketStock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRespPacketStock_u(short value)
	{
		this.RespPacketStock_u = value;
	}


	/**
	 * 获取红包批次总数
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockTotal value 类型为:long
	 * 
	 */
	public long getPacketStockTotal()
	{
		return PacketStockTotal;
	}


	/**
	 * 设置红包批次总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockTotal(long value)
	{
		this.PacketStockTotal = value;
		this.PacketStockTotal_u = 1;
	}

	public boolean issetPacketStockTotal()
	{
		return this.PacketStockTotal_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockTotal_u value 类型为:short
	 * 
	 */
	public short getPacketStockTotal_u()
	{
		return PacketStockTotal_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockTotal_u(short value)
	{
		this.PacketStockTotal_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPacketStockResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RespUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RespUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RespPacketStock, null);  //计算字段RespPacketStock的长度 size_of(Vector)
				length += 1;  //计算字段RespPacketStock_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketStockTotal的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketStockTotal_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPacketStockResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RespUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RespUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RespPacketStock, encoding);  //计算字段RespPacketStock的长度 size_of(Vector)
				length += 1;  //计算字段RespPacketStock_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketStockTotal的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketStockTotal_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
