//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.RefundRedPacketReq.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *退回红包请求
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class RefundRedPacketRequest  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 退回红包用户uin
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 退回红包列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> RefundRedPacketInfo = new Vector<uint64_t>();

	/**
	 * 版本 >= 0
	 */
	 private short RefundRedPacketInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushObject(RefundRedPacketInfo);
		bs.pushUByte(RefundRedPacketInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		RefundRedPacketInfo = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		RefundRedPacketInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取退回红包用户uin
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置退回红包用户uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}

	public boolean issetOwnerUin()
	{
		return this.OwnerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取退回红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundRedPacketInfo value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getRefundRedPacketInfo()
	{
		return RefundRedPacketInfo;
	}


	/**
	 * 设置退回红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setRefundRedPacketInfo(Vector<uint64_t> value)
	{
		if (value != null) {
				this.RefundRedPacketInfo = value;
				this.RefundRedPacketInfo_u = 1;
		}
	}

	public boolean issetRefundRedPacketInfo()
	{
		return this.RefundRedPacketInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundRedPacketInfo_u value 类型为:short
	 * 
	 */
	public short getRefundRedPacketInfo_u()
	{
		return RefundRedPacketInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRefundRedPacketInfo_u(short value)
	{
		this.RefundRedPacketInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(RefundRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RefundRedPacketInfo, null);  //计算字段RefundRedPacketInfo的长度 size_of(Vector)
				length += 1;  //计算字段RefundRedPacketInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(RefundRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RefundRedPacketInfo, encoding);  //计算字段RefundRedPacketInfo的长度 size_of(Vector)
				length += 1;  //计算字段RefundRedPacketInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
