//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPackeAndStockResp.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *获取红包和批次列表回复
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:20130328
*/
public class GetPackeAndStockResponse  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 20130328;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 获取者uin
	 *
	 * 版本 >= 0
	 */
	 private long RespUin;

	/**
	 * 版本 >= 0
	 */
	 private short RespUin_u;

	/**
	 * 红包和批次列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<PacketAndStockList> PacketAndStock = new Vector<PacketAndStockList>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketAndStock_u;

	/**
	 * 红包总数
	 *
	 * 版本 >= 20130328
	 */
	 private long totalNum;

	/**
	 * 
	 *
	 * 版本 >= 20130328
	 */
	 private short totalNum_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(RespUin);
		bs.pushUByte(RespUin_u);
		bs.pushObject(PacketAndStock);
		bs.pushUByte(PacketAndStock_u);
		if(  this.version >= 20130328 ){
				bs.pushUInt(totalNum);
		}
		if(  this.version >= 20130328 ){
				bs.pushUByte(totalNum_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		RespUin = bs.popUInt();
		RespUin_u = bs.popUByte();
		PacketAndStock = (Vector<PacketAndStockList>)bs.popVector(PacketAndStockList.class);
		PacketAndStock_u = bs.popUByte();
		if(  this.version >= 20130328 ){
				totalNum = bs.popUInt();
		}
		if(  this.version >= 20130328 ){
				totalNum_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取获取者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return RespUin value 类型为:long
	 * 
	 */
	public long getRespUin()
	{
		return RespUin;
	}


	/**
	 * 设置获取者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRespUin(long value)
	{
		this.RespUin = value;
		this.RespUin_u = 1;
	}

	public boolean issetRespUin()
	{
		return this.RespUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RespUin_u value 类型为:short
	 * 
	 */
	public short getRespUin_u()
	{
		return RespUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRespUin_u(short value)
	{
		this.RespUin_u = value;
	}


	/**
	 * 获取红包和批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketAndStock value 类型为:Vector<PacketAndStockList>
	 * 
	 */
	public Vector<PacketAndStockList> getPacketAndStock()
	{
		return PacketAndStock;
	}


	/**
	 * 设置红包和批次列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PacketAndStockList>
	 * 
	 */
	public void setPacketAndStock(Vector<PacketAndStockList> value)
	{
		if (value != null) {
				this.PacketAndStock = value;
				this.PacketAndStock_u = 1;
		}
	}

	public boolean issetPacketAndStock()
	{
		return this.PacketAndStock_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketAndStock_u value 类型为:short
	 * 
	 */
	public short getPacketAndStock_u()
	{
		return PacketAndStock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketAndStock_u(short value)
	{
		this.PacketAndStock_u = value;
	}


	/**
	 * 获取红包总数
	 * 
	 * 此字段的版本 >= 20130328
	 * @return totalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return totalNum;
	}


	/**
	 * 设置红包总数
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.totalNum = value;
		this.totalNum_u = 1;
	}

	public boolean issetTotalNum()
	{
		return this.totalNum_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130328
	 * @return totalNum_u value 类型为:short
	 * 
	 */
	public short getTotalNum_u()
	{
		return totalNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalNum_u(short value)
	{
		this.totalNum_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPackeAndStockResponse)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RespUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RespUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketAndStock, null);  //计算字段PacketAndStock的长度 size_of(Vector)
				length += 1;  //计算字段PacketAndStock_u的长度 size_of(uint8_t)
				if(  this.version >= 20130328 ){
						length += 4;  //计算字段totalNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130328 ){
						length += 1;  //计算字段totalNum_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPackeAndStockResponse)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RespUin的长度 size_of(uint32_t)
				length += 1;  //计算字段RespUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketAndStock, encoding);  //计算字段PacketAndStock的长度 size_of(Vector)
				length += 1;  //计算字段PacketAndStock_u的长度 size_of(uint8_t)
				if(  this.version >= 20130328 ){
						length += 4;  //计算字段totalNum的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130328 ){
						length += 1;  //计算字段totalNum_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20130328所包含的字段*******
 *	long version;///<版本
 *	short version_u;
 *	long RespUin;///<获取者uin
 *	short RespUin_u;
 *	Vector<PacketAndStockList> PacketAndStock;///<红包和批次列表
 *	short PacketAndStock_u;
 *	long totalNum;///<红包总数
 *	short totalNum_u;
 *****以上是版本20130328所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
