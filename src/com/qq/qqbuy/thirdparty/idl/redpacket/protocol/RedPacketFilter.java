

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint64_t;
import java.util.Vector;
import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *请求filter
 *
 *@date 2013-08-20 05:45::21
 *
 *@since version:20130328
*/
public class RedPacketFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20130328; 

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String PacketName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short PacketName_u;

	/**
	 * 红包批次id
	 *
	 * 版本 >= 0
	 */
	 private long PacketStockId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketStockId_u;

	/**
	 * 所属者
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 对应卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short SellerUin_u;

	/**
	 * 红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 绑定的订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealCode = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DealCode_u;

	/**
	 * 兑现编号
	 *
	 * 版本 >= 0
	 */
	 private long WithdrawId;

	/**
	 * 版本 >= 0
	 */
	 private short WithdrawId_u;

	/**
	 * 最大面值
	 *
	 * 版本 >= 0
	 */
	 private long MaxFaceValue;

	/**
	 * 版本 >= 0
	 */
	 private short MaxFaceValue_u;

	/**
	 * 适用范围
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> ApplicableScope = new HashSet<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ApplicableScope_u;

	/**
	 * 红包id列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> PacketIdList = new Vector<uint64_t>();

	/**
	 * 版本 >= 0
	 */
	 private short PacketIdList_u;

	/**
	 * 红包类型,红包(1)，店铺代金券(2)
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 版本 >= 0
	 */
	 private short Type_u;

	/**
	 * 拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)
	 *
	 * 版本 >= 0
	 */
	 private long Info;

	/**
	 * 版本 >= 0
	 */
	 private short Info_u;

	/**
	 * 分页开始位置, 从0开始
	 *
	 * 版本 >= 20130328
	 */
	 private long offset;

	/**
	 * 
	 *
	 * 版本 >= 20130328
	 */
	 private short offset_u;

	/**
	 * 分页所需数目, 最大值20
	 *
	 * 版本 >= 20130328
	 */
	 private long limit;

	/**
	 * 
	 *
	 * 版本 >= 20130328
	 */
	 private short limit_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushString(PacketName);
		bs.pushUByte(PacketName_u);
		bs.pushLong(PacketStockId);
		bs.pushUByte(PacketStockId_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushUInt(SellerUin);
		bs.pushUByte(SellerUin_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushString(DealCode);
		bs.pushUByte(DealCode_u);
		bs.pushLong(WithdrawId);
		bs.pushUByte(WithdrawId_u);
		bs.pushUInt(MaxFaceValue);
		bs.pushUByte(MaxFaceValue_u);
		bs.pushObject(ApplicableScope);
		bs.pushUByte(ApplicableScope_u);
		bs.pushObject(PacketIdList);
		bs.pushUByte(PacketIdList_u);
		bs.pushUInt(Type);
		bs.pushUByte(Type_u);
		bs.pushUInt(Info);
		bs.pushUByte(Info_u);
		if(  this.version >= 20130328 ){
				bs.pushUInt(offset);
		}
		if(  this.version >= 20130328 ){
				bs.pushUByte(offset_u);
		}
		if(  this.version >= 20130328 ){
				bs.pushUInt(limit);
		}
		if(  this.version >= 20130328 ){
				bs.pushUByte(limit_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		PacketName = bs.popString();
		PacketName_u = bs.popUByte();
		PacketStockId = bs.popLong();
		PacketStockId_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		SellerUin = bs.popUInt();
		SellerUin_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		DealCode = bs.popString();
		DealCode_u = bs.popUByte();
		WithdrawId = bs.popLong();
		WithdrawId_u = bs.popUByte();
		MaxFaceValue = bs.popUInt();
		MaxFaceValue_u = bs.popUByte();
		ApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		ApplicableScope_u = bs.popUByte();
		PacketIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		PacketIdList_u = bs.popUByte();
		Type = bs.popUInt();
		Type_u = bs.popUByte();
		Info = bs.popUInt();
		Info_u = bs.popUByte();
		if(  this.version >= 20130328 ){
				offset = bs.popUInt();
		}
		if(  this.version >= 20130328 ){
				offset_u = bs.popUByte();
		}
		if(  this.version >= 20130328 ){
				limit = bs.popUInt();
		}
		if(  this.version >= 20130328 ){
				limit_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName value 类型为:String
	 * 
	 */
	public String getPacketName()
	{
		return PacketName;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPacketName(String value)
	{
		if (value != null) {
				this.PacketName = value;
				this.PacketName_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketName_u value 类型为:short
	 * 
	 */
	public short getPacketName_u()
	{
		return PacketName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketName_u(short value)
	{
		this.PacketName_u = value;
	}


	/**
	 * 获取红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId value 类型为:long
	 * 
	 */
	public long getPacketStockId()
	{
		return PacketStockId;
	}


	/**
	 * 设置红包批次id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketStockId(long value)
	{
		this.PacketStockId = value;
		this.PacketStockId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketStockId_u value 类型为:short
	 * 
	 */
	public short getPacketStockId_u()
	{
		return PacketStockId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketStockId_u(short value)
	{
		this.PacketStockId_u = value;
	}


	/**
	 * 获取所属者
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置所属者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取对应卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置对应卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
		this.SellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin_u value 类型为:short
	 * 
	 */
	public short getSellerUin_u()
	{
		return SellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerUin_u(short value)
	{
		this.SellerUin_u = value;
	}


	/**
	 * 获取红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取绑定的订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return DealCode;
	}


	/**
	 * 设置绑定的订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		if (value != null) {
				this.DealCode = value;
				this.DealCode_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealCode_u value 类型为:short
	 * 
	 */
	public short getDealCode_u()
	{
		return DealCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealCode_u(short value)
	{
		this.DealCode_u = value;
	}


	/**
	 * 获取兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId value 类型为:long
	 * 
	 */
	public long getWithdrawId()
	{
		return WithdrawId;
	}


	/**
	 * 设置兑现编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWithdrawId(long value)
	{
		this.WithdrawId = value;
		this.WithdrawId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return WithdrawId_u value 类型为:short
	 * 
	 */
	public short getWithdrawId_u()
	{
		return WithdrawId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setWithdrawId_u(short value)
	{
		this.WithdrawId_u = value;
	}


	/**
	 * 获取最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxFaceValue value 类型为:long
	 * 
	 */
	public long getMaxFaceValue()
	{
		return MaxFaceValue;
	}


	/**
	 * 设置最大面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMaxFaceValue(long value)
	{
		this.MaxFaceValue = value;
		this.MaxFaceValue_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MaxFaceValue_u value 类型为:short
	 * 
	 */
	public short getMaxFaceValue_u()
	{
		return MaxFaceValue_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMaxFaceValue_u(short value)
	{
		this.MaxFaceValue_u = value;
	}


	/**
	 * 获取适用范围
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getApplicableScope()
	{
		return ApplicableScope;
	}


	/**
	 * 设置适用范围
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.ApplicableScope = value;
				this.ApplicableScope_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ApplicableScope_u value 类型为:short
	 * 
	 */
	public short getApplicableScope_u()
	{
		return ApplicableScope_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setApplicableScope_u(short value)
	{
		this.ApplicableScope_u = value;
	}


	/**
	 * 获取红包id列表
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getPacketIdList()
	{
		return PacketIdList;
	}


	/**
	 * 设置红包id列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setPacketIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.PacketIdList = value;
				this.PacketIdList_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketIdList_u value 类型为:short
	 * 
	 */
	public short getPacketIdList_u()
	{
		return PacketIdList_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketIdList_u(short value)
	{
		this.PacketIdList_u = value;
	}


	/**
	 * 获取红包类型,红包(1)，店铺代金券(2)
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置红包类型,红包(1)，店铺代金券(2)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
		this.Type_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Type_u value 类型为:short
	 * 
	 */
	public short getType_u()
	{
		return Type_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType_u(short value)
	{
		this.Type_u = value;
	}


	/**
	 * 获取拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)
	 * 
	 * 此字段的版本 >= 0
	 * @return Info value 类型为:long
	 * 
	 */
	public long getInfo()
	{
		return Info;
	}


	/**
	 * 设置拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInfo(long value)
	{
		this.Info = value;
		this.Info_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Info_u value 类型为:short
	 * 
	 */
	public short getInfo_u()
	{
		return Info_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInfo_u(short value)
	{
		this.Info_u = value;
	}


	/**
	 * 获取分页开始位置, 从0开始
	 * 
	 * 此字段的版本 >= 20130328
	 * @return offset value 类型为:long
	 * 
	 */
	public long getOffset()
	{
		return offset;
	}


	/**
	 * 设置分页开始位置, 从0开始
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:long
	 * 
	 */
	public void setOffset(long value)
	{
		this.offset = value;
		this.offset_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130328
	 * @return offset_u value 类型为:short
	 * 
	 */
	public short getOffset_u()
	{
		return offset_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:short
	 * 
	 */
	public void setOffset_u(short value)
	{
		this.offset_u = value;
	}


	/**
	 * 获取分页所需数目, 最大值20
	 * 
	 * 此字段的版本 >= 20130328
	 * @return limit value 类型为:long
	 * 
	 */
	public long getLimit()
	{
		return limit;
	}


	/**
	 * 设置分页所需数目, 最大值20
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:long
	 * 
	 */
	public void setLimit(long value)
	{
		this.limit = value;
		this.limit_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20130328
	 * @return limit_u value 类型为:short
	 * 
	 */
	public short getLimit_u()
	{
		return limit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20130328
	 * @param  value 类型为:short
	 * 
	 */
	public void setLimit_u(short value)
	{
		this.limit_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(RedPacketFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketName);  //计算字段PacketName的长度 size_of(String)
				length += 1;  //计算字段PacketName_u的长度 size_of(uint8_t)
				length += 17;  //计算字段PacketStockId的长度 size_of(uint64_t)
				length += 1;  //计算字段PacketStockId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段SellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DealCode);  //计算字段DealCode的长度 size_of(String)
				length += 1;  //计算字段DealCode_u的长度 size_of(uint8_t)
				length += 17;  //计算字段WithdrawId的长度 size_of(uint64_t)
				length += 1;  //计算字段WithdrawId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段MaxFaceValue的长度 size_of(uint32_t)
				length += 1;  //计算字段MaxFaceValue_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ApplicableScope);  //计算字段ApplicableScope的长度 size_of(Set)
				length += 1;  //计算字段ApplicableScope_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(PacketIdList);  //计算字段PacketIdList的长度 size_of(Vector)
				length += 1;  //计算字段PacketIdList_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 1;  //计算字段Type_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Info的长度 size_of(uint32_t)
				length += 1;  //计算字段Info_u的长度 size_of(uint8_t)
				if(  this.version >= 20130328 ){
						length += 4;  //计算字段offset的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130328 ){
						length += 1;  //计算字段offset_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20130328 ){
						length += 4;  //计算字段limit的长度 size_of(uint32_t)
				}
				if(  this.version >= 20130328 ){
						length += 1;  //计算字段limit_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20130328所包含的字段*******
 *	long version;///<版本号
 *	short version_u;
 *	String PacketName;///<红包名称
 *	short PacketName_u;
 *	long PacketStockId;///<红包批次id
 *	short PacketStockId_u;
 *	long OwnerUin;///<所属者
 *	short OwnerUin_u;
 *	long SellerUin;///<对应卖家号
 *	short SellerUin_u;
 *	long State;///<红包状态,待使用(100),使用中--绑定和冻结(101)，已使用(102),过期(2)
 *	short State_u;
 *	String DealCode;///<绑定的订单id
 *	short DealCode_u;
 *	long WithdrawId;///<兑现编号
 *	short WithdrawId_u;
 *	long MaxFaceValue;///<最大面值
 *	short MaxFaceValue_u;
 *	Set<uint32_t> ApplicableScope;///<适用范围
 *	short ApplicableScope_u;
 *	Vector<uint64_t> PacketIdList;///<红包id列表
 *	short PacketIdList_u;
 *	long Type;///<红包类型,红包(1)，店铺代金券(2)
 *	short Type_u;
 *	long Info;///<拉取信息类型，基本信息(0x01),批次(0x02),兑现信息(0x04),详细信息(0x08),代金券购买(0x1)
 *	short Info_u;
 *	long offset;///<分页开始位置, 从0开始
 *	short offset_u;
 *	long limit;///<分页所需数目, 最大值20
 *	short limit_u;
 *****以上是版本20130328所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
