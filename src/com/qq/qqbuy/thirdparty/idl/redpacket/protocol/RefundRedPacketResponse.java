//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.RefundRedPacketResp.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *退回红包回复
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class RefundRedPacketResponse  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 退回红包错误列表
	 *
	 * 版本 >= 0
	 */
	 private Map<uint64_t,uint32_t> RefundRedPacketFailed = new HashMap<uint64_t,uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short RefundRedPacketFailed_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushObject(RefundRedPacketFailed);
		bs.pushUByte(RefundRedPacketFailed_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		RefundRedPacketFailed = (Map<uint64_t,uint32_t>)bs.popMap(uint64_t.class,uint32_t.class);
		RefundRedPacketFailed_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取退回红包错误列表
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundRedPacketFailed value 类型为:Map<uint64_t,uint32_t>
	 * 
	 */
	public Map<uint64_t,uint32_t> getRefundRedPacketFailed()
	{
		return RefundRedPacketFailed;
	}


	/**
	 * 设置退回红包错误列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint64_t,uint32_t>
	 * 
	 */
	public void setRefundRedPacketFailed(Map<uint64_t,uint32_t> value)
	{
		if (value != null) {
				this.RefundRedPacketFailed = value;
				this.RefundRedPacketFailed_u = 1;
		}
	}

	public boolean issetRefundRedPacketFailed()
	{
		return this.RefundRedPacketFailed_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundRedPacketFailed_u value 类型为:short
	 * 
	 */
	public short getRefundRedPacketFailed_u()
	{
		return RefundRedPacketFailed_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRefundRedPacketFailed_u(short value)
	{
		this.RefundRedPacketFailed_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(RefundRedPacketResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RefundRedPacketFailed, null);  //计算字段RefundRedPacketFailed的长度 size_of(Map)
				length += 1;  //计算字段RefundRedPacketFailed_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(RefundRedPacketResponse)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(RefundRedPacketFailed, encoding);  //计算字段RefundRedPacketFailed的长度 size_of(Map)
				length += 1;  //计算字段RefundRedPacketFailed_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
