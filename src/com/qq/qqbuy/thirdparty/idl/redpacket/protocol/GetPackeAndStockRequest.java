//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.GetPackeAndStockReq.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *获取红包和批次列表请求
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class GetPackeAndStockRequest  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 获取者uin
	 *
	 * 版本 >= 0
	 */
	 private long ReqUin;

	/**
	 * 版本 >= 0
	 */
	 private short ReqUin_u;

	/**
	 * 获取请求过滤器
	 *
	 * 版本 >= 0
	 */
	 private PacketAndStockReqFilter ReqFilter = new PacketAndStockReqFilter();

	/**
	 * 版本 >= 0
	 */
	 private short ReqFilter_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(ReqUin);
		bs.pushUByte(ReqUin_u);
		bs.pushObject(ReqFilter);
		bs.pushUByte(ReqFilter_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		ReqUin = bs.popUInt();
		ReqUin_u = bs.popUByte();
		ReqFilter = (PacketAndStockReqFilter) bs.popObject(PacketAndStockReqFilter.class);
		ReqFilter_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取获取者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqUin value 类型为:long
	 * 
	 */
	public long getReqUin()
	{
		return ReqUin;
	}


	/**
	 * 设置获取者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReqUin(long value)
	{
		this.ReqUin = value;
		this.ReqUin_u = 1;
	}

	public boolean issetReqUin()
	{
		return this.ReqUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqUin_u value 类型为:short
	 * 
	 */
	public short getReqUin_u()
	{
		return ReqUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReqUin_u(short value)
	{
		this.ReqUin_u = value;
	}


	/**
	 * 获取获取请求过滤器
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqFilter value 类型为:PacketAndStockReqFilter
	 * 
	 */
	public PacketAndStockReqFilter getReqFilter()
	{
		return ReqFilter;
	}


	/**
	 * 设置获取请求过滤器
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PacketAndStockReqFilter
	 * 
	 */
	public void setReqFilter(PacketAndStockReqFilter value)
	{
		if (value != null) {
				this.ReqFilter = value;
				this.ReqFilter_u = 1;
		}
	}

	public boolean issetReqFilter()
	{
		return this.ReqFilter_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqFilter_u value 类型为:short
	 * 
	 */
	public short getReqFilter_u()
	{
		return ReqFilter_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReqFilter_u(short value)
	{
		this.ReqFilter_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPackeAndStockRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ReqUin的长度 size_of(uint32_t)
				length += 1;  //计算字段ReqUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReqFilter, null);  //计算字段ReqFilter的长度 size_of(PacketAndStockReqFilter)
				length += 1;  //计算字段ReqFilter_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPackeAndStockRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段ReqUin的长度 size_of(uint32_t)
				length += 1;  //计算字段ReqUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReqFilter, encoding);  //计算字段ReqFilter的长度 size_of(PacketAndStockReqFilter)
				length += 1;  //计算字段ReqFilter_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
