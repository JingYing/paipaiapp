//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.FreezeRedPacketReq.java

package com.qq.qqbuy.thirdparty.idl.redpacket.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Map;
import java.util.HashMap;

/**
 *冻结红包请求
 *
 *@date 2014-10-16 05:56:15
 *
 *@since version:0
*/
public class FreezeRedPacketRequest  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 冻结红包用户uin
	 *
	 * 版本 >= 0
	 */
	 private long OwnerUin;

	/**
	 * 版本 >= 0
	 */
	 private short OwnerUin_u;

	/**
	 * 冻结红包列表
	 *
	 * 版本 >= 0
	 */
	 private Map<uint64_t,String> FreezeRedPacketInfo = new HashMap<uint64_t,String>();

	/**
	 * 版本 >= 0
	 */
	 private short FreezeRedPacketInfo_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(OwnerUin);
		bs.pushUByte(OwnerUin_u);
		bs.pushObject(FreezeRedPacketInfo);
		bs.pushUByte(FreezeRedPacketInfo_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		OwnerUin = bs.popUInt();
		OwnerUin_u = bs.popUByte();
		FreezeRedPacketInfo = (Map<uint64_t,String>)bs.popMap(uint64_t.class,String.class);
		FreezeRedPacketInfo_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取冻结红包用户uin
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin value 类型为:long
	 * 
	 */
	public long getOwnerUin()
	{
		return OwnerUin;
	}


	/**
	 * 设置冻结红包用户uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOwnerUin(long value)
	{
		this.OwnerUin = value;
		this.OwnerUin_u = 1;
	}

	public boolean issetOwnerUin()
	{
		return this.OwnerUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OwnerUin_u value 类型为:short
	 * 
	 */
	public short getOwnerUin_u()
	{
		return OwnerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOwnerUin_u(short value)
	{
		this.OwnerUin_u = value;
	}


	/**
	 * 获取冻结红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @return FreezeRedPacketInfo value 类型为:Map<uint64_t,String>
	 * 
	 */
	public Map<uint64_t,String> getFreezeRedPacketInfo()
	{
		return FreezeRedPacketInfo;
	}


	/**
	 * 设置冻结红包列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint64_t,String>
	 * 
	 */
	public void setFreezeRedPacketInfo(Map<uint64_t,String> value)
	{
		if (value != null) {
				this.FreezeRedPacketInfo = value;
				this.FreezeRedPacketInfo_u = 1;
		}
	}

	public boolean issetFreezeRedPacketInfo()
	{
		return this.FreezeRedPacketInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return FreezeRedPacketInfo_u value 类型为:short
	 * 
	 */
	public short getFreezeRedPacketInfo_u()
	{
		return FreezeRedPacketInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFreezeRedPacketInfo_u(short value)
	{
		this.FreezeRedPacketInfo_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FreezeRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(FreezeRedPacketInfo, null);  //计算字段FreezeRedPacketInfo的长度 size_of(Map)
				length += 1;  //计算字段FreezeRedPacketInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(FreezeRedPacketRequest)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OwnerUin的长度 size_of(uint32_t)
				length += 1;  //计算字段OwnerUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(FreezeRedPacketInfo, encoding);  //计算字段FreezeRedPacketInfo的长度 size_of(Map)
				length += 1;  //计算字段FreezeRedPacketInfo_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
