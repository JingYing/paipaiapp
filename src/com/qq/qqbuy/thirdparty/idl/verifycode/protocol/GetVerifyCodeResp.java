 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;

/**
 * 获取验证码回复
 *
 *@date 2011-09-29 10:15::58
 *
 *@since version:0
*/
public class  GetVerifyCodeResp extends NetMessage
{
	/**
	 * 返回信息
	 *
	 * 版本 >= 0
	 */
	 private String retMsg = new String();

	/**
	 * 图片内容
	 *
	 * 版本 >= 0
	 */
	 private byte[] pic = new byte[1];

	/**
	 * 签名
	 *
	 * 版本 >= 0
	 */
	 private String sig = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(retMsg);
		bs.pushBytes(pic);
		bs.pushString(sig);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retMsg = bs.popString();
		pic = bs.popBytes();
		sig = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91068801L;
	}


	/**
	 * 获取返回信息
	 * 
	 * 此字段的版本 >= 0
	 * @return retMsg value 类型为:String
	 * 
	 */
	public String getRetMsg()
	{
		return retMsg;
	}


	/**
	 * 设置返回信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRetMsg(String value)
	{
		if (value != null) {
				this.retMsg = value;
		}else{
				this.retMsg = new String();
		}
	}


	/**
	 * 获取图片内容
	 * 
	 * 此字段的版本 >= 0
	 * @return pic value 类型为:String
	 * 
	 */
	public byte[] getPic()
	{
		return pic;
	}


	/**
	 * 设置图片内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPic(byte[] value)
	{
		if(value == null)
			return ;
		this.pic = value;
	}


	/**
	 * 获取签名
	 * 
	 * 此字段的版本 >= 0
	 * @return sig value 类型为:String
	 * 
	 */
	public String getSig()
	{
		return sig;
	}


	/**
	 * 设置签名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSig(String value)
	{
		if (value != null) {
				this.sig = value;
		}else{
				this.sig = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetVerifyCodeResp)
				length += ByteStream.getObjectSize(retMsg);  //计算字段retMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(pic);  //计算字段pic的长度 size_of(String)
				length += ByteStream.getObjectSize(sig);  //计算字段sig的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return "result=" + result + " retMsg=" + retMsg + " picSize=" + pic.length + " sig=" + sig;
	}
}
