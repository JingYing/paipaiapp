 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.verifycode.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;
import com.qq.qqbuy.common.po.QQBuyServiceComm;

/**
 * 检查验证码请求
 *
 *@date 2011-09-29 10:15::58
 *
 *@since version:0
*/
public class  CheckVerifyCodeReq extends NetMessage
{
	/**
	 * 包含来源，uin等信息
	 *
	 * 版本 >= 0
	 */
	 private QQBuyServiceComm commParm = new QQBuyServiceComm();

	/**
	 * appid
	 *
	 * 版本 >= 0
	 */
	 private long appId;

	/**
	 * 图片复杂度 0 , 1, 2 数字越高代表复杂程度越高
	 *
	 * 版本 >= 0
	 */
	 private short capType;

	/**
	 * 图片值
	 *
	 * 版本 >= 0
	 */
	 private String code = new String();

	/**
	 * 时间范围内是否合法
	 *
	 * 版本 >= 0
	 */
	 private long validTime;

	/**
	 * 签名
	 *
	 * 版本 >= 0
	 */
	 private String sig = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(commParm);
		bs.pushUInt(appId);
		bs.pushUByte(capType);
		bs.pushString(code);
		bs.pushUInt(validTime);
		bs.pushString(sig);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		commParm = (QQBuyServiceComm) bs.popObject(QQBuyServiceComm.class);
		appId = bs.popUInt();
		capType = bs.popUByte();
		code = bs.popString();
		validTime = bs.popUInt();
		sig = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91061802L;
	}


	/**
	 * 获取包含来源，uin等信息
	 * 
	 * 此字段的版本 >= 0
	 * @return commParm value 类型为:QQBuyServiceComm
	 * 
	 */
	public QQBuyServiceComm getCommParm()
	{
		return commParm;
	}


	/**
	 * 设置包含来源，uin等信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:QQBuyServiceComm
	 * 
	 */
	public void setCommParm(QQBuyServiceComm value)
	{
		if (value != null) {
				this.commParm = value;
		}else{
				this.commParm = new QQBuyServiceComm();
		}
	}


	/**
	 * 获取appid
	 * 
	 * 此字段的版本 >= 0
	 * @return appId value 类型为:long
	 * 
	 */
	public long getAppId()
	{
		return appId;
	}


	/**
	 * 设置appid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAppId(long value)
	{
		this.appId = value;
	}


	/**
	 * 获取图片复杂度 0 , 1, 2 数字越高代表复杂程度越高
	 * 
	 * 此字段的版本 >= 0
	 * @return capType value 类型为:short
	 * 
	 */
	public short getCapType()
	{
		return capType;
	}


	/**
	 * 设置图片复杂度 0 , 1, 2 数字越高代表复杂程度越高
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCapType(short value)
	{
		this.capType = value;
	}


	/**
	 * 获取图片值
	 * 
	 * 此字段的版本 >= 0
	 * @return code value 类型为:String
	 * 
	 */
	public String getCode()
	{
		return code;
	}


	/**
	 * 设置图片值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCode(String value)
	{
		if (value != null) {
				this.code = value;
		}else{
				this.code = new String();
		}
	}


	/**
	 * 获取时间范围内是否合法
	 * 
	 * 此字段的版本 >= 0
	 * @return validTime value 类型为:long
	 * 
	 */
	public long getValidTime()
	{
		return validTime;
	}


	/**
	 * 设置时间范围内是否合法
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setValidTime(long value)
	{
		this.validTime = value;
	}


	/**
	 * 获取签名
	 * 
	 * 此字段的版本 >= 0
	 * @return sig value 类型为:String
	 * 
	 */
	public String getSig()
	{
		return sig;
	}


	/**
	 * 设置签名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSig(String value)
	{
		if (value != null) {
				this.sig = value;
		}else{
				this.sig = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckVerifyCodeReq)
				length += ByteStream.getObjectSize(commParm);  //计算字段commParm的长度 size_of(QQBuyServiceComm)
				length += 4;  //计算字段appId的长度 size_of(uint32_t)
				length += 1;  //计算字段capType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(code);  //计算字段code的长度 size_of(String)
				length += 4;  //计算字段validTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sig);  //计算字段sig的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return "commParm=[" + commParm.toString() + "] appId=" + appId + " capType=" + capType + " code=" + code + " validTime=" + validTime + " sig=" + sig;
	}
}
