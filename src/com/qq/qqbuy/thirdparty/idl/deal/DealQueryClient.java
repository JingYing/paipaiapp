package com.qq.qqbuy.thirdparty.idl.deal;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerCloseCodDealReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerCloseCodDealResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealDetailByDealIDReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealDetailByDealIDResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealListByConditionReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.BuyerGetDealListByConditionResp;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinReq;
import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinResp;

//货到付款查询客户端
public class DealQueryClient extends SupportIDLBaseClient {

	public static GetAllStateDealCountByUinResp getAllStateDealCountByUin(
			GetAllStateDealCountByUinReq allStateReq) {
		GetAllStateDealCountByUinResp resp = new GetAllStateDealCountByUinResp();
		int ret;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if (EnvManager.isIdc()) {
			ret = invokePaiPaiIDL(allStateReq, resp, stub);
		} else {
			ret = invokePaiPaiIDL(allStateReq, resp, stub, GAMMA_IP, 9110);
		}
		return ret == SUCCESS ? resp : null;
	}

	public static BuyerGetDealListByConditionResp getDealListByCondition(
			BuyerGetDealListByConditionReq req) {
		BuyerGetDealListByConditionResp resp = new BuyerGetDealListByConditionResp();
		int ret;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
		if (EnvManager.isIdc()) {
			ret = invokePaiPaiIDL(req, resp, stub);
		} else {
			ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9110);
		}
		return ret == SUCCESS ? resp : null;
	}

	public static BuyerGetDealDetailByDealIDResp getDealDetailByDealID(
			BuyerGetDealDetailByDealIDReq req) {

		BuyerGetDealDetailByDealIDResp resp = new BuyerGetDealDetailByDealIDResp();
		int ret;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if (EnvManager.isIdc()) {
			ret = invokePaiPaiIDL(req, resp, stub);
		} else {
			ret = invokePaiPaiIDL(req, resp, stub, GAMMA_IP, 9110);
		}
		return ret == SUCCESS ? resp : null;
	}

	public static BuyerCloseCodDealResp closeDeal(long uin, String dealId,
			long closeReason) {
		BuyerCloseCodDealReq req = new BuyerCloseCodDealReq();
		BuyerCloseCodDealResp res = new BuyerCloseCodDealResp();
		req.setUin(uin);
		req.setDealCodeList(dealId);
		req.setCodCloseReason((int) closeReason);
		req.setOpType(1);
		req.setSource(CALL_IDL_SOURCE);

		int ret = -1;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if (EnvManager.isIdc()) {
			ret = invokePaiPaiIDL(req, res, stub);
		} else {
			ret = invokePaiPaiIDL(req, res, stub, GAMMA_IP, 9110);
		}
		return ret == SUCCESS ? res : null;
	}
}
