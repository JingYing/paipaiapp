 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  GetAllStateDealCountByUinReq implements IServiceObject
{
	/**
	 * 查询QQ号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 查询类型：0：包括拍拍所有订单，货到付款订单，在线支付订单三种类型的所有状态下订单数量
	 * 1: 拍拍订单 
	 * 21：卖家uin查货到付款订单
	 * 22：买家uin查货到付款订单
	 * 31：卖家uin查在线支付订单
	 * 32：买家uin查在线支付订单
	 *
	 * 版本 >= 0
	 */
	 private int dealType;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(uin);
		bs.pushInt(dealType);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		uin = bs.popUInt();
		dealType = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081801L;
	}


	/**
	 * 获取查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置查询QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取查询类型：0：包括拍拍所有订单，货到付款订单，在线支付订单三种类型的所有状态下订单数量1: 拍拍订单21：卖家uin查货到付款订单22：买家uin查货到付款订单31：卖家uin查在线支付订单32：买家uin查在线支付订单
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType value 类型为:int
	 * 
	 */
	public int getDealType()
	{
		return dealType;
	}


	/**
	 * 设置查询类型：0：包括拍拍所有订单，货到付款订单，在线支付订单三种类型的所有状态下订单数量1: 拍拍订单21：卖家uin查货到付款订单22：买家uin查货到付款订单31：卖家uin查在线支付订单32：买家uin查在线支付订单
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setDealType(int value)
	{
		this.dealType = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetAllStateDealCountByUinReq)
				length += 4;  //计算字段uin的长度 size_of(long)
				length += 4;  //计算字段dealType的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
