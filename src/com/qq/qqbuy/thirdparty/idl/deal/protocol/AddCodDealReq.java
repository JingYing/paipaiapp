 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-08-22 02:47::41
 *
 *@since version:1
*/
public class  AddCodDealReq implements IServiceObject
{
	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private String dealId = new String();

	/**
	 * 订单生成时间
	 *
	 * 版本 >= 0
	 */
	 private String dealCreateTime = new String();

	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 买家附言
	 *
	 * 版本 >= 0
	 */
	 private String buyerNote = new String();

	/**
	 * 订单来源信息
	 *
	 * 版本 >= 0
	 */
	 private String referer = new String();

	/**
	 * 商品编号
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String stockAttr = new String();

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long buyAmount;

	/**
	 * 费用合计
	 *
	 * 版本 >= 0
	 */
	 private long payFeeTotal;

	/**
	 * 收获地址_编号
	 *
	 * 版本 >= 0
	 */
	 private long recvAddrId;

	/**
	 * 收货地址_姓名
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrName = new String();

	/**
	 * 收货地址_地址
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrDetail = new String();

	/**
	 * 收货地址_邮编
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrPostcode = new String();

	/**
	 * 收货地址_电话
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrPhone = new String();

	/**
	 * 收货地址_手机
	 *
	 * 版本 >= 0
	 */
	 private String recvAddrMobile = new String();

	/**
	 * 保留字段，目前用于存soso和直通车pps参数，格式为name1//pps1//name2//pps2
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(dealId);
		bs.pushString(dealCreateTime);
		bs.pushUInt(sellerUin);
		bs.pushUInt(buyerUin);
		bs.pushString(buyerNote);
		bs.pushString(referer);
		bs.pushString(itemCode);
		bs.pushString(stockAttr);
		bs.pushUInt(buyAmount);
		bs.pushUInt(payFeeTotal);
		bs.pushUInt(recvAddrId);
		bs.pushString(recvAddrName);
		bs.pushString(recvAddrDetail);
		bs.pushString(recvAddrPostcode);
		bs.pushString(recvAddrPhone);
		bs.pushString(recvAddrMobile);
		bs.pushString(reserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dealId = bs.popString();
		dealCreateTime = bs.popString();
		sellerUin = bs.popUInt();
		buyerUin = bs.popUInt();
		buyerNote = bs.popString();
		referer = bs.popString();
		itemCode = bs.popString();
		stockAttr = bs.popString();
		buyAmount = bs.popUInt();
		payFeeTotal = bs.popUInt();
		recvAddrId = bs.popUInt();
		recvAddrName = bs.popString();
		recvAddrDetail = bs.popString();
		recvAddrPostcode = bs.popString();
		recvAddrPhone = bs.popString();
		recvAddrMobile = bs.popString();
		reserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001801L;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return dealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return dealId;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		if (value != null) {
				this.dealId = value;
		}else{
				this.dealId = new String();
		}
	}


	/**
	 * 获取订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCreateTime value 类型为:String
	 * 
	 */
	public String getDealCreateTime()
	{
		return dealCreateTime;
	}


	/**
	 * 设置订单生成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCreateTime(String value)
	{
		if (value != null) {
				this.dealCreateTime = value;
		}else{
				this.dealCreateTime = new String();
		}
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取买家附言
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerNote value 类型为:String
	 * 
	 */
	public String getBuyerNote()
	{
		return buyerNote;
	}


	/**
	 * 设置买家附言
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNote(String value)
	{
		if (value != null) {
				this.buyerNote = value;
		}else{
				this.buyerNote = new String();
		}
	}


	/**
	 * 获取订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @return referer value 类型为:String
	 * 
	 */
	public String getReferer()
	{
		return referer;
	}


	/**
	 * 设置订单来源信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReferer(String value)
	{
		if (value != null) {
				this.referer = value;
		}else{
				this.referer = new String();
		}
	}


	/**
	 * 获取商品编号
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		if (value != null) {
				this.itemCode = value;
		}else{
				this.itemCode = new String();
		}
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return stockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return stockAttr;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		if (value != null) {
				this.stockAttr = value;
		}else{
				this.stockAttr = new String();
		}
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return buyAmount value 类型为:long
	 * 
	 */
	public long getBuyAmount()
	{
		return buyAmount;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyAmount(long value)
	{
		this.buyAmount = value;
	}


	/**
	 * 获取费用合计
	 * 
	 * 此字段的版本 >= 0
	 * @return payFeeTotal value 类型为:long
	 * 
	 */
	public long getPayFeeTotal()
	{
		return payFeeTotal;
	}


	/**
	 * 设置费用合计
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayFeeTotal(long value)
	{
		this.payFeeTotal = value;
	}


	/**
	 * 获取收获地址_编号
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrId value 类型为:long
	 * 
	 */
	public long getRecvAddrId()
	{
		return recvAddrId;
	}


	/**
	 * 设置收获地址_编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvAddrId(long value)
	{
		this.recvAddrId = value;
	}


	/**
	 * 获取收货地址_姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrName value 类型为:String
	 * 
	 */
	public String getRecvAddrName()
	{
		return recvAddrName;
	}


	/**
	 * 设置收货地址_姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrName(String value)
	{
		if (value != null) {
				this.recvAddrName = value;
		}else{
				this.recvAddrName = new String();
		}
	}


	/**
	 * 获取收货地址_地址
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrDetail value 类型为:String
	 * 
	 */
	public String getRecvAddrDetail()
	{
		return recvAddrDetail;
	}


	/**
	 * 设置收货地址_地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrDetail(String value)
	{
		if (value != null) {
				this.recvAddrDetail = value;
		}else{
				this.recvAddrDetail = new String();
		}
	}


	/**
	 * 获取收货地址_邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrPostcode value 类型为:String
	 * 
	 */
	public String getRecvAddrPostcode()
	{
		return recvAddrPostcode;
	}


	/**
	 * 设置收货地址_邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrPostcode(String value)
	{
		if (value != null) {
				this.recvAddrPostcode = value;
		}else{
				this.recvAddrPostcode = new String();
		}
	}


	/**
	 * 获取收货地址_电话
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrPhone value 类型为:String
	 * 
	 */
	public String getRecvAddrPhone()
	{
		return recvAddrPhone;
	}


	/**
	 * 设置收货地址_电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrPhone(String value)
	{
		if (value != null) {
				this.recvAddrPhone = value;
		}else{
				this.recvAddrPhone = new String();
		}
	}


	/**
	 * 获取收货地址_手机
	 * 
	 * 此字段的版本 >= 0
	 * @return recvAddrMobile value 类型为:String
	 * 
	 */
	public String getRecvAddrMobile()
	{
		return recvAddrMobile;
	}


	/**
	 * 设置收货地址_手机
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecvAddrMobile(String value)
	{
		if (value != null) {
				this.recvAddrMobile = value;
		}else{
				this.recvAddrMobile = new String();
		}
	}


	/**
	 * 获取保留字段，目前用于存soso和直通车pps参数，格式为name1//pps1//name2//pps2
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留字段，目前用于存soso和直通车pps参数，格式为name1//pps1//name2//pps2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		if (value != null) {
				this.reserve = value;
		}else{
				this.reserve = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddCodDealReq)
				length += ByteStream.getObjectSize(dealId);  //计算字段dealId的长度 size_of(String)
				length += ByteStream.getObjectSize(dealCreateTime);  //计算字段dealCreateTime的长度 size_of(String)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(buyerNote);  //计算字段buyerNote的长度 size_of(String)
				length += ByteStream.getObjectSize(referer);  //计算字段referer的长度 size_of(String)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(stockAttr);  //计算字段stockAttr的长度 size_of(String)
				length += 4;  //计算字段buyAmount的长度 size_of(long)
				length += 4;  //计算字段payFeeTotal的长度 size_of(long)
				length += 4;  //计算字段recvAddrId的长度 size_of(long)
				length += ByteStream.getObjectSize(recvAddrName);  //计算字段recvAddrName的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrDetail);  //计算字段recvAddrDetail的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrPostcode);  //计算字段recvAddrPostcode的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrPhone);  //计算字段recvAddrPhone的长度 size_of(String)
				length += ByteStream.getObjectSize(recvAddrMobile);  //计算字段recvAddrMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve);  //计算字段reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
