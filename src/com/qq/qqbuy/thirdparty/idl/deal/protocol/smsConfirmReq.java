 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-03-04 02:59::17
 *
 *@since version:1
*/
public class  smsConfirmReq implements IServiceObject, java.io.Serializable
{
	/**
	 * 订单编号
	 *
	 * 版本 >= 0
	 */
	 private String dealId = new String();

	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(dealId);
		bs.pushUInt(sellerUin);
		bs.pushUInt(buyerUin);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dealId = bs.popString();
		sellerUin = bs.popUInt();
		buyerUin = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001802L;
	}


	/**
	 * 获取订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @return dealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return dealId;
	}


	/**
	 * 设置订单编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		if (value != null) {
				this.dealId = value;
		}else{
				this.dealId = new String();
		}
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(smsConfirmReq)
				length += ByteStream.getObjectSize(dealId);  //计算字段dealId的长度 size_of(String)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "smsConfirmReq [dealId=" + dealId + ", sellerUin=" + sellerUin + ", buyerUin=" + buyerUin + "]";
    }

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
