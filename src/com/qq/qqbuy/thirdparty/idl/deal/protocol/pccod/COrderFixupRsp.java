 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu,skyzhuang

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *下单回复
 *
 *@date 2013-01-17 03:33::47
 *
 *@since version:0
*/
public class COrderFixupRsp  implements ICanSerializeObject
{
	/**
	 * 总费用
	 *
	 * 版本 >= 0
	 */
	 private long TotalFee;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private long ItemPrice;

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private long BuyNum;

	/**
	 * 商品code
	 *
	 * 版本 >= 0
	 */
	 private long ItemCode;

	/**
	 * 商品title
	 *
	 * 版本 >= 0
	 */
	 private String ItemTitle = new String();

	/**
	 * 库存属性
	 *
	 * 版本 >= 0
	 */
	 private String StockAttr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short TotalFee_u;

	/**
	 * 版本 >= 0
	 */
	 private short DealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short ItemPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short BuyNum_u;

	/**
	 * 版本 >= 0
	 */
	 private short ItemCode_u;

	/**
	 * 版本 >= 0
	 */
	 private short ItemTitle_u;

	/**
	 * 版本 >= 0
	 */
	 private short StockAttr_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(TotalFee);
		bs.pushString(DealId);
		bs.pushUInt(ItemPrice);
		bs.pushUInt(BuyNum);
		bs.pushUInt(ItemCode);
		bs.pushString(ItemTitle);
		bs.pushString(StockAttr);
		bs.pushUByte(TotalFee_u);
		bs.pushUByte(DealId_u);
		bs.pushUByte(ItemPrice_u);
		bs.pushUByte(BuyNum_u);
		bs.pushUByte(ItemCode_u);
		bs.pushUByte(ItemTitle_u);
		bs.pushUByte(StockAttr_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		TotalFee = bs.popUInt();
		DealId = bs.popString();
		ItemPrice = bs.popUInt();
		BuyNum = bs.popUInt();
		ItemCode = bs.popUInt();
		ItemTitle = bs.popString();
		StockAttr = bs.popString();
		TotalFee_u = bs.popUByte();
		DealId_u = bs.popUByte();
		ItemPrice_u = bs.popUByte();
		BuyNum_u = bs.popUByte();
		ItemCode_u = bs.popUByte();
		ItemTitle_u = bs.popUByte();
		StockAttr_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取总费用
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFee value 类型为:long
	 * 
	 */
	public long getTotalFee()
	{
		return TotalFee;
	}


	/**
	 * 设置总费用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalFee(long value)
	{
		this.TotalFee = value;
		this.TotalFee_u = 1;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
		this.DealId_u = 1;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPrice value 类型为:long
	 * 
	 */
	public long getItemPrice()
	{
		return ItemPrice;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemPrice(long value)
	{
		this.ItemPrice = value;
		this.ItemPrice_u = 1;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyNum value 类型为:long
	 * 
	 */
	public long getBuyNum()
	{
		return BuyNum;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyNum(long value)
	{
		this.BuyNum = value;
		this.BuyNum_u = 1;
	}


	/**
	 * 获取商品code
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemCode value 类型为:long
	 * 
	 */
	public long getItemCode()
	{
		return ItemCode;
	}


	/**
	 * 设置商品code
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemCode(long value)
	{
		this.ItemCode = value;
		this.ItemCode_u = 1;
	}


	/**
	 * 获取商品title
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitle value 类型为:String
	 * 
	 */
	public String getItemTitle()
	{
		return ItemTitle;
	}


	/**
	 * 设置商品title
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemTitle(String value)
	{
		this.ItemTitle = value;
		this.ItemTitle_u = 1;
	}


	/**
	 * 获取库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return StockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return StockAttr;
	}


	/**
	 * 设置库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		this.StockAttr = value;
		this.StockAttr_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalFee_u value 类型为:short
	 * 
	 */
	public short getTotalFee_u()
	{
		return TotalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTotalFee_u(short value)
	{
		this.TotalFee_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId_u value 类型为:short
	 * 
	 */
	public short getDealId_u()
	{
		return DealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealId_u(short value)
	{
		this.DealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPrice_u value 类型为:short
	 * 
	 */
	public short getItemPrice_u()
	{
		return ItemPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemPrice_u(short value)
	{
		this.ItemPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyNum_u value 类型为:short
	 * 
	 */
	public short getBuyNum_u()
	{
		return BuyNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyNum_u(short value)
	{
		this.BuyNum_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemCode_u value 类型为:short
	 * 
	 */
	public short getItemCode_u()
	{
		return ItemCode_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemCode_u(short value)
	{
		this.ItemCode_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemTitle_u value 类型为:short
	 * 
	 */
	public short getItemTitle_u()
	{
		return ItemTitle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemTitle_u(short value)
	{
		this.ItemTitle_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StockAttr_u value 类型为:short
	 * 
	 */
	public short getStockAttr_u()
	{
		return StockAttr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStockAttr_u(short value)
	{
		this.StockAttr_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(COrderFixupRsp)
				length += 4;  //计算字段TotalFee的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段ItemPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段ItemCode的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ItemTitle);  //计算字段ItemTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(StockAttr);  //计算字段StockAttr的长度 size_of(String)
				length += 1;  //计算字段TotalFee_u的长度 size_of(uint8_t)
				length += 1;  //计算字段DealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段BuyNum_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemCode_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ItemTitle_u的长度 size_of(uint8_t)
				length += 1;  //计算字段StockAttr_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
