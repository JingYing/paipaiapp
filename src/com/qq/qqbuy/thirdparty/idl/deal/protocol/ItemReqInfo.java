//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *下单时商品信息
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class ItemReqInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 商品属性
	 *
	 * 版本 >= 0
	 */
	 private String itemStockAttr = new String();

	/**
	 * 购买数量
	 *
	 * 版本 >= 0
	 */
	 private int buyCount = 1;

	/**
	 * 价格类型
	 *
	 * 版本 >= 0
	 */
	 private int priceType;

	/**
	 * 红包ID
	 *
	 * 版本 >= 0
	 */
	 private int redPacketId;

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(sellerUin);
		bs.pushString(itemCode);
		bs.pushString(itemStockAttr);
		bs.pushInt(buyCount);
		bs.pushInt(priceType);
		bs.pushInt(redPacketId);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		sellerUin = bs.popUInt();
		itemCode = bs.popString();
		itemStockAttr = bs.popString();
		buyCount = bs.popInt();
		priceType = bs.popInt();
		redPacketId = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取商品属性
	 * 
	 * 此字段的版本 >= 0
	 * @return itemStockAttr value 类型为:String
	 * 
	 */
	public String getItemStockAttr()
	{
		return itemStockAttr;
	}


	/**
	 * 设置商品属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemStockAttr(String value)
	{
		this.itemStockAttr = value;
	}


	/**
	 * 获取购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @return buyCount value 类型为:int
	 * 
	 */
	public int getBuyCount()
	{
		return buyCount;
	}


	/**
	 * 设置购买数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyCount(int value)
	{
		this.buyCount = value;
	}


	/**
	 * 获取价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @return priceType value 类型为:int
	 * 
	 */
	public int getPriceType()
	{
		return priceType;
	}


	/**
	 * 设置价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPriceType(int value)
	{
		this.priceType = value;
	}


	/**
	 * 获取红包ID
	 * 
	 * 此字段的版本 >= 0
	 * @return redPacketId value 类型为:int
	 * 
	 */
	public int getRedPacketId()
	{
		return redPacketId;
	}


	/**
	 * 设置红包ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRedPacketId(int value)
	{
		this.redPacketId = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemReqInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段sellerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(itemStockAttr);  //计算字段itemStockAttr的长度 size_of(String)
				length += 4;  //计算字段buyCount的长度 size_of(int)
				length += 4;  //计算字段priceType的长度 size_of(int)
				length += 4;  //计算字段redPacketId的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
