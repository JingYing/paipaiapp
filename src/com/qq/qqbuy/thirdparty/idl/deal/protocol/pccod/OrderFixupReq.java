 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu,skyzhuang

package com.qq.qqbuy.thirdparty.idl.deal.protocol.pccod;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *一口价确认下单请求
 *
 *@date 2013-01-17 03:33::47
 *
 *@since version:0
*/
public class  OrderFixupReq implements IServiceObject
{
	/**
	 * 请求来源描述
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 确认下单请求
	 *
	 * 版本 >= 0
	 */
	 private COrderFixupReq OReq = new COrderFixupReq();

	/**
	 * 请求MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushObject(OReq);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		OReq = (COrderFixupReq) bs.popObject(COrderFixupReq.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x97771002L;
	}


	/**
	 * 获取请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求来源描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取确认下单请求
	 * 
	 * 此字段的版本 >= 0
	 * @return OReq value 类型为:COrderFixupReq
	 * 
	 */
	public COrderFixupReq getOReq()
	{
		return OReq;
	}


	/**
	 * 设置确认下单请求
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:COrderFixupReq
	 * 
	 */
	public void setOReq(COrderFixupReq value)
	{
		if (value != null) {
				this.OReq = value;
		}else{
				this.OReq = new COrderFixupReq();
		}
	}


	/**
	 * 获取请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置请求MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(OrderFixupReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(OReq);  //计算字段OReq的长度 size_of(COrderFixupReq)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
