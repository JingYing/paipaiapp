 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  GetDealTraceStrResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 订单来源名称
	 *
	 * 版本 >= 0
	 */
	 private String sourceName = new String();

	/**
	 * 订单来源pps，如soso pps
	 *
	 * 版本 >= 0
	 */
	 private String sourcePps = new String();

	/**
	 * 订单上报名称
	 *
	 * 版本 >= 0
	 */
	 private String destName = new String();

	/**
	 * 订单上报pps，如焦点pps
	 *
	 * 版本 >= 0
	 */
	 private String destPps = new String();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushString(sourceName);
		bs.pushString(sourcePps);
		bs.pushString(destName);
		bs.pushString(destPps);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		sourceName = bs.popString();
		sourcePps = bs.popString();
		destName = bs.popString();
		destPps = bs.popString();
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80088825L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取订单来源名称
	 * 
	 * 此字段的版本 >= 0
	 * @return sourceName value 类型为:String
	 * 
	 */
	public String getSourceName()
	{
		return sourceName;
	}


	/**
	 * 设置订单来源名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSourceName(String value)
	{
		this.sourceName = value;
	}


	/**
	 * 获取订单来源pps，如soso pps
	 * 
	 * 此字段的版本 >= 0
	 * @return sourcePps value 类型为:String
	 * 
	 */
	public String getSourcePps()
	{
		return sourcePps;
	}


	/**
	 * 设置订单来源pps，如soso pps
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSourcePps(String value)
	{
		this.sourcePps = value;
	}


	/**
	 * 获取订单上报名称
	 * 
	 * 此字段的版本 >= 0
	 * @return destName value 类型为:String
	 * 
	 */
	public String getDestName()
	{
		return destName;
	}


	/**
	 * 设置订单上报名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDestName(String value)
	{
		this.destName = value;
	}


	/**
	 * 获取订单上报pps，如焦点pps
	 * 
	 * 此字段的版本 >= 0
	 * @return destPps value 类型为:String
	 * 
	 */
	public String getDestPps()
	{
		return destPps;
	}


	/**
	 * 设置订单上报pps，如焦点pps
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDestPps(String value)
	{
		this.destPps = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealTraceStrResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(sourceName);  //计算字段sourceName的长度 size_of(String)
				length += ByteStream.getObjectSize(sourcePps);  //计算字段sourcePps的长度 size_of(String)
				length += ByteStream.getObjectSize(destName);  //计算字段destName的长度 size_of(String)
				length += ByteStream.getObjectSize(destPps);  //计算字段destPps的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
