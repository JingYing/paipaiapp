package com.qq.qqbuy.thirdparty.idl.deal.protocol;


//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java




import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  BuyerCloseCodDealReq implements IServiceObject
{
	/**
	 * 订单编号列表，以分号分割
	 *
	 * 版本 >= 0
	 */
	 private String dealCodeList = new String();

	/**
	 * 关闭者uin
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 货到付款关闭原因
	 *
	 * 版本 >= 0
	 */
	 private int codCloseReason;

	/**
	 * 操作类型，0=只更新手机拍拍db,1=同时更新手机拍拍和拍拍状态
	 *
	 * 版本 >= 0
	 */
	 private int opType;

	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(dealCodeList);
		bs.pushUInt(uin);
		bs.pushInt(codCloseReason);
		bs.pushInt(opType);
		bs.pushString(source);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		dealCodeList = bs.popString();
		uin = bs.popUInt();
		codCloseReason = bs.popInt();
		opType = bs.popInt();
		source = bs.popString();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80081811L;
	}


	/**
	 * 获取订单编号列表，以分号分割
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCodeList value 类型为:String
	 * 
	 */
	public String getDealCodeList()
	{
		return dealCodeList;
	}


	/**
	 * 设置订单编号列表，以分号分割
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCodeList(String value)
	{
		this.dealCodeList = value;
	}


	/**
	 * 获取关闭者uin
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置关闭者uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取货到付款关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return codCloseReason value 类型为:int
	 * 
	 */
	public int getCodCloseReason()
	{
		return codCloseReason;
	}


	/**
	 * 设置货到付款关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCodCloseReason(int value)
	{
		this.codCloseReason = value;
	}


	/**
	 * 获取操作类型，0=只更新手机拍拍db,1=同时更新手机拍拍和拍拍状态
	 * 
	 * 此字段的版本 >= 0
	 * @return opType value 类型为:int
	 * 
	 */
	public int getOpType()
	{
		return opType;
	}


	/**
	 * 设置操作类型，0=只更新手机拍拍db,1=同时更新手机拍拍和拍拍状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setOpType(int value)
	{
		this.opType = value;
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(BuyerCloseCodDealReq)
				length += ByteStream.getObjectSize(dealCodeList);  //计算字段dealCodeList的长度 size_of(String)
				length += 4;  //计算字段uin的长度 size_of(long)
				length += 4;  //计算字段codCloseReason的长度 size_of(int)
				length += 4;  //计算字段opType的长度 size_of(int)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
