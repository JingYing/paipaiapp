//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *下单时邮递信息
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:20110816
*/
public class MailReqInfo  implements ICanSerializeObject
{
	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110816;

	/**
	 * 邮递类型[0=无需物流, 1=平邮, 2=快递, 3=ems]
	 *
	 * 版本 >= 0
	 */
	 private int mailType = 1;

	/**
	 * 收货地址ID
	 *
	 * 版本 >= 0
	 */
	 private int addressId;

	/**
	 * 收货人姓名
	 *
	 * 版本 >= 0
	 */
	 private String receiveName = new String();

	/**
	 * 收货地址
	 *
	 * 版本 >= 0
	 */
	 private String receiveAddress = new String();

	/**
	 * 收货人手机号
	 *
	 * 版本 >= 0
	 */
	 private String receiveMobile = new String();

	/**
	 * 收货人电话
	 *
	 * 版本 >= 0
	 */
	 private String receivePhone = new String();

	/**
	 * 收货邮编
	 *
	 * 版本 >= 0
	 */
	 private String receivePost = new String();

	/**
	 * 发票标题
	 *
	 * 版本 >= 0
	 */
	 private String innoviceTitle = new String();

	/**
	 * req保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(mailType);
		bs.pushInt(addressId);
		bs.pushString(receiveName);
		bs.pushString(receiveAddress);
		bs.pushString(receiveMobile);
		bs.pushString(receivePhone);
		bs.pushString(receivePost);
		bs.pushString(innoviceTitle);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		mailType = bs.popInt();
		addressId = bs.popInt();
		receiveName = bs.popString();
		receiveAddress = bs.popString();
		receiveMobile = bs.popString();
		receivePhone = bs.popString();
		receivePost = bs.popString();
		innoviceTitle = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取邮递类型[0=无需物流, 1=平邮, 2=快递, 3=ems]
	 * 
	 * 此字段的版本 >= 0
	 * @return mailType value 类型为:int
	 * 
	 */
	public int getMailType()
	{
		return mailType;
	}


	/**
	 * 设置邮递类型[0=无需物流, 1=平邮, 2=快递, 3=ems]
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setMailType(int value)
	{
		this.mailType = value;
	}


	/**
	 * 获取收货地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @return addressId value 类型为:int
	 * 
	 */
	public int getAddressId()
	{
		return addressId;
	}


	/**
	 * 设置收货地址ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAddressId(int value)
	{
		this.addressId = value;
	}


	/**
	 * 获取收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @return receiveName value 类型为:String
	 * 
	 */
	public String getReceiveName()
	{
		return receiveName;
	}


	/**
	 * 设置收货人姓名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveName(String value)
	{
		this.receiveName = value;
	}


	/**
	 * 获取收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @return receiveAddress value 类型为:String
	 * 
	 */
	public String getReceiveAddress()
	{
		return receiveAddress;
	}


	/**
	 * 设置收货地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveAddress(String value)
	{
		this.receiveAddress = value;
	}


	/**
	 * 获取收货人手机号
	 * 
	 * 此字段的版本 >= 0
	 * @return receiveMobile value 类型为:String
	 * 
	 */
	public String getReceiveMobile()
	{
		return receiveMobile;
	}


	/**
	 * 设置收货人手机号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceiveMobile(String value)
	{
		this.receiveMobile = value;
	}


	/**
	 * 获取收货人电话
	 * 
	 * 此字段的版本 >= 0
	 * @return receivePhone value 类型为:String
	 * 
	 */
	public String getReceivePhone()
	{
		return receivePhone;
	}


	/**
	 * 设置收货人电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceivePhone(String value)
	{
		this.receivePhone = value;
	}


	/**
	 * 获取收货邮编
	 * 
	 * 此字段的版本 >= 0
	 * @return receivePost value 类型为:String
	 * 
	 */
	public String getReceivePost()
	{
		return receivePost;
	}


	/**
	 * 设置收货邮编
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceivePost(String value)
	{
		this.receivePost = value;
	}


	/**
	 * 获取发票标题
	 * 
	 * 此字段的版本 >= 0
	 * @return innoviceTitle value 类型为:String
	 * 
	 */
	public String getInnoviceTitle()
	{
		return innoviceTitle;
	}


	/**
	 * 设置发票标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInnoviceTitle(String value)
	{
		this.innoviceTitle = value;
	}


	/**
	 * 获取req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置req保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MailReqInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段mailType的长度 size_of(int)
				length += 4;  //计算字段addressId的长度 size_of(int)
				length += ByteStream.getObjectSize(receiveName);  //计算字段receiveName的长度 size_of(String)
				length += ByteStream.getObjectSize(receiveAddress);  //计算字段receiveAddress的长度 size_of(String)
				length += ByteStream.getObjectSize(receiveMobile);  //计算字段receiveMobile的长度 size_of(String)
				length += ByteStream.getObjectSize(receivePhone);  //计算字段receivePhone的长度 size_of(String)
				length += ByteStream.getObjectSize(receivePost);  //计算字段receivePost的长度 size_of(String)
				length += ByteStream.getObjectSize(innoviceTitle);  //计算字段innoviceTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
