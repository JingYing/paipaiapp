 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.deal.idl.DealService.java

package com.qq.qqbuy.thirdparty.idl.deal.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2012-12-07 10:51:37
 *
 *@since version:1
*/
public class  SellerGetDealListByConditionResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码,errCode为0表示调用成功,非0表示错误
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 符合条件总数
	 *
	 * 版本 >= 0
	 */
	 private int rowCount;

	/**
	 * 卖家订单详情查询结果
	 *
	 * 版本 >= 0
	 */
	 private List<DealInfo> sellerDealList = new ArrayList<DealInfo>();

	/**
	 * resp保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushInt(rowCount);
		bs.pushObject(sellerDealList);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		rowCount = bs.popInt();
		sellerDealList = (List<DealInfo>)bs.popList(ArrayList.class,DealInfo.class);
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80088806L;
	}


	/**
	 * 获取错误码,errCode为0表示调用成功,非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码,errCode为0表示调用成功,非0表示错误
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @return rowCount value 类型为:int
	 * 
	 */
	public int getRowCount()
	{
		return rowCount;
	}


	/**
	 * 设置符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRowCount(int value)
	{
		this.rowCount = value;
	}


	/**
	 * 获取卖家订单详情查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerDealList value 类型为:List<DealInfo>
	 * 
	 */
	public List<DealInfo> getSellerDealList()
	{
		return sellerDealList;
	}


	/**
	 * 设置卖家订单详情查询结果
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<DealInfo>
	 * 
	 */
	public void setSellerDealList(List<DealInfo> value)
	{
		if (value != null) {
				this.sellerDealList = value;
		}else{
				this.sellerDealList = new ArrayList<DealInfo>();
		}
	}


	/**
	 * 获取resp保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置resp保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SellerGetDealListByConditionResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段rowCount的长度 size_of(int)
				length += ByteStream.getObjectSize(sellerDealList);  //计算字段sellerDealList的长度 size_of(List)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
