//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.shippingFeeV2.TemplateDetailInfo.java

package com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

/**
 *
 *
 *@date 2013-08-22 11:20:23
 *
 *@since version:0
*/
public class TemplateSimpleInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本 >= 0
	 */
	 private long id;

	/**
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 版本 >= 0
	 */
	 private short innerId;

	/**
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 版本 >= 0
	 */
	 private String name = "";

	/**
	 * 版本 >= 0
	 */
	 private String desc = "";

	/**
	 * 版本 >= 0
	 */
	 private long createTime;

	/**
	 * 版本 >= 0
	 */
	 private long lastModifyTime;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(id);
		bs.pushUInt(uin);
		bs.pushUByte(innerId);
		bs.pushUInt(property);
		bs.pushString(name);
		bs.pushString(desc);
		bs.pushUInt(createTime);
		bs.pushUInt(lastModifyTime);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		id = bs.popUInt();
		uin = bs.popUInt();
		innerId = bs.popUByte();
		property = bs.popUInt();
		name = bs.popString();
		desc = bs.popString();
		createTime = bs.popUInt();
		lastModifyTime = bs.popUInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return id;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.id = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return innerId value 类型为:short
	 * 
	 */
	public short getInnerId()
	{
		return innerId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerId(short value)
	{
		this.innerId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.createTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:long
	 * 
	 */
	public long getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifyTime(long value)
	{
		this.lastModifyTime = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateSimpleInfo)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 1;  //计算字段innerId的长度 size_of(uint8_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(name, null);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(desc, null);  //计算字段desc的长度 size_of(String)
				length += 4;  //计算字段createTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(TemplateSimpleInfo)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 1;  //计算字段innerId的长度 size_of(uint8_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(name, encoding);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(desc, encoding);  //计算字段desc的长度 size_of(String)
				length += 4;  //计算字段createTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
