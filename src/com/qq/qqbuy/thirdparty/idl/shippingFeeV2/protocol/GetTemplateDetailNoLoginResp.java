 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.shippingFeeV2.ShippingfeeService.java

package com.qq.qqbuy.thirdparty.idl.shippingFeeV2.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-08-22 11:20:23
 *
 *@since version:1
*/
public class  GetTemplateDetailNoLoginResp implements IServiceObject
{
	public long result;
	/**
	 * 运费模板详情
	 *
	 * 版本 >= 0
	 */
	 private TemplateDetailInfo detailInfo = new TemplateDetailInfo();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(detailInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		detailInfo = (TemplateDetailInfo) bs.popObject(TemplateDetailInfo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22108822L;
	}


	/**
	 * 获取运费模板详情
	 * 
	 * 此字段的版本 >= 0
	 * @return detailInfo value 类型为:TemplateDetailInfo
	 * 
	 */
	public TemplateDetailInfo getDetailInfo()
	{
		return detailInfo;
	}


	/**
	 * 设置运费模板详情
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:TemplateDetailInfo
	 * 
	 */
	public void setDetailInfo(TemplateDetailInfo value)
	{
		if (value != null) {
				this.detailInfo = value;
		}else{
				this.detailInfo = new TemplateDetailInfo();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetTemplateDetailNoLoginResp)
				length += ByteStream.getObjectSize(detailInfo, null);  //计算字段detailInfo的长度 size_of(TemplateDetailInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetTemplateDetailNoLoginResp)
				length += ByteStream.getObjectSize(detailInfo, encoding);  //计算字段detailInfo的长度 size_of(TemplateDetailInfo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
