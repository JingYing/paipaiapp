package com.qq.qqbuy.thirdparty.idl.favorite;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace = "c2cent::ao::favorite", needInit = true)
public class FavoritePo
{
	@Member(desc = "收藏店铺请求PO", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true )
	public class AddFavShopPo {
		@Field(desc = "版本号", defaultValue="0")
		uint32_t version;
		
		@Field(desc = "被收藏的店铺的id，即卖家qq号")
		uint32_t ShopId;
		
		@Field(version=0 , desc = "标记位,当收藏一个已经被收藏过的店铺时，收藏ao会判断该标记位，为1：则收藏成功，并更新已被收藏的店铺的收藏时间；为0:如果店铺已经被收藏过，则返回失败")
		uint32_t AddFlag;

		uint8_t ShopId_u;
		
		uint8_t AddFlag_u;
		
		uint8_t version_u;
	}
	
	@Member(desc = "收藏商品请求PO", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true )
	public class AddFavItemPo{
		@Field(desc = "版本号", defaultValue="0" )
		uint32_t version;
		
		@Field(desc = "被收藏的商品的id，长度为32个16进制编码的字符,如:3745F6320000000000373B2D07BC7EA0(因为字母要求全大写)，不能输入商品快照")
		String   ItemId;
		
		@Field(desc = "标记位,当收藏一个已经被收藏过的商品时，收藏ao会判断该标记位，为1：则收藏成功，并更新被收藏商品的收藏时间；为0:如果商品已经被收藏过，则返回失败")
		uint32_t AddFlag;
		
		uint8_t ItemId_u;
		
		uint8_t AddFlag_u;
		
		uint8_t version_u;
	}
	
	@Member(desc = "收藏店铺查询filter", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true )
	public class FavShopFilter{
		@Field(desc = "版本号", defaultValue="0" )
		uint32_t version;
		
		@Field(desc = "被收藏的店铺的id，即卖家qq号")
		uint32_t ShopId;
		
		@Field(desc = "用户号码，收藏店铺的用户，带登录态接口中，UserId必须为当前用户的id")
		uint32_t UserId;
		
		uint8_t UserId_u;
		
		uint8_t ShopId_u;
		
		uint8_t version_u;
	}
	
	@Member(desc = "收藏店铺查询结果", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true )
	public class FavShopPo{
		@Field(desc = "版本号", defaultValue="20130411" )
		uint32_t version;
		
		@Field(desc = "收藏店铺的用户")
		uint32_t UserId;
		
		@Field(version=0 ,desc = "被收藏的店铺的id，即卖家qq号")
		uint32_t ShopId;
		
		@Field(desc = "收藏该店铺的时间")
		uint32_t AddTime;		
	
		uint8_t UserId_u;
		
		uint8_t ShopId_u;
		
		uint8_t AddTime_u;
		
		uint8_t version_u;
	
		@Field(desc = "店铺名称" , version=20130411)
		String shopName;
		
		@Field(desc = "店铺图片", version=20130411)
		String shopLogoPos;
		
		@Field(desc = "店铺卖家昵称", version=20130411)
		String sellerNickName;
		
		@Field(desc = "店铺信用", version=20130411)
		uint32_t shopCredit;
		
		@Field(desc = "店铺好评", version=20130411)
		String shopGoodRate;
		
		@Field(desc = "店铺收藏人数", version=20130411)
		uint32_t favoriteHeat;
		
		@Field(desc = "动态数字", version=20130411)
		Vector<uint32_t> dynamicNum;
		 
		@Field(desc = "tencent 标识", version=20130411)
		String sigTencent;

		@Field(desc = "paipai 标识", version=20130411)
		String sigUI;

		@Field(desc = "属性字段", version=20130411)
		String shopProperty;

		@Field(desc = "用户标签", version=20130411)
		String label;

		@Field(desc = "预留字段", version=20130411)
		String reserved;
	
		@Field(version=20130411)
		uint8_t shopName_u;
		@Field(version=20130411)
		uint8_t shopLogoPos_u;
		@Field(version=20130411)
		uint8_t sellerNickName_u;
		@Field(version=20130411)
		uint8_t shopCredit_u;
		@Field(version=20130411)
		uint8_t shopGoodRate_u;
		@Field(version=20130411)
		uint8_t favoriteHeat_u;
		@Field(version=20130411)
		uint8_t dynamicNum_u;
		@Field(version=20130411)
		uint8_t sigTencent_u;
		@Field(version=20130411)
		uint8_t sigUI_u;
		@Field(version=20130411)
		uint8_t shopProperty_u;
		@Field(version=20130411)
		uint8_t label_u;
		@Field(version=20130411)
		uint8_t reserved_u;

	}
	
	@Member(desc = "收藏查询filter", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true )
	public class FavFilterPo
	{
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "用户qq号")
		uint32_t uin;
		
		@Field(desc = "页面大小，即请求数量")
		uint32_t pageSize;
		
		@Field(desc = "页码")
		uint32_t startPage;
		
		@Field(desc = "取收藏信息标志位")
		uint64_t option;
		
		@Field(desc = "版本号_u")
		uint8_t version_u;
		
		@Field(desc = "用户qq号_u")
		uint8_t uin_u;
		
		@Field(desc = "页面大小_u")
		uint8_t pageSize_u;
		
		@Field(desc = "请求页面_u")
		uint8_t startPage_u;
		
		@Field(desc = "取收藏信息标志位")
		uint8_t option_u;
	}
	
	@Member(desc = "返回查询收藏的商品信息", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true)
	public class FavItemPo
	{
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "商品Id")
		String itemId;
		
		@Field(desc = "收藏时的价格")
		uint32_t oldPrice;
		
		@Field(desc = "当前价格")
		uint32_t price;
		
		@Field(desc = "商品名称")
		String itemTitle;
		
		@Field(desc = "商品图片")
		String itemPic;
		
		@Field(desc = "当前状态")
		uint32_t state;
		
		@Field(desc = "收藏时间")
		uint32_t	addTime;
		
		@Field(desc = "属性字段")
		String	itemProperty;
		
		@Field(desc = "预留字段")
		String reserved;	
		
		@Field(desc = "版本号_u")
		uint8_t version_u;
		
		@Field(desc = "商品Id_u")
		uint8_t itemId_u;
		
		@Field(desc = "商品最低价格_u")
		uint8_t oldPrice_u;
		
		@Field(desc = "当前价格")
		uint8_t price_u;
		
		@Field(desc = "商品名称")
		uint8_t itemTitle_u;
		
		@Field(desc = "商品图片")
		uint8_t itemPic_u;
		
		@Field(desc = "当前状态")
		uint8_t state_u;
		
		@Field(desc = "收藏时间_u")
		uint8_t addTime_u;
		
		@Field(desc = "预留商品属性_u")
		uint8_t itemProperty_u;
		
		@Field(desc = "预留字段_u")
		uint8_t reserved_u;
		
	}
	@Member(desc = "返回查询收藏的商品信息列表", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true)
	public class FavItemListPo
	{
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "商品信息")
		Vector<FavItemPo> favItemList;
		
		@Field(desc = "总数")
		uint32_t total;
		
		@Field(desc = "预留字段")
		String reserved;	
		
		@Field(desc = "版本号_u")
		uint8_t version_u;
		
		@Field(desc = "商品信息_u")
		uint8_t favItemList_u;
		
		@Field(desc = "总数_u")
		uint8_t total_u;
		
		@Field(desc = "预留字段_u")
		uint8_t reserved_u;
	}
	
	@Member(desc = "返回查询收藏的店铺信息列表", cPlusNamespace = "c2cent::po::favorite", isNeedUFlag = true)
	public class FavShopListPo
	{
		@Field(desc = "版本号", defaultValue = "0")
		uint32_t version;
		
		@Field(desc = "商品信息")
		Vector<FavShopPo> favShopList;
		
		@Field(desc = "总数")
		uint32_t total;
		
		@Field(desc = "预留字段")
		String reserved;	
		
		@Field(desc = "版本号_u")
		uint8_t version_u;
		
		@Field(desc = "店铺信息_u")
		uint8_t favShopList_u;
		
		@Field(desc = "总数_u")
		uint8_t total_u;
		
		@Field(desc = "预留字段_u")
		uint8_t reserved_u;
	}
}