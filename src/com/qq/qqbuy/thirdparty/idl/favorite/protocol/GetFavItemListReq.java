 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *取商品信息列表请求类
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  GetFavItemListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String mechineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 查询filter
	 *
	 * 版本 >= 0
	 */
	 private FavFilterPo favItemFiter = new FavFilterPo();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(mechineKey);
		bs.pushString(source);
		bs.pushObject(favItemFiter);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		mechineKey = bs.popString();
		source = bs.popString();
		favItemFiter = (FavFilterPo) bs.popObject(FavFilterPo.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69091804L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return mechineKey value 类型为:String
	 * 
	 */
	public String getMechineKey()
	{
		return mechineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMechineKey(String value)
	{
		this.mechineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取查询filter
	 * 
	 * 此字段的版本 >= 0
	 * @return favItemFiter value 类型为:FavFilterPo
	 * 
	 */
	public FavFilterPo getFavItemFiter()
	{
		return favItemFiter;
	}


	/**
	 * 设置查询filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:FavFilterPo
	 * 
	 */
	public void setFavItemFiter(FavFilterPo value)
	{
		if (value != null) {
				this.favItemFiter = value;
		}else{
				this.favItemFiter = new FavFilterPo();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFavItemListReq)
				length += ByteStream.getObjectSize(mechineKey);  //计算字段mechineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(favItemFiter);  //计算字段favItemFiter的长度 size_of(FavFilterPo)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

    @Override
    public String toString()
    {
        return "GetFavItemListReq [favItemFiter=" + favItemFiter
                + ", inReserve=" + inReserve + ", mechineKey=" + mechineKey
                + ", source=" + source + "]";
    }
	
	

}
