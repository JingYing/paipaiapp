 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoriteApi.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *查询指定用户是否收藏了指定店铺
 *
 *@date 2012-12-26 03:41:56
 *
 *@since version:0
*/
public class  GetFavShopDetailReq implements IServiceObject
{
	/**
	 * 机器码(即visitkey)，不能为空
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 请求方来源,请填为调用方的代码文件名.不能为空,为空则返回失败
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 调用方业务id，不能为空,请找基础组申请
	 *
	 * 版本 >= 0
	 */
	 private long BusinessId;

	/**
	 * 查询收藏店铺fiter，具体定义参考FavoritePo文件
	 *
	 * 版本 >= 0
	 */
	 private FavShopFilter FavShopFilter = new FavShopFilter();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(MachineKey);
		bs.pushString(Source);
		bs.pushUInt(BusinessId);
		bs.pushObject(FavShopFilter);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		MachineKey = bs.popString();
		Source = bs.popString();
		BusinessId = bs.popUInt();
		FavShopFilter = (FavShopFilter) bs.popObject(FavShopFilter.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x69091802L;
	}


	/**
	 * 获取机器码(即visitkey)，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置机器码(即visitkey)，不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取请求方来源,请填为调用方的代码文件名.不能为空,为空则返回失败
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置请求方来源,请填为调用方的代码文件名.不能为空,为空则返回失败
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取调用方业务id，不能为空,请找基础组申请
	 * 
	 * 此字段的版本 >= 0
	 * @return BusinessId value 类型为:long
	 * 
	 */
	public long getBusinessId()
	{
		return BusinessId;
	}


	/**
	 * 设置调用方业务id，不能为空,请找基础组申请
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBusinessId(long value)
	{
		this.BusinessId = value;
	}


	/**
	 * 获取查询收藏店铺fiter，具体定义参考FavoritePo文件
	 * 
	 * 此字段的版本 >= 0
	 * @return FavShopFilter value 类型为:FavShopFilter
	 * 
	 */
	public FavShopFilter getFavShopFilter()
	{
		return FavShopFilter;
	}


	/**
	 * 设置查询收藏店铺fiter，具体定义参考FavoritePo文件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:FavShopFilter
	 * 
	 */
	public void setFavShopFilter(FavShopFilter value)
	{
		if (value != null) {
				this.FavShopFilter = value;
		}else{
				this.FavShopFilter = new FavShopFilter();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFavShopDetailReq)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += 4;  //计算字段BusinessId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavShopFilter);  //计算字段FavShopFilter的长度 size_of(FavShopFilter)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
