//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *收藏店铺查询filter
 *
 *@date 2012-12-26 03:41:48
 *
 *@since version:0
*/
public class FavShopFilter  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 被收藏的店铺的id，即卖家qq号
	 *
	 * 版本 >= 0
	 */
	 private long ShopId;

	/**
	 * 用户号码，收藏店铺的用户，带登录态接口中，UserId必须为当前用户的id
	 *
	 * 版本 >= 0
	 */
	 private long UserId;

	/**
	 * 版本 >= 0
	 */
	 private short UserId_u;

	/**
	 * 版本 >= 0
	 */
	 private short ShopId_u;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(ShopId);
		bs.pushUInt(UserId);
		bs.pushUByte(UserId_u);
		bs.pushUByte(ShopId_u);
		bs.pushUByte(version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		ShopId = bs.popUInt();
		UserId = bs.popUInt();
		UserId_u = bs.popUByte();
		ShopId_u = bs.popUByte();
		version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取被收藏的店铺的id，即卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopId value 类型为:long
	 * 
	 */
	public long getShopId()
	{
		return ShopId;
	}


	/**
	 * 设置被收藏的店铺的id，即卖家qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShopId(long value)
	{
		this.ShopId = value;
		this.ShopId_u = 1;
	}


	/**
	 * 获取用户号码，收藏店铺的用户，带登录态接口中，UserId必须为当前用户的id
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId value 类型为:long
	 * 
	 */
	public long getUserId()
	{
		return UserId;
	}


	/**
	 * 设置用户号码，收藏店铺的用户，带登录态接口中，UserId必须为当前用户的id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserId(long value)
	{
		this.UserId = value;
		this.UserId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UserId_u value 类型为:short
	 * 
	 */
	public short getUserId_u()
	{
		return UserId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUserId_u(short value)
	{
		this.UserId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ShopId_u value 类型为:short
	 * 
	 */
	public short getShopId_u()
	{
		return ShopId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setShopId_u(short value)
	{
		this.ShopId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavShopFilter)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段ShopId的长度 size_of(uint32_t)
				length += 4;  //计算字段UserId的长度 size_of(uint32_t)
				length += 1;  //计算字段UserId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段ShopId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
