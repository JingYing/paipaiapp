//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.favorite.ao.idl.FavoritePo.java

package com.qq.qqbuy.thirdparty.idl.favorite.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *收藏商品请求PO
 *
 *@date 2012-12-26 03:41:48
 *
 *@since version:0
*/
public class AddFavItemPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 被收藏的商品的id，长度为32个16进制编码的字符,如:3745F6320000000000373B2D07BC7EA0(因为字母要求全大写)，不能输入商品快照
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 标记位,当收藏一个已经被收藏过的商品时，收藏ao会判断该标记位，为1：则收藏成功，并更新被收藏商品的收藏时间；为0:如果商品已经被收藏过，则返回失败
	 *
	 * 版本 >= 0
	 */
	 private long AddFlag;

	/**
	 * 版本 >= 0
	 */
	 private short ItemId_u;

	/**
	 * 版本 >= 0
	 */
	 private short AddFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(ItemId);
		bs.pushUInt(AddFlag);
		bs.pushUByte(ItemId_u);
		bs.pushUByte(AddFlag_u);
		bs.pushUByte(version_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		ItemId = bs.popString();
		AddFlag = bs.popUInt();
		ItemId_u = bs.popUByte();
		AddFlag_u = bs.popUByte();
		version_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取被收藏的商品的id，长度为32个16进制编码的字符,如:3745F6320000000000373B2D07BC7EA0(因为字母要求全大写)，不能输入商品快照
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置被收藏的商品的id，长度为32个16进制编码的字符,如:3745F6320000000000373B2D07BC7EA0(因为字母要求全大写)，不能输入商品快照
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.ItemId = value;
		this.ItemId_u = 1;
	}


	/**
	 * 获取标记位,当收藏一个已经被收藏过的商品时，收藏ao会判断该标记位，为1：则收藏成功，并更新被收藏商品的收藏时间；为0:如果商品已经被收藏过，则返回失败
	 * 
	 * 此字段的版本 >= 0
	 * @return AddFlag value 类型为:long
	 * 
	 */
	public long getAddFlag()
	{
		return AddFlag;
	}


	/**
	 * 设置标记位,当收藏一个已经被收藏过的商品时，收藏ao会判断该标记位，为1：则收藏成功，并更新被收藏商品的收藏时间；为0:如果商品已经被收藏过，则返回失败
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddFlag(long value)
	{
		this.AddFlag = value;
		this.AddFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return ItemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.ItemId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddFlag_u value 类型为:short
	 * 
	 */
	public short getAddFlag_u()
	{
		return AddFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddFlag_u(short value)
	{
		this.AddFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(AddFavItemPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += 4;  //计算字段AddFlag的长度 size_of(uint32_t)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段AddFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
