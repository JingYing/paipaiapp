package com.qq.qqbuy.thirdparty.idl;

import com.paipai.component.c2cplatform.IServiceObject;

/* @FileName: IDLResult.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2013-8-6 下午04:52:33    
 */
public class IDLResult<T extends IServiceObject> {
	
	private T result;
	private long invokeErrCode;
	private long bizErrCode;
	
	private final int RET_SUCCESS = 0;
	private final int RET_NULL_RESULT = -99;
	
	

	public IDLResult(T result, long invokeErrCode, long bizErrCode) {
		this.result = result;
		this.invokeErrCode = invokeErrCode;
		this.bizErrCode = bizErrCode;
	}

	public boolean isSucceed() {
		if (result != null && invokeErrCode ==RET_SUCCESS && bizErrCode == RET_SUCCESS) {
			return true;
		}
		return false;
	}
	
	public long  getErrCode() {
		if (!isSucceed()) {
			if (result == null) {
				return RET_NULL_RESULT;
			} else {
				return invokeErrCode != RET_SUCCESS ? invokeErrCode : bizErrCode;
			}
		}
		return RET_SUCCESS;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public long getInvokeErrCode() {
		return invokeErrCode;
	}

	public void setInvokeErrCode(int invokeErrCode) {
		this.invokeErrCode = invokeErrCode;
	}

	public long getBizErrCode() {
		return bizErrCode;
	}

	public void setBizErrCode(int bizErrCode) {
		this.bizErrCode = bizErrCode;
	}

	@Override
	public String toString() {
		return "IDLResult [result=" + result + ", invokeErrCode="
				+ invokeErrCode + ", bizErrCode=" + bizErrCode
				+ ", RET_SUCCESS=" + RET_SUCCESS + ", RET_NULL_RESULT="
				+ RET_NULL_RESULT + "]";
	}
}
