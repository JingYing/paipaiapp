 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *获取查件记录列表 resp
 *
 *@date 2014-12-19 11:53:14
 *
 *@since version:0
*/
public class  DelQueryHistoryResp extends NetMessage
{
	/**
	 * 用于存储debug信息
	 *
	 * 版本 >= 0
	 */
	 private String RetMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(RetMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		RetMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x1fb38804L;
	}


	/**
	 * 获取用于存储debug信息
	 * 
	 * 此字段的版本 >= 0
	 * @return RetMsg value 类型为:String
	 * 
	 */
	public String getRetMsg()
	{
		return RetMsg;
	}


	/**
	 * 设置用于存储debug信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRetMsg(String value)
	{
		this.RetMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DelQueryHistoryResp)
				length += ByteStream.getObjectSize(RetMsg);  //计算字段RetMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
