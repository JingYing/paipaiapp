//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.logistics.protocol.QueryTrackInfo.java

package com.qq.qqbuy.thirdparty.idl.logistics.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *包裹跟踪详情步骤
 *
 *@date 2014-12-19 11:53:13
 *
 *@since version:0
*/
public class TrackStep  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 时间
	 *
	 * 版本 >= 0
	 */
	 private String EventTime = new String();

	/**
	 * step描述
	 *
	 * 版本 >= 0
	 */
	 private String Desc = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 版本 >= 0
	 */
	 private short EventTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short Desc_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushString(EventTime);
		bs.pushString(Desc);
		bs.pushUByte(Version_u);
		bs.pushUByte(EventTime_u);
		bs.pushUByte(Desc_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		EventTime = bs.popString();
		Desc = bs.popString();
		Version_u = bs.popUByte();
		EventTime_u = bs.popUByte();
		Desc_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EventTime value 类型为:String
	 * 
	 */
	public String getEventTime()
	{
		return EventTime;
	}


	/**
	 * 设置时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setEventTime(String value)
	{
		this.EventTime = value;
		this.EventTime_u = 1;
	}


	/**
	 * 获取step描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return Desc;
	}


	/**
	 * 设置step描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.Desc = value;
		this.Desc_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EventTime_u value 类型为:short
	 * 
	 */
	public short getEventTime_u()
	{
		return EventTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEventTime_u(short value)
	{
		this.EventTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return Desc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.Desc_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TrackStep)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(EventTime);  //计算字段EventTime的长度 size_of(String)
				length += ByteStream.getObjectSize(Desc);  //计算字段Desc的长度 size_of(String)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段EventTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段Desc_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
