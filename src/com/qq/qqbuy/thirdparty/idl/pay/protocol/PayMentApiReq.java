 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.pay.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *付款Api请求
 *
 *@date 2010-10-25 04:18::19
 *
 *@since version:0
*/
public class  PayMentApiReq implements IServiceObject
{
	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 订单号
	 *
	 * 版本 >= 0
	 */
	 private String dealCode = new String();

	/**
	 * 订单类型
	 *
	 * 版本 >= 0
	 */
	 private long type;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(dealCode);
		bs.pushUInt(type);
		bs.pushString(reserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		dealCode = bs.popString();
		type = bs.popUInt();
		reserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x84021806L;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		if (value != null) {
				this.source = value;
		}else{
				this.source = new String();
		}
	}


	/**
	 * 获取订单号
	 * 
	 * 此字段的版本 >= 0
	 * @return dealCode value 类型为:String
	 * 
	 */
	public String getDealCode()
	{
		return dealCode;
	}


	/**
	 * 设置订单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealCode(String value)
	{
		if (value != null) {
				this.dealCode = value;
		}else{
				this.dealCode = new String();
		}
	}


	/**
	 * 获取订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return type;
	}


	/**
	 * 设置订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.type = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		if (value != null) {
				this.reserve = value;
		}else{
				this.reserve = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(PayMentApiReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(dealCode);  //计算字段dealCode的长度 size_of(String)
				length += 4;  //计算字段type的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(reserve);  //计算字段reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
