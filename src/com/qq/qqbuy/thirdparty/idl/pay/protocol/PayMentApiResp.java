 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.pay.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *付款Api请求回复
 *
 *@date 2010-10-25 04:18::19
 *
 *@since version:0
*/
public class  PayMentApiResp implements IServiceObject
{
	public long result;
	/**
	 * 返回字段
	 *
	 * 版本 >= 0
	 */
	 private String retMsg = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(retMsg);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		retMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x84028806L;
	}


	/**
	 * 获取返回字段
	 * 
	 * 此字段的版本 >= 0
	 * @return retMsg value 类型为:String
	 * 
	 */
	public String getRetMsg()
	{
		return retMsg;
	}


	/**
	 * 设置返回字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRetMsg(String value)
	{
		if (value != null) {
				this.retMsg = value;
		}else{
				this.retMsg = new String();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PayMentApiResp)
				length += ByteStream.getObjectSize(retMsg);  //计算字段retMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
