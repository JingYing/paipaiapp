

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.netframework.kernal.NetAction;
import com.paipai.util.annotation.Protocol;
/**
 *
 *NetAction类
 *
 */
public class PayRecommendAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x877f1802L, desc = "根据订单获取可支付方式", export = true)
	 public GetUseablePayListResp GetUseablePayList(GetUseablePayListReq req){
		GetUseablePayListResp resp = new  GetUseablePayListResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x877f1801L, desc = "获取用户推荐列表", export = true)
	 public GetPayRecommendListResp GetPayRecommendList(GetPayRecommendListReq req){
		GetPayRecommendListResp resp = new  GetPayRecommendListResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
