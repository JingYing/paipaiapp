 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.PayRecommendAo.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *获取用户推荐列表响应
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class  GetPayRecommendListResp extends NetMessage
{
	/**
	 * 支付方式推荐列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<PayRecommend> payRecommendList = new Vector<PayRecommend>();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(payRecommendList);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		payRecommendList = (Vector<PayRecommend>)bs.popVector(PayRecommend.class);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x877f8801L;
	}


	/**
	 * 获取支付方式推荐列表
	 * 
	 * 此字段的版本 >= 0
	 * @return payRecommendList value 类型为:Vector<PayRecommend>
	 * 
	 */
	public Vector<PayRecommend> getPayRecommendList()
	{
		return payRecommendList;
	}


	/**
	 * 设置支付方式推荐列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<PayRecommend>
	 * 
	 */
	public void setPayRecommendList(Vector<PayRecommend> value)
	{
		if (value != null) {
				this.payRecommendList = value;
		}else{
				this.payRecommendList = new Vector<PayRecommend>();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetPayRecommendListResp)
				length += ByteStream.getObjectSize(payRecommendList, null);  //计算字段payRecommendList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPayRecommendListResp)
				length += ByteStream.getObjectSize(payRecommendList, encoding);  //计算字段payRecommendList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
