//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.GetPayRecommendListReq.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *?????????
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class PayRecommendCondition  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * Э??汾??
	 *
	 * 版本 >= 0
	 */
	 private long version = 20150119;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * ???UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwUin_u;

	/**
	 * ???????
	 *
	 * 版本 >= 0
	 */
	 private long dwScene;

	/**
	 * 版本 >= 0
	 */
	 private short dwScene_u;

	/**
	 * ???????
	 *
	 * 版本 >= 20150119
	 */
	 private long dwUType;

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short dwUType_u;

	/**
	 * ???id,?????????
	 *
	 * 版本 >= 20150119
	 */
	 private String strCommodityIds = new String();

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short strCommodityIds_u;

	/**
	 * ???id,?????????
	 *
	 * 版本 >= 20150119
	 */
	 private String strSellerIds = new String();

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short strSellerIds_u;

	/**
	 * ??????????????,?????????
	 *
	 * 版本 >= 20150119
	 */
	 private String strCategorys = new String();

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short strCategorys_u;

	/**
	 * ??????cod,?????????
	 *
	 * 版本 >= 20150119
	 */
	 private String strSupportCods = new String();

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short strSupportCods_u;

	/**
	 * ?????????*100
	 *
	 * 版本 >= 20150119
	 */
	 private long dwTotalFee;

	/**
	 * 
	 *
	 * 版本 >= 20150119
	 */
	 private short dwTotalFee_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushLong(ddwUin);
		bs.pushUByte(ddwUin_u);
		bs.pushUInt(dwScene);
		bs.pushUByte(dwScene_u);
		if(  this.version >= 20150119 ){
				bs.pushUInt(dwUType);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(dwUType_u);
		}
		if(  this.version >= 20150119 ){
				bs.pushString(strCommodityIds);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(strCommodityIds_u);
		}
		if(  this.version >= 20150119 ){
				bs.pushString(strSellerIds);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(strSellerIds_u);
		}
		if(  this.version >= 20150119 ){
				bs.pushString(strCategorys);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(strCategorys_u);
		}
		if(  this.version >= 20150119 ){
				bs.pushString(strSupportCods);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(strSupportCods_u);
		}
		if(  this.version >= 20150119 ){
				bs.pushUInt(dwTotalFee);
		}
		if(  this.version >= 20150119 ){
				bs.pushUByte(dwTotalFee_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		ddwUin = bs.popLong();
		ddwUin_u = bs.popUByte();
		dwScene = bs.popUInt();
		dwScene_u = bs.popUByte();
		if(  this.version >= 20150119 ){
				dwUType = bs.popUInt();
		}
		if(  this.version >= 20150119 ){
				dwUType_u = bs.popUByte();
		}
		if(  this.version >= 20150119 ){
				strCommodityIds = bs.popString();
		}
		if(  this.version >= 20150119 ){
				strCommodityIds_u = bs.popUByte();
		}
		if(  this.version >= 20150119 ){
				strSellerIds = bs.popString();
		}
		if(  this.version >= 20150119 ){
				strSellerIds_u = bs.popUByte();
		}
		if(  this.version >= 20150119 ){
				strCategorys = bs.popString();
		}
		if(  this.version >= 20150119 ){
				strCategorys_u = bs.popUByte();
		}
		if(  this.version >= 20150119 ){
				strSupportCods = bs.popString();
		}
		if(  this.version >= 20150119 ){
				strSupportCods_u = bs.popUByte();
		}
		if(  this.version >= 20150119 ){
				dwTotalFee = bs.popUInt();
		}
		if(  this.version >= 20150119 ){
				dwTotalFee_u = bs.popUByte();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置Э??汾??
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取???UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin value 类型为:long
	 * 
	 */
	public long getDdwUin()
	{
		return ddwUin;
	}


	/**
	 * 设置???UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUin(long value)
	{
		this.ddwUin = value;
		this.ddwUin_u = 1;
	}

	public boolean issetDdwUin()
	{
		return this.ddwUin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin_u value 类型为:short
	 * 
	 */
	public short getDdwUin_u()
	{
		return ddwUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwUin_u(short value)
	{
		this.ddwUin_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 0
	 * @return dwScene value 类型为:long
	 * 
	 */
	public long getDwScene()
	{
		return dwScene;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwScene(long value)
	{
		this.dwScene = value;
		this.dwScene_u = 1;
	}

	public boolean issetDwScene()
	{
		return this.dwScene_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwScene_u value 类型为:short
	 * 
	 */
	public short getDwScene_u()
	{
		return dwScene_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwScene_u(short value)
	{
		this.dwScene_u = value;
	}


	/**
	 * 获取???????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwUType value 类型为:long
	 * 
	 */
	public long getDwUType()
	{
		return dwUType;
	}


	/**
	 * 设置???????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUType(long value)
	{
		this.dwUType = value;
		this.dwUType_u = 1;
	}

	public boolean issetDwUType()
	{
		return this.dwUType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwUType_u value 类型为:short
	 * 
	 */
	public short getDwUType_u()
	{
		return dwUType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwUType_u(short value)
	{
		this.dwUType_u = value;
	}


	/**
	 * 获取???id,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strCommodityIds value 类型为:String
	 * 
	 */
	public String getStrCommodityIds()
	{
		return strCommodityIds;
	}


	/**
	 * 设置???id,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCommodityIds(String value)
	{
		this.strCommodityIds = value;
		this.strCommodityIds_u = 1;
	}

	public boolean issetStrCommodityIds()
	{
		return this.strCommodityIds_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strCommodityIds_u value 类型为:short
	 * 
	 */
	public short getStrCommodityIds_u()
	{
		return strCommodityIds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrCommodityIds_u(short value)
	{
		this.strCommodityIds_u = value;
	}


	/**
	 * 获取???id,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strSellerIds value 类型为:String
	 * 
	 */
	public String getStrSellerIds()
	{
		return strSellerIds;
	}


	/**
	 * 设置???id,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrSellerIds(String value)
	{
		this.strSellerIds = value;
		this.strSellerIds_u = 1;
	}

	public boolean issetStrSellerIds()
	{
		return this.strSellerIds_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strSellerIds_u value 类型为:short
	 * 
	 */
	public short getStrSellerIds_u()
	{
		return strSellerIds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrSellerIds_u(short value)
	{
		this.strSellerIds_u = value;
	}


	/**
	 * 获取??????????????,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strCategorys value 类型为:String
	 * 
	 */
	public String getStrCategorys()
	{
		return strCategorys;
	}


	/**
	 * 设置??????????????,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCategorys(String value)
	{
		this.strCategorys = value;
		this.strCategorys_u = 1;
	}

	public boolean issetStrCategorys()
	{
		return this.strCategorys_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strCategorys_u value 类型为:short
	 * 
	 */
	public short getStrCategorys_u()
	{
		return strCategorys_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrCategorys_u(short value)
	{
		this.strCategorys_u = value;
	}


	/**
	 * 获取??????cod,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strSupportCods value 类型为:String
	 * 
	 */
	public String getStrSupportCods()
	{
		return strSupportCods;
	}


	/**
	 * 设置??????cod,?????????
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrSupportCods(String value)
	{
		this.strSupportCods = value;
		this.strSupportCods_u = 1;
	}

	public boolean issetStrSupportCods()
	{
		return this.strSupportCods_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return strSupportCods_u value 类型为:short
	 * 
	 */
	public short getStrSupportCods_u()
	{
		return strSupportCods_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrSupportCods_u(short value)
	{
		this.strSupportCods_u = value;
	}


	/**
	 * 获取?????????*100
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwTotalFee value 类型为:long
	 * 
	 */
	public long getDwTotalFee()
	{
		return dwTotalFee;
	}


	/**
	 * 设置?????????*100
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwTotalFee(long value)
	{
		this.dwTotalFee = value;
		this.dwTotalFee_u = 1;
	}

	public boolean issetDwTotalFee()
	{
		return this.dwTotalFee_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 20150119
	 * @return dwTotalFee_u value 类型为:short
	 * 
	 */
	public short getDwTotalFee_u()
	{
		return dwTotalFee_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 20150119
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwTotalFee_u(short value)
	{
		this.dwTotalFee_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PayRecommendCondition)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwScene的长度 size_of(uint32_t)
				length += 1;  //计算字段dwScene_u的长度 size_of(uint8_t)
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwUType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwUType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strCommodityIds, null);  //计算字段strCommodityIds的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strCommodityIds_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strSellerIds, null);  //计算字段strSellerIds的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strSellerIds_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strCategorys, null);  //计算字段strCategorys的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strCategorys_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strSupportCods, null);  //计算字段strSupportCods的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strSupportCods_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwTotalFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwTotalFee_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PayRecommendCondition)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwScene的长度 size_of(uint32_t)
				length += 1;  //计算字段dwScene_u的长度 size_of(uint8_t)
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwUType的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwUType_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strCommodityIds, encoding);  //计算字段strCommodityIds的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strCommodityIds_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strSellerIds, encoding);  //计算字段strSellerIds的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strSellerIds_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strCategorys, encoding);  //计算字段strCategorys的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strCategorys_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += ByteStream.getObjectSize(strSupportCods, encoding);  //计算字段strSupportCods的长度 size_of(String)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段strSupportCods_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 20150119 ){
						length += 4;  //计算字段dwTotalFee的长度 size_of(uint32_t)
				}
				if(  this.version >= 20150119 ){
						length += 1;  //计算字段dwTotalFee_u的长度 size_of(uint8_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本20150119所包含的字段*******
 *	long version;///<Э??汾??
 *	short version_u;
 *	long ddwUin;///<???UIN
 *	short ddwUin_u;
 *	long dwScene;///<???????
 *	short dwScene_u;
 *	long dwUType;///<???????
 *	short dwUType_u;
 *	String strCommodityIds;///<???id,?????????
 *	short strCommodityIds_u;
 *	String strSellerIds;///<???id,?????????
 *	short strSellerIds_u;
 *	String strCategorys;///<??????????????,?????????
 *	short strCategorys_u;
 *	String strSupportCods;///<??????cod,?????????
 *	short strSupportCods_u;
 *	long dwTotalFee;///<?????????*100
 *	short dwTotalFee_u;
 *****以上是版本20150119所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
