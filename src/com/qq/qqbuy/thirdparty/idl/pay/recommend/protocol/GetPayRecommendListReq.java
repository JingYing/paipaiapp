 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.PayRecommendAo.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *获取用户推荐列表请求
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class  GetPayRecommendListReq extends NetMessage
{
	/**
	 * 机器码，必填
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源，必填
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 保留入参, 传cookie数据
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 当前支付情况
	 *
	 * 版本 >= 0
	 */
	 private PayRecommendCondition payRecommendCondition = new PayRecommendCondition();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushObject(payRecommendCondition);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		payRecommendCondition = (PayRecommendCondition)bs.popObject(PayRecommendCondition.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x877f1801L;
	}


	/**
	 * 获取机器码，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取保留入参, 传cookie数据
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置保留入参, 传cookie数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取当前支付情况
	 * 
	 * 此字段的版本 >= 0
	 * @return payRecommendCondition value 类型为:PayRecommendCondition
	 * 
	 */
	public PayRecommendCondition getPayRecommendCondition()
	{
		return payRecommendCondition;
	}


	/**
	 * 设置当前支付情况
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PayRecommendCondition
	 * 
	 */
	public void setPayRecommendCondition(PayRecommendCondition value)
	{
		if (value != null) {
				this.payRecommendCondition = value;
		}else{
				this.payRecommendCondition = new PayRecommendCondition();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPayRecommendListReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(payRecommendCondition, null);  //计算字段payRecommendCondition的长度 size_of(PayRecommendCondition)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPayRecommendListReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(payRecommendCondition, encoding);  //计算字段payRecommendCondition的长度 size_of(PayRecommendCondition)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
