 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.pay.ao.idl.PayRecommendAo.java

package com.qq.qqbuy.thirdparty.idl.pay.recommend.protocol;


import java.util.Vector;

import com.paipai.netframework.kernal.NetMessage;
import com.paipai.util.io.ByteStream;

/**
 *根据订单获取可支付方式响应
 *
 *@date 2015-04-24 10:22:18
 *
 *@since version:0
*/
public class  GetUseablePayListResp extends NetMessage
{
	/**
	 * 可用支付方式列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<UseablePay> useablePayList = new Vector<UseablePay>();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(useablePayList);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		useablePayList = (Vector<UseablePay>)bs.popVector(UseablePay.class);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x877f8802L;
	}


	/**
	 * 获取可用支付方式列表
	 * 
	 * 此字段的版本 >= 0
	 * @return useablePayList value 类型为:Vector<UseablePay>
	 * 
	 */
	public Vector<UseablePay> getUseablePayList()
	{
		return useablePayList;
	}


	/**
	 * 设置可用支付方式列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<UseablePay>
	 * 
	 */
	public void setUseablePayList(Vector<UseablePay> value)
	{
		if (value != null) {
				this.useablePayList = value;
		}else{
				this.useablePayList = new Vector<UseablePay>();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetUseablePayListResp)
				length += ByteStream.getObjectSize(useablePayList, null);  //计算字段useablePayList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errMsg, null);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUseablePayListResp)
				length += ByteStream.getObjectSize(useablePayList, encoding);  //计算字段useablePayList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errMsg, encoding);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
