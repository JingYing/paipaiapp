package com.qq.qqbuy.thirdparty.idl.pay;

import com.paipai.component.c2cplatform.IAsynWebStub;

import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.pay.protocol.PayMentApiReq;
import com.qq.qqbuy.thirdparty.idl.pay.protocol.PayMentApiResp;

public class PaymentClient extends SupportIDLBaseClient {
	
	private final static int PAY_TYPE = 1; 
	public static PayMentApiResp initPayInfo(String dealCode) {
//        long start = System.currentTimeMillis();
        PayMentApiReq req = new PayMentApiReq();
       
        req.setSource(CALL_IDL_SOURCE);
        req.setDealCode(dealCode);
        req.setType(PAY_TYPE);
        req.setReserve("qgo");

        PayMentApiResp resp = new PayMentApiResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        ret = invokePaiPaiIDL(req, resp,stub);

//        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }
	
}
