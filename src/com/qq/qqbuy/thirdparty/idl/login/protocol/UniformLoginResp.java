 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.b2b2c.user.idl.UserWangGouAo.java

package com.qq.qqbuy.thirdparty.idl.login.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *
 *
 *@date 2014-09-04 03:27:58
 *
 *@since version:1
*/
public class  UniformLoginResp extends NetMessage
{
	/**
	 * 统一后台用户id，目前仅支持32位
	 *
	 * 版本 >= 0
	 */
	 private long wgUid;

	/**
	 * sessionKey，当accountType填1时输出为qqskey
	 *
	 * 版本 >= 0
	 */
	 private String skey = new String();

	/**
	 * 用户昵称，当accountType填1时取的是oidb昵称
	 *
	 * 版本 >= 0
	 */
	 private String nickname = new String();

	/**
	 * 错误提示信息
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushLong(wgUid);
		bs.pushString(skey);
		bs.pushString(nickname);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		wgUid = bs.popLong();
		skey = bs.popString();
		nickname = bs.popString();
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x30908801L;
	}


	/**
	 * 获取统一后台用户id，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @return wgUid value 类型为:long
	 * 
	 */
	public long getWgUid()
	{
		return wgUid;
	}


	/**
	 * 设置统一后台用户id，目前仅支持32位
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWgUid(long value)
	{
		this.wgUid = value;
	}


	/**
	 * 获取sessionKey，当accountType填1时输出为qqskey
	 * 
	 * 此字段的版本 >= 0
	 * @return skey value 类型为:String
	 * 
	 */
	public String getSkey()
	{
		return skey;
	}


	/**
	 * 设置sessionKey，当accountType填1时输出为qqskey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSkey(String value)
	{
		this.skey = value;
	}


	/**
	 * 获取用户昵称，当accountType填1时取的是oidb昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return nickname value 类型为:String
	 * 
	 */
	public String getNickname()
	{
		return nickname;
	}


	/**
	 * 设置用户昵称，当accountType填1时取的是oidb昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNickname(String value)
	{
		this.nickname = value;
	}


	/**
	 * 获取错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误提示信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(UniformLoginResp)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(skey, null);  //计算字段skey的长度 size_of(String)
				length += ByteStream.getObjectSize(nickname, null);  //计算字段nickname的长度 size_of(String)
				length += ByteStream.getObjectSize(errmsg, null);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UniformLoginResp)
				length += 17;  //计算字段wgUid的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(skey, encoding);  //计算字段skey的长度 size_of(String)
				length += ByteStream.getObjectSize(nickname, encoding);  //计算字段nickname的长度 size_of(String)
				length += ByteStream.getObjectSize(errmsg, encoding);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
