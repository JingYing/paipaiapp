//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.b2b2c.user.idl.UserWangGouAo.java

package com.qq.qqbuy.thirdparty.idl.login.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *绑定帐号po
 *
 *@date 2014-09-04 03:27:58
 *
 *@since version:0
*/
public class BindInfoPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 绑定帐号类型，必需，0-无效值 1-email 2-手机号，3-易迅微信号，4-网购微信公众号(填4时，appid和openid必填).目前只支持4
	 *
	 * 版本 >= 0
	 */
	 private short bindInfoType;

	/**
	 * 微信appid,bindInfoType为4时必填
	 *
	 * 版本 >= 0
	 */
	 private String appid = new String();

	/**
	 * 网购微信openid,bindInfoType为4时必填
	 *
	 * 版本 >= 0
	 */
	 private String openid = new String();

	/**
	 * 预留
	 *
	 * 版本 >= 0
	 */
	 private String reserve = new String();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short bindInfoType_u;

	/**
	 * 版本 >= 0
	 */
	 private short appid_u;

	/**
	 * 版本 >= 0
	 */
	 private short openid_u;

	/**
	 * 版本 >= 0
	 */
	 private short reserve_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(bindInfoType);
		bs.pushString(appid);
		bs.pushString(openid);
		bs.pushString(reserve);
		bs.pushUByte(version_u);
		bs.pushUByte(bindInfoType_u);
		bs.pushUByte(appid_u);
		bs.pushUByte(openid_u);
		bs.pushUByte(reserve_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		bindInfoType = bs.popUByte();
		appid = bs.popString();
		openid = bs.popString();
		reserve = bs.popString();
		version_u = bs.popUByte();
		bindInfoType_u = bs.popUByte();
		appid_u = bs.popUByte();
		openid_u = bs.popUByte();
		reserve_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取绑定帐号类型，必需，0-无效值 1-email 2-手机号，3-易迅微信号，4-网购微信公众号(填4时，appid和openid必填).目前只支持4
	 * 
	 * 此字段的版本 >= 0
	 * @return bindInfoType value 类型为:short
	 * 
	 */
	public short getBindInfoType()
	{
		return bindInfoType;
	}


	/**
	 * 设置绑定帐号类型，必需，0-无效值 1-email 2-手机号，3-易迅微信号，4-网购微信公众号(填4时，appid和openid必填).目前只支持4
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBindInfoType(short value)
	{
		this.bindInfoType = value;
	}


	/**
	 * 获取微信appid,bindInfoType为4时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return appid value 类型为:String
	 * 
	 */
	public String getAppid()
	{
		return appid;
	}


	/**
	 * 设置微信appid,bindInfoType为4时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAppid(String value)
	{
		this.appid = value;
	}


	/**
	 * 获取网购微信openid,bindInfoType为4时必填
	 * 
	 * 此字段的版本 >= 0
	 * @return openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return openid;
	}


	/**
	 * 设置网购微信openid,bindInfoType为4时必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.openid = value;
	}


	/**
	 * 获取预留
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return reserve;
	}


	/**
	 * 设置预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.reserve = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return bindInfoType_u value 类型为:short
	 * 
	 */
	public short getBindInfoType_u()
	{
		return bindInfoType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBindInfoType_u(short value)
	{
		this.bindInfoType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return appid_u value 类型为:short
	 * 
	 */
	public short getAppid_u()
	{
		return appid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAppid_u(short value)
	{
		this.appid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return openid_u value 类型为:short
	 * 
	 */
	public short getOpenid_u()
	{
		return openid_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOpenid_u(short value)
	{
		this.openid_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return reserve_u value 类型为:short
	 * 
	 */
	public short getReserve_u()
	{
		return reserve_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserve_u(short value)
	{
		this.reserve_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BindInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段bindInfoType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(appid, null);  //计算字段appid的长度 size_of(String)
				length += ByteStream.getObjectSize(openid, null);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, null);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段bindInfoType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段appid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(BindInfoPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段bindInfoType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(appid, encoding);  //计算字段appid的长度 size_of(String)
				length += ByteStream.getObjectSize(openid, encoding);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(reserve, encoding);  //计算字段reserve的长度 size_of(String)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段bindInfoType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段appid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段openid_u的长度 size_of(uint8_t)
				length += 1;  //计算字段reserve_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
