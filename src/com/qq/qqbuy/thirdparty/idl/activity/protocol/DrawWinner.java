 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *抽奖中奖信息
 *
 *@date 2011-05-23 03:30::06
 *
 *@since version:0
*/
public class DrawWinner  implements ICanSerializeObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 中奖规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 奖项名称
	 *
	 * 版本 >= 0
	 */
	 private String awardNames = new String();

	/**
	 * 奖项等级
	 *
	 * 版本 >= 0
	 */
	 private int awardLevel;

	/**
	 * 中奖序号
	 *
	 * 版本 >= 0
	 */
	 private int winnerIndex;

	/**
	 * 福彩号码
	 *
	 * 版本 >= 0
	 */
	 private String welfareLotteryInfo = new String();

	/**
	 * 抽奖时间
	 *
	 * 版本 >= 0
	 */
	 private String winAwardTime = new String();

	/**
	 * 标准奖号
	 *
	 * 版本 >= 0
	 */
	 private String perfectLotteryNo = new String();

	/**
	 * 买家奖号
	 *
	 * 版本 >= 0
	 */
	 private long buyerLotteryNo;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(actId);
		bs.pushUInt(ruleId);
		bs.pushString(awardNames);
		bs.pushInt(awardLevel);
		bs.pushInt(winnerIndex);
		bs.pushString(welfareLotteryInfo);
		bs.pushString(winAwardTime);
		bs.pushString(perfectLotteryNo);
		bs.pushUInt(buyerLotteryNo);
		bs.pushUInt(buyerUin);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		actId = bs.popUInt();
		ruleId = bs.popUInt();
		awardNames = bs.popString();
		awardLevel = bs.popInt();
		winnerIndex = bs.popInt();
		welfareLotteryInfo = bs.popString();
		winAwardTime = bs.popString();
		perfectLotteryNo = bs.popString();
		buyerLotteryNo = bs.popUInt();
		buyerUin = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取奖项名称
	 * 
	 * 此字段的版本 >= 0
	 * @return awardNames value 类型为:String
	 * 
	 */
	public String getAwardNames()
	{
		return encode(awardNames);
	}


	/**
	 * 设置奖项名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardNames(String value)
	{
		if (value != null) {
				this.awardNames = value;
		}else{
				this.awardNames = new String();
		}
	}


	/**
	 * 获取奖项等级
	 * 
	 * 此字段的版本 >= 0
	 * @return awardLevel value 类型为:int
	 * 
	 */
	public int getAwardLevel()
	{
		return awardLevel;
	}


	/**
	 * 设置奖项等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setAwardLevel(int value)
	{
		this.awardLevel = value;
	}


	/**
	 * 获取中奖序号
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerIndex value 类型为:int
	 * 
	 */
	public int getWinnerIndex()
	{
		return winnerIndex;
	}


	/**
	 * 设置中奖序号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setWinnerIndex(int value)
	{
		this.winnerIndex = value;
	}


	/**
	 * 获取福彩号码
	 * 
	 * 此字段的版本 >= 0
	 * @return welfareLotteryInfo value 类型为:String
	 * 
	 */
	public String getWelfareLotteryInfo()
	{
		return encode(welfareLotteryInfo);
	}


	/**
	 * 设置福彩号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWelfareLotteryInfo(String value)
	{
		if (value != null) {
				this.welfareLotteryInfo = value;
		}else{
				this.welfareLotteryInfo = new String();
		}
	}


	/**
	 * 获取抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @return winAwardTime value 类型为:String
	 * 
	 */
	public String getWinAwardTime()
	{
		return encode(winAwardTime);
	}


	/**
	 * 设置抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinAwardTime(String value)
	{
		if (value != null) {
				this.winAwardTime = value;
		}else{
				this.winAwardTime = new String();
		}
	}


	/**
	 * 获取标准奖号
	 * 
	 * 此字段的版本 >= 0
	 * @return perfectLotteryNo value 类型为:String
	 * 
	 */
	public String getPerfectLotteryNo()
	{
		return encode(perfectLotteryNo);
	}


	/**
	 * 设置标准奖号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPerfectLotteryNo(String value)
	{
		if (value != null) {
				this.perfectLotteryNo = value;
		}else{
				this.perfectLotteryNo = new String();
		}
	}


	/**
	 * 获取买家奖号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerLotteryNo value 类型为:long
	 * 
	 */
	public long getBuyerLotteryNo()
	{
		return buyerLotteryNo;
	}


	/**
	 * 设置买家奖号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerLotteryNo(long value)
	{
		this.buyerLotteryNo = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DrawWinner)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += ByteStream.getObjectSize(awardNames);  //计算字段awardNames的长度 size_of(String)
				length += 4;  //计算字段awardLevel的长度 size_of(int)
				length += 4;  //计算字段winnerIndex的长度 size_of(int)
				length += ByteStream.getObjectSize(welfareLotteryInfo);  //计算字段welfareLotteryInfo的长度 size_of(String)
				length += ByteStream.getObjectSize(winAwardTime);  //计算字段winAwardTime的长度 size_of(String)
				length += ByteStream.getObjectSize(perfectLotteryNo);  //计算字段perfectLotteryNo的长度 size_of(String)
				length += 4;  //计算字段buyerLotteryNo的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
