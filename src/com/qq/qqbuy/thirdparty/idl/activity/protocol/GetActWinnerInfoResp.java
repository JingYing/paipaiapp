 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2011-05-13 10:10::09
 *
 *@since version:1
*/
public class  GetActWinnerInfoResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 活动标题
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 活动参与人数
	 *
	 * 版本 >= 0
	 */
	 private int joinPersonCount;

	/**
	 * 活动获奖人数
	 *
	 * 版本 >= 0
	 */
	 private int winPersonCount;

	/**
	 * 中奖信息
	 *
	 * 版本 >= 0
	 */
	 private List<BuyerWinner> winnerInfo = new ArrayList<BuyerWinner>();

	/**
	 * 页码
	 *
	 * 版本 >= 0
	 */
	 private int pageNo;

	/**
	 * 总记录数
	 *
	 * 版本 >= 0
	 */
	 private int rowCount;


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushUInt(actId);
		bs.pushString(actTitle);
		bs.pushInt(joinPersonCount);
		bs.pushInt(winPersonCount);
		bs.pushObject(winnerInfo);
		bs.pushInt(pageNo);
		bs.pushInt(rowCount);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		actId = bs.popUInt();
		actTitle = bs.popString();
		joinPersonCount = bs.popInt();
		winPersonCount = bs.popInt();
		winnerInfo = (List<BuyerWinner>)bs.popList(ArrayList.class,BuyerWinner.class);
		pageNo = bs.popInt();
		rowCount = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008814L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return actTitle;
	}


	/**
	 * 设置活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		if (value != null) {
				this.actTitle = value;
		}else{
				this.actTitle = new String();
		}
	}


	/**
	 * 获取活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @return joinPersonCount value 类型为:int
	 * 
	 */
	public int getJoinPersonCount()
	{
		return joinPersonCount;
	}


	/**
	 * 设置活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setJoinPersonCount(int value)
	{
		this.joinPersonCount = value;
	}


	/**
	 * 获取活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @return winPersonCount value 类型为:int
	 * 
	 */
	public int getWinPersonCount()
	{
		return winPersonCount;
	}


	/**
	 * 设置活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setWinPersonCount(int value)
	{
		this.winPersonCount = value;
	}


	/**
	 * 获取中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @return winnerInfo value 类型为:List<BuyerWinner>
	 * 
	 */
	public List<BuyerWinner> getWinnerInfo()
	{
		return winnerInfo;
	}


	/**
	 * 设置中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<BuyerWinner>
	 * 
	 */
	public void setWinnerInfo(List<BuyerWinner> value)
	{
		if (value != null) {
				this.winnerInfo = value;
		}else{
				this.winnerInfo = new ArrayList<BuyerWinner>();
		}
	}


	/**
	 * 获取页码
	 * 
	 * 此字段的版本 >= 0
	 * @return pageNo value 类型为:int
	 * 
	 */
	public int getPageNo()
	{
		return pageNo;
	}


	/**
	 * 设置页码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageNo(int value)
	{
		this.pageNo = value;
	}


	/**
	 * 获取总记录数
	 * 
	 * 此字段的版本 >= 0
	 * @return rowCount value 类型为:int
	 * 
	 */
	public int getRowCount()
	{
		return rowCount;
	}


	/**
	 * 设置总记录数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRowCount(int value)
	{
		this.rowCount = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetActWinnerInfoResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += 4;  //计算字段joinPersonCount的长度 size_of(int)
				length += 4;  //计算字段winPersonCount的长度 size_of(int)
				length += ByteStream.getObjectSize(winnerInfo);  //计算字段winnerInfo的长度 size_of(List)
				length += 4;  //计算字段pageNo的长度 size_of(int)
				length += 4;  //计算字段rowCount的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
