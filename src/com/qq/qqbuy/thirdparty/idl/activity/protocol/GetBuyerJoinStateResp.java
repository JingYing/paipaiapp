 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-05-12 07:46::59
 *
 *@since version:1
*/
public class  GetBuyerJoinStateResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 报名状态
	 *
	 * 版本 >= 0
	 */
	 private int joinState;


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushInt(joinState);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		joinState = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008815L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取报名状态
	 * 
	 * 此字段的版本 >= 0
	 * @return joinState value 类型为:int
	 * 
	 */
	public int getJoinState()
	{
		return joinState;
	}


	/**
	 * 设置报名状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setJoinState(int value)
	{
		this.joinState = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetBuyerJoinStateResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段joinState的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
