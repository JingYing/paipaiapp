 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2011-05-12 07:46::59
 *
 *@since version:1
*/
public class  GetBuyerWinnerInfoResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 活动标题
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 买家中奖信息
	 *
	 * 版本 >= 0
	 */
	 private List<BuyerWinner> buyerWinnerInfo = new ArrayList<BuyerWinner>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushUInt(actId);
		bs.pushString(actTitle);
		bs.pushUInt(buyerUin);
		bs.pushObject(buyerWinnerInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		actId = bs.popUInt();
		actTitle = bs.popString();
		buyerUin = bs.popUInt();
		buyerWinnerInfo = (List<BuyerWinner>)bs.popList(ArrayList.class,BuyerWinner.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008817L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return this.encode(errMsg);
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return this.encode(actTitle);
	}


	/**
	 * 设置活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		if (value != null) {
				this.actTitle = value;
		}else{
				this.actTitle = new String();
		}
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取买家中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerWinnerInfo value 类型为:List<BuyerWinner>
	 * 
	 */
	public List<BuyerWinner> getBuyerWinnerInfo()
	{
		return buyerWinnerInfo;
	}


	/**
	 * 设置买家中奖信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<BuyerWinner>
	 * 
	 */
	public void setBuyerWinnerInfo(List<BuyerWinner> value)
	{
		if (value != null) {
				this.buyerWinnerInfo = value;
		}else{
				this.buyerWinnerInfo = new ArrayList<BuyerWinner>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetBuyerWinnerInfoResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(buyerWinnerInfo);  //计算字段buyerWinnerInfo的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
