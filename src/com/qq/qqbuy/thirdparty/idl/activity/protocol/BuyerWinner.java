 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *买家中奖信息
 *
 *@date 2011-05-20 06:19::57
 *
 *@since version:0
*/
public class BuyerWinner  implements ICanSerializeObject,Serializable
{
	/**
	 * 中奖操作号
	 *
	 * 版本 >= 0
	 */
	 private long serialNo;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 奖品名称
	 *
	 * 版本 >= 0
	 */
	 private String awardName = new String();

	/**
	 * 中奖规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 中奖规则类型
	 *
	 * 版本 >= 0
	 */
	 private int ruleType;

	/**
	 * 中奖时间
	 *
	 * 版本 >= 0
	 */
	 private String winAwardTime = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(serialNo);
		bs.pushUInt(buyerUin);
		bs.pushString(awardName);
		bs.pushUInt(ruleId);
		bs.pushInt(ruleType);
		bs.pushString(winAwardTime);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		serialNo = bs.popUInt();
		buyerUin = bs.popUInt();
		awardName = bs.popString();
		ruleId = bs.popUInt();
		ruleType = bs.popInt();
		winAwardTime = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取中奖操作号
	 * 
	 * 此字段的版本 >= 0
	 * @return serialNo value 类型为:long
	 * 
	 */
	public long getSerialNo()
	{
		return serialNo;
	}


	/**
	 * 设置中奖操作号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSerialNo(long value)
	{
		this.serialNo = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return awardName value 类型为:String
	 * 
	 */
	public String getAwardName()
	{
		return encode(awardName);
	}


	/**
	 * 设置奖品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAwardName(String value)
	{
		if (value != null) {
				this.awardName = value;
		}else{
				this.awardName = new String();
		}
	}


	/**
	 * 获取中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取中奖规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleType value 类型为:int
	 * 
	 */
	public int getRuleType()
	{
		return ruleType;
	}


	/**
	 * 设置中奖规则类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRuleType(int value)
	{
		this.ruleType = value;
	}


	/**
	 * 获取中奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @return winAwardTime value 类型为:String
	 * 
	 */
	public String getWinAwardTime()
	{
		return encode(winAwardTime);
	}


	/**
	 * 设置中奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setWinAwardTime(String value)
	{
		if (value != null) {
				this.winAwardTime = value;
		}else{
				this.winAwardTime = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	private String encode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BuyerWinner)
				length += 4;  //计算字段serialNo的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += ByteStream.getObjectSize(awardName);  //计算字段awardName的长度 size_of(String)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += 4;  //计算字段ruleType的长度 size_of(int)
				length += ByteStream.getObjectSize(winAwardTime);  //计算字段winAwardTime的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
