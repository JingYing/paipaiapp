 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-05-13 04:27::41
 *
 *@since version:1
*/
public class  GetFitRuleLegalDealReq implements IServiceObject,Serializable
{
	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 买家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long buyerUin;

	/**
	 * 规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(actId);
		bs.pushUInt(buyerUin);
		bs.pushUInt(ruleId);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		actId = bs.popUInt();
		buyerUin = bs.popUInt();
		ruleId = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80001818L;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return buyerUin;
	}


	/**
	 * 设置买家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.buyerUin = value;
	}


	/**
	 * 获取规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetFitRuleLegalDealReq)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += 4;  //计算字段buyerUin的长度 size_of(long)
				length += 4;  //计算字段ruleId的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
