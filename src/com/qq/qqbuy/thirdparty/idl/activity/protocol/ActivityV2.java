 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *活动V2
 *
 *@date 2011-09-07 08:32::22
 *
 *@since version:20110907
*/
public class ActivityV2  implements ICanSerializeObject,Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3040740704189437749L;

	/**
	 * 版本控制的字段
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110907; 

	/**
	 * 活动ID
	 *
	 * 版本 >= 0
	 */
	 private long actId;

	/**
	 * 活动标题
	 *
	 * 版本 >= 0
	 */
	 private String actTitle = new String();

	/**
	 * 活动图片URL
	 *
	 * 版本 >= 0
	 */
	 private String bannerUrl = new String();

	/**
	 * 下单开始时间
	 *
	 * 版本 >= 0
	 */
	 private String orderStartTime = new String();

	/**
	 * 下单结束时间
	 *
	 * 版本 >= 0
	 */
	 private String orderEndTime = new String();

	/**
	 * 是否需要报名
	 *
	 * 版本 >= 0
	 */
	 private int needSignIn;

	/**
	 * 报名开始时间
	 *
	 * 版本 >= 0
	 */
	 private String signStartTime = new String();

	/**
	 * 报名结束时间
	 *
	 * 版本 >= 0
	 */
	 private String signEndTime = new String();

	/**
	 * 彩钻等级
	 *
	 * 版本 >= 0
	 */
	 private int colorTileGrade;

	/**
	 * 超Q等级
	 *
	 * 版本 >= 0
	 */
	 private int qqvipLevel;

	/**
	 * 购物达人积分
	 *
	 * 版本 >= 0
	 */
	 private int buyerPointLevel;

	/**
	 * 报名次数
	 *
	 * 版本 >= 0
	 */
	 private long signTimes;

	/**
	 * 总报名次数限制
	 *
	 * 版本 >= 0
	 */
	 private long limitSignTimes;

	/**
	 * 是否需要生成奖号
	 *
	 * 版本 >= 0
	 */
	 private int needCreateLottery;

	/**
	 * 获得奖号最低金额
	 *
	 * 版本 >= 0
	 */
	 private long leastDealFee;

	/**
	 * 是否可领奖
	 *
	 * 版本 >= 0
	 */
	 private int canChangeAward;

	/**
	 * 领奖开始时间
	 *
	 * 版本 >= 0
	 */
	 private String changeStartTime = new String();

	/**
	 * 领奖结束时间
	 *
	 * 版本 >= 0
	 */
	 private String changeEndTime = new String();

	/**
	 * 是否可以抽奖
	 *
	 * 版本 >= 0
	 */
	 private int canDrawAward;

	/**
	 * 抽奖时间
	 *
	 * 版本 >= 0
	 */
	 private String drawAwardTime = new String();

	/**
	 * 活动简要规则
	 *
	 * 版本 >= 0
	 */
	 private String briefActRule = new String();

	/**
	 * 活动详细规则
	 *
	 * 版本 >= 0
	 */
	 private String fullActRule = new String();

	/**
	 * 活动参与人数
	 *
	 * 版本 >= 0
	 */
	 private long joinPersonCount;

	/**
	 * 活动获奖人数
	 *
	 * 版本 >= 0
	 */
	 private long winPersonCount;

	/**
	 * 活动奖品规则
	 *
	 * 版本 >= 0
	 */
	 private List<ActRuleV2> actRuleInfo = new ArrayList<ActRuleV2>();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushUInt(actId);
		bs.pushString(actTitle);
		bs.pushString(bannerUrl);
		bs.pushString(orderStartTime);
		bs.pushString(orderEndTime);
		bs.pushInt(needSignIn);
		bs.pushString(signStartTime);
		bs.pushString(signEndTime);
		bs.pushInt(colorTileGrade);
		bs.pushInt(qqvipLevel);
		bs.pushInt(buyerPointLevel);
		bs.pushUInt(signTimes);
		bs.pushUInt(limitSignTimes);
		bs.pushInt(needCreateLottery);
		bs.pushUInt(leastDealFee);
		bs.pushInt(canChangeAward);
		bs.pushString(changeStartTime);
		bs.pushString(changeEndTime);
		bs.pushInt(canDrawAward);
		bs.pushString(drawAwardTime);
		bs.pushString(briefActRule);
		bs.pushString(fullActRule);
		bs.pushUInt(joinPersonCount);
		bs.pushUInt(winPersonCount);
		bs.pushObject(actRuleInfo);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		actId = bs.popUInt();
		actTitle = bs.popString();
		bannerUrl = bs.popString();
		orderStartTime = bs.popString();
		orderEndTime = bs.popString();
		needSignIn = bs.popInt();
		signStartTime = bs.popString();
		signEndTime = bs.popString();
		colorTileGrade = bs.popInt();
		qqvipLevel = bs.popInt();
		buyerPointLevel = bs.popInt();
		signTimes = bs.popUInt();
		limitSignTimes = bs.popUInt();
		needCreateLottery = bs.popInt();
		leastDealFee = bs.popUInt();
		canChangeAward = bs.popInt();
		changeStartTime = bs.popString();
		changeEndTime = bs.popString();
		canDrawAward = bs.popInt();
		drawAwardTime = bs.popString();
		briefActRule = bs.popString();
		fullActRule = bs.popString();
		joinPersonCount = bs.popUInt();
		winPersonCount = bs.popUInt();
		actRuleInfo = (List<ActRuleV2>)bs.popList(ArrayList.class,ActRuleV2.class);
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本控制的字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @return actId value 类型为:long
	 * 
	 */
	public long getActId()
	{
		return actId;
	}


	/**
	 * 设置活动ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActId(long value)
	{
		this.actId = value;
	}


	/**
	 * 获取活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @return actTitle value 类型为:String
	 * 
	 */
	public String getActTitle()
	{
		return this.decode(actTitle);
	}


	/**
	 * 设置活动标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActTitle(String value)
	{
		if (value != null) {
				this.actTitle = value;
		}else{
				this.actTitle = new String();
		}
	}


	/**
	 * 获取活动图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @return bannerUrl value 类型为:String
	 * 
	 */
	public String getBannerUrl()
	{
		return this.decode(bannerUrl);
	}


	/**
	 * 设置活动图片URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBannerUrl(String value)
	{
		if (value != null) {
				this.bannerUrl = value;
		}else{
				this.bannerUrl = new String();
		}
	}


	/**
	 * 获取下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderStartTime value 类型为:String
	 * 
	 */
	public String getOrderStartTime()
	{
		return orderStartTime;
	}


	/**
	 * 设置下单开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderStartTime(String value)
	{
		if (value != null) {
				this.orderStartTime = value;
		}else{
				this.orderStartTime = new String();
		}
	}


	/**
	 * 获取下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return orderEndTime value 类型为:String
	 * 
	 */
	public String getOrderEndTime()
	{
		return orderEndTime;
	}


	/**
	 * 设置下单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOrderEndTime(String value)
	{
		if (value != null) {
				this.orderEndTime = value;
		}else{
				this.orderEndTime = new String();
		}
	}


	/**
	 * 获取是否需要报名
	 * 
	 * 此字段的版本 >= 0
	 * @return needSignIn value 类型为:int
	 * 
	 */
	public int getNeedSignIn()
	{
		return needSignIn;
	}


	/**
	 * 设置是否需要报名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setNeedSignIn(int value)
	{
		this.needSignIn = value;
	}


	/**
	 * 获取报名开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return signStartTime value 类型为:String
	 * 
	 */
	public String getSignStartTime()
	{
		return signStartTime;
	}


	/**
	 * 设置报名开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSignStartTime(String value)
	{
		if (value != null) {
				this.signStartTime = value;
		}else{
				this.signStartTime = new String();
		}
	}


	/**
	 * 获取报名结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return signEndTime value 类型为:String
	 * 
	 */
	public String getSignEndTime()
	{
		return signEndTime;
	}


	/**
	 * 设置报名结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSignEndTime(String value)
	{
		if (value != null) {
				this.signEndTime = value;
		}else{
				this.signEndTime = new String();
		}
	}


	/**
	 * 获取彩钻等级
	 * 
	 * 此字段的版本 >= 0
	 * @return colorTileGrade value 类型为:int
	 * 
	 */
	public int getColorTileGrade()
	{
		return colorTileGrade;
	}


	/**
	 * 设置彩钻等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setColorTileGrade(int value)
	{
		this.colorTileGrade = value;
	}


	/**
	 * 获取超Q等级
	 * 
	 * 此字段的版本 >= 0
	 * @return qqvipLevel value 类型为:int
	 * 
	 */
	public int getQqvipLevel()
	{
		return qqvipLevel;
	}


	/**
	 * 设置超Q等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setQqvipLevel(int value)
	{
		this.qqvipLevel = value;
	}


	/**
	 * 获取购物达人积分
	 * 
	 * 此字段的版本 >= 0
	 * @return buyerPointLevel value 类型为:int
	 * 
	 */
	public int getBuyerPointLevel()
	{
		return buyerPointLevel;
	}


	/**
	 * 设置购物达人积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerPointLevel(int value)
	{
		this.buyerPointLevel = value;
	}


	/**
	 * 获取报名次数
	 * 
	 * 此字段的版本 >= 0
	 * @return signTimes value 类型为:long
	 * 
	 */
	public long getSignTimes()
	{
		return signTimes;
	}


	/**
	 * 设置报名次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSignTimes(long value)
	{
		this.signTimes = value;
	}


	/**
	 * 获取总报名次数限制
	 * 
	 * 此字段的版本 >= 0
	 * @return limitSignTimes value 类型为:long
	 * 
	 */
	public long getLimitSignTimes()
	{
		return limitSignTimes;
	}


	/**
	 * 设置总报名次数限制
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLimitSignTimes(long value)
	{
		this.limitSignTimes = value;
	}


	/**
	 * 获取是否需要生成奖号
	 * 
	 * 此字段的版本 >= 0
	 * @return needCreateLottery value 类型为:int
	 * 
	 */
	public int getNeedCreateLottery()
	{
		return needCreateLottery;
	}


	/**
	 * 设置是否需要生成奖号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setNeedCreateLottery(int value)
	{
		this.needCreateLottery = value;
	}


	/**
	 * 获取获得奖号最低金额
	 * 
	 * 此字段的版本 >= 0
	 * @return leastDealFee value 类型为:long
	 * 
	 */
	public long getLeastDealFee()
	{
		return leastDealFee;
	}


	/**
	 * 设置获得奖号最低金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLeastDealFee(long value)
	{
		this.leastDealFee = value;
	}


	/**
	 * 获取是否可领奖
	 * 
	 * 此字段的版本 >= 0
	 * @return canChangeAward value 类型为:int
	 * 
	 */
	public int getCanChangeAward()
	{
		return canChangeAward;
	}


	/**
	 * 设置是否可领奖
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCanChangeAward(int value)
	{
		this.canChangeAward = value;
	}


	/**
	 * 获取领奖开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @return changeStartTime value 类型为:String
	 * 
	 */
	public String getChangeStartTime()
	{
		return changeStartTime;
	}


	/**
	 * 设置领奖开始时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChangeStartTime(String value)
	{
		if (value != null) {
				this.changeStartTime = value;
		}else{
				this.changeStartTime = new String();
		}
	}


	/**
	 * 获取领奖结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return changeEndTime value 类型为:String
	 * 
	 */
	public String getChangeEndTime()
	{
		return changeEndTime;
	}


	/**
	 * 设置领奖结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setChangeEndTime(String value)
	{
		if (value != null) {
				this.changeEndTime = value;
		}else{
				this.changeEndTime = new String();
		}
	}


	/**
	 * 获取是否可以抽奖
	 * 
	 * 此字段的版本 >= 0
	 * @return canDrawAward value 类型为:int
	 * 
	 */
	public int getCanDrawAward()
	{
		return canDrawAward;
	}


	/**
	 * 设置是否可以抽奖
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCanDrawAward(int value)
	{
		this.canDrawAward = value;
	}


	/**
	 * 获取抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @return drawAwardTime value 类型为:String
	 * 
	 */
	public String getDrawAwardTime()
	{
		return drawAwardTime;
	}


	/**
	 * 设置抽奖时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDrawAwardTime(String value)
	{
		if (value != null) {
				this.drawAwardTime = value;
		}else{
				this.drawAwardTime = new String();
		}
	}


	/**
	 * 获取活动简要规则
	 * 
	 * 此字段的版本 >= 0
	 * @return briefActRule value 类型为:String
	 * 
	 */
	public String getBriefActRule()
	{
		return this.decode(briefActRule);
	}


	/**
	 * 设置活动简要规则
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBriefActRule(String value)
	{
		if (value != null) {
				this.briefActRule = value;
		}else{
				this.briefActRule = new String();
		}
	}


	/**
	 * 获取活动详细规则
	 * 
	 * 此字段的版本 >= 0
	 * @return fullActRule value 类型为:String
	 * 
	 */
	public String getFullActRule()
	{
		return this.decode(fullActRule);
	}


	/**
	 * 设置活动详细规则
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFullActRule(String value)
	{
		if (value != null) {
				this.fullActRule = value;
		}else{
				this.fullActRule = new String();
		}
	}


	/**
	 * 获取活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @return joinPersonCount value 类型为:long
	 * 
	 */
	public long getJoinPersonCount()
	{
		return joinPersonCount;
	}


	/**
	 * 设置活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setJoinPersonCount(long value)
	{
		this.joinPersonCount = value;
	}


	/**
	 * 获取活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @return winPersonCount value 类型为:long
	 * 
	 */
	public long getWinPersonCount()
	{
		return winPersonCount;
	}


	/**
	 * 设置活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWinPersonCount(long value)
	{
		this.winPersonCount = value;
	}


	/**
	 * 获取活动奖品规则
	 * 
	 * 此字段的版本 >= 0
	 * @return actRuleInfo value 类型为:List<ActRuleV2>
	 * 
	 */
	public List<ActRuleV2> getActRuleInfo()
	{
		return actRuleInfo;
	}


	/**
	 * 设置活动奖品规则
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<ActRuleV2>
	 * 
	 */
	public void setActRuleInfo(List<ActRuleV2> value)
	{
		if (value != null) {
				this.actRuleInfo = value;
		}else{
				this.actRuleInfo = new ArrayList<ActRuleV2>();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return this.decode(reserved);
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		if (value != null) {
				this.reserved = value;
		}else{
				this.reserved = new String();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ActivityV2)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段actId的长度 size_of(long)
				length += ByteStream.getObjectSize(actTitle);  //计算字段actTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(bannerUrl);  //计算字段bannerUrl的长度 size_of(String)
				length += ByteStream.getObjectSize(orderStartTime);  //计算字段orderStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(orderEndTime);  //计算字段orderEndTime的长度 size_of(String)
				length += 4;  //计算字段needSignIn的长度 size_of(int)
				length += ByteStream.getObjectSize(signStartTime);  //计算字段signStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(signEndTime);  //计算字段signEndTime的长度 size_of(String)
				length += 4;  //计算字段colorTileGrade的长度 size_of(int)
				length += 4;  //计算字段qqvipLevel的长度 size_of(int)
				length += 4;  //计算字段buyerPointLevel的长度 size_of(int)
				length += 4;  //计算字段signTimes的长度 size_of(long)
				length += 4;  //计算字段limitSignTimes的长度 size_of(long)
				length += 4;  //计算字段needCreateLottery的长度 size_of(int)
				length += 4;  //计算字段leastDealFee的长度 size_of(long)
				length += 4;  //计算字段canChangeAward的长度 size_of(int)
				length += ByteStream.getObjectSize(changeStartTime);  //计算字段changeStartTime的长度 size_of(String)
				length += ByteStream.getObjectSize(changeEndTime);  //计算字段changeEndTime的长度 size_of(String)
				length += 4;  //计算字段canDrawAward的长度 size_of(int)
				length += ByteStream.getObjectSize(drawAwardTime);  //计算字段drawAwardTime的长度 size_of(String)
				length += ByteStream.getObjectSize(briefActRule);  //计算字段briefActRule的长度 size_of(String)
				length += ByteStream.getObjectSize(fullActRule);  //计算字段fullActRule的长度 size_of(String)
				length += 4;  //计算字段joinPersonCount的长度 size_of(long)
				length += 4;  //计算字段winPersonCount的长度 size_of(long)
				length += ByteStream.getObjectSize(actRuleInfo);  //计算字段actRuleInfo的长度 size_of(List)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	private String decode(String str){
		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
}
