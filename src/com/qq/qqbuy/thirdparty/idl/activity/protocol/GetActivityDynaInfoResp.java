 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2011-05-12 07:46::59
 *
 *@since version:1
*/
public class  GetActivityDynaInfoResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 活动参与人数
	 *
	 * 版本 >= 0
	 */
	 private int joinPersonCount;

	/**
	 * 活动获奖人数
	 *
	 * 版本 >= 0
	 */
	 private int winPersonCount;


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushInt(joinPersonCount);
		bs.pushInt(winPersonCount);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		joinPersonCount = bs.popInt();
		winPersonCount = bs.popInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008811L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @return joinPersonCount value 类型为:int
	 * 
	 */
	public int getJoinPersonCount()
	{
		return joinPersonCount;
	}


	/**
	 * 设置活动参与人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setJoinPersonCount(int value)
	{
		this.joinPersonCount = value;
	}


	/**
	 * 获取活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @return winPersonCount value 类型为:int
	 * 
	 */
	public int getWinPersonCount()
	{
		return winPersonCount;
	}


	/**
	 * 设置活动获奖人数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setWinPersonCount(int value)
	{
		this.winPersonCount = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetActivityDynaInfoResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段joinPersonCount的长度 size_of(int)
				length += 4;  //计算字段winPersonCount的长度 size_of(int)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
