 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.activity.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *@date 2011-05-13 04:27::41
 *
 *@since version:1
*/
public class  GetFitRuleLegalDealResp implements IServiceObject,Serializable
{
	public long result;
	/**
	 * IDL接口的标准返回错误码
	 *
	 * 版本 >= 0
	 */
	 private int errorCode;

	/**
	 * IDL接口的标准返回错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 中奖规则ID
	 *
	 * 版本 >= 0
	 */
	 private long ruleId;

	/**
	 * 订单总金额
	 *
	 * 版本 >= 0
	 */
	 private long dealTotalFee;

	/**
	 * 合规订单信息
	 *
	 * 版本 >= 0
	 */
	 private List<LegalDeal> legalDealInfo = new ArrayList<LegalDeal>();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errorCode);
		bs.pushString(errMsg);
		bs.pushUInt(ruleId);
		bs.pushUInt(dealTotalFee);
		bs.pushObject(legalDealInfo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errorCode = bs.popInt();
		errMsg = bs.popString();
		ruleId = bs.popUInt();
		dealTotalFee = bs.popUInt();
		legalDealInfo = (List<LegalDeal>)bs.popList(ArrayList.class,LegalDeal.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80008818L;
	}


	/**
	 * 获取IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errorCode value 类型为:int
	 * 
	 */
	public int getErrorCode()
	{
		return errorCode;
	}


	/**
	 * 设置IDL接口的标准返回错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrorCode(int value)
	{
		this.errorCode = value;
	}


	/**
	 * 获取IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置IDL接口的标准返回错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		if (value != null) {
				this.errMsg = value;
		}else{
				this.errMsg = new String();
		}
	}


	/**
	 * 获取中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ruleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return ruleId;
	}


	/**
	 * 设置中奖规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.ruleId = value;
	}


	/**
	 * 获取订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @return dealTotalFee value 类型为:long
	 * 
	 */
	public long getDealTotalFee()
	{
		return dealTotalFee;
	}


	/**
	 * 设置订单总金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealTotalFee(long value)
	{
		this.dealTotalFee = value;
	}


	/**
	 * 获取合规订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @return legalDealInfo value 类型为:List<LegalDeal>
	 * 
	 */
	public List<LegalDeal> getLegalDealInfo()
	{
		return legalDealInfo;
	}


	/**
	 * 设置合规订单信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<LegalDeal>
	 * 
	 */
	public void setLegalDealInfo(List<LegalDeal> value)
	{
		if (value != null) {
				this.legalDealInfo = value;
		}else{
				this.legalDealInfo = new ArrayList<LegalDeal>();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetFitRuleLegalDealResp)
				length += 4;  //计算字段errorCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += 4;  //计算字段ruleId的长度 size_of(long)
				length += 4;  //计算字段dealTotalFee的长度 size_of(long)
				length += ByteStream.getObjectSize(legalDealInfo);  //计算字段legalDealInfo的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
