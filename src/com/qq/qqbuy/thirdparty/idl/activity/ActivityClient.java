package com.qq.qqbuy.thirdparty.idl.activity;

import java.net.InetSocketAddress;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDrawWinnerInfoReq;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDrawWinnerInfoResp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDynaInfoReq;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDynaInfoResp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDynaInfoV2Req;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityDynaInfoV2Resp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityInfoV2Req;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetActivityInfoV2Resp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerActivityInfoReq;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerActivityInfoResp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerJoinActivityInfoReq;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerJoinActivityInfoResp;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerJoinStateReq;
import com.qq.qqbuy.thirdparty.idl.activity.protocol.GetBuyerJoinStateResp;

/* @FileName: ActivityClient.java    
 * @Description: TODO 
 * @author: homerwu   
 * @date:2013-1-8 下午03:49:50    
 */
public class ActivityClient  extends SupportIDLBaseClient{
	
	/**
	 * 买家参与活动列表
	 * @param req
	 * @return
	 */
	public static GetBuyerJoinActivityInfoResp getBuyerJoinActivityInfo(
			GetBuyerJoinActivityInfoReq req) {

        long start = System.currentTimeMillis();
        GetBuyerJoinActivityInfoResp resp = new GetBuyerJoinActivityInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 所有活动列表
	 * @param req
	 * @return
	 */
	public static GetBuyerActivityInfoResp getBuyerActivityInfoReq(
			GetBuyerActivityInfoReq req) {

        long start = System.currentTimeMillis();
        GetBuyerActivityInfoResp resp = new GetBuyerActivityInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 活动详情
	 * @param req
	 * @return
	 */
	public static GetActivityInfoV2Resp getActivityInfo(
			GetActivityInfoV2Req req) {

        long start = System.currentTimeMillis();
        GetActivityInfoV2Resp resp = new GetActivityInfoV2Resp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 买家参与状态
	 * @param req
	 * @return
	 */
	public static GetBuyerJoinStateResp getBuyerJoinState(
			GetBuyerJoinStateReq req) {

        long start = System.currentTimeMillis();
        GetBuyerJoinStateResp resp = new GetBuyerJoinStateResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 获取活动参与信息
	 * @param req
	 * @return
	 */
	public static GetActivityDynaInfoV2Resp getActivityDynaInfoV2(
			GetActivityDynaInfoV2Req req) {

        long start = System.currentTimeMillis();
        GetActivityDynaInfoV2Resp resp = new GetActivityDynaInfoV2Resp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
	
	public static GetActivityDynaInfoResp getActivityDynaInfo(
			GetActivityDynaInfoReq req) {

        long start = System.currentTimeMillis();
        GetActivityDynaInfoResp resp = new GetActivityDynaInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }
        return ret == SUCCESS ? resp : null;
	}
	
	/**
	 * 获取活动参与信息
	 * @param req
	 * @return
	 */
	public static GetActivityDrawWinnerInfoResp getActivityDrawWinnerInfo(
			GetActivityDrawWinnerInfoReq req) {

        long start = System.currentTimeMillis();
        GetActivityDrawWinnerInfoResp resp = new GetActivityDrawWinnerInfoResp();
        int ret;
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPai();
        if(EnvManager.isIdc())
        {
        	InetSocketAddress address = getAddressFromConfig("qgo.sys.activity");
            ret = invokePaiPaiIDL(req, resp, stub,address.getHostName(),address.getPort());
        }
        else
        {
        	ret = invokePaiPaiIDL(req, resp,stub, "172.23.0.58", 9107);
        }

        return ret == SUCCESS ? resp : null;
	}
}
