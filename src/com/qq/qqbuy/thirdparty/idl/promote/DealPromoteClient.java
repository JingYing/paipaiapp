package com.qq.qqbuy.thirdparty.idl.promote;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.dealpromote.po.DealPromotePo;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromoteSaleReq;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.DealPromoteSaleResp;
import com.qq.qqbuy.thirdparty.idl.promote.protocol.PromoteFilter;

public class DealPromoteClient extends SupportIDLBaseClient  {
	// 订单侧ip
//	protected static final String BETA_IP = "10.6.223.152";
//	protected static final String DEV_IP = "10.6.222.151";
	
	// 营销侧ip
	protected static final String BETA_IP = "172.25.36.38";
	protected static final String DEV_IP = "172.25.32.186";
	protected static final int PROMOTE_PORT = 53101;
	
	protected static final String DEAL_PROMOTE_CALL_IDL_DEAL_SOURCE = "mobileLife";
	
	protected static final Long DEAL_PROMOTE_ORDER_FROM_FOR_WEI_DIAN = 8L ;

	protected static final Long DEAL_PROMOTE_ORDER_FROM_APP = 11L ;

	protected static IAsynWebStub getWebStub4DealPromote() {
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if (EnvManager.isLocal() || EnvManager.isGamma()) {
			//stub.setIpAndPort(BETA_IP, PROMOTE_PORT);
//			stub.setIpAndPort(DEV_IP, PROMOTE_PORT);
		}
		return stub;
	}
	
	// 分单
	public static DealPromoteSaleResp queryPromote(DealPromotePo po, String sk, String mk) {
//		long start = System.currentTimeMillis();
		
		PromoteFilter promoteFilter = new PromoteFilter();
		
		promoteFilter.setBuyerUin(po.getBuyerUin());
		promoteFilter.setDealSource(DEAL_PROMOTE_CALL_IDL_DEAL_SOURCE);
		promoteFilter.setReqSource(po.getReqSource());
		promoteFilter.setDealPromoteList(po.getDealPromoteList());
//		if (EnvManager.isGamma()){//立即购买批价--修改orderfrom=11，场景券支持--郭申
			promoteFilter.setOrderFrom(DEAL_PROMOTE_ORDER_FROM_APP) ;
//		}else{
//			promoteFilter.setOrderFrom(DEAL_PROMOTE_ORDER_FROM_FOR_WEI_DIAN) ;
//		}
		DealPromoteSaleReq req = new DealPromoteSaleReq();
		req.setOPromoteFilter(promoteFilter);
		
		DealPromoteSaleResp resp = new DealPromoteSaleResp();
		
		IAsynWebStub stub = getWebStub4DealPromote();
		stub.setSkey(sk.getBytes());
		stub.setMachineKey(mk.getBytes());
		stub.setOperator(po.getBuyerUin());
		
		int ret = invokePaiPaiIDL(req, resp, stub);
		Log.run.info("DealPromoteSaleReq#DealPromoteSaleResp#ip=" + stub.getIp());
//		long timeCost = System.currentTimeMillis() - start;
		
		String msg =  "IDL批价成功";
		if (ret != 0 || resp.getResult() != 0)
		{
			msg = "IDL批价失败";
		}
		return ret == SUCCESS ? resp : null;
	}
}
