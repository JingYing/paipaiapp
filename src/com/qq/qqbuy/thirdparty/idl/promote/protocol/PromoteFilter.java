 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *订单批价po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class PromoteFilter  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 买家号码
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 订单来源，区分PC 端与移动端
	 *
	 * 版本 >= 0
	 */
	 private String DealSource = new String();

	/**
	 * 订单业务请求来源, 购物车页面、下单页、确认页
	 *
	 * 版本 >= 0
	 */
	 private long ReqSource;

	/**
	 * 特价订单标识
	 *
	 * 版本 >= 0
	 */
	 private long OrderFrom;

	/**
	 * 批价订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<DealPromote> DealPromoteList = new Vector<DealPromote>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushUInt(BuyerUin);
		bs.pushString(DealSource);
		bs.pushUInt(ReqSource);
		bs.pushUInt(OrderFrom);
		bs.pushObject(DealPromoteList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		BuyerUin = bs.popUInt();
		DealSource = bs.popString();
		ReqSource = bs.popUInt();
		OrderFrom = bs.popUInt();
		DealPromoteList = (Vector<DealPromote>)bs.popVector(DealPromote.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取买家号码
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取订单来源，区分PC 端与移动端
	 * 
	 * 此字段的版本 >= 0
	 * @return DealSource value 类型为:String
	 * 
	 */
	public String getDealSource()
	{
		return DealSource;
	}


	/**
	 * 设置订单来源，区分PC 端与移动端
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealSource(String value)
	{
		if (value != null) {
				this.DealSource = value;
		}else{
				this.DealSource = new String();
		}
	}


	/**
	 * 获取订单业务请求来源, 购物车页面、下单页、确认页
	 * 
	 * 此字段的版本 >= 0
	 * @return ReqSource value 类型为:long
	 * 
	 */
	public long getReqSource()
	{
		return ReqSource;
	}


	/**
	 * 设置订单业务请求来源, 购物车页面、下单页、确认页
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReqSource(long value)
	{
		this.ReqSource = value;
	}


	/**
	 * 获取特价订单标识
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderFrom value 类型为:long
	 * 
	 */
	public long getOrderFrom()
	{
		return OrderFrom;
	}


	/**
	 * 设置特价订单标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderFrom(long value)
	{
		this.OrderFrom = value;
	}


	/**
	 * 获取批价订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPromoteList value 类型为:Vector<DealPromote>
	 * 
	 */
	public Vector<DealPromote> getDealPromoteList()
	{
		return DealPromoteList;
	}


	/**
	 * 设置批价订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DealPromote>
	 * 
	 */
	public void setDealPromoteList(Vector<DealPromote> value)
	{
		if (value != null) {
				this.DealPromoteList = value;
		}else{
				this.DealPromoteList = new Vector<DealPromote>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PromoteFilter)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealSource);  //计算字段DealSource的长度 size_of(String)
				length += 4;  //计算字段ReqSource的长度 size_of(uint32_t)
				length += 4;  //计算字段OrderFrom的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealPromoteList);  //计算字段DealPromoteList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
