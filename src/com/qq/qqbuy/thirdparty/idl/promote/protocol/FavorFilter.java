 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *请求的优惠集合
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class FavorFilter  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 请求优惠类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 优惠ID列表，如请求的红包ID，包邮卡ID，店铺优惠券ID
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> FavorIdList = new Vector<uint64_t>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushUInt(Type);
		bs.pushObject(FavorIdList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		Type = bs.popUInt();
		FavorIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取请求优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置请求优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取优惠ID列表，如请求的红包ID，包邮卡ID，店铺优惠券ID
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getFavorIdList()
	{
		return FavorIdList;
	}


	/**
	 * 设置优惠ID列表，如请求的红包ID，包邮卡ID，店铺优惠券ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setFavorIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.FavorIdList = value;
		}else{
				this.FavorIdList = new Vector<uint64_t>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavorFilter)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorIdList);  //计算字段FavorIdList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
