 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.HashSet;
import java.util.Set;

/**
 *商品与订单维度优惠po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class FavorEx  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 概括优惠类型
	 *
	 * 版本 >= 0
	 */
	 private long TotalType;

	/**
	 * 优惠类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 优惠标识
	 *
	 * 版本 >= 0
	 */
	 private long Flag;

	/**
	 * 优惠ID
	 *
	 * 版本 >= 0
	 */
	 private long FavorId;

	/**
	 * 买家号码
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 优惠面值
	 *
	 * 版本 >= 0
	 */
	 private int FavorPrice;

	/**
	 * 优惠面值
	 *
	 * 版本 >= 0
	 */
	 private long Minimum;

	/**
	 * 优惠名称
	 *
	 * 版本 >= 0
	 */
	 private String FavorName = new String();

	/**
	 * 优惠描述
	 *
	 * 版本 >= 0
	 */
	 private String FavorDesc = new String();

	/**
	 * 生效时间
	 *
	 * 版本 >= 0
	 */
	 private long BeginTime;

	/**
	 * 失效时间
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 适用店铺集合
	 *
	 * 版本 >= 0
	 */
	 private Set<uint32_t> setApplicableScope = new HashSet<uint32_t>();

	/**
	 * 选中标识
	 *
	 * 版本 >= 0
	 */
	 private long CheckStat;

	/**
	 * 调价标识
	 *
	 * 版本 >= 0
	 */
	 private long ChangeStat;

	/**
	 * 可用标识
	 *
	 * 版本 >= 0
	 */
	 private long UseStat;

	/**
	 * 包邮标识
	 *
	 * 版本 >= 0
	 */
	 private long PostStat;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 商品库存
	 *
	 * 版本 >= 0
	 */
	 private String Attr = new String();

	/**
	 * 商品允许抵消金额
	 *
	 * 版本 >= 0
	 */
	 private long AllowFee;

	/**
	 * 实际优惠金额
	 *
	 * 版本 >= 0
	 */
	 private int RealFee;

	/**
	 * 商品skuid
	 *
	 * 版本 >= 0
	 */
	 private long Skuid;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushUInt(TotalType);
		bs.pushUInt(Type);
		bs.pushUInt(Flag);
		bs.pushLong(FavorId);
		bs.pushUInt(BuyerUin);
		bs.pushInt(FavorPrice);
		bs.pushUInt(Minimum);
		bs.pushString(FavorName);
		bs.pushString(FavorDesc);
		bs.pushUInt(BeginTime);
		bs.pushUInt(EndTime);
		bs.pushObject(setApplicableScope);
		bs.pushUInt(CheckStat);
		bs.pushUInt(ChangeStat);
		bs.pushUInt(UseStat);
		bs.pushUInt(PostStat);
		bs.pushString(ItemId);
		bs.pushString(Attr);
		bs.pushUInt(AllowFee);
		bs.pushInt(RealFee);
		bs.pushLong(Skuid);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		TotalType = bs.popUInt();
		Type = bs.popUInt();
		Flag = bs.popUInt();
		FavorId = bs.popLong();
		BuyerUin = bs.popUInt();
		FavorPrice = bs.popInt();
		Minimum = bs.popUInt();
		FavorName = bs.popString();
		FavorDesc = bs.popString();
		BeginTime = bs.popUInt();
		EndTime = bs.popUInt();
		setApplicableScope = (Set<uint32_t>)bs.popSet(HashSet.class,uint32_t.class);
		CheckStat = bs.popUInt();
		ChangeStat = bs.popUInt();
		UseStat = bs.popUInt();
		PostStat = bs.popUInt();
		ItemId = bs.popString();
		Attr = bs.popString();
		AllowFee = bs.popUInt();
		RealFee = bs.popInt();
		Skuid = bs.popLong();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取概括优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalType value 类型为:long
	 * 
	 */
	public long getTotalType()
	{
		return TotalType;
	}


	/**
	 * 设置概括优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalType(long value)
	{
		this.TotalType = value;
	}


	/**
	 * 获取优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置优惠类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取优惠标识
	 * 
	 * 此字段的版本 >= 0
	 * @return Flag value 类型为:long
	 * 
	 */
	public long getFlag()
	{
		return Flag;
	}


	/**
	 * 设置优惠标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlag(long value)
	{
		this.Flag = value;
	}


	/**
	 * 获取优惠ID
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorId value 类型为:long
	 * 
	 */
	public long getFavorId()
	{
		return FavorId;
	}


	/**
	 * 设置优惠ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFavorId(long value)
	{
		this.FavorId = value;
	}


	/**
	 * 获取买家号码
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取优惠面值
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorPrice value 类型为:int
	 * 
	 */
	public int getFavorPrice()
	{
		return FavorPrice;
	}


	/**
	 * 设置优惠面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setFavorPrice(int value)
	{
		this.FavorPrice = value;
	}


	/**
	 * 获取优惠面值
	 * 
	 * 此字段的版本 >= 0
	 * @return Minimum value 类型为:long
	 * 
	 */
	public long getMinimum()
	{
		return Minimum;
	}


	/**
	 * 设置优惠面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMinimum(long value)
	{
		this.Minimum = value;
	}


	/**
	 * 获取优惠名称
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorName value 类型为:String
	 * 
	 */
	public String getFavorName()
	{
		return FavorName;
	}


	/**
	 * 设置优惠名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFavorName(String value)
	{
		if (value != null) {
				this.FavorName = value;
		}else{
				this.FavorName = new String();
		}
	}


	/**
	 * 获取优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorDesc value 类型为:String
	 * 
	 */
	public String getFavorDesc()
	{
		return FavorDesc;
	}


	/**
	 * 设置优惠描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFavorDesc(String value)
	{
		if (value != null) {
				this.FavorDesc = value;
		}else{
				this.FavorDesc = new String();
		}
	}


	/**
	 * 获取生效时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BeginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return BeginTime;
	}


	/**
	 * 设置生效时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.BeginTime = value;
	}


	/**
	 * 获取失效时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置失效时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
	}


	/**
	 * 获取适用店铺集合
	 * 
	 * 此字段的版本 >= 0
	 * @return setApplicableScope value 类型为:Set<uint32_t>
	 * 
	 */
	public Set<uint32_t> getSetApplicableScope()
	{
		return setApplicableScope;
	}


	/**
	 * 设置适用店铺集合
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Set<uint32_t>
	 * 
	 */
	public void setSetApplicableScope(Set<uint32_t> value)
	{
		if (value != null) {
				this.setApplicableScope = value;
		}else{
				this.setApplicableScope = new HashSet<uint32_t>();
		}
	}


	/**
	 * 获取选中标识
	 * 
	 * 此字段的版本 >= 0
	 * @return CheckStat value 类型为:long
	 * 
	 */
	public long getCheckStat()
	{
		return CheckStat;
	}


	/**
	 * 设置选中标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCheckStat(long value)
	{
		this.CheckStat = value;
	}


	/**
	 * 获取调价标识
	 * 
	 * 此字段的版本 >= 0
	 * @return ChangeStat value 类型为:long
	 * 
	 */
	public long getChangeStat()
	{
		return ChangeStat;
	}


	/**
	 * 设置调价标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setChangeStat(long value)
	{
		this.ChangeStat = value;
	}


	/**
	 * 获取可用标识
	 * 
	 * 此字段的版本 >= 0
	 * @return UseStat value 类型为:long
	 * 
	 */
	public long getUseStat()
	{
		return UseStat;
	}


	/**
	 * 设置可用标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUseStat(long value)
	{
		this.UseStat = value;
	}


	/**
	 * 获取包邮标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PostStat value 类型为:long
	 * 
	 */
	public long getPostStat()
	{
		return PostStat;
	}


	/**
	 * 设置包邮标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPostStat(long value)
	{
		this.PostStat = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.ItemId = value;
		}else{
				this.ItemId = new String();
		}
	}


	/**
	 * 获取商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return Attr;
	}


	/**
	 * 设置商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		if (value != null) {
				this.Attr = value;
		}else{
				this.Attr = new String();
		}
	}


	/**
	 * 获取商品允许抵消金额
	 * 
	 * 此字段的版本 >= 0
	 * @return AllowFee value 类型为:long
	 * 
	 */
	public long getAllowFee()
	{
		return AllowFee;
	}


	/**
	 * 设置商品允许抵消金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAllowFee(long value)
	{
		this.AllowFee = value;
	}


	/**
	 * 获取实际优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @return RealFee value 类型为:int
	 * 
	 */
	public int getRealFee()
	{
		return RealFee;
	}


	/**
	 * 设置实际优惠金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setRealFee(int value)
	{
		this.RealFee = value;
	}


	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return Skuid value 类型为:long
	 * 
	 */
	public long getSkuid()
	{
		return Skuid;
	}


	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuid(long value)
	{
		this.Skuid = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(FavorEx)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 4;  //计算字段TotalType的长度 size_of(uint32_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 4;  //计算字段Flag的长度 size_of(uint32_t)
				length += 17;  //计算字段FavorId的长度 size_of(uint64_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段FavorPrice的长度 size_of(int)
				length += 4;  //计算字段Minimum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorName);  //计算字段FavorName的长度 size_of(String)
				length += ByteStream.getObjectSize(FavorDesc);  //计算字段FavorDesc的长度 size_of(String)
				length += 4;  //计算字段BeginTime的长度 size_of(uint32_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(setApplicableScope);  //计算字段setApplicableScope的长度 size_of(Set)
				length += 4;  //计算字段CheckStat的长度 size_of(uint32_t)
				length += 4;  //计算字段ChangeStat的长度 size_of(uint32_t)
				length += 4;  //计算字段UseStat的长度 size_of(uint32_t)
				length += 4;  //计算字段PostStat的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(Attr);  //计算字段Attr的长度 size_of(String)
				length += 4;  //计算字段AllowFee的长度 size_of(uint32_t)
				length += 4;  //计算字段RealFee的长度 size_of(int)
				length += 17;  //计算字段Skuid的长度 size_of(uint64_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
