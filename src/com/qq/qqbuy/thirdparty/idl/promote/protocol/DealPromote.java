 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *请求批价的订单po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class DealPromote  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 订单ID
	 *
	 * 版本 >= 0
	 */
	 private long DealId;

	/**
	 * 订单SeqId
	 *
	 * 版本 >= 0
	 */
	 private long SeqId;

	/**
	 * 卖家号码
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 卖家号码
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 场景ID
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 是否参加促销标识
	 *
	 * 版本 >= 0
	 */
	 private long MarketStat;

	/**
	 * 套餐ID
	 *
	 * 版本 >= 0
	 */
	 private String ComBoId = new String();

	/**
	 * 商品货到付款标识
	 *
	 * 版本 >= 0
	 */
	 private long PayOnReciew;

	/**
	 * 订单维度请求的优惠列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<FavorFilter> FavorFilterList = new Vector<FavorFilter>();

	/**
	 * 批价订单中商品列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemPromote> ItemPromoteList = new Vector<ItemPromote>();

	/**
	 * 商品MD5值
	 *
	 * 版本 >= 0
	 */
	 private String MDValue = new String();

	/**
	 * 标识是否网购卖家0非网购卖家 1网购卖家
	 * 2 使用红包
	 * 版本 >= 0
	 */
	 private long UserPropery;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushLong(DealId);
		bs.pushLong(SeqId);
		bs.pushUInt(SellerUin);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SceneId);
		bs.pushUInt(MarketStat);
		bs.pushString(ComBoId);
		bs.pushUInt(PayOnReciew);
		bs.pushObject(FavorFilterList);
		bs.pushObject(ItemPromoteList);
		bs.pushString(MDValue);
		bs.pushUInt(UserPropery);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		DealId = bs.popLong();
		SeqId = bs.popLong();
		SellerUin = bs.popUInt();
		BuyerUin = bs.popUInt();
		SceneId = bs.popUInt();
		MarketStat = bs.popUInt();
		ComBoId = bs.popString();
		PayOnReciew = bs.popUInt();
		FavorFilterList = (Vector<FavorFilter>)bs.popVector(FavorFilter.class);
		ItemPromoteList = (Vector<ItemPromote>)bs.popVector(ItemPromote.class);
		MDValue = bs.popString();
		UserPropery = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:long
	 * 
	 */
	public long getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealId(long value)
	{
		this.DealId = value;
	}


	/**
	 * 获取订单SeqId
	 * 
	 * 此字段的版本 >= 0
	 * @return SeqId value 类型为:long
	 * 
	 */
	public long getSeqId()
	{
		return SeqId;
	}


	/**
	 * 设置订单SeqId
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSeqId(long value)
	{
		this.SeqId = value;
	}


	/**
	 * 获取卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置卖家号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取场景ID
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取是否参加促销标识
	 * 
	 * 此字段的版本 >= 0
	 * @return MarketStat value 类型为:long
	 * 
	 */
	public long getMarketStat()
	{
		return MarketStat;
	}


	/**
	 * 设置是否参加促销标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketStat(long value)
	{
		this.MarketStat = value;
	}


	/**
	 * 获取套餐ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ComBoId value 类型为:String
	 * 
	 */
	public String getComBoId()
	{
		return ComBoId;
	}


	/**
	 * 设置套餐ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setComBoId(String value)
	{
		if (value != null) {
				this.ComBoId = value;
		}else{
				this.ComBoId = new String();
		}
	}


	/**
	 * 获取商品货到付款标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PayOnReciew value 类型为:long
	 * 
	 */
	public long getPayOnReciew()
	{
		return PayOnReciew;
	}


	/**
	 * 设置商品货到付款标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayOnReciew(long value)
	{
		this.PayOnReciew = value;
	}


	/**
	 * 获取订单维度请求的优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorFilterList value 类型为:Vector<FavorFilter>
	 * 
	 */
	public Vector<FavorFilter> getFavorFilterList()
	{
		return FavorFilterList;
	}


	/**
	 * 设置订单维度请求的优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FavorFilter>
	 * 
	 */
	public void setFavorFilterList(Vector<FavorFilter> value)
	{
		if (value != null) {
				this.FavorFilterList = value;
		}else{
				this.FavorFilterList = new Vector<FavorFilter>();
		}
	}


	/**
	 * 获取批价订单中商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPromoteList value 类型为:Vector<ItemPromote>
	 * 
	 */
	public Vector<ItemPromote> getItemPromoteList()
	{
		return ItemPromoteList;
	}


	/**
	 * 设置批价订单中商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemPromote>
	 * 
	 */
	public void setItemPromoteList(Vector<ItemPromote> value)
	{
		if (value != null) {
				this.ItemPromoteList = value;
		}else{
				this.ItemPromoteList = new Vector<ItemPromote>();
		}
	}


	/**
	 * 获取商品MD5值
	 * 
	 * 此字段的版本 >= 0
	 * @return MDValue value 类型为:String
	 * 
	 */
	public String getMDValue()
	{
		return MDValue;
	}


	/**
	 * 设置商品MD5值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMDValue(String value)
	{
		if (value != null) {
				this.MDValue = value;
		}else{
				this.MDValue = new String();
		}
	}


	/**
	 * 获取标识是否网购卖家0非网购卖家 1网购卖家
	 * 
	 * 此字段的版本 >= 0
	 * @return UserPropery value 类型为:long
	 * 
	 */
	public long getUserPropery()
	{
		return UserPropery;
	}


	/**
	 * 设置标识是否网购卖家0非网购卖家 1网购卖家
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUserPropery(long value)
	{
		this.UserPropery = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(DealPromote)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += 17;  //计算字段DealId的长度 size_of(uint64_t)
				length += 17;  //计算字段SeqId的长度 size_of(uint64_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段MarketStat的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ComBoId);  //计算字段ComBoId的长度 size_of(String)
				length += 4;  //计算字段PayOnReciew的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FavorFilterList);  //计算字段FavorFilterList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(ItemPromoteList);  //计算字段ItemPromoteList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MDValue);  //计算字段MDValue的长度 size_of(String)
				length += 4;  //计算字段UserPropery的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
