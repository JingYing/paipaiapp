 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.promote.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import java.util.Vector;

/**
 *商品维度优惠po
 *
 *@date 2013-09-12 07:21::36
 *
 *@since version:1
*/
public class ItemPromoteFavor  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 4; 

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 商品库存
	 *
	 * 版本 >= 0
	 */
	 private String Attr = new String();

	/**
	 * 商品维度多价列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemPrice> ItemPriceList = new Vector<ItemPrice>();

	/**
	 * 商品维度优惠列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<FavorEx> FavorExList = new Vector<FavorEx>();

	/**
	 * 商品skuid
	 *
	 * 版本 >= 0
	 */
	 private long Skuid;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUShort(version);
		bs.pushString(ItemId);
		bs.pushString(Attr);
		bs.pushObject(ItemPriceList);
		bs.pushObject(FavorExList);
		bs.pushLong(Skuid);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUShort();
		ItemId = bs.popString();
		Attr = bs.popString();
		ItemPriceList = (Vector<ItemPrice>)bs.popVector(ItemPrice.class);
		FavorExList = (Vector<FavorEx>)bs.popVector(FavorEx.class);
		Skuid = bs.popLong();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		if (value != null) {
				this.ItemId = value;
		}else{
				this.ItemId = new String();
		}
	}


	/**
	 * 获取商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return Attr;
	}


	/**
	 * 设置商品库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		if (value != null) {
				this.Attr = value;
		}else{
				this.Attr = new String();
		}
	}


	/**
	 * 获取商品维度多价列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemPriceList value 类型为:Vector<ItemPrice>
	 * 
	 */
	public Vector<ItemPrice> getItemPriceList()
	{
		return ItemPriceList;
	}


	/**
	 * 设置商品维度多价列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemPrice>
	 * 
	 */
	public void setItemPriceList(Vector<ItemPrice> value)
	{
		if (value != null) {
				this.ItemPriceList = value;
		}else{
				this.ItemPriceList = new Vector<ItemPrice>();
		}
	}


	/**
	 * 获取商品维度优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @return FavorExList value 类型为:Vector<FavorEx>
	 * 
	 */
	public Vector<FavorEx> getFavorExList()
	{
		return FavorExList;
	}


	/**
	 * 设置商品维度优惠列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<FavorEx>
	 * 
	 */
	public void setFavorExList(Vector<FavorEx> value)
	{
		if (value != null) {
				this.FavorExList = value;
		}else{
				this.FavorExList = new Vector<FavorEx>();
		}
	}


	/**
	 * 获取商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return Skuid value 类型为:long
	 * 
	 */
	public long getSkuid()
	{
		return Skuid;
	}


	/**
	 * 设置商品skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSkuid(long value)
	{
		this.Skuid = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPromoteFavor)
				length += 2;  //计算字段version的长度 size_of(uint16_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += ByteStream.getObjectSize(Attr);  //计算字段Attr的长度 size_of(String)
				length += ByteStream.getObjectSize(ItemPriceList);  //计算字段ItemPriceList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(FavorExList);  //计算字段FavorExList的长度 size_of(Vector)
				length += 17;  //计算字段Skuid的长度 size_of(uint64_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
