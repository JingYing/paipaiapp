//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bounspackage.idl.BonusPackagesAo.java

package com.qq.qqbuy.thirdparty.idl.bounspackage.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *
 *
 *@date 2015-03-10 04:52:47
 *
 *@since version:1
*/
public class BonusPackage  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwId;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwRuleId;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sName = new String();

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iState;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwOwnerUin;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sOpenId = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sOwnerNickname = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sKey = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwStartTime;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwEndTime;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iBusinessType;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sBusinessKey0 = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sBusinessKey1 = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sBusinessKey2 = new String();

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iTotalValue;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iLeftValue;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iTotalNumber;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iLeftNumber;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iCouponMaxValue;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iCouponMinValue;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwCouponStartTime;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwCouponEndTime;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iShareTimes;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iCouponUseThreshold;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iRequestSource;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwUpdateTime;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iExt1;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iExt2;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iExt3;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iExt4;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sExt5 = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sExt6 = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sExt7 = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sExt8 = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(ddwId);
		bs.pushLong(ddwRuleId);
		bs.pushString(sName);
		bs.pushInt(iState);
		bs.pushLong(ddwOwnerUin);
		bs.pushString(sOpenId);
		bs.pushString(sOwnerNickname);
		bs.pushString(sKey);
		bs.pushUInt(dwStartTime);
		bs.pushUInt(dwEndTime);
		bs.pushInt(iBusinessType);
		bs.pushString(sBusinessKey0);
		bs.pushString(sBusinessKey1);
		bs.pushString(sBusinessKey2);
		bs.pushInt(iTotalValue);
		bs.pushInt(iLeftValue);
		bs.pushInt(iTotalNumber);
		bs.pushInt(iLeftNumber);
		bs.pushInt(iCouponMaxValue);
		bs.pushInt(iCouponMinValue);
		bs.pushUInt(dwCouponStartTime);
		bs.pushUInt(dwCouponEndTime);
		bs.pushInt(iShareTimes);
		bs.pushInt(iCouponUseThreshold);
		bs.pushInt(iRequestSource);
		bs.pushUInt(dwUpdateTime);
		bs.pushInt(iExt1);
		bs.pushInt(iExt2);
		bs.pushInt(iExt3);
		bs.pushInt(iExt4);
		bs.pushString(sExt5);
		bs.pushString(sExt6);
		bs.pushString(sExt7);
		bs.pushString(sExt8);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		ddwId = bs.popLong();
		ddwRuleId = bs.popLong();
		sName = bs.popString();
		iState = bs.popInt();
		ddwOwnerUin = bs.popLong();
		sOpenId = bs.popString();
		sOwnerNickname = bs.popString();
		sKey = bs.popString();
		dwStartTime = bs.popUInt();
		dwEndTime = bs.popUInt();
		iBusinessType = bs.popInt();
		sBusinessKey0 = bs.popString();
		sBusinessKey1 = bs.popString();
		sBusinessKey2 = bs.popString();
		iTotalValue = bs.popInt();
		iLeftValue = bs.popInt();
		iTotalNumber = bs.popInt();
		iLeftNumber = bs.popInt();
		iCouponMaxValue = bs.popInt();
		iCouponMinValue = bs.popInt();
		dwCouponStartTime = bs.popUInt();
		dwCouponEndTime = bs.popUInt();
		iShareTimes = bs.popInt();
		iCouponUseThreshold = bs.popInt();
		iRequestSource = bs.popInt();
		dwUpdateTime = bs.popUInt();
		iExt1 = bs.popInt();
		iExt2 = bs.popInt();
		iExt3 = bs.popInt();
		iExt4 = bs.popInt();
		sExt5 = bs.popString();
		sExt6 = bs.popString();
		sExt7 = bs.popString();
		sExt8 = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwId value 类型为:long
	 * 
	 */
	public long getDdwId()
	{
		return ddwId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwId(long value)
	{
		this.ddwId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwRuleId value 类型为:long
	 * 
	 */
	public long getDdwRuleId()
	{
		return ddwRuleId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwRuleId(long value)
	{
		this.ddwRuleId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sName value 类型为:String
	 * 
	 */
	public String getSName()
	{
		return sName;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSName(String value)
	{
		this.sName = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iState value 类型为:int
	 * 
	 */
	public int getIState()
	{
		return iState;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIState(int value)
	{
		this.iState = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwOwnerUin value 类型为:long
	 * 
	 */
	public long getDdwOwnerUin()
	{
		return ddwOwnerUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwOwnerUin(long value)
	{
		this.ddwOwnerUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sOpenId value 类型为:String
	 * 
	 */
	public String getSOpenId()
	{
		return sOpenId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSOpenId(String value)
	{
		this.sOpenId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sOwnerNickname value 类型为:String
	 * 
	 */
	public String getSOwnerNickname()
	{
		return sOwnerNickname;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSOwnerNickname(String value)
	{
		this.sOwnerNickname = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sKey value 类型为:String
	 * 
	 */
	public String getSKey()
	{
		return sKey;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSKey(String value)
	{
		this.sKey = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwStartTime value 类型为:long
	 * 
	 */
	public long getDwStartTime()
	{
		return dwStartTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwStartTime(long value)
	{
		this.dwStartTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEndTime value 类型为:long
	 * 
	 */
	public long getDwEndTime()
	{
		return dwEndTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwEndTime(long value)
	{
		this.dwEndTime = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iBusinessType value 类型为:int
	 * 
	 */
	public int getIBusinessType()
	{
		return iBusinessType;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIBusinessType(int value)
	{
		this.iBusinessType = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sBusinessKey0 value 类型为:String
	 * 
	 */
	public String getSBusinessKey0()
	{
		return sBusinessKey0;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSBusinessKey0(String value)
	{
		this.sBusinessKey0 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sBusinessKey1 value 类型为:String
	 * 
	 */
	public String getSBusinessKey1()
	{
		return sBusinessKey1;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSBusinessKey1(String value)
	{
		this.sBusinessKey1 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sBusinessKey2 value 类型为:String
	 * 
	 */
	public String getSBusinessKey2()
	{
		return sBusinessKey2;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSBusinessKey2(String value)
	{
		this.sBusinessKey2 = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iTotalValue value 类型为:int
	 * 
	 */
	public int getITotalValue()
	{
		return iTotalValue;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setITotalValue(int value)
	{
		this.iTotalValue = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iLeftValue value 类型为:int
	 * 
	 */
	public int getILeftValue()
	{
		return iLeftValue;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setILeftValue(int value)
	{
		this.iLeftValue = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iTotalNumber value 类型为:int
	 * 
	 */
	public int getITotalNumber()
	{
		return iTotalNumber;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setITotalNumber(int value)
	{
		this.iTotalNumber = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iLeftNumber value 类型为:int
	 * 
	 */
	public int getILeftNumber()
	{
		return iLeftNumber;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setILeftNumber(int value)
	{
		this.iLeftNumber = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iCouponMaxValue value 类型为:int
	 * 
	 */
	public int getICouponMaxValue()
	{
		return iCouponMaxValue;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setICouponMaxValue(int value)
	{
		this.iCouponMaxValue = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iCouponMinValue value 类型为:int
	 * 
	 */
	public int getICouponMinValue()
	{
		return iCouponMinValue;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setICouponMinValue(int value)
	{
		this.iCouponMinValue = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwCouponStartTime value 类型为:long
	 * 
	 */
	public long getDwCouponStartTime()
	{
		return dwCouponStartTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwCouponStartTime(long value)
	{
		this.dwCouponStartTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwCouponEndTime value 类型为:long
	 * 
	 */
	public long getDwCouponEndTime()
	{
		return dwCouponEndTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwCouponEndTime(long value)
	{
		this.dwCouponEndTime = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iShareTimes value 类型为:int
	 * 
	 */
	public int getIShareTimes()
	{
		return iShareTimes;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIShareTimes(int value)
	{
		this.iShareTimes = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iCouponUseThreshold value 类型为:int
	 * 
	 */
	public int getICouponUseThreshold()
	{
		return iCouponUseThreshold;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setICouponUseThreshold(int value)
	{
		this.iCouponUseThreshold = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iRequestSource value 类型为:int
	 * 
	 */
	public int getIRequestSource()
	{
		return iRequestSource;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIRequestSource(int value)
	{
		this.iRequestSource = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUpdateTime value 类型为:long
	 * 
	 */
	public long getDwUpdateTime()
	{
		return dwUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUpdateTime(long value)
	{
		this.dwUpdateTime = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iExt1 value 类型为:int
	 * 
	 */
	public int getIExt1()
	{
		return iExt1;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIExt1(int value)
	{
		this.iExt1 = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iExt2 value 类型为:int
	 * 
	 */
	public int getIExt2()
	{
		return iExt2;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIExt2(int value)
	{
		this.iExt2 = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iExt3 value 类型为:int
	 * 
	 */
	public int getIExt3()
	{
		return iExt3;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIExt3(int value)
	{
		this.iExt3 = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iExt4 value 类型为:int
	 * 
	 */
	public int getIExt4()
	{
		return iExt4;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIExt4(int value)
	{
		this.iExt4 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sExt5 value 类型为:String
	 * 
	 */
	public String getSExt5()
	{
		return sExt5;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSExt5(String value)
	{
		this.sExt5 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sExt6 value 类型为:String
	 * 
	 */
	public String getSExt6()
	{
		return sExt6;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSExt6(String value)
	{
		this.sExt6 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sExt7 value 类型为:String
	 * 
	 */
	public String getSExt7()
	{
		return sExt7;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSExt7(String value)
	{
		this.sExt7 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sExt8 value 类型为:String
	 * 
	 */
	public String getSExt8()
	{
		return sExt8;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSExt8(String value)
	{
		this.sExt8 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(BonusPackage)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段ddwId的长度 size_of(uint64_t)
				length += 17;  //计算字段ddwRuleId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(sName, null);  //计算字段sName的长度 size_of(String)
				length += 4;  //计算字段iState的长度 size_of(int)
				length += 17;  //计算字段ddwOwnerUin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(sOpenId, null);  //计算字段sOpenId的长度 size_of(String)
				length += ByteStream.getObjectSize(sOwnerNickname, null);  //计算字段sOwnerNickname的长度 size_of(String)
				length += ByteStream.getObjectSize(sKey, null);  //计算字段sKey的长度 size_of(String)
				length += 4;  //计算字段dwStartTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iBusinessType的长度 size_of(int)
				length += ByteStream.getObjectSize(sBusinessKey0, null);  //计算字段sBusinessKey0的长度 size_of(String)
				length += ByteStream.getObjectSize(sBusinessKey1, null);  //计算字段sBusinessKey1的长度 size_of(String)
				length += ByteStream.getObjectSize(sBusinessKey2, null);  //计算字段sBusinessKey2的长度 size_of(String)
				length += 4;  //计算字段iTotalValue的长度 size_of(int)
				length += 4;  //计算字段iLeftValue的长度 size_of(int)
				length += 4;  //计算字段iTotalNumber的长度 size_of(int)
				length += 4;  //计算字段iLeftNumber的长度 size_of(int)
				length += 4;  //计算字段iCouponMaxValue的长度 size_of(int)
				length += 4;  //计算字段iCouponMinValue的长度 size_of(int)
				length += 4;  //计算字段dwCouponStartTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCouponEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iShareTimes的长度 size_of(int)
				length += 4;  //计算字段iCouponUseThreshold的长度 size_of(int)
				length += 4;  //计算字段iRequestSource的长度 size_of(int)
				length += 4;  //计算字段dwUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iExt1的长度 size_of(int)
				length += 4;  //计算字段iExt2的长度 size_of(int)
				length += 4;  //计算字段iExt3的长度 size_of(int)
				length += 4;  //计算字段iExt4的长度 size_of(int)
				length += ByteStream.getObjectSize(sExt5, null);  //计算字段sExt5的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt6, null);  //计算字段sExt6的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt7, null);  //计算字段sExt7的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt8, null);  //计算字段sExt8的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(BonusPackage)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段ddwId的长度 size_of(uint64_t)
				length += 17;  //计算字段ddwRuleId的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(sName, encoding);  //计算字段sName的长度 size_of(String)
				length += 4;  //计算字段iState的长度 size_of(int)
				length += 17;  //计算字段ddwOwnerUin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(sOpenId, encoding);  //计算字段sOpenId的长度 size_of(String)
				length += ByteStream.getObjectSize(sOwnerNickname, encoding);  //计算字段sOwnerNickname的长度 size_of(String)
				length += ByteStream.getObjectSize(sKey, encoding);  //计算字段sKey的长度 size_of(String)
				length += 4;  //计算字段dwStartTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iBusinessType的长度 size_of(int)
				length += ByteStream.getObjectSize(sBusinessKey0, encoding);  //计算字段sBusinessKey0的长度 size_of(String)
				length += ByteStream.getObjectSize(sBusinessKey1, encoding);  //计算字段sBusinessKey1的长度 size_of(String)
				length += ByteStream.getObjectSize(sBusinessKey2, encoding);  //计算字段sBusinessKey2的长度 size_of(String)
				length += 4;  //计算字段iTotalValue的长度 size_of(int)
				length += 4;  //计算字段iLeftValue的长度 size_of(int)
				length += 4;  //计算字段iTotalNumber的长度 size_of(int)
				length += 4;  //计算字段iLeftNumber的长度 size_of(int)
				length += 4;  //计算字段iCouponMaxValue的长度 size_of(int)
				length += 4;  //计算字段iCouponMinValue的长度 size_of(int)
				length += 4;  //计算字段dwCouponStartTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwCouponEndTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iShareTimes的长度 size_of(int)
				length += 4;  //计算字段iCouponUseThreshold的长度 size_of(int)
				length += 4;  //计算字段iRequestSource的长度 size_of(int)
				length += 4;  //计算字段dwUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iExt1的长度 size_of(int)
				length += 4;  //计算字段iExt2的长度 size_of(int)
				length += 4;  //计算字段iExt3的长度 size_of(int)
				length += 4;  //计算字段iExt4的长度 size_of(int)
				length += ByteStream.getObjectSize(sExt5, encoding);  //计算字段sExt5的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt6, encoding);  //计算字段sExt6的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt7, encoding);  //计算字段sExt7的长度 size_of(String)
				length += ByteStream.getObjectSize(sExt8, encoding);  //计算字段sExt8的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
