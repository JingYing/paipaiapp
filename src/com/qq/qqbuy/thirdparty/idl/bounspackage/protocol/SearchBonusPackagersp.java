//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bounspackage.idl.SearchBonusPackageResp.java

package com.qq.qqbuy.thirdparty.idl.bounspackage.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *
 *
 *@date 2015-03-10 04:52:47
 *
 *@since version:1
*/
public class SearchBonusPackagersp  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iReturnCode;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String sReturnDesc = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwTotalRows;

	/**
	 * 版本 >= 0
	 */
	 private Vector<BonusPackage> BonusPackageList = new Vector<BonusPackage>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushInt(iReturnCode);
		bs.pushString(sReturnDesc);
		bs.pushUInt(dwTotalRows);
		bs.pushObject(BonusPackageList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		iReturnCode = bs.popInt();
		sReturnDesc = bs.popString();
		dwTotalRows = bs.popUInt();
		BonusPackageList = (Vector<BonusPackage>)bs.popVector(BonusPackage.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iReturnCode value 类型为:int
	 * 
	 */
	public int getIReturnCode()
	{
		return iReturnCode;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIReturnCode(int value)
	{
		this.iReturnCode = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sReturnDesc value 类型为:String
	 * 
	 */
	public String getSReturnDesc()
	{
		return sReturnDesc;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSReturnDesc(String value)
	{
		this.sReturnDesc = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTotalRows value 类型为:long
	 * 
	 */
	public long getDwTotalRows()
	{
		return dwTotalRows;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwTotalRows(long value)
	{
		this.dwTotalRows = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BonusPackageList value 类型为:Vector<BonusPackage>
	 * 
	 */
	public Vector<BonusPackage> getBonusPackageList()
	{
		return BonusPackageList;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<BonusPackage>
	 * 
	 */
	public void setBonusPackageList(Vector<BonusPackage> value)
	{
		if (value != null) {
				this.BonusPackageList = value;
		}else{
				this.BonusPackageList = new Vector<BonusPackage>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SearchBonusPackagersp)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段iReturnCode的长度 size_of(int)
				length += ByteStream.getObjectSize(sReturnDesc, null);  //计算字段sReturnDesc的长度 size_of(String)
				length += 4;  //计算字段dwTotalRows的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BonusPackageList, null);  //计算字段BonusPackageList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SearchBonusPackagersp)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段iReturnCode的长度 size_of(int)
				length += ByteStream.getObjectSize(sReturnDesc, encoding);  //计算字段sReturnDesc的长度 size_of(String)
				length += 4;  //计算字段dwTotalRows的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BonusPackageList, encoding);  //计算字段BonusPackageList的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
