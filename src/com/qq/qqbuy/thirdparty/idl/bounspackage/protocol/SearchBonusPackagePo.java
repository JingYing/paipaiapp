//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: bounspackage.idl.SearchBonusPackageReq.java

package com.qq.qqbuy.thirdparty.idl.bounspackage.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *
 *
 *@date 2015-03-10 04:52:47
 *
 *@since version:1
*/
public class SearchBonusPackagePo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwTransactionId;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwUin;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iPageSize;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iPageIndex;

	/**
	 * C++中是int型
	 *
	 * 版本 >= 0
	 */
	 private int iState;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long ddwShopUin;

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long dwUpdateTime;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushLong(ddwTransactionId);
		bs.pushLong(ddwUin);
		bs.pushInt(iPageSize);
		bs.pushInt(iPageIndex);
		bs.pushInt(iState);
		bs.pushLong(ddwShopUin);
		bs.pushUInt(dwUpdateTime);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		ddwTransactionId = bs.popLong();
		ddwUin = bs.popLong();
		iPageSize = bs.popInt();
		iPageIndex = bs.popInt();
		iState = bs.popInt();
		ddwShopUin = bs.popLong();
		dwUpdateTime = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwTransactionId value 类型为:long
	 * 
	 */
	public long getDdwTransactionId()
	{
		return ddwTransactionId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwTransactionId(long value)
	{
		this.ddwTransactionId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwUin value 类型为:long
	 * 
	 */
	public long getDdwUin()
	{
		return ddwUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwUin(long value)
	{
		this.ddwUin = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iPageSize value 类型为:int
	 * 
	 */
	public int getIPageSize()
	{
		return iPageSize;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIPageSize(int value)
	{
		this.iPageSize = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iPageIndex value 类型为:int
	 * 
	 */
	public int getIPageIndex()
	{
		return iPageIndex;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIPageIndex(int value)
	{
		this.iPageIndex = value;
	}


	/**
	 * 获取C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @return iState value 类型为:int
	 * 
	 */
	public int getIState()
	{
		return iState;
	}


	/**
	 * 设置C++中是int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIState(int value)
	{
		this.iState = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwShopUin value 类型为:long
	 * 
	 */
	public long getDdwShopUin()
	{
		return ddwShopUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwShopUin(long value)
	{
		this.ddwShopUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwUpdateTime value 类型为:long
	 * 
	 */
	public long getDwUpdateTime()
	{
		return dwUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwUpdateTime(long value)
	{
		this.dwUpdateTime = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SearchBonusPackagePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段ddwTransactionId的长度 size_of(uint64_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 4;  //计算字段iPageSize的长度 size_of(int)
				length += 4;  //计算字段iPageIndex的长度 size_of(int)
				length += 4;  //计算字段iState的长度 size_of(int)
				length += 17;  //计算字段ddwShopUin的长度 size_of(uint64_t)
				length += 4;  //计算字段dwUpdateTime的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(SearchBonusPackagePo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 17;  //计算字段ddwTransactionId的长度 size_of(uint64_t)
				length += 17;  //计算字段ddwUin的长度 size_of(uint64_t)
				length += 4;  //计算字段iPageSize的长度 size_of(int)
				length += 4;  //计算字段iPageIndex的长度 size_of(int)
				length += 4;  //计算字段iState的长度 size_of(int)
				length += 17;  //计算字段ddwShopUin的长度 size_of(uint64_t)
				length += 4;  //计算字段dwUpdateTime的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
