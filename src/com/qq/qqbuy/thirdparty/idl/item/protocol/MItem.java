 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *手机拍拍商品
 *
 *@date 2011-06-08 04:38::32
 *
 *@since version:0
*/
public class MItem  implements ICanSerializeObject
{
    /**
     * 商品编码
     *
     * 版本 >= 0
     */
     private String itemCode = new String();

    /**
     * 是否包邮
     *
     * 版本 >= 0
     */
     private boolean isFree4Freight;

    /**
     * 多买立减金额
     *
     * 版本 >= 0
     */
     private int savingFee;

    /**
     * 在线支付立减金额
     *
     * 版本 >= 0
     */
     private int additionalFee;

    /**
     * 是否支持货到付款
     *
     * 版本 >= 0
     */
     private boolean supportCOD;

    /**
     * 卖家类型
     *
     * 版本 >= 0
     */
     private int sellerType;

    /**
     * 商品价格,成本分摊后的
     *
     * 版本 >= 0
     */
     private int itemPrice;

    /**
     * 商品价格,成本分摊后的
     *
     * 版本 >= 0
     */
     private int mailFee;

    /**
     * 商品价格,成本分摊后的
     *
     * 版本 >= 0
     */
     private int mailFeeAdd;

    /**
     * 商品1~6级彩钻价格
     *
     * 版本 >= 0
     */
     private Vector<Integer> discountPrice = new Vector<Integer>();

    /**
     * 库存信息
     *
     * 版本 >= 0
     */
     private Vector<MStock> stockList = new Vector<MStock>();



    public int serialize(ByteStream bs) throws Exception
    {
        bs.pushUInt(getClassSize());
        bs.pushString(itemCode);
        bs.pushBoolean(isFree4Freight);
        bs.pushInt(savingFee);
        bs.pushInt(additionalFee);
        bs.pushBoolean(supportCOD);
        bs.pushInt(sellerType);
        bs.pushInt(itemPrice);
        bs.pushInt(mailFee);
        bs.pushInt(mailFeeAdd);
        bs.pushObject(discountPrice);
        bs.pushObject(stockList);
        return bs.getWrittenLength();
    }
    
    public int unSerialize(ByteStream bs) throws Exception
    {
        long size = bs.popUInt();
        int startPosPop = bs.getReadLength();
        if (size == 0)
                return 0;
        itemCode = bs.popString();
        isFree4Freight = bs.popBoolean();
        savingFee = bs.popInt();
        additionalFee = bs.popInt();
        supportCOD = bs.popBoolean();
        sellerType = bs.popInt();
        itemPrice = bs.popInt();
        mailFee = bs.popInt();
        mailFeeAdd = bs.popInt();
        discountPrice = (Vector<Integer>)bs.popVector(Integer.class);
        stockList = (Vector<MStock>)bs.popVector(MStock.class);

        /**********************为了支持多个版本的客户端************************/
        int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
        for(int i = 0;i< needPopBytes; i++)
                bs.popByte();
        /**********************为了支持多个版本的客户端************************/

        return bs.getReadLength();
    } 


    /**
     * 获取商品编码
     * 
     * 此字段的版本 >= 0
     * @return itemCode value 类型为:String
     * 
     */
    public String getItemCode()
    {
        return itemCode;
    }


    /**
     * 设置商品编码
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:String
     * 
     */
    public void setItemCode(String value)
    {
        if (value != null) {
                this.itemCode = value;
        }else{
                this.itemCode = new String();
        }
    }


    /**
     * 获取是否包邮
     * 
     * 此字段的版本 >= 0
     * @return isFree4Freight value 类型为:boolean
     * 
     */
    public boolean getIsFree4Freight()
    {
        return isFree4Freight;
    }


    /**
     * 设置是否包邮
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:boolean
     * 
     */
    public void setIsFree4Freight(boolean value)
    {
        this.isFree4Freight = value;
    }


    /**
     * 获取多买立减金额
     * 
     * 此字段的版本 >= 0
     * @return savingFee value 类型为:int
     * 
     */
    public int getSavingFee()
    {
        return savingFee;
    }


    /**
     * 设置多买立减金额
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setSavingFee(int value)
    {
        this.savingFee = value;
    }


    /**
     * 获取在线支付立减金额
     * 
     * 此字段的版本 >= 0
     * @return additionalFee value 类型为:int
     * 
     */
    public int getAdditionalFee()
    {
        return additionalFee;
    }


    /**
     * 设置在线支付立减金额
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setAdditionalFee(int value)
    {
        this.additionalFee = value;
    }


    /**
     * 获取是否支持货到付款
     * 
     * 此字段的版本 >= 0
     * @return supportCOD value 类型为:boolean
     * 
     */
    public boolean getSupportCOD()
    {
        return supportCOD;
    }


    /**
     * 设置是否支持货到付款
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:boolean
     * 
     */
    public void setSupportCOD(boolean value)
    {
        this.supportCOD = value;
    }


    /**
     * 获取卖家类型
     * 
     * 此字段的版本 >= 0
     * @return sellerType value 类型为:int
     * 
     */
    public int getSellerType()
    {
        return sellerType;
    }


    /**
     * 设置卖家类型
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setSellerType(int value)
    {
        this.sellerType = value;
    }


    /**
     * 获取商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @return itemPrice value 类型为:int
     * 
     */
    public int getItemPrice()
    {
        return itemPrice;
    }


    /**
     * 设置商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setItemPrice(int value)
    {
        this.itemPrice = value;
    }


    /**
     * 获取商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @return mailFee value 类型为:int
     * 
     */
    public int getMailFee()
    {
        return mailFee;
    }


    /**
     * 设置商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setMailFee(int value)
    {
        this.mailFee = value;
    }


    /**
     * 获取商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @return mailFeeAdd value 类型为:int
     * 
     */
    public int getMailFeeAdd()
    {
        return mailFeeAdd;
    }


    /**
     * 设置商品价格,成本分摊后的
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:int
     * 
     */
    public void setMailFeeAdd(int value)
    {
        this.mailFeeAdd = value;
    }


    /**
     * 获取商品1~6级彩钻价格
     * 
     * 此字段的版本 >= 0
     * @return discountPrice value 类型为:Vector<Integer>
     * 
     */
    public Vector<Integer> getDiscountPrice()
    {
        return discountPrice;
    }


    /**
     * 设置商品1~6级彩钻价格
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:Vector<Integer>
     * 
     */
    public void setDiscountPrice(Vector<Integer> value)
    {
        if (value != null) {
                this.discountPrice = value;
        }else{
                this.discountPrice = new Vector<Integer>();
        }
    }


    /**
     * 获取库存信息
     * 
     * 此字段的版本 >= 0
     * @return stockList value 类型为:Vector<MStock>
     * 
     */
    public Vector<MStock> getStockList()
    {
        return stockList;
    }


    /**
     * 设置库存信息
     * 
     * 此字段的版本 >= 0
     * @param  value 类型为:Vector<MStock>
     * 
     */
    public void setStockList(Vector<MStock> value)
    {
        if (value != null) {
                this.stockList = value;
        }else{
                this.stockList = new Vector<MStock>();
        }
    }


    /**
     *   计算类长度
     *   用于告诉解包者，该类只放了这么长的数据
     *  
     */
    protected int getClassSize()
    {
        int length = getSize() - 4;
        try{

        }catch(Exception e){
                e.printStackTrace();
        }
        return length;
    }

    
    /**
     *   计算类长度
     *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
     *  
     */
    public int getSize()
    {
        int length = 4;
        try{
                length = 4;  //size_of(MItem)
                length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
                length += 1;  //计算字段isFree4Freight的长度 size_of(boolean)
                length += 4;  //计算字段savingFee的长度 size_of(int)
                length += 4;  //计算字段additionalFee的长度 size_of(int)
                length += 1;  //计算字段supportCOD的长度 size_of(boolean)
                length += 4;  //计算字段sellerType的长度 size_of(int)
                length += 4;  //计算字段itemPrice的长度 size_of(int)
                length += 4;  //计算字段mailFee的长度 size_of(int)
                length += 4;  //计算字段mailFeeAdd的长度 size_of(int)
                length += ByteStream.getObjectSize(discountPrice);  //计算字段discountPrice的长度 size_of(Vector)
                length += ByteStream.getObjectSize(stockList);  //计算字段stockList的长度 size_of(Vector)
        }catch(Exception e){
                e.printStackTrace();
        }
        return length;
    }


/**
 ********************以下信息是每个版本的字段********************
 */



    /**
     *下面是生成toString()方法
     此方法用于调试时打开*
     *如果要打开此方法，请加入commons-lang-2.4.jar
     *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
     *      import org.apache.commons.lang.builder.ToStringStyle;
     *
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
