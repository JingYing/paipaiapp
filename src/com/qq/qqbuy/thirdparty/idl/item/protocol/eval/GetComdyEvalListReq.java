 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *商品评价列表
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class  GetComdyEvalListReq implements IServiceObject
{
	/**
	 * 调用者
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * sceneId
	 *
	 * 版本 >= 0
	 */
	 private String SceneId = new String();

	/**
	 * 商品评价过滤器
	 *
	 * 版本 >= 0
	 */
	 private ComdyEvalFilter Filter = new ComdyEvalFilter();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(SceneId);
		bs.pushObject(Filter);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		SceneId = bs.popString();
		Filter = (ComdyEvalFilter) bs.popObject(ComdyEvalFilter.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23151806L;
	}


	/**
	 * 获取调用者
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取sceneId
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:String
	 * 
	 */
	public String getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置sceneId
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSceneId(String value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取商品评价过滤器
	 * 
	 * 此字段的版本 >= 0
	 * @return Filter value 类型为:ComdyEvalFilter
	 * 
	 */
	public ComdyEvalFilter getFilter()
	{
		return Filter;
	}


	/**
	 * 设置商品评价过滤器
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ComdyEvalFilter
	 * 
	 */
	public void setFilter(ComdyEvalFilter value)
	{
		if (value != null) {
				this.Filter = value;
		}else{
				this.Filter = new ComdyEvalFilter();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetComdyEvalListReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(SceneId);  //计算字段SceneId的长度 size_of(String)
				length += ByteStream.getObjectSize(Filter);  //计算字段Filter的长度 size_of(ComdyEvalFilter)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
