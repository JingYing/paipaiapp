//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *评价po
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class EvalRecordPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 子订单id-int型
	 *
	 * 版本 >= 0
	 */
	 private long FDealId;

	/**
	 * 子订单id-str型
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String CommodityId = new String();

	/**
	 * 商品logo
	 *
	 * 版本 >= 0
	 */
	 private String CommodityLogo = new String();

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String CommodityTitle = new String();

	/**
	 * 快照号
	 *
	 * 版本 >= 0
	 */
	 private String SnapId = new String();

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 卖家nick
	 *
	 * 版本 >= 0
	 */
	 private String SellerNickName = new String();

	/**
	 * 买家nick
	 *
	 * 版本 >= 0
	 */
	 private String BuyerNickName = new String();

	/**
	 * 买家做评时间
	 *
	 * 版本 >= 0
	 */
	 private long BuyerEvalTime;

	/**
	 * 卖家做评时间
	 *
	 * 版本 >= 0
	 */
	 private long SellerEvalTime;

	/**
	 * 买家评价内容
	 *
	 * 版本 >= 0
	 */
	 private String BuyerExplain = new String();

	/**
	 * 卖家评价内容
	 *
	 * 版本 >= 0
	 */
	 private String SellerExplain = new String();

	/**
	 * 买家评价原因
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> BuyerReason = new Vector<uint32_t>();

	/**
	 * 卖家评价原因
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> SellerReason = new Vector<uint32_t>();

	/**
	 * 买家得分
	 *
	 * 版本 >= 0
	 */
	 private int BuyerScore;

	/**
	 * 卖家得分
	 *
	 * 版本 >= 0
	 */
	 private int SellerScore;

	/**
	 * 实际交易总额，单位分
	 *
	 * 版本 >= 0
	 */
	 private long DealPayment;

	/**
	 * 系统计分标记
	 *
	 * 版本 >= 0
	 */
	 private long ScoreFlag;

	/**
	 * 系统计分时间
	 *
	 * 版本 >= 0
	 */
	 private long ScoreTime;

	/**
	 * 商品价格
	 *
	 * 版本 >= 0
	 */
	 private long Price;

	/**
	 * 评价创建时间
	 *
	 * 版本 >= 0
	 */
	 private long EvalCreateTime;

	/**
	 * 订单支付类型
	 *
	 * 版本 >= 0
	 */
	 private long paytype;

	/**
	 * 订单结束时间
	 *
	 * 版本 >= 0
	 */
	 private long DealFinishTime;

	/**
	 * 订单支付时间
	 *
	 * 版本 >= 0
	 */
	 private long DealPayTime;

	/**
	 * 买家评价级别1：差评，2：中评，3：好评
	 *
	 * 版本 >= 0
	 */
	 private short BuyerEvalLevel;

	/**
	 * 卖家评价级别1：差评，2：中评，3：好评
	 *
	 * 版本 >= 0
	 */
	 private short SellerEvalLevel;

	/**
	 * 买家记录是否被删除,0不删除1删除
	 *
	 * 版本 >= 0
	 */
	 private short BuyerEvalDelFlag;

	/**
	 * 卖家记录是否被删除，0不删除1删除
	 *
	 * 版本 >= 0
	 */
	 private short SellerEvalDelFlag;

	/**
	 * 对方是否为系统做评
	 *
	 * 版本 >= 0
	 */
	 private short PeerSysEval;

	/**
	 * 自己是否为系统做评
	 *
	 * 版本 >= 0
	 */
	 private short SelfSysEval;

	/**
	 * 评价回复
	 *
	 * 版本 >= 0
	 */
	 private Vector<CEvalReply> ReplyMsg = new Vector<CEvalReply>();

	/**
	 * 可以回复次数
	 *
	 * 版本 >= 0
	 */
	 private long NumCanReply;

	/**
	 * 子订单对应的大订单号
	 *
	 * 版本 >= 0
	 */
	 private String FtradeId = new String();

	/**
	 * 发货速度
	 *
	 * 版本 >= 0
	 */
	 private long Dsr1;

	/**
	 * 商品质量
	 *
	 * 版本 >= 0
	 */
	 private long Dsr2;

	/**
	 * 服务态度
	 *
	 * 版本 >= 0
	 */
	 private long Dsr3;

	/**
	 * dsr做评时间
	 *
	 * 版本 >= 0
	 */
	 private long DsrEvalTime;

	/**
	 * 0表示有效，1为无效
	 *
	 * 版本 >= 0
	 */
	 private long DsrFlag;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUInt(FDealId);
		bs.pushString(DealId);
		bs.pushString(CommodityId);
		bs.pushString(CommodityLogo);
		bs.pushString(CommodityTitle);
		bs.pushString(SnapId);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SellerUin);
		bs.pushString(SellerNickName);
		bs.pushString(BuyerNickName);
		bs.pushUInt(BuyerEvalTime);
		bs.pushUInt(SellerEvalTime);
		bs.pushString(BuyerExplain);
		bs.pushString(SellerExplain);
		bs.pushObject(BuyerReason);
		bs.pushObject(SellerReason);
		bs.pushInt(BuyerScore);
		bs.pushInt(SellerScore);
		bs.pushUInt(DealPayment);
		bs.pushUInt(ScoreFlag);
		bs.pushUInt(ScoreTime);
		bs.pushUInt(Price);
		bs.pushUInt(EvalCreateTime);
		bs.pushUInt(paytype);
		bs.pushUInt(DealFinishTime);
		bs.pushUInt(DealPayTime);
		bs.pushUByte(BuyerEvalLevel);
		bs.pushUByte(SellerEvalLevel);
		bs.pushUByte(BuyerEvalDelFlag);
		bs.pushUByte(SellerEvalDelFlag);
		bs.pushUByte(PeerSysEval);
		bs.pushUByte(SelfSysEval);
		bs.pushObject(ReplyMsg);
		bs.pushUInt(NumCanReply);
		bs.pushString(FtradeId);
		bs.pushUInt(Dsr1);
		bs.pushUInt(Dsr2);
		bs.pushUInt(Dsr3);
		bs.pushUInt(DsrEvalTime);
		bs.pushUInt(DsrFlag);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		FDealId = bs.popUInt();
		DealId = bs.popString();
		CommodityId = bs.popString();
		CommodityLogo = bs.popString();
		CommodityTitle = bs.popString();
		SnapId = bs.popString();
		BuyerUin = bs.popUInt();
		SellerUin = bs.popUInt();
		SellerNickName = bs.popString();
		BuyerNickName = bs.popString();
		BuyerEvalTime = bs.popUInt();
		SellerEvalTime = bs.popUInt();
		BuyerExplain = bs.popString();
		SellerExplain = bs.popString();
		BuyerReason = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		SellerReason = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		BuyerScore = bs.popInt();
		SellerScore = bs.popInt();
		DealPayment = bs.popUInt();
		ScoreFlag = bs.popUInt();
		ScoreTime = bs.popUInt();
		Price = bs.popUInt();
		EvalCreateTime = bs.popUInt();
		paytype = bs.popUInt();
		DealFinishTime = bs.popUInt();
		DealPayTime = bs.popUInt();
		BuyerEvalLevel = bs.popUByte();
		SellerEvalLevel = bs.popUByte();
		BuyerEvalDelFlag = bs.popUByte();
		SellerEvalDelFlag = bs.popUByte();
		PeerSysEval = bs.popUByte();
		SelfSysEval = bs.popUByte();
		ReplyMsg = (Vector<CEvalReply>)bs.popVector(CEvalReply.class);
		NumCanReply = bs.popUInt();
		FtradeId = bs.popString();
		Dsr1 = bs.popUInt();
		Dsr2 = bs.popUInt();
		Dsr3 = bs.popUInt();
		DsrEvalTime = bs.popUInt();
		DsrFlag = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
	}


	/**
	 * 获取子订单id-int型
	 * 
	 * 此字段的版本 >= 0
	 * @return FDealId value 类型为:long
	 * 
	 */
	public long getFDealId()
	{
		return FDealId;
	}


	/**
	 * 设置子订单id-int型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFDealId(long value)
	{
		this.FDealId = value;
	}


	/**
	 * 获取子订单id-str型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置子订单id-str型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityId value 类型为:String
	 * 
	 */
	public String getCommodityId()
	{
		return CommodityId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityId(String value)
	{
		this.CommodityId = value;
	}


	/**
	 * 获取商品logo
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityLogo value 类型为:String
	 * 
	 */
	public String getCommodityLogo()
	{
		return CommodityLogo;
	}


	/**
	 * 设置商品logo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityLogo(String value)
	{
		this.CommodityLogo = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityTitle value 类型为:String
	 * 
	 */
	public String getCommodityTitle()
	{
		return CommodityTitle;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityTitle(String value)
	{
		this.CommodityTitle = value;
	}


	/**
	 * 获取快照号
	 * 
	 * 此字段的版本 >= 0
	 * @return SnapId value 类型为:String
	 * 
	 */
	public String getSnapId()
	{
		return SnapId;
	}


	/**
	 * 设置快照号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSnapId(String value)
	{
		this.SnapId = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取卖家nick
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerNickName value 类型为:String
	 * 
	 */
	public String getSellerNickName()
	{
		return SellerNickName;
	}


	/**
	 * 设置卖家nick
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerNickName(String value)
	{
		this.SellerNickName = value;
	}


	/**
	 * 获取买家nick
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerNickName value 类型为:String
	 * 
	 */
	public String getBuyerNickName()
	{
		return BuyerNickName;
	}


	/**
	 * 设置买家nick
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerNickName(String value)
	{
		this.BuyerNickName = value;
	}


	/**
	 * 获取买家做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerEvalTime value 类型为:long
	 * 
	 */
	public long getBuyerEvalTime()
	{
		return BuyerEvalTime;
	}


	/**
	 * 设置买家做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerEvalTime(long value)
	{
		this.BuyerEvalTime = value;
	}


	/**
	 * 获取卖家做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerEvalTime value 类型为:long
	 * 
	 */
	public long getSellerEvalTime()
	{
		return SellerEvalTime;
	}


	/**
	 * 设置卖家做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerEvalTime(long value)
	{
		this.SellerEvalTime = value;
	}


	/**
	 * 获取买家评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerExplain value 类型为:String
	 * 
	 */
	public String getBuyerExplain()
	{
		return BuyerExplain;
	}


	/**
	 * 设置买家评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setBuyerExplain(String value)
	{
		this.BuyerExplain = value;
	}


	/**
	 * 获取卖家评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerExplain value 类型为:String
	 * 
	 */
	public String getSellerExplain()
	{
		return SellerExplain;
	}


	/**
	 * 设置卖家评价内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSellerExplain(String value)
	{
		this.SellerExplain = value;
	}


	/**
	 * 获取买家评价原因
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerReason value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getBuyerReason()
	{
		return BuyerReason;
	}


	/**
	 * 设置买家评价原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setBuyerReason(Vector<uint32_t> value)
	{
		if (value != null) {
				this.BuyerReason = value;
		}else{
				this.BuyerReason = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取卖家评价原因
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerReason value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getSellerReason()
	{
		return SellerReason;
	}


	/**
	 * 设置卖家评价原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setSellerReason(Vector<uint32_t> value)
	{
		if (value != null) {
				this.SellerReason = value;
		}else{
				this.SellerReason = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取买家得分
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerScore value 类型为:int
	 * 
	 */
	public int getBuyerScore()
	{
		return BuyerScore;
	}


	/**
	 * 设置买家得分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setBuyerScore(int value)
	{
		this.BuyerScore = value;
	}


	/**
	 * 获取卖家得分
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerScore value 类型为:int
	 * 
	 */
	public int getSellerScore()
	{
		return SellerScore;
	}


	/**
	 * 设置卖家得分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSellerScore(int value)
	{
		this.SellerScore = value;
	}


	/**
	 * 获取实际交易总额，单位分
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayment value 类型为:long
	 * 
	 */
	public long getDealPayment()
	{
		return DealPayment;
	}


	/**
	 * 设置实际交易总额，单位分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayment(long value)
	{
		this.DealPayment = value;
	}


	/**
	 * 获取系统计分标记
	 * 
	 * 此字段的版本 >= 0
	 * @return ScoreFlag value 类型为:long
	 * 
	 */
	public long getScoreFlag()
	{
		return ScoreFlag;
	}


	/**
	 * 设置系统计分标记
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScoreFlag(long value)
	{
		this.ScoreFlag = value;
	}


	/**
	 * 获取系统计分时间
	 * 
	 * 此字段的版本 >= 0
	 * @return ScoreTime value 类型为:long
	 * 
	 */
	public long getScoreTime()
	{
		return ScoreTime;
	}


	/**
	 * 设置系统计分时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScoreTime(long value)
	{
		this.ScoreTime = value;
	}


	/**
	 * 获取商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @return Price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return Price;
	}


	/**
	 * 设置商品价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.Price = value;
	}


	/**
	 * 获取评价创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EvalCreateTime value 类型为:long
	 * 
	 */
	public long getEvalCreateTime()
	{
		return EvalCreateTime;
	}


	/**
	 * 设置评价创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEvalCreateTime(long value)
	{
		this.EvalCreateTime = value;
	}


	/**
	 * 获取订单支付类型
	 * 
	 * 此字段的版本 >= 0
	 * @return paytype value 类型为:long
	 * 
	 */
	public long getPaytype()
	{
		return paytype;
	}


	/**
	 * 设置订单支付类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPaytype(long value)
	{
		this.paytype = value;
	}


	/**
	 * 获取订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealFinishTime value 类型为:long
	 * 
	 */
	public long getDealFinishTime()
	{
		return DealFinishTime;
	}


	/**
	 * 设置订单结束时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealFinishTime(long value)
	{
		this.DealFinishTime = value;
	}


	/**
	 * 获取订单支付时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayTime value 类型为:long
	 * 
	 */
	public long getDealPayTime()
	{
		return DealPayTime;
	}


	/**
	 * 设置订单支付时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealPayTime(long value)
	{
		this.DealPayTime = value;
	}


	/**
	 * 获取买家评价级别1：差评，2：中评，3：好评
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerEvalLevel value 类型为:short
	 * 
	 */
	public short getBuyerEvalLevel()
	{
		return BuyerEvalLevel;
	}


	/**
	 * 设置买家评价级别1：差评，2：中评，3：好评
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerEvalLevel(short value)
	{
		this.BuyerEvalLevel = value;
	}


	/**
	 * 获取卖家评价级别1：差评，2：中评，3：好评
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerEvalLevel value 类型为:short
	 * 
	 */
	public short getSellerEvalLevel()
	{
		return SellerEvalLevel;
	}


	/**
	 * 设置卖家评价级别1：差评，2：中评，3：好评
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerEvalLevel(short value)
	{
		this.SellerEvalLevel = value;
	}


	/**
	 * 获取买家记录是否被删除,0不删除1删除
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerEvalDelFlag value 类型为:short
	 * 
	 */
	public short getBuyerEvalDelFlag()
	{
		return BuyerEvalDelFlag;
	}


	/**
	 * 设置买家记录是否被删除,0不删除1删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBuyerEvalDelFlag(short value)
	{
		this.BuyerEvalDelFlag = value;
	}


	/**
	 * 获取卖家记录是否被删除，0不删除1删除
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerEvalDelFlag value 类型为:short
	 * 
	 */
	public short getSellerEvalDelFlag()
	{
		return SellerEvalDelFlag;
	}


	/**
	 * 设置卖家记录是否被删除，0不删除1删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSellerEvalDelFlag(short value)
	{
		this.SellerEvalDelFlag = value;
	}


	/**
	 * 获取对方是否为系统做评
	 * 
	 * 此字段的版本 >= 0
	 * @return PeerSysEval value 类型为:short
	 * 
	 */
	public short getPeerSysEval()
	{
		return PeerSysEval;
	}


	/**
	 * 设置对方是否为系统做评
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPeerSysEval(short value)
	{
		this.PeerSysEval = value;
	}


	/**
	 * 获取自己是否为系统做评
	 * 
	 * 此字段的版本 >= 0
	 * @return SelfSysEval value 类型为:short
	 * 
	 */
	public short getSelfSysEval()
	{
		return SelfSysEval;
	}


	/**
	 * 设置自己是否为系统做评
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSelfSysEval(short value)
	{
		this.SelfSysEval = value;
	}


	/**
	 * 获取评价回复
	 * 
	 * 此字段的版本 >= 0
	 * @return ReplyMsg value 类型为:Vector<CEvalReply>
	 * 
	 */
	public Vector<CEvalReply> getReplyMsg()
	{
		return ReplyMsg;
	}


	/**
	 * 设置评价回复
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CEvalReply>
	 * 
	 */
	public void setReplyMsg(Vector<CEvalReply> value)
	{
		if (value != null) {
				this.ReplyMsg = value;
		}else{
				this.ReplyMsg = new Vector<CEvalReply>();
		}
	}


	/**
	 * 获取可以回复次数
	 * 
	 * 此字段的版本 >= 0
	 * @return NumCanReply value 类型为:long
	 * 
	 */
	public long getNumCanReply()
	{
		return NumCanReply;
	}


	/**
	 * 设置可以回复次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNumCanReply(long value)
	{
		this.NumCanReply = value;
	}


	/**
	 * 获取子订单对应的大订单号
	 * 
	 * 此字段的版本 >= 0
	 * @return FtradeId value 类型为:String
	 * 
	 */
	public String getFtradeId()
	{
		return FtradeId;
	}


	/**
	 * 设置子订单对应的大订单号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setFtradeId(String value)
	{
		this.FtradeId = value;
	}


	/**
	 * 获取发货速度
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr1 value 类型为:long
	 * 
	 */
	public long getDsr1()
	{
		return Dsr1;
	}


	/**
	 * 设置发货速度
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr1(long value)
	{
		this.Dsr1 = value;
	}


	/**
	 * 获取商品质量
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr2 value 类型为:long
	 * 
	 */
	public long getDsr2()
	{
		return Dsr2;
	}


	/**
	 * 设置商品质量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr2(long value)
	{
		this.Dsr2 = value;
	}


	/**
	 * 获取服务态度
	 * 
	 * 此字段的版本 >= 0
	 * @return Dsr3 value 类型为:long
	 * 
	 */
	public long getDsr3()
	{
		return Dsr3;
	}


	/**
	 * 设置服务态度
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsr3(long value)
	{
		this.Dsr3 = value;
	}


	/**
	 * 获取dsr做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @return DsrEvalTime value 类型为:long
	 * 
	 */
	public long getDsrEvalTime()
	{
		return DsrEvalTime;
	}


	/**
	 * 设置dsr做评时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsrEvalTime(long value)
	{
		this.DsrEvalTime = value;
	}


	/**
	 * 获取0表示有效，1为无效
	 * 
	 * 此字段的版本 >= 0
	 * @return DsrFlag value 类型为:long
	 * 
	 */
	public long getDsrFlag()
	{
		return DsrFlag;
	}


	/**
	 * 设置0表示有效，1为无效
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDsrFlag(long value)
	{
		this.DsrFlag = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EvalRecordPo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 4;  //计算字段FDealId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(CommodityId);  //计算字段CommodityId的长度 size_of(String)
				length += ByteStream.getObjectSize(CommodityLogo);  //计算字段CommodityLogo的长度 size_of(String)
				length += ByteStream.getObjectSize(CommodityTitle);  //计算字段CommodityTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(SnapId);  //计算字段SnapId的长度 size_of(String)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(SellerNickName);  //计算字段SellerNickName的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerNickName);  //计算字段BuyerNickName的长度 size_of(String)
				length += 4;  //计算字段BuyerEvalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段SellerEvalTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(BuyerExplain);  //计算字段BuyerExplain的长度 size_of(String)
				length += ByteStream.getObjectSize(SellerExplain);  //计算字段SellerExplain的长度 size_of(String)
				length += ByteStream.getObjectSize(BuyerReason);  //计算字段BuyerReason的长度 size_of(Vector)
				length += ByteStream.getObjectSize(SellerReason);  //计算字段SellerReason的长度 size_of(Vector)
				length += 4;  //计算字段BuyerScore的长度 size_of(int)
				length += 4;  //计算字段SellerScore的长度 size_of(int)
				length += 4;  //计算字段DealPayment的长度 size_of(uint32_t)
				length += 4;  //计算字段ScoreFlag的长度 size_of(uint32_t)
				length += 4;  //计算字段ScoreTime的长度 size_of(uint32_t)
				length += 4;  //计算字段Price的长度 size_of(uint32_t)
				length += 4;  //计算字段EvalCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段paytype的长度 size_of(uint32_t)
				length += 4;  //计算字段DealFinishTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DealPayTime的长度 size_of(uint32_t)
				length += 1;  //计算字段BuyerEvalLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段SellerEvalLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段BuyerEvalDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段SellerEvalDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段PeerSysEval的长度 size_of(uint8_t)
				length += 1;  //计算字段SelfSysEval的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ReplyMsg);  //计算字段ReplyMsg的长度 size_of(Vector)
				length += 4;  //计算字段NumCanReply的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(FtradeId);  //计算字段FtradeId的长度 size_of(String)
				length += 4;  //计算字段Dsr1的长度 size_of(uint32_t)
				length += 4;  //计算字段Dsr2的长度 size_of(uint32_t)
				length += 4;  //计算字段Dsr3的长度 size_of(uint32_t)
				length += 4;  //计算字段DsrEvalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段DsrFlag的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
