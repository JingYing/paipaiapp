 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.ao.idl.MartAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *卖场快车商品详细信息接口响应
 *
 *@date 2014-09-04 05:10:45
 *
 *@since version:0
*/
public class  GetTabDetailResp extends NetMessage
{
	/**
	 * 详情数据
	 *
	 * 版本 >= 0
	 */
	 private Vector<TabData> detailList = new Vector<TabData>();

	/**
	 * 错误消息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(detailList);
		bs.pushString(errMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		detailList = (Vector<TabData>)bs.popVector(TabData.class);
		errMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87488002L;
	}


	/**
	 * 获取详情数据
	 * 
	 * 此字段的版本 >= 0
	 * @return detailList value 类型为:Vector<TabData>
	 * 
	 */
	public Vector<TabData> getDetailList()
	{
		return detailList;
	}


	/**
	 * 设置详情数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<TabData>
	 * 
	 */
	public void setDetailList(Vector<TabData> value)
	{
		if (value != null) {
				this.detailList = value;
		}else{
				this.detailList = new Vector<TabData>();
		}
	}


	/**
	 * 获取错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误消息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(getTabDetailResp)
				length += ByteStream.getObjectSize(detailList);  //计算字段detailList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
