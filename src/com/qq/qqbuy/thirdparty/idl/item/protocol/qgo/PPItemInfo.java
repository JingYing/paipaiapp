//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.item.idl.ItemAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.qgo;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *拍拍商品基本信息po
 *
 *@date 2013-04-10 02:32:23
 *
 *@since version:0
*/
public class PPItemInfo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 0;

	/**
	 * 商品ID，32位字符串
	 *
	 * 版本 >= 0
	 */
	 private String itemId = new String();

	/**
	 * 卖家Uin号码
	 *
	 * 版本 >= 0
	 */
	 private long qqUin;

	/**
	 * 商品名称
	 *
	 * 版本 >= 0
	 */
	 private String title = new String();

	/**
	 * 卖家昵称
	 *
	 * 版本 >= 0
	 */
	 private String qqNick = new String();

	/**
	 * 商品状态
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 所在国家
	 *
	 * 版本 >= 0
	 */
	 private long country;

	/**
	 * 所在省份
	 *
	 * 版本 >= 0
	 */
	 private long province;

	/**
	 * 所在城市
	 *
	 * 版本 >= 0
	 */
	 private long city;

	/**
	 * EMS价格
	 *
	 * 版本 >= 0
	 */
	 private long newType;

	/**
	 * 出售方式 【一口价/拍卖】
	 *
	 * 版本 >= 0
	 */
	 private long dealType;

	/**
	 * 是否支持财付通
	 *
	 * 版本 >= 0
	 */
	 private long supportPayAgency;

	/**
	 * 运费承担方式, 【卖家承担/买家承担】两种类型
	 *
	 * 版本 >= 0
	 */
	 private long transportPriceType;

	/**
	 * 运费模板的id，可以通过此id取运费模板的具体信息
	 *
	 * 版本 >= 0
	 */
	 private long shippingfeeId;

	/**
	 * 平邮价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 *
	 * 版本 >= 0
	 */
	 private long normalMailPrice;

	/**
	 * 快递价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 *
	 * 版本 >= 0
	 */
	 private long expressMailPrice;

	/**
	 * EMS价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 *
	 * 版本 >= 0
	 */
	 private long emsMailPrice;

	/**
	 * 商品重量 预留字段, 暂时无用
	 *
	 * 版本 >= 0
	 */
	 private long weight;

	/**
	 * 最新快照版本号，已售出商品，在编辑时，会产生新的版本号，此字段返回数据库中最大的版本号
	 *
	 * 版本 >= 0
	 */
	 private long snapVersion;

	/**
	 * 输入的商品的快照版本号，记录输入时的商品的版本号，如果没有版本号则此字段为0，快照号目前只支持8个bit
	 *
	 * 版本 >= 0
	 */
	 private long inputSnapVersion;

	/**
	 * 商品类目/品类 ID
	 *
	 * 版本 >= 0
	 */
	 private long leafClassId;

	/**
	 * EMS价格
	 *
	 * 版本 >= 0
	 */
	 private long productId;

	/**
	 * 类目属性串
	 *
	 * 版本 >= 0
	 */
	 private String attrText = new String();

	/**
	 * 店铺自定义分类，这里只返回自定义分类的数值Vector
	 *
	 * 版本 >= 0
	 */
	 private Vector<Long> shopClassId = new Vector<Long>();

	/**
	 * 商品限制购买数量,0表示不限制
	 *
	 * 版本 >= 0
	 */
	 private long buyLimit;

	/**
	 * 商品详情文件名，具体文件内容需要到tfs服务器拉取
	 *
	 * 版本 >= 0
	 */
	 private String descFilePos = new String();

	/**
	 * 商品使用的详情模板id，为0表示使用系统缺省模板，非零为用户自定义模板id
	 *
	 * 版本 >= 0
	 */
	 private long customStyleType;

	/**
	 * 商家编码 用于查询，此编码不是库存编码，库存编码在库存结构体中
	 *
	 * 版本 >= 0
	 */
	 private String itemLocalCode = new String();

	/**
	 * 重新上架次数，这个值不为0，则商品到期后会自动重新上架
	 *
	 * 版本 >= 0
	 */
	 private long reloadCnt;

	/**
	 * 推荐搭配商品ID的Vector，具体的关联商品的信息需要根据商品id拉取商品信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> relatedItems = new Vector<String>();

	/**
	 * 优惠套餐的套餐id的Vector，如果需要套餐的详细信息，调用套餐的接口拉取套餐的信息
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> comboIds = new Vector<String>();

	/**
	 * 商品价格，这里是所有商品库存里的最低价格，如果商品有库存，请以商品库存中的价格为准，商品价格默认情况下都是以分为单位的，比如值为100，则代表是1元
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 商品市场价，不是所有的商品都有市场价的，目前只有商城的商品有市场价
	 *
	 * 版本 >= 0
	 */
	 private long marketPrice;

	/**
	 * 最大可抵用红包面额,如果不支持红包，则此字段为0
	 *
	 * 版本 >= 0
	 */
	 private long redPrice;

	/**
	 * 货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 *
	 * 版本 >= 0
	 */
	 private long codShipTempId;

	/**
	 * 是否历史商品
	 *
	 * 版本 >= 0
	 */
	 private long isHistory;

	/**
	 * 商品总数，即目前还有的商品数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 付款的商品数
	 *
	 * 版本 >= 0
	 */
	 private long payNum;

	/**
	 * 全部付款商品数
	 *
	 * 版本 >= 0
	 */
	 private long totalPayNum;

	/**
	 * 付款操作次数（1次付款操作可以付款多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long payCount;

	/**
	 * 总的付款操作次数（1次付款操作可以付款多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long totalPayCount;

	/**
	 * 下单商品数
	 *
	 * 版本 >= 0
	 */
	 private long buyNum;

	/**
	 * 下单总商品数
	 *
	 * 版本 >= 0
	 */
	 private long toalBuyNum;

	/**
	 * 下单操作次数（1次下单可以下单多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long buyCount;

	/**
	 * 总的下单操作次数（1次下单可以下单多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long totalBuyCount;

	/**
	 * 商品浏览次数
	 *
	 * 版本 >= 0
	 */
	 private long visitCount;

	/**
	 * 当期销售量数量
	 *
	 * 版本 >= 0
	 */
	 private long currPayNum;

	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> property = new HashMap<Integer,Integer>();

	/**
	 * 新的扩展属性和值，请不要再用mapExt
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,String> MultiMapExt = new HashMap<Integer,String>();

	/**
	 * 图片
	 *
	 * 版本 >= 0
	 */
	 private Vector<String> vecImg = new Vector<String>();

	/**
	 * 库存
	 *
	 * 版本 >= 0
	 */
	 private Vector<ItemStock> stockList = new Vector<ItemStock>();

	/**
	 * 促销信息
	 *
	 * 版本 >= 0
	 */
	 private ItemPriceExtPo oItemPriceExt = new ItemPriceExtPo();

	/**
	 * 商品详情描述
	 *
	 * 版本 >= 0
	 */
	 private String itemDetail = new String();

	/**
	 * 备用字段
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs) - 4);
		bs.pushInt(version);
		bs.pushString(itemId);
		bs.pushUInt(qqUin);
		bs.pushString(title);
		bs.pushString(qqNick);
		bs.pushUInt(state);
		bs.pushUInt(country);
		bs.pushUInt(province);
		bs.pushUInt(city);
		bs.pushUInt(newType);
		bs.pushUInt(dealType);
		bs.pushUInt(supportPayAgency);
		bs.pushUInt(transportPriceType);
		bs.pushUInt(shippingfeeId);
		bs.pushUInt(normalMailPrice);
		bs.pushUInt(expressMailPrice);
		bs.pushUInt(emsMailPrice);
		bs.pushUInt(weight);
		bs.pushUInt(snapVersion);
		bs.pushUInt(inputSnapVersion);
		bs.pushUInt(leafClassId);
		bs.pushUInt(productId);
		bs.pushString(attrText);
		bs.pushObject(shopClassId);
		bs.pushUInt(buyLimit);
		bs.pushString(descFilePos);
		bs.pushUInt(customStyleType);
		bs.pushString(itemLocalCode);
		bs.pushUInt(reloadCnt);
		bs.pushObject(relatedItems);
		bs.pushObject(comboIds);
		bs.pushUInt(price);
		bs.pushUInt(marketPrice);
		bs.pushUInt(redPrice);
		bs.pushUInt(codShipTempId);
		bs.pushUInt(isHistory);
		bs.pushUInt(num);
		bs.pushUInt(payNum);
		bs.pushUInt(totalPayNum);
		bs.pushUInt(payCount);
		bs.pushUInt(totalPayCount);
		bs.pushUInt(buyNum);
		bs.pushUInt(toalBuyNum);
		bs.pushUInt(buyCount);
		bs.pushUInt(totalBuyCount);
		bs.pushUInt(visitCount);
		bs.pushUInt(currPayNum);
		bs.pushObject(property);
		bs.pushObject(MultiMapExt);
		bs.pushObject(vecImg);
		bs.pushObject(stockList);
		bs.pushObject(oItemPriceExt);
		bs.pushString(itemDetail);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		itemId = bs.popString();
		qqUin = bs.popUInt();
		title = bs.popString();
		qqNick = bs.popString();
		state = bs.popUInt();
		country = bs.popUInt();
		province = bs.popUInt();
		city = bs.popUInt();
		newType = bs.popUInt();
		dealType = bs.popUInt();
		supportPayAgency = bs.popUInt();
		transportPriceType = bs.popUInt();
		shippingfeeId = bs.popUInt();
		normalMailPrice = bs.popUInt();
		expressMailPrice = bs.popUInt();
		emsMailPrice = bs.popUInt();
		weight = bs.popUInt();
		snapVersion = bs.popUInt();
		inputSnapVersion = bs.popUInt();
		leafClassId = bs.popUInt();
		productId = bs.popUInt();
		attrText = bs.popString();
		shopClassId = (Vector<Long>)bs.popVector(Long.class);
		buyLimit = bs.popUInt();
		descFilePos = bs.popString();
		customStyleType = bs.popUInt();
		itemLocalCode = bs.popString();
		reloadCnt = bs.popUInt();
		relatedItems = (Vector<String>)bs.popVector(String.class);
		comboIds = (Vector<String>)bs.popVector(String.class);
		price = bs.popUInt();
		marketPrice = bs.popUInt();
		redPrice = bs.popUInt();
		codShipTempId = bs.popUInt();
		isHistory = bs.popUInt();
		num = bs.popUInt();
		payNum = bs.popUInt();
		totalPayNum = bs.popUInt();
		payCount = bs.popUInt();
		totalPayCount = bs.popUInt();
		buyNum = bs.popUInt();
		toalBuyNum = bs.popUInt();
		buyCount = bs.popUInt();
		totalBuyCount = bs.popUInt();
		visitCount = bs.popUInt();
		currPayNum = bs.popUInt();
		property = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		MultiMapExt = (Map<Integer,String>)bs.popMap(Integer.class,String.class);
		vecImg = (Vector<String>)bs.popVector(String.class);
		stockList = (Vector<ItemStock>)bs.popVector(ItemStock.class);
		oItemPriceExt = (ItemPriceExtPo) bs.popObject(ItemPriceExtPo.class);
		itemDetail = bs.popString();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品ID，32位字符串
	 * 
	 * 此字段的版本 >= 0
	 * @return itemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return itemId;
	}


	/**
	 * 设置商品ID，32位字符串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.itemId = value;
	}


	/**
	 * 获取卖家Uin号码
	 * 
	 * 此字段的版本 >= 0
	 * @return qqUin value 类型为:long
	 * 
	 */
	public long getQqUin()
	{
		return qqUin;
	}


	/**
	 * 设置卖家Uin号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQqUin(long value)
	{
		this.qqUin = value;
	}


	/**
	 * 获取商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @return title value 类型为:String
	 * 
	 */
	public String getTitle()
	{
		return title;
	}


	/**
	 * 设置商品名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTitle(String value)
	{
		this.title = value;
	}


	/**
	 * 获取卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return qqNick value 类型为:String
	 * 
	 */
	public String getQqNick()
	{
		return qqNick;
	}


	/**
	 * 设置卖家昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setQqNick(String value)
	{
		this.qqNick = value;
	}


	/**
	 * 获取商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
	}


	/**
	 * 获取所在国家
	 * 
	 * 此字段的版本 >= 0
	 * @return country value 类型为:long
	 * 
	 */
	public long getCountry()
	{
		return country;
	}


	/**
	 * 设置所在国家
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCountry(long value)
	{
		this.country = value;
	}


	/**
	 * 获取所在省份
	 * 
	 * 此字段的版本 >= 0
	 * @return province value 类型为:long
	 * 
	 */
	public long getProvince()
	{
		return province;
	}


	/**
	 * 设置所在省份
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProvince(long value)
	{
		this.province = value;
	}


	/**
	 * 获取所在城市
	 * 
	 * 此字段的版本 >= 0
	 * @return city value 类型为:long
	 * 
	 */
	public long getCity()
	{
		return city;
	}


	/**
	 * 设置所在城市
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCity(long value)
	{
		this.city = value;
	}


	/**
	 * 获取EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @return newType value 类型为:long
	 * 
	 */
	public long getNewType()
	{
		return newType;
	}


	/**
	 * 设置EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNewType(long value)
	{
		this.newType = value;
	}


	/**
	 * 获取出售方式 【一口价/拍卖】
	 * 
	 * 此字段的版本 >= 0
	 * @return dealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return dealType;
	}


	/**
	 * 设置出售方式 【一口价/拍卖】
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.dealType = value;
	}


	/**
	 * 获取是否支持财付通
	 * 
	 * 此字段的版本 >= 0
	 * @return supportPayAgency value 类型为:long
	 * 
	 */
	public long getSupportPayAgency()
	{
		return supportPayAgency;
	}


	/**
	 * 设置是否支持财付通
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSupportPayAgency(long value)
	{
		this.supportPayAgency = value;
	}


	/**
	 * 获取运费承担方式, 【卖家承担/买家承担】两种类型
	 * 
	 * 此字段的版本 >= 0
	 * @return transportPriceType value 类型为:long
	 * 
	 */
	public long getTransportPriceType()
	{
		return transportPriceType;
	}


	/**
	 * 设置运费承担方式, 【卖家承担/买家承担】两种类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTransportPriceType(long value)
	{
		this.transportPriceType = value;
	}


	/**
	 * 获取运费模板的id，可以通过此id取运费模板的具体信息
	 * 
	 * 此字段的版本 >= 0
	 * @return shippingfeeId value 类型为:long
	 * 
	 */
	public long getShippingfeeId()
	{
		return shippingfeeId;
	}


	/**
	 * 设置运费模板的id，可以通过此id取运费模板的具体信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setShippingfeeId(long value)
	{
		this.shippingfeeId = value;
	}


	/**
	 * 获取平邮价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @return normalMailPrice value 类型为:long
	 * 
	 */
	public long getNormalMailPrice()
	{
		return normalMailPrice;
	}


	/**
	 * 设置平邮价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalMailPrice(long value)
	{
		this.normalMailPrice = value;
	}


	/**
	 * 获取快递价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @return expressMailPrice value 类型为:long
	 * 
	 */
	public long getExpressMailPrice()
	{
		return expressMailPrice;
	}


	/**
	 * 设置快递价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setExpressMailPrice(long value)
	{
		this.expressMailPrice = value;
	}


	/**
	 * 获取EMS价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @return emsMailPrice value 类型为:long
	 * 
	 */
	public long getEmsMailPrice()
	{
		return emsMailPrice;
	}


	/**
	 * 设置EMS价格，如果没有运费模板，或者无法根据卖家的信息取到正确的运费模板信息则取这个值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmsMailPrice(long value)
	{
		this.emsMailPrice = value;
	}


	/**
	 * 获取商品重量 预留字段, 暂时无用
	 * 
	 * 此字段的版本 >= 0
	 * @return weight value 类型为:long
	 * 
	 */
	public long getWeight()
	{
		return weight;
	}


	/**
	 * 设置商品重量 预留字段, 暂时无用
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setWeight(long value)
	{
		this.weight = value;
	}


	/**
	 * 获取最新快照版本号，已售出商品，在编辑时，会产生新的版本号，此字段返回数据库中最大的版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return snapVersion value 类型为:long
	 * 
	 */
	public long getSnapVersion()
	{
		return snapVersion;
	}


	/**
	 * 设置最新快照版本号，已售出商品，在编辑时，会产生新的版本号，此字段返回数据库中最大的版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSnapVersion(long value)
	{
		this.snapVersion = value;
	}


	/**
	 * 获取输入的商品的快照版本号，记录输入时的商品的版本号，如果没有版本号则此字段为0，快照号目前只支持8个bit
	 * 
	 * 此字段的版本 >= 0
	 * @return inputSnapVersion value 类型为:long
	 * 
	 */
	public long getInputSnapVersion()
	{
		return inputSnapVersion;
	}


	/**
	 * 设置输入的商品的快照版本号，记录输入时的商品的版本号，如果没有版本号则此字段为0，快照号目前只支持8个bit
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInputSnapVersion(long value)
	{
		this.inputSnapVersion = value;
	}


	/**
	 * 获取商品类目/品类 ID
	 * 
	 * 此字段的版本 >= 0
	 * @return leafClassId value 类型为:long
	 * 
	 */
	public long getLeafClassId()
	{
		return leafClassId;
	}


	/**
	 * 设置商品类目/品类 ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLeafClassId(long value)
	{
		this.leafClassId = value;
	}


	/**
	 * 获取EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @return productId value 类型为:long
	 * 
	 */
	public long getProductId()
	{
		return productId;
	}


	/**
	 * 设置EMS价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProductId(long value)
	{
		this.productId = value;
	}


	/**
	 * 获取类目属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return attrText value 类型为:String
	 * 
	 */
	public String getAttrText()
	{
		return attrText;
	}


	/**
	 * 设置类目属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttrText(String value)
	{
		this.attrText = value;
	}


	/**
	 * 获取店铺自定义分类，这里只返回自定义分类的数值Vector
	 * 
	 * 此字段的版本 >= 0
	 * @return shopClassId value 类型为:Vector<Long>
	 * 
	 */
	public Vector<Long> getShopClassId()
	{
		return shopClassId;
	}


	/**
	 * 设置店铺自定义分类，这里只返回自定义分类的数值Vector
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Long>
	 * 
	 */
	public void setShopClassId(Vector<Long> value)
	{
		if (value != null) {
				this.shopClassId = value;
		}else{
				this.shopClassId = new Vector<Long>();
		}
	}


	/**
	 * 获取商品限制购买数量,0表示不限制
	 * 
	 * 此字段的版本 >= 0
	 * @return buyLimit value 类型为:long
	 * 
	 */
	public long getBuyLimit()
	{
		return buyLimit;
	}


	/**
	 * 设置商品限制购买数量,0表示不限制
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyLimit(long value)
	{
		this.buyLimit = value;
	}


	/**
	 * 获取商品详情文件名，具体文件内容需要到tfs服务器拉取
	 * 
	 * 此字段的版本 >= 0
	 * @return descFilePos value 类型为:String
	 * 
	 */
	public String getDescFilePos()
	{
		return descFilePos;
	}


	/**
	 * 设置商品详情文件名，具体文件内容需要到tfs服务器拉取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDescFilePos(String value)
	{
		this.descFilePos = value;
	}


	/**
	 * 获取商品使用的详情模板id，为0表示使用系统缺省模板，非零为用户自定义模板id
	 * 
	 * 此字段的版本 >= 0
	 * @return customStyleType value 类型为:long
	 * 
	 */
	public long getCustomStyleType()
	{
		return customStyleType;
	}


	/**
	 * 设置商品使用的详情模板id，为0表示使用系统缺省模板，非零为用户自定义模板id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCustomStyleType(long value)
	{
		this.customStyleType = value;
	}


	/**
	 * 获取商家编码 用于查询，此编码不是库存编码，库存编码在库存结构体中
	 * 
	 * 此字段的版本 >= 0
	 * @return itemLocalCode value 类型为:String
	 * 
	 */
	public String getItemLocalCode()
	{
		return itemLocalCode;
	}


	/**
	 * 设置商家编码 用于查询，此编码不是库存编码，库存编码在库存结构体中
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemLocalCode(String value)
	{
		this.itemLocalCode = value;
	}


	/**
	 * 获取重新上架次数，这个值不为0，则商品到期后会自动重新上架
	 * 
	 * 此字段的版本 >= 0
	 * @return reloadCnt value 类型为:long
	 * 
	 */
	public long getReloadCnt()
	{
		return reloadCnt;
	}


	/**
	 * 设置重新上架次数，这个值不为0，则商品到期后会自动重新上架
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReloadCnt(long value)
	{
		this.reloadCnt = value;
	}


	/**
	 * 获取推荐搭配商品ID的Vector，具体的关联商品的信息需要根据商品id拉取商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return relatedItems value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getRelatedItems()
	{
		return relatedItems;
	}


	/**
	 * 设置推荐搭配商品ID的Vector，具体的关联商品的信息需要根据商品id拉取商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setRelatedItems(Vector<String> value)
	{
		if (value != null) {
				this.relatedItems = value;
		}else{
				this.relatedItems = new Vector<String>();
		}
	}


	/**
	 * 获取优惠套餐的套餐id的Vector，如果需要套餐的详细信息，调用套餐的接口拉取套餐的信息
	 * 
	 * 此字段的版本 >= 0
	 * @return comboIds value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getComboIds()
	{
		return comboIds;
	}


	/**
	 * 设置优惠套餐的套餐id的Vector，如果需要套餐的详细信息，调用套餐的接口拉取套餐的信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setComboIds(Vector<String> value)
	{
		if (value != null) {
				this.comboIds = value;
		}else{
				this.comboIds = new Vector<String>();
		}
	}


	/**
	 * 获取商品价格，这里是所有商品库存里的最低价格，如果商品有库存，请以商品库存中的价格为准，商品价格默认情况下都是以分为单位的，比如值为100，则代表是1元
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置商品价格，这里是所有商品库存里的最低价格，如果商品有库存，请以商品库存中的价格为准，商品价格默认情况下都是以分为单位的，比如值为100，则代表是1元
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
	}


	/**
	 * 获取商品市场价，不是所有的商品都有市场价的，目前只有商城的商品有市场价
	 * 
	 * 此字段的版本 >= 0
	 * @return marketPrice value 类型为:long
	 * 
	 */
	public long getMarketPrice()
	{
		return marketPrice;
	}


	/**
	 * 设置商品市场价，不是所有的商品都有市场价的，目前只有商城的商品有市场价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMarketPrice(long value)
	{
		this.marketPrice = value;
	}


	/**
	 * 获取最大可抵用红包面额,如果不支持红包，则此字段为0
	 * 
	 * 此字段的版本 >= 0
	 * @return redPrice value 类型为:long
	 * 
	 */
	public long getRedPrice()
	{
		return redPrice;
	}


	/**
	 * 设置最大可抵用红包面额,如果不支持红包，则此字段为0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRedPrice(long value)
	{
		this.redPrice = value;
	}


	/**
	 * 获取货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 * 
	 * 此字段的版本 >= 0
	 * @return codShipTempId value 类型为:long
	 * 
	 */
	public long getCodShipTempId()
	{
		return codShipTempId;
	}


	/**
	 * 设置货到付款运费模板id,为0表示没有货到付款运费模板id，非0表示存在
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCodShipTempId(long value)
	{
		this.codShipTempId = value;
	}


	/**
	 * 获取是否历史商品
	 * 
	 * 此字段的版本 >= 0
	 * @return isHistory value 类型为:long
	 * 
	 */
	public long getIsHistory()
	{
		return isHistory;
	}


	/**
	 * 设置是否历史商品
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsHistory(long value)
	{
		this.isHistory = value;
	}


	/**
	 * 获取商品总数，即目前还有的商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置商品总数，即目前还有的商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
	}


	/**
	 * 获取付款的商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return payNum value 类型为:long
	 * 
	 */
	public long getPayNum()
	{
		return payNum;
	}


	/**
	 * 设置付款的商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayNum(long value)
	{
		this.payNum = value;
	}


	/**
	 * 获取全部付款商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayNum value 类型为:long
	 * 
	 */
	public long getTotalPayNum()
	{
		return totalPayNum;
	}


	/**
	 * 设置全部付款商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayNum(long value)
	{
		this.totalPayNum = value;
	}


	/**
	 * 获取付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return payCount value 类型为:long
	 * 
	 */
	public long getPayCount()
	{
		return payCount;
	}


	/**
	 * 设置付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayCount(long value)
	{
		this.payCount = value;
	}


	/**
	 * 获取总的付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayCount value 类型为:long
	 * 
	 */
	public long getTotalPayCount()
	{
		return totalPayCount;
	}


	/**
	 * 设置总的付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayCount(long value)
	{
		this.totalPayCount = value;
	}


	/**
	 * 获取下单商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return buyNum value 类型为:long
	 * 
	 */
	public long getBuyNum()
	{
		return buyNum;
	}


	/**
	 * 设置下单商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyNum(long value)
	{
		this.buyNum = value;
	}


	/**
	 * 获取下单总商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return toalBuyNum value 类型为:long
	 * 
	 */
	public long getToalBuyNum()
	{
		return toalBuyNum;
	}


	/**
	 * 设置下单总商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToalBuyNum(long value)
	{
		this.toalBuyNum = value;
	}


	/**
	 * 获取下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return buyCount value 类型为:long
	 * 
	 */
	public long getBuyCount()
	{
		return buyCount;
	}


	/**
	 * 设置下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyCount(long value)
	{
		this.buyCount = value;
	}


	/**
	 * 获取总的下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return totalBuyCount value 类型为:long
	 * 
	 */
	public long getTotalBuyCount()
	{
		return totalBuyCount;
	}


	/**
	 * 设置总的下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalBuyCount(long value)
	{
		this.totalBuyCount = value;
	}


	/**
	 * 获取商品浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @return visitCount value 类型为:long
	 * 
	 */
	public long getVisitCount()
	{
		return visitCount;
	}


	/**
	 * 设置商品浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVisitCount(long value)
	{
		this.visitCount = value;
	}


	/**
	 * 获取当期销售量数量
	 * 
	 * 此字段的版本 >= 0
	 * @return currPayNum value 类型为:long
	 * 
	 */
	public long getCurrPayNum()
	{
		return currPayNum;
	}


	/**
	 * 设置当期销售量数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCurrPayNum(long value)
	{
		this.currPayNum = value;
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getProperty()
	{
		return property;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setProperty(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.property = value;
		}else{
				this.property = new HashMap<Integer,Integer>();
		}
	}


	/**
	 * 获取新的扩展属性和值，请不要再用mapExt
	 * 
	 * 此字段的版本 >= 0
	 * @return MultiMapExt value 类型为:Map<Integer,String>
	 * 
	 */
	public Map<Integer,String> getMultiMapExt()
	{
		return MultiMapExt;
	}


	/**
	 * 设置新的扩展属性和值，请不要再用mapExt
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,String>
	 * 
	 */
	public void setMultiMapExt(Map<Integer,String> value)
	{
		if (value != null) {
				this.MultiMapExt = value;
		}else{
				this.MultiMapExt = new HashMap<Integer,String>();
		}
	}


	/**
	 * 获取图片
	 * 
	 * 此字段的版本 >= 0
	 * @return vecImg value 类型为:Vector<String>
	 * 
	 */
	public Vector<String> getVecImg()
	{
		return vecImg;
	}


	/**
	 * 设置图片
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<String>
	 * 
	 */
	public void setVecImg(Vector<String> value)
	{
		if (value != null) {
				this.vecImg = value;
		}else{
				this.vecImg = new Vector<String>();
		}
	}


	/**
	 * 获取库存
	 * 
	 * 此字段的版本 >= 0
	 * @return stockList value 类型为:Vector<ItemStock>
	 * 
	 */
	public Vector<ItemStock> getStockList()
	{
		return stockList;
	}


	/**
	 * 设置库存
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ItemStock>
	 * 
	 */
	public void setStockList(Vector<ItemStock> value)
	{
		if (value != null) {
				this.stockList = value;
		}else{
				this.stockList = new Vector<ItemStock>();
		}
	}


	/**
	 * 获取促销信息
	 * 
	 * 此字段的版本 >= 0
	 * @return oItemPriceExt value 类型为:ItemPriceExtPo
	 * 
	 */
	public ItemPriceExtPo getOItemPriceExt()
	{
		return oItemPriceExt;
	}


	/**
	 * 设置促销信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemPriceExtPo
	 * 
	 */
	public void setOItemPriceExt(ItemPriceExtPo value)
	{
		if (value != null) {
				this.oItemPriceExt = value;
		}else{
				this.oItemPriceExt = new ItemPriceExtPo();
		}
	}


	/**
	 * 获取商品详情描述
	 * 
	 * 此字段的版本 >= 0
	 * @return itemDetail value 类型为:String
	 * 
	 */
	public String getItemDetail()
	{
		return itemDetail;
	}


	/**
	 * 设置商品详情描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemDetail(String value)
	{
		this.itemDetail = value;
	}


	/**
	 * 获取备用字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PPItemInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(itemId);  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段qqUin的长度 size_of(long)
				length += ByteStream.getObjectSize(title);  //计算字段title的长度 size_of(String)
				length += ByteStream.getObjectSize(qqNick);  //计算字段qqNick的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(long)
				length += 4;  //计算字段country的长度 size_of(long)
				length += 4;  //计算字段province的长度 size_of(long)
				length += 4;  //计算字段city的长度 size_of(long)
				length += 4;  //计算字段newType的长度 size_of(long)
				length += 4;  //计算字段dealType的长度 size_of(long)
				length += 4;  //计算字段supportPayAgency的长度 size_of(long)
				length += 4;  //计算字段transportPriceType的长度 size_of(long)
				length += 4;  //计算字段shippingfeeId的长度 size_of(long)
				length += 4;  //计算字段normalMailPrice的长度 size_of(long)
				length += 4;  //计算字段expressMailPrice的长度 size_of(long)
				length += 4;  //计算字段emsMailPrice的长度 size_of(long)
				length += 4;  //计算字段weight的长度 size_of(long)
				length += 4;  //计算字段snapVersion的长度 size_of(long)
				length += 4;  //计算字段inputSnapVersion的长度 size_of(long)
				length += 4;  //计算字段leafClassId的长度 size_of(long)
				length += 4;  //计算字段productId的长度 size_of(long)
				length += ByteStream.getObjectSize(attrText);  //计算字段attrText的长度 size_of(String)
				length += ByteStream.getObjectSize(shopClassId);  //计算字段shopClassId的长度 size_of(Vector)
				length += 4;  //计算字段buyLimit的长度 size_of(long)
				length += ByteStream.getObjectSize(descFilePos);  //计算字段descFilePos的长度 size_of(String)
				length += 4;  //计算字段customStyleType的长度 size_of(long)
				length += ByteStream.getObjectSize(itemLocalCode);  //计算字段itemLocalCode的长度 size_of(String)
				length += 4;  //计算字段reloadCnt的长度 size_of(long)
				length += ByteStream.getObjectSize(relatedItems);  //计算字段relatedItems的长度 size_of(Vector)
				length += ByteStream.getObjectSize(comboIds);  //计算字段comboIds的长度 size_of(Vector)
				length += 4;  //计算字段price的长度 size_of(long)
				length += 4;  //计算字段marketPrice的长度 size_of(long)
				length += 4;  //计算字段redPrice的长度 size_of(long)
				length += 4;  //计算字段codShipTempId的长度 size_of(long)
				length += 4;  //计算字段isHistory的长度 size_of(long)
				length += 4;  //计算字段num的长度 size_of(long)
				length += 4;  //计算字段payNum的长度 size_of(long)
				length += 4;  //计算字段totalPayNum的长度 size_of(long)
				length += 4;  //计算字段payCount的长度 size_of(long)
				length += 4;  //计算字段totalPayCount的长度 size_of(long)
				length += 4;  //计算字段buyNum的长度 size_of(long)
				length += 4;  //计算字段toalBuyNum的长度 size_of(long)
				length += 4;  //计算字段buyCount的长度 size_of(long)
				length += 4;  //计算字段totalBuyCount的长度 size_of(long)
				length += 4;  //计算字段visitCount的长度 size_of(long)
				length += 4;  //计算字段currPayNum的长度 size_of(long)
				length += ByteStream.getObjectSize(property);  //计算字段property的长度 size_of(Map)
				length += ByteStream.getObjectSize(MultiMapExt);  //计算字段MultiMapExt的长度 size_of(Map)
				length += ByteStream.getObjectSize(vecImg);  //计算字段vecImg的长度 size_of(Vector)
				length += ByteStream.getObjectSize(stockList);  //计算字段stockList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oItemPriceExt);  //计算字段oItemPriceExt的长度 size_of(ItemPriceExtPo)
				length += ByteStream.getObjectSize(itemDetail);  //计算字段itemDetail的长度 size_of(String)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(ByteStream bs)
	{
		int length = 4;
		try{
				length = 4;  //size_of(PPItemInfo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4 + itemId.getBytes(bs.getDecodeCharset()).length;  //计算字段itemId的长度 size_of(String)
				length += 4;  //计算字段qqUin的长度 size_of(long)
				length += 4 + title.getBytes(bs.getDecodeCharset()).length;  //计算字段title的长度 size_of(String)
				length += 4 + qqNick.getBytes(bs.getDecodeCharset()).length;  //计算字段qqNick的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(long)
				length += 4;  //计算字段country的长度 size_of(long)
				length += 4;  //计算字段province的长度 size_of(long)
				length += 4;  //计算字段city的长度 size_of(long)
				length += 4;  //计算字段newType的长度 size_of(long)
				length += 4;  //计算字段dealType的长度 size_of(long)
				length += 4;  //计算字段supportPayAgency的长度 size_of(long)
				length += 4;  //计算字段transportPriceType的长度 size_of(long)
				length += 4;  //计算字段shippingfeeId的长度 size_of(long)
				length += 4;  //计算字段normalMailPrice的长度 size_of(long)
				length += 4;  //计算字段expressMailPrice的长度 size_of(long)
				length += 4;  //计算字段emsMailPrice的长度 size_of(long)
				length += 4;  //计算字段weight的长度 size_of(long)
				length += 4;  //计算字段snapVersion的长度 size_of(long)
				length += 4;  //计算字段inputSnapVersion的长度 size_of(long)
				length += 4;  //计算字段leafClassId的长度 size_of(long)
				length += 4;  //计算字段productId的长度 size_of(long)
				length += 4 + attrText.getBytes(bs.getDecodeCharset()).length;  //计算字段attrText的长度 size_of(String)
				length += ByteStream.getObjectSize(shopClassId);  //计算字段shopClassId的长度 size_of(Vector)
				length += 4;  //计算字段buyLimit的长度 size_of(long)
				length += 4 + descFilePos.getBytes(bs.getDecodeCharset()).length;  //计算字段descFilePos的长度 size_of(String)
				length += 4;  //计算字段customStyleType的长度 size_of(long)
				length += 4 + itemLocalCode.getBytes(bs.getDecodeCharset()).length;  //计算字段itemLocalCode的长度 size_of(String)
				length += 4;  //计算字段reloadCnt的长度 size_of(long)
				length += ByteStream.getObjectSize(relatedItems);  //计算字段relatedItems的长度 size_of(Vector)
				length += ByteStream.getObjectSize(comboIds);  //计算字段comboIds的长度 size_of(Vector)
				length += 4;  //计算字段price的长度 size_of(long)
				length += 4;  //计算字段marketPrice的长度 size_of(long)
				length += 4;  //计算字段redPrice的长度 size_of(long)
				length += 4;  //计算字段codShipTempId的长度 size_of(long)
				length += 4;  //计算字段isHistory的长度 size_of(long)
				length += 4;  //计算字段num的长度 size_of(long)
				length += 4;  //计算字段payNum的长度 size_of(long)
				length += 4;  //计算字段totalPayNum的长度 size_of(long)
				length += 4;  //计算字段payCount的长度 size_of(long)
				length += 4;  //计算字段totalPayCount的长度 size_of(long)
				length += 4;  //计算字段buyNum的长度 size_of(long)
				length += 4;  //计算字段toalBuyNum的长度 size_of(long)
				length += 4;  //计算字段buyCount的长度 size_of(long)
				length += 4;  //计算字段totalBuyCount的长度 size_of(long)
				length += 4;  //计算字段visitCount的长度 size_of(long)
				length += 4;  //计算字段currPayNum的长度 size_of(long)
				length += ByteStream.getObjectSize(property);  //计算字段property的长度 size_of(Map)
				length += ByteStream.getObjectSize(MultiMapExt);  //计算字段MultiMapExt的长度 size_of(Map)
				length += ByteStream.getObjectSize(vecImg);  //计算字段vecImg的长度 size_of(Vector)
				length += ByteStream.getObjectSize(stockList);  //计算字段stockList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oItemPriceExt);  //计算字段oItemPriceExt的长度 size_of(ItemPriceExtPo)
				length += 4 + itemDetail.getBytes(bs.getDecodeCharset()).length;  //计算字段itemDetail的长度 size_of(String)
				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
