//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.portal.po.idl.MartPo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *商品数据
 *
 *@date 2014-09-04 05:20:14
 *
 *@since version:0
*/
public class TabData  implements ICanSerializeObject
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140807;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 卖快商品表主键
	 *
	 * 版本 >= 0
	 */
	 private long dwTabId;

	/**
	 * 版本 >= 0
	 */
	 private short dwTabId_u;

	/**
	 * 商品池ID 与BI侧关联
	 *
	 * 版本 >= 0
	 */
	 private long dwPoolId;

	/**
	 * 版本 >= 0
	 */
	 private short dwPoolId_u;

	/**
	 * 区域排序号
	 *
	 * 版本 >= 0
	 */
	 private long dwAreaOrderNum;

	/**
	 * 版本 >= 0
	 */
	 private short dwAreaOrderNum_u;

	/**
	 * beginTime
	 *
	 * 版本 >= 0
	 */
	 private long dwBeginTime;

	/**
	 * 版本 >= 0
	 */
	 private short dwBeginTime_u;

	/**
	 * endTime
	 *
	 * 版本 >= 0
	 */
	 private long dwEndTime;

	/**
	 * 版本 >= 0
	 */
	 private short dwEndTime_u;

	/**
	 * 商品字符串ID
	 *
	 * 版本 >= 0
	 */
	 private String strCommodityId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strCommodityId_u;

	/**
	 * 商品标题
	 *
	 * 版本 >= 0
	 */
	 private String strTitle = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strTitle_u;

	/**
	 * 商品库存量
	 *
	 * 版本 >= 0
	 */
	 private long dwStock;

	/**
	 * 版本 >= 0
	 */
	 private short dwStock_u;

	/**
	 * 报名时提交的参加活动的库存量
	 *
	 * 版本 >= 0
	 */
	 private long dwSignInStock;

	/**
	 * 版本 >= 0
	 */
	 private short dwSignInStock_u;

	/**
	 * 商品状态
	 *
	 * 版本 >= 0
	 */
	 private long dwState;

	/**
	 * 版本 >= 0
	 */
	 private short dwState_u;

	/**
	 * 运费
	 *
	 * 版本 >= 0
	 */
	 private long dwPostAge;

	/**
	 * 版本 >= 0
	 */
	 private short dwPostAge_u;

	/**
	 * 商品主图
	 *
	 * 版本 >= 0
	 */
	 private String strPic = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strPic_u;

	/**
	 * 商品售价
	 *
	 * 版本 >= 0
	 */
	 private long dwPrice;

	/**
	 * 版本 >= 0
	 */
	 private short dwPrice_u;

	/**
	 * 商品市场价
	 *
	 * 版本 >= 0
	 */
	 private long dwMarketPrice;

	/**
	 * 版本 >= 0
	 */
	 private short dwMarketPrice_u;

	/**
	 * 参加活动报名时提交的价格
	 *
	 * 版本 >= 0
	 */
	 private long dwSignInPrice;

	/**
	 * 版本 >= 0
	 */
	 private short dwSignInPrice_u;

	/**
	 * 当前生效的最低可售价
	 *
	 * 版本 >= 0
	 */
	 private long dwActivePrice;

	/**
	 * 版本 >= 0
	 */
	 private short dwActivePrice_u;

	/**
	 * 商品排序号
	 *
	 * 版本 >= 0
	 */
	 private long dwOrderNum;

	/**
	 * 版本 >= 0
	 */
	 private short dwOrderNum_u;

	/**
	 * 店铺名称
	 *
	 * 版本 >= 0
	 */
	 private String strShopName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strShopName_u;

	/**
	 * 店铺UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwSellerUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwSellerUin_u;

	/**
	 * 卖家分
	 *
	 * 版本 >= 0
	 */
	 private long dwSellerCredit;

	/**
	 * 版本 >= 0
	 */
	 private short dwSellerCredit_u;

	/**
	 * ptag
	 *
	 * 版本 >= 0
	 */
	 private String strPtag = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strPtag_u;

	/**
	 * 商品详情原始URL
	 *
	 * 版本 >= 0
	 */
	 private String strDetailUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strDetailUrl_u;

	/**
	 * 带详细参数的商品详情URL
	 *
	 * 版本 >= 0
	 */
	 private String strClickUrl = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strClickUrl_u;

	/**
	 * 一级类目ID
	 *
	 * 版本 >= 0
	 */
	 private long dwClassIdL1;

	/**
	 * 版本 >= 0
	 */
	 private short dwClassIdL1_u;

	/**
	 * 二级类目ID
	 *
	 * 版本 >= 0
	 */
	 private long dwClassIdL2;

	/**
	 * 版本 >= 0
	 */
	 private short dwClassIdL2_u;

	/**
	 * 三级类目ID
	 *
	 * 版本 >= 0
	 */
	 private long dwClassIdL3;

	/**
	 * 版本 >= 0
	 */
	 private short dwClassIdL3_u;

	/**
	 * 四级类目ID
	 *
	 * 版本 >= 0
	 */
	 private long dwClassIdL4;

	/**
	 * 版本 >= 0
	 */
	 private short dwClassIdL4_u;

	/**
	 * 评论内容
	 *
	 * 版本 >= 0
	 */
	 private String strEvalText = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strEvalText_u;

	/**
	 * 评论发布者UIN
	 *
	 * 版本 >= 0
	 */
	 private long ddwEvalUin;

	/**
	 * 版本 >= 0
	 */
	 private short ddwEvalUin_u;

	/**
	 * 评论发布者昵称
	 *
	 * 版本 >= 0
	 */
	 private String strEvalName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strEvalName_u;

	/**
	 * 卖家自定义信息 如 附加图文等
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> mapCustomData = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short mapCustomData_u;

	/**
	 * 其他附加信息
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> mapExt = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short mapExt_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(dwTabId);
		bs.pushUByte(dwTabId_u);
		bs.pushUInt(dwPoolId);
		bs.pushUByte(dwPoolId_u);
		bs.pushUInt(dwAreaOrderNum);
		bs.pushUByte(dwAreaOrderNum_u);
		bs.pushUInt(dwBeginTime);
		bs.pushUByte(dwBeginTime_u);
		bs.pushUInt(dwEndTime);
		bs.pushUByte(dwEndTime_u);
		bs.pushString(strCommodityId);
		bs.pushUByte(strCommodityId_u);
		bs.pushString(strTitle);
		bs.pushUByte(strTitle_u);
		bs.pushUInt(dwStock);
		bs.pushUByte(dwStock_u);
		bs.pushUInt(dwSignInStock);
		bs.pushUByte(dwSignInStock_u);
		bs.pushUInt(dwState);
		bs.pushUByte(dwState_u);
		bs.pushUInt(dwPostAge);
		bs.pushUByte(dwPostAge_u);
		bs.pushString(strPic);
		bs.pushUByte(strPic_u);
		bs.pushUInt(dwPrice);
		bs.pushUByte(dwPrice_u);
		bs.pushUInt(dwMarketPrice);
		bs.pushUByte(dwMarketPrice_u);
		bs.pushUInt(dwSignInPrice);
		bs.pushUByte(dwSignInPrice_u);
		bs.pushUInt(dwActivePrice);
		bs.pushUByte(dwActivePrice_u);
		bs.pushUInt(dwOrderNum);
		bs.pushUByte(dwOrderNum_u);
		bs.pushString(strShopName);
		bs.pushUByte(strShopName_u);
		bs.pushLong(ddwSellerUin);
		bs.pushUByte(ddwSellerUin_u);
		bs.pushUInt(dwSellerCredit);
		bs.pushUByte(dwSellerCredit_u);
		bs.pushString(strPtag);
		bs.pushUByte(strPtag_u);
		bs.pushString(strDetailUrl);
		bs.pushUByte(strDetailUrl_u);
		bs.pushString(strClickUrl);
		bs.pushUByte(strClickUrl_u);
		bs.pushUInt(dwClassIdL1);
		bs.pushUByte(dwClassIdL1_u);
		bs.pushUInt(dwClassIdL2);
		bs.pushUByte(dwClassIdL2_u);
		bs.pushUInt(dwClassIdL3);
		bs.pushUByte(dwClassIdL3_u);
		bs.pushUInt(dwClassIdL4);
		bs.pushUByte(dwClassIdL4_u);
		bs.pushString(strEvalText);
		bs.pushUByte(strEvalText_u);
		bs.pushLong(ddwEvalUin);
		bs.pushUByte(ddwEvalUin_u);
		bs.pushString(strEvalName);
		bs.pushUByte(strEvalName_u);
		bs.pushObject(mapCustomData);
		bs.pushUByte(mapCustomData_u);
		bs.pushObject(mapExt);
		bs.pushUByte(mapExt_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		dwTabId = bs.popUInt();
		dwTabId_u = bs.popUByte();
		dwPoolId = bs.popUInt();
		dwPoolId_u = bs.popUByte();
		dwAreaOrderNum = bs.popUInt();
		dwAreaOrderNum_u = bs.popUByte();
		dwBeginTime = bs.popUInt();
		dwBeginTime_u = bs.popUByte();
		dwEndTime = bs.popUInt();
		dwEndTime_u = bs.popUByte();
		strCommodityId = bs.popString();
		strCommodityId_u = bs.popUByte();
		strTitle = bs.popString();
		strTitle_u = bs.popUByte();
		dwStock = bs.popUInt();
		dwStock_u = bs.popUByte();
		dwSignInStock = bs.popUInt();
		dwSignInStock_u = bs.popUByte();
		dwState = bs.popUInt();
		dwState_u = bs.popUByte();
		dwPostAge = bs.popUInt();
		dwPostAge_u = bs.popUByte();
		strPic = bs.popString();
		strPic_u = bs.popUByte();
		dwPrice = bs.popUInt();
		dwPrice_u = bs.popUByte();
		dwMarketPrice = bs.popUInt();
		dwMarketPrice_u = bs.popUByte();
		dwSignInPrice = bs.popUInt();
		dwSignInPrice_u = bs.popUByte();
		dwActivePrice = bs.popUInt();
		dwActivePrice_u = bs.popUByte();
		dwOrderNum = bs.popUInt();
		dwOrderNum_u = bs.popUByte();
		strShopName = bs.popString();
		strShopName_u = bs.popUByte();
		ddwSellerUin = bs.popLong();
		ddwSellerUin_u = bs.popUByte();
		dwSellerCredit = bs.popUInt();
		dwSellerCredit_u = bs.popUByte();
		strPtag = bs.popString();
		strPtag_u = bs.popUByte();
		strDetailUrl = bs.popString();
		strDetailUrl_u = bs.popUByte();
		strClickUrl = bs.popString();
		strClickUrl_u = bs.popUByte();
		dwClassIdL1 = bs.popUInt();
		dwClassIdL1_u = bs.popUByte();
		dwClassIdL2 = bs.popUInt();
		dwClassIdL2_u = bs.popUByte();
		dwClassIdL3 = bs.popUInt();
		dwClassIdL3_u = bs.popUByte();
		dwClassIdL4 = bs.popUInt();
		dwClassIdL4_u = bs.popUByte();
		strEvalText = bs.popString();
		strEvalText_u = bs.popUByte();
		ddwEvalUin = bs.popLong();
		ddwEvalUin_u = bs.popUByte();
		strEvalName = bs.popString();
		strEvalName_u = bs.popUByte();
		mapCustomData = (Map<String,String>)bs.popMap(String.class,String.class);
		mapCustomData_u = bs.popUByte();
		mapExt = (Map<String,String>)bs.popMap(String.class,String.class);
		mapExt_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取卖快商品表主键
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTabId value 类型为:long
	 * 
	 */
	public long getDwTabId()
	{
		return dwTabId;
	}


	/**
	 * 设置卖快商品表主键
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwTabId(long value)
	{
		this.dwTabId = value;
		this.dwTabId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTabId_u value 类型为:short
	 * 
	 */
	public short getDwTabId_u()
	{
		return dwTabId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwTabId_u(short value)
	{
		this.dwTabId_u = value;
	}


	/**
	 * 获取商品池ID 与BI侧关联
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPoolId value 类型为:long
	 * 
	 */
	public long getDwPoolId()
	{
		return dwPoolId;
	}


	/**
	 * 设置商品池ID 与BI侧关联
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPoolId(long value)
	{
		this.dwPoolId = value;
		this.dwPoolId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPoolId_u value 类型为:short
	 * 
	 */
	public short getDwPoolId_u()
	{
		return dwPoolId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPoolId_u(short value)
	{
		this.dwPoolId_u = value;
	}


	/**
	 * 获取区域排序号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaOrderNum value 类型为:long
	 * 
	 */
	public long getDwAreaOrderNum()
	{
		return dwAreaOrderNum;
	}


	/**
	 * 设置区域排序号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwAreaOrderNum(long value)
	{
		this.dwAreaOrderNum = value;
		this.dwAreaOrderNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwAreaOrderNum_u value 类型为:short
	 * 
	 */
	public short getDwAreaOrderNum_u()
	{
		return dwAreaOrderNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwAreaOrderNum_u(short value)
	{
		this.dwAreaOrderNum_u = value;
	}


	/**
	 * 获取beginTime
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBeginTime value 类型为:long
	 * 
	 */
	public long getDwBeginTime()
	{
		return dwBeginTime;
	}


	/**
	 * 设置beginTime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBeginTime(long value)
	{
		this.dwBeginTime = value;
		this.dwBeginTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBeginTime_u value 类型为:short
	 * 
	 */
	public short getDwBeginTime_u()
	{
		return dwBeginTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwBeginTime_u(short value)
	{
		this.dwBeginTime_u = value;
	}


	/**
	 * 获取endTime
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEndTime value 类型为:long
	 * 
	 */
	public long getDwEndTime()
	{
		return dwEndTime;
	}


	/**
	 * 设置endTime
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwEndTime(long value)
	{
		this.dwEndTime = value;
		this.dwEndTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEndTime_u value 类型为:short
	 * 
	 */
	public short getDwEndTime_u()
	{
		return dwEndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwEndTime_u(short value)
	{
		this.dwEndTime_u = value;
	}


	/**
	 * 获取商品字符串ID
	 * 
	 * 此字段的版本 >= 0
	 * @return strCommodityId value 类型为:String
	 * 
	 */
	public String getStrCommodityId()
	{
		return strCommodityId;
	}


	/**
	 * 设置商品字符串ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCommodityId(String value)
	{
		this.strCommodityId = value;
		this.strCommodityId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCommodityId_u value 类型为:short
	 * 
	 */
	public short getStrCommodityId_u()
	{
		return strCommodityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrCommodityId_u(short value)
	{
		this.strCommodityId_u = value;
	}


	/**
	 * 获取商品标题
	 * 
	 * 此字段的版本 >= 0
	 * @return strTitle value 类型为:String
	 * 
	 */
	public String getStrTitle()
	{
		return strTitle;
	}


	/**
	 * 设置商品标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrTitle(String value)
	{
		this.strTitle = value;
		this.strTitle_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strTitle_u value 类型为:short
	 * 
	 */
	public short getStrTitle_u()
	{
		return strTitle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrTitle_u(short value)
	{
		this.strTitle_u = value;
	}


	/**
	 * 获取商品库存量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwStock value 类型为:long
	 * 
	 */
	public long getDwStock()
	{
		return dwStock;
	}


	/**
	 * 设置商品库存量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwStock(long value)
	{
		this.dwStock = value;
		this.dwStock_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwStock_u value 类型为:short
	 * 
	 */
	public short getDwStock_u()
	{
		return dwStock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwStock_u(short value)
	{
		this.dwStock_u = value;
	}


	/**
	 * 获取报名时提交的参加活动的库存量
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSignInStock value 类型为:long
	 * 
	 */
	public long getDwSignInStock()
	{
		return dwSignInStock;
	}


	/**
	 * 设置报名时提交的参加活动的库存量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSignInStock(long value)
	{
		this.dwSignInStock = value;
		this.dwSignInStock_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSignInStock_u value 类型为:short
	 * 
	 */
	public short getDwSignInStock_u()
	{
		return dwSignInStock_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwSignInStock_u(short value)
	{
		this.dwSignInStock_u = value;
	}


	/**
	 * 获取商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @return dwState value 类型为:long
	 * 
	 */
	public long getDwState()
	{
		return dwState;
	}


	/**
	 * 设置商品状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwState(long value)
	{
		this.dwState = value;
		this.dwState_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwState_u value 类型为:short
	 * 
	 */
	public short getDwState_u()
	{
		return dwState_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwState_u(short value)
	{
		this.dwState_u = value;
	}


	/**
	 * 获取运费
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPostAge value 类型为:long
	 * 
	 */
	public long getDwPostAge()
	{
		return dwPostAge;
	}


	/**
	 * 设置运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPostAge(long value)
	{
		this.dwPostAge = value;
		this.dwPostAge_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPostAge_u value 类型为:short
	 * 
	 */
	public short getDwPostAge_u()
	{
		return dwPostAge_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPostAge_u(short value)
	{
		this.dwPostAge_u = value;
	}


	/**
	 * 获取商品主图
	 * 
	 * 此字段的版本 >= 0
	 * @return strPic value 类型为:String
	 * 
	 */
	public String getStrPic()
	{
		return strPic;
	}


	/**
	 * 设置商品主图
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrPic(String value)
	{
		this.strPic = value;
		this.strPic_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strPic_u value 类型为:short
	 * 
	 */
	public short getStrPic_u()
	{
		return strPic_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrPic_u(short value)
	{
		this.strPic_u = value;
	}


	/**
	 * 获取商品售价
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPrice value 类型为:long
	 * 
	 */
	public long getDwPrice()
	{
		return dwPrice;
	}


	/**
	 * 设置商品售价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPrice(long value)
	{
		this.dwPrice = value;
		this.dwPrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPrice_u value 类型为:short
	 * 
	 */
	public short getDwPrice_u()
	{
		return dwPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwPrice_u(short value)
	{
		this.dwPrice_u = value;
	}


	/**
	 * 获取商品市场价
	 * 
	 * 此字段的版本 >= 0
	 * @return dwMarketPrice value 类型为:long
	 * 
	 */
	public long getDwMarketPrice()
	{
		return dwMarketPrice;
	}


	/**
	 * 设置商品市场价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwMarketPrice(long value)
	{
		this.dwMarketPrice = value;
		this.dwMarketPrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwMarketPrice_u value 类型为:short
	 * 
	 */
	public short getDwMarketPrice_u()
	{
		return dwMarketPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwMarketPrice_u(short value)
	{
		this.dwMarketPrice_u = value;
	}


	/**
	 * 获取参加活动报名时提交的价格
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSignInPrice value 类型为:long
	 * 
	 */
	public long getDwSignInPrice()
	{
		return dwSignInPrice;
	}


	/**
	 * 设置参加活动报名时提交的价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSignInPrice(long value)
	{
		this.dwSignInPrice = value;
		this.dwSignInPrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSignInPrice_u value 类型为:short
	 * 
	 */
	public short getDwSignInPrice_u()
	{
		return dwSignInPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwSignInPrice_u(short value)
	{
		this.dwSignInPrice_u = value;
	}


	/**
	 * 获取当前生效的最低可售价
	 * 
	 * 此字段的版本 >= 0
	 * @return dwActivePrice value 类型为:long
	 * 
	 */
	public long getDwActivePrice()
	{
		return dwActivePrice;
	}


	/**
	 * 设置当前生效的最低可售价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwActivePrice(long value)
	{
		this.dwActivePrice = value;
		this.dwActivePrice_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwActivePrice_u value 类型为:short
	 * 
	 */
	public short getDwActivePrice_u()
	{
		return dwActivePrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwActivePrice_u(short value)
	{
		this.dwActivePrice_u = value;
	}


	/**
	 * 获取商品排序号
	 * 
	 * 此字段的版本 >= 0
	 * @return dwOrderNum value 类型为:long
	 * 
	 */
	public long getDwOrderNum()
	{
		return dwOrderNum;
	}


	/**
	 * 设置商品排序号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwOrderNum(long value)
	{
		this.dwOrderNum = value;
		this.dwOrderNum_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwOrderNum_u value 类型为:short
	 * 
	 */
	public short getDwOrderNum_u()
	{
		return dwOrderNum_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwOrderNum_u(short value)
	{
		this.dwOrderNum_u = value;
	}


	/**
	 * 获取店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @return strShopName value 类型为:String
	 * 
	 */
	public String getStrShopName()
	{
		return strShopName;
	}


	/**
	 * 设置店铺名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrShopName(String value)
	{
		this.strShopName = value;
		this.strShopName_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strShopName_u value 类型为:short
	 * 
	 */
	public short getStrShopName_u()
	{
		return strShopName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrShopName_u(short value)
	{
		this.strShopName_u = value;
	}


	/**
	 * 获取店铺UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwSellerUin value 类型为:long
	 * 
	 */
	public long getDdwSellerUin()
	{
		return ddwSellerUin;
	}


	/**
	 * 设置店铺UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwSellerUin(long value)
	{
		this.ddwSellerUin = value;
		this.ddwSellerUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwSellerUin_u value 类型为:short
	 * 
	 */
	public short getDdwSellerUin_u()
	{
		return ddwSellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwSellerUin_u(short value)
	{
		this.ddwSellerUin_u = value;
	}


	/**
	 * 获取卖家分
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerCredit value 类型为:long
	 * 
	 */
	public long getDwSellerCredit()
	{
		return dwSellerCredit;
	}


	/**
	 * 设置卖家分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerCredit(long value)
	{
		this.dwSellerCredit = value;
		this.dwSellerCredit_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerCredit_u value 类型为:short
	 * 
	 */
	public short getDwSellerCredit_u()
	{
		return dwSellerCredit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwSellerCredit_u(short value)
	{
		this.dwSellerCredit_u = value;
	}


	/**
	 * 获取ptag
	 * 
	 * 此字段的版本 >= 0
	 * @return strPtag value 类型为:String
	 * 
	 */
	public String getStrPtag()
	{
		return strPtag;
	}


	/**
	 * 设置ptag
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrPtag(String value)
	{
		this.strPtag = value;
		this.strPtag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strPtag_u value 类型为:short
	 * 
	 */
	public short getStrPtag_u()
	{
		return strPtag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrPtag_u(short value)
	{
		this.strPtag_u = value;
	}


	/**
	 * 获取商品详情原始URL
	 * 
	 * 此字段的版本 >= 0
	 * @return strDetailUrl value 类型为:String
	 * 
	 */
	public String getStrDetailUrl()
	{
		return strDetailUrl;
	}


	/**
	 * 设置商品详情原始URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrDetailUrl(String value)
	{
		this.strDetailUrl = value;
		this.strDetailUrl_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strDetailUrl_u value 类型为:short
	 * 
	 */
	public short getStrDetailUrl_u()
	{
		return strDetailUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrDetailUrl_u(short value)
	{
		this.strDetailUrl_u = value;
	}


	/**
	 * 获取带详细参数的商品详情URL
	 * 
	 * 此字段的版本 >= 0
	 * @return strClickUrl value 类型为:String
	 * 
	 */
	public String getStrClickUrl()
	{
		return strClickUrl;
	}


	/**
	 * 设置带详细参数的商品详情URL
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrClickUrl(String value)
	{
		this.strClickUrl = value;
		this.strClickUrl_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strClickUrl_u value 类型为:short
	 * 
	 */
	public short getStrClickUrl_u()
	{
		return strClickUrl_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrClickUrl_u(short value)
	{
		this.strClickUrl_u = value;
	}


	/**
	 * 获取一级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL1 value 类型为:long
	 * 
	 */
	public long getDwClassIdL1()
	{
		return dwClassIdL1;
	}


	/**
	 * 设置一级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwClassIdL1(long value)
	{
		this.dwClassIdL1 = value;
		this.dwClassIdL1_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL1_u value 类型为:short
	 * 
	 */
	public short getDwClassIdL1_u()
	{
		return dwClassIdL1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwClassIdL1_u(short value)
	{
		this.dwClassIdL1_u = value;
	}


	/**
	 * 获取二级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL2 value 类型为:long
	 * 
	 */
	public long getDwClassIdL2()
	{
		return dwClassIdL2;
	}


	/**
	 * 设置二级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwClassIdL2(long value)
	{
		this.dwClassIdL2 = value;
		this.dwClassIdL2_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL2_u value 类型为:short
	 * 
	 */
	public short getDwClassIdL2_u()
	{
		return dwClassIdL2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwClassIdL2_u(short value)
	{
		this.dwClassIdL2_u = value;
	}


	/**
	 * 获取三级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL3 value 类型为:long
	 * 
	 */
	public long getDwClassIdL3()
	{
		return dwClassIdL3;
	}


	/**
	 * 设置三级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwClassIdL3(long value)
	{
		this.dwClassIdL3 = value;
		this.dwClassIdL3_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL3_u value 类型为:short
	 * 
	 */
	public short getDwClassIdL3_u()
	{
		return dwClassIdL3_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwClassIdL3_u(short value)
	{
		this.dwClassIdL3_u = value;
	}


	/**
	 * 获取四级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL4 value 类型为:long
	 * 
	 */
	public long getDwClassIdL4()
	{
		return dwClassIdL4;
	}


	/**
	 * 设置四级类目ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwClassIdL4(long value)
	{
		this.dwClassIdL4 = value;
		this.dwClassIdL4_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwClassIdL4_u value 类型为:short
	 * 
	 */
	public short getDwClassIdL4_u()
	{
		return dwClassIdL4_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwClassIdL4_u(short value)
	{
		this.dwClassIdL4_u = value;
	}


	/**
	 * 获取评论内容
	 * 
	 * 此字段的版本 >= 0
	 * @return strEvalText value 类型为:String
	 * 
	 */
	public String getStrEvalText()
	{
		return strEvalText;
	}


	/**
	 * 设置评论内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrEvalText(String value)
	{
		this.strEvalText = value;
		this.strEvalText_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strEvalText_u value 类型为:short
	 * 
	 */
	public short getStrEvalText_u()
	{
		return strEvalText_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrEvalText_u(short value)
	{
		this.strEvalText_u = value;
	}


	/**
	 * 获取评论发布者UIN
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwEvalUin value 类型为:long
	 * 
	 */
	public long getDdwEvalUin()
	{
		return ddwEvalUin;
	}


	/**
	 * 设置评论发布者UIN
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDdwEvalUin(long value)
	{
		this.ddwEvalUin = value;
		this.ddwEvalUin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ddwEvalUin_u value 类型为:short
	 * 
	 */
	public short getDdwEvalUin_u()
	{
		return ddwEvalUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDdwEvalUin_u(short value)
	{
		this.ddwEvalUin_u = value;
	}


	/**
	 * 获取评论发布者昵称
	 * 
	 * 此字段的版本 >= 0
	 * @return strEvalName value 类型为:String
	 * 
	 */
	public String getStrEvalName()
	{
		return strEvalName;
	}


	/**
	 * 设置评论发布者昵称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrEvalName(String value)
	{
		this.strEvalName = value;
		this.strEvalName_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strEvalName_u value 类型为:short
	 * 
	 */
	public short getStrEvalName_u()
	{
		return strEvalName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrEvalName_u(short value)
	{
		this.strEvalName_u = value;
	}


	/**
	 * 获取卖家自定义信息 如 附加图文等
	 *  "itemExtValue1": "美的迷你饮水机",
	    "uploadPicUrl1": "http://img6.paipaiimg.com/boss-53DB801E-6E5AF63253DB801E04010000343CB337.1.jpg",
	    "uploadPicUrl2": "http://img6.paipaiimg.com/boss-53DB801E-6E5AF63253DB801E04010000343CB337.2.jpg",
	    "uploadPicUrl3": "",
	    "uploadPicUrl4": ""
	 * 
	 * 此字段的版本 >= 0
	 * @return mapCustomData value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapCustomData()
	{
		return mapCustomData;
	}


	/**
	 * 设置卖家自定义信息 如 附加图文等
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapCustomData(Map<String,String> value)
	{
		if (value != null) {
				this.mapCustomData = value;
				this.mapCustomData_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mapCustomData_u value 类型为:short
	 * 
	 */
	public short getMapCustomData_u()
	{
		return mapCustomData_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapCustomData_u(short value)
	{
		this.mapCustomData_u = value;
	}


	/**
	 * 获取其他附加信息
	 * 
	 * 此字段的版本 >= 0
	 * @return mapExt value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapExt()
	{
		return mapExt;
	}


	/**
	 * 设置其他附加信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapExt(Map<String,String> value)
	{
		if (value != null) {
				this.mapExt = value;
				this.mapExt_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return mapExt_u value 类型为:short
	 * 
	 */
	public short getMapExt_u()
	{
		return mapExt_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMapExt_u(short value)
	{
		this.mapExt_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(TabData)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwTabId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwTabId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPoolId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPoolId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwAreaOrderNum的长度 size_of(uint32_t)
				length += 1;  //计算字段dwAreaOrderNum_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwBeginTime的长度 size_of(uint32_t)
				length += 1;  //计算字段dwBeginTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwEndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段dwEndTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strCommodityId);  //计算字段strCommodityId的长度 size_of(String)
				length += 1;  //计算字段strCommodityId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strTitle);  //计算字段strTitle的长度 size_of(String)
				length += 1;  //计算字段strTitle_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwStock的长度 size_of(uint32_t)
				length += 1;  //计算字段dwStock_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwSignInStock的长度 size_of(uint32_t)
				length += 1;  //计算字段dwSignInStock_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwState的长度 size_of(uint32_t)
				length += 1;  //计算字段dwState_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPostAge的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPostAge_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strPic);  //计算字段strPic的长度 size_of(String)
				length += 1;  //计算字段strPic_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段dwPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwMarketPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段dwMarketPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwSignInPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段dwSignInPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwActivePrice的长度 size_of(uint32_t)
				length += 1;  //计算字段dwActivePrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwOrderNum的长度 size_of(uint32_t)
				length += 1;  //计算字段dwOrderNum_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strShopName);  //计算字段strShopName的长度 size_of(String)
				length += 1;  //计算字段strShopName_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwSellerUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwSellerUin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwSellerCredit的长度 size_of(uint32_t)
				length += 1;  //计算字段dwSellerCredit_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strPtag);  //计算字段strPtag的长度 size_of(String)
				length += 1;  //计算字段strPtag_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strDetailUrl);  //计算字段strDetailUrl的长度 size_of(String)
				length += 1;  //计算字段strDetailUrl_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strClickUrl);  //计算字段strClickUrl的长度 size_of(String)
				length += 1;  //计算字段strClickUrl_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClassIdL1的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClassIdL1_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClassIdL2的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClassIdL2_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClassIdL3的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClassIdL3_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwClassIdL4的长度 size_of(uint32_t)
				length += 1;  //计算字段dwClassIdL4_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strEvalText);  //计算字段strEvalText的长度 size_of(String)
				length += 1;  //计算字段strEvalText_u的长度 size_of(uint8_t)
				length += 17;  //计算字段ddwEvalUin的长度 size_of(uint64_t)
				length += 1;  //计算字段ddwEvalUin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strEvalName);  //计算字段strEvalName的长度 size_of(String)
				length += 1;  //计算字段strEvalName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapCustomData);  //计算字段mapCustomData的长度 size_of(Map)
				length += 1;  //计算字段mapCustomData_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(mapExt);  //计算字段mapExt的长度 size_of(Map)
				length += 1;  //计算字段mapExt_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
