//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;


/**
 *商品库存图片信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemStockImg  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 库存属性串
	 *
	 * 版本 >= 0
	 */
	 private String attr = new String();

	/**
	 * 库存属性值
	 *
	 * 版本 >= 0
	 */
	 private String attrValue = new String();

	/**
	 * 图片位置
	 *
	 * 版本 >= 0
	 */
	 private String imgPos = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushString(attr);
		bs.pushString(attrValue);
		bs.pushString(imgPos);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		attr = bs.popString();
		attrValue = bs.popString();
		imgPos = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return attr;
	}


	/**
	 * 设置库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		this.attr = value;
	}


	/**
	 * 获取库存属性值
	 * 
	 * 此字段的版本 >= 0
	 * @return attrValue value 类型为:String
	 * 
	 */
	public String getAttrValue()
	{
		return attrValue;
	}


	/**
	 * 设置库存属性值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttrValue(String value)
	{
		this.attrValue = value;
	}


	/**
	 * 获取图片位置
	 * 
	 * 此字段的版本 >= 0
	 * @return imgPos value 类型为:String
	 * 
	 */
	public String getImgPos()
	{
		return imgPos;
	}


	/**
	 * 设置图片位置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setImgPos(String value)
	{
		this.imgPos = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemStockImg)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(attr);  //计算字段attr的长度 size_of(String)
				length += ByteStream.getObjectSize(attrValue);  //计算字段attrValue的长度 size_of(String)
				length += ByteStream.getObjectSize(imgPos);  //计算字段imgPos的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
