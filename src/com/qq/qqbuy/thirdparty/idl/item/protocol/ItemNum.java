//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品统计信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemNum  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 商品总数，即目前还有的商品数量
	 *
	 * 版本 >= 0
	 */
	 private long num;

	/**
	 * 付款的商品数
	 *
	 * 版本 >= 0
	 */
	 private long payNum;

	/**
	 * 全部付款商品数
	 *
	 * 版本 >= 0
	 */
	 private long totalPayNum;

	/**
	 * 付款操作次数（1次付款操作可以付款多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long payCount;

	/**
	 * 总的付款操作次数（1次付款操作可以付款多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long totalPayCount;

	/**
	 * 下单商品数
	 *
	 * 版本 >= 0
	 */
	 private long buyNum;

	/**
	 * 下单总商品数
	 *
	 * 版本 >= 0
	 */
	 private long toalBuyNum;

	/**
	 * 下单操作次数（1次下单可以下单多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long buyCount;

	/**
	 * 总的下单操作次数（1次下单可以下单多个商品）
	 *
	 * 版本 >= 0
	 */
	 private long totalBuyCount;

	/**
	 * 商品浏览次数
	 *
	 * 版本 >= 0
	 */
	 private long visitCount;

	/**
	 * 当期销售量数量
	 *
	 * 版本 >= 0
	 */
	 private long currPayNum;

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved1 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved1;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved2;

	/**
	 * dwReserved
	 *
	 * 版本 >= 0
	 */
	 private long dwReserved3;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(num);
		bs.pushUInt(payNum);
		bs.pushUInt(totalPayNum);
		bs.pushUInt(payCount);
		bs.pushUInt(totalPayCount);
		bs.pushUInt(buyNum);
		bs.pushUInt(toalBuyNum);
		bs.pushUInt(buyCount);
		bs.pushUInt(totalBuyCount);
		bs.pushUInt(visitCount);
		bs.pushUInt(currPayNum);
		bs.pushString(strReserved1);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(dwReserved1);
		bs.pushUInt(dwReserved2);
		bs.pushUInt(dwReserved3);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		num = bs.popUInt();
		payNum = bs.popUInt();
		totalPayNum = bs.popUInt();
		payCount = bs.popUInt();
		totalPayCount = bs.popUInt();
		buyNum = bs.popUInt();
		toalBuyNum = bs.popUInt();
		buyCount = bs.popUInt();
		totalBuyCount = bs.popUInt();
		visitCount = bs.popUInt();
		currPayNum = bs.popUInt();
		strReserved1 = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		dwReserved1 = bs.popUInt();
		dwReserved2 = bs.popUInt();
		dwReserved3 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取商品总数，即目前还有的商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return num;
	}


	/**
	 * 设置商品总数，即目前还有的商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.num = value;
	}


	/**
	 * 获取付款的商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return payNum value 类型为:long
	 * 
	 */
	public long getPayNum()
	{
		return payNum;
	}


	/**
	 * 设置付款的商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayNum(long value)
	{
		this.payNum = value;
	}


	/**
	 * 获取全部付款商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayNum value 类型为:long
	 * 
	 */
	public long getTotalPayNum()
	{
		return totalPayNum;
	}


	/**
	 * 设置全部付款商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayNum(long value)
	{
		this.totalPayNum = value;
	}


	/**
	 * 获取付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return payCount value 类型为:long
	 * 
	 */
	public long getPayCount()
	{
		return payCount;
	}


	/**
	 * 设置付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayCount(long value)
	{
		this.payCount = value;
	}


	/**
	 * 获取总的付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return totalPayCount value 类型为:long
	 * 
	 */
	public long getTotalPayCount()
	{
		return totalPayCount;
	}


	/**
	 * 设置总的付款操作次数（1次付款操作可以付款多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalPayCount(long value)
	{
		this.totalPayCount = value;
	}


	/**
	 * 获取下单商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return buyNum value 类型为:long
	 * 
	 */
	public long getBuyNum()
	{
		return buyNum;
	}


	/**
	 * 设置下单商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyNum(long value)
	{
		this.buyNum = value;
	}


	/**
	 * 获取下单总商品数
	 * 
	 * 此字段的版本 >= 0
	 * @return toalBuyNum value 类型为:long
	 * 
	 */
	public long getToalBuyNum()
	{
		return toalBuyNum;
	}


	/**
	 * 设置下单总商品数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToalBuyNum(long value)
	{
		this.toalBuyNum = value;
	}


	/**
	 * 获取下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return buyCount value 类型为:long
	 * 
	 */
	public long getBuyCount()
	{
		return buyCount;
	}


	/**
	 * 设置下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyCount(long value)
	{
		this.buyCount = value;
	}


	/**
	 * 获取总的下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @return totalBuyCount value 类型为:long
	 * 
	 */
	public long getTotalBuyCount()
	{
		return totalBuyCount;
	}


	/**
	 * 设置总的下单操作次数（1次下单可以下单多个商品）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalBuyCount(long value)
	{
		this.totalBuyCount = value;
	}


	/**
	 * 获取商品浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @return visitCount value 类型为:long
	 * 
	 */
	public long getVisitCount()
	{
		return visitCount;
	}


	/**
	 * 设置商品浏览次数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVisitCount(long value)
	{
		this.visitCount = value;
	}


	/**
	 * 获取当期销售量数量
	 * 
	 * 此字段的版本 >= 0
	 * @return currPayNum value 类型为:long
	 * 
	 */
	public long getCurrPayNum()
	{
		return currPayNum;
	}


	/**
	 * 设置当期销售量数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCurrPayNum(long value)
	{
		this.currPayNum = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved1 value 类型为:String
	 * 
	 */
	public String getStrReserved1()
	{
		return strReserved1;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved1(String value)
	{
		this.strReserved1 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved1 value 类型为:long
	 * 
	 */
	public long getDwReserved1()
	{
		return dwReserved1;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved1(long value)
	{
		this.dwReserved1 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved2 value 类型为:long
	 * 
	 */
	public long getDwReserved2()
	{
		return dwReserved2;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved2(long value)
	{
		this.dwReserved2 = value;
	}


	/**
	 * 获取dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return dwReserved3 value 类型为:long
	 * 
	 */
	public long getDwReserved3()
	{
		return dwReserved3;
	}


	/**
	 * 设置dwReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwReserved3(long value)
	{
		this.dwReserved3 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemNum)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段num的长度 size_of(uint32_t)
				length += 4;  //计算字段payNum的长度 size_of(uint32_t)
				length += 4;  //计算字段totalPayNum的长度 size_of(uint32_t)
				length += 4;  //计算字段payCount的长度 size_of(uint32_t)
				length += 4;  //计算字段totalPayCount的长度 size_of(uint32_t)
				length += 4;  //计算字段buyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段toalBuyNum的长度 size_of(uint32_t)
				length += 4;  //计算字段buyCount的长度 size_of(uint32_t)
				length += 4;  //计算字段totalBuyCount的长度 size_of(uint32_t)
				length += 4;  //计算字段visitCount的长度 size_of(uint32_t)
				length += 4;  //计算字段currPayNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strReserved1);  //计算字段strReserved1的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段dwReserved1的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved2的长度 size_of(uint32_t)
				length += 4;  //计算字段dwReserved3的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
