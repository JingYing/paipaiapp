//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.api.item.po.idl.ItemPoV2.java

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *商品价格扩展信息
 *
 *@date 2013-02-27 05:06:02
 *
 *@since version:0
*/
public class ItemPriceExt  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 3;

	/**
	 * 彩钻价格，如果有彩钻价格则vector不为空
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> colorPrice = new Vector<uint32_t>();

	/**
	 * 活动价格
	 *
	 * 版本 >= 0
	 */
	 private long activityPrice;

	/**
	 * 活动类型
	 *
	 * 版本 >= 0
	 */
	 private long activityType;

	/**
	 * 活动信息描述
	 *
	 * 版本 >= 0
	 */
	 private String activityDesc = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved2 = new String();

	/**
	 * strReserved
	 *
	 * 版本 >= 0
	 */
	 private String strReserved3 = new String();

	/**
	 * 活动的当前状态:0-活动尚未开始，1-活动已经开始
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 *
	 * 版本 >= 0
	 */
	 private long beginTime;

	/**
	 * 活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 *
	 * 版本 >= 0
	 */
	 private long endTime;

	/**
	 * 店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 *
	 * 版本 >= 1
	 */
	 private Vector<uint32_t> shopVipPrice = new Vector<uint32_t>();

	/**
	 * 活动信息是否需要显示
	 *
	 * 版本 >= 2
	 */
	 private boolean isActivityNeedShow;

	/**
	 * 活动属性信息
	 *
	 * 版本 >= 3
	 */
	 private long ActiveProperty;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushObject(colorPrice);
		bs.pushUInt(activityPrice);
		bs.pushUInt(activityType);
		bs.pushString(activityDesc);
		bs.pushString(strReserved2);
		bs.pushString(strReserved3);
		bs.pushUInt(state);
		bs.pushUInt(beginTime);
		bs.pushUInt(endTime);
		if(  this.version >= 1 ){
				bs.pushObject(shopVipPrice);
		}
		if(  this.version >= 2 ){
				bs.pushBoolean(isActivityNeedShow);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(ActiveProperty);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		colorPrice = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		activityPrice = bs.popUInt();
		activityType = bs.popUInt();
		activityDesc = bs.popString();
		strReserved2 = bs.popString();
		strReserved3 = bs.popString();
		state = bs.popUInt();
		beginTime = bs.popUInt();
		endTime = bs.popUInt();
		if(  this.version >= 1 ){
				shopVipPrice = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		}
		if(  this.version >= 2 ){
				isActivityNeedShow = bs.popBoolean();
		}
		if(  this.version >= 3 ){
				ActiveProperty = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取彩钻价格，如果有彩钻价格则vector不为空
	 * 
	 * 此字段的版本 >= 0
	 * @return colorPrice value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getColorPrice()
	{
		return colorPrice;
	}


	/**
	 * 设置彩钻价格，如果有彩钻价格则vector不为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setColorPrice(Vector<uint32_t> value)
	{
		if (value != null) {
				this.colorPrice = value;
		}else{
				this.colorPrice = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @return activityPrice value 类型为:long
	 * 
	 */
	public long getActivityPrice()
	{
		return activityPrice;
	}


	/**
	 * 设置活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityPrice(long value)
	{
		this.activityPrice = value;
	}


	/**
	 * 获取活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @return activityType value 类型为:long
	 * 
	 */
	public long getActivityType()
	{
		return activityType;
	}


	/**
	 * 设置活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityType(long value)
	{
		this.activityType = value;
	}


	/**
	 * 获取活动信息描述
	 * 
	 * 此字段的版本 >= 0
	 * @return activityDesc value 类型为:String
	 * 
	 */
	public String getActivityDesc()
	{
		return activityDesc;
	}


	/**
	 * 设置活动信息描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActivityDesc(String value)
	{
		this.activityDesc = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved2 value 类型为:String
	 * 
	 */
	public String getStrReserved2()
	{
		return strReserved2;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved2(String value)
	{
		this.strReserved2 = value;
	}


	/**
	 * 获取strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @return strReserved3 value 类型为:String
	 * 
	 */
	public String getStrReserved3()
	{
		return strReserved3;
	}


	/**
	 * 设置strReserved
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrReserved3(String value)
	{
		this.strReserved3 = value;
	}


	/**
	 * 获取活动的当前状态:0-活动尚未开始，1-活动已经开始
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置活动的当前状态:0-活动尚未开始，1-活动已经开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
	}


	/**
	 * 获取活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @return beginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return beginTime;
	}


	/**
	 * 设置活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.beginTime = value;
	}


	/**
	 * 获取活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.endTime = value;
	}


	/**
	 * 获取店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 * 
	 * 此字段的版本 >= 1
	 * @return shopVipPrice value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getShopVipPrice()
	{
		return shopVipPrice;
	}


	/**
	 * 设置店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setShopVipPrice(Vector<uint32_t> value)
	{
		if (value != null) {
				this.shopVipPrice = value;
		}else{
				this.shopVipPrice = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取活动信息是否需要显示
	 * 
	 * 此字段的版本 >= 2
	 * @return isActivityNeedShow value 类型为:boolean
	 * 
	 */
	public boolean getIsActivityNeedShow()
	{
		return isActivityNeedShow;
	}


	/**
	 * 设置活动信息是否需要显示
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setIsActivityNeedShow(boolean value)
	{
		this.isActivityNeedShow = value;
	}


	/**
	 * 获取活动属性信息
	 * 
	 * 此字段的版本 >= 3
	 * @return ActiveProperty value 类型为:long
	 * 
	 */
	public long getActiveProperty()
	{
		return ActiveProperty;
	}


	/**
	 * 设置活动属性信息
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveProperty(long value)
	{
		this.ActiveProperty = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPriceExt)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(colorPrice);  //计算字段colorPrice的长度 size_of(Vector)
				length += 4;  //计算字段activityPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段activityType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(activityDesc);  //计算字段activityDesc的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved2);  //计算字段strReserved2的长度 size_of(String)
				length += ByteStream.getObjectSize(strReserved3);  //计算字段strReserved3的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(uint32_t)
				length += 4;  //计算字段beginTime的长度 size_of(uint32_t)
				length += 4;  //计算字段endTime的长度 size_of(uint32_t)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(shopVipPrice);  //计算字段shopVipPrice的长度 size_of(Vector)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段isActivityNeedShow的长度 size_of(boolean)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段ActiveProperty的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;///<版本号
 *	Vector<uint32_t> colorPrice;///<彩钻价格，如果有彩钻价格则vector不为空
 *	long activityPrice;///<活动价格
 *	long activityType;///<活动类型
 *	String activityDesc;///<活动信息描述
 *	String strReserved2;///<strReserved
 *	String strReserved3;///<strReserved
 *	long state;///<活动的当前状态:0-活动尚未开始，1-活动已经开始
 *	long beginTime;///<活动的开始时间，为uinxi_time_stamp的格式，单位为秒
 *	long endTime;///<活动的结束时间，为uinxi_time_stamp的格式，单位为秒
 *	Vector<uint32_t> shopVipPrice;///<店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
 *****以上是版本1所包含的字段*******
 *
 *****以下是版本2所包含的字段*******
 *	long version;///<版本号
 *	Vector<uint32_t> colorPrice;///<彩钻价格，如果有彩钻价格则vector不为空
 *	long activityPrice;///<活动价格
 *	long activityType;///<活动类型
 *	String activityDesc;///<活动信息描述
 *	String strReserved2;///<strReserved
 *	String strReserved3;///<strReserved
 *	long state;///<活动的当前状态:0-活动尚未开始，1-活动已经开始
 *	long beginTime;///<活动的开始时间，为uinxi_time_stamp的格式，单位为秒
 *	long endTime;///<活动的结束时间，为uinxi_time_stamp的格式，单位为秒
 *	Vector<uint32_t> shopVipPrice;///<店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
 *	boolean isActivityNeedShow;///<活动信息是否需要显示
 *****以上是版本2所包含的字段*******
 *
 *****以下是版本3所包含的字段*******
 *	long version;///<版本号
 *	Vector<uint32_t> colorPrice;///<彩钻价格，如果有彩钻价格则vector不为空
 *	long activityPrice;///<活动价格
 *	long activityType;///<活动类型
 *	String activityDesc;///<活动信息描述
 *	String strReserved2;///<strReserved
 *	String strReserved3;///<strReserved
 *	long state;///<活动的当前状态:0-活动尚未开始，1-活动已经开始
 *	long beginTime;///<活动的开始时间，为uinxi_time_stamp的格式，单位为秒
 *	long endTime;///<活动的结束时间，为uinxi_time_stamp的格式，单位为秒
 *	Vector<uint32_t> shopVipPrice;///<店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
 *	boolean isActivityNeedShow;///<活动信息是否需要显示
 *	long ActiveProperty;///<活动属性信息
 *****以上是版本3所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
