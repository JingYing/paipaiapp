//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.item.idl.PPItemInfo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.qgo;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *商品库存信息
 *
 *@date 2013-04-10 02:32:23
 *
 *@since version:0
*/
public class ItemStock  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 0;

	/**
	 * 商品库存id
	 *
	 * 版本 >= 0
	 */
	 private String stockId = new String();

	/**
	 * 商品库存价格
	 *
	 * 版本 >= 0
	 */
	 private int stockPrice;

	/**
	 * 商品的库存属性串
	 *
	 * 版本 >= 0
	 */
	 private String stockAttr = new String();

	/**
	 * 商品的该库存对应的销售数量
	 *
	 * 版本 >= 0
	 */
	 private int soldCount;

	/**
	 * 商品的库存数量
	 *
	 * 版本 >= 0
	 */
	 private int stockCount;

	/**
	 * 商品1~6级彩钻价格
	 *
	 * 版本 >= 0
	 */
	 private Vector<Integer> discountPrice = new Vector<Integer>();

	/**
	 * 促销价
	 *
	 * 版本 >= 0
	 */
	 private ItemPriceExtPo oPriceExt = new ItemPriceExtPo();

	/**
	 * 备用字段版本
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs) - 4);
		bs.pushInt(version);
		bs.pushString(stockId);
		bs.pushInt(stockPrice);
		bs.pushString(stockAttr);
		bs.pushInt(soldCount);
		bs.pushInt(stockCount);
		bs.pushObject(discountPrice);
		bs.pushObject(oPriceExt);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		stockId = bs.popString();
		stockPrice = bs.popInt();
		stockAttr = bs.popString();
		soldCount = bs.popInt();
		stockCount = bs.popInt();
		discountPrice = (Vector<Integer>)bs.popVector(Integer.class);
		oPriceExt = (ItemPriceExtPo) bs.popObject(ItemPriceExtPo.class);
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取商品库存id
	 * 
	 * 此字段的版本 >= 0
	 * @return stockId value 类型为:String
	 * 
	 */
	public String getStockId()
	{
		return stockId;
	}


	/**
	 * 设置商品库存id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockId(String value)
	{
		this.stockId = value;
	}


	/**
	 * 获取商品库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @return stockPrice value 类型为:int
	 * 
	 */
	public int getStockPrice()
	{
		return stockPrice;
	}


	/**
	 * 设置商品库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStockPrice(int value)
	{
		this.stockPrice = value;
	}


	/**
	 * 获取商品的库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @return stockAttr value 类型为:String
	 * 
	 */
	public String getStockAttr()
	{
		return stockAttr;
	}


	/**
	 * 设置商品的库存属性串
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStockAttr(String value)
	{
		this.stockAttr = value;
	}


	/**
	 * 获取商品的该库存对应的销售数量
	 * 
	 * 此字段的版本 >= 0
	 * @return soldCount value 类型为:int
	 * 
	 */
	public int getSoldCount()
	{
		return soldCount;
	}


	/**
	 * 设置商品的该库存对应的销售数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setSoldCount(int value)
	{
		this.soldCount = value;
	}


	/**
	 * 获取商品的库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @return stockCount value 类型为:int
	 * 
	 */
	public int getStockCount()
	{
		return stockCount;
	}


	/**
	 * 设置商品的库存数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStockCount(int value)
	{
		this.stockCount = value;
	}


	/**
	 * 获取商品1~6级彩钻价格
	 * 
	 * 此字段的版本 >= 0
	 * @return discountPrice value 类型为:Vector<Integer>
	 * 
	 */
	public Vector<Integer> getDiscountPrice()
	{
		return discountPrice;
	}


	/**
	 * 设置商品1~6级彩钻价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Integer>
	 * 
	 */
	public void setDiscountPrice(Vector<Integer> value)
	{
		if (value != null) {
				this.discountPrice = value;
		}else{
				this.discountPrice = new Vector<Integer>();
		}
	}


	/**
	 * 获取促销价
	 * 
	 * 此字段的版本 >= 0
	 * @return oPriceExt value 类型为:ItemPriceExtPo
	 * 
	 */
	public ItemPriceExtPo getOPriceExt()
	{
		return oPriceExt;
	}


	/**
	 * 设置促销价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ItemPriceExtPo
	 * 
	 */
	public void setOPriceExt(ItemPriceExtPo value)
	{
		if (value != null) {
				this.oPriceExt = value;
		}else{
				this.oPriceExt = new ItemPriceExtPo();
		}
	}


	/**
	 * 获取备用字段版本
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemStock)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(stockId);  //计算字段stockId的长度 size_of(String)
				length += 4;  //计算字段stockPrice的长度 size_of(int)
				length += ByteStream.getObjectSize(stockAttr);  //计算字段stockAttr的长度 size_of(String)
				length += 4;  //计算字段soldCount的长度 size_of(int)
				length += 4;  //计算字段stockCount的长度 size_of(int)
				length += ByteStream.getObjectSize(discountPrice);  //计算字段discountPrice的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oPriceExt);  //计算字段oPriceExt的长度 size_of(ItemPriceExtPo)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(ByteStream bs)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemStock)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4 + stockId.getBytes(bs.getDecodeCharset()).length;  //计算字段stockId的长度 size_of(String)
				length += 4;  //计算字段stockPrice的长度 size_of(int)
				length += 4 + stockAttr.getBytes(bs.getDecodeCharset()).length;  //计算字段stockAttr的长度 size_of(String)
				length += 4;  //计算字段soldCount的长度 size_of(int)
				length += 4;  //计算字段stockCount的长度 size_of(int)
				length += ByteStream.getObjectSize(discountPrice);  //计算字段discountPrice的长度 size_of(Vector)
				length += ByteStream.getObjectSize(oPriceExt);  //计算字段oPriceExt的长度 size_of(ItemPriceExtPo)
				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
