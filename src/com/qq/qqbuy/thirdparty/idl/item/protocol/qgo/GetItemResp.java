 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.item.idl.ItemAo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.qgo;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-04-08 02:57:15
 *
 *@since version:0
*/
public class  GetItemResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 拍拍商品信息
	 *
	 * 版本 >= 0
	 */
	 private PPItemInfo ppItemInfo = new PPItemInfo();

	/**
	 * 手拍商品信息
	 *
	 * 版本 >= 0
	 */
	 private MMItemInfo mmItemInfo = new MMItemInfo();

	/**
	 * 请求保留字 
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushObject(ppItemInfo);
		bs.pushObject(mmItemInfo);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		ppItemInfo = (PPItemInfo) bs.popObject(PPItemInfo.class);
		mmItemInfo = (MMItemInfo) bs.popObject(MMItemInfo.class);
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80038805L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ppItemInfo value 类型为:PPItemInfo
	 * 
	 */
	public PPItemInfo getPpItemInfo()
	{
		return ppItemInfo;
	}


	/**
	 * 设置拍拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:PPItemInfo
	 * 
	 */
	public void setPpItemInfo(PPItemInfo value)
	{
		if (value != null) {
				this.ppItemInfo = value;
		}else{
				this.ppItemInfo = new PPItemInfo();
		}
	}


	/**
	 * 获取手拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @return mmItemInfo value 类型为:MMItemInfo
	 * 
	 */
	public MMItemInfo getMmItemInfo()
	{
		return mmItemInfo;
	}


	/**
	 * 设置手拍商品信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MMItemInfo
	 * 
	 */
	public void setMmItemInfo(MMItemInfo value)
	{
		if (value != null) {
				this.mmItemInfo = value;
		}else{
				this.mmItemInfo = new MMItemInfo();
		}
	}


	/**
	 * 获取请求保留字 
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置请求保留字 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetItemResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ppItemInfo);  //计算字段ppItemInfo的长度 size_of(PPItemInfo)
				length += ByteStream.getObjectSize(mmItemInfo);  //计算字段mmItemInfo的长度 size_of(MMItemInfo)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
