//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qgo.item.idl.PPItemInfo.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.qgo;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *促销商品信息po
 *
 *@date 2013-04-10 02:32:23
 *
 *@since version:0
*/
public class ItemPriceExtPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private int version = 3;

	/**
	 * 彩钻价格，如果有彩钻价格则vector不为空
	 *
	 * 版本 >= 0
	 */
	 private Vector<Long> colorPrice = new Vector<Long>();

	/**
	 * 活动价格
	 *
	 * 版本 >= 0
	 */
	 private long activityPrice;

	/**
	 * 活动类型
	 *
	 * 版本 >= 0
	 */
	 private long activityType;

	/**
	 * 活动信息描述
	 *
	 * 版本 >= 0
	 */
	 private String activityDesc = new String();

	/**
	 * 活动的当前状态:0-活动尚未开始，1-活动已经开始
	 *
	 * 版本 >= 0
	 */
	 private long state;

	/**
	 * 活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 *
	 * 版本 >= 0
	 */
	 private long beginTime;

	/**
	 * 活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 *
	 * 版本 >= 0
	 */
	 private long endTime;

	/**
	 * 店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 *
	 * 版本 >= 0
	 */
	 private Vector<Long> shopVipPrice = new Vector<Long>();

	/**
	 * 活动信息是否需要显示
	 *
	 * 版本 >= 0
	 */
	 private boolean isActivityNeedShow;

	/**
	 * 活动属性信息
	 *
	 * 版本 >= 0
	 */
	 private long activeProperty;

	/**
	 * 备用字段版本
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs) - 4);
		bs.pushInt(version);
		bs.pushObject(colorPrice);
		bs.pushUInt(activityPrice);
		bs.pushUInt(activityType);
		bs.pushString(activityDesc);
		bs.pushUInt(state);
		bs.pushUInt(beginTime);
		bs.pushUInt(endTime);
		bs.pushObject(shopVipPrice);
		bs.pushBoolean(isActivityNeedShow);
		bs.pushUInt(activeProperty);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		colorPrice = (Vector<Long>)bs.popVector(Long.class);
		activityPrice = bs.popUInt();
		activityType = bs.popUInt();
		activityDesc = bs.popString();
		state = bs.popUInt();
		beginTime = bs.popUInt();
		endTime = bs.popUInt();
		shopVipPrice = (Vector<Long>)bs.popVector(Long.class);
		isActivityNeedShow = bs.popBoolean();
		activeProperty = bs.popUInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取彩钻价格，如果有彩钻价格则vector不为空
	 * 
	 * 此字段的版本 >= 0
	 * @return colorPrice value 类型为:Vector<Long>
	 * 
	 */
	public Vector<Long> getColorPrice()
	{
		return colorPrice;
	}


	/**
	 * 设置彩钻价格，如果有彩钻价格则vector不为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Long>
	 * 
	 */
	public void setColorPrice(Vector<Long> value)
	{
		if (value != null) {
				this.colorPrice = value;
		}else{
				this.colorPrice = new Vector<Long>();
		}
	}


	/**
	 * 获取活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @return activityPrice value 类型为:long
	 * 
	 */
	public long getActivityPrice()
	{
		return activityPrice;
	}


	/**
	 * 设置活动价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityPrice(long value)
	{
		this.activityPrice = value;
	}


	/**
	 * 获取活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @return activityType value 类型为:long
	 * 
	 */
	public long getActivityType()
	{
		return activityType;
	}


	/**
	 * 设置活动类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActivityType(long value)
	{
		this.activityType = value;
	}


	/**
	 * 获取活动信息描述
	 * 
	 * 此字段的版本 >= 0
	 * @return activityDesc value 类型为:String
	 * 
	 */
	public String getActivityDesc()
	{
		return activityDesc;
	}


	/**
	 * 设置活动信息描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setActivityDesc(String value)
	{
		this.activityDesc = value;
	}


	/**
	 * 获取活动的当前状态:0-活动尚未开始，1-活动已经开始
	 * 
	 * 此字段的版本 >= 0
	 * @return state value 类型为:long
	 * 
	 */
	public long getState()
	{
		return state;
	}


	/**
	 * 设置活动的当前状态:0-活动尚未开始，1-活动已经开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.state = value;
	}


	/**
	 * 获取活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @return beginTime value 类型为:long
	 * 
	 */
	public long getBeginTime()
	{
		return beginTime;
	}


	/**
	 * 设置活动的开始时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBeginTime(long value)
	{
		this.beginTime = value;
	}


	/**
	 * 获取活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @return endTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return endTime;
	}


	/**
	 * 设置活动的结束时间，为uinxi_time_stamp的格式，单位为秒
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.endTime = value;
	}


	/**
	 * 获取店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 * 
	 * 此字段的版本 >= 0
	 * @return shopVipPrice value 类型为:Vector<Long>
	 * 
	 */
	public Vector<Long> getShopVipPrice()
	{
		return shopVipPrice;
	}


	/**
	 * 设置店铺VIP价格列表,如果商品不支持店铺VIP，则本Vector的为空；如果支持店铺VIP，则本Vector的size应为4，分别保存会员价，黄金会员价，白金会员价，钻石会员价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<Long>
	 * 
	 */
	public void setShopVipPrice(Vector<Long> value)
	{
		if (value != null) {
				this.shopVipPrice = value;
		}else{
				this.shopVipPrice = new Vector<Long>();
		}
	}


	/**
	 * 获取活动信息是否需要显示
	 * 
	 * 此字段的版本 >= 0
	 * @return isActivityNeedShow value 类型为:boolean
	 * 
	 */
	public boolean getIsActivityNeedShow()
	{
		return isActivityNeedShow;
	}


	/**
	 * 设置活动信息是否需要显示
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:boolean
	 * 
	 */
	public void setIsActivityNeedShow(boolean value)
	{
		this.isActivityNeedShow = value;
	}


	/**
	 * 获取活动属性信息
	 * 
	 * 此字段的版本 >= 0
	 * @return activeProperty value 类型为:long
	 * 
	 */
	public long getActiveProperty()
	{
		return activeProperty;
	}


	/**
	 * 设置活动属性信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setActiveProperty(long value)
	{
		this.activeProperty = value;
	}


	/**
	 * 获取备用字段版本
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPriceExtPo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(colorPrice);  //计算字段colorPrice的长度 size_of(Vector)
				length += 4;  //计算字段activityPrice的长度 size_of(long)
				length += 4;  //计算字段activityType的长度 size_of(long)
				length += ByteStream.getObjectSize(activityDesc);  //计算字段activityDesc的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(long)
				length += 4;  //计算字段beginTime的长度 size_of(long)
				length += 4;  //计算字段endTime的长度 size_of(long)
				length += ByteStream.getObjectSize(shopVipPrice);  //计算字段shopVipPrice的长度 size_of(Vector)
				length += 1;  //计算字段isActivityNeedShow的长度 size_of(boolean)
				length += 4;  //计算字段activeProperty的长度 size_of(long)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(ByteStream bs)
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPriceExtPo)
				length += 4;  //计算字段version的长度 size_of(int)
				length += ByteStream.getObjectSize(colorPrice);  //计算字段colorPrice的长度 size_of(Vector)
				length += 4;  //计算字段activityPrice的长度 size_of(long)
				length += 4;  //计算字段activityType的长度 size_of(long)
				length += 4 + activityDesc.getBytes(bs.getDecodeCharset()).length;  //计算字段activityDesc的长度 size_of(String)
				length += 4;  //计算字段state的长度 size_of(long)
				length += 4;  //计算字段beginTime的长度 size_of(long)
				length += 4;  //计算字段endTime的长度 size_of(long)
				length += ByteStream.getObjectSize(shopVipPrice);  //计算字段shopVipPrice的长度 size_of(Vector)
				length += 1;  //计算字段isActivityNeedShow的长度 size_of(boolean)
				length += 4;  //计算字段activeProperty的长度 size_of(long)
				length += 4 + reserved.getBytes(bs.getDecodeCharset()).length;  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
