 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.EvalIdl.java

package com.qq.qqbuy.thirdparty.idl.item.protocol.eval;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *商品评价列表返回
 *
 *@date 2012-12-17 10:47:50
 *
 *@since version:0
*/
public class  GetComdyEvalListResp implements IServiceObject
{
	public long result;
	/**
	 * 评价信息poList
	 *
	 * 版本 >= 0
	 */
	 private Vector<EvalRecordPo> EvalInfoList = new Vector<EvalRecordPo>();

	/**
	 * 总数
	 *
	 * 版本 >= 0
	 */
	 private long TotalNum;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 保留输出
	 *
	 * 版本 >= 0
	 */
	 private String ResveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(EvalInfoList);
		bs.pushUInt(TotalNum);
		bs.pushString(ErrMsg);
		bs.pushString(ResveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		EvalInfoList = (Vector<EvalRecordPo>)bs.popVector(EvalRecordPo.class);
		TotalNum = bs.popUInt();
		ErrMsg = bs.popString();
		ResveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23158806L;
	}


	/**
	 * 获取评价信息poList
	 * 
	 * 此字段的版本 >= 0
	 * @return EvalInfoList value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public Vector<EvalRecordPo> getEvalInfoList()
	{
		return EvalInfoList;
	}


	/**
	 * 设置评价信息poList
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<EvalRecordPo>
	 * 
	 */
	public void setEvalInfoList(Vector<EvalRecordPo> value)
	{
		if (value != null) {
				this.EvalInfoList = value;
		}else{
				this.EvalInfoList = new Vector<EvalRecordPo>();
		}
	}


	/**
	 * 获取总数
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return TotalNum;
	}


	/**
	 * 设置总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.TotalNum = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取保留输出
	 * 
	 * 此字段的版本 >= 0
	 * @return ResveOut value 类型为:String
	 * 
	 */
	public String getResveOut()
	{
		return ResveOut;
	}


	/**
	 * 设置保留输出
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setResveOut(String value)
	{
		this.ResveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetComdyEvalListResp)
				length += ByteStream.getObjectSize(EvalInfoList);  //计算字段EvalInfoList的长度 size_of(Vector)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ResveOut);  //计算字段ResveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
