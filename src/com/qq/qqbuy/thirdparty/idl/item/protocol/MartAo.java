

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.item.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class MartAo  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x87481002L, desc = "卖场快车商品详细信息接口", export = true)
	 public GetTabDetailResp getTabDetail(GetTabDetailReq req){
		GetTabDetailResp resp = new  GetTabDetailResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x87481001L, desc = "卖场快车活动数据曝光接口", export = true)
	 public getActiveItemListResp getActiveItemList(getActiveItemListReq req){
		getActiveItemListResp resp = new  getActiveItemListResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
