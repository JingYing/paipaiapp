package com.qq.qqbuy.thirdparty.idl.item.protocol;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ApiDownLoadDescResp implements IServiceObject {
    public long result;
    private String descFileContent = new String();

    private String errmsg = new String();

    private String reserve = new String();

    public int Serialize(ByteStream bs) throws Exception {
        bs.pushUInt(this.result);
        bs.pushString(this.descFileContent);
        bs.pushString(this.errmsg);
        bs.pushString(this.reserve);
        return bs.getWrittenLength();
    }

    public int UnSerialize(ByteStream bs) throws Exception {
        this.result = bs.popUInt();
        this.descFileContent = bs.popString();
        this.errmsg = bs.popString();
        this.reserve = bs.popString();
        return bs.getReadLength();
    }

    public long getCmdId() {
        return 1763477505L;
    }

    public String getDescFileContent() {
        return this.descFileContent;
    }

    public void setDescFileContent(String value) {
        this.descFileContent = value;
    }

    public String getErrmsg() {
        return this.errmsg;
    }

    public void setErrmsg(String value) {
        this.errmsg = value;
    }

    public String getReserve() {
        return this.reserve;
    }

    public void setReserve(String value) {
        this.reserve = value;
    }

    protected int getClassSize() {
        return getSize() - 4;
    }

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    public int getSize() {
        int length = 4;
        try {
            length = 4;
            length += ByteStream.getObjectSize(this.descFileContent);
            length += ByteStream.getObjectSize(this.errmsg);
            length += ByteStream.getObjectSize(this.reserve);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return length;
    }
}