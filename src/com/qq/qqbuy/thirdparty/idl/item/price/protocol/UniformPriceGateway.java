

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class UniformPriceGateway  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x52301801L, desc = "获取商品价格体系的有效价格", export = true)
	 public GetCommodityListPriceResp GetCommodityListPrice(GetCommodityListPriceReq req){
		GetCommodityListPriceResp resp = new  GetCommodityListPriceResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
