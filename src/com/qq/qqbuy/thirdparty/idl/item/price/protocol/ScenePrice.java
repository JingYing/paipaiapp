//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: item.UniformPriceGateway.java

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品单价结构体 
 *
 *@date 2015-04-21 04:22:25
 *
 *@since version:20150309
*/
public class ScenePrice  implements ICanSerializeObject
{
	/**
	 * 商品多价场景类型 1原价 4彩钻价 5店铺vip价 41特价 43coss活动价 44拍便宜 45限时折扣 46大促价 105微店价 106批发价 107大V价 108大V优惠价 111单人团价
	 *
	 * 版本 >= 0
	 */
	 private long id;

	/**
	 * 多价类型价格
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 多价类型身份等级
	 *
	 * 版本 >= 0
	 */
	 private long level;

	/**
	 * 类型名称
	 *
	 * 版本 >= 0
	 */
	 private String name = new String();

	/**
	 * 类型描述
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 参考价格id
	 *
	 * 版本 >= 0
	 */
	 private long basePriceId;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(id);
		bs.pushUInt(price);
		bs.pushUInt(level);
		bs.pushString(name);
		bs.pushString(desc);
		bs.pushUInt(basePriceId);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		id = bs.popUInt();
		price = bs.popUInt();
		level = bs.popUInt();
		name = bs.popString();
		desc = bs.popString();
		basePriceId = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品多价场景类型 1原价 4彩钻价 5店铺vip价 41特价 43coss活动价 44拍便宜 45限时折扣 46大促价 105微店价 106批发价 107大V价 108大V优惠价 111单人团价
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return id;
	}


	/**
	 * 设置商品多价场景类型 1原价 4彩钻价 5店铺vip价 41特价 43coss活动价 44拍便宜 45限时折扣 46大促价 105微店价 106批发价 107大V价 108大V优惠价 111单人团价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.id = value;
	}


	/**
	 * 获取多价类型价格
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置多价类型价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
	}


	/**
	 * 获取多价类型身份等级
	 * 
	 * 此字段的版本 >= 0
	 * @return level value 类型为:long
	 * 
	 */
	public long getLevel()
	{
		return level;
	}


	/**
	 * 设置多价类型身份等级
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLevel(long value)
	{
		this.level = value;
	}


	/**
	 * 获取类型名称
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置类型名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
	}


	/**
	 * 获取类型描述
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置类型描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
	}


	/**
	 * 获取参考价格id
	 * 
	 * 此字段的版本 >= 0
	 * @return basePriceId value 类型为:long
	 * 
	 */
	public long getBasePriceId()
	{
		return basePriceId;
	}


	/**
	 * 设置参考价格id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBasePriceId(long value)
	{
		this.basePriceId = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ScenePrice)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += 4;  //计算字段level的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(name);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(desc);  //计算字段desc的长度 size_of(String)
				length += 4;  //计算字段basePriceId的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
