//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: item.UniformPriceGateway.java

package com.qq.qqbuy.thirdparty.idl.item.price.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *商品库存价格结构体 
 *
 *@date 2015-04-21 04:22:25
 *
 *@since version:20150309
*/
public class SkuPrice  implements ICanSerializeObject
{
	/**
	 * 商品库存skuid
	 *
	 * 版本 >= 0
	 */
	 private long id;

	/**
	 * 库存价格
	 *
	 * 版本 >= 0
	 */
	 private long price;

	/**
	 * 库存价格列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ScenePrice> scenePriceList = new Vector<ScenePrice>();

	/**
	 * 推荐价格
	 *
	 * 版本 >= 0
	 */
	 private ScenePrice recommendedScenePrice = new ScenePrice();

	/**
	 * 商品库存描述
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushLong(id);
		bs.pushUInt(price);
		bs.pushObject(scenePriceList);
		bs.pushObject(recommendedScenePrice);
		bs.pushString(desc);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		id = bs.popLong();
		price = bs.popUInt();
		scenePriceList = (Vector<ScenePrice>)bs.popVector(ScenePrice.class);
		recommendedScenePrice = (ScenePrice) bs.popObject(ScenePrice.class);
		desc = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品库存skuid
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return id;
	}


	/**
	 * 设置商品库存skuid
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.id = value;
	}


	/**
	 * 获取库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @return price value 类型为:long
	 * 
	 */
	public long getPrice()
	{
		return price;
	}


	/**
	 * 设置库存价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPrice(long value)
	{
		this.price = value;
	}


	/**
	 * 获取库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @return scenePriceList value 类型为:Vector<ScenePrice>
	 * 
	 */
	public Vector<ScenePrice> getScenePriceList()
	{
		return scenePriceList;
	}


	/**
	 * 设置库存价格列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ScenePrice>
	 * 
	 */
	public void setScenePriceList(Vector<ScenePrice> value)
	{
		if (value != null) {
				this.scenePriceList = value;
		}else{
				this.scenePriceList = new Vector<ScenePrice>();
		}
	}


	/**
	 * 获取推荐价格
	 * 
	 * 此字段的版本 >= 0
	 * @return recommendedScenePrice value 类型为:ScenePrice
	 * 
	 */
	public ScenePrice getRecommendedScenePrice()
	{
		return recommendedScenePrice;
	}


	/**
	 * 设置推荐价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ScenePrice
	 * 
	 */
	public void setRecommendedScenePrice(ScenePrice value)
	{
		if (value != null) {
				this.recommendedScenePrice = value;
		}else{
				this.recommendedScenePrice = new ScenePrice();
		}
	}


	/**
	 * 获取商品库存描述
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置商品库存描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SkuPrice)
				length += 17;  //计算字段id的长度 size_of(uint64_t)
				length += 4;  //计算字段price的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(scenePriceList);  //计算字段scenePriceList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(recommendedScenePrice);  //计算字段recommendedScenePrice的长度 size_of(ScenePrice)
				length += ByteStream.getObjectSize(desc);  //计算字段desc的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
