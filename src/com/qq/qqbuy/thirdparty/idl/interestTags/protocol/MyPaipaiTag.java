//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.getTagInfoResp.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *标签对象
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class MyPaipaiTag  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 协议版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 20140613;

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 标签ID
	 *
	 * 版本 >= 0
	 */
	 private long dwTagId;

	/**
	 * 版本 >= 0
	 */
	 private short dwTagId_u;

	/**
	 * 标签名称
	 *
	 * 版本 >= 0
	 */
	 private String strTagName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short strTagName_u;

	/**
	 * 标签映射商品池ID列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> poolIds = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short poolIds_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUByte(version_u);
		bs.pushUInt(dwTagId);
		bs.pushUByte(dwTagId_u);
		bs.pushString(strTagName);
		bs.pushUByte(strTagName_u);
		bs.pushObject(poolIds);
		bs.pushUByte(poolIds_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		version_u = bs.popUByte();
		dwTagId = bs.popUInt();
		dwTagId_u = bs.popUByte();
		strTagName = bs.popString();
		strTagName_u = bs.popUByte();
		poolIds = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		poolIds_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置协议版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取标签ID
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTagId value 类型为:long
	 * 
	 */
	public long getDwTagId()
	{
		return dwTagId;
	}


	/**
	 * 设置标签ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwTagId(long value)
	{
		this.dwTagId = value;
		this.dwTagId_u = 1;
	}

	public boolean issetDwTagId()
	{
		return this.dwTagId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwTagId_u value 类型为:short
	 * 
	 */
	public short getDwTagId_u()
	{
		return dwTagId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDwTagId_u(short value)
	{
		this.dwTagId_u = value;
	}


	/**
	 * 获取标签名称
	 * 
	 * 此字段的版本 >= 0
	 * @return strTagName value 类型为:String
	 * 
	 */
	public String getStrTagName()
	{
		return strTagName;
	}


	/**
	 * 设置标签名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrTagName(String value)
	{
		this.strTagName = value;
		this.strTagName_u = 1;
	}

	public boolean issetStrTagName()
	{
		return this.strTagName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strTagName_u value 类型为:short
	 * 
	 */
	public short getStrTagName_u()
	{
		return strTagName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStrTagName_u(short value)
	{
		this.strTagName_u = value;
	}


	/**
	 * 获取标签映射商品池ID列表
	 * 
	 * 此字段的版本 >= 0
	 * @return poolIds value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getPoolIds()
	{
		return poolIds;
	}


	/**
	 * 设置标签映射商品池ID列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setPoolIds(Vector<uint32_t> value)
	{
		if (value != null) {
				this.poolIds = value;
				this.poolIds_u = 1;
		}
	}

	public boolean issetPoolIds()
	{
		return this.poolIds_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return poolIds_u value 类型为:short
	 * 
	 */
	public short getPoolIds_u()
	{
		return poolIds_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPoolIds_u(short value)
	{
		this.poolIds_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiTag)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwTagId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwTagId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strTagName, null);  //计算字段strTagName的长度 size_of(String)
				length += 1;  //计算字段strTagName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(poolIds, null);  //计算字段poolIds的长度 size_of(Vector)
				length += 1;  //计算字段poolIds_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(MyPaipaiTag)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段dwTagId的长度 size_of(uint32_t)
				length += 1;  //计算字段dwTagId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(strTagName, encoding);  //计算字段strTagName的长度 size_of(String)
				length += 1;  //计算字段strTagName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(poolIds, encoding);  //计算字段poolIds的长度 size_of(Vector)
				length += 1;  //计算字段poolIds_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
