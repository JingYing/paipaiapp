 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.opr.ao.idl.MyPaipaiAo.java

package com.qq.qqbuy.thirdparty.idl.interestTags.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiExt;
import com.qq.qqbuy.thirdparty.idl.interestTags.protocol.MyPaipaiUser;

/**
 *我的拍拍 瀑布流商品信息接口 请求参数
 *
 *@date 2014-09-10 06:49:37
 *
 *@since version:0
*/
public class  GetTagInfoReq extends NetMessage
{
	/**
	 * 来源，传客户端IP或者__FILE__，必须
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 传用户的VisitKey，必须
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 场景ID，必须
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 鉴权Token
	 *
	 * 版本 >= 0
	 */
	 private String token = new String();

	/**
	 * 用户信息
	 *
	 * 版本 >= 0
	 */
	 private MyPaipaiUser userInfo = new MyPaipaiUser();

	/**
	 * 所需查询标签ID 查询全部请留空
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> tagIds = new Vector<uint32_t>();

	/**
	 * 输入扩展
	 *
	 * 版本 >= 0
	 */
	 private MyPaipaiExt extIn = new MyPaipaiExt();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushString(machineKey);
		bs.pushUInt(sceneId);
		bs.pushString(token);
		bs.pushObject(userInfo);
		bs.pushObject(tagIds);
		bs.pushObject(extIn);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		machineKey = bs.popString();
		sceneId = bs.popUInt();
		token = bs.popString();
		userInfo = (MyPaipaiUser)bs.popObject(MyPaipaiUser.class);
		tagIds = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		extIn = (MyPaipaiExt)bs.popObject(MyPaipaiExt.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x87071002L;
	}


	/**
	 * 获取来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源，传客户端IP或者__FILE__，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置传用户的VisitKey，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取场景ID，必须
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景ID，必须
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @return token value 类型为:String
	 * 
	 */
	public String getToken()
	{
		return token;
	}


	/**
	 * 设置鉴权Token
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setToken(String value)
	{
		this.token = value;
	}


	/**
	 * 获取用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @return userInfo value 类型为:MyPaipaiUser
	 * 
	 */
	public MyPaipaiUser getUserInfo()
	{
		return userInfo;
	}


	/**
	 * 设置用户信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MyPaipaiUser
	 * 
	 */
	public void setUserInfo(MyPaipaiUser value)
	{
		if (value != null) {
				this.userInfo = value;
		}else{
				this.userInfo = new MyPaipaiUser();
		}
	}


	/**
	 * 获取所需查询标签ID 查询全部请留空
	 * 
	 * 此字段的版本 >= 0
	 * @return tagIds value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getTagIds()
	{
		return tagIds;
	}


	/**
	 * 设置所需查询标签ID 查询全部请留空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setTagIds(Vector<uint32_t> value)
	{
		if (value != null) {
				this.tagIds = value;
		}else{
				this.tagIds = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取输入扩展
	 * 
	 * 此字段的版本 >= 0
	 * @return extIn value 类型为:MyPaipaiExt
	 * 
	 */
	public MyPaipaiExt getExtIn()
	{
		return extIn;
	}


	/**
	 * 设置输入扩展
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MyPaipaiExt
	 * 
	 */
	public void setExtIn(MyPaipaiExt value)
	{
		if (value != null) {
				this.extIn = value;
		}else{
				this.extIn = new MyPaipaiExt();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(getTagInfoReq)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(token, null);  //计算字段token的长度 size_of(String)
				length += ByteStream.getObjectSize(userInfo, null);  //计算字段userInfo的长度 size_of(MyPaipaiUser)
				length += ByteStream.getObjectSize(tagIds, null);  //计算字段tagIds的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extIn, null);  //计算字段extIn的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(getTagInfoReq)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(token, encoding);  //计算字段token的长度 size_of(String)
				length += ByteStream.getObjectSize(userInfo, encoding);  //计算字段userInfo的长度 size_of(MyPaipaiUser)
				length += ByteStream.getObjectSize(tagIds, encoding);  //计算字段tagIds的长度 size_of(Vector)
				length += ByteStream.getObjectSize(extIn, encoding);  //计算字段extIn的长度 size_of(MyPaipaiExt)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
