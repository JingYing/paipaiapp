 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *判断用户是否有场景vector所需的权限请求类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  CheckMultiAuthReq implements IServiceObject
{
	/**
	 * 员工qq号
	 *
	 * 版本 >= 0
	 */
	 private long employeeUin;

	/**
	 * 场景id vector
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> sceneId = new Vector<uint32_t>();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(employeeUin);
		bs.pushObject(sceneId);
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		employeeUin = bs.popUInt();
		sceneId = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1921807L;
	}


	/**
	 * 获取员工qq号
	 * 
	 * 此字段的版本 >= 0
	 * @return employeeUin value 类型为:long
	 * 
	 */
	public long getEmployeeUin()
	{
		return employeeUin;
	}


	/**
	 * 设置员工qq号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEmployeeUin(long value)
	{
		this.employeeUin = value;
	}


	/**
	 * 获取场景id vector
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景id vector
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setSceneId(Vector<uint32_t> value)
	{
		if (value != null) {
				this.sceneId = value;
		}else{
				this.sceneId = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckMultiAuthReq)
				length += 4;  //计算字段employeeUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sceneId, null);  //计算字段sceneId的长度 size_of(Vector)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(CheckMultiAuthReq)
				length += 4;  //计算字段employeeUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sceneId, encoding);  //计算字段sceneId的长度 size_of(Vector)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
