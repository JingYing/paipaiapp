 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *查询某一场景相关信息返回类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  GetSceneInfoResp implements IServiceObject
{
	public long result;
	/**
	 * 场景描述
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 场景类型，当前有两种：隐藏、非隐藏
	 *
	 * 版本 >= 0
	 */
	 private short type;

	/**
	 * 某一场景所需的权限列表
	 *
	 * 版本 >= 0
	 */
	 private String auth = new String();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(desc);
		bs.pushUByte(type);
		bs.pushString(auth);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		desc = bs.popString();
		type = bs.popUByte();
		auth = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1928805L;
	}


	/**
	 * 获取场景描述
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置场景描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
	}


	/**
	 * 获取场景类型，当前有两种：隐藏、非隐藏
	 * 
	 * 此字段的版本 >= 0
	 * @return type value 类型为:short
	 * 
	 */
	public short getType()
	{
		return type;
	}


	/**
	 * 设置场景类型，当前有两种：隐藏、非隐藏
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setType(short value)
	{
		this.type = value;
	}


	/**
	 * 获取某一场景所需的权限列表
	 * 
	 * 此字段的版本 >= 0
	 * @return auth value 类型为:String
	 * 
	 */
	public String getAuth()
	{
		return auth;
	}


	/**
	 * 设置某一场景所需的权限列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAuth(String value)
	{
		this.auth = value;
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetSceneInfoResp)
				length += ByteStream.getObjectSize(desc, null);  //计算字段desc的长度 size_of(String)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(auth, null);  //计算字段auth的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetSceneInfoResp)
				length += ByteStream.getObjectSize(desc, encoding);  //计算字段desc的长度 size_of(String)
				length += 1;  //计算字段type的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(auth, encoding);  //计算字段auth的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
