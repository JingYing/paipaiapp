

//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.annotation.Protocol;
import com.paipai.netframework.kernal.NetAction;


import com.paipai.lang.GenericWrapper;/**
 *
 *NetAction类
 *
 */
public class DealIdl  extends NetAction
{
	@Override
	 public int getSnowslideThresold(){
		// 这里设置防雪崩时间，也就是超时时间值
		  return 2;
	 }




	@Protocol(cmdid = 0x2630181DL, desc = "微购订单卖家强制标记发货买家申请退款订单", export = true)
	 public WGRefundDealMarkShippingResp WGRefundDealMarkShipping(WGRefundDealMarkShippingReq req){
		WGRefundDealMarkShippingResp resp = new  WGRefundDealMarkShippingResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301811L, desc = "微购订单付款成功", export = true)
	 public WGDealPayOkResp WGDealPayOk(WGDealPayOkReq req){
		WGDealPayOkResp resp = new  WGDealPayOkResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301812L, desc = "微购订单标记发货", export = true)
	 public WGDealMarkShippingResp WGDealMarkShipping(WGDealMarkShippingReq req){
		WGDealMarkShippingResp resp = new  WGDealMarkShippingResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301813L, desc = "微购订单确认收货", export = true)
	 public WGDealConfirmRecvResp WGDealConfirmRecv(WGDealConfirmRecvReq req){
		WGDealConfirmRecvResp resp = new  WGDealConfirmRecvResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630181BL, desc = "微购订单：发货前买家取消退款申请", export = true)
	 public WGDealCancelRefundResp WGDealCancelRefund(WGDealCancelRefundReq req){
		WGDealCancelRefundResp resp = new  WGDealCancelRefundResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630181AL, desc = "微购订单：发货前买家申请退款、修改退款协议", export = true)
	 public WGDealApplyRefundResp WGDealApplyRefund(WGDealApplyRefundReq req){
		WGDealApplyRefundResp resp = new  WGDealApplyRefundResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630181CL, desc = "微购订单：发货前卖家同意买家退款申请", export = true)
	 public WGDealAgreeRefundResp WGDealAgreeRefund(WGDealAgreeRefundReq req){
		WGDealAgreeRefundResp resp = new  WGDealAgreeRefundResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301817L, desc = "微购COD订单卖家标记买家签收or拒签", export = true)
	 public WGCODDealSignResp WGCODDealSign(WGCODDealSignReq req){
		WGCODDealSignResp resp = new  WGCODDealSignResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301816L, desc = "微购COD订单卖家标记发货", export = true)
	 public WGCODDealMarkShippingResp WGCODDealMarkShipping(WGCODDealMarkShippingReq req){
		WGCODDealMarkShippingResp resp = new  WGCODDealMarkShippingResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301807L, desc = "更新交易备注", export = true)
	 public UpdateDealNoteResp UpdateDealNote(UpdateDealNoteReq req){
		UpdateDealNoteResp resp = new  UpdateDealNoteResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301801L, desc = "获取订单信息列表", export = true)
	 public SysGetDealInfoListResp SysGetDealInfoList(SysGetDealInfoListReq req){
		SysGetDealInfoListResp resp = new  SysGetDealInfoListResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180cL, desc = "系统获取订单信息", export = true)
	 public SysGetDealInfoResp SysGetDealInfo(SysGetDealInfoReq req){
		SysGetDealInfoResp resp = new  SysGetDealInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301819L, desc = "微购订单卖家改价", export = true)
	 public SellerModifyWGDealPriceResp SellerModifyWGDealPrice(SellerModifyWGDealPriceReq req){
		SellerModifyWGDealPriceResp resp = new  SellerModifyWGDealPriceResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301808L, desc = "卖家改价", export = true)
	 public SellerModifyPriceResp SellerModifyPrice(SellerModifyPriceReq req){
		SellerModifyPriceResp resp = new  SellerModifyPriceResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301803L, desc = "配货中..", export = true)
	 public PrepareGoodsResp PrepareGoods(PrepareGoodsReq req){
		PrepareGoodsResp resp = new  PrepareGoodsResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301802L, desc = "标记缺货", export = true)
	 public MarkStockOutResp MarkStockOut(MarkStockOutReq req){
		MarkStockOutResp resp = new  MarkStockOutResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301806L, desc = "标记发货", export = true)
	 public MarkShippingResp MarkShipping(MarkShippingReq req){
		MarkShippingResp resp = new  MarkShippingResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180bL, desc = "获取订单信息列表new", export = true)
	 public GetDealInfoList2Resp GetDealInfoList2(GetDealInfoList2Req req){
		GetDealInfoList2Resp resp = new  GetDealInfoList2Resp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180aL, desc = "获取订单信息", export = true)
	 public GetDealInfoResp GetDealInfo(GetDealInfoReq req){
		GetDealInfoResp resp = new  GetDealInfoResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180dL, desc = "获取订单drawid", export = true)
	 public GetDealDrawIdResp GetDealDrawId(GetDealDrawIdReq req){
		GetDealDrawIdResp resp = new  GetDealDrawIdResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301805L, desc = " 延迟收货时间", export = true)
	 public DelayRecvTimeResp DelayRecvTime(DelayRecvTimeReq req){
		DelayRecvTimeResp resp = new  DelayRecvTimeResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630181EL, desc = "订单付款成功", export = true)
	 public DealPayOkResp DealPayOk(DealPayOkReq req){
		DealPayOkResp resp = new  DealPayOkResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301810L, desc = "创建订单", export = true)
	 public CreateDealResp CreateDeal(CreateDealReq req){
		CreateDealResp resp = new  CreateDealResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180fL, desc = "带签名的确认收货", export = true)
	 public ConfirmRecvWithCipherResp ConfirmRecvWithCipher(ConfirmRecvWithCipherReq req){
		ConfirmRecvWithCipherResp resp = new  ConfirmRecvWithCipherResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301809L, desc = "确认收货", export = true)
	 public ConfirmRecvResp ConfirmRecv(ConfirmRecvReq req){
		ConfirmRecvResp resp = new  ConfirmRecvResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301815L, desc = "付款后关闭微购订单", export = true)
	 public CloseWGDealPayResp CloseWGDealPay(CloseWGDealPayReq req){
		CloseWGDealPayResp resp = new  CloseWGDealPayResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301814L, desc = "付款前关闭微购订单", export = true)
	 public CloseWGDealResp CloseWGDeal(CloseWGDealReq req){
		CloseWGDealResp resp = new  CloseWGDealResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301818L, desc = "发货前关闭微购COD订单", export = true)
	 public CloseWGCODDealResp CloseWGCODDeal(CloseWGCODDealReq req){
		CloseWGCODDealResp resp = new  CloseWGCODDealResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x26301804L, desc = "取消订单", export = true)
	 public CloseDealResp CloseDeal(CloseDealReq req){
		CloseDealResp resp = new  CloseDealResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }


	@Protocol(cmdid = 0x2630180eL, desc = "自动发货商品c2c退款接口", export = true)
	 public AutoSendRefundResp AutoSendRefund(AutoSendRefundReq req){
		AutoSendRefundResp resp = new  AutoSendRefundResp();
		 //TODO:do request and send resp(处理业务)
		 return resp;
	 }



}
