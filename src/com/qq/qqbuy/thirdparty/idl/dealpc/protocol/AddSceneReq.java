 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *添加场景数据请求类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  AddSceneReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 多工号系统的场景数据
	 *
	 * 版本 >= 0
	 */
	 private SceneInfoPo sceneInfoPo = new SceneInfoPo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushObject(sceneInfoPo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		sceneInfoPo = (SceneInfoPo) bs.popObject(SceneInfoPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1911808L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取多工号系统的场景数据
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneInfoPo value 类型为:SceneInfoPo
	 * 
	 */
	public SceneInfoPo getSceneInfoPo()
	{
		return sceneInfoPo;
	}


	/**
	 * 设置多工号系统的场景数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:SceneInfoPo
	 * 
	 */
	public void setSceneInfoPo(SceneInfoPo value)
	{
		if (value != null) {
				this.sceneInfoPo = value;
		}else{
				this.sceneInfoPo = new SceneInfoPo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddSceneReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(sceneInfoPo, null);  //计算字段sceneInfoPo的长度 size_of(SceneInfoPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(AddSceneReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(sceneInfoPo, encoding);  //计算字段sceneInfoPo的长度 size_of(SceneInfoPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
