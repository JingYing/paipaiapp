 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *修改员工请求类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  ModifyEmployeeReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 员工数据
	 *
	 * 版本 >= 0
	 */
	 private EmployeeInfoPo employeeInfoPo = new EmployeeInfoPo();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		bs.pushObject(employeeInfoPo);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		employeeInfoPo = (EmployeeInfoPo) bs.popObject(EmployeeInfoPo.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1911803L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取员工数据
	 * 
	 * 此字段的版本 >= 0
	 * @return employeeInfoPo value 类型为:EmployeeInfoPo
	 * 
	 */
	public EmployeeInfoPo getEmployeeInfoPo()
	{
		return employeeInfoPo;
	}


	/**
	 * 设置员工数据
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:EmployeeInfoPo
	 * 
	 */
	public void setEmployeeInfoPo(EmployeeInfoPo value)
	{
		if (value != null) {
				this.employeeInfoPo = value;
		}else{
				this.employeeInfoPo = new EmployeeInfoPo();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ModifyEmployeeReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(employeeInfoPo, null);  //计算字段employeeInfoPo的长度 size_of(EmployeeInfoPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(ModifyEmployeeReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(employeeInfoPo, encoding);  //计算字段employeeInfoPo的长度 size_of(EmployeeInfoPo)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
