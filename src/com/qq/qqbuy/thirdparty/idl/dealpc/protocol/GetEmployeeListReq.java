 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *查询卖家下的员工请求类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  GetEmployeeListReq implements IServiceObject
{
	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 员工信息过滤。目前有四种：员工账号、昵称、姓名、角色
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> filter = new HashMap<String,String>();

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(sellerUin);
		bs.pushObject(filter);
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		sellerUin = bs.popUInt();
		filter = (Map<String,String>)bs.popMap(String.class,String.class);
		machineKey = bs.popString();
		source = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1921801L;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取员工信息过滤。目前有四种：员工账号、昵称、姓名、角色
	 * 
	 * 此字段的版本 >= 0
	 * @return filter value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getFilter()
	{
		return filter;
	}


	/**
	 * 设置员工信息过滤。目前有四种：员工账号、昵称、姓名、角色
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setFilter(Map<String,String> value)
	{
		if (value != null) {
				this.filter = value;
		}else{
				this.filter = new HashMap<String,String>();
		}
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetEmployeeListReq)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(filter, null);  //计算字段filter的长度 size_of(Map)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetEmployeeListReq)
				length += 4;  //计算字段sellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(filter, encoding);  //计算字段filter的长度 size_of(Map)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
