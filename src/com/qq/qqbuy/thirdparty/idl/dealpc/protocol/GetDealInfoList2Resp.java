 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Vector;

/**
 *获取订单信息列表new返回
 *
 *@date 2015-03-04 11:46:41
 *
 *@since version:0
*/
public class  GetDealInfoList2Resp extends NetMessage
{
	/**
	 * 订单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<CDealInfo> oDealInfoList = new Vector<CDealInfo>();

	/**
	 * 符合条件总数
	 *
	 * 版本 >= 0
	 */
	 private long TotalNum;

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(oDealInfoList);
		bs.pushUInt(TotalNum);
		bs.pushString(ErrMsg);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		oDealInfoList = (Vector<CDealInfo>)bs.popVector(CDealInfo.class);
		TotalNum = bs.popUInt();
		ErrMsg = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x2630880bL;
	}


	/**
	 * 获取订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return oDealInfoList value 类型为:Vector<CDealInfo>
	 * 
	 */
	public Vector<CDealInfo> getODealInfoList()
	{
		return oDealInfoList;
	}


	/**
	 * 设置订单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<CDealInfo>
	 * 
	 */
	public void setODealInfoList(Vector<CDealInfo> value)
	{
		if (value != null) {
				this.oDealInfoList = value;
		}else{
				this.oDealInfoList = new Vector<CDealInfo>();
		}
	}


	/**
	 * 获取符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @return TotalNum value 类型为:long
	 * 
	 */
	public long getTotalNum()
	{
		return TotalNum;
	}


	/**
	 * 设置符合条件总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalNum(long value)
	{
		this.TotalNum = value;
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealInfoList2Resp)
				length += ByteStream.getObjectSize(oDealInfoList, null);  //计算字段oDealInfoList的长度 size_of(Vector)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ErrMsg, null);  //计算字段ErrMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealInfoList2Resp)
				length += ByteStream.getObjectSize(oDealInfoList, encoding);  //计算字段oDealInfoList的长度 size_of(Vector)
				length += 4;  //计算字段TotalNum的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(ErrMsg, encoding);  //计算字段ErrMsg的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
