//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *改价
 *
 *@date 2015-03-04 11:46:40
 *
 *@since version:0
*/
public class CTradePrice  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 子单id
	 *
	 * 版本 >= 0
	 */
	 private long TradeId;

	/**
	 * 子单价格
	 *
	 * 版本 >= 0
	 */
	 private int Price;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushLong(TradeId);
		bs.pushInt(Price);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		TradeId = bs.popLong();
		Price = bs.popInt();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取子单id
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeId value 类型为:long
	 * 
	 */
	public long getTradeId()
	{
		return TradeId;
	}


	/**
	 * 设置子单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTradeId(long value)
	{
		this.TradeId = value;
	}


	/**
	 * 获取子单价格
	 * 
	 * 此字段的版本 >= 0
	 * @return Price value 类型为:int
	 * 
	 */
	public int getPrice()
	{
		return Price;
	}


	/**
	 * 设置子单价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPrice(int value)
	{
		this.Price = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CTradePrice)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 4;  //计算字段Price的长度 size_of(int)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CTradePrice)
				length += 17;  //计算字段TradeId的长度 size_of(uint64_t)
				length += 4;  //计算字段Price的长度 size_of(int)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
