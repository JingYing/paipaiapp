//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.deelpc.protocol.DealIdl.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint64_t;
import java.util.Vector;

/**
 *订单支付信息
 *
 *@date 2015-03-04 11:46:40
 *
 *@since version:0
*/
public class CPayInfo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 子单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> TradeList = new Vector<uint64_t>();

	/**
	 * 卖家号
	 *
	 * 版本 >= 0
	 */
	 private long SellerUin;

	/**
	 * 买家号
	 *
	 * 版本 >= 0
	 */
	 private long BuyerUin;

	/**
	 * 商品总价
	 *
	 * 版本 >= 0
	 */
	 private long SumFee;

	/**
	 * 运费
	 *
	 * 版本 >= 0
	 */
	 private long MailFee;

	/**
	 * 支付现金
	 *
	 * 版本 >= 0
	 */
	 private long FeeCash;

	/**
	 * 支付现金卷
	 *
	 * 版本 >= 0
	 */
	 private long FeeTicket;

	/**
	 * 支付使用积分
	 *
	 * 版本 >= 0
	 */
	 private long FeePoint;

	/**
	 * 支付完成时间
	 *
	 * 版本 >= 0
	 */
	 private long PayTime;

	/**
	 * 统一支付时间
	 *
	 * 版本 >= 0
	 */
	 private long UnipayTime;

	/**
	 * 支付返回时间
	 *
	 * 版本 >= 0
	 */
	 private long PayReturnTime;

	/**
	 * 银行类型
	 *
	 * 版本 >= 0
	 */
	 private long BankType;

	/**
	 * 订单支付ID
	 *
	 * 版本 >= 0
	 */
	 private String DealPayid = new String();

	/**
	 * 支付单ID
	 *
	 * 版本 >= 0
	 */
	 private String Payid = new String();

	/**
	 * open id
	 *
	 * 版本 >= 0
	 */
	 private String Openid = new String();

	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private String Reserve = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushString(DealId);
		bs.pushObject(TradeList);
		bs.pushUInt(SellerUin);
		bs.pushUInt(BuyerUin);
		bs.pushUInt(SumFee);
		bs.pushUInt(MailFee);
		bs.pushUInt(FeeCash);
		bs.pushUInt(FeeTicket);
		bs.pushUInt(FeePoint);
		bs.pushUInt(PayTime);
		bs.pushUInt(UnipayTime);
		bs.pushUInt(PayReturnTime);
		bs.pushUInt(BankType);
		bs.pushString(DealPayid);
		bs.pushString(Payid);
		bs.pushString(Openid);
		bs.pushString(Reserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		DealId = bs.popString();
		TradeList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		SellerUin = bs.popUInt();
		BuyerUin = bs.popUInt();
		SumFee = bs.popUInt();
		MailFee = bs.popUInt();
		FeeCash = bs.popUInt();
		FeeTicket = bs.popUInt();
		FeePoint = bs.popUInt();
		PayTime = bs.popUInt();
		UnipayTime = bs.popUInt();
		PayReturnTime = bs.popUInt();
		BankType = bs.popUInt();
		DealPayid = bs.popString();
		Payid = bs.popString();
		Openid = bs.popString();
		Reserve = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return TradeList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getTradeList()
	{
		return TradeList;
	}


	/**
	 * 设置子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setTradeList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.TradeList = value;
		}else{
				this.TradeList = new Vector<uint64_t>();
		}
	}


	/**
	 * 获取卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @return SellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return SellerUin;
	}


	/**
	 * 设置卖家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.SellerUin = value;
	}


	/**
	 * 获取买家号
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerUin value 类型为:long
	 * 
	 */
	public long getBuyerUin()
	{
		return BuyerUin;
	}


	/**
	 * 设置买家号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBuyerUin(long value)
	{
		this.BuyerUin = value;
	}


	/**
	 * 获取商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @return SumFee value 类型为:long
	 * 
	 */
	public long getSumFee()
	{
		return SumFee;
	}


	/**
	 * 设置商品总价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSumFee(long value)
	{
		this.SumFee = value;
	}


	/**
	 * 获取运费
	 * 
	 * 此字段的版本 >= 0
	 * @return MailFee value 类型为:long
	 * 
	 */
	public long getMailFee()
	{
		return MailFee;
	}


	/**
	 * 设置运费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMailFee(long value)
	{
		this.MailFee = value;
	}


	/**
	 * 获取支付现金
	 * 
	 * 此字段的版本 >= 0
	 * @return FeeCash value 类型为:long
	 * 
	 */
	public long getFeeCash()
	{
		return FeeCash;
	}


	/**
	 * 设置支付现金
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFeeCash(long value)
	{
		this.FeeCash = value;
	}


	/**
	 * 获取支付现金卷
	 * 
	 * 此字段的版本 >= 0
	 * @return FeeTicket value 类型为:long
	 * 
	 */
	public long getFeeTicket()
	{
		return FeeTicket;
	}


	/**
	 * 设置支付现金卷
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFeeTicket(long value)
	{
		this.FeeTicket = value;
	}


	/**
	 * 获取支付使用积分
	 * 
	 * 此字段的版本 >= 0
	 * @return FeePoint value 类型为:long
	 * 
	 */
	public long getFeePoint()
	{
		return FeePoint;
	}


	/**
	 * 设置支付使用积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFeePoint(long value)
	{
		this.FeePoint = value;
	}


	/**
	 * 获取支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayTime value 类型为:long
	 * 
	 */
	public long getPayTime()
	{
		return PayTime;
	}


	/**
	 * 设置支付完成时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayTime(long value)
	{
		this.PayTime = value;
	}


	/**
	 * 获取统一支付时间
	 * 
	 * 此字段的版本 >= 0
	 * @return UnipayTime value 类型为:long
	 * 
	 */
	public long getUnipayTime()
	{
		return UnipayTime;
	}


	/**
	 * 设置统一支付时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUnipayTime(long value)
	{
		this.UnipayTime = value;
	}


	/**
	 * 获取支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @return PayReturnTime value 类型为:long
	 * 
	 */
	public long getPayReturnTime()
	{
		return PayReturnTime;
	}


	/**
	 * 设置支付返回时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPayReturnTime(long value)
	{
		this.PayReturnTime = value;
	}


	/**
	 * 获取银行类型
	 * 
	 * 此字段的版本 >= 0
	 * @return BankType value 类型为:long
	 * 
	 */
	public long getBankType()
	{
		return BankType;
	}


	/**
	 * 设置银行类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBankType(long value)
	{
		this.BankType = value;
	}


	/**
	 * 获取订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @return DealPayid value 类型为:String
	 * 
	 */
	public String getDealPayid()
	{
		return DealPayid;
	}


	/**
	 * 设置订单支付ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealPayid(String value)
	{
		this.DealPayid = value;
	}


	/**
	 * 获取支付单ID
	 * 
	 * 此字段的版本 >= 0
	 * @return Payid value 类型为:String
	 * 
	 */
	public String getPayid()
	{
		return Payid;
	}


	/**
	 * 设置支付单ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPayid(String value)
	{
		this.Payid = value;
	}


	/**
	 * 获取open id
	 * 
	 * 此字段的版本 >= 0
	 * @return Openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return Openid;
	}


	/**
	 * 设置open id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.Openid = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserve value 类型为:String
	 * 
	 */
	public String getReserve()
	{
		return Reserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserve(String value)
	{
		this.Reserve = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CPayInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId, null);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeList, null);  //计算字段TradeList的长度 size_of(Vector)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段FeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段FeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段FeePoint的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段UnipayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BankType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealPayid, null);  //计算字段DealPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Payid, null);  //计算字段Payid的长度 size_of(String)
				length += ByteStream.getObjectSize(Openid, null);  //计算字段Openid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, null);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CPayInfo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealId, encoding);  //计算字段DealId的长度 size_of(String)
				length += ByteStream.getObjectSize(TradeList, encoding);  //计算字段TradeList的长度 size_of(Vector)
				length += 4;  //计算字段SellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段BuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段SumFee的长度 size_of(uint32_t)
				length += 4;  //计算字段MailFee的长度 size_of(uint32_t)
				length += 4;  //计算字段FeeCash的长度 size_of(uint32_t)
				length += 4;  //计算字段FeeTicket的长度 size_of(uint32_t)
				length += 4;  //计算字段FeePoint的长度 size_of(uint32_t)
				length += 4;  //计算字段PayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段UnipayTime的长度 size_of(uint32_t)
				length += 4;  //计算字段PayReturnTime的长度 size_of(uint32_t)
				length += 4;  //计算字段BankType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(DealPayid, encoding);  //计算字段DealPayid的长度 size_of(String)
				length += ByteStream.getObjectSize(Payid, encoding);  //计算字段Payid的长度 size_of(String)
				length += ByteStream.getObjectSize(Openid, encoding);  //计算字段Openid的长度 size_of(String)
				length += ByteStream.getObjectSize(Reserve, encoding);  //计算字段Reserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
