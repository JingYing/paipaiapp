 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: idl.EmployeeAo.java

package com.qq.qqbuy.thirdparty.idl.dealpc.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Vector;

/**
 *查询多工号系统的权限列表返回类
 *
 *@date 2014-12-16 05:53:03
 *
 *@since version:0
*/
public class  GetAuthListResp implements IServiceObject
{
	public long result;
	/**
	 * 多工号系统的权限列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<AuthInfoPo> authInfoPo = new Vector<AuthInfoPo>();

	/**
	 * 输出保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(authInfoPo);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		authInfoPo = (Vector<AuthInfoPo>)bs.popVector(AuthInfoPo.class);
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1928804L;
	}


	/**
	 * 获取多工号系统的权限列表
	 * 
	 * 此字段的版本 >= 0
	 * @return authInfoPo value 类型为:Vector<AuthInfoPo>
	 * 
	 */
	public Vector<AuthInfoPo> getAuthInfoPo()
	{
		return authInfoPo;
	}


	/**
	 * 设置多工号系统的权限列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<AuthInfoPo>
	 * 
	 */
	public void setAuthInfoPo(Vector<AuthInfoPo> value)
	{
		if (value != null) {
				this.authInfoPo = value;
		}else{
				this.authInfoPo = new Vector<AuthInfoPo>();
		}
	}


	/**
	 * 获取输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置输出保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetAuthListResp)
				length += ByteStream.getObjectSize(authInfoPo, null);  //计算字段authInfoPo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(outReserve, null);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetAuthListResp)
				length += ByteStream.getObjectSize(authInfoPo, encoding);  //计算字段authInfoPo的长度 size_of(Vector)
				length += ByteStream.getObjectSize(outReserve, encoding);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
