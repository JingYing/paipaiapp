package com.qq.qqbuy.thirdparty.idl.boss;


/**
 * 上报数据到oz
 * @author rengaohe
 * @version 1.0
 * @created 2012-7-24 上午10:11:07
 */
public class DataReportClient {
	/**
	 * 将landingpage <tplId>的曝光数据上报到oz
	 * @param tplId landingpage id
	 * @param lpHoles landingpage坑位数据
	 * @throws Exception 
	 */
//	public static int reportShowDataInLp(String sid,String uin,String tplId,List<LpHole> lpHoles,String basePath) throws Exception{
//		int iRet = -1;
//		BossWebStub2 oBossWebStub2 = new BossWebStub2(DataReportClient.class.getName());
//		Vector<DataserverData> vecDataserver = new Vector<DataserverData>();
//		DataserverBatchReq req = new DataserverBatchReq();
//		long showtime = (new java.util.Date()).getTime()/1000;
//		for(LpHole hole:lpHoles){
//			if(hole.getPoolId() != null && hole.getItem() == null)
//				continue;
//			StringBuilder reportData = new StringBuilder();
//    		DataserverData data = new DataserverData();
//    		data.setUiQQ(518800380);
//    		data.setStrBusiness("focus");
//    		data.setStrType("show");
//    		reportData.append(showtime).append("\t").append(sid).append("\t");
//    		reportData.append(uin).append("\t").append(hole.getResourceId()).append("\t");
//    		if(hole.getType().equals("picBar") || hole.getType().equals("platUrl")){
//    			reportData.append(hole.getUrl()).append("\t");
//    		}else{
//    			String itemCode = hole.getItem().getItemCode();
//    			String url = basePath + "/w/item/detailItem.xhtml?ic=" + itemCode;
//    			reportData.append(url).append("\t");
//    		}
//    		reportData.append(hole.getTraceId()).append("\t\t").append(1).append("\t");
//    		reportData.append(0).append("\t").append(0).append("\t0\t");
//    		data.setStrData(reportData.toString());
//    		vecDataserver.add(data);
//		}
//		req.setVecDataserver2(vecDataserver);
//		iRet = oBossWebStub2.invoke("ds_focusshow", req, new EmptyResponse(req.getCmdId()), 2);
//		return iRet;
//	}
}
