package com.qq.qqbuy.thirdparty.idl.boss.protocol;



import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;


public class CSelfMarketCommitReq implements IBossServiceObject
{
	private long cmdId = 0x00030004;
    private String cmdDesc = "BossSelfMarkManage";
    private long subCmdId = 0;
    
    
    private uint32_t uin = new uint32_t();
    private int type;
    private CBoSlfMktMljActive oBoSlfMktMljActive = new CBoSlfMktMljActive();
    
	@Override
	public int Serialize(ByteStream bs) throws Exception
	{
        bs.pushUInt32_t(uin);
        bs.pushUInt(type);
        bs.pushObject(oBoSlfMktMljActive);
        return bs.getWrittenLength();
	}
    
	@Override
	public String getCmdDesc()
	{
		return cmdDesc;
	}
	@Override
	public long getSubCmdId()
	{
		return subCmdId;
	}
	
	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int UnSerialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public long getCmdId()
	{
		// TODO Auto-generated method stub
		return cmdId;
	}

	
	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	public void setCmdDesc(String cmdDesc)
	{
		this.cmdDesc = cmdDesc;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public uint32_t getUin() 
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}

	public int getType() 
	{
		return type;
	}

	public void setType(int type) 
	{
		this.type = type;
	}

	public CBoSlfMktMljActive getoBoSlfMktMljActive() 
	{
		return oBoSlfMktMljActive;
	}

	public void setoBoSlfMktMljActive(CBoSlfMktMljActive oBoSlfMktMljActive) 
	{
		this.oBoSlfMktMljActive = oBoSlfMktMljActive;
	}
	
	
	
}