package com.qq.qqbuy.thirdparty.idl.boss.protocol;




import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;


public class CSelfMarketCopyReq implements IBossServiceObject
{
	private long cmdId = 0x00030008;
    private String cmdDesc = "BossSelfMarkManage";
    private long subCmdId = 0;
    
    
    private uint32_t uin = new uint32_t();
    private uint32_t activeID = new uint32_t();
    
	@Override
	public int Serialize(ByteStream bs) throws Exception
	{
        bs.pushUInt32_t(uin);
        bs.pushUInt32_t(activeID);
        return bs.getWrittenLength();
	}
    
	@Override
	public String getCmdDesc()
	{
		return cmdDesc;
	}
	@Override
	public long getSubCmdId()
	{
		return subCmdId;
	}
	
	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int UnSerialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public long getCmdId()
	{
		// TODO Auto-generated method stub
		return cmdId;
	}

	
	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	public void setCmdDesc(String cmdDesc)
	{
		this.cmdDesc = cmdDesc;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public uint32_t getUin() 
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}


	public uint32_t getActiveID() 
	{
		return activeID;
	}

	public void setActiveID(uint32_t activeID) 
	{
		this.activeID = activeID;
	}	
} 