 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: gen.DataserverGen.java

package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *dataserver协议批量异步请求结构
 *
 *@date 2012-06-15 02:49:01
 *
 *@since version:0
*/
public class  DataserverSyncBatchReq implements IServiceObject
{
	/**
	 * 版本 >= 0
	 */
	 private Vector<DataserverSyncData> vecDataserver2 = new Vector<DataserverSyncData>();


	public int Serialize(ByteStream bs) throws Exception
	{
		int length = bs.getWrittenLength();   
		bs.pushUInt(0);    
		bs.pushObject(vecDataserver2);
		int endlength = bs.getWrittenLength();  
		bs.skip(-(endlength - length));      
		bs.pushUInt(endlength - length - 4);    
		bs.skip(endlength - length - 4);  
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		int len = (int) bs.popUInt(); 
		int iReadLen = bs.getReadLength(); 
		vecDataserver2 = (Vector<DataserverSyncData>)bs.popVector(DataserverSyncData.class);
		int iReadLen2 = bs.getReadLength();
		int skip = len - (iReadLen2 - iReadLen);
		bs.skip(skip);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x110F0002L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecDataserver2 value 类型为:Vector<DataserverSyncData>
	 * 
	 */
	public Vector<DataserverSyncData> getVecDataserver2()
	{
		return vecDataserver2;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<DataserverSyncData>
	 * 
	 */
	public void setVecDataserver2(Vector<DataserverSyncData> value)
	{
		if (value != null) {
				this.vecDataserver2 = value;
		}else{
				this.vecDataserver2 = new Vector<DataserverSyncData>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(DataserverSyncBatchReq)
				length += ByteStream.getObjectSize(vecDataserver2);  //计算字段vecDataserver2的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
