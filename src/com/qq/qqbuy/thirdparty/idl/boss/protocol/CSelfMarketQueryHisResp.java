package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;



public class CSelfMarketQueryHisResp implements IBossServiceObject
{
	private long cmdId;
    private long subCmdId;
    
    private int sendResult = -1;
    private uint32_t dwTotal = new uint32_t();
    private uint32_t uin = new uint32_t();
    private Vector<CBoSlfMktMljActive> vHistoryActives = new Vector<CBoSlfMktMljActive>();
    
	@Override
	public int Serialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int UnSerialize(ByteStream bs) throws Exception
	{
		this.cmdId = bs.popInt();
        this.sendResult = bs.popInt();
        this.dwTotal = bs.popUint32_t();
        this.uin = bs.popUint32_t();
        this.vHistoryActives = (Vector<CBoSlfMktMljActive>) bs.popVector(CBoSlfMktMljActive.class);
 
		return bs.getReadLength();
	}

	@Override
	public long getCmdId()
	{
		return cmdId;
	}

	public long getSubCmdId()
	{
		return subCmdId;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	@Override
	public String getCmdDesc()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}

	
	public uint32_t getDwTotal() 
	{
		return dwTotal;
	}

	public void setDwTotal(uint32_t dwTotal) 
	{
		this.dwTotal = dwTotal;
	}

	public Vector<CBoSlfMktMljActive> getvHistoryActives() 
	{
		return vHistoryActives;
	}

	public void setvHistoryActives(Vector<CBoSlfMktMljActive> vHistoryActives) 
	{
		this.vHistoryActives = vHistoryActives;
	}

	public uint32_t getUin()
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}

	public int getSendResult() 
	{
		return sendResult;
	}

	public void setSendResult(int sendResult) 
	{
		this.sendResult = sendResult;
	}
}
