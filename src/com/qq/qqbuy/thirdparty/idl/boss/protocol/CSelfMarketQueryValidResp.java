package com.qq.qqbuy.thirdparty.idl.boss.protocol;


import java.util.Vector;

import com.paipai.lang.uint32_t;
import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;



public class CSelfMarketQueryValidResp implements IBossServiceObject
{
	private long cmdId;
    private long subCmdId;
    
    private int sendResult = -1;
    private uint32_t uin = new uint32_t();
    private CSelfMarketActiveInfo oSelfMarketActiveInfo = new CSelfMarketActiveInfo();
    private CBoSlfMktMljActive oCBoSlfMktMljActive = new CBoSlfMktMljActive();
    
    private int Trans()
    {
   
    	oCBoSlfMktMljActive.setDwActiveID(oSelfMarketActiveInfo.getDwActiveID());
    	oCBoSlfMktMljActive.setDwUin(oSelfMarketActiveInfo.getDwUin());
    	oCBoSlfMktMljActive.setsNickName(oSelfMarketActiveInfo.getsNickName());
    	oCBoSlfMktMljActive.setDwUserCredit(oSelfMarketActiveInfo.getDwUserCredit());
    	oCBoSlfMktMljActive.setsShopName(oSelfMarketActiveInfo.getsShopName());
    	oCBoSlfMktMljActive.setsPicUrl(oSelfMarketActiveInfo.getsPicUrl());
    	oCBoSlfMktMljActive.setDwBeginTime(oSelfMarketActiveInfo.getDwBeginTime());
    	oCBoSlfMktMljActive.setDwEndTime(oSelfMarketActiveInfo.getDwEndTime());
    	oCBoSlfMktMljActive.setsCommClassify(oSelfMarketActiveInfo.getsCommClassify());
    	oCBoSlfMktMljActive.setsActiveDesc(oSelfMarketActiveInfo.getsActiveDesc());
    	oCBoSlfMktMljActive.setDwState(oSelfMarketActiveInfo.getDwState());
    	oCBoSlfMktMljActive.setDwSyncFlag(oSelfMarketActiveInfo.getDwSyncFlag());
    	oCBoSlfMktMljActive.setDwCreateFlag(oSelfMarketActiveInfo.getDwCreateFlag());
    	oCBoSlfMktMljActive.setDwCreateTime(oSelfMarketActiveInfo.getDwCreateTime());
    	oCBoSlfMktMljActive.setDwUpdateTime(oSelfMarketActiveInfo.getDwUpdateTime());
    	oCBoSlfMktMljActive.setDwUserProperty(oSelfMarketActiveInfo.getDwUserProperty());
    	oCBoSlfMktMljActive.setDwActiveProperty(oSelfMarketActiveInfo.getDwActiveProperty());
    	oCBoSlfMktMljActive.setDwCommClass(oSelfMarketActiveInfo.getDwCommClass());
    	oCBoSlfMktMljActive.setsDisableReason(oSelfMarketActiveInfo.getsDisableReason());
    	oCBoSlfMktMljActive.setDwActualEndTime(oSelfMarketActiveInfo.getDwActualEndTime());
    	oCBoSlfMktMljActive.setDwFacePerson(oSelfMarketActiveInfo.getDwFacePerson());
    	Vector<CSelfMarkActiveContent> vContents = oSelfMarketActiveInfo.getvContents();
    	Vector<CActiveRecmdInfo> vCRecmds = oSelfMarketActiveInfo.getvCRecmds();
    	Vector<CSelfMarkContentTicket> vTickets  = oSelfMarketActiveInfo.getvTickets();

    	if(vTickets.size() > 0)
	    {
	        for(int i=0; i < vTickets.size(); i++)
	        {
	            for(int j=0; j < vContents.size(); j++)
	            {
	                if(vContents.get(j).dwContentID == vTickets.get(i).dwContentID)
	                {
	                	vContents.get(j).setDwBatchID(vTickets.get(i).getDwBatchID());
	                	vContents.get(j).setDwFaceValue(vTickets.get(i).getDwFaceValue());
					    break;
	                }
	            }
	        }
	    }
	 
    	Vector<CBoSlfMktMljContent> vBoSlfMktMljContent = new Vector<CBoSlfMktMljContent>();
    	for(int i = 0; i < vContents.size(); i++)
    	{
    		CBoSlfMktMljContent oCBoSlfMktMljContent = new CBoSlfMktMljContent();
    	
    		oCBoSlfMktMljContent.setDwContentID(vContents.get(i).getDwContentID());
    		oCBoSlfMktMljContent.setDwActiveID(vContents.get(i).getDwActiveID());
    		oCBoSlfMktMljContent.setDwUin(vContents.get(i).getDwUin());
    		oCBoSlfMktMljContent.setDwInvalidFlag(vContents.get(i).getDwInvalidFlag());
    		oCBoSlfMktMljContent.setDwCostFlag(vContents.get(i).getDwCostFlag());
    		oCBoSlfMktMljContent.setDwCostMoney(vContents.get(i).getDwCostMoney());
    		oCBoSlfMktMljContent.setDwFavorableFlag(vContents.get(i).getDwFavorableFlag());
    		oCBoSlfMktMljContent.setDwFreeMoney(vContents.get(i).getDwFreeMoney());
    		oCBoSlfMktMljContent.setDwFreeRebate(vContents.get(i).getDwFreeRebate());
    		oCBoSlfMktMljContent.setsPresentName(vContents.get(i).getsPresentName());
    		oCBoSlfMktMljContent.setDwPresentType(vContents.get(i).getDwPresentType());
    		oCBoSlfMktMljContent.setsPresentID(vContents.get(i).getsPresentID());
    		oCBoSlfMktMljContent.setsPresentUrl(vContents.get(i).getsPresentUrl());
    		oCBoSlfMktMljContent.setDwBarterMoney(vContents.get(i).getDwBarterMoney());
    		oCBoSlfMktMljContent.setsBarterName(vContents.get(i).getsBarterName());
    		oCBoSlfMktMljContent.setDwBarterType(vContents.get(i).getDwBarterType());
    		oCBoSlfMktMljContent.setsBarterID(vContents.get(i).getsBarterID());
    		oCBoSlfMktMljContent.setsBarterUrl(vContents.get(i).getsBarterUrl());
    		oCBoSlfMktMljContent.setDwFacePerson(vContents.get(i).getDwFacePerson());
    		oCBoSlfMktMljContent.setDwBatchID(vContents.get(i).getDwBatchID());
    		oCBoSlfMktMljContent.setDwFaceValue(vContents.get(i).getDwFaceValue());
    		
    		vBoSlfMktMljContent.add(oCBoSlfMktMljContent);
    		
    	}
    	
    	oCBoSlfMktMljActive.setvContents(vBoSlfMktMljContent);
    	
    
    	return 0;
    }
	@Override
	public int Serialize(ByteStream arg0) throws Exception
	{
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public int UnSerialize(ByteStream bs) throws Exception
	{
		this.cmdId = bs.popInt();
        this.sendResult = bs.popInt();
        this.uin = bs.popUint32_t();
        this.oSelfMarketActiveInfo = (CSelfMarketActiveInfo) bs.popObject(CSelfMarketActiveInfo.class);
    
        Trans();
    
		return bs.getReadLength();
	}

	@Override
	public long getCmdId()
	{
		return cmdId;
	}

	public long getSubCmdId()
	{
		return subCmdId;
	}

	public void setSubCmdId(long subCmdId)
	{
		this.subCmdId = subCmdId;
	}

	public void setCmdId(long cmdId)
	{
		this.cmdId = cmdId;
	}

	@Override
	public String getCmdDesc()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHasResult(boolean arg0)
	{
		// TODO Auto-generated method stub
		
	}

	
	
	public uint32_t getUin()
	{
		return uin;
	}

	public void setUin(uint32_t uin) 
	{
		this.uin = uin;
	}

	public int getSendResult() 
	{
		return sendResult;
	}

	public void setSendResult(int sendResult) 
	{
		this.sendResult = sendResult;
	}
	public CSelfMarketActiveInfo getoSelfMarketActiveInfo() 
	{
		return oSelfMarketActiveInfo;
	}
	public void setoSelfMarketActiveInfo(CSelfMarketActiveInfo oSelfMarketActiveInfo)
	{
		this.oSelfMarketActiveInfo = oSelfMarketActiveInfo;
	}
	public CBoSlfMktMljActive getoCBoSlfMktMljActive() 
	{
		return oCBoSlfMktMljActive;
	}
	public void setoCBoSlfMktMljActive(CBoSlfMktMljActive oCBoSlfMktMljActive) 
	{
		this.oCBoSlfMktMljActive = oCBoSlfMktMljActive;
	}
    @Override
    public String toString()
    {
        return "CSelfMarketQueryValidResp [cmdId=" + cmdId
                + ", oCBoSlfMktMljActive=" + oCBoSlfMktMljActive
                + ", oSelfMarketActiveInfo=" + oSelfMarketActiveInfo
                + ", sendResult=" + sendResult + ", subCmdId=" + subCmdId
                + ", uin=" + uin + "]";
    }
	
	
}
