 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import java.util.Vector;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *获取运费模板列表返回类
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class  ApiGetShippingfeeListResp implements IServiceObject
{
	public long result;
	/**
	 * 运费模板Po列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiShippingfee> apiShippingfeeList = new Vector<ApiShippingfee>();

	/**
	 * 总数
	 *
	 * 版本 >= 0
	 */
	 private long totalCount;

	/**
	 * 错误信息，用于debug
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(apiShippingfeeList);
		bs.pushUInt(totalCount);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		apiShippingfeeList = (Vector<ApiShippingfee>)bs.popVector(ApiShippingfee.class);
		totalCount = bs.popUInt();
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22098801L;
	}


	/**
	 * 获取运费模板Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @return apiShippingfeeList value 类型为:Vector<ApiShippingfee>
	 * 
	 */
	public Vector<ApiShippingfee> getApiShippingfeeList()
	{
		return apiShippingfeeList;
	}


	/**
	 * 设置运费模板Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiShippingfee>
	 * 
	 */
	public void setApiShippingfeeList(Vector<ApiShippingfee> value)
	{
		if (value != null) {
				this.apiShippingfeeList = value;
		}else{
				this.apiShippingfeeList = new Vector<ApiShippingfee>();
		}
	}


	/**
	 * 获取总数
	 * 
	 * 此字段的版本 >= 0
	 * @return totalCount value 类型为:long
	 * 
	 */
	public long getTotalCount()
	{
		return totalCount;
	}


	/**
	 * 设置总数
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setTotalCount(long value)
	{
		this.totalCount = value;
	}


	/**
	 * 获取错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiGetShippingfeeListResp)
				length += ByteStream.getObjectSize(apiShippingfeeList);  //计算字段apiShippingfeeList的长度 size_of(Vector)
				length += 4;  //计算字段totalCount的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
