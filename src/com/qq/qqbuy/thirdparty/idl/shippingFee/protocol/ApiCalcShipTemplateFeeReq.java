 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *根据运费模板计算运费
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class  ApiCalcShipTemplateFeeReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id,预留
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 收货地址id
	 *
	 * 版本 >= 0
	 */
	 private long recvRegId;

	/**
	 * 运费模板所属卖家QQ号(需要和操作者QQ一致)
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 计算运费相关信息
	 *
	 * 版本 >= 0
	 */
	 private ApiShipInfoList apiShipInfoListIn = new ApiShipInfoList();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushUInt(recvRegId);
		bs.pushUInt(uin);
		bs.pushObject(apiShipInfoListIn);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		recvRegId = bs.popUInt();
		uin = bs.popUInt();
		apiShipInfoListIn = (ApiShipInfoList) bs.popObject(ApiShipInfoList.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22091803L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取收货地址id
	 * 
	 * 此字段的版本 >= 0
	 * @return recvRegId value 类型为:long
	 * 
	 */
	public long getRecvRegId()
	{
		return recvRegId;
	}


	/**
	 * 设置收货地址id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRecvRegId(long value)
	{
		this.recvRegId = value;
	}


	/**
	 * 获取运费模板所属卖家QQ号(需要和操作者QQ一致)
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置运费模板所属卖家QQ号(需要和操作者QQ一致)
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取计算运费相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @return apiShipInfoListIn value 类型为:ApiShipInfoList
	 * 
	 */
	public ApiShipInfoList getApiShipInfoListIn()
	{
		return apiShipInfoListIn;
	}


	/**
	 * 设置计算运费相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiShipInfoList
	 * 
	 */
	public void setApiShipInfoListIn(ApiShipInfoList value)
	{
		if (value != null) {
				this.apiShipInfoListIn = value;
		}else{
				this.apiShipInfoListIn = new ApiShipInfoList();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiCalcShipTemplateFeeReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += 4;  //计算字段recvRegId的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(apiShipInfoListIn);  //计算字段apiShipInfoListIn的长度 size_of(ApiShipInfoList)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
