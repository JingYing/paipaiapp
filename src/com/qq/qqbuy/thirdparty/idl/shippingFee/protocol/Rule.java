 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *规则信息
 *
 *@date 2012-12-26 02:59::35
 *
 *@since version:0
*/
public class Rule  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private short cType;

	/**
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecDest = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private long dwProperty;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceNormal;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceExpress;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceEms;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceNormalAdd;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceExpressAdd;

	/**
	 * 版本 >= 0
	 */
	 private long dwPriceEmsAdd;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUByte(cType);
		bs.pushObject(vecDest);
		bs.pushUInt(dwProperty);
		bs.pushUInt(dwPriceNormal);
		bs.pushUInt(dwPriceExpress);
		bs.pushUInt(dwPriceEms);
		bs.pushUInt(dwPriceNormalAdd);
		bs.pushUInt(dwPriceExpressAdd);
		bs.pushUInt(dwPriceEmsAdd);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		cType = bs.popUByte();
		vecDest = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		dwProperty = bs.popUInt();
		dwPriceNormal = bs.popUInt();
		dwPriceExpress = bs.popUInt();
		dwPriceEms = bs.popUInt();
		dwPriceNormalAdd = bs.popUInt();
		dwPriceExpressAdd = bs.popUInt();
		dwPriceEmsAdd = bs.popUInt();
		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cType value 类型为:short
	 * 
	 */
	public short getCType()
	{
		return cType;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCType(short value)
	{
		this.cType = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecDest value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecDest()
	{
		return vecDest;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecDest(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecDest = value;
		}else{
				this.vecDest = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwProperty value 类型为:long
	 * 
	 */
	public long getDwProperty()
	{
		return dwProperty;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwProperty(long value)
	{
		this.dwProperty = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceNormal value 类型为:long
	 * 
	 */
	public long getDwPriceNormal()
	{
		return dwPriceNormal;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceNormal(long value)
	{
		this.dwPriceNormal = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceExpress value 类型为:long
	 * 
	 */
	public long getDwPriceExpress()
	{
		return dwPriceExpress;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceExpress(long value)
	{
		this.dwPriceExpress = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceEms value 类型为:long
	 * 
	 */
	public long getDwPriceEms()
	{
		return dwPriceEms;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceEms(long value)
	{
		this.dwPriceEms = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceNormalAdd value 类型为:long
	 * 
	 */
	public long getDwPriceNormalAdd()
	{
		return dwPriceNormalAdd;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceNormalAdd(long value)
	{
		this.dwPriceNormalAdd = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceExpressAdd value 类型为:long
	 * 
	 */
	public long getDwPriceExpressAdd()
	{
		return dwPriceExpressAdd;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceExpressAdd(long value)
	{
		this.dwPriceExpressAdd = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPriceEmsAdd value 类型为:long
	 * 
	 */
	public long getDwPriceEmsAdd()
	{
		return dwPriceEmsAdd;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPriceEmsAdd(long value)
	{
		this.dwPriceEmsAdd = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(Rule)
				length += 1;  //计算字段cType的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecDest);  //计算字段vecDest的长度 size_of(Vector)
				length += 4;  //计算字段dwProperty的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceNormal的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceExpress的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceEms的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceNormalAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceExpressAdd的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPriceEmsAdd的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
