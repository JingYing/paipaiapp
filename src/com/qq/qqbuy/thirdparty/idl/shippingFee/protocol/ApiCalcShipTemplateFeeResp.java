 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *根据运费模板计算运费
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class  ApiCalcShipTemplateFeeResp implements IServiceObject
{
	public long result;
	/**
	 * 计算运费相关信息
	 *
	 * 版本 >= 0
	 */
	 private ApiShipInfoList apiShipInfoListOut = new ApiShipInfoList();

	/**
	 * 错误信息，用于debug
	 *
	 * 版本 >= 0
	 */
	 private String errmsg = new String();

	/**
	 * 返回保留字
	 *
	 * 版本 >= 0
	 */
	 private String outReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(apiShipInfoListOut);
		bs.pushString(errmsg);
		bs.pushString(outReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		apiShipInfoListOut = (ApiShipInfoList) bs.popObject(ApiShipInfoList.class);
		errmsg = bs.popString();
		outReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22098803L;
	}


	/**
	 * 获取计算运费相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @return apiShipInfoListOut value 类型为:ApiShipInfoList
	 * 
	 */
	public ApiShipInfoList getApiShipInfoListOut()
	{
		return apiShipInfoListOut;
	}


	/**
	 * 设置计算运费相关信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:ApiShipInfoList
	 * 
	 */
	public void setApiShipInfoListOut(ApiShipInfoList value)
	{
		if (value != null) {
				this.apiShipInfoListOut = value;
		}else{
				this.apiShipInfoListOut = new ApiShipInfoList();
		}
	}


	/**
	 * 获取错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @return errmsg value 类型为:String
	 * 
	 */
	public String getErrmsg()
	{
		return errmsg;
	}


	/**
	 * 设置错误信息，用于debug
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrmsg(String value)
	{
		this.errmsg = value;
	}


	/**
	 * 获取返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserve value 类型为:String
	 * 
	 */
	public String getOutReserve()
	{
		return outReserve;
	}


	/**
	 * 设置返回保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserve(String value)
	{
		this.outReserve = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiCalcShipTemplateFeeResp)
				length += ByteStream.getObjectSize(apiShipInfoListOut);  //计算字段apiShipInfoListOut的长度 size_of(ApiShipInfoList)
				length += ByteStream.getObjectSize(errmsg);  //计算字段errmsg的长度 size_of(String)
				length += ByteStream.getObjectSize(outReserve);  //计算字段outReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
