 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *获取运费模板列表请求类
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class  ApiGetShippingfeeListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id,预留
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 卖家QQ号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 每页大小,最多50
	 *
	 * 版本 >= 0
	 */
	 private long pageSize;

	/**
	 * 页号,从0开始
	 *
	 * 版本 >= 0
	 */
	 private long startIndex;

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushUInt(uin);
		bs.pushUInt(pageSize);
		bs.pushUInt(startIndex);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		uin = bs.popUInt();
		pageSize = bs.popUInt();
		startIndex = bs.popUInt();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22091801L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置卖家QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取每页大小,最多50
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:long
	 * 
	 */
	public long getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置每页大小,最多50
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPageSize(long value)
	{
		this.pageSize = value;
	}


	/**
	 * 获取页号,从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @return startIndex value 类型为:long
	 * 
	 */
	public long getStartIndex()
	{
		return startIndex;
	}


	/**
	 * 设置页号,从0开始
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setStartIndex(long value)
	{
		this.startIndex = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiGetShippingfeeListReq)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += 4;  //计算字段pageSize的长度 size_of(uint32_t)
				length += 4;  //计算字段startIndex的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
