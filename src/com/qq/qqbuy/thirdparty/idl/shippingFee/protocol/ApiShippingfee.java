//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.shippingfee.ShippingfeeApiAo.java

package com.qq.qqbuy.thirdparty.idl.shippingFee.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *运费模板Po
 *
 *@date 2012-12-28 07:19:32
 *
 *@since version:0
*/
public class ApiShippingfee  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long version = 0;

	/**
	 * 自增id
	 *
	 * 版本 >= 0
	 */
	 private long id;

	/**
	 * 运费模板id
	 *
	 * 版本 >= 0
	 */
	 private long innerId;

	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private long property;

	/**
	 * 名称
	 *
	 * 版本 >= 0
	 */
	 private String name = new String();

	/**
	 * 描述信息
	 *
	 * 版本 >= 0
	 */
	 private String desc = new String();

	/**
	 * 创建时间
	 *
	 * 版本 >= 0
	 */
	 private long createTime;

	/**
	 * 最后编辑时间
	 *
	 * 版本 >= 0
	 */
	 private long lastModifyTime;

	/**
	 * 具体运费规则
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiShippingfeeRule> vecRule = new Vector<ApiShippingfeeRule>();

	/**
	 * 版本 >= 0
	 */
	 private short version_u;

	/**
	 * 版本 >= 0
	 */
	 private short id_u;

	/**
	 * 版本 >= 0
	 */
	 private short innerId_u;

	/**
	 * 版本 >= 0
	 */
	 private short property_u;

	/**
	 * 版本 >= 0
	 */
	 private short name_u;

	/**
	 * 版本 >= 0
	 */
	 private short desc_u;

	/**
	 * 版本 >= 0
	 */
	 private short createTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short lastModifyTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short vecRule_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(id);
		bs.pushUInt(innerId);
		bs.pushUInt(property);
		bs.pushString(name);
		bs.pushString(desc);
		bs.pushUInt(createTime);
		bs.pushUInt(lastModifyTime);
		bs.pushObject(vecRule);
		bs.pushUByte(version_u);
		bs.pushUByte(id_u);
		bs.pushUByte(innerId_u);
		bs.pushUByte(property_u);
		bs.pushUByte(name_u);
		bs.pushUByte(desc_u);
		bs.pushUByte(createTime_u);
		bs.pushUByte(lastModifyTime_u);
		bs.pushUByte(vecRule_u);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		id = bs.popUInt();
		innerId = bs.popUInt();
		property = bs.popUInt();
		name = bs.popString();
		desc = bs.popString();
		createTime = bs.popUInt();
		lastModifyTime = bs.popUInt();
		vecRule = (Vector<ApiShippingfeeRule>)bs.popVector(ApiShippingfeeRule.class);
		version_u = bs.popUByte();
		id_u = bs.popUByte();
		innerId_u = bs.popUByte();
		property_u = bs.popUByte();
		name_u = bs.popUByte();
		desc_u = bs.popUByte();
		createTime_u = bs.popUByte();
		lastModifyTime_u = bs.popUByte();
		vecRule_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
		this.version_u = 1;
	}


	/**
	 * 获取自增id
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:long
	 * 
	 */
	public long getId()
	{
		return id;
	}


	/**
	 * 设置自增id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setId(long value)
	{
		this.id = value;
		this.id_u = 1;
	}


	/**
	 * 获取运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @return innerId value 类型为:long
	 * 
	 */
	public long getInnerId()
	{
		return innerId;
	}


	/**
	 * 设置运费模板id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInnerId(long value)
	{
		this.innerId = value;
		this.innerId_u = 1;
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return property;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.property = value;
		this.property_u = 1;
	}


	/**
	 * 获取名称
	 * 
	 * 此字段的版本 >= 0
	 * @return name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return name;
	}


	/**
	 * 设置名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.name = value;
		this.name_u = 1;
	}


	/**
	 * 获取描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @return desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return desc;
	}


	/**
	 * 设置描述信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.desc = value;
		this.desc_u = 1;
	}


	/**
	 * 获取创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime value 类型为:long
	 * 
	 */
	public long getCreateTime()
	{
		return createTime;
	}


	/**
	 * 设置创建时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCreateTime(long value)
	{
		this.createTime = value;
		this.createTime_u = 1;
	}


	/**
	 * 获取最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:long
	 * 
	 */
	public long getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置最后编辑时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLastModifyTime(long value)
	{
		this.lastModifyTime = value;
		this.lastModifyTime_u = 1;
	}


	/**
	 * 获取具体运费规则
	 * 
	 * 此字段的版本 >= 0
	 * @return vecRule value 类型为:Vector<ApiShippingfeeRule>
	 * 
	 */
	public Vector<ApiShippingfeeRule> getVecRule()
	{
		return vecRule;
	}


	/**
	 * 设置具体运费规则
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiShippingfeeRule>
	 * 
	 */
	public void setVecRule(Vector<ApiShippingfeeRule> value)
	{
		if (value != null) {
				this.vecRule = value;
				this.vecRule_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.version_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return id_u value 类型为:short
	 * 
	 */
	public short getId_u()
	{
		return id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setId_u(short value)
	{
		this.id_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return innerId_u value 类型为:short
	 * 
	 */
	public short getInnerId_u()
	{
		return innerId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInnerId_u(short value)
	{
		this.innerId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.property_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return name_u value 类型为:short
	 * 
	 */
	public short getName_u()
	{
		return name_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setName_u(short value)
	{
		this.name_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return desc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.desc_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return createTime_u value 类型为:short
	 * 
	 */
	public short getCreateTime_u()
	{
		return createTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCreateTime_u(short value)
	{
		this.createTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime_u value 类型为:short
	 * 
	 */
	public short getLastModifyTime_u()
	{
		return lastModifyTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLastModifyTime_u(short value)
	{
		this.lastModifyTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecRule_u value 类型为:short
	 * 
	 */
	public short getVecRule_u()
	{
		return vecRule_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVecRule_u(short value)
	{
		this.vecRule_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ApiShippingfee)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段id的长度 size_of(uint32_t)
				length += 4;  //计算字段innerId的长度 size_of(uint32_t)
				length += 4;  //计算字段property的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(name);  //计算字段name的长度 size_of(String)
				length += ByteStream.getObjectSize(desc);  //计算字段desc的长度 size_of(String)
				length += 4;  //计算字段createTime的长度 size_of(uint32_t)
				length += 4;  //计算字段lastModifyTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecRule);  //计算字段vecRule的长度 size_of(Vector)
				length += 1;  //计算字段version_u的长度 size_of(uint8_t)
				length += 1;  //计算字段id_u的长度 size_of(uint8_t)
				length += 1;  //计算字段innerId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段property_u的长度 size_of(uint8_t)
				length += 1;  //计算字段name_u的长度 size_of(uint8_t)
				length += 1;  //计算字段desc_u的长度 size_of(uint8_t)
				length += 1;  //计算字段createTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段lastModifyTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段vecRule_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
