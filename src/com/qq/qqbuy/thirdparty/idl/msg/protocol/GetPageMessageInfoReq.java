 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  GetPageMessageInfoReq implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	/**
	 * 分页大小,必须大于0
	 *
	 * 版本 >= 0
	 */
	 private int pageSize;

	/**
	 * 页码,必须大于0
	 *
	 * 版本 >= 0
	 */
	 private int pageNo;

	/**
	 * 消息状态
	 *
	 * 版本 >= 0
	 */
	 private int status;

	/**
	 * 接收者QQ号
	 *
	 * 版本 >= 0
	 */
	 private long toUin;


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushInt(pageSize);
		bs.pushInt(pageNo);
		bs.pushInt(status);
		bs.pushUInt(toUin);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		pageSize = bs.popInt();
		pageNo = bs.popInt();
		status = bs.popInt();
		toUin = bs.popUInt();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80091802L;
	}


	/**
	 * 获取分页大小,必须大于0
	 * 
	 * 此字段的版本 >= 0
	 * @return pageSize value 类型为:int
	 * 
	 */
	public int getPageSize()
	{
		return pageSize;
	}


	/**
	 * 设置分页大小,必须大于0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageSize(int value)
	{
		this.pageSize = value;
	}


	/**
	 * 获取页码,必须大于0
	 * 
	 * 此字段的版本 >= 0
	 * @return pageNo value 类型为:int
	 * 
	 */
	public int getPageNo()
	{
		return pageNo;
	}


	/**
	 * 设置页码,必须大于0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPageNo(int value)
	{
		this.pageNo = value;
	}


	/**
	 * 获取消息状态
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:int
	 * 
	 */
	public int getStatus()
	{
		return status;
	}


	/**
	 * 设置消息状态
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStatus(int value)
	{
		this.status = value;
	}


	/**
	 * 获取接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return toUin value 类型为:long
	 * 
	 */
	public long getToUin()
	{
		return toUin;
	}


	/**
	 * 设置接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToUin(long value)
	{
		this.toUin = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetPageMessageInfoReq)
				length += 4;  //计算字段pageSize的长度 size_of(int)
				length += 4;  //计算字段pageNo的长度 size_of(int)
				length += 4;  //计算字段status的长度 size_of(int)
				length += 4;  //计算字段toUin的长度 size_of(long)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
