 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  GetMessageCountResp implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 各状态的站内信数量
	 *
	 * 版本 >= 0
	 */
	 private Map<Integer,Integer> statusCount = new HashMap<Integer,Integer>();

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushObject(statusCount);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	@SuppressWarnings("unchecked")
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		statusCount = (Map<Integer,Integer>)bs.popMap(Integer.class,Integer.class);
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80098805L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取各状态的站内信数量
	 * 
	 * 此字段的版本 >= 0
	 * @return statusCount value 类型为:Map<Integer,Integer>
	 * 
	 */
	public Map<Integer,Integer> getStatusCount()
	{
		return statusCount;
	}


	/**
	 * 设置各状态的站内信数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<Integer,Integer>
	 * 
	 */
	public void setStatusCount(Map<Integer,Integer> value)
	{
		if (value != null) {
				this.statusCount = value;
		}else{
				this.statusCount = new HashMap<Integer,Integer>();
		}
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetMessageCountResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(statusCount);  //计算字段statusCount的长度 size_of(Map)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
