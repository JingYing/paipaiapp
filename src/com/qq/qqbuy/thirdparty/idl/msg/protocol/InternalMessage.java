 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.msg.protocol;


import java.io.Serializable;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

/**
 *站内信
 *
 *@date 2011-11-10 09:22::14
 *
 *@since version:0
*/
public class InternalMessage  implements ICanSerializeObject,Serializable
{
	private static final long serialVersionUID = 973614446354105815L;
	/**
	 * 用于记录IDL接口的版本
	 *
	 * 版本 >= 0
	 */
	 private int version = 20110927; 

	/**
	 * 消息ID
	 *
	 * 版本 >= 0
	 */
	 private int id;

	/**
	 * 消息标题
	 *
	 * 版本 >= 0
	 */
	 private String title = new String();

	/**
	 * wap1消息内容
	 *
	 * 版本 >= 0
	 */
	 private String contentWap1 = new String();

	/**
	 * wap2消息内容
	 *
	 * 版本 >= 0
	 */
	 private String contentWap2 = new String();

	/**
	 * 发件人QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long senderUin;

	/**
	 * 发件人名称
	 *
	 * 版本 >= 0
	 */
	 private String senderName = new String();

	/**
	 * 收件人QQ号码
	 *
	 * 版本 >= 0
	 */
	 private long receiverUin;

	/**
	 * 消息发送时间
	 *
	 * 版本 >= 0
	 */
	 private String receivedTime = new String();

	/**
	 * 最后修改时间
	 *
	 * 版本 >= 0
	 */
	 private String lastModifyTime = new String();

	/**
	 * 消息过期时间
	 *
	 * 版本 >= 0
	 */
	 private String expireTime = new String();

	/**
	 * 消息状态,各状态见MessageStatusContant定义
	 *
	 * 版本 >= 0
	 */
	 private int status;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String reserved = new String();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushInt(version);
		bs.pushInt(id);
		bs.pushString(title);
		bs.pushString(contentWap1);
		bs.pushString(contentWap2);
		bs.pushUInt(senderUin);
		bs.pushString(senderName);
		bs.pushUInt(receiverUin);
		bs.pushString(receivedTime);
		bs.pushString(lastModifyTime);
		bs.pushString(expireTime);
		bs.pushInt(status);
		bs.pushString(reserved);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popInt();
		id = bs.popInt();
		title = bs.popString();
		contentWap1 = bs.popString();
		contentWap2 = bs.popString();
		senderUin = bs.popUInt();
		senderName = bs.popString();
		receiverUin = bs.popUInt();
		receivedTime = bs.popString();
		lastModifyTime = bs.popString();
		expireTime = bs.popString();
		status = bs.popInt();
		reserved = bs.popString();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:int
	 * 
	 */
	public int getVersion()
	{
		return version;
	}


	/**
	 * 设置用于记录IDL接口的版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setVersion(int value)
	{
		this.version = value;
	}


	/**
	 * 获取消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @return id value 类型为:int
	 * 
	 */
	public int getId()
	{
		return id;
	}


	/**
	 * 设置消息ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setId(int value)
	{
		this.id = value;
	}


	/**
	 * 获取消息标题
	 * 
	 * 此字段的版本 >= 0
	 * @return title value 类型为:String
	 * 
	 */
	public String getTitle()
	{
		return title;
	}


	/**
	 * 设置消息标题
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTitle(String value)
	{
		this.title = value;
	}


	/**
	 * 获取wap1消息内容
	 * 
	 * 此字段的版本 >= 0
	 * @return contentWap1 value 类型为:String
	 * 
	 */
	public String getContentWap1()
	{
		return contentWap1;
	}


	/**
	 * 设置wap1消息内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContentWap1(String value)
	{
		this.contentWap1 = value;
	}


	/**
	 * 获取wap2消息内容
	 * 
	 * 此字段的版本 >= 0
	 * @return contentWap2 value 类型为:String
	 * 
	 */
	public String getContentWap2()
	{
		return contentWap2;
	}


	/**
	 * 设置wap2消息内容
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setContentWap2(String value)
	{
		this.contentWap2 = value;
	}


	/**
	 * 获取发件人QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return senderUin value 类型为:long
	 * 
	 */
	public long getSenderUin()
	{
		return senderUin;
	}


	/**
	 * 设置发件人QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSenderUin(long value)
	{
		this.senderUin = value;
	}


	/**
	 * 获取发件人名称
	 * 
	 * 此字段的版本 >= 0
	 * @return senderName value 类型为:String
	 * 
	 */
	public String getSenderName()
	{
		return senderName;
	}


	/**
	 * 设置发件人名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSenderName(String value)
	{
		this.senderName = value;
	}


	/**
	 * 获取收件人QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @return receiverUin value 类型为:long
	 * 
	 */
	public long getReceiverUin()
	{
		return receiverUin;
	}


	/**
	 * 设置收件人QQ号码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReceiverUin(long value)
	{
		this.receiverUin = value;
	}


	/**
	 * 获取消息发送时间
	 * 
	 * 此字段的版本 >= 0
	 * @return receivedTime value 类型为:String
	 * 
	 */
	public String getReceivedTime()
	{
		return receivedTime;
	}


	/**
	 * 设置消息发送时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReceivedTime(String value)
	{
		this.receivedTime = value;
	}


	/**
	 * 获取最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @return lastModifyTime value 类型为:String
	 * 
	 */
	public String getLastModifyTime()
	{
		return lastModifyTime;
	}


	/**
	 * 设置最后修改时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setLastModifyTime(String value)
	{
		this.lastModifyTime = value;
	}


	/**
	 * 获取消息过期时间
	 * 
	 * 此字段的版本 >= 0
	 * @return expireTime value 类型为:String
	 * 
	 */
	public String getExpireTime()
	{
		return expireTime;
	}


	/**
	 * 设置消息过期时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setExpireTime(String value)
	{
		this.expireTime = value;
	}


	/**
	 * 获取消息状态,各状态见MessageStatusContant定义
	 * 
	 * 此字段的版本 >= 0
	 * @return status value 类型为:int
	 * 
	 */
	public int getStatus()
	{
		return status;
	}


	/**
	 * 设置消息状态,各状态见MessageStatusContant定义
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setStatus(int value)
	{
		this.status = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return reserved value 类型为:String
	 * 
	 */
	public String getReserved()
	{
		return reserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserved(String value)
	{
		this.reserved = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(InternalMessage)
				length += 4;  //计算字段version的长度 size_of(int)
				length += 4;  //计算字段id的长度 size_of(int)
				length += ByteStream.getObjectSize(title);  //计算字段title的长度 size_of(String)
				length += ByteStream.getObjectSize(contentWap1);  //计算字段contentWap1的长度 size_of(String)
				length += ByteStream.getObjectSize(contentWap2);  //计算字段contentWap2的长度 size_of(String)
				length += 4;  //计算字段senderUin的长度 size_of(long)
				length += ByteStream.getObjectSize(senderName);  //计算字段senderName的长度 size_of(String)
				length += 4;  //计算字段receiverUin的长度 size_of(long)
				length += ByteStream.getObjectSize(receivedTime);  //计算字段receivedTime的长度 size_of(String)
				length += ByteStream.getObjectSize(lastModifyTime);  //计算字段lastModifyTime的长度 size_of(String)
				length += ByteStream.getObjectSize(expireTime);  //计算字段expireTime的长度 size_of(String)
				length += 4;  //计算字段status的长度 size_of(int)
				length += ByteStream.getObjectSize(reserved);  //计算字段reserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
