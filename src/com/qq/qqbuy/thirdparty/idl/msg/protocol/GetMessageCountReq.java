package com.qq.qqbuy.thirdparty.idl.msg.protocol;

 

import java.io.Serializable;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2011-09-28 06:53::23
 *
 *@since version:0
*/
public class  GetMessageCountReq implements IServiceObject,Serializable
{
	private static final long serialVersionUID = 243825132523119073L;

	/**
	 * 接收者QQ号
	 *
	 * 版本 >= 0
	 */
	 private long toUin;

	/**
	 * 备用字段,用于一些紧急临时解决方案
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(toUin);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		toUin = bs.popUInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x80091805L;
	}


	/**
	 * 获取接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return toUin value 类型为:long
	 * 
	 */
	public long getToUin()
	{
		return toUin;
	}


	/**
	 * 设置接收者QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setToUin(long value)
	{
		this.toUin = value;
	}


	/**
	 * 获取备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置备用字段,用于一些紧急临时解决方案
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetMessageCountReq)
				length += 4;  //计算字段toUin的长度 size_of(long)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

}
