//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *商品信息
 *
 *@date 2013-09-13 04:55::45
 *
 *@since version:0
*/
public class CommodityInformation  implements ICanSerializeObject
{
	/**
	 * 版本
	 *
	 * 版本 >= 0
	 */
	 private long Version = 2; 

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long CommodityNumber;

	/**
	 * 版本 >= 0
	 */
	 private short CommodityNumber_u;

	/**
	 * 价格类型
	 *
	 * 版本 >= 0
	 */
	 private long PriceType;

	/**
	 * 版本 >= 0
	 */
	 private short PriceType_u;

	/**
	 * 红包id
	 *
	 * 版本 >= 0
	 */
	 private long RedPacketId;

	/**
	 * 版本 >= 0
	 */
	 private short RedPacketId_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String CommodityId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityId_u;

	/**
	 * 库存属
	 *
	 * 版本 >= 0
	 */
	 private String StockAttribute = new String();

	/**
	 * 版本 >= 0
	 */
	 private short StockAttribute_u;

	/**
	 * 
	 *
	 * 
	 */
	 private long ItemSkuId;

	/**
	 * 
	 *
	 * 
	 */
	 private short ItemSkuId_u;

	/**
	 * 
	 *
	 * 
	 */
	 private Map<String,String> cmdyReserves = new HashMap<String,String>();

	/**
	 * 
	 *
	 * 
	 */
	 private short cmdyReserves_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(CommodityNumber);
		bs.pushUByte(CommodityNumber_u);
		bs.pushUInt(PriceType);
		bs.pushUByte(PriceType_u);
		bs.pushUInt(RedPacketId);
		bs.pushUByte(RedPacketId_u);
		bs.pushString(CommodityId);
		bs.pushUByte(CommodityId_u);
		bs.pushString(StockAttribute);
		bs.pushUByte(StockAttribute_u);
if(  this.Version >= 2 ){
		bs.pushLong(ItemSkuId);
		bs.pushUByte(ItemSkuId_u);
		bs.pushObject(cmdyReserves);
		bs.pushUByte(cmdyReserves_u);
}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		CommodityNumber = bs.popUInt();
		CommodityNumber_u = bs.popUByte();
		PriceType = bs.popUInt();
		PriceType_u = bs.popUByte();
		RedPacketId = bs.popUInt();
		RedPacketId_u = bs.popUByte();
		CommodityId = bs.popString();
		CommodityId_u = bs.popUByte();
		StockAttribute = bs.popString();
		StockAttribute_u = bs.popUByte();
if(  this.Version >= 2 ){
		ItemSkuId = bs.popLong();
		ItemSkuId_u = bs.popUByte();
		cmdyReserves = (Map<String,String>)bs.popMap(String.class,String.class);
		cmdyReserves_u = bs.popUByte();
}
		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityNumber value 类型long
	 * 
	 */
	public long getCommodityNumber()
	{
		return CommodityNumber;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型long
	 * 
	 */
	public void setCommodityNumber(long value)
	{
		this.CommodityNumber = value;
		this.CommodityNumber_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityNumber_u value 类型short
	 * 
	 */
	public short getCommodityNumber_u()
	{
		return CommodityNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型short
	 * 
	 */
	public void setCommodityNumber_u(short value)
	{
		this.CommodityNumber_u = value;
	}


	/**
	 * 获取价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @return PriceType value 类型long
	 * 
	 */
	public long getPriceType()
	{
		return PriceType;
	}


	/**
	 * 设置价格类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型long
	 * 
	 */
	public void setPriceType(long value)
	{
		this.PriceType = value;
		this.PriceType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PriceType_u value 类型short
	 * 
	 */
	public short getPriceType_u()
	{
		return PriceType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型short
	 * 
	 */
	public void setPriceType_u(short value)
	{
		this.PriceType_u = value;
	}


	/**
	 * 获取红包id
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketId value 类型long
	 * 
	 */
	public long getRedPacketId()
	{
		return RedPacketId;
	}


	/**
	 * 设置红包id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型long
	 * 
	 */
	public void setRedPacketId(long value)
	{
		this.RedPacketId = value;
		this.RedPacketId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketId_u value 类型short
	 * 
	 */
	public short getRedPacketId_u()
	{
		return RedPacketId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型�?short
	 * 
	 */
	public void setRedPacketId_u(short value)
	{
		this.RedPacketId_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityId value 类型String
	 * 
	 */
	public String getCommodityId()
	{
		return CommodityId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型String
	 * 
	 */
	public void setCommodityId(String value)
	{
		if (value != null) {
				this.CommodityId = value;
				this.CommodityId_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityId_u value 类型short
	 * 
	 */
	public short getCommodityId_u()
	{
		return CommodityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型 short
	 * 
	 */
	public void setCommodityId_u(short value)
	{
		this.CommodityId_u = value;
	}


	/**
	 * 获取库存属 
	 * 
	 * 此字段的版本 >= 0
	 * @return StockAttribute value 类型 String
	 * 
	 */
	public String getStockAttribute()
	{
		return StockAttribute;
	}


	/**
	 * 设置库存属 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型 String
	 * 
	 */
	public void setStockAttribute(String value)
	{
		if (value != null) {
		this.StockAttribute = value;
		this.StockAttribute_u = 1;
	}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StockAttribute_u value 类型 short
	 * 
	 */
	public short getStockAttribute_u()
	{
		return StockAttribute_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型 short
	 * 
	 */
	public void setStockAttribute_u(short value)
	{
		this.StockAttribute_u = value;
	}


	/**
	 * 
	 * 
	 * 
	 * @return ItemSkuId value 
	 * 
	 */
	public long getItemSkuId()
	{
		return ItemSkuId;
	}


	/**
	 * 
	 * 
	 * 
	 * @param  value
	 * 
	 */
	public void setItemSkuId(long value)
	{
		this.ItemSkuId = value;
		this.ItemSkuId_u = 1;
	}


	/**
	 * 
	 * 
	 * 
	 * @return ItemSkuId_u value short
	 * 
	 */
	public short getItemSkuId_u()
	{
		return ItemSkuId_u;
	}


	/**
	 * 
	 * 
	 * 
	 * @param  value short
	 * 
	 */
	public void setItemSkuId_u(short value)
	{
		this.ItemSkuId_u = value;
	}


	/**
	 * 
	 * 
	 *  >= 2
	 * @return cmdyReserves value Map<String,String>
	 * 
	 */
	public Map<String,String> getCmdyReserves()
	{
		return cmdyReserves;
	}


	/**
	 * 
	 * 
	 *  >= 2
	 * @param  value Map<String,String>
	 * 
	 */
	public void setCmdyReserves(Map<String,String> value)
	{
		if (value != null) {
				this.cmdyReserves = value;
				this.cmdyReserves_u = 1;
		}
	}


	/**
	 * 
	 * 
	 * 
	 * @return cmdyReserves_u value short
	 * 
	 */
	public short getCmdyReserves_u()
	{
		return cmdyReserves_u;
	}


	/**
	 * 
	 * 
	 * >= 2
	 * @param  value short
	 * 
	 */
	public void setCmdyReserves_u(short value)
	{
		this.cmdyReserves_u = value;
	}


	/**
	 *   计算类长 
	 *   用于告诉解包者，该类只放了这么长的数 
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长 
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方 
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CommodityInformation)
				length += 4;  //计算字段Version的长 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长 size_of(uint8_t)
				length += 4;  //计算字段CommodityNumber的长 size_of(uint32_t)
				length += 1;  //计算字段CommodityNumber_u的长 size_of(uint8_t)
				length += 4;  //计算字段PriceType的长 size_of(uint32_t)
				length += 1;  //计算字段PriceType_u的长 size_of(uint8_t)
				length += 4;  //计算字段RedPacketId的长 size_of(uint32_t)
				length += 1;  //计算字段RedPacketId_u的长 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityId);  //计算字段CommodityId的长 size_of(String)
				length += 1;  //计算字段CommodityId_u的长 size_of(uint8_t)
				if(StockAttribute == null)
                    length += 4;
                else
                    length += 4+ StockAttribute.getBytes("GBK").length;
//				length += ByteStream.getObjectSize(StockAttribute);  //计算字段StockAttribute的长 size_of(String)
				length += 1;  //计算字段StockAttribute_u的长 size_of(uint8_t)
if(  this.Version >= 2 ){
				length += 17;  //size_of(uint64_t)
				length += 1;  //size_of(uint8_t)
				length += ByteStream.getObjectSize(cmdyReserves);  //cmdyReservessize_of(Map)
				length += 1;  //cmdyReserves_usize_of(uint8_t)
}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
