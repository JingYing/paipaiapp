//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *促销规则信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class PromotionRule  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 规则ID
	 *
	 * 版本 >= 0
	 */
	 private long RuleId;

	/**
	 * 版本 >= 0
	 */
	 private short RuleId_u;

	/**
	 * 限制类型：商品数量或价格
	 *
	 * 版本 >= 0
	 */
	 private long LimitType;

	/**
	 * 版本 >= 0
	 */
	 private short LimitType_u;

	/**
	 * 最小限制
	 *
	 * 版本 >= 0
	 */
	 private long LowLimit;

	/**
	 * 版本 >= 0
	 */
	 private short LowLimit_u;

	/**
	 * 促销标，促销类型
	 *
	 * 版本 >= 0
	 */
	 private long PromotionMask;

	/**
	 * 版本 >= 0
	 */
	 private short PromotionMask_u;

	/**
	 * 促销折扣
	 *
	 * 版本 >= 0
	 */
	 private long FreeMoney;

	/**
	 * 版本 >= 0
	 */
	 private short FreeMoney_u;

	/**
	 * 促销优惠的金额
	 *
	 * 版本 >= 0
	 */
	 private long FreeRebate;

	/**
	 * 版本 >= 0
	 */
	 private short FreeRebate_u;

	/**
	 * 加价购
	 *
	 * 版本 >= 0
	 */
	 private long BarterMoney;

	/**
	 * 版本 >= 0
	 */
	 private short BarterMoney_u;

	/**
	 * 促销规则描述
	 *
	 * 版本 >= 0
	 */
	 private String Desc = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Desc_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(RuleId);
		bs.pushUByte(RuleId_u);
		bs.pushUInt(LimitType);
		bs.pushUByte(LimitType_u);
		bs.pushUInt(LowLimit);
		bs.pushUByte(LowLimit_u);
		bs.pushUInt(PromotionMask);
		bs.pushUByte(PromotionMask_u);
		bs.pushUInt(FreeMoney);
		bs.pushUByte(FreeMoney_u);
		bs.pushUInt(FreeRebate);
		bs.pushUByte(FreeRebate_u);
		bs.pushUInt(BarterMoney);
		bs.pushUByte(BarterMoney_u);
		bs.pushString(Desc);
		bs.pushUByte(Desc_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		RuleId = bs.popUInt();
		RuleId_u = bs.popUByte();
		LimitType = bs.popUInt();
		LimitType_u = bs.popUByte();
		LowLimit = bs.popUInt();
		LowLimit_u = bs.popUByte();
		PromotionMask = bs.popUInt();
		PromotionMask_u = bs.popUByte();
		FreeMoney = bs.popUInt();
		FreeMoney_u = bs.popUByte();
		FreeRebate = bs.popUInt();
		FreeRebate_u = bs.popUByte();
		BarterMoney = bs.popUInt();
		BarterMoney_u = bs.popUByte();
		Desc = bs.popString();
		Desc_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @return RuleId value 类型为:long
	 * 
	 */
	public long getRuleId()
	{
		return RuleId;
	}


	/**
	 * 设置规则ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRuleId(long value)
	{
		this.RuleId = value;
		this.RuleId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RuleId_u value 类型为:short
	 * 
	 */
	public short getRuleId_u()
	{
		return RuleId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRuleId_u(short value)
	{
		this.RuleId_u = value;
	}


	/**
	 * 获取限制类型：商品数量或价格
	 * 
	 * 此字段的版本 >= 0
	 * @return LimitType value 类型为:long
	 * 
	 */
	public long getLimitType()
	{
		return LimitType;
	}


	/**
	 * 设置限制类型：商品数量或价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLimitType(long value)
	{
		this.LimitType = value;
		this.LimitType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LimitType_u value 类型为:short
	 * 
	 */
	public short getLimitType_u()
	{
		return LimitType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLimitType_u(short value)
	{
		this.LimitType_u = value;
	}


	/**
	 * 获取最小限制
	 * 
	 * 此字段的版本 >= 0
	 * @return LowLimit value 类型为:long
	 * 
	 */
	public long getLowLimit()
	{
		return LowLimit;
	}


	/**
	 * 设置最小限制
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLowLimit(long value)
	{
		this.LowLimit = value;
		this.LowLimit_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return LowLimit_u value 类型为:short
	 * 
	 */
	public short getLowLimit_u()
	{
		return LowLimit_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLowLimit_u(short value)
	{
		this.LowLimit_u = value;
	}


	/**
	 * 获取促销标，促销类型
	 * 
	 * 此字段的版本 >= 0
	 * @return PromotionMask value 类型为:long
	 * 
	 */
	public long getPromotionMask()
	{
		return PromotionMask;
	}


	/**
	 * 设置促销标，促销类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPromotionMask(long value)
	{
		this.PromotionMask = value;
		this.PromotionMask_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PromotionMask_u value 类型为:short
	 * 
	 */
	public short getPromotionMask_u()
	{
		return PromotionMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPromotionMask_u(short value)
	{
		this.PromotionMask_u = value;
	}


	/**
	 * 获取促销折扣
	 * 
	 * 此字段的版本 >= 0
	 * @return FreeMoney value 类型为:long
	 * 
	 */
	public long getFreeMoney()
	{
		return FreeMoney;
	}


	/**
	 * 设置促销折扣
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFreeMoney(long value)
	{
		this.FreeMoney = value;
		this.FreeMoney_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return FreeMoney_u value 类型为:short
	 * 
	 */
	public short getFreeMoney_u()
	{
		return FreeMoney_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFreeMoney_u(short value)
	{
		this.FreeMoney_u = value;
	}


	/**
	 * 获取促销优惠的金额
	 * 
	 * 此字段的版本 >= 0
	 * @return FreeRebate value 类型为:long
	 * 
	 */
	public long getFreeRebate()
	{
		return FreeRebate;
	}


	/**
	 * 设置促销优惠的金额
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFreeRebate(long value)
	{
		this.FreeRebate = value;
		this.FreeRebate_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return FreeRebate_u value 类型为:short
	 * 
	 */
	public short getFreeRebate_u()
	{
		return FreeRebate_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFreeRebate_u(short value)
	{
		this.FreeRebate_u = value;
	}


	/**
	 * 获取加价购
	 * 
	 * 此字段的版本 >= 0
	 * @return BarterMoney value 类型为:long
	 * 
	 */
	public long getBarterMoney()
	{
		return BarterMoney;
	}


	/**
	 * 设置加价购
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setBarterMoney(long value)
	{
		this.BarterMoney = value;
		this.BarterMoney_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return BarterMoney_u value 类型为:short
	 * 
	 */
	public short getBarterMoney_u()
	{
		return BarterMoney_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setBarterMoney_u(short value)
	{
		this.BarterMoney_u = value;
	}


	/**
	 * 获取促销规则描述
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc value 类型为:String
	 * 
	 */
	public String getDesc()
	{
		return Desc;
	}


	/**
	 * 设置促销规则描述
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDesc(String value)
	{
		this.Desc = value;
		this.Desc_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Desc_u value 类型为:short
	 * 
	 */
	public short getDesc_u()
	{
		return Desc_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDesc_u(short value)
	{
		this.Desc_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(PromotionRule)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RuleId的长度 size_of(uint32_t)
				length += 1;  //计算字段RuleId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LimitType的长度 size_of(uint32_t)
				length += 1;  //计算字段LimitType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段LowLimit的长度 size_of(uint32_t)
				length += 1;  //计算字段LowLimit_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PromotionMask的长度 size_of(uint32_t)
				length += 1;  //计算字段PromotionMask_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FreeMoney的长度 size_of(uint32_t)
				length += 1;  //计算字段FreeMoney_u的长度 size_of(uint8_t)
				length += 4;  //计算字段FreeRebate的长度 size_of(uint32_t)
				length += 1;  //计算字段FreeRebate_u的长度 size_of(uint8_t)
				length += 4;  //计算字段BarterMoney的长度 size_of(uint32_t)
				length += 1;  //计算字段BarterMoney_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Desc);  //计算字段Desc的长度 size_of(String)
				length += 1;  //计算字段Desc_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
