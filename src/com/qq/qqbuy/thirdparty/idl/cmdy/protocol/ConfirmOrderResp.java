 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *购物车确认订单返回
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  ConfirmOrderResp implements IServiceObject
{
	public long result;
	/**
	 * 买家信息
	 *
	 * 版本 >= 0
	 */
	 private CartBuyerInfo BuyerInfo = new CartBuyerInfo();

	/**
	 * 正常商品分单列表
	 *
	 * 版本 >= 0
	 */
	 private DealViewDataList dealViewLst = new DealViewDataList();

	/**
	 * 异常商品分单列表
	 *
	 * 版本 >= 0
	 */
	 private DealViewDataList ProblemDealViewLst = new DealViewDataList();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(BuyerInfo);
		bs.pushObject(dealViewLst);
		bs.pushObject(ProblemDealViewLst);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		BuyerInfo = (CartBuyerInfo) bs.popObject(CartBuyerInfo.class);
		dealViewLst = (DealViewDataList) bs.popObject(DealViewDataList.class);
		ProblemDealViewLst = (DealViewDataList) bs.popObject(DealViewDataList.class);
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91128806L;
	}


	/**
	 * 获取买家信息
	 * 
	 * 此字段的版本 >= 0
	 * @return BuyerInfo value 类型为:CartBuyerInfo
	 * 
	 */
	public CartBuyerInfo getBuyerInfo()
	{
		return BuyerInfo;
	}


	/**
	 * 设置买家信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:CartBuyerInfo
	 * 
	 */
	public void setBuyerInfo(CartBuyerInfo value)
	{
		if (value != null) {
				this.BuyerInfo = value;
		}else{
				this.BuyerInfo = new CartBuyerInfo();
		}
	}


	/**
	 * 获取正常商品分单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return dealViewLst value 类型为:DealViewDataList
	 * 
	 */
	public DealViewDataList getDealViewLst()
	{
		return dealViewLst;
	}


	/**
	 * 设置正常商品分单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealViewDataList
	 * 
	 */
	public void setDealViewLst(DealViewDataList value)
	{
		if (value != null) {
				this.dealViewLst = value;
		}else{
				this.dealViewLst = new DealViewDataList();
		}
	}


	/**
	 * 获取异常商品分单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return ProblemDealViewLst value 类型为:DealViewDataList
	 * 
	 */
	public DealViewDataList getProblemDealViewLst()
	{
		return ProblemDealViewLst;
	}


	/**
	 * 设置异常商品分单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:DealViewDataList
	 * 
	 */
	public void setProblemDealViewLst(DealViewDataList value)
	{
		if (value != null) {
				this.ProblemDealViewLst = value;
		}else{
				this.ProblemDealViewLst = new DealViewDataList();
		}
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ConfirmOrderResp)
				length += ByteStream.getObjectSize(BuyerInfo);  //计算字段BuyerInfo的长度 size_of(CartBuyerInfo)
				length += ByteStream.getObjectSize(dealViewLst);  //计算字段dealViewLst的长度 size_of(DealViewDataList)
				length += ByteStream.getObjectSize(ProblemDealViewLst);  //计算字段ProblemDealViewLst的长度 size_of(DealViewDataList)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
