 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *购物车下单返回
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class  MakeOrderResp implements IServiceObject
{
	public long result;
	/**
	 * 下单返回
	 *
	 * 版本 >= 0
	 */
	 private MakeOrderResponse Response = new MakeOrderResponse();

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String ErrMsg = new String();

	/**
	 * 保留输出字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveOut = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(Response);
		bs.pushString(ErrMsg);
		bs.pushString(ReserveOut);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		Response = (MakeOrderResponse) bs.popObject(MakeOrderResponse.class);
		ErrMsg = bs.popString();
		ReserveOut = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x91128801L;
	}


	/**
	 * 获取下单返回
	 * 
	 * 此字段的版本 >= 0
	 * @return Response value 类型为:MakeOrderResponse
	 * 
	 */
	public MakeOrderResponse getResponse()
	{
		return Response;
	}


	/**
	 * 设置下单返回
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:MakeOrderResponse
	 * 
	 */
	public void setResponse(MakeOrderResponse value)
	{
		if (value != null) {
				this.Response = value;
		}else{
				this.Response = new MakeOrderResponse();
		}
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return ErrMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return ErrMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.ErrMsg = value;
	}


	/**
	 * 获取保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveOut value 类型为:String
	 * 
	 */
	public String getReserveOut()
	{
		return ReserveOut;
	}


	/**
	 * 设置保留输出字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveOut(String value)
	{
		this.ReserveOut = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(MakeOrderResp)
				length += ByteStream.getObjectSize(Response);  //计算字段Response的长度 size_of(MakeOrderResponse)
				length += ByteStream.getObjectSize(ErrMsg);  //计算字段ErrMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveOut);  //计算字段ReserveOut的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
