//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.uint32_t;
import java.util.Vector;

/**
 *红包
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class CartRedPacketPo  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 所有者Uin
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 红包编号
	 *
	 * 版本 >= 0
	 */
	 private long PacketId;

	/**
	 * 版本 >= 0
	 */
	 private short PacketId_u;

	/**
	 * 红包类型标识（优惠券，包邮卡还是红包）
	 *
	 * 版本 >= 0
	 */
	 private long Flag;

	/**
	 * 版本 >= 0
	 */
	 private short Flag_u;

	/**
	 * 红包标识
	 *
	 * 版本 >= 0
	 */
	 private long Property;

	/**
	 * 版本 >= 0
	 */
	 private short Property_u;

	/**
	 * 面值
	 *
	 * 版本 >= 0
	 */
	 private long Value;

	/**
	 * 版本 >= 0
	 */
	 private short Value_u;

	/**
	 * 最低消费
	 *
	 * 版本 >= 0
	 */
	 private long Quota;

	/**
	 * 版本 >= 0
	 */
	 private short Quota_u;

	/**
	 * 到期时间
	 *
	 * 版本 >= 0
	 */
	 private long EndTime;

	/**
	 * 版本 >= 0
	 */
	 private short EndTime_u;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private long Reserved;

	/**
	 * 版本 >= 0
	 */
	 private short Reserved_u;

	/**
	 * 红包名称
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Name_u;

	/**
	 * 可用的店铺
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> ValidShop = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short ValidShop_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(Uin);
		bs.pushUByte(Uin_u);
		bs.pushUInt(PacketId);
		bs.pushUByte(PacketId_u);
		bs.pushUInt(Flag);
		bs.pushUByte(Flag_u);
		bs.pushUInt(Property);
		bs.pushUByte(Property_u);
		bs.pushUInt(Value);
		bs.pushUByte(Value_u);
		bs.pushUInt(Quota);
		bs.pushUByte(Quota_u);
		bs.pushUInt(EndTime);
		bs.pushUByte(EndTime_u);
		bs.pushUInt(Reserved);
		bs.pushUByte(Reserved_u);
		bs.pushString(Name);
		bs.pushUByte(Name_u);
		bs.pushObject(ValidShop);
		bs.pushUByte(ValidShop_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Uin = bs.popUInt();
		Uin_u = bs.popUByte();
		PacketId = bs.popUInt();
		PacketId_u = bs.popUByte();
		Flag = bs.popUInt();
		Flag_u = bs.popUByte();
		Property = bs.popUInt();
		Property_u = bs.popUByte();
		Value = bs.popUInt();
		Value_u = bs.popUByte();
		Quota = bs.popUInt();
		Quota_u = bs.popUByte();
		EndTime = bs.popUInt();
		EndTime_u = bs.popUByte();
		Reserved = bs.popUInt();
		Reserved_u = bs.popUByte();
		Name = bs.popString();
		Name_u = bs.popUByte();
		ValidShop = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		ValidShop_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取所有者Uin
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置所有者Uin
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
		this.Uin_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId value 类型为:long
	 * 
	 */
	public long getPacketId()
	{
		return PacketId;
	}


	/**
	 * 设置红包编号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPacketId(long value)
	{
		this.PacketId = value;
		this.PacketId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PacketId_u value 类型为:short
	 * 
	 */
	public short getPacketId_u()
	{
		return PacketId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPacketId_u(short value)
	{
		this.PacketId_u = value;
	}


	/**
	 * 获取红包类型标识（优惠券，包邮卡还是红包）
	 * 
	 * 此字段的版本 >= 0
	 * @return Flag value 类型为:long
	 * 
	 */
	public long getFlag()
	{
		return Flag;
	}


	/**
	 * 设置红包类型标识（优惠券，包邮卡还是红包）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlag(long value)
	{
		this.Flag = value;
		this.Flag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Flag_u value 类型为:short
	 * 
	 */
	public short getFlag_u()
	{
		return Flag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlag_u(short value)
	{
		this.Flag_u = value;
	}


	/**
	 * 获取红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return Property;
	}


	/**
	 * 设置红包标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.Property = value;
		this.Property_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Property_u value 类型为:short
	 * 
	 */
	public short getProperty_u()
	{
		return Property_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProperty_u(short value)
	{
		this.Property_u = value;
	}


	/**
	 * 获取面值
	 * 
	 * 此字段的版本 >= 0
	 * @return Value value 类型为:long
	 * 
	 */
	public long getValue()
	{
		return Value;
	}


	/**
	 * 设置面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setValue(long value)
	{
		this.Value = value;
		this.Value_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Value_u value 类型为:short
	 * 
	 */
	public short getValue_u()
	{
		return Value_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setValue_u(short value)
	{
		this.Value_u = value;
	}


	/**
	 * 获取最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @return Quota value 类型为:long
	 * 
	 */
	public long getQuota()
	{
		return Quota;
	}


	/**
	 * 设置最低消费
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setQuota(long value)
	{
		this.Quota = value;
		this.Quota_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Quota_u value 类型为:short
	 * 
	 */
	public short getQuota_u()
	{
		return Quota_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setQuota_u(short value)
	{
		this.Quota_u = value;
	}


	/**
	 * 获取到期时间
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime value 类型为:long
	 * 
	 */
	public long getEndTime()
	{
		return EndTime;
	}


	/**
	 * 设置到期时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setEndTime(long value)
	{
		this.EndTime = value;
		this.EndTime_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return EndTime_u value 类型为:short
	 * 
	 */
	public short getEndTime_u()
	{
		return EndTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setEndTime_u(short value)
	{
		this.EndTime_u = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserved value 类型为:long
	 * 
	 */
	public long getReserved()
	{
		return Reserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setReserved(long value)
	{
		this.Reserved = value;
		this.Reserved_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Reserved_u value 类型为:short
	 * 
	 */
	public short getReserved_u()
	{
		return Reserved_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setReserved_u(short value)
	{
		this.Reserved_u = value;
	}


	/**
	 * 获取红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置红包名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.Name = value;
		this.Name_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Name_u value 类型为:short
	 * 
	 */
	public short getName_u()
	{
		return Name_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setName_u(short value)
	{
		this.Name_u = value;
	}


	/**
	 * 获取可用的店铺
	 * 
	 * 此字段的版本 >= 0
	 * @return ValidShop value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getValidShop()
	{
		return ValidShop;
	}


	/**
	 * 设置可用的店铺
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setValidShop(Vector<uint32_t> value)
	{
		if (value != null) {
				this.ValidShop = value;
				this.ValidShop_u = 1;
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ValidShop_u value 类型为:short
	 * 
	 */
	public short getValidShop_u()
	{
		return ValidShop_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setValidShop_u(short value)
	{
		this.ValidShop_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CartRedPacketPo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Uin的长度 size_of(uint32_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PacketId的长度 size_of(uint32_t)
				length += 1;  //计算字段PacketId_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Flag的长度 size_of(uint32_t)
				length += 1;  //计算字段Flag_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 1;  //计算字段Property_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Value的长度 size_of(uint32_t)
				length += 1;  //计算字段Value_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Quota的长度 size_of(uint32_t)
				length += 1;  //计算字段Quota_u的长度 size_of(uint8_t)
				length += 4;  //计算字段EndTime的长度 size_of(uint32_t)
				length += 1;  //计算字段EndTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Reserved的长度 size_of(uint32_t)
				length += 1;  //计算字段Reserved_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
				length += 1;  //计算字段Name_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ValidShop);  //计算字段ValidShop的长度 size_of(Vector)
				length += 1;  //计算字段ValidShop_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
