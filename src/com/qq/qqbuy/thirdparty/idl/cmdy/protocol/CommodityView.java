package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import java.util.Map;
import java.util.Vector;
import java.util.HashMap;

/**
 *商品展示
 *
 *@date 2015-05-13 09:50:39
 *
 *@since version:0
*/
public class CommodityView  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 场景ID
	 */
	private long sceneId;
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version = 1;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 属性标识
	 *
	 * 版本 >= 0
	 */
	 private long PropertyMask;

	/**
	 * 版本 >= 0
	 */
	 private short PropertyMask_u;

	/**
	 * 普通价
	 *
	 * 版本 >= 0
	 */
	 private long NormalPrice;

	/**
	 * 版本 >= 0
	 */
	 private short NormalPrice_u;

	/**
	 * 商品数量
	 *
	 * 版本 >= 0
	 */
	 private long CommodityNumber;

	/**
	 * 版本 >= 0
	 */
	 private short CommodityNumber_u;

	/**
	 * 红包面值
	 *
	 * 版本 >= 0
	 */
	 private long RedPacketValue;

	/**
	 * 版本 >= 0
	 */
	 private short RedPacketValue_u;

	/**
	 * 商品积分
	 *
	 * 版本 >= 0
	 */
	 private long CommodityScore;

	/**
	 * 版本 >= 0
	 */
	 private short CommodityScore_u;

	/**
	 * 商品最大数据量
	 *
	 * 版本 >= 0
	 */
	 private long CommodityMaxNumber;

	/**
	 * 版本 >= 0
	 */
	 private short CommodityMaxNumber_u;

	/**
	 * 订单标识
	 *
	 * 版本 >= 0
	 */
	 private long OrderMask;

	/**
	 * 版本 >= 0
	 */
	 private short OrderMask_u;

	/**
	 * 发票类型
	 *
	 * 版本 >= 0
	 */
	 private long InvoiceType;

	/**
	 * 版本 >= 0
	 */
	 private short InvoiceType_u;

	/**
	 * 订单类型
	 *
	 * 版本 >= 0
	 */
	 private long DealType;

	/**
	 * 版本 >= 0
	 */
	 private short DealType_u;

	/**
	 * 类目导航节点id
	 *
	 * 版本 >= 0
	 */
	 private long NavigationNodeId;

	/**
	 * 版本 >= 0
	 */
	 private short NavigationNodeId_u;

	/**
	 * 商品id
	 *
	 * 版本 >= 0
	 */
	 private String CommodityId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityId_u;

	/**
	 * 商品title
	 *
	 * 版本 >= 0
	 */
	 private String CommodityTitle = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityTitle_u;

	/**
	 * 商品logo
	 *
	 * 版本 >= 0
	 */
	 private String CommodityLogo = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityLogo_u;

	/**
	 * 商品库存属性
	 *
	 * 版本 >= 0
	 */
	 private String CommodityStockAttribute = new String();

	/**
	 * 版本 >= 0
	 */
	 private short CommodityStockAttribute_u;

	/**
	 * 商品多价
	 *
	 * 版本 >= 0
	 */
	 private Vector<MultiPrice> MultiPriceInfo = new Vector<MultiPrice>();

	/**
	 * 版本 >= 0
	 */
	 private short MultiPriceInfo_u;

	/**
	 * 商品skuid
	 *
	 * 版本 >= 1
	 */
	 private long ItemSkuId;

	/**
	 * 商品skuid
	 *
	 * 版本 >= 1
	 */
	 private short ItemSkuId_u;

	/**
	 *商品扩展标识
	 *
	 * 版本 >= 1
	 */
	 private Map<String,String> mapCmdyReserves = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short cCmdyReserves_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(PropertyMask);
		bs.pushUByte(PropertyMask_u);
		bs.pushUInt(NormalPrice);
		bs.pushUByte(NormalPrice_u);
		bs.pushUInt(CommodityNumber);
		bs.pushUByte(CommodityNumber_u);
		bs.pushUInt(RedPacketValue);
		bs.pushUByte(RedPacketValue_u);
		bs.pushUInt(CommodityScore);
		bs.pushUByte(CommodityScore_u);
		bs.pushUInt(CommodityMaxNumber);
		bs.pushUByte(CommodityMaxNumber_u);
		bs.pushUInt(OrderMask);
		bs.pushUByte(OrderMask_u);
		bs.pushUInt(InvoiceType);
		bs.pushUByte(InvoiceType_u);
		bs.pushUInt(DealType);
		bs.pushUByte(DealType_u);
		bs.pushUInt(NavigationNodeId);
		bs.pushUByte(NavigationNodeId_u);
		bs.pushString(CommodityId);
		bs.pushUByte(CommodityId_u);
		bs.pushString(CommodityTitle);
		bs.pushUByte(CommodityTitle_u);
		bs.pushString(CommodityLogo);
		bs.pushUByte(CommodityLogo_u);
		bs.pushString(CommodityStockAttribute);
		bs.pushUByte(CommodityStockAttribute_u);
		bs.pushObject(MultiPriceInfo);
		bs.pushUByte(MultiPriceInfo_u);
		if(  this.Version >= 1 ){
				bs.pushLong(ItemSkuId);
		}
		if(  this.Version >= 1 ){
				bs.pushUByte(ItemSkuId_u);
		}
		if(  this.Version >= 1 ){
				bs.pushObject(mapCmdyReserves);
		}
		bs.pushUByte(cCmdyReserves_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		PropertyMask = bs.popUInt();
		PropertyMask_u = bs.popUByte();
		NormalPrice = bs.popUInt();
		NormalPrice_u = bs.popUByte();
		CommodityNumber = bs.popUInt();
		CommodityNumber_u = bs.popUByte();
		RedPacketValue = bs.popUInt();
		RedPacketValue_u = bs.popUByte();
		CommodityScore = bs.popUInt();
		CommodityScore_u = bs.popUByte();
		CommodityMaxNumber = bs.popUInt();
		CommodityMaxNumber_u = bs.popUByte();
		OrderMask = bs.popUInt();
		OrderMask_u = bs.popUByte();
		InvoiceType = bs.popUInt();
		InvoiceType_u = bs.popUByte();
		DealType = bs.popUInt();
		DealType_u = bs.popUByte();
		NavigationNodeId = bs.popUInt();
		NavigationNodeId_u = bs.popUByte();
		CommodityId = bs.popString();
		CommodityId_u = bs.popUByte();
		CommodityTitle = bs.popString();
		CommodityTitle_u = bs.popUByte();
		CommodityLogo = bs.popString();
		CommodityLogo_u = bs.popUByte();
		CommodityStockAttribute = bs.popString();
		CommodityStockAttribute_u = bs.popUByte();
		MultiPriceInfo = (Vector<MultiPrice>)bs.popVector(MultiPrice.class);
		MultiPriceInfo_u = bs.popUByte();
		if(  this.Version >= 1 ){
				ItemSkuId = bs.popLong();
		}
		if(  this.Version >= 1 ){
				ItemSkuId_u = bs.popUByte();
		}
		if(  this.Version >= 1 ){
				mapCmdyReserves = (Map<String,String>)bs.popMap(String.class,String.class);
		}
		cCmdyReserves_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取属性标识
	 * 
	 * 此字段的版本 >= 0
	 * @return PropertyMask value 类型为:long
	 * 
	 */
	public long getPropertyMask()
	{
		return PropertyMask;
	}


	/**
	 * 设置属性标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPropertyMask(long value)
	{
		this.PropertyMask = value;
		this.PropertyMask_u = 1;
	}

	public boolean issetPropertyMask()
	{
		return this.PropertyMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return PropertyMask_u value 类型为:short
	 * 
	 */
	public short getPropertyMask_u()
	{
		return PropertyMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setPropertyMask_u(short value)
	{
		this.PropertyMask_u = value;
	}


	/**
	 * 获取普通价
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalPrice value 类型为:long
	 * 
	 */
	public long getNormalPrice()
	{
		return NormalPrice;
	}


	/**
	 * 设置普通价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNormalPrice(long value)
	{
		this.NormalPrice = value;
		this.NormalPrice_u = 1;
	}

	public boolean issetNormalPrice()
	{
		return this.NormalPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NormalPrice_u value 类型为:short
	 * 
	 */
	public short getNormalPrice_u()
	{
		return NormalPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNormalPrice_u(short value)
	{
		this.NormalPrice_u = value;
	}


	/**
	 * 获取商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityNumber value 类型为:long
	 * 
	 */
	public long getCommodityNumber()
	{
		return CommodityNumber;
	}


	/**
	 * 设置商品数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCommodityNumber(long value)
	{
		this.CommodityNumber = value;
		this.CommodityNumber_u = 1;
	}

	public boolean issetCommodityNumber()
	{
		return this.CommodityNumber_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityNumber_u value 类型为:short
	 * 
	 */
	public short getCommodityNumber_u()
	{
		return CommodityNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityNumber_u(short value)
	{
		this.CommodityNumber_u = value;
	}


	/**
	 * 获取红包面值
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketValue value 类型为:long
	 * 
	 */
	public long getRedPacketValue()
	{
		return RedPacketValue;
	}


	/**
	 * 设置红包面值
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRedPacketValue(long value)
	{
		this.RedPacketValue = value;
		this.RedPacketValue_u = 1;
	}

	public boolean issetRedPacketValue()
	{
		return this.RedPacketValue_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return RedPacketValue_u value 类型为:short
	 * 
	 */
	public short getRedPacketValue_u()
	{
		return RedPacketValue_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRedPacketValue_u(short value)
	{
		this.RedPacketValue_u = value;
	}


	/**
	 * 获取商品积分
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityScore value 类型为:long
	 * 
	 */
	public long getCommodityScore()
	{
		return CommodityScore;
	}


	/**
	 * 设置商品积分
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCommodityScore(long value)
	{
		this.CommodityScore = value;
		this.CommodityScore_u = 1;
	}

	public boolean issetCommodityScore()
	{
		return this.CommodityScore_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityScore_u value 类型为:short
	 * 
	 */
	public short getCommodityScore_u()
	{
		return CommodityScore_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityScore_u(short value)
	{
		this.CommodityScore_u = value;
	}


	/**
	 * 获取商品最大数据量
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityMaxNumber value 类型为:long
	 * 
	 */
	public long getCommodityMaxNumber()
	{
		return CommodityMaxNumber;
	}


	/**
	 * 设置商品最大数据量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCommodityMaxNumber(long value)
	{
		this.CommodityMaxNumber = value;
		this.CommodityMaxNumber_u = 1;
	}

	public boolean issetCommodityMaxNumber()
	{
		return this.CommodityMaxNumber_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityMaxNumber_u value 类型为:short
	 * 
	 */
	public short getCommodityMaxNumber_u()
	{
		return CommodityMaxNumber_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityMaxNumber_u(short value)
	{
		this.CommodityMaxNumber_u = value;
	}


	/**
	 * 获取订单标识
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderMask value 类型为:long
	 * 
	 */
	public long getOrderMask()
	{
		return OrderMask;
	}


	/**
	 * 设置订单标识
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrderMask(long value)
	{
		this.OrderMask = value;
		this.OrderMask_u = 1;
	}

	public boolean issetOrderMask()
	{
		return this.OrderMask_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return OrderMask_u value 类型为:short
	 * 
	 */
	public short getOrderMask_u()
	{
		return OrderMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOrderMask_u(short value)
	{
		this.OrderMask_u = value;
	}


	/**
	 * 获取发票类型
	 * 
	 * 此字段的版本 >= 0
	 * @return InvoiceType value 类型为:long
	 * 
	 */
	public long getInvoiceType()
	{
		return InvoiceType;
	}


	/**
	 * 设置发票类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setInvoiceType(long value)
	{
		this.InvoiceType = value;
		this.InvoiceType_u = 1;
	}

	public boolean issetInvoiceType()
	{
		return this.InvoiceType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return InvoiceType_u value 类型为:short
	 * 
	 */
	public short getInvoiceType_u()
	{
		return InvoiceType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setInvoiceType_u(short value)
	{
		this.InvoiceType_u = value;
	}


	/**
	 * 获取订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @return DealType value 类型为:long
	 * 
	 */
	public long getDealType()
	{
		return DealType;
	}


	/**
	 * 设置订单类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDealType(long value)
	{
		this.DealType = value;
		this.DealType_u = 1;
	}

	public boolean issetDealType()
	{
		return this.DealType_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DealType_u value 类型为:short
	 * 
	 */
	public short getDealType_u()
	{
		return DealType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDealType_u(short value)
	{
		this.DealType_u = value;
	}


	/**
	 * 获取类目导航节点id
	 * 
	 * 此字段的版本 >= 0
	 * @return NavigationNodeId value 类型为:long
	 * 
	 */
	public long getNavigationNodeId()
	{
		return NavigationNodeId;
	}


	/**
	 * 设置类目导航节点id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNavigationNodeId(long value)
	{
		this.NavigationNodeId = value;
		this.NavigationNodeId_u = 1;
	}

	public boolean issetNavigationNodeId()
	{
		return this.NavigationNodeId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return NavigationNodeId_u value 类型为:short
	 * 
	 */
	public short getNavigationNodeId_u()
	{
		return NavigationNodeId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNavigationNodeId_u(short value)
	{
		this.NavigationNodeId_u = value;
	}


	/**
	 * 获取商品id
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityId value 类型为:String
	 * 
	 */
	public String getCommodityId()
	{
		return CommodityId;
	}


	/**
	 * 设置商品id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityId(String value)
	{
		this.CommodityId = value;
		this.CommodityId_u = 1;
	}

	public boolean issetCommodityId()
	{
		return this.CommodityId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityId_u value 类型为:short
	 * 
	 */
	public short getCommodityId_u()
	{
		return CommodityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityId_u(short value)
	{
		this.CommodityId_u = value;
	}


	/**
	 * 获取商品title
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityTitle value 类型为:String
	 * 
	 */
	public String getCommodityTitle()
	{
		return CommodityTitle;
	}


	/**
	 * 设置商品title
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityTitle(String value)
	{
		this.CommodityTitle = value;
		this.CommodityTitle_u = 1;
	}

	public boolean issetCommodityTitle()
	{
		return this.CommodityTitle_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityTitle_u value 类型为:short
	 * 
	 */
	public short getCommodityTitle_u()
	{
		return CommodityTitle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityTitle_u(short value)
	{
		this.CommodityTitle_u = value;
	}


	/**
	 * 获取商品logo
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityLogo value 类型为:String
	 * 
	 */
	public String getCommodityLogo()
	{
		return CommodityLogo;
	}


	/**
	 * 设置商品logo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityLogo(String value)
	{
		this.CommodityLogo = value;
		this.CommodityLogo_u = 1;
	}

	public boolean issetCommodityLogo()
	{
		return this.CommodityLogo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityLogo_u value 类型为:short
	 * 
	 */
	public short getCommodityLogo_u()
	{
		return CommodityLogo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityLogo_u(short value)
	{
		this.CommodityLogo_u = value;
	}


	/**
	 * 获取商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityStockAttribute value 类型为:String
	 * 
	 */
	public String getCommodityStockAttribute()
	{
		return CommodityStockAttribute;
	}


	/**
	 * 设置商品库存属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCommodityStockAttribute(String value)
	{
		this.CommodityStockAttribute = value;
		this.CommodityStockAttribute_u = 1;
	}

	public boolean issetCommodityStockAttribute()
	{
		return this.CommodityStockAttribute_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return CommodityStockAttribute_u value 类型为:short
	 * 
	 */
	public short getCommodityStockAttribute_u()
	{
		return CommodityStockAttribute_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCommodityStockAttribute_u(short value)
	{
		this.CommodityStockAttribute_u = value;
	}


	/**
	 * 获取商品多价
	 * 
	 * 此字段的版本 >= 0
	 * @return MultiPriceInfo value 类型为:Vector<MultiPrice>
	 * 
	 */
	public Vector<MultiPrice> getMultiPriceInfo()
	{
		return MultiPriceInfo;
	}


	/**
	 * 设置商品多价
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<MultiPrice>
	 * 
	 */
	public void setMultiPriceInfo(Vector<MultiPrice> value)
	{
		if (value != null) {
				this.MultiPriceInfo = value;
				this.MultiPriceInfo_u = 1;
		}
	}

	public boolean issetMultiPriceInfo()
	{
		return this.MultiPriceInfo_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return MultiPriceInfo_u value 类型为:short
	 * 
	 */
	public short getMultiPriceInfo_u()
	{
		return MultiPriceInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setMultiPriceInfo_u(short value)
	{
		this.MultiPriceInfo_u = value;
	}


	/**
	 * 获取��Ʒskuid
	 * 
	 * 此字段的版本 >= 1
	 * @return ItemSkuId value 类型为:long
	 * 
	 */
	public long getItemSkuId()
	{
		return ItemSkuId;
	}


	/**
	 * 设置��Ʒskuid
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:long
	 * 
	 */
	public void setItemSkuId(long value)
	{
		this.ItemSkuId = value;
		this.ItemSkuId_u = 1;
	}

	public boolean issetItemSkuId()
	{
		return this.ItemSkuId_u != 0;
	}
	/**
	 * 获取��Ʒskuid��ʶ
	 * 
	 * 此字段的版本 >= 1
	 * @return ItemSkuId_u value 类型为:short
	 * 
	 */
	public short getItemSkuId_u()
	{
		return ItemSkuId_u;
	}


	/**
	 * 设置��Ʒskuid��ʶ
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemSkuId_u(short value)
	{
		this.ItemSkuId_u = value;
	}


	/**
	 * 获取��Ʒ)չ��ʶ
	 * 
	 * 此字段的版本 >= 1
	 * @return mapCmdyReserves value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getMapCmdyReserves()
	{
		return mapCmdyReserves;
	}


	/**
	 * 设置��Ʒ)չ��ʶ
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setMapCmdyReserves(Map<String,String> value)
	{
		if (value != null) {
				this.mapCmdyReserves = value;
		}else{
				this.mapCmdyReserves = new HashMap<String,String>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cCmdyReserves_u value 类型为:short
	 * 
	 */
	public short getCCmdyReserves_u()
	{
		return cCmdyReserves_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCCmdyReserves_u(short value)
	{
		this.cCmdyReserves_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CommodityView)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PropertyMask的长度 size_of(uint32_t)
				length += 1;  //计算字段PropertyMask_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NormalPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段NormalPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RedPacketValue的长度 size_of(uint32_t)
				length += 1;  //计算字段RedPacketValue_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityScore的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityScore_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityMaxNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityMaxNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderMask的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderMask_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InvoiceType的长度 size_of(uint32_t)
				length += 1;  //计算字段InvoiceType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 1;  //计算字段DealType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NavigationNodeId的长度 size_of(uint32_t)
				length += 1;  //计算字段NavigationNodeId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityId, null);  //计算字段CommodityId的长度 size_of(String)
				length += 1;  //计算字段CommodityId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityTitle, null);  //计算字段CommodityTitle的长度 size_of(String)
				length += 1;  //计算字段CommodityTitle_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityLogo, null);  //计算字段CommodityLogo的长度 size_of(String)
				length += 1;  //计算字段CommodityLogo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityStockAttribute, null);  //计算字段CommodityStockAttribute的长度 size_of(String)
				length += 1;  //计算字段CommodityStockAttribute_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(MultiPriceInfo, null);  //计算字段MultiPriceInfo的长度 size_of(Vector)
				length += 1;  //计算字段MultiPriceInfo_u的长度 size_of(uint8_t)
				if(  this.Version >= 1 ){
						length += 17;  //计算字段ItemSkuId的长度 size_of(uint64_t)
				}
				if(  this.Version >= 1 ){
						length += 1;  //计算字段ItemSkuId_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 1 ){
						length += ByteStream.getObjectSize(mapCmdyReserves, null);  //计算字段mapCmdyReserves的长度 size_of(Map)
				}
				length += 1;  //计算字段cCmdyReserves_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(CommodityView)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段PropertyMask的长度 size_of(uint32_t)
				length += 1;  //计算字段PropertyMask_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NormalPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段NormalPrice_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段RedPacketValue的长度 size_of(uint32_t)
				length += 1;  //计算字段RedPacketValue_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityScore的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityScore_u的长度 size_of(uint8_t)
				length += 4;  //计算字段CommodityMaxNumber的长度 size_of(uint32_t)
				length += 1;  //计算字段CommodityMaxNumber_u的长度 size_of(uint8_t)
				length += 4;  //计算字段OrderMask的长度 size_of(uint32_t)
				length += 1;  //计算字段OrderMask_u的长度 size_of(uint8_t)
				length += 4;  //计算字段InvoiceType的长度 size_of(uint32_t)
				length += 1;  //计算字段InvoiceType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段DealType的长度 size_of(uint32_t)
				length += 1;  //计算字段DealType_u的长度 size_of(uint8_t)
				length += 4;  //计算字段NavigationNodeId的长度 size_of(uint32_t)
				length += 1;  //计算字段NavigationNodeId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityId, encoding);  //计算字段CommodityId的长度 size_of(String)
				length += 1;  //计算字段CommodityId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityTitle, encoding);  //计算字段CommodityTitle的长度 size_of(String)
				length += 1;  //计算字段CommodityTitle_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityLogo, encoding);  //计算字段CommodityLogo的长度 size_of(String)
				length += 1;  //计算字段CommodityLogo_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(CommodityStockAttribute, encoding);  //计算字段CommodityStockAttribute的长度 size_of(String)
				length += 1;  //计算字段CommodityStockAttribute_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(MultiPriceInfo, encoding);  //计算字段MultiPriceInfo的长度 size_of(Vector)
				length += 1;  //计算字段MultiPriceInfo_u的长度 size_of(uint8_t)
				if(  this.Version >= 1 ){
						length += 17;  //计算字段ItemSkuId的长度 size_of(uint64_t)
				}
				if(  this.Version >= 1 ){
						length += 1;  //计算字段ItemSkuId_u的长度 size_of(uint8_t)
				}
				if(  this.Version >= 1 ){
						length += ByteStream.getObjectSize(mapCmdyReserves, encoding);  //计算字段mapCmdyReserves的长度 size_of(Map)
				}
				length += 1;  //计算字段cCmdyReserves_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}


/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long Version;///<�汾��
 *	short Version_u;
 *	long PropertyMask;///<���Ա�ʶ
 *	short PropertyMask_u;
 *	long NormalPrice;///<��ͨ��
 *	short NormalPrice_u;
 *	long CommodityNumber;///<��Ʒ��
 *	short CommodityNumber_u;
 *	long RedPacketValue;///<�����ֵ
 *	short RedPacketValue_u;
 *	long CommodityScore;///<��Ʒ���
 *	short CommodityScore_u;
 *	long CommodityMaxNumber;///<��Ʒ������
 *	short CommodityMaxNumber_u;
 *	long OrderMask;///<������ʶ
 *	short OrderMask_u;
 *	long InvoiceType;///<��Ʊ����
 *	short InvoiceType_u;
 *	long DealType;///<��������
 *	short DealType_u;
 *	long NavigationNodeId;///<��Ŀ�����ڵ�id
 *	short NavigationNodeId_u;
 *	String CommodityId;///<��Ʒid
 *	short CommodityId_u;
 *	String CommodityTitle;///<��Ʒtitle
 *	short CommodityTitle_u;
 *	String CommodityLogo;///<��Ʒlogo
 *	short CommodityLogo_u;
 *	String CommodityStockAttribute;///<��Ʒ�������
 *	short CommodityStockAttribute_u;
 *	Vector<MultiPrice> MultiPriceInfo;///<��Ʒ���
 *	short MultiPriceInfo_u;
 *	long ItemSkuId;///<��Ʒskuid
 *	short ItemSkuId_u;///<��Ʒskuid��ʶ
 *	Map<String,String> mapCmdyReserves;///<��Ʒ)չ��ʶ
 *	short cCmdyReserves_u;
 *****以上是版本1所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
