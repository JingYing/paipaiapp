//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.cmdy.shopcartapi.java

package com.qq.qqbuy.thirdparty.idl.cmdy.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *商品信息
 *
 *@date 2013-03-08 09:40:21
 *
 *@since version:0
*/
public class CartItemData  implements ICanSerializeObject
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 数量
	 *
	 * 版本 >= 0
	 */
	 private long Num;

	/**
	 * 版本 >= 0
	 */
	 private short Num_u;

	/**
	 * 商品ID
	 *
	 * 版本 >= 0
	 */
	 private String ItemId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short ItemId_u;

	/**
	 * 属性
	 *
	 * 版本 >= 0
	 */
	 private String Attr = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Attr_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushUInt(Num);
		bs.pushUByte(Num_u);
		bs.pushString(ItemId);
		bs.pushUByte(ItemId_u);
		bs.pushString(Attr);
		bs.pushUByte(Attr_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Num = bs.popUInt();
		Num_u = bs.popUByte();
		ItemId = bs.popString();
		ItemId_u = bs.popUByte();
		Attr = bs.popString();
		Attr_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取数量
	 * 
	 * 此字段的版本 >= 0
	 * @return Num value 类型为:long
	 * 
	 */
	public long getNum()
	{
		return Num;
	}


	/**
	 * 设置数量
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNum(long value)
	{
		this.Num = value;
		this.Num_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Num_u value 类型为:short
	 * 
	 */
	public short getNum_u()
	{
		return Num_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setNum_u(short value)
	{
		this.Num_u = value;
	}


	/**
	 * 获取商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId value 类型为:String
	 * 
	 */
	public String getItemId()
	{
		return ItemId;
	}


	/**
	 * 设置商品ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemId(String value)
	{
		this.ItemId = value;
		this.ItemId_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return ItemId_u value 类型为:short
	 * 
	 */
	public short getItemId_u()
	{
		return ItemId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setItemId_u(short value)
	{
		this.ItemId_u = value;
	}


	/**
	 * 获取属性
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr value 类型为:String
	 * 
	 */
	public String getAttr()
	{
		return Attr;
	}


	/**
	 * 设置属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAttr(String value)
	{
		this.Attr = value;
		this.Attr_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Attr_u value 类型为:short
	 * 
	 */
	public short getAttr_u()
	{
		return Attr_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAttr_u(short value)
	{
		this.Attr_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(CartItemData)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Num的长度 size_of(uint32_t)
				length += 1;  //计算字段Num_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(ItemId);  //计算字段ItemId的长度 size_of(String)
				length += 1;  //计算字段ItemId_u的长度 size_of(uint8_t)
				if(Attr == null)
				    length += 4;
				else
				    length += 4+ Attr.getBytes("GBK").length;
				length += 1;  //计算字段Attr_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
