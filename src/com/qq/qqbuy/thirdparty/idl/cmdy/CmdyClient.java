package com.qq.qqbuy.thirdparty.idl.cmdy;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.qq.qqbuy.cmdy.po.CmdyItemListPo;
import com.qq.qqbuy.cmdy.po.CmdyMakeOrderPo;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.recvaddr.po.ReceiverAddress;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.AddCmdy2CartReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.AddCmdy2CartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ConfirmOrderReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ConfirmOrderResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.GetCmdyKindsReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.GetCmdyKindsResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MakeOrderReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.MakeOrderResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ModifyCmdyNumInCartReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ModifyCmdyNumInCartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.RmvCmdyFromCartReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.RmvCmdyFromCartResp;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ViewCartReq;
import com.qq.qqbuy.thirdparty.idl.cmdy.protocol.ViewCartResp;

/**
 * @author winsonwu
 * @Created 2013-1-30 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class CmdyClient extends SupportIDLBaseClient {

	protected static final String CMDY_CALL_IDL_MACHINEKEY = "mobileLife";
	protected static final String CMDY_CALL_IDL_SOURCE = "mobileLife"; // 用于IDL接口服务端判断是否进行免登陆处理
	protected static final String APP_CMDY_CALL_IDL_SOURCE = "appOrder"; // 用于IDL接口服务端判断是否进行免登陆处理
	protected static final String BETA_IP = "10.6.223.152";
	protected static final String DEV_IP = "10.6.222.233";
	protected static final int CMDY_PORT = 53101;

	protected static IAsynWebStub getWebStub4Deal() {
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		if (EnvManager.isLocal() || EnvManager.isGamma()) {
//			 stub.setIpAndPort(BETA_IP, CMDY_PORT);
			// stub.setIpAndPort("10.130.67.81", 53101);
		}
		return stub;
	}

	// 分单
	public static ConfirmOrderResp cmdyConfirmOrder(long buyerUin, String sk,
			CmdyItemListPo cmdyItemList, long regionId) {
		long start = System.currentTimeMillis();

		ConfirmOrderReq req = new ConfirmOrderReq();
		req.setReqLst(cmdyItemList.toIDLCartItemDataList());
		req.setRegionId(regionId);
		req.setMachineKey("" + start);
		req.setSource(CMDY_CALL_IDL_SOURCE);
		// req.setReqIp("10.130.67.81");

		ConfirmOrderResp resp = new ConfirmOrderResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setSkey(sk.getBytes());
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);

		long timeCost = System.currentTimeMillis() - start;
		String msg = resp.getResult() == 0 ? "IDL确认订单成功" : "IDL确认订单失败";
		return ret == SUCCESS ? resp : null;
	}
	
	// 分单
	public static ConfirmOrderResp cmdyConfirmOrderV2(long buyerUin, String sk,
			CmdyItemListPo cmdyItemList, long regionId) {
		long start = System.currentTimeMillis();

		ConfirmOrderReq req = new ConfirmOrderReq();
		req.setReqLst(cmdyItemList.toIDLCartItemDataList());
		req.setRegionId(regionId);
		req.setReserveIn("NewProcess");
		req.setMachineKey("" + start);
		req.setSource(CMDY_CALL_IDL_SOURCE);

		ConfirmOrderResp resp = new ConfirmOrderResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setSkey(sk.getBytes());
		stub.setOperator(buyerUin);
		stub.setRouteKey(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}

	// 下单
	public static MakeOrderResp cmdyMakeOrder(long buyerUin, String sk,
			CmdyMakeOrderPo po) {
//		long start = System.currentTimeMillis();

		MakeOrderReq req = new MakeOrderReq();
		req.setRequest(po.toIDLMakeOrderRequest());

		req.setMachineKey(APP_CMDY_CALL_IDL_SOURCE);
//		if(EnvManager.isGamma()){
			req.setSource(APP_CMDY_CALL_IDL_SOURCE);
//		}else{
//			req.setSource(CMDY_CALL_IDL_SOURCE);
//		}
		MakeOrderResp resp = new MakeOrderResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setSkey(sk.getBytes());
		stub.setOperator(buyerUin);
		stub.setRouteKey(buyerUin);
		/**
		 * gamma环境测试积分红包
		 */
		if(EnvManager.isGamma()){
			stub.setIpAndPort("10.130.0.237",53101);
		}
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}

	// 增加
	public static AddCmdy2CartResp addCmdy2Cart(long buyerUin, String itemCode,
			String itemAttr, long buyNum, boolean addForce, long secenid) {
//		long start = System.currentTimeMillis();

		AddCmdy2CartReq req = new AddCmdy2CartReq();
		req.setItemId(itemCode);
		req.setStockAttr(itemAttr);
		req.setNum(buyNum);
		req.setBAddForce(addForce);
		
//		if(EnvManager.isGamma()){
			req.setReserveIn(secenid + "");//场景价
//		}
		
		req.setMachineKey(CMDY_CALL_IDL_MACHINEKEY);
		req.setSource(CMDY_CALL_IDL_SOURCE);

		AddCmdy2CartResp resp = new AddCmdy2CartResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);

//		long timeCost = System.currentTimeMillis() - start;
//		String msg = resp.getResult() == 0 ? "IDL查询信息成功" : "IDL查询信息失败";
//		int reportRe = 0;
//		if (resp != null && resp.result == 33) {
//			reportRe = 0;
//		} else if (resp != null && resp.result == 34) {
//			reportRe = 0;
//		} else if (resp != null && resp.result == 35) {
//			reportRe = 0;
//		} else if (resp != null && resp.getResult() == 196) {
//			reportRe = 0;
//		} else if (resp != null && resp.getResult() == 519) {
//			reportRe = 0;
//		} else if (resp != null && resp.getResult() == 103) {
//			reportRe = 0;
//		} else if (resp != null && resp.getResult() == 432) {
//			reportRe = 0;
//		} else {
//			reportRe = (int) resp.getResult();
//		}
		return ret == SUCCESS ? resp : null;
	}

	// 删除
	public static RmvCmdyFromCartResp rmvCmdy4Cart(long buyerUin,
			CmdyItemListPo po) {
		long start = System.currentTimeMillis();
		RmvCmdyFromCartReq req = new RmvCmdyFromCartReq();
		req.setReqLst(po.toIDLCartItemDataList());

		req.setMachineKey(CMDY_CALL_IDL_MACHINEKEY);
		req.setSource(CMDY_CALL_IDL_SOURCE);

		RmvCmdyFromCartResp resp = new RmvCmdyFromCartResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}

	// 修改商品数量
	public static ModifyCmdyNumInCartResp modifyCmdyNum(long buyerUin,
			String itemCode, String itemAttr, int buyNum) {
//		long start = System.currentTimeMillis();

		ModifyCmdyNumInCartReq req = new ModifyCmdyNumInCartReq();
		req.setItemId(itemCode);
		req.setStockAttr(itemAttr);
		req.setNum(buyNum);

		req.setMachineKey(CMDY_CALL_IDL_MACHINEKEY);
		req.setSource(CMDY_CALL_IDL_SOURCE);

		ModifyCmdyNumInCartResp resp = new ModifyCmdyNumInCartResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}

	// 查询购物车商品信息
	public static ViewCartResp queryCmdy(long buyerUin, boolean opForce) {
//		long start = System.currentTimeMillis();

		ViewCartReq req = new ViewCartReq();
		req.setOpForce(opForce);

		req.setMachineKey(APP_CMDY_CALL_IDL_SOURCE);
		req.setSource(APP_CMDY_CALL_IDL_SOURCE);

		ViewCartResp resp = new ViewCartResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}

	public static GetCmdyKindsResp queryCmdyKinds(long buyerUin) {
//		long start = System.currentTimeMillis();

		GetCmdyKindsReq req = new GetCmdyKindsReq();

		req.setMachineKey(CMDY_CALL_IDL_MACHINEKEY);
		req.setSource(CMDY_CALL_IDL_SOURCE);

		GetCmdyKindsResp resp = new GetCmdyKindsResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setOperator(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);
		return ret == SUCCESS ? resp : null;
	}
	/**
	 * 京东收银台下单
	
	 * @Title: cmdyMakeOrderByJd 
	
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	
	 * @param @param buyerUin
	 * @param @param skey
	 * @param @param po
	 * @param @param defaultAddr
	 * @param @return    设定文件 
	
	 * @return MakeOrderResp    返回类型 
	
	 * @throws
	 */
	public static MakeOrderResp cmdyMakeOrderByJd(long buyerUin, String sk,
			CmdyMakeOrderPo po, ReceiverAddress defaultAddr) {
//		long start = System.currentTimeMillis();

		MakeOrderReq req = new MakeOrderReq();
		req.setRequest(po.toIDLMakeOrderRequest(defaultAddr));

		req.setMachineKey(CMDY_CALL_IDL_MACHINEKEY);
		if(EnvManager.isGamma()){
			req.setSource(APP_CMDY_CALL_IDL_SOURCE);
		}else{
			req.setSource(CMDY_CALL_IDL_SOURCE);
		}
//		req.setSceneId(sceneId);//娲诲姩鍦烘櫙
		MakeOrderResp resp = new MakeOrderResp();
		IAsynWebStub stub = getWebStub4Deal();
		stub.setSkey(sk.getBytes());
		stub.setOperator(buyerUin);
		stub.setRouteKey(buyerUin);
		int ret = invokePaiPaiIDL(req, resp, stub);

		return ret == SUCCESS ? resp : null;
	}

}
