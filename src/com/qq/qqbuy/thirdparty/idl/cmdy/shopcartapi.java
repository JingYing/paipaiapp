/***********************************************************
 * @author 		wakinzhang
 * @version 	1.0
 * @date 		2011.06.27
 * @modify      2013.01.22 by hailezhang
 ***********************************************************/
package com.qq.qqbuy.thirdparty.idl.cmdy;
import java.util.Vector;

import com.paipai.lang.MultiMap;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint64_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;
@HeadApiProtocol(cPlusNamespace="c2cent::ao::shopcartapi",needInit=true)
public class shopcartapi{
	@ApiProtocol(cmdid="0x91121801L",desc="购物车下单")
	class MakeOrder{
		@ApiProtocol(cmdid="0x91121801L",desc="购物车下单请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="场景id")
			uint32_t SceneId;
			@Field(desc="下单请求")
			MakeOrderRequest Request;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128801L",desc="购物车下单返回")
		class Resp{
			@Field(desc="下单返回")
			MakeOrderResponse Response;
			@Field(desc="错误信息")
			String ErrMsg;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="购物车下单请求",isSetClassSize=true,isNeedUFlag=true,version=0)
	class MakeOrderRequest{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="序列号")
		uint64_t SequenceId;
		uint8_t SequenceId_u;
		@Field(desc="验证码")
		String VerifyCode;
		uint8_t VerifyCode_u;
		@Field(desc="Refer")
		String Refer;
		uint8_t Refer_u;
		@Field(desc="收获地址")
		ReceiveAddress RecvAddr;
		uint8_t RecvAddr_u;
		@Field(desc="下单列表")
		Vector<OrderInformation> OrderInfo;
		uint8_t OrderInfo_u;
		@Field(desc="扩展信息")
		MultiMap<String, String> ExtInfo;
		uint8_t ExtInfo_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="购物车下单返回",isSetClassSize=true,isNeedUFlag=true,version=0)
	class MakeOrderResponse{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="订单展示")
		Vector<OrderView> OrderViewInfo;
		uint8_t OrderViewInfo_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="收货地址",isSetClassSize=true,isNeedUFlag=true,version=0)
	class ReceiveAddress{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="区域id")
		uint32_t RegionId;
		uint8_t RegionId_u;
		@Field(desc="地址id")
		uint32_t AddressId;
		uint8_t AddressId_u;
		@Field(desc="姓名")
		String Name;
		uint8_t Name_u;
		@Field(desc="手机")
		String Mobile;
		uint8_t Mobile_u;
		@Field(desc="电话")
		String Phone;
		uint8_t Phone_u;
		@Field(desc="邮编")
		String PostCode;
		uint8_t PostCode_u;
		@Field(desc="地址编码")
		String AddressCode;
		uint8_t AddressCode_u;
		@Field(desc="地址")
		String Address;
		uint8_t Address_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="下单信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class OrderInformation{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="交易信息")
		DealInformation DealInfo;
		uint8_t DealInfo_u;
		@Field(desc="店铺信息")
		ShopInformation ShopInfo;
		uint8_t ShopInfo_u;
		@Field(desc="商品信息列表")
		Vector<CommodityInformation> CmdyInfo;
		uint8_t CmdyInfo_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="交易信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class DealInformation{
		@Field(desc="版本号", defaultValue="1")
		uint32_t version;
		uint8_t version_u;
		@Field(desc="运送方式")
		uint32_t TransportType;
		uint8_t TransportType_u;
		@Field(desc="促销规则id")
		uint32_t PromotionRuleId;
		uint8_t PromotionRuleId_u;
		@Field(desc="发票类型")
		uint32_t InvoiceType;
		uint8_t InvoiceType_u;
		@Field(desc="订单类型")
		uint32_t DealType;
		uint8_t DealType_u;
		@Field(desc="指定费用")
		uint32_t SpecialFee;
		uint8_t SpecialFee_u;
		@Field(desc="指定费用类型")
		uint32_t SpecialFeeType;
		uint8_t SpecialFeeType_u;
		@Field(desc="套餐id")
		String ComboId;
		uint8_t ComboId_u;
		@Field(desc="订单留言")
		String DealNote;
		uint8_t DealNote_u;
		@Field(desc="发票title")
		String InvoiceTitle;
		uint8_t InvoiceTitle_u;
		
		@Field(desc="优惠券id列表，目前只支持一张优惠券", version=1)
        Vector<uint64_t> CouponId;
        @Field(desc="优惠券id列表标识", version=1)
        uint8_t CouponId_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="店铺信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class ShopInformation{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="卖家uin")
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc="卖家属性")
		uint32_t SellerProperty;
		uint8_t SellerProperty_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="商品信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CommodityInformation{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="商品数量")
		uint32_t CommodityNumber;
		uint8_t CommodityNumber_u;
		@Field(desc="价格类型")
		uint32_t PriceType;
		uint8_t PriceType_u;
		@Field(desc="红包id")
		uint32_t RedPacketId;
		uint8_t RedPacketId_u;
		@Field(desc="商品id")
		String CommodityId;
		uint8_t CommodityId_u;
		@Field(desc="库存属性")
		String StockAttribute;
		uint8_t StockAttribute_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="订单展示",isSetClassSize=true,isNeedUFlag=true,version=0)
	class OrderView{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="订单状态")
		uint32_t OrderResult;
		uint8_t OrderResult_u;
		@Field(desc="订单总费用")
		uint32_t TotalFee;
		uint8_t TotalFee_u;
		@Field(desc="订单id")
		String DealId;
		uint8_t DealId_u;
		@Field(desc="店铺展示")
		ShopView ShopViewInfo;
		uint8_t ShopViewInfo_u;
		@Field(desc="商品展示")
		Vector<CommodityView> CommodityViewInfo;
		uint8_t CommodityViewInfo_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="店铺展示",isSetClassSize=true,isNeedUFlag=true,version=0)
	class ShopView{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="卖家uin")
		uint32_t SellerUin;
		uint8_t SellerUin_u;
		@Field(desc="店铺属性")
		uint32_t ShopProperty;
		uint8_t ShopProperty_u;
		@Field(desc="店铺名称")
		String ShopName;
		uint8_t ShopName_u;
		@Field(desc="卖家昵称")
		String SellerNick;
		uint8_t SellerNick_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="商品展示",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CommodityView{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="属性标识")
		uint32_t PropertyMask;
		uint8_t PropertyMask_u;
		@Field(desc="普通价")
		uint32_t NormalPrice;
		uint8_t NormalPrice_u;
		@Field(desc="商品数量")
		uint32_t CommodityNumber;
		uint8_t CommodityNumber_u;
		@Field(desc="红包面值")
		uint32_t RedPacketValue;
		uint8_t RedPacketValue_u;
		@Field(desc="商品积分")
		uint32_t CommodityScore;
		uint8_t CommodityScore_u;
		@Field(desc="商品最大数据量")
		uint32_t CommodityMaxNumber;
		uint8_t CommodityMaxNumber_u;
		@Field(desc="订单标识")
		uint32_t OrderMask;
		uint8_t OrderMask_u;
		@Field(desc="发票类型")
		uint32_t InvoiceType;
		uint8_t InvoiceType_u;
		@Field(desc="订单类型")
		uint32_t DealType;
		uint8_t DealType_u;
		@Field(desc="类目导航节点id")
		uint32_t NavigationNodeId;
		uint8_t NavigationNodeId_u;
		@Field(desc="商品id")
		String CommodityId;
		uint8_t CommodityId_u;
		@Field(desc="商品title")
		String CommodityTitle;
		uint8_t CommodityTitle_u;
		@Field(desc="商品logo")
		String CommodityLogo;
		uint8_t CommodityLogo_u;
		@Field(desc="商品库存属性")
		String CommodityStockAttribute;
		uint8_t CommodityStockAttribute_u;
		@Field(desc="商品多价")
		Vector<MultiPrice> MultiPriceInfo;
		uint8_t MultiPriceInfo_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="商品多价",isSetClassSize=true,isNeedUFlag=true,version=0)
	class MultiPrice{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="特定价格")
		uint32_t ParticularPrice;
		uint8_t ParticularPrice_u;
		@Field(desc="价格类型")
		uint32_t ParticularType;
		uint8_t ParticularType_u;
	}

	@ApiProtocol(cmdid="0x91121802L",desc="添加商品到购物车")
	class AddCmdy2Cart{
		@ApiProtocol(cmdid="0x91121802L",desc="添加商品到购物车请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="商品Id")
			String ItemId;
			@Field(desc="库存属性")
			String StockAttr;
			@Field(desc="数量")
			uint32_t Num;
			@Field(desc="是否强制加入购物车")
			boolean bAddForce;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128802L",desc="添加商品到购物车返回")
		class Resp{
			@Field(desc="购物车中商品总数")
			uint32_t TotalNum;
			@Field(desc="购物车中商品总价格")
			uint32_t TotalFee;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}

	@ApiProtocol(cmdid="0x91121803L",desc="从购物车中删除商品")
	class RmvCmdyFromCart{
		@ApiProtocol(cmdid="0x91121803L",desc="从购物车中删除商品请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="要删除的商品列表")
			CartItemDataList ReqLst;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128803L",desc="从购物车中删除商品返回")
		class Resp{
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="商品列表",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CartItemDataList{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="支付方式")
		uint32_t PayType;
		uint8_t PayType_u;
		@Field(desc="商品列表")
		Vector<CartItemData> DataList;
		uint8_t DataList_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="商品信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CartItemData{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="数量")
		uint32_t Num;
		uint8_t Num_u;
		@Field(desc="商品ID")
		String ItemId;
		uint8_t ItemId_u;
		@Field(desc="属性")
		String Attr;
		uint8_t Attr_u;
	}

	@ApiProtocol(cmdid="0x91121804L",desc="修改购物车中商品数量")
	class ModifyCmdyNumInCart{
		@ApiProtocol(cmdid="0x91121804L",desc="修改购物车中商品数量请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="商品Id")
			String ItemId;
			@Field(desc="库存属性")
			String StockAttr;
			@Field(desc="商品数量")
			uint32_t Num;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128804L",desc="修改购物车中商品数量返回")
		class Resp{
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}

	@ApiProtocol(cmdid="0x91121805L",desc="查看购物车数据")
	class ViewCart{
		@ApiProtocol(cmdid="0x91121805L",desc="查看购物车数据请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="购物车满时是否需要合并数据")
			boolean OpForce;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128805L",desc="查看购物车数据返回")
		class Resp{
			@Field(desc="正常商品列表")
			DealViewDataList DealViewLst;
			@Field(desc="异常商品列表")
			DealViewDataList ProblemDealViewLst;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="订单列表",isSetClassSize=true,isNeedUFlag=true,version=0)
	class DealViewDataList{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="订单列表")
		Vector<DealViewData> DealViewDataLst;
		uint8_t DealViewDataLst_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="订单信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class DealViewData{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="返回结果")
		uint32_t Result;
		uint8_t Result_u;
		@Field(desc="促销活动信息")
		PromotionActive Promotionactive;
		uint8_t Promotionactive_u;
		@Field(desc="店铺信息")
		ShopView Shopview;
		uint8_t Shopview_u;
		@Field(desc="运费信息")
		ShipFeeInfo ShipfeeInfo;
		uint8_t ShipfeeInfo_u;
		@Field(desc="商品信息列表")
		Vector<CommodityView> CmdyViewList;
		uint8_t CmdyViewList_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="促销活动信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class PromotionActive{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="Uin")
		uint32_t Uin;
		uint8_t Uin_u;
		@Field(desc="促销活动开始时间")
		uint32_t StartTime;
		uint8_t StartTime_u;
		@Field(desc="促销活动结束时间")
		uint32_t EndTime;
		uint8_t EndTime_u;
		@Field(desc="促销活动描述")
		String Content;
		uint8_t Content_u;
		@Field(desc="商品类目")
		Vector<String> ShopClass;
		uint8_t ShopClass_u;
		@Field(desc="活动规则列表")
		Vector<PromotionRule> RuleList;
		uint8_t RuleList_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="促销规则信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class PromotionRule{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="规则ID")
		uint32_t RuleId;
		uint8_t RuleId_u;
		@Field(desc="限制类型：商品数量或价格")
		uint32_t LimitType;
		uint8_t LimitType_u;
		@Field(desc="最小限制")
		uint32_t LowLimit;
		uint8_t LowLimit_u;
		@Field(desc="促销标，促销类型")
		uint32_t PromotionMask;
		uint8_t PromotionMask_u;
		@Field(desc="促销折扣")
		uint32_t FreeMoney;
		uint8_t FreeMoney_u;
		@Field(desc="促销优惠的金额")
		uint32_t FreeRebate;
		uint8_t FreeRebate_u;
		@Field(desc="加价购")
		uint32_t BarterMoney;
		uint8_t BarterMoney_u;
		@Field(desc="促销规则描述")
		String Desc;
		uint8_t Desc_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="运费信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class ShipFeeInfo{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="谁支付运费")
		uint32_t WhoPayShipFee;
		uint8_t WhoPayShipFee_u;
		@Field(desc="平邮运费")
		uint32_t NormalShipFee;
		uint8_t NormalShipFee_u;
		@Field(desc="快递运费")
		uint32_t ExpressFee;
		uint8_t ExpressFee_u;
		@Field(desc="EMS运费")
		uint32_t EmsFee;
		uint8_t EmsFee_u;
		@Field(desc="运送类型")
		uint32_t ShipMask;
		uint8_t ShipMask_u;
	}

	@ApiProtocol(cmdid="0x91121806L",desc="购物车确认订单")
	class ConfirmOrder{
		@ApiProtocol(cmdid="0x91121806L",desc="购物车确认订单请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="请求分单的商品列表")
			CartItemDataList ReqLst;
			@Field(desc="区域ID")
			uint32_t RegionId;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128806L",desc="购物车确认订单返回")
		class Resp{
			@Field(desc="买家信息")
			CartBuyerInfo BuyerInfo;
			@Field(desc="正常商品分单列表")
			DealViewDataList dealViewLst;
			@Field(desc="异常商品分单列表")
			DealViewDataList ProblemDealViewLst;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="买家信息",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CartBuyerInfo{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="买家Uin")
		uint32_t BuyerUin;
		uint8_t BuyerUin_u;
		@Field(desc="序列号")
		uint32_t Seq;
		uint8_t Seq_u;
		@Field(desc="红包列表")
		CartRedPacketPoList RedPacketList;
		uint8_t RedPacketList_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="红包列表",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CartRedPacketPoList{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="红包列表")
		Vector<CartRedPacketPo> RedPacketPoList;
		uint8_t RedPacketPoList_u;
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="红包",isSetClassSize=true,isNeedUFlag=true,version=0)
	class CartRedPacketPo{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="所有者Uin")
		uint32_t Uin;
		uint8_t Uin_u;
		@Field(desc="红包编号")
		uint32_t PacketId;
		uint8_t PacketId_u;
		@Field(desc="红包类型标识（优惠券，包邮卡还是红包）")
		uint32_t Flag;
		uint8_t Flag_u;
		@Field(desc="红包标识")
		uint32_t Property;
		uint8_t Property_u;
		@Field(desc="面值")
		uint32_t Value;
		uint8_t Value_u;
		@Field(desc="最低消费")
		uint32_t Quota;
		uint8_t Quota_u;
		@Field(desc="到期时间")
		uint32_t EndTime;
		uint8_t EndTime_u;
		@Field(desc="保留字段")
		uint32_t Reserved;
		uint8_t Reserved_u;
		@Field(desc="红包名称")
		String Name;
		uint8_t Name_u;
		@Field(desc="可用的店铺")
		Vector<uint32_t> ValidShop;
		uint8_t ValidShop_u;
	}
	@ApiProtocol(cmdid="0x91121807L",desc="获取购物车商品属性")
	class GetCmdyKinds{
		@ApiProtocol(cmdid="0x91121807L",desc="获取购物车商品属性请求")
		class Req{
			@Field(desc="请求源")
			String Source;
			@Field(desc="请求ip")
			String ReqIp;
			@Field(desc="请求MachineKey")
			String MachineKey;
			@Field(desc="保留输入字段")
			String ReserveIn;
		}
		@ApiProtocol(cmdid="0x91128807L",desc="获取购物车商品属性返回")
		class Resp{
			@Field(desc="购物车商品属性")
			ShopCartCmdyAttr CmdyAttr;
			@Field(desc="保留输出字段")
			String ReserveOut;
		}
	}
	@Member(cPlusNamespace="c2cent::po::shopcartapi",desc="购物车商品属性",isSetClassSize=true,isNeedUFlag=true,version=0)
	class ShopCartCmdyAttr{
		@Field(desc="版本号")
		uint32_t Version;
		uint8_t Version_u;
		@Field(desc="商品种类数")
		uint32_t TotalKinds;
		uint8_t TotalKinds_u;
		@Field(desc="商品总数量")
		uint32_t TotalNum;
		uint8_t TotalNum_u;
		@Field(desc="保留字段")
		uint32_t ReserveInt;
		uint8_t ReserveInt_u;
		@Field(desc="保留字段")
		String ReserveStr;
		uint8_t ReserveStr_u;
	}
}
