//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.EvaluationService.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.uint32_t;
import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 *
 *@date 2013-07-09 10:14:40
 *
 *@since version:0
*/
public class EvalRecordPo  implements ICanSerializeObject
{
	/**
	 * 版本 >= 0
	 */
	 private long version = 4;

	/**
	 * 版本 >= 0
	 */
	 private long dwFDealId;

	/**
	 * 版本 >= 0
	 */
	 private long dwBuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private long dwSellerUin;

	/**
	 * 版本 >= 0
	 */
	 private long dwBuyerEvalTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwSellerEvalTime;

	/**
	 * 版本 >= 0
	 */
	 private int iBuyerScore;

	/**
	 * 版本 >= 0
	 */
	 private int iSellerScore;

	/**
	 * 版本 >= 0
	 */
	 private long dwDealPayment;

	/**
	 * 版本 >= 0
	 */
	 private long dwScoreTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwPrice;

	/**
	 * 版本 >= 0
	 */
	 private long dwEvalCreateTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwDealFinishTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwPropertyMask;

	/**
	 * 版本 >= 0
	 */
	 private long dwLastUpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private long dwDealPayTime;

	/**
	 * 版本 >= 0
	 */
	 private String strDealId = "";

	/**
	 * 版本 >= 0
	 */
	 private String strCommodityId = "";

	/**
	 * 版本 >= 0
	 */
	 private String strCommodityLogo = "";

	/**
	 * 版本 >= 0
	 */
	 private String strCommodityTitle = "";

	/**
	 * 版本 >= 0
	 */
	 private String strSnapId = "";

	/**
	 * 版本 >= 0
	 */
	 private String strSellerNickName = "";

	/**
	 * 版本 >= 0
	 */
	 private String strBuyerNickName = "";

	/**
	 * 版本 >= 0
	 */
	 private String strBuyerExplain = "";

	/**
	 * 版本 >= 0
	 */
	 private String strSellerExplain = "";

	/**
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecBuyerReason = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private Vector<uint32_t> vecSellerReason = new Vector<uint32_t>();

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerEvalLevel;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerEvalLevel;

	/**
	 * 版本 >= 0
	 */
	 private short cTimeOutClose;

	/**
	 * 版本 >= 0
	 */
	 private short cScoreFlag;

	/**
	 * 版本 >= 0
	 */
	 private short cPayType;

	/**
	 * 版本 >= 0
	 */
	 private short cEvalStatus;

	/**
	 * 版本 >= 0
	 */
	 private short cNumSellerCanModEval;

	/**
	 * 版本 >= 0
	 */
	 private short cNumBuyerCanModEval;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerEvalDelFlag;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerEvalDelFlag;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyUsrDelFlag;

	/**
	 * 版本 >= 0
	 */
	 private short cSellUsrDelFlag;

	/**
	 * 版本 >= 0
	 */
	 private short cNumBuyerCanReply;

	/**
	 * 版本 >= 0
	 */
	 private short cNumSellerCanReply;

	/**
	 * 版本 >= 0
	 */
	 private short cFDealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cDealId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cCommodityId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cCommodityLogo_u;

	/**
	 * 版本 >= 0
	 */
	 private short cCommodityTitle_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSnapId_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerUin_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerNickName_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerNickName_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerEvalTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerEvalTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerExplain_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerExplain_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerScore_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerScore_u;

	/**
	 * 版本 >= 0
	 */
	 private short cDealPayment_u;

	/**
	 * 版本 >= 0
	 */
	 private short cScoreTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cPrice_u;

	/**
	 * 版本 >= 0
	 */
	 private short cEvalCreateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cDealFinishTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cPropertyMask_u;

	/**
	 * 版本 >= 0
	 */
	 private short cLastUpdateTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cDealPayTime_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerEvalLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerEvalLevel_u;

	/**
	 * 版本 >= 0
	 */
	 private short cTimeOutClose_u;

	/**
	 * 版本 >= 0
	 */
	 private short cScoreFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short cPayType_u;

	/**
	 * 版本 >= 0
	 */
	 private short cEvalStatus_u;

	/**
	 * 版本 >= 0
	 */
	 private short cNumSellerCanModEval_u;

	/**
	 * 版本 >= 0
	 */
	 private short cNumBuyerCanModEval_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyerEvalDelFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellerEvalDelFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short cBuyUsrDelFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short cSellUsrDelFlag_u;

	/**
	 * 版本 >= 0
	 */
	 private short cNumBuyerCanReply_u;

	/**
	 * 版本 >= 0
	 */
	 private short cNumSellerCanReply_u;

	/**
	 * 版本 >= 0
	 */
	 private Vector<EvalReplyPo> vecReplyMsg = new Vector<EvalReplyPo>();

	/**
	 * 
	 *
	 * 版本 >= 1
	 */
	 private String strItemNavInfo = new String();

	/**
	 * 
	 *
	 * 版本 >= 1
	 */
	 private String cItemNavInfo_u = new String();

	/**
	 * 
	 *
	 * 版本 >= 2
	 */
	 private String strFtradeId = new String();

	/**
	 * 
	 *
	 * 版本 >= 2
	 */
	 private short cFtradeId_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwDsr1;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwDsr2;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwDsr3;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cDsr1_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cDsr2_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cDsr3_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwSellerLevel;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cSellerLevel_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwDsrEvalTime;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private long dwDsrFlag;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cDsrEvalTime_u;

	/**
	 * 
	 *
	 * 版本 >= 3
	 */
	 private short cDsrFlag_u;

	/**
	 * 
	 *
	 * 版本 >= 4
	 */
	 private long dwResetTime;

	/**
	 * 
	 *
	 * 版本 >= 4
	 */
	 private long cResetTime_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize() - 4);
		bs.pushUInt(version);
		bs.pushUInt(dwFDealId);
		bs.pushUInt(dwBuyerUin);
		bs.pushUInt(dwSellerUin);
		bs.pushUInt(dwBuyerEvalTime);
		bs.pushUInt(dwSellerEvalTime);
		bs.pushInt(iBuyerScore);
		bs.pushInt(iSellerScore);
		bs.pushUInt(dwDealPayment);
		bs.pushUInt(dwScoreTime);
		bs.pushUInt(dwPrice);
		bs.pushUInt(dwEvalCreateTime);
		bs.pushUInt(dwDealFinishTime);
		bs.pushUInt(dwPropertyMask);
		bs.pushUInt(dwLastUpdateTime);
		bs.pushUInt(dwDealPayTime);
		bs.pushString(strDealId);
		bs.pushString(strCommodityId);
		bs.pushString(strCommodityLogo);
		bs.pushString(strCommodityTitle);
		bs.pushString(strSnapId);
		bs.pushString(strSellerNickName);
		bs.pushString(strBuyerNickName);
		bs.pushString(strBuyerExplain);
		bs.pushString(strSellerExplain);
		bs.pushObject(vecBuyerReason);
		bs.pushObject(vecSellerReason);
		bs.pushUByte(cBuyerEvalLevel);
		bs.pushUByte(cSellerEvalLevel);
		bs.pushUByte(cTimeOutClose);
		bs.pushUByte(cScoreFlag);
		bs.pushUByte(cPayType);
		bs.pushUByte(cEvalStatus);
		bs.pushUByte(cNumSellerCanModEval);
		bs.pushUByte(cNumBuyerCanModEval);
		bs.pushUByte(cBuyerEvalDelFlag);
		bs.pushUByte(cSellerEvalDelFlag);
		bs.pushUByte(cBuyUsrDelFlag);
		bs.pushUByte(cSellUsrDelFlag);
		bs.pushUByte(cNumBuyerCanReply);
		bs.pushUByte(cNumSellerCanReply);
		bs.pushUByte(cFDealId_u);
		bs.pushUByte(cDealId_u);
		bs.pushUByte(cCommodityId_u);
		bs.pushUByte(cCommodityLogo_u);
		bs.pushUByte(cCommodityTitle_u);
		bs.pushUByte(cSnapId_u);
		bs.pushUByte(cBuyerUin_u);
		bs.pushUByte(cSellerUin_u);
		bs.pushUByte(cSellerNickName_u);
		bs.pushUByte(cBuyerNickName_u);
		bs.pushUByte(cBuyerEvalTime_u);
		bs.pushUByte(cSellerEvalTime_u);
		bs.pushUByte(cBuyerExplain_u);
		bs.pushUByte(cSellerExplain_u);
		bs.pushUByte(cBuyerScore_u);
		bs.pushUByte(cSellerScore_u);
		bs.pushUByte(cDealPayment_u);
		bs.pushUByte(cScoreTime_u);
		bs.pushUByte(cPrice_u);
		bs.pushUByte(cEvalCreateTime_u);
		bs.pushUByte(cDealFinishTime_u);
		bs.pushUByte(cPropertyMask_u);
		bs.pushUByte(cLastUpdateTime_u);
		bs.pushUByte(cDealPayTime_u);
		bs.pushUByte(cBuyerEvalLevel_u);
		bs.pushUByte(cSellerEvalLevel_u);
		bs.pushUByte(cTimeOutClose_u);
		bs.pushUByte(cScoreFlag_u);
		bs.pushUByte(cPayType_u);
		bs.pushUByte(cEvalStatus_u);
		bs.pushUByte(cNumSellerCanModEval_u);
		bs.pushUByte(cNumBuyerCanModEval_u);
		bs.pushUByte(cBuyerEvalDelFlag_u);
		bs.pushUByte(cSellerEvalDelFlag_u);
		bs.pushUByte(cBuyUsrDelFlag_u);
		bs.pushUByte(cSellUsrDelFlag_u);
		bs.pushUByte(cNumBuyerCanReply_u);
		bs.pushUByte(cNumSellerCanReply_u);
		bs.pushObject(vecReplyMsg);
		if(  this.version >= 1 ){
				bs.pushString(strItemNavInfo);
		}
		if(  this.version >= 1 ){
				bs.pushString(cItemNavInfo_u);
		}
		if(  this.version >= 2 ){
				bs.pushString(strFtradeId);
		}
		if(  this.version >= 2 ){
				bs.pushUByte(cFtradeId_u);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwDsr1);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwDsr2);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwDsr3);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cDsr1_u);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cDsr2_u);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cDsr3_u);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwSellerLevel);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cSellerLevel_u);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwDsrEvalTime);
		}
		if(  this.version >= 3 ){
				bs.pushUInt(dwDsrFlag);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cDsrEvalTime_u);
		}
		if(  this.version >= 3 ){
				bs.pushUByte(cDsrFlag_u);
		}
		if(  this.version >= 4 ){
				bs.pushUInt(dwResetTime);
		}
		if(  this.version >= 4 ){
				bs.pushUInt(cResetTime_u);
		}
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		dwFDealId = bs.popUInt();
		dwBuyerUin = bs.popUInt();
		dwSellerUin = bs.popUInt();
		dwBuyerEvalTime = bs.popUInt();
		dwSellerEvalTime = bs.popUInt();
		iBuyerScore = bs.popInt();
		iSellerScore = bs.popInt();
		dwDealPayment = bs.popUInt();
		dwScoreTime = bs.popUInt();
		dwPrice = bs.popUInt();
		dwEvalCreateTime = bs.popUInt();
		dwDealFinishTime = bs.popUInt();
		dwPropertyMask = bs.popUInt();
		dwLastUpdateTime = bs.popUInt();
		dwDealPayTime = bs.popUInt();
		strDealId = bs.popString();
		strCommodityId = bs.popString();
		strCommodityLogo = bs.popString();
		strCommodityTitle = bs.popString();
		strSnapId = bs.popString();
		strSellerNickName = bs.popString();
		strBuyerNickName = bs.popString();
		strBuyerExplain = bs.popString();
		strSellerExplain = bs.popString();
		vecBuyerReason = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		vecSellerReason = (Vector<uint32_t>)bs.popVector(uint32_t.class);
		cBuyerEvalLevel = bs.popUByte();
		cSellerEvalLevel = bs.popUByte();
		cTimeOutClose = bs.popUByte();
		cScoreFlag = bs.popUByte();
		cPayType = bs.popUByte();
		cEvalStatus = bs.popUByte();
		cNumSellerCanModEval = bs.popUByte();
		cNumBuyerCanModEval = bs.popUByte();
		cBuyerEvalDelFlag = bs.popUByte();
		cSellerEvalDelFlag = bs.popUByte();
		cBuyUsrDelFlag = bs.popUByte();
		cSellUsrDelFlag = bs.popUByte();
		cNumBuyerCanReply = bs.popUByte();
		cNumSellerCanReply = bs.popUByte();
		cFDealId_u = bs.popUByte();
		cDealId_u = bs.popUByte();
		cCommodityId_u = bs.popUByte();
		cCommodityLogo_u = bs.popUByte();
		cCommodityTitle_u = bs.popUByte();
		cSnapId_u = bs.popUByte();
		cBuyerUin_u = bs.popUByte();
		cSellerUin_u = bs.popUByte();
		cSellerNickName_u = bs.popUByte();
		cBuyerNickName_u = bs.popUByte();
		cBuyerEvalTime_u = bs.popUByte();
		cSellerEvalTime_u = bs.popUByte();
		cBuyerExplain_u = bs.popUByte();
		cSellerExplain_u = bs.popUByte();
		cBuyerScore_u = bs.popUByte();
		cSellerScore_u = bs.popUByte();
		cDealPayment_u = bs.popUByte();
		cScoreTime_u = bs.popUByte();
		cPrice_u = bs.popUByte();
		cEvalCreateTime_u = bs.popUByte();
		cDealFinishTime_u = bs.popUByte();
		cPropertyMask_u = bs.popUByte();
		cLastUpdateTime_u = bs.popUByte();
		cDealPayTime_u = bs.popUByte();
		cBuyerEvalLevel_u = bs.popUByte();
		cSellerEvalLevel_u = bs.popUByte();
		cTimeOutClose_u = bs.popUByte();
		cScoreFlag_u = bs.popUByte();
		cPayType_u = bs.popUByte();
		cEvalStatus_u = bs.popUByte();
		cNumSellerCanModEval_u = bs.popUByte();
		cNumBuyerCanModEval_u = bs.popUByte();
		cBuyerEvalDelFlag_u = bs.popUByte();
		cSellerEvalDelFlag_u = bs.popUByte();
		cBuyUsrDelFlag_u = bs.popUByte();
		cSellUsrDelFlag_u = bs.popUByte();
		cNumBuyerCanReply_u = bs.popUByte();
		cNumSellerCanReply_u = bs.popUByte();
		vecReplyMsg = (Vector<EvalReplyPo>)bs.popVector(EvalReplyPo.class);
		if(  this.version >= 1 ){
				strItemNavInfo = bs.popString();
		}
		if(  this.version >= 1 ){
				cItemNavInfo_u = bs.popString();
		}
		if(  this.version >= 2 ){
				strFtradeId = bs.popString();
		}
		if(  this.version >= 2 ){
				cFtradeId_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				dwDsr1 = bs.popUInt();
		}
		if(  this.version >= 3 ){
				dwDsr2 = bs.popUInt();
		}
		if(  this.version >= 3 ){
				dwDsr3 = bs.popUInt();
		}
		if(  this.version >= 3 ){
				cDsr1_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				cDsr2_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				cDsr3_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				dwSellerLevel = bs.popUInt();
		}
		if(  this.version >= 3 ){
				cSellerLevel_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				dwDsrEvalTime = bs.popUInt();
		}
		if(  this.version >= 3 ){
				dwDsrFlag = bs.popUInt();
		}
		if(  this.version >= 3 ){
				cDsrEvalTime_u = bs.popUByte();
		}
		if(  this.version >= 3 ){
				cDsrFlag_u = bs.popUByte();
		}
		if(  this.version >= 4 ){
				dwResetTime = bs.popUInt();
		}
		if(  this.version >= 4 ){
				cResetTime_u = bs.popUInt();
		}

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwFDealId value 类型为:long
	 * 
	 */
	public long getDwFDealId()
	{
		return dwFDealId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwFDealId(long value)
	{
		this.dwFDealId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin()
	{
		return dwBuyerUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value)
	{
		this.dwBuyerUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerUin value 类型为:long
	 * 
	 */
	public long getDwSellerUin()
	{
		return dwSellerUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerUin(long value)
	{
		this.dwSellerUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerEvalTime value 类型为:long
	 * 
	 */
	public long getDwBuyerEvalTime()
	{
		return dwBuyerEvalTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerEvalTime(long value)
	{
		this.dwBuyerEvalTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerEvalTime value 类型为:long
	 * 
	 */
	public long getDwSellerEvalTime()
	{
		return dwSellerEvalTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerEvalTime(long value)
	{
		this.dwSellerEvalTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return iBuyerScore value 类型为:int
	 * 
	 */
	public int getIBuyerScore()
	{
		return iBuyerScore;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setIBuyerScore(int value)
	{
		this.iBuyerScore = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return iSellerScore value 类型为:int
	 * 
	 */
	public int getISellerScore()
	{
		return iSellerScore;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setISellerScore(int value)
	{
		this.iSellerScore = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwDealPayment value 类型为:long
	 * 
	 */
	public long getDwDealPayment()
	{
		return dwDealPayment;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDealPayment(long value)
	{
		this.dwDealPayment = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwScoreTime value 类型为:long
	 * 
	 */
	public long getDwScoreTime()
	{
		return dwScoreTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwScoreTime(long value)
	{
		this.dwScoreTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPrice value 类型为:long
	 * 
	 */
	public long getDwPrice()
	{
		return dwPrice;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPrice(long value)
	{
		this.dwPrice = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwEvalCreateTime value 类型为:long
	 * 
	 */
	public long getDwEvalCreateTime()
	{
		return dwEvalCreateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwEvalCreateTime(long value)
	{
		this.dwEvalCreateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwDealFinishTime value 类型为:long
	 * 
	 */
	public long getDwDealFinishTime()
	{
		return dwDealFinishTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDealFinishTime(long value)
	{
		this.dwDealFinishTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwPropertyMask value 类型为:long
	 * 
	 */
	public long getDwPropertyMask()
	{
		return dwPropertyMask;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwPropertyMask(long value)
	{
		this.dwPropertyMask = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwLastUpdateTime value 类型为:long
	 * 
	 */
	public long getDwLastUpdateTime()
	{
		return dwLastUpdateTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwLastUpdateTime(long value)
	{
		this.dwLastUpdateTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwDealPayTime value 类型为:long
	 * 
	 */
	public long getDwDealPayTime()
	{
		return dwDealPayTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDealPayTime(long value)
	{
		this.dwDealPayTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strDealId value 类型为:String
	 * 
	 */
	public String getStrDealId()
	{
		return strDealId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrDealId(String value)
	{
		this.strDealId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCommodityId value 类型为:String
	 * 
	 */
	public String getStrCommodityId()
	{
		return strCommodityId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCommodityId(String value)
	{
		this.strCommodityId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCommodityLogo value 类型为:String
	 * 
	 */
	public String getStrCommodityLogo()
	{
		return strCommodityLogo;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCommodityLogo(String value)
	{
		this.strCommodityLogo = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strCommodityTitle value 类型为:String
	 * 
	 */
	public String getStrCommodityTitle()
	{
		return strCommodityTitle;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrCommodityTitle(String value)
	{
		this.strCommodityTitle = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strSnapId value 类型为:String
	 * 
	 */
	public String getStrSnapId()
	{
		return strSnapId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrSnapId(String value)
	{
		this.strSnapId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strSellerNickName value 类型为:String
	 * 
	 */
	public String getStrSellerNickName()
	{
		return strSellerNickName;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrSellerNickName(String value)
	{
		this.strSellerNickName = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strBuyerNickName value 类型为:String
	 * 
	 */
	public String getStrBuyerNickName()
	{
		return strBuyerNickName;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrBuyerNickName(String value)
	{
		this.strBuyerNickName = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strBuyerExplain value 类型为:String
	 * 
	 */
	public String getStrBuyerExplain()
	{
		return strBuyerExplain;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrBuyerExplain(String value)
	{
		this.strBuyerExplain = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strSellerExplain value 类型为:String
	 * 
	 */
	public String getStrSellerExplain()
	{
		return strSellerExplain;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrSellerExplain(String value)
	{
		this.strSellerExplain = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecBuyerReason value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecBuyerReason()
	{
		return vecBuyerReason;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecBuyerReason(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecBuyerReason = value;
		}else{
				this.vecBuyerReason = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecSellerReason value 类型为:Vector<uint32_t>
	 * 
	 */
	public Vector<uint32_t> getVecSellerReason()
	{
		return vecSellerReason;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint32_t>
	 * 
	 */
	public void setVecSellerReason(Vector<uint32_t> value)
	{
		if (value != null) {
				this.vecSellerReason = value;
		}else{
				this.vecSellerReason = new Vector<uint32_t>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerEvalLevel value 类型为:short
	 * 
	 */
	public short getCBuyerEvalLevel()
	{
		return cBuyerEvalLevel;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerEvalLevel(short value)
	{
		this.cBuyerEvalLevel = value;
		this.cBuyerEvalLevel_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerEvalLevel value 类型为:short
	 * 
	 */
	public short getCSellerEvalLevel()
	{
		return cSellerEvalLevel;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerEvalLevel(short value)
	{
		this.cSellerEvalLevel = value;
		this.cSellerEvalLevel_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cTimeOutClose value 类型为:short
	 * 
	 */
	public short getCTimeOutClose()
	{
		return cTimeOutClose;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCTimeOutClose(short value)
	{
		this.cTimeOutClose = value;
		this.cTimeOutClose_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cScoreFlag value 类型为:short
	 * 
	 */
	public short getCScoreFlag()
	{
		return cScoreFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCScoreFlag(short value)
	{
		this.cScoreFlag = value;
		this.cScoreFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cPayType value 类型为:short
	 * 
	 */
	public short getCPayType()
	{
		return cPayType;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCPayType(short value)
	{
		this.cPayType = value;
		this.cPayType_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cEvalStatus value 类型为:short
	 * 
	 */
	public short getCEvalStatus()
	{
		return cEvalStatus;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCEvalStatus(short value)
	{
		this.cEvalStatus = value;
		this.cEvalStatus_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumSellerCanModEval value 类型为:short
	 * 
	 */
	public short getCNumSellerCanModEval()
	{
		return cNumSellerCanModEval;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumSellerCanModEval(short value)
	{
		this.cNumSellerCanModEval = value;
		this.cNumSellerCanModEval_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumBuyerCanModEval value 类型为:short
	 * 
	 */
	public short getCNumBuyerCanModEval()
	{
		return cNumBuyerCanModEval;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumBuyerCanModEval(short value)
	{
		this.cNumBuyerCanModEval = value;
		this.cNumBuyerCanModEval_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerEvalDelFlag value 类型为:short
	 * 
	 */
	public short getCBuyerEvalDelFlag()
	{
		return cBuyerEvalDelFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerEvalDelFlag(short value)
	{
		this.cBuyerEvalDelFlag = value;
		this.cBuyerEvalDelFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerEvalDelFlag value 类型为:short
	 * 
	 */
	public short getCSellerEvalDelFlag()
	{
		return cSellerEvalDelFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerEvalDelFlag(short value)
	{
		this.cSellerEvalDelFlag = value;
		this.cSellerEvalDelFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyUsrDelFlag value 类型为:short
	 * 
	 */
	public short getCBuyUsrDelFlag()
	{
		return cBuyUsrDelFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyUsrDelFlag(short value)
	{
		this.cBuyUsrDelFlag = value;
		this.cBuyUsrDelFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellUsrDelFlag value 类型为:short
	 * 
	 */
	public short getCSellUsrDelFlag()
	{
		return cSellUsrDelFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellUsrDelFlag(short value)
	{
		this.cSellUsrDelFlag = value;
		this.cSellUsrDelFlag_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumBuyerCanReply value 类型为:short
	 * 
	 */
	public short getCNumBuyerCanReply()
	{
		return cNumBuyerCanReply;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumBuyerCanReply(short value)
	{
		this.cNumBuyerCanReply = value;
		this.cNumBuyerCanReply_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumSellerCanReply value 类型为:short
	 * 
	 */
	public short getCNumSellerCanReply()
	{
		return cNumSellerCanReply;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumSellerCanReply(short value)
	{
		this.cNumSellerCanReply = value;
		this.cNumSellerCanReply_u = 1;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cFDealId_u value 类型为:short
	 * 
	 */
	public short getCFDealId_u()
	{
		return cFDealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCFDealId_u(short value)
	{
		this.cFDealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cDealId_u value 类型为:short
	 * 
	 */
	public short getCDealId_u()
	{
		return cDealId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDealId_u(short value)
	{
		this.cDealId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cCommodityId_u value 类型为:short
	 * 
	 */
	public short getCCommodityId_u()
	{
		return cCommodityId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCCommodityId_u(short value)
	{
		this.cCommodityId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cCommodityLogo_u value 类型为:short
	 * 
	 */
	public short getCCommodityLogo_u()
	{
		return cCommodityLogo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCCommodityLogo_u(short value)
	{
		this.cCommodityLogo_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cCommodityTitle_u value 类型为:short
	 * 
	 */
	public short getCCommodityTitle_u()
	{
		return cCommodityTitle_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCCommodityTitle_u(short value)
	{
		this.cCommodityTitle_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSnapId_u value 类型为:short
	 * 
	 */
	public short getCSnapId_u()
	{
		return cSnapId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSnapId_u(short value)
	{
		this.cSnapId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerUin_u value 类型为:short
	 * 
	 */
	public short getCBuyerUin_u()
	{
		return cBuyerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerUin_u(short value)
	{
		this.cBuyerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerUin_u value 类型为:short
	 * 
	 */
	public short getCSellerUin_u()
	{
		return cSellerUin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerUin_u(short value)
	{
		this.cSellerUin_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerNickName_u value 类型为:short
	 * 
	 */
	public short getCSellerNickName_u()
	{
		return cSellerNickName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerNickName_u(short value)
	{
		this.cSellerNickName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerNickName_u value 类型为:short
	 * 
	 */
	public short getCBuyerNickName_u()
	{
		return cBuyerNickName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerNickName_u(short value)
	{
		this.cBuyerNickName_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerEvalTime_u value 类型为:short
	 * 
	 */
	public short getCBuyerEvalTime_u()
	{
		return cBuyerEvalTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerEvalTime_u(short value)
	{
		this.cBuyerEvalTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerEvalTime_u value 类型为:short
	 * 
	 */
	public short getCSellerEvalTime_u()
	{
		return cSellerEvalTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerEvalTime_u(short value)
	{
		this.cSellerEvalTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerExplain_u value 类型为:short
	 * 
	 */
	public short getCBuyerExplain_u()
	{
		return cBuyerExplain_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerExplain_u(short value)
	{
		this.cBuyerExplain_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerExplain_u value 类型为:short
	 * 
	 */
	public short getCSellerExplain_u()
	{
		return cSellerExplain_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerExplain_u(short value)
	{
		this.cSellerExplain_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerScore_u value 类型为:short
	 * 
	 */
	public short getCBuyerScore_u()
	{
		return cBuyerScore_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerScore_u(short value)
	{
		this.cBuyerScore_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerScore_u value 类型为:short
	 * 
	 */
	public short getCSellerScore_u()
	{
		return cSellerScore_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerScore_u(short value)
	{
		this.cSellerScore_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cDealPayment_u value 类型为:short
	 * 
	 */
	public short getCDealPayment_u()
	{
		return cDealPayment_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDealPayment_u(short value)
	{
		this.cDealPayment_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cScoreTime_u value 类型为:short
	 * 
	 */
	public short getCScoreTime_u()
	{
		return cScoreTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCScoreTime_u(short value)
	{
		this.cScoreTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cPrice_u value 类型为:short
	 * 
	 */
	public short getCPrice_u()
	{
		return cPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCPrice_u(short value)
	{
		this.cPrice_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cEvalCreateTime_u value 类型为:short
	 * 
	 */
	public short getCEvalCreateTime_u()
	{
		return cEvalCreateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCEvalCreateTime_u(short value)
	{
		this.cEvalCreateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cDealFinishTime_u value 类型为:short
	 * 
	 */
	public short getCDealFinishTime_u()
	{
		return cDealFinishTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDealFinishTime_u(short value)
	{
		this.cDealFinishTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cPropertyMask_u value 类型为:short
	 * 
	 */
	public short getCPropertyMask_u()
	{
		return cPropertyMask_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCPropertyMask_u(short value)
	{
		this.cPropertyMask_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cLastUpdateTime_u value 类型为:short
	 * 
	 */
	public short getCLastUpdateTime_u()
	{
		return cLastUpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCLastUpdateTime_u(short value)
	{
		this.cLastUpdateTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cDealPayTime_u value 类型为:short
	 * 
	 */
	public short getCDealPayTime_u()
	{
		return cDealPayTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDealPayTime_u(short value)
	{
		this.cDealPayTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerEvalLevel_u value 类型为:short
	 * 
	 */
	public short getCBuyerEvalLevel_u()
	{
		return cBuyerEvalLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerEvalLevel_u(short value)
	{
		this.cBuyerEvalLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerEvalLevel_u value 类型为:short
	 * 
	 */
	public short getCSellerEvalLevel_u()
	{
		return cSellerEvalLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerEvalLevel_u(short value)
	{
		this.cSellerEvalLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cTimeOutClose_u value 类型为:short
	 * 
	 */
	public short getCTimeOutClose_u()
	{
		return cTimeOutClose_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCTimeOutClose_u(short value)
	{
		this.cTimeOutClose_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cScoreFlag_u value 类型为:short
	 * 
	 */
	public short getCScoreFlag_u()
	{
		return cScoreFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCScoreFlag_u(short value)
	{
		this.cScoreFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cPayType_u value 类型为:short
	 * 
	 */
	public short getCPayType_u()
	{
		return cPayType_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCPayType_u(short value)
	{
		this.cPayType_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cEvalStatus_u value 类型为:short
	 * 
	 */
	public short getCEvalStatus_u()
	{
		return cEvalStatus_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCEvalStatus_u(short value)
	{
		this.cEvalStatus_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumSellerCanModEval_u value 类型为:short
	 * 
	 */
	public short getCNumSellerCanModEval_u()
	{
		return cNumSellerCanModEval_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumSellerCanModEval_u(short value)
	{
		this.cNumSellerCanModEval_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumBuyerCanModEval_u value 类型为:short
	 * 
	 */
	public short getCNumBuyerCanModEval_u()
	{
		return cNumBuyerCanModEval_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumBuyerCanModEval_u(short value)
	{
		this.cNumBuyerCanModEval_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyerEvalDelFlag_u value 类型为:short
	 * 
	 */
	public short getCBuyerEvalDelFlag_u()
	{
		return cBuyerEvalDelFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyerEvalDelFlag_u(short value)
	{
		this.cBuyerEvalDelFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellerEvalDelFlag_u value 类型为:short
	 * 
	 */
	public short getCSellerEvalDelFlag_u()
	{
		return cSellerEvalDelFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerEvalDelFlag_u(short value)
	{
		this.cSellerEvalDelFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cBuyUsrDelFlag_u value 类型为:short
	 * 
	 */
	public short getCBuyUsrDelFlag_u()
	{
		return cBuyUsrDelFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCBuyUsrDelFlag_u(short value)
	{
		this.cBuyUsrDelFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cSellUsrDelFlag_u value 类型为:short
	 * 
	 */
	public short getCSellUsrDelFlag_u()
	{
		return cSellUsrDelFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellUsrDelFlag_u(short value)
	{
		this.cSellUsrDelFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumBuyerCanReply_u value 类型为:short
	 * 
	 */
	public short getCNumBuyerCanReply_u()
	{
		return cNumBuyerCanReply_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumBuyerCanReply_u(short value)
	{
		this.cNumBuyerCanReply_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return cNumSellerCanReply_u value 类型为:short
	 * 
	 */
	public short getCNumSellerCanReply_u()
	{
		return cNumSellerCanReply_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCNumSellerCanReply_u(short value)
	{
		this.cNumSellerCanReply_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return vecReplyMsg value 类型为:Vector<EvalReplyPo>
	 * 
	 */
	public Vector<EvalReplyPo> getVecReplyMsg()
	{
		return vecReplyMsg;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<EvalReplyPo>
	 * 
	 */
	public void setVecReplyMsg(Vector<EvalReplyPo> value)
	{
		if (value != null) {
				this.vecReplyMsg = value;
		}else{
				this.vecReplyMsg = new Vector<EvalReplyPo>();
		}
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * @return strItemNavInfo value 类型为:String
	 * 
	 */
	public String getStrItemNavInfo()
	{
		return strItemNavInfo;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrItemNavInfo(String value)
	{
		this.strItemNavInfo = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 1
	 * @return cItemNavInfo_u value 类型为:String
	 * 
	 */
	public String getCItemNavInfo_u()
	{
		return cItemNavInfo_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 1
	 * @param  value 类型为:String
	 * 
	 */
	public void setCItemNavInfo_u(String value)
	{
		this.cItemNavInfo_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 2
	 * @return strFtradeId value 类型为:String
	 * 
	 */
	public String getStrFtradeId()
	{
		return strFtradeId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrFtradeId(String value)
	{
		this.strFtradeId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 2
	 * @return cFtradeId_u value 类型为:short
	 * 
	 */
	public short getCFtradeId_u()
	{
		return cFtradeId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 2
	 * @param  value 类型为:short
	 * 
	 */
	public void setCFtradeId_u(short value)
	{
		this.cFtradeId_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwDsr1 value 类型为:long
	 * 
	 */
	public long getDwDsr1()
	{
		return dwDsr1;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDsr1(long value)
	{
		this.dwDsr1 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwDsr2 value 类型为:long
	 * 
	 */
	public long getDwDsr2()
	{
		return dwDsr2;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDsr2(long value)
	{
		this.dwDsr2 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwDsr3 value 类型为:long
	 * 
	 */
	public long getDwDsr3()
	{
		return dwDsr3;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDsr3(long value)
	{
		this.dwDsr3 = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cDsr1_u value 类型为:short
	 * 
	 */
	public short getCDsr1_u()
	{
		return cDsr1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDsr1_u(short value)
	{
		this.cDsr1_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cDsr2_u value 类型为:short
	 * 
	 */
	public short getCDsr2_u()
	{
		return cDsr2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDsr2_u(short value)
	{
		this.cDsr2_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cDsr3_u value 类型为:short
	 * 
	 */
	public short getCDsr3_u()
	{
		return cDsr3_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDsr3_u(short value)
	{
		this.cDsr3_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwSellerLevel value 类型为:long
	 * 
	 */
	public long getDwSellerLevel()
	{
		return dwSellerLevel;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerLevel(long value)
	{
		this.dwSellerLevel = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cSellerLevel_u value 类型为:short
	 * 
	 */
	public short getCSellerLevel_u()
	{
		return cSellerLevel_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCSellerLevel_u(short value)
	{
		this.cSellerLevel_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwDsrEvalTime value 类型为:long
	 * 
	 */
	public long getDwDsrEvalTime()
	{
		return dwDsrEvalTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDsrEvalTime(long value)
	{
		this.dwDsrEvalTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return dwDsrFlag value 类型为:long
	 * 
	 */
	public long getDwDsrFlag()
	{
		return dwDsrFlag;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwDsrFlag(long value)
	{
		this.dwDsrFlag = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cDsrEvalTime_u value 类型为:short
	 * 
	 */
	public short getCDsrEvalTime_u()
	{
		return cDsrEvalTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDsrEvalTime_u(short value)
	{
		this.cDsrEvalTime_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 3
	 * @return cDsrFlag_u value 类型为:short
	 * 
	 */
	public short getCDsrFlag_u()
	{
		return cDsrFlag_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 3
	 * @param  value 类型为:short
	 * 
	 */
	public void setCDsrFlag_u(short value)
	{
		this.cDsrFlag_u = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 4
	 * @return dwResetTime value 类型为:long
	 * 
	 */
	public long getDwResetTime()
	{
		return dwResetTime;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwResetTime(long value)
	{
		this.dwResetTime = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 4
	 * @return cResetTime_u value 类型为:long
	 * 
	 */
	public long getCResetTime_u()
	{
		return cResetTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 4
	 * @param  value 类型为:long
	 * 
	 */
	public void setCResetTime_u(long value)
	{
		this.cResetTime_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EvalRecordPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段dwFDealId的长度 size_of(uint32_t)
				length += 4;  //计算字段dwBuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSellerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwBuyerEvalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSellerEvalTime的长度 size_of(uint32_t)
				length += 4;  //计算字段iBuyerScore的长度 size_of(int)
				length += 4;  //计算字段iSellerScore的长度 size_of(int)
				length += 4;  //计算字段dwDealPayment的长度 size_of(uint32_t)
				length += 4;  //计算字段dwScoreTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPrice的长度 size_of(uint32_t)
				length += 4;  //计算字段dwEvalCreateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwDealFinishTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwPropertyMask的长度 size_of(uint32_t)
				length += 4;  //计算字段dwLastUpdateTime的长度 size_of(uint32_t)
				length += 4;  //计算字段dwDealPayTime的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(strDealId);  //计算字段strDealId的长度 size_of(String)
				length += ByteStream.getObjectSize(strCommodityId);  //计算字段strCommodityId的长度 size_of(String)
				length += ByteStream.getObjectSize(strCommodityLogo);  //计算字段strCommodityLogo的长度 size_of(String)
				length += ByteStream.getObjectSize(strCommodityTitle);  //计算字段strCommodityTitle的长度 size_of(String)
				length += ByteStream.getObjectSize(strSnapId);  //计算字段strSnapId的长度 size_of(String)
				length += ByteStream.getObjectSize(strSellerNickName);  //计算字段strSellerNickName的长度 size_of(String)
				length += ByteStream.getObjectSize(strBuyerNickName);  //计算字段strBuyerNickName的长度 size_of(String)
				length += ByteStream.getObjectSize(strBuyerExplain);  //计算字段strBuyerExplain的长度 size_of(String)
				length += ByteStream.getObjectSize(strSellerExplain);  //计算字段strSellerExplain的长度 size_of(String)
				length += ByteStream.getObjectSize(vecBuyerReason);  //计算字段vecBuyerReason的长度 size_of(Vector)
				length += ByteStream.getObjectSize(vecSellerReason);  //计算字段vecSellerReason的长度 size_of(Vector)
				length += 1;  //计算字段cBuyerEvalLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerEvalLevel的长度 size_of(uint8_t)
				length += 1;  //计算字段cTimeOutClose的长度 size_of(uint8_t)
				length += 1;  //计算字段cScoreFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段cPayType的长度 size_of(uint8_t)
				length += 1;  //计算字段cEvalStatus的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumSellerCanModEval的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumBuyerCanModEval的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerEvalDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerEvalDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyUsrDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellUsrDelFlag的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumBuyerCanReply的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumSellerCanReply的长度 size_of(uint8_t)
				length += 1;  //计算字段cFDealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cDealId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cCommodityId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cCommodityLogo_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cCommodityTitle_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSnapId_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerUin_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerNickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerNickName_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerEvalTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerEvalTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerExplain_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerExplain_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerScore_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerScore_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cDealPayment_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cScoreTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cPrice_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cEvalCreateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cDealFinishTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cPropertyMask_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cLastUpdateTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cDealPayTime_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerEvalLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerEvalLevel_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cTimeOutClose_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cScoreFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cPayType_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cEvalStatus_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumSellerCanModEval_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumBuyerCanModEval_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyerEvalDelFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellerEvalDelFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cBuyUsrDelFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cSellUsrDelFlag_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumBuyerCanReply_u的长度 size_of(uint8_t)
				length += 1;  //计算字段cNumSellerCanReply_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(vecReplyMsg);  //计算字段vecReplyMsg的长度 size_of(Vector)
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(strItemNavInfo);  //计算字段strItemNavInfo的长度 size_of(String)
				}
				if(  this.version >= 1 ){
						length += ByteStream.getObjectSize(cItemNavInfo_u);  //计算字段cItemNavInfo_u的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += ByteStream.getObjectSize(strFtradeId);  //计算字段strFtradeId的长度 size_of(String)
				}
				if(  this.version >= 2 ){
						length += 1;  //计算字段cFtradeId_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwDsr1的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwDsr2的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwDsr3的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cDsr1_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cDsr2_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cDsr3_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwSellerLevel的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cSellerLevel_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwDsrEvalTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 4;  //计算字段dwDsrFlag的长度 size_of(uint32_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cDsrEvalTime_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 3 ){
						length += 1;  //计算字段cDsrFlag_u的长度 size_of(uint8_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段dwResetTime的长度 size_of(uint32_t)
				}
				if(  this.version >= 4 ){
						length += 4;  //计算字段cResetTime_u的长度 size_of(uint32_t)
				}
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

/**
 ********************以下信息是每个版本的字段********************
 *
 *****以下是版本1所包含的字段*******
 *	long version;
 *	long dwFDealId;
 *	long dwBuyerUin;
 *	long dwSellerUin;
 *	long dwBuyerEvalTime;
 *	long dwSellerEvalTime;
 *	int iBuyerScore;
 *	int iSellerScore;
 *	long dwDealPayment;
 *	long dwScoreTime;
 *	long dwPrice;
 *	long dwEvalCreateTime;
 *	long dwDealFinishTime;
 *	long dwPropertyMask;
 *	long dwLastUpdateTime;
 *	long dwDealPayTime;
 *	String strDealId;
 *	String strCommodityId;
 *	String strCommodityLogo;
 *	String strCommodityTitle;
 *	String strSnapId;
 *	String strSellerNickName;
 *	String strBuyerNickName;
 *	String strBuyerExplain;
 *	String strSellerExplain;
 *	Vector<uint32_t> vecBuyerReason;
 *	Vector<uint32_t> vecSellerReason;
 *	short cBuyerEvalLevel;
 *	short cSellerEvalLevel;
 *	short cTimeOutClose;
 *	short cScoreFlag;
 *	short cPayType;
 *	short cEvalStatus;
 *	short cNumSellerCanModEval;
 *	short cNumBuyerCanModEval;
 *	short cBuyerEvalDelFlag;
 *	short cSellerEvalDelFlag;
 *	short cBuyUsrDelFlag;
 *	short cSellUsrDelFlag;
 *	short cNumBuyerCanReply;
 *	short cNumSellerCanReply;
 *	short cFDealId_u;
 *	short cDealId_u;
 *	short cCommodityId_u;
 *	short cCommodityLogo_u;
 *	short cCommodityTitle_u;
 *	short cSnapId_u;
 *	short cBuyerUin_u;
 *	short cSellerUin_u;
 *	short cSellerNickName_u;
 *	short cBuyerNickName_u;
 *	short cBuyerEvalTime_u;
 *	short cSellerEvalTime_u;
 *	short cBuyerExplain_u;
 *	short cSellerExplain_u;
 *	short cBuyerScore_u;
 *	short cSellerScore_u;
 *	short cDealPayment_u;
 *	short cScoreTime_u;
 *	short cPrice_u;
 *	short cEvalCreateTime_u;
 *	short cDealFinishTime_u;
 *	short cPropertyMask_u;
 *	short cLastUpdateTime_u;
 *	short cDealPayTime_u;
 *	short cBuyerEvalLevel_u;
 *	short cSellerEvalLevel_u;
 *	short cTimeOutClose_u;
 *	short cScoreFlag_u;
 *	short cPayType_u;
 *	short cEvalStatus_u;
 *	short cNumSellerCanModEval_u;
 *	short cNumBuyerCanModEval_u;
 *	short cBuyerEvalDelFlag_u;
 *	short cSellerEvalDelFlag_u;
 *	short cBuyUsrDelFlag_u;
 *	short cSellUsrDelFlag_u;
 *	short cNumBuyerCanReply_u;
 *	short cNumSellerCanReply_u;
 *	Vector<EvalReplyPo> vecReplyMsg;
 *	String strItemNavInfo;
 *	String cItemNavInfo_u;
 *****以上是版本1所包含的字段*******
 *
 *****以下是版本2所包含的字段*******
 *	long version;
 *	long dwFDealId;
 *	long dwBuyerUin;
 *	long dwSellerUin;
 *	long dwBuyerEvalTime;
 *	long dwSellerEvalTime;
 *	int iBuyerScore;
 *	int iSellerScore;
 *	long dwDealPayment;
 *	long dwScoreTime;
 *	long dwPrice;
 *	long dwEvalCreateTime;
 *	long dwDealFinishTime;
 *	long dwPropertyMask;
 *	long dwLastUpdateTime;
 *	long dwDealPayTime;
 *	String strDealId;
 *	String strCommodityId;
 *	String strCommodityLogo;
 *	String strCommodityTitle;
 *	String strSnapId;
 *	String strSellerNickName;
 *	String strBuyerNickName;
 *	String strBuyerExplain;
 *	String strSellerExplain;
 *	Vector<uint32_t> vecBuyerReason;
 *	Vector<uint32_t> vecSellerReason;
 *	short cBuyerEvalLevel;
 *	short cSellerEvalLevel;
 *	short cTimeOutClose;
 *	short cScoreFlag;
 *	short cPayType;
 *	short cEvalStatus;
 *	short cNumSellerCanModEval;
 *	short cNumBuyerCanModEval;
 *	short cBuyerEvalDelFlag;
 *	short cSellerEvalDelFlag;
 *	short cBuyUsrDelFlag;
 *	short cSellUsrDelFlag;
 *	short cNumBuyerCanReply;
 *	short cNumSellerCanReply;
 *	short cFDealId_u;
 *	short cDealId_u;
 *	short cCommodityId_u;
 *	short cCommodityLogo_u;
 *	short cCommodityTitle_u;
 *	short cSnapId_u;
 *	short cBuyerUin_u;
 *	short cSellerUin_u;
 *	short cSellerNickName_u;
 *	short cBuyerNickName_u;
 *	short cBuyerEvalTime_u;
 *	short cSellerEvalTime_u;
 *	short cBuyerExplain_u;
 *	short cSellerExplain_u;
 *	short cBuyerScore_u;
 *	short cSellerScore_u;
 *	short cDealPayment_u;
 *	short cScoreTime_u;
 *	short cPrice_u;
 *	short cEvalCreateTime_u;
 *	short cDealFinishTime_u;
 *	short cPropertyMask_u;
 *	short cLastUpdateTime_u;
 *	short cDealPayTime_u;
 *	short cBuyerEvalLevel_u;
 *	short cSellerEvalLevel_u;
 *	short cTimeOutClose_u;
 *	short cScoreFlag_u;
 *	short cPayType_u;
 *	short cEvalStatus_u;
 *	short cNumSellerCanModEval_u;
 *	short cNumBuyerCanModEval_u;
 *	short cBuyerEvalDelFlag_u;
 *	short cSellerEvalDelFlag_u;
 *	short cBuyUsrDelFlag_u;
 *	short cSellUsrDelFlag_u;
 *	short cNumBuyerCanReply_u;
 *	short cNumSellerCanReply_u;
 *	Vector<EvalReplyPo> vecReplyMsg;
 *	String strItemNavInfo;
 *	String cItemNavInfo_u;
 *	String strFtradeId;
 *	short cFtradeId_u;
 *****以上是版本2所包含的字段*******
 *
 *****以下是版本3所包含的字段*******
 *	long version;
 *	long dwFDealId;
 *	long dwBuyerUin;
 *	long dwSellerUin;
 *	long dwBuyerEvalTime;
 *	long dwSellerEvalTime;
 *	int iBuyerScore;
 *	int iSellerScore;
 *	long dwDealPayment;
 *	long dwScoreTime;
 *	long dwPrice;
 *	long dwEvalCreateTime;
 *	long dwDealFinishTime;
 *	long dwPropertyMask;
 *	long dwLastUpdateTime;
 *	long dwDealPayTime;
 *	String strDealId;
 *	String strCommodityId;
 *	String strCommodityLogo;
 *	String strCommodityTitle;
 *	String strSnapId;
 *	String strSellerNickName;
 *	String strBuyerNickName;
 *	String strBuyerExplain;
 *	String strSellerExplain;
 *	Vector<uint32_t> vecBuyerReason;
 *	Vector<uint32_t> vecSellerReason;
 *	short cBuyerEvalLevel;
 *	short cSellerEvalLevel;
 *	short cTimeOutClose;
 *	short cScoreFlag;
 *	short cPayType;
 *	short cEvalStatus;
 *	short cNumSellerCanModEval;
 *	short cNumBuyerCanModEval;
 *	short cBuyerEvalDelFlag;
 *	short cSellerEvalDelFlag;
 *	short cBuyUsrDelFlag;
 *	short cSellUsrDelFlag;
 *	short cNumBuyerCanReply;
 *	short cNumSellerCanReply;
 *	short cFDealId_u;
 *	short cDealId_u;
 *	short cCommodityId_u;
 *	short cCommodityLogo_u;
 *	short cCommodityTitle_u;
 *	short cSnapId_u;
 *	short cBuyerUin_u;
 *	short cSellerUin_u;
 *	short cSellerNickName_u;
 *	short cBuyerNickName_u;
 *	short cBuyerEvalTime_u;
 *	short cSellerEvalTime_u;
 *	short cBuyerExplain_u;
 *	short cSellerExplain_u;
 *	short cBuyerScore_u;
 *	short cSellerScore_u;
 *	short cDealPayment_u;
 *	short cScoreTime_u;
 *	short cPrice_u;
 *	short cEvalCreateTime_u;
 *	short cDealFinishTime_u;
 *	short cPropertyMask_u;
 *	short cLastUpdateTime_u;
 *	short cDealPayTime_u;
 *	short cBuyerEvalLevel_u;
 *	short cSellerEvalLevel_u;
 *	short cTimeOutClose_u;
 *	short cScoreFlag_u;
 *	short cPayType_u;
 *	short cEvalStatus_u;
 *	short cNumSellerCanModEval_u;
 *	short cNumBuyerCanModEval_u;
 *	short cBuyerEvalDelFlag_u;
 *	short cSellerEvalDelFlag_u;
 *	short cBuyUsrDelFlag_u;
 *	short cSellUsrDelFlag_u;
 *	short cNumBuyerCanReply_u;
 *	short cNumSellerCanReply_u;
 *	Vector<EvalReplyPo> vecReplyMsg;
 *	String strItemNavInfo;
 *	String cItemNavInfo_u;
 *	String strFtradeId;
 *	short cFtradeId_u;
 *	long dwDsr1;
 *	long dwDsr2;
 *	long dwDsr3;
 *	short cDsr1_u;
 *	short cDsr2_u;
 *	short cDsr3_u;
 *	long dwSellerLevel;
 *	short cSellerLevel_u;
 *	long dwDsrEvalTime;
 *	long dwDsrFlag;
 *	short cDsrEvalTime_u;
 *	short cDsrFlag_u;
 *****以上是版本3所包含的字段*******
 *
 *****以下是版本4所包含的字段*******
 *	long version;
 *	long dwFDealId;
 *	long dwBuyerUin;
 *	long dwSellerUin;
 *	long dwBuyerEvalTime;
 *	long dwSellerEvalTime;
 *	int iBuyerScore;
 *	int iSellerScore;
 *	long dwDealPayment;
 *	long dwScoreTime;
 *	long dwPrice;
 *	long dwEvalCreateTime;
 *	long dwDealFinishTime;
 *	long dwPropertyMask;
 *	long dwLastUpdateTime;
 *	long dwDealPayTime;
 *	String strDealId;
 *	String strCommodityId;
 *	String strCommodityLogo;
 *	String strCommodityTitle;
 *	String strSnapId;
 *	String strSellerNickName;
 *	String strBuyerNickName;
 *	String strBuyerExplain;
 *	String strSellerExplain;
 *	Vector<uint32_t> vecBuyerReason;
 *	Vector<uint32_t> vecSellerReason;
 *	short cBuyerEvalLevel;
 *	short cSellerEvalLevel;
 *	short cTimeOutClose;
 *	short cScoreFlag;
 *	short cPayType;
 *	short cEvalStatus;
 *	short cNumSellerCanModEval;
 *	short cNumBuyerCanModEval;
 *	short cBuyerEvalDelFlag;
 *	short cSellerEvalDelFlag;
 *	short cBuyUsrDelFlag;
 *	short cSellUsrDelFlag;
 *	short cNumBuyerCanReply;
 *	short cNumSellerCanReply;
 *	short cFDealId_u;
 *	short cDealId_u;
 *	short cCommodityId_u;
 *	short cCommodityLogo_u;
 *	short cCommodityTitle_u;
 *	short cSnapId_u;
 *	short cBuyerUin_u;
 *	short cSellerUin_u;
 *	short cSellerNickName_u;
 *	short cBuyerNickName_u;
 *	short cBuyerEvalTime_u;
 *	short cSellerEvalTime_u;
 *	short cBuyerExplain_u;
 *	short cSellerExplain_u;
 *	short cBuyerScore_u;
 *	short cSellerScore_u;
 *	short cDealPayment_u;
 *	short cScoreTime_u;
 *	short cPrice_u;
 *	short cEvalCreateTime_u;
 *	short cDealFinishTime_u;
 *	short cPropertyMask_u;
 *	short cLastUpdateTime_u;
 *	short cDealPayTime_u;
 *	short cBuyerEvalLevel_u;
 *	short cSellerEvalLevel_u;
 *	short cTimeOutClose_u;
 *	short cScoreFlag_u;
 *	short cPayType_u;
 *	short cEvalStatus_u;
 *	short cNumSellerCanModEval_u;
 *	short cNumBuyerCanModEval_u;
 *	short cBuyerEvalDelFlag_u;
 *	short cSellerEvalDelFlag_u;
 *	short cBuyUsrDelFlag_u;
 *	short cSellUsrDelFlag_u;
 *	short cNumBuyerCanReply_u;
 *	short cNumSellerCanReply_u;
 *	Vector<EvalReplyPo> vecReplyMsg;
 *	String strItemNavInfo;
 *	String cItemNavInfo_u;
 *	String strFtradeId;
 *	short cFtradeId_u;
 *	long dwDsr1;
 *	long dwDsr2;
 *	long dwDsr3;
 *	short cDsr1_u;
 *	short cDsr2_u;
 *	short cDsr3_u;
 *	long dwSellerLevel;
 *	short cSellerLevel_u;
 *	long dwDsrEvalTime;
 *	long dwDsrFlag;
 *	short cDsrEvalTime_u;
 *	short cDsrFlag_u;
 *	long dwResetTime;
 *	long cResetTime_u;
 *****以上是版本4所包含的字段*******
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
