 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.evaluation.idl.EvaluationService.java

package com.qq.qqbuy.thirdparty.idl.evaluation.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.util.io.ByteStream;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *
 *
 *@date 2013-07-09 10:14:40
 *
 *@since version:1
*/
public class  CreateEvalFromDealIdReq implements IServiceObject
{
	/**
	 * 版本 >= 0
	 */
	 private String sDealId = "";

	/**
	 * 版本 >= 0
	 */
	 private long dwBuyerUin;

	/**
	 * 版本 >= 0
	 */
	 private long dwSellerUin;

	/**
	 * 版本 >= 0
	 */
	 private String sMachineKey = "";

	/**
	 * 版本 >= 0
	 */
	 private String sSource = "";

	/**
	 * 版本 >= 0
	 */
	 private String sInReserve = "";

	/**
	 * 版本 >= 0
	 */
	 private String strOutReserve = "";


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(sDealId);
		bs.pushUInt(dwBuyerUin);
		bs.pushUInt(dwSellerUin);
		bs.pushString(sMachineKey);
		bs.pushString(sSource);
		bs.pushString(sInReserve);
//		bs.pushString(strOutReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		sDealId = bs.popString();
		dwBuyerUin = bs.popUInt();
		dwSellerUin = bs.popUInt();
		sMachineKey = bs.popString();
		sSource = bs.popString();
		sInReserve = bs.popString();
//		strOutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x23101002L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sDealId value 类型为:String
	 * 
	 */
	public String getSDealId()
	{
		return sDealId;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSDealId(String value)
	{
		this.sDealId = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwBuyerUin value 类型为:long
	 * 
	 */
	public long getDwBuyerUin()
	{
		return dwBuyerUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwBuyerUin(long value)
	{
		this.dwBuyerUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return dwSellerUin value 类型为:long
	 * 
	 */
	public long getDwSellerUin()
	{
		return dwSellerUin;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setDwSellerUin(long value)
	{
		this.dwSellerUin = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sMachineKey value 类型为:String
	 * 
	 */
	public String getSMachineKey()
	{
		return sMachineKey;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSMachineKey(String value)
	{
		this.sMachineKey = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sSource value 类型为:String
	 * 
	 */
	public String getSSource()
	{
		return sSource;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSSource(String value)
	{
		this.sSource = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sInReserve value 类型为:String
	 * 
	 */
	public String getSInReserve()
	{
		return sInReserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSInReserve(String value)
	{
		this.sInReserve = value;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return strOutReserve value 类型为:String
	 * 
	 */
	public String getStrOutReserve()
	{
		return strOutReserve;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStrOutReserve(String value)
	{
		this.strOutReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CreateEvalFromDealIdReq)
				length += ByteStream.getObjectSize(sDealId);  //计算字段sDealId的长度 size_of(String)
				length += 4;  //计算字段dwBuyerUin的长度 size_of(uint32_t)
				length += 4;  //计算字段dwSellerUin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(sMachineKey);  //计算字段sMachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(sSource);  //计算字段sSource的长度 size_of(String)
				length += ByteStream.getObjectSize(sInReserve);  //计算字段sInReserve的长度 size_of(String)
//				length += ByteStream.getObjectSize(strOutReserve);  //计算字段strOutReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
