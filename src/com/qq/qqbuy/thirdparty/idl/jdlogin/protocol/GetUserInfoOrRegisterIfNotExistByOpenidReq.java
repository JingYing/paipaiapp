package com.qq.qqbuy.thirdparty.idl.jdlogin.protocol;

 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.b2b2c.user.idl.UserWangGouBjAo.java



import com.paipai.netframework.kernal.NetMessage;
import com.paipai.util.io.ByteStream;

/**
 *
 *
 *@date 2015-02-10 02:07:43
 *
 *@since version:1
*/
public class  GetUserInfoOrRegisterIfNotExistByOpenidReq extends NetMessage
{
	/**
	 * 机器码，必需
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源，必需, 请填调用方自己的源文件名称
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id，默认填0, 请联系joelwzli/wisdomlin获取
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 选项掩码，默认填0即可
	 *
	 * 版本 >= 0
	 */
	 private long option;

	/**
	 * openid来源,必填
	 *
	 * 版本 >= 0
	 */
	 private long openidFrom;

	/**
	 * openid值，必填
	 *
	 * 版本 >= 0
	 */
	 private String openid = new String();

	/**
	 * 输入保留字，无需设置，传空字符串即可
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(sceneId);
		bs.pushUInt(option);
		bs.pushUInt(openidFrom);
		bs.pushString(openid);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		sceneId = bs.popUInt();
		option = bs.popUInt();
		openidFrom = bs.popUInt();
		openid = bs.popString();
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x30081815L;
	}


	/**
	 * 获取机器码，必需
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码，必需
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源，必需, 请填调用方自己的源文件名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id，默认填0, 请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景id，默认填0, 请联系joelwzli/wisdomlin获取
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取选项掩码，默认填0即可
	 * 
	 * 此字段的版本 >= 0
	 * @return option value 类型为:long
	 * 
	 */
	public long getOption()
	{
		return option;
	}


	/**
	 * 设置选项掩码，默认填0即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOption(long value)
	{
		this.option = value;
	}


	/**
	 * 获取openid来源,必填
	 * 
	 * 此字段的版本 >= 0
	 * @return openidFrom value 类型为:long
	 * 
	 */
	public long getOpenidFrom()
	{
		return openidFrom;
	}


	/**
	 * 设置openid来源,必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOpenidFrom(long value)
	{
		this.openidFrom = value;
	}


	/**
	 * 获取openid值，必填
	 * 
	 * 此字段的版本 >= 0
	 * @return openid value 类型为:String
	 * 
	 */
	public String getOpenid()
	{
		return openid;
	}


	/**
	 * 设置openid值，必填
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenid(String value)
	{
		this.openid = value;
	}


	/**
	 * 获取输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置输入保留字，无需设置，传空字符串即可
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserInfoOrRegisterIfNotExistByOpenidReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段option的长度 size_of(uint32_t)
				length += 4;  //计算字段openidFrom的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(openid, null);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetUserInfoOrRegisterIfNotExistByOpenidReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段option的长度 size_of(uint32_t)
				length += 4;  //计算字段openidFrom的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(openid, encoding);  //计算字段openid的长度 size_of(String)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
