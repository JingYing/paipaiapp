 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-06-13 11:47::33
 *
 *@since version:0
*/
public class  GetItemPoolReq implements IServiceObject
{
	/**
	 * 商品组ID，必填 
	 *
	 * 版本 >= 0
	 */
	 private String poolId = new String();

	/**
	 * 需要商品数量 
	 *
	 * 版本 >= 0
	 */
	 private int itemNum;

	/**
	 * 场景 
	 *
	 * 版本 >= 0
	 */
	 private int scene;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(poolId);
		bs.pushInt(itemNum);
		bs.pushInt(scene);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		poolId = bs.popString();
		itemNum = bs.popInt();
		scene = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2051803L;
	}


	/**
	 * 获取商品组ID，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolId value 类型为:String
	 * 
	 */
	public String getPoolId()
	{
		return poolId;
	}


	/**
	 * 设置商品组ID，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPoolId(String value)
	{
		if (value != null) {
				this.poolId = value;
		}else{
				this.poolId = new String();
		}
	}


	/**
	 * 获取需要商品数量 
	 * 
	 * 此字段的版本 >= 0
	 * @return itemNum value 类型为:int
	 * 
	 */
	public int getItemNum()
	{
		return itemNum;
	}


	/**
	 * 设置需要商品数量 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setItemNum(int value)
	{
		this.itemNum = value;
	}


	/**
	 * 获取场景 
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:int
	 * 
	 */
	public int getScene()
	{
		return scene;
	}


	/**
	 * 设置场景 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setScene(int value)
	{
		this.scene = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		if (value != null) {
				this.inReserved = value;
		}else{
				this.inReserved = new String();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetItemPoolReq)
				length += ByteStream.getObjectSize(poolId);  //计算字段poolId的长度 size_of(String)
				length += 4;  //计算字段itemNum的长度 size_of(int)
				length += 4;  //计算字段scene的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
