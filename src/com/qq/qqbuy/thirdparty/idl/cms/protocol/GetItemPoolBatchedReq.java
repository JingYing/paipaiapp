 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu,skyzhuang

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;

import java.util.Map;
import java.util.HashMap;

/**
 *
 *
 *@date 2012-07-30 06:29::36
 *
 *@since version:0
*/
public class  GetItemPoolBatchedReq implements IServiceObject
{
	/**
	 * 商品组ID及需要从对应商品组获取的商品数目，必填 
	 *
	 * 版本 >= 0
	 */
	 private Map<String,Integer> poolIds = new HashMap<String,Integer>();

	/**
	 * 场景 
	 *
	 * 版本 >= 0
	 */
	 private int scene;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(poolIds);
		bs.pushInt(scene);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		poolIds = (Map<String,Integer>)bs.popMap(String.class,Integer.class);
		scene = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2051805L;
	}


	/**
	 * 获取商品组ID及需要从对应商品组获取的商品数目，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolIds value 类型为:Map<String,Integer>
	 * 
	 */
	public Map<String,Integer> getPoolIds()
	{
		return poolIds;
	}


	/**
	 * 设置商品组ID及需要从对应商品组获取的商品数目，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,Integer>
	 * 
	 */
	public void setPoolIds(Map<String,Integer> value)
	{
		if (value != null) {
				this.poolIds = value;
		}else{
				this.poolIds = new HashMap<String,Integer>();
		}
	}


	/**
	 * 获取场景 
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:int
	 * 
	 */
	public int getScene()
	{
		return scene;
	}


	/**
	 * 设置场景 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setScene(int value)
	{
		this.scene = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetItemPoolBatchedReq)
				length += ByteStream.getObjectSize(poolIds);  //计算字段poolIds的长度 size_of(Map)
				length += 4;  //计算字段scene的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
