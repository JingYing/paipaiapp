 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-06-29 09:45::07
 *
 *@since version:0
*/
public class  GetAdvertResp implements IServiceObject
{
	public long result;
	/**
	 * 错误码
	 *
	 * 版本 >= 0
	 */
	 private int errCode;

	/**
	 * 错误信息
	 *
	 * 版本 >= 0
	 */
	 private String errMsg = new String();

	/**
	 * 广告信息
	 *
	 * 版本 >= 0
	 */
	 private AdvertInfo advert = new AdvertInfo();

	/**
	 * 广告位信息
	 *
	 * 版本 >= 0
	 */
	 private AdvertPositionInfo advertPosition = new AdvertPositionInfo();

	/**
	 * 广告策略信息
	 *
	 * 版本 >= 0
	 */
	 private AdvertPolicyInfo advertPolicy = new AdvertPolicyInfo();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String outReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushInt(errCode);
		bs.pushString(errMsg);
		bs.pushObject(advert);
		bs.pushObject(advertPosition);
		bs.pushObject(advertPolicy);
		bs.pushString(outReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		errCode = bs.popInt();
		errMsg = bs.popString();
		advert = (AdvertInfo) bs.popObject(AdvertInfo.class);
		advertPosition = (AdvertPositionInfo) bs.popObject(AdvertPositionInfo.class);
		advertPolicy = (AdvertPolicyInfo) bs.popObject(AdvertPolicyInfo.class);
		outReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2058804L;
	}


	/**
	 * 获取错误码
	 * 
	 * 此字段的版本 >= 0
	 * @return errCode value 类型为:int
	 * 
	 */
	public int getErrCode()
	{
		return errCode;
	}


	/**
	 * 设置错误码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setErrCode(int value)
	{
		this.errCode = value;
	}


	/**
	 * 获取错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @return errMsg value 类型为:String
	 * 
	 */
	public String getErrMsg()
	{
		return errMsg;
	}


	/**
	 * 设置错误信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setErrMsg(String value)
	{
		this.errMsg = value;
	}


	/**
	 * 获取广告信息
	 * 
	 * 此字段的版本 >= 0
	 * @return advert value 类型为:AdvertInfo
	 * 
	 */
	public AdvertInfo getAdvert()
	{
		return advert;
	}


	/**
	 * 设置广告信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:AdvertInfo
	 * 
	 */
	public void setAdvert(AdvertInfo value)
	{
		if (value != null) {
				this.advert = value;
		}else{
				this.advert = new AdvertInfo();
		}
	}


	/**
	 * 获取广告位信息
	 * 
	 * 此字段的版本 >= 0
	 * @return advertPosition value 类型为:AdvertPositionInfo
	 * 
	 */
	public AdvertPositionInfo getAdvertPosition()
	{
		return advertPosition;
	}


	/**
	 * 设置广告位信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:AdvertPositionInfo
	 * 
	 */
	public void setAdvertPosition(AdvertPositionInfo value)
	{
		if (value != null) {
				this.advertPosition = value;
		}else{
				this.advertPosition = new AdvertPositionInfo();
		}
	}


	/**
	 * 获取广告策略信息
	 * 
	 * 此字段的版本 >= 0
	 * @return advertPolicy value 类型为:AdvertPolicyInfo
	 * 
	 */
	public AdvertPolicyInfo getAdvertPolicy()
	{
		return advertPolicy;
	}


	/**
	 * 设置广告策略信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:AdvertPolicyInfo
	 * 
	 */
	public void setAdvertPolicy(AdvertPolicyInfo value)
	{
		if (value != null) {
				this.advertPolicy = value;
		}else{
				this.advertPolicy = new AdvertPolicyInfo();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return outReserved value 类型为:String
	 * 
	 */
	public String getOutReserved()
	{
		return outReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOutReserved(String value)
	{
		this.outReserved = value;
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetAdvertResp)
				length += 4;  //计算字段errCode的长度 size_of(int)
				length += ByteStream.getObjectSize(errMsg);  //计算字段errMsg的长度 size_of(String)
				length += ByteStream.getObjectSize(advert);  //计算字段advert的长度 size_of(AdvertInfo)
				length += ByteStream.getObjectSize(advertPosition);  //计算字段advertPosition的长度 size_of(AdvertPositionInfo)
				length += ByteStream.getObjectSize(advertPolicy);  //计算字段advertPolicy的长度 size_of(AdvertPolicyInfo)
				length += ByteStream.getObjectSize(outReserved);  //计算字段outReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
