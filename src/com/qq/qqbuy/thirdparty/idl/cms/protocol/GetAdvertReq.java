 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *
 *
 *@date 2012-06-29 09:45::07
 *
 *@since version:0
*/
public class  GetAdvertReq implements IServiceObject
{
	/**
	 * 广告位ID，必填 
	 *
	 * 版本 >= 0
	 */
	 private long adposId;

	/**
	 * 当前商品的ItemCode, 需设置scene=2
	 *
	 * 版本 >= 0
	 */
	 private String itemCode = new String();

	/**
	 * 当前搜索的关键字, 需设置scene=3
	 *
	 * 版本 >= 0
	 */
	 private String key = new String();

	/**
	 * 当前搜索的Path路径, 需设置scene=3
	 *
	 * 版本 >= 0
	 */
	 private String searchPath = new String();

	/**
	 * 场景 
	 *
	 * 版本 >= 0
	 */
	 private int scene;

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserved = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(adposId);
		bs.pushString(itemCode);
		bs.pushString(key);
		bs.pushString(searchPath);
		bs.pushInt(scene);
		bs.pushString(inReserved);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		adposId = bs.popUInt();
		itemCode = bs.popString();
		key = bs.popString();
		searchPath = bs.popString();
		scene = bs.popInt();
		inReserved = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xF2051804L;
	}


	/**
	 * 获取广告位ID，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @return adposId value 类型为:long
	 * 
	 */
	public long getAdposId()
	{
		return adposId;
	}


	/**
	 * 设置广告位ID，必填 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAdposId(long value)
	{
		this.adposId = value;
	}


	/**
	 * 获取当前商品的ItemCode, 需设置scene=2
	 * 
	 * 此字段的版本 >= 0
	 * @return itemCode value 类型为:String
	 * 
	 */
	public String getItemCode()
	{
		return itemCode;
	}


	/**
	 * 设置当前商品的ItemCode, 需设置scene=2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setItemCode(String value)
	{
		this.itemCode = value;
	}


	/**
	 * 获取当前搜索的关键字, 需设置scene=3
	 * 
	 * 此字段的版本 >= 0
	 * @return key value 类型为:String
	 * 
	 */
	public String getKey()
	{
		return key;
	}


	/**
	 * 设置当前搜索的关键字, 需设置scene=3
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setKey(String value)
	{
		this.key = value;
	}


	/**
	 * 获取当前搜索的Path路径, 需设置scene=3
	 * 
	 * 此字段的版本 >= 0
	 * @return searchPath value 类型为:String
	 * 
	 */
	public String getSearchPath()
	{
		return searchPath;
	}


	/**
	 * 设置当前搜索的Path路径, 需设置scene=3
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSearchPath(String value)
	{
		this.searchPath = value;
	}


	/**
	 * 获取场景 
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:int
	 * 
	 */
	public int getScene()
	{
		return scene;
	}


	/**
	 * 设置场景 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setScene(int value)
	{
		this.scene = value;
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserved value 类型为:String
	 * 
	 */
	public String getInReserved()
	{
		return inReserved;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserved(String value)
	{
		this.inReserved = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetAdvertReq)
				length += 4;  //计算字段adposId的长度 size_of(long)
				length += ByteStream.getObjectSize(itemCode);  //计算字段itemCode的长度 size_of(String)
				length += ByteStream.getObjectSize(key);  //计算字段key的长度 size_of(String)
				length += ByteStream.getObjectSize(searchPath);  //计算字段searchPath的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(int)
				length += ByteStream.getObjectSize(inReserved);  //计算字段inReserved的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
