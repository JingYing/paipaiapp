 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu,skyzhuang

package com.qq.qqbuy.thirdparty.idl.cms.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.ArrayList;
import java.util.List;

/**
 *商品池
 *
 *@date 2012-07-20 09:48::28
 *
 *@since version:0
*/
public class ItemPoolInfo  implements ICanSerializeObject
{
	/**
	 * 商品组ID 
	 *
	 * 版本 >= 0
	 */
	 private String poolId = new String();

	/**
	 * 商品组版本号 
	 *
	 * 版本 >= 0
	 */
	 private int poolVersion;

	/**
	 * 类目分类 
	 *
	 * 版本 >= 0
	 */
	 private int categoryId;

	/**
	 * 场景 
	 *
	 * 版本 >= 0
	 */
	 private int scene;

	/**
	 * 商品列表
	 *
	 * 版本 >= 0
	 */
	 private List<ItemInfo> itemList = new ArrayList<ItemInfo>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushString(poolId);
		bs.pushInt(poolVersion);
		bs.pushInt(categoryId);
		bs.pushInt(scene);
		bs.pushObject(itemList);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		poolId = bs.popString();
		poolVersion = bs.popInt();
		categoryId = bs.popInt();
		scene = bs.popInt();
		itemList = (List<ItemInfo>)bs.popList(ArrayList.class,ItemInfo.class);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取商品组ID 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolId value 类型为:String
	 * 
	 */
	public String getPoolId()
	{
		return poolId;
	}


	/**
	 * 设置商品组ID 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPoolId(String value)
	{
		this.poolId = value;
	}


	/**
	 * 获取商品组版本号 
	 * 
	 * 此字段的版本 >= 0
	 * @return poolVersion value 类型为:int
	 * 
	 */
	public int getPoolVersion()
	{
		return poolVersion;
	}


	/**
	 * 设置商品组版本号 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setPoolVersion(int value)
	{
		this.poolVersion = value;
	}


	/**
	 * 获取类目分类 
	 * 
	 * 此字段的版本 >= 0
	 * @return categoryId value 类型为:int
	 * 
	 */
	public int getCategoryId()
	{
		return categoryId;
	}


	/**
	 * 设置类目分类 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setCategoryId(int value)
	{
		this.categoryId = value;
	}


	/**
	 * 获取场景 
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:int
	 * 
	 */
	public int getScene()
	{
		return scene;
	}


	/**
	 * 设置场景 
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:int
	 * 
	 */
	public void setScene(int value)
	{
		this.scene = value;
	}


	/**
	 * 获取商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @return itemList value 类型为:List<ItemInfo>
	 * 
	 */
	public List<ItemInfo> getItemList()
	{
		return itemList;
	}


	/**
	 * 设置商品列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:List<ItemInfo>
	 * 
	 */
	public void setItemList(List<ItemInfo> value)
	{
		if (value != null) {
				this.itemList = value;
		}else{
				this.itemList = new ArrayList<ItemInfo>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(ItemPoolInfo)
				length += ByteStream.getObjectSize(poolId);  //计算字段poolId的长度 size_of(String)
				length += 4;  //计算字段poolVersion的长度 size_of(int)
				length += 4;  //计算字段categoryId的长度 size_of(int)
				length += 4;  //计算字段scene的长度 size_of(int)
				length += ByteStream.getObjectSize(itemList);  //计算字段itemList的长度 size_of(List)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
