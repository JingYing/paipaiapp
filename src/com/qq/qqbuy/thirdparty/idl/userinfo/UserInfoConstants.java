package com.qq.qqbuy.thirdparty.idl.userinfo;

public class UserInfoConstants {
	public static final int NEW_USER_PROP_MOBILE_BIND = 0; // <!手机绑定0x01
	public static final int NEW_USER_PROP_EMAIL_BIND = 1; // <!email 绑定0x02
	public static final int NEW_USER_PROP_PAY_ONLINE = 2; // <!开通财付通0x4
	public static final int NEW_USER_PROP_FREEZED = 3; // <!冻结用户0x8(修改为停业整顿)
	public static final int NEW_USER_PROP_AUTHING = 4; // <!认证中0x10
	public static final int NEW_USER_PROP_AUTH_FAILED = 5; // <!是否认证失败0x20
	public static final int NEW_USER_PROP_WARNED = 6; // <!警告用户0x40
	public static final int NEW_USER_PROP_FORBIDDEN = 7; // <!禁止用户0x80(修改为冻结)
	public static final int NEW_USER_PROP_MERITMAN = 8; // <!是否元勋0x100
	public static final int NEW_USER_PROP_SETC2CPASSWD = 9; // <!是否设置了交易密码0x200
	public static final int NEW_USER_PROP_LOOKAROUND = 10; // <!察看用户0x400
	public static final int NEW_USER_PROP_ACTIVEANGLE = 11; // <! 互动天使0x800
	public static final int NEW_USER_PROP_REFER_CREDIT = 12; // <!设置了参考信用度0x1000
	public static final int NEW_USER_PROP_GOLDEN_SELLER = 13; // <!金牌卖家0x2000
	public static final int NEW_USER_PROP_HEYYO_USER = 14; // <!开通和悦VOIP 0x4000
	public static final int NEW_USER_PROP_HEYYO_ACTIVE = 15; // <!和悦帐号已激活0x8000
	public static final int NEW_USER_PROP_QZONE_AUTH = 16; // <!QZONE认证用户0x10000
	public static final int NEW_USER_PROP_JNET_AUTH = 17; // <!JNET网游认证用户0x20000
	public static final int NEW_USER_PROP_PUNISH_BUY = 18; // <!禁止购买0x40000
	public static final int NEW_USER_PROP_PUNISH_LAW = 19; // <!禁止投诉或举报0x80000
	public static final int NEW_USER_PROP_PUNISH_SELL = 20; // <!禁止发布商品0x100000
	public static final int NEW_USER_PROP_PUNISH_MSG = 21; // <!禁止留言0x200000
	public static final int NEW_USER_PROP_PUNISH_EVAL = 22; // <!禁止评价0x400000
	public static final int NEW_USER_PROP_PUNISH_REPORT = 23; // <!禁止举报0x800000
	public static final int NEW_USER_PROP_PLEDGE = 24; // <!保证金用户0x1000000
	public static final int NEW_USER_PROP_ICBC_AUTHING = 25; // <!工行认证中0x2000000
	public static final int NEW_USER_PROP_ICBC_AUTH_FAIL = 26; // <!工行认证失败0x4000000
	public static final int NEW_USER_PROP_AUTO_CARD_AUTH = 27; // <!卡密自动发货用户0x8000000
	public static final int NEW_USER_PROP_XIAOBAO = 28; // <!消保用户(诚保一级)0x10000000
	public static final int NEW_USER_PROP_NEWUSER = 29; // <!拍拍登录新用户0x20000000
	public static final int NEW_USER_PROP_WEIGOU_USER = 30; // <!微构用户0x20000000

	// FpropertyMask1 认证属性。表示用户已经通过的认证。所有与第三方相关的属性都作为认证属性看待。
	public static final int NEW_USER_PROP_BOS_AUTH_MOBILE = 32; // 手机认证0x01[废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_IDEN = 33; // 身份证认证//0x02[废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_ICBC = 34; // 工行认证//0x4[废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_CFT = 35; // 开通了财付通//0x8 [废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_QZONE = 36; // QZone认证//0x10
																// [废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_JNET = 37; // JNET网游认证用户//0x20
																// [废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_OUFEI = 38; // 欧飞卡密自动发货用户//0x40
	public static final int NEW_USER_PROP_BOS_AUTH_91KA = 39; // 91卡密自动发货用户//0x80
	public static final int NEW_USER_PROP_BOS_AUTH_CRM = 40; // 开通CRM功能//0x100
	public static final int NEW_USER_PROP_BOS_AUTH_MOBILE_SELLER = 41; // (官字卖家)/0x200
	public static final int NEW_USER_PROP_BOS_AUTH_DRESS_SELLER = 42; // 认证女装卖家//0x400
	public static final int NEW_USER_PROP_BOS_AUTH_VIP = 43; // vip用户//0x800
	public static final int NEW_USER_PROP_BOS_AUTH_BSELLER = 44; // B2C
																	// 商家//0x1000
	public static final int NEW_USER_PROP_BOS_OFFICIAL_SELLER = 45; // 腾讯官方卖家0x2000
	public static final int NEW_USER_PROP_BOS_CHARITY_SELLER = 46; // 腾讯公益商家0x4000
	public static final int NEW_USER_PROP_BOS_LICENCE_SELLER = 47; // 商家营业执照认证0x8000
	public static final int NEW_USER_PROP_BOS_CHENGBAO_L2 = 48; // 诚保二级卖家0x10000
	public static final int NEW_USER_PROP_BOS_PROMOT_MLS = 49; // !
																// 满立送;满立减;包快递0x20000
	public static final int NEW_USER_PROP_BOS_MEMBER_PRIVILEGE = 50; // QQ商城
																		// 0x40000
																		// //20090514
	public static final int NEW_USER_PROP_BOS_VIRTUAL_SELLER = 51; // 虚拟商品卖家0x80000
																	// //20090714
	public static final int NEW_USER_PROP_BOS_SHOP_MARKETING = 52; // 店铺自主营销0x100000
																	// //20090901，70标记启用后，此标记将被废弃
	public static final int NEW_USER_PROP_BOS_QQSHOW_PRIVILEGE = 53; // QQ秀特权0x200000
																		// //20090925
	public static final int NEW_USER_PROP_BOS_FASHION = 54; // 时尚标记0x400000
															// //20091104
	public static final int NEW_USER_PROP_BOS_3CMARGIN = 55; // 3C类目保证金0x800000
																// //20091109
	public static final int NEW_USER_PROP_BOS_DECREASEMONEY = 56; // 减金额优惠0x1000000
																	// //20091110
	public static final int NEW_USER_PROP_BOS_DISCOUNT = 57; // 打折优惠0x2000000
																// //20091110
	public static final int NEW_USER_PROP_BOS_GIVEPRESENT = 58; // 送赠品优惠0x4000000
																// //20091110
	public static final int NEW_USER_PROP_BOS_OVERFLOWCHANGE = 59; // 加钱换购优惠0x8000000
																	// //20091110
	public static final int NEW_USER_PROP_BOS_EXPRESS = 60; // 包快递优惠0x10000000
															// //20091110
	public static final int NEW_USER_PROP_BOS_B2C_NEWSELLER = 61; // B2C
																	// 商家0x20000000
																	// //20091119
																	// 这个属性是用来记录c2c/b2c合并迁移的用户
	public static final int NEW_USER_PROP_BOS_BALTIC_GROWN_PRIVILEGE = 62; // 绿钻特权0x40000000
																			// //20091201
	public static final int NEW_USER_PROP_BOS_EXPRESS_TRAIN = 63; // 直通车0x80000000
																	// //20100114
	public static final int NEW_USER_PROP_BOS_OPENAPI_USER = 64; // 通过OPEN_API发布商品的用户0x100000000
																	// //20100412
	public static final int NEW_USER_PROP_BOS_COLORFUL_DIAMONDS = 65; // 彩钻用户0x200000000
																		// //20100414
	public static final int NEW_USER_PROP_BOS_SHOP3 = 66; // 店铺3.0灰度属性 (临时支持)
															// 2010-427 skydu
	public static final int NEW_USER_PROP_BOS_3C_SELLER = 67; // 3C卖家
	public static final int NEW_USER_PROP_BOS_MOBILE_CONTACT = 68; // 卖家支持手机客服

	//
	public static final int NEW_USER_PROP_BOS_PROMOT_TEMP = 69; // 卖家促销临时标记位
	public static final int NEW_USER_PROP_BOS_PROMOT_ACTIVE = 70; // 卖家新促销活动标记，原52标记将被废弃
																	// 2010-10-27
	public static final int NEW_USER_PROP_BOS_SHOP_VIP = 71; // 店铺vip属性
	public static final int NEW_USER_PROP_BOS_3C_SHOP = 72; // 3C直营店
	public static final int NEW_USER_PROP_BOS_CRM_AUTH_USER = 73; // 多客服权限用户
	public static final int NEW_USER_PROP_BOS_COD_SELLER = 74; // 支持货到付款卖家
	public static final int NEW_USER_PROP_BOS_FAST_CONSIGN_SELLER = 75; // 快速发货卖家
	public static final int NEW_USER_PROP_BOS_7POSTFREE_SELLER = 76; // 7天免邮包退卖家
	public static final int NEW_USER_PROP_BOS_DO4_CHARGE_SELLER = 77; // 平台代充(卡易售)卖家
	public static final int NEW_USER_PROP_BOS_NETGAME_API_SELLER = 78; // 网游API卖家
	public static final int NEW_USER_PROP_BOS_CPS_SELLER = 79; // CPS卖家
	public static final int NEW_USER_PROP_BOS_IMPORT_ITEM_SELLER = 80; // 进口商品卖家
	public static final int NEW_USER_PROP_BOS_KEY_SELLER = 81; // 重点商户卖家
	public static final int NEW_USER_PROP_BOS_PRESELL_SELLER = 82; // 预售商品卖家
	public static final int NEW_USER_PROP_BOS_ICSON_JOINT_SELLER = 83; // 易迅联营卖家
	public static final int NEW_USER_PROP_BOS_BKS_AREA_SHIPTPT_SELLER = 84; // 支持不可售地区运费模板卖家
	public static final int NEW_USER_PROP_BOS_ICSON_OPEN_SELLER = 85; // 易迅开放卖家
	public static final int NEW_USER_PROP_BOS_WEIXIN_SCENE_PRICE = 86; // 微信场景价
	public static final int NEW_USER_PROP_BOS_WEIDIAN_SELLER = 87; // 微店卖家
	public static final int NEW_USER_PROP_BOS_SHIELD_PAIPAI_SHOP = 88; // 屏蔽PC店铺
	public static final int NEW_USER_PROP_BOS_JOIN_DIRECT_DELIVERY = 89; // 易迅联营直发
	public static final int NEW_USER_PROP_BOS_WEIDIAN_JINGDONG = 90; // 京东微店
	public static final int NEW_USER_PROP_BOS_SELLER_JINGDONG = 91; // 京东商品同步卖家
	public static final int NEW_USER_PROP_BOS_PAIPAI_WEIDIAN_SELLER = 92; // 拍拍微店卖家

	// FpropertyMask2
	// 常规属性。不属于其他三种属性的都放在这里。
	public static final int NEW_USER_PROP_BOS_BIND_MOBILE = 96; // 手机绑定ox01[废弃]
	public static final int NEW_USER_PROP_BOS_BIND_EMAIL = 97; // email绑定0x02[废弃]
	public static final int NEW_USER_PROP_BOS_MERITMAN = 98; // 拍拍元勋0x04[废弃]
	public static final int NEW_USER_PROP_BOS_SETC2CPASSWD = 99; // 设置了交易密码0x08[废弃]
	public static final int NEW_USER_PROP_BOS_ACTIVEANGLE = 100; // 互助天使0x10[废弃]
	public static final int NEW_USER_PROP_BOS_REFER_CREDIT = 101; // 设置了参考信用度0x20[废弃]
	public static final int NEW_USER_PROP_BOS_GOLDEN_SELLER = 102; // 金牌卖家ox40[废弃]
	public static final int NEW_USER_PROP_BOS_PLEDGE = 103; // 保证金用户0x80[废弃]
	public static final int NEW_USER_PROP_BOS_AUTH_CARD = 104; // 卡密自动发货用户0x100[废弃]
	public static final int NEW_USER_PROP_BOS_XIAOBAO = 105; // 消保用户0x200[废弃]
	public static final int NEW_USER_PROP_BOS_FLASH_CARD = 106; // 闪电发货0x400
	public static final int NEW_USER_PROP_BOS_GAME_RECHARGE = 107; // 网游快冲0x800
	public static final int NEW_USER_PROP_BOS_SHOP_NEW = 108; // 新店铺0x1000
	public static final int NEW_USER_PROP_NOT_RESET_CURRENT_SALES = 109; // 当期销售量，不清除0x2000
	public static final int NEW_USER_PROP_BOSS_SHOP_CHANGE_NAME = 110; // QQ商城卖家授权修改店名和店招标记
	public static final int NEW_USER_PROP_BOS_PAIPAI_TEST_USER = 111; // 拍拍测试号码
	public static final int NEW_USER_PROP_BOS_CHEAP_BUY_TOKEN_USER = 112; // 优惠购买QQ令牌用户
	public static final int NEW_USER_PROP_BOS_BIND_DOMAIN_NAME_USER = 113; // 可绑定店铺域名用户
	public static final int NEW_USER_PROP_BOS_BSHOP = 114; // B店铺卖家
	public static final int NEW_USER_PROP_BOS_CSHOP = 115; // C店铺卖家
	public static final int NEW_USER_PROP_BOS_OVERSEA = 116; // 海外购
	public static final int NEW_USER_PROP_BOS_WEIXIN_PAY = 117; // 微信支付

	// FpropertyMask3 易变属性。维护一些易变的，不会维持很久的状态。比如身份证认证中等
	public static final int NEW_USER_PROP_BOS_AUTHING_IDEN = 160; // 身份证认证中[废弃]
																	// 0x01
	public static final int NEW_USER_PROP_BOS_AUTHFAIL_IDEN = 161; // 身份证认证失败[废弃]
																	// 0x02
	public static final int NEW_USER_PROP_BOS_AUTHING_ICBC = 162; // 工行认证中[废弃]
																	// 0x04
	public static final int NEW_USER_PROP_BOS_AUTHFAIL_ICBC = 163; // 工行认证失败[废弃]
																	// 0x08
	public static final int NEW_USER_PPOP_BOS_POINT_ADDING = 164; // 卖家处于加分状态0x10
	public static final int NEW_USER_PPOP_BOS_INVITED_USER = 165; // 受邀请用户(B2C
																	// hoky)
																	// 0x20
	// FpropertyMask4 受限属性。本字段的属性位置1可能会导致用户的相关权限受限。
	// 因为session agent 有问题，暂时不要在此段增加定义
	public static final int NEW_USER_PROP_BOS_PUNISH_FREEZED = 224; // 用户被冻结[废弃0x01]
	public static final int NEW_USER_PROP_BOS_PUNISH_WARNED = 225; // 用户被警告[废弃0x02]
	public static final int NEW_USER_PROP_BOS_PUNISH_FORBIDDEN = 226; // 用户被禁止[废弃0x04]
	public static final int NEW_USER_PROP_BOS_PUNISH_LOOKING = 227; // 用户处于察看期[废弃0x08]
	public static final int NEW_USER_PROP_BOS_PUNISH_BUY = 228; // 禁止购买[废弃0x10]
	public static final int NEW_USER_PROP_BOS_PUNISH_COMPLAIN = 229; // 禁止投诉[废弃0x20]
	public static final int NEW_USER_PROP_BOS_PUNISH_SELL = 230; // 禁止发布商品[废弃0x40]
	public static final int NEW_USER_PROP_BOS_PUNISH_MSG = 231; // 禁止留言[废弃0x80]
	public static final int NEW_USER_PROP_BOS_PUNISH_EVAL = 232; // 禁止评价[废弃0x100]
	public static final int NEW_USER_PROP_BOS_PUNISH_REPORT = 233; // 禁止举报[废弃0x200]
}
