 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.userinfo.UserApiAo.java

package com.qq.qqbuy.thirdparty.idl.userinfo.protocol.apiuser;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.component.c2cplatform.IServiceObject;


/**
 *更新用户基本信息请求类
 *
 *@date 2013-06-20 10:08:11
 *
 *@since version:0
*/
public class  ApiModifyUserProfileReq implements IServiceObject
{
	/**
	 * 用户QQ号
	 *
	 * 版本 >= 0
	 */
	 private long uin;

	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 调用来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();

	/**
	 * 用户Po
	 *
	 * 版本 >= 0
	 */
	 private APIUserProfile modifyProfile = new APIUserProfile();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(uin);
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushString(inReserve);
		bs.pushObject(modifyProfile);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		uin = bs.popUInt();
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		inReserve = bs.popString();
		modifyProfile = (APIUserProfile) bs.popObject(APIUserProfile.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0xf1601804L;
	}


	/**
	 * 获取用户QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @return uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return uin;
	}


	/**
	 * 设置用户QQ号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.uin = value;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置调用来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	/**
	 * 获取用户Po
	 * 
	 * 此字段的版本 >= 0
	 * @return modifyProfile value 类型为:APIUserProfile
	 * 
	 */
	public APIUserProfile getModifyProfile()
	{
		return modifyProfile;
	}


	/**
	 * 设置用户Po
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:APIUserProfile
	 * 
	 */
	public void setModifyProfile(APIUserProfile value)
	{
		if (value != null) {
				this.modifyProfile = value;
		}else{
				this.modifyProfile = new APIUserProfile();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiModifyUserProfileReq)
				length += 4;  //计算字段uin的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(machineKey);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(inReserve);  //计算字段inReserve的长度 size_of(String)
				length += ByteStream.getObjectSize(modifyProfile);  //计算字段modifyProfile的长度 size_of(APIUserProfile)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
