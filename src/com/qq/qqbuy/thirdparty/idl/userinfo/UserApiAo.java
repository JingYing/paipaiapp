package com.qq.qqbuy.thirdparty.idl.userinfo;

import java.util.Map;

import com.paipai.lang.uint16_t;
import com.paipai.lang.uint32_t;
import com.paipai.lang.uint8_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;


/**
 * 用户api ao
 * @author skydu
 *
 */
@HeadApiProtocol(cPlusNamespace = "c2cent::ao::user", needInit = true)
public class UserApiAo {
	@Member(desc = "用户po", cPlusNamespace = "c2cent::po::user", isNeedUFlag = true)
	public class APIUserProfile {
		@Field(desc = "用户Po", defaultValue = "4")
		uint8_t version;

		@Field(desc = "性别")
		uint8_t sex;

		@Field(desc = "登录级别")
		uint8_t loginLevel;

		@Field(desc = "认证属性")
		uint8_t authMask;

		@Field(desc = "省份")
		uint8_t province;

		@Field(desc = "默认联系方式")
		uint8_t commContactType;

		@Field(desc = "证件类别")
		uint8_t identitycardType;

		@Field(desc = "最后登录类型")
		uint8_t lastLoginType;

		@Field(desc = "版本_u")
		uint8_t version_u;

		@Field(desc = "性别_u")
		uint8_t sex_u;

		@Field(desc = "登录级别_u")
		uint8_t loginLevel_u;

		@Field(desc = "认证属性_u")
		uint8_t authMask_u;

		@Field(desc = "省份_u")
		uint8_t province_u;

		@Field(desc = "默认联系方式_u")
		uint8_t commContactType_u;

		@Field(desc = "证件类别_u")
		uint8_t identitycardType_u;

		@Field(desc = "最后登录类型_u")
		uint8_t lastLoginType_u;

		@Field(desc = "用户QQ号码_u")
		uint8_t userId_u;

		@Field(desc = "注册时间_u")
		uint8_t regTime_u;

		@Field(desc = "国家_u")
		uint8_t country_u;

		uint8_t honorMask_u;

		uint8_t userInfoMask_u;

		@Field(desc = "最后登录ip_u")
		uint8_t lastLoginIp_u;

		@Field(desc = "最后登录时间_u")
		uint8_t lastLoginTime_u;

		@Field(desc = "最后更新时间_u")
		uint8_t modifyTime_u;

		@Field(desc = "昵称_u")
		uint8_t nickName_u;

		@Field(desc = "交易密码_u")
		uint8_t c2CPasswd_u;

		@Field(desc = "手机_u")
		uint8_t mobile_u;

		@Field(desc = "邮箱_u")
		uint8_t email_u;

		@Field(desc = "参考信用_u")
		uint8_t referCredit_u;

		@Field(desc = "肖像_u")
		uint8_t mainLogoPos_u;

		@Field(desc = "真实姓名_u")
		uint8_t name_u;

		@Field(desc = "证件号码_u")
		uint8_t identityCardNum_u;

		uint8_t identityIndex_u;

		@Field(desc = "固定电话_u")
		uint8_t phone_u;

		@Field(desc = "传真_u")
		uint8_t fax_u;

		@Field(desc = "邮政编码_u")
		uint8_t postCode_u;

		@Field(desc = "家庭住址_u")
		uint8_t address_u;

		@Field(desc = "用户全部属性_u")
		uint8_t sproperty_u;
		
		@Field(desc = "保留字_u")
		uint8_t reserve_u;

		@Field(desc = "买家信用_u")
		uint8_t buyerCredit_u;

		@Field(desc = "卖家信用_u")
		uint8_t sellerCredit_u;

		uint8_t recvMsgMask_u;

		@Field(desc = "城市_u")
		uint8_t city_u;

		@Field(desc = "用户属性map_u")
		uint8_t prop_u;

		@Field(desc = "用户QQ号码")
		uint32_t userId;

		@Field(desc = "注册时间")
		uint32_t regTime;

		@Field(desc = "国家")
		uint32_t country;

		uint32_t honorMask;

		uint32_t userInfoMask;

		@Field(desc = "最后登录IP")
		uint32_t lastLoginIp;

		@Field(desc = "最后登录时间")
		uint32_t lastLoginTime;

		@Field(desc = "最后更新时间")
		uint32_t modifyTime;

		@Field(desc = "昵称")
		String nickName;

		@Field(desc = "交易密码")
		String c2CPasswd;

		@Field(desc = "手机")
		String mobile;

		@Field(desc = "邮箱")
		String email;

		String referCredit;

		@Field(desc = "肖像图")
		String mainLogoPos;

		@Field(desc = "真实姓名")
		String name;

		@Field(desc = "证件号码")
		String identityCardNum;

		String identityIndex;

		@Field(desc = "固定电话")
		String phone;

		@Field(desc = "传真")
		String fax;

		@Field(desc = "邮政编码")
		String postCode;

		@Field(desc = "家庭住址")
		String address;

		@Field(desc = "用户全部属性，用于前台js")
		String sproperty;

		@Field(desc = "保留字")
		String reserve;

		@Field(desc = "买家信用")
		int buyerCredit;

		@Field(desc = "卖家信用")
		int sellerCredit;

		uint16_t recvMsgMask;

		@Field(desc = "城市")
		uint16_t city;

		@Field(desc = "用户属性Map,用于CGI和Ao.使用方法见{@link com.paipai.c2c.user.constants.UserConstants}")
		Map<uint16_t, uint8_t> prop;

		@Field(desc = "虚拟信用_u", version = 1)
		uint8_t virtualCredit_u;

		@Field(desc = "实物信用_u", version = 1)
		uint8_t objectCredit_u;

		@Field(desc = "地区_u", version = 1)
		uint8_t region_u;

		@Field(desc = "虚拟信用", version = 1)
		int virtualCredit;

		@Field(desc = "实物信用", version = 1)
		int objectCredit;

		@Field(desc = "地区", version = 1)
		uint32_t region;
		
		@Field(desc = "卖家联系方式",version = 2)
		String contacts;	
		
		@Field(desc = "卖家联系方式_u",version = 2)
		uint8_t contacts_u;
		
		@Field(desc = "卖家uin",version = 3)
		uint32_t sellerUin;	
		
		@Field(desc = "卖家uin_u",version = 3)
		uint8_t sellerUin_u;		
		
		@Field(desc = "财付通账户",version = 4)
		String cftAccount;	
		
		@Field(desc = "财付通账户_u",version = 4)
		uint8_t cftAccount_u;
		
		@Field(desc = "备用字符串",version = 4)
		String tempStr;	
		
		@Field(desc = "备用字符串_u",version = 4)
		uint8_t tempStr_u;	
		
		@Field(desc = "店铺类目id",version = 4)
		uint32_t shopClassId;	
		
		@Field(desc = "店铺类目id_u",version = 4)
		uint8_t shopClassId_u;						
	}
	@ApiProtocol(cmdid = "0xf1601801L", desc = "获取用户简单信息类")
	class ApiGetUserSimpleProfile {
		@ApiProtocol(cmdid = "0xf1601801L", desc = "获取用户简单信息请求类")
		class Req {
			@Field(desc = "用户QQ号，表示获取此用户信息")
			uint32_t uin;

			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "场景id")
			uint32_t scene;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xf1608801L", desc = "获取用户简单信息返回类")
		class Resp {
			@Field(desc = "用户po")
			APIUserProfile simpleProfile;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xf1601802L", desc = "获取用户详情信息类")
	class ApiGetUserDetailProfile {
		@ApiProtocol(cmdid = "0xf1601802L", desc = "获取用户详情信息请求类")
		class Req {
			@Field(desc = "用户QQ号,表示获取此用户信息")
			uint32_t uin;

			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "场景id")
			uint32_t scene;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xf1608802L", desc = "获取详情信息返回类")
		class Resp {
			@Field(desc = "用户po")
			APIUserProfile simpleProfile;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xf1601803L", desc = "获取用户登录级别类")
	class ApiGetUserLoginLevel {
		@ApiProtocol(cmdid = "0xf1601803L", desc = "获取用户登录级别请求类")
		class Req {
			@Field(desc = "用户QQ号")
			uint32_t uin;

			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "场景id")
			uint32_t scene;
			
			@Field(desc = "请求保留字")
			String inReserve;
		}

		@ApiProtocol(cmdid = "0xf1608803L", desc = "获取登录级别返回类")
		class Resp {
			@Field(desc = "用户登录级别")
			uint8_t loginLevel;

			@Field(desc = "输出保留字")
			String outReserve;
		}
	}

	@ApiProtocol(cmdid = "0xf1601804L", desc = "更新用户基本信息类")
	class ApiModifyUserProfile {
		@ApiProtocol(cmdid = "0xf1601804L", desc = "更新用户基本信息请求类")
		class Req {
			@Field(desc = "用户QQ号")
			uint32_t uin;

			@Field(desc = "机器码")
			String machineKey;

			@Field(desc = "调用来源")
			String source;
			
			@Field(desc = "场景id")
			uint32_t scene;

			@Field(desc = "请求保留字")
			String inReserve;

			@Field(desc = "用户Po")
			APIUserProfile modifyProfile;
		}

		@ApiProtocol(cmdid = "0xf1608804L", desc = "更新用户基本信息返回类")
		class Resp {
			@Field(desc = "输出保留字")
			String outReserve;
		}
	}
}
