//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.oneday.CachestockAO.java

package com.qq.qqbuy.thirdparty.idl.oneday.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import java.util.Vector;

/**
 *bo
 *
 *@date 2013-08-19 11:07:36
 *
 *@since version:20100914
*/
public class ComdyStockInfoList  implements ICanSerializeObject
{
	/**
	 * m_vecComdyStockInfo
	 *
	 * 版本 >= 0
	 */
	 private Vector<ComdyStockInfo> m_vecComdyStockInfo = new Vector<ComdyStockInfo>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushObject(m_vecComdyStockInfo);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		m_vecComdyStockInfo = (Vector<ComdyStockInfo>)bs.popVector(ComdyStockInfo.class);
		return bs.getReadLength();
	} 


	/**
	 * 获取m_vecComdyStockInfo
	 * 
	 * 此字段的版本 >= 0
	 * @return m_vecComdyStockInfo value 类型为:Vector<ComdyStockInfo>
	 * 
	 */
	public Vector<ComdyStockInfo> getM_vecComdyStockInfo()
	{
		return m_vecComdyStockInfo;
	}


	/**
	 * 设置m_vecComdyStockInfo
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ComdyStockInfo>
	 * 
	 */
	public void setM_vecComdyStockInfo(Vector<ComdyStockInfo> value)
	{
		if (value != null) {
				this.m_vecComdyStockInfo = value;
		}else{
				this.m_vecComdyStockInfo = new Vector<ComdyStockInfo>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ComdyStockInfoList)
				length += ByteStream.getObjectSize(m_vecComdyStockInfo);  //计算字段m_vecComdyStockInfo的长度 size_of(Vector)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
