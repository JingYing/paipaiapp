package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

public class CSubAttrOption
  implements ICanSerializeObject
{
  private long attrId;
  private long optionId;
  private long property;
  private byte attrId_u;
  private byte optionId_u;
  private byte property_u;

  public int getSize()
  {
    return 12;
  }

  public int serialize(ByteStream bs) throws Exception
  {
    bs.pushUInt(getSize());
    bs.pushUInt(this.attrId);
    bs.pushUInt(this.optionId);
    bs.pushUInt(this.property);

    return 0;
  }

  public int unSerialize(ByteStream bs) throws Exception
  {
    bs.popUInt();
    this.attrId = bs.popUInt();
    this.optionId = bs.popUInt();
    this.property = bs.popUInt();

    return 0;
  }

  public long getAttrId()
  {
    return this.attrId;
  }

  public void setAttrId(long attrId)
  {
    this.attrId = attrId;
  }

  public long getOptionId()
  {
    return this.optionId;
  }

  public void setOptionId(long optionId)
  {
    this.optionId = optionId;
  }

  public long getProperty()
  {
    return this.property;
  }

  public void setProperty(long property)
  {
    this.property = property;
  }
}