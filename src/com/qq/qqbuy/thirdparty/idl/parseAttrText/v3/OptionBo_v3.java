//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;
import com.paipai.lang.GenericWrapper;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.Vector;
import java.util.HashMap;

/**
 *属性值结构体
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class OptionBo_v3  implements ICanSerializeObject
{
	/**
	 * 版本号, version需要小写
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 属性项id
	 *
	 * 版本 >= 0
	 */
	 private long AttrId;

	/**
	 * 属性值id
	 *
	 * 版本 >= 0
	 */
	 private long OptionId;

	/**
	 * 类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * property
	 *
	 * 版本 >= 0
	 */
	 private long Property;

	/**
	 * 属性值排序
	 *
	 * 版本 >= 0
	 */
	 private long Order;

	/**
	 * 属性值名称
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * 属性值下的子属性值对
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Vector<SubAttrOption_v3>> SubAttrIds = new HashMap<uint32_t,Vector<SubAttrOption_v3>>();



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(AttrId);
		bs.pushUInt(OptionId);
		bs.pushUInt(Type);
		bs.pushUInt(Property);
		bs.pushUInt(Order);
		bs.pushString(Name);
		bs.pushObject(SubAttrIds);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		AttrId = bs.popUInt();
		OptionId = bs.popUInt();
		Type = bs.popUInt();
		Property = bs.popUInt();
		Order = bs.popUInt();
		Name = bs.popString();

		// 生成反序列化属性SubAttrIds相应的范型参数包裹对象(包裹了该属性中范型的类型)。 
		GenericWrapper SubAttrIdsPaiPai00 = new GenericWrapper();
		SubAttrIdsPaiPai00.setType(HashMap.class);
		GenericWrapper[] SubAttrIdsPaiPaiArray00= new GenericWrapper[2];
		SubAttrIdsPaiPaiArray00[0] = new GenericWrapper(uint32_t.class);
		SubAttrIdsPaiPaiArray00[1] = new GenericWrapper();
		GenericWrapper SubAttrIdsPaiPai11 = new GenericWrapper();
		SubAttrIdsPaiPai11.setType(Vector.class);
		GenericWrapper[] SubAttrIdsPaiPaiArray11= new GenericWrapper[2];
		SubAttrIdsPaiPaiArray11[0] = new GenericWrapper(SubAttrOption_v3.class);
		SubAttrIdsPaiPai11.setGenericParameters(SubAttrIdsPaiPaiArray11);


		SubAttrIdsPaiPaiArray00[1] = SubAttrIdsPaiPai11;
		SubAttrIdsPaiPai00.setGenericParameters(SubAttrIdsPaiPaiArray00);



		SubAttrIds = (Map<uint32_t,Vector<SubAttrOption_v3>>)bs.popObject(SubAttrIdsPaiPai00);

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @return AttrId value 类型为:long
	 * 
	 */
	public long getAttrId()
	{
		return AttrId;
	}


	/**
	 * 设置属性项id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAttrId(long value)
	{
		this.AttrId = value;
	}


	/**
	 * 获取属性值id
	 * 
	 * 此字段的版本 >= 0
	 * @return OptionId value 类型为:long
	 * 
	 */
	public long getOptionId()
	{
		return OptionId;
	}


	/**
	 * 设置属性值id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOptionId(long value)
	{
		this.OptionId = value;
	}


	/**
	 * 获取类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取property
	 * 
	 * 此字段的版本 >= 0
	 * @return Property value 类型为:long
	 * 
	 */
	public long getProperty()
	{
		return Property;
	}


	/**
	 * 设置property
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setProperty(long value)
	{
		this.Property = value;
	}


	/**
	 * 获取属性值排序
	 * 
	 * 此字段的版本 >= 0
	 * @return Order value 类型为:long
	 * 
	 */
	public long getOrder()
	{
		return Order;
	}


	/**
	 * 设置属性值排序
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrder(long value)
	{
		this.Order = value;
	}


	/**
	 * 获取属性值名称
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置属性值名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.Name = value;
	}


	/**
	 * 获取属性值下的子属性值对
	 * 
	 * 此字段的版本 >= 0
	 * @return SubAttrIds value 类型为:Map<uint32_t,Vector<SubAttrOption_v3>>
	 * 
	 */
	public Map<uint32_t,Vector<SubAttrOption_v3>> getSubAttrIds()
	{
		return SubAttrIds;
	}


	/**
	 * 设置属性值下的子属性值对
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Vector<SubAttrOption_v3>>
	 * 
	 */
	public void setSubAttrIds(Map<uint32_t,Vector<SubAttrOption_v3>> value)
	{
		if (value != null) {
				this.SubAttrIds = value;
		}else{
				this.SubAttrIds = new HashMap<uint32_t,Vector<SubAttrOption_v3>>();
		}
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(OptionBo_v3)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段AttrId的长度 size_of(uint32_t)
				length += 4;  //计算字段OptionId的长度 size_of(uint32_t)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 4;  //计算字段Property的长度 size_of(uint32_t)
				length += 4;  //计算字段Order的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
				length += ByteStream.getObjectSize(SubAttrIds);  //计算字段SubAttrIds的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
