//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.weigou.mall.NcaDao.java

package com.qq.qqbuy.thirdparty.idl.parseAttrText.v3;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;

import com.paipai.lang.GenericWrapper;

/**
 *导航简易结构体
 *
 *@date 2013-11-27 05:00:47
 *
 *@since version:0
*/
public class NavEntry_v3  implements ICanSerializeObject
{
	/**
	 * 版本号, version需要小写
	 *
	 * 版本 >= 0
	 */
	 private long version;

	/**
	 * 导航id
	 *
	 * 版本 >= 0
	 */
	 private long NavId;

	/**
	 * 地图id
	 *
	 * 版本 >= 0
	 */
	 private long MapId;

	/**
	 * 父导航id
	 *
	 * 版本 >= 0
	 */
	 private long PNavId;

	/**
	 * 导航名称
	 *
	 * 版本 >= 0
	 */
	 private String Name = new String();

	/**
	 * 导航类型
	 *
	 * 版本 >= 0
	 */
	 private long Type;

	/**
	 * 导航分类
	 *
	 * 版本 >= 0
	 */
	 private long Catalog;

	/**
	 * 备注
	 *
	 * 版本 >= 0
	 */
	 private String Note = new String();

	/**
	 * 排序字段
	 *
	 * 版本 >= 0
	 */
	 private long Order;

	/**
	 * 导航property
	 *
	 * 版本 >= 0
	 */
	 private String PropertyStr = new String();

	/**
	 * 搜索条件
	 *
	 * 版本 >= 0
	 */
	 private String SearchCond = new String();

	/**
	 * 是否有属性
	 *
	 * 版本 >= 0
	 */
	 private long HasAttr;

	/**
	 * 导航预留自定义串1
	 *
	 * 版本 >= 0
	 */
	 private String CustomStr1 = new String();

	/**
	 * 导航预留自定义串2
	 *
	 * 版本 >= 0
	 */
	 private String CustomStr2 = new String();

	/**
	 *  metaclass type
	 *
	 * 版本 >= 0
	 */
	 private long MetaCatalog;

	/**
	 * 导航预留自定义整形字段2
	 *
	 * 版本 >= 0
	 */
	 private long CustomUint2;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getClassSize());
		bs.pushUInt(version);
		bs.pushUInt(NavId);
		bs.pushUInt(MapId);
		bs.pushUInt(PNavId);
		bs.pushString(Name);
		bs.pushUInt(Type);
		bs.pushUInt(Catalog);
		bs.pushString(Note);
		bs.pushUInt(Order);
		bs.pushString(PropertyStr);
		bs.pushString(SearchCond);
		bs.pushUInt(HasAttr);
		bs.pushString(CustomStr1);
		bs.pushString(CustomStr2);
		bs.pushUInt(MetaCatalog);
		bs.pushUInt(CustomUint2);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		NavId = bs.popUInt();
		MapId = bs.popUInt();
		PNavId = bs.popUInt();
		Name = bs.popString();
		Type = bs.popUInt();
		Catalog = bs.popUInt();
		Note = bs.popString();
		Order = bs.popUInt();
		PropertyStr = bs.popString();
		SearchCond = bs.popString();
		HasAttr = bs.popUInt();
		CustomStr1 = bs.popString();
		CustomStr2 = bs.popString();
		MetaCatalog = bs.popUInt();
		CustomUint2 = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号, version需要小写
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取导航id
	 * 
	 * 此字段的版本 >= 0
	 * @return NavId value 类型为:long
	 * 
	 */
	public long getNavId()
	{
		return NavId;
	}


	/**
	 * 设置导航id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setNavId(long value)
	{
		this.NavId = value;
	}


	/**
	 * 获取地图id
	 * 
	 * 此字段的版本 >= 0
	 * @return MapId value 类型为:long
	 * 
	 */
	public long getMapId()
	{
		return MapId;
	}


	/**
	 * 设置地图id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMapId(long value)
	{
		this.MapId = value;
	}


	/**
	 * 获取父导航id
	 * 
	 * 此字段的版本 >= 0
	 * @return PNavId value 类型为:long
	 * 
	 */
	public long getPNavId()
	{
		return PNavId;
	}


	/**
	 * 设置父导航id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setPNavId(long value)
	{
		this.PNavId = value;
	}


	/**
	 * 获取导航名称
	 * 
	 * 此字段的版本 >= 0
	 * @return Name value 类型为:String
	 * 
	 */
	public String getName()
	{
		return Name;
	}


	/**
	 * 设置导航名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setName(String value)
	{
		this.Name = value;
	}


	/**
	 * 获取导航类型
	 * 
	 * 此字段的版本 >= 0
	 * @return Type value 类型为:long
	 * 
	 */
	public long getType()
	{
		return Type;
	}


	/**
	 * 设置导航类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setType(long value)
	{
		this.Type = value;
	}


	/**
	 * 获取导航分类
	 * 
	 * 此字段的版本 >= 0
	 * @return Catalog value 类型为:long
	 * 
	 */
	public long getCatalog()
	{
		return Catalog;
	}


	/**
	 * 设置导航分类
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCatalog(long value)
	{
		this.Catalog = value;
	}


	/**
	 * 获取备注
	 * 
	 * 此字段的版本 >= 0
	 * @return Note value 类型为:String
	 * 
	 */
	public String getNote()
	{
		return Note;
	}


	/**
	 * 设置备注
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setNote(String value)
	{
		this.Note = value;
	}


	/**
	 * 获取排序字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Order value 类型为:long
	 * 
	 */
	public long getOrder()
	{
		return Order;
	}


	/**
	 * 设置排序字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setOrder(long value)
	{
		this.Order = value;
	}


	/**
	 * 获取导航property
	 * 
	 * 此字段的版本 >= 0
	 * @return PropertyStr value 类型为:String
	 * 
	 */
	public String getPropertyStr()
	{
		return PropertyStr;
	}


	/**
	 * 设置导航property
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setPropertyStr(String value)
	{
		this.PropertyStr = value;
	}


	/**
	 * 获取搜索条件
	 * 
	 * 此字段的版本 >= 0
	 * @return SearchCond value 类型为:String
	 * 
	 */
	public String getSearchCond()
	{
		return SearchCond;
	}


	/**
	 * 设置搜索条件
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSearchCond(String value)
	{
		this.SearchCond = value;
	}


	/**
	 * 获取是否有属性
	 * 
	 * 此字段的版本 >= 0
	 * @return HasAttr value 类型为:long
	 * 
	 */
	public long getHasAttr()
	{
		return HasAttr;
	}


	/**
	 * 设置是否有属性
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setHasAttr(long value)
	{
		this.HasAttr = value;
	}


	/**
	 * 获取导航预留自定义串1
	 * 
	 * 此字段的版本 >= 0
	 * @return CustomStr1 value 类型为:String
	 * 
	 */
	public String getCustomStr1()
	{
		return CustomStr1;
	}


	/**
	 * 设置导航预留自定义串1
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCustomStr1(String value)
	{
		this.CustomStr1 = value;
	}


	/**
	 * 获取导航预留自定义串2
	 * 
	 * 此字段的版本 >= 0
	 * @return CustomStr2 value 类型为:String
	 * 
	 */
	public String getCustomStr2()
	{
		return CustomStr2;
	}


	/**
	 * 设置导航预留自定义串2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setCustomStr2(String value)
	{
		this.CustomStr2 = value;
	}


	/**
	 * 获取 metaclass type
	 * 
	 * 此字段的版本 >= 0
	 * @return MetaCatalog value 类型为:long
	 * 
	 */
	public long getMetaCatalog()
	{
		return MetaCatalog;
	}


	/**
	 * 设置 metaclass type
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setMetaCatalog(long value)
	{
		this.MetaCatalog = value;
	}


	/**
	 * 获取导航预留自定义整形字段2
	 * 
	 * 此字段的版本 >= 0
	 * @return CustomUint2 value 类型为:long
	 * 
	 */
	public long getCustomUint2()
	{
		return CustomUint2;
	}


	/**
	 * 设置导航预留自定义整形字段2
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCustomUint2(long value)
	{
		this.CustomUint2 = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(NavEntry_v3)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段NavId的长度 size_of(uint32_t)
				length += 4;  //计算字段MapId的长度 size_of(uint32_t)
				length += 4;  //计算字段PNavId的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Name);  //计算字段Name的长度 size_of(String)
				length += 4;  //计算字段Type的长度 size_of(uint32_t)
				length += 4;  //计算字段Catalog的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(Note);  //计算字段Note的长度 size_of(String)
				length += 4;  //计算字段Order的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(PropertyStr);  //计算字段PropertyStr的长度 size_of(String)
				length += ByteStream.getObjectSize(SearchCond);  //计算字段SearchCond的长度 size_of(String)
				length += 4;  //计算字段HasAttr的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(CustomStr1);  //计算字段CustomStr1的长度 size_of(String)
				length += ByteStream.getObjectSize(CustomStr2);  //计算字段CustomStr2的长度 size_of(String)
				length += 4;  //计算字段MetaCatalog的长度 size_of(uint32_t)
				length += 4;  //计算字段CustomUint2的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
