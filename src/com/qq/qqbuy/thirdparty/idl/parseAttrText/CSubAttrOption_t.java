package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class CSubAttrOption_t
  implements ICanSerializeObject
{
  private Map<Long, Vector<CSubAttrOption>> map = new HashMap();

  public Map<Long, Vector<CSubAttrOption>> getSubAttrOptionMap()
  {
    return this.map;
  }

  public void setSubAttrOptionMap(Map<Long, Vector<CSubAttrOption>> map)
  {
    if (map == null) {
      throw new NullPointerException();
    }
    this.map = map;
  }

  public int getSize()
  {
    return 0;
  }

  public int serialize(ByteStream bs) throws Exception
  {
    bs.pushMap(this.map);
    return 0;
  }

  public int unSerialize(ByteStream bs)
    throws Exception
  {
    this.map.clear();
    int size = bs.popInt();
    while (size-- > 0) {
      Long id = Long.valueOf(bs.popUInt());
      Vector subAttrOptionList = bs.popVector(CSubAttrOption.class);
      this.map.put(id, subAttrOptionList);
    }

    return 0;
  }
}