package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;
import java.util.Vector;

public class CAttrBo
  implements ICanSerializeObject
{
  private long id;
  private long type;
  private long navId;
  private long parrentAttrId;
  private long parrentOptionId;
  private long property;
  private long order;
  private String name = new String();
  private String desc = new String();
  private CIdList hlOptionIds = new CIdList();
  private Vector<COptionBo> optionList = new Vector();
  private byte id_u;
  private byte type_u;
  private byte navId_u;
  private byte parrentAttrId_u;
  private byte parrentOptionId_u;
  private byte property_u;
  private byte name_u;
  private byte desc_u;
  private byte hlOptionIds_u;
  private byte optionList_u;
  private byte order_u;

  public int getSize()
  {
    byte[] buffer = new byte[1048576];
    ByteStream bsDummy = new ByteStream(buffer, buffer.length);
    bsDummy.setRealWrite(false);
    try {
      serialize_i(bsDummy);
    }
    catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
    return bsDummy.getWrittenLength();
  }

  public int serialize(ByteStream bs) throws Exception
  {
    bs.pushUInt(getSize());
    serialize_i(bs);
    return bs.getWrittenLength();
  }

  private int serialize_i(ByteStream bs) throws Exception {
    bs.pushUInt(this.id);
    bs.pushUInt(this.type);
    bs.pushUInt(this.navId);
    bs.pushUInt(this.parrentAttrId);
    bs.pushUInt(this.parrentOptionId);
    bs.pushUInt(this.property);
    bs.pushString(this.name);
    bs.pushString(this.desc);
    bs.pushUInt(this.order);
    this.hlOptionIds.serialize(bs);
    bs.pushVector(this.optionList);
    return bs.getWrittenLength();
  }

  public int unSerialize(ByteStream bs)
    throws Exception
  {
    bs.popUInt();
    this.id = bs.popUInt();
    this.type = bs.popUInt();
    this.navId = bs.popUInt();
    this.parrentAttrId = bs.popUInt();
    this.parrentOptionId = bs.popUInt();
    this.property = bs.popUInt();
    this.name = bs.popString();
    this.desc = bs.popString();
    this.order = bs.popUInt();
    this.hlOptionIds.unSerialize(bs);
    this.optionList = (Vector<COptionBo>) bs.popVector(COptionBo.class);
    return bs.getReadLength();
  }

  public long getId()
  {
    return this.id;
  }

  public void setId(long id)
  {
    this.id = id;
    this.id_u = 1;
  }

  public boolean isIdSet() {
    return this.id_u != 0;
  }

  public long getType()
  {
    return this.type;
  }

  public void setType(long type)
  {
    this.type = type;
    this.type_u = 1;
  }

  public boolean isTypeSet() {
    return this.type_u != 0;
  }

  public long getNavId()
  {
    return this.navId;
  }

  public void setNavId(long navId)
  {
    this.navId = navId;
    this.navId_u = 1;
  }

  public boolean isNavIdSet() {
    return this.navId_u != 0;
  }

  public long getParrentAttrId()
  {
    return this.parrentAttrId;
  }

  public void setParrentAttrId(long parrentAttrId)
  {
    this.parrentAttrId = parrentAttrId;
    this.parrentAttrId_u = 1;
  }

  public boolean isParrentAttrIdSet() {
    return this.parrentAttrId_u != 0;
  }

  public long getParrentOptionId()
  {
    return this.parrentOptionId;
  }

  public void setParrentOptionId(long parrentOptionId)
  {
    this.parrentOptionId = parrentOptionId;
    this.parrentOptionId_u = 1;
  }

  public boolean isParrentOptionIdSet() {
    return this.parrentOptionId_u != 0;
  }

  public long getProperty()
  {
    return this.property;
  }

  public void setProperty(long property)
  {
    this.property = property;
    this.property_u = 1;
  }

  public boolean isPropertySet() {
    return this.property_u != 0;
  }

  public long getOrder()
  {
    return this.order;
  }

  public void setOrder(long order)
  {
    this.order = order;
    this.order_u = 1;
  }

  public boolean isOrderSet() {
    return this.order_u != 0;
  }

  public String getName()
  {
    return this.name;
  }

  public void setName(String name)
  {
    if (name == null) {
      throw new NullPointerException();
    }
    this.name = name;
    this.name_u = 1;
  }

  public boolean isNameSet() {
    return this.name_u != 0;
  }

  public String getDesc()
  {
    return this.desc;
  }

  public void setDesc(String desc)
  {
    if (desc == null) {
      throw new NullPointerException();
    }
    this.desc = desc;
    this.desc_u = 1;
  }

  public boolean isDescSet() {
    return this.desc_u != 0;
  }

  public CIdList getHLOptionIds()
  {
    return this.hlOptionIds;
  }

  public void setHLOptionIds(CIdList hlOptionIds)
  {
    if (hlOptionIds == null) {
      throw new NullPointerException();
    }
    this.hlOptionIds = hlOptionIds;
    this.hlOptionIds_u = 1;
  }

  public boolean isHLOptionIdsSet() {
    return this.hlOptionIds_u != 0;
  }

  public Vector<COptionBo> getOptionList()
  {
    return this.optionList;
  }

  public void setOptionList(Vector<COptionBo> optionList)
  {
    if (optionList == null) {
      throw new NullPointerException();
    }
    this.optionList = optionList;
    this.optionList_u = 1;
  }

  public boolean isOptionListSet() {
    return this.optionList_u != 0;
  }
}