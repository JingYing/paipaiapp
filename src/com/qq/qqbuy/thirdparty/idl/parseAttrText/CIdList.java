package com.qq.qqbuy.thirdparty.idl.parseAttrText;

import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObject;
import java.util.Vector;

public class CIdList
  implements ICanSerializeObject
{
  private Vector<Long> idList = new Vector();
  private byte idList_u;

  public int getSize()
  {
    byte[] buffer = new byte[1048576];
    ByteStream bsDummy = new ByteStream(buffer, buffer.length);
    bsDummy.setRealWrite(false);
    try {
      serialize_i(bsDummy);
    }
    catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
    return bsDummy.getWrittenLength();
  }

  public int serialize(ByteStream bs) throws Exception
  {
    bs.pushUInt(getSize());
    serialize_i(bs);
    return bs.getWrittenLength();
  }

  private int serialize_i(ByteStream bs) throws Exception {
    bs.pushVector(this.idList);
    bs.pushByte(this.idList_u);
    return 0;
  }

  public int unSerialize(ByteStream bs)
    throws Exception
  {
    bs.popUInt();
    this.idList = (Vector<Long>) bs.popVector(Long.class);
    this.idList_u = bs.popByte();
    return bs.getWrittenLength();
  }

  public void clear() {
    this.idList.clear();
    this.idList_u = 0;
  }

  public void add(long id)
  {
    this.idList.add(Long.valueOf(id));
    this.idList_u = 1;
  }

  public int getListSize() {
    return this.idList.size();
  }

  long getId(int index)
  {
    return ((Long)this.idList.get(index)).longValue();
  }

  public Vector<Long> toVector() {
    return this.idList;
  }
}