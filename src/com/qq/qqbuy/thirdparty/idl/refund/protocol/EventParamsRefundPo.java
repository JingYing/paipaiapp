//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.c2c.Refund.ao.idl.BuyerRefundReq.java

package com.qq.qqbuy.thirdparty.idl.refund.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *标记发货事件参数Po
 *
 *@date 2014-12-01 02:20:40
 *
 *@since version:0
*/
public class EventParamsRefundPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 版本号
	 *
	 * 版本 >= 0
	 */
	 private long version = 1;

	/**
	 * 是否收到货
	 *
	 * 版本 >= 0
	 */
	 private long isRecvItem;

	/**
	 * 是否请求退货
	 *
	 * 版本 >= 0
	 */
	 private long isReqitem;

	/**
	 * 退货原因类型
	 * 
		0 其他退款原因
      	5 与卖家协商一致退款 
		6 卖家虚假标记发货 
		7 商品缺货无法发货 
		8 物流公司运送问题 
      <option value="9">买错或者买多了</option>
      <option value="10">无法联系卖家发货</option>
      <option value="11">卖家商品已经缺货</option>
      <option value="12">交易细节未达成一致</option>
	 *
	 * 版本 >= 0
	 */
	 private long RefundReasonType;

	/**
	 * 退货申请详细说明
	 *
	 * 版本 >= 0
	 */
	 private String RefundApplyMsg = new String();

	/**
	 * 退款金额，单位是分，四舍五入
	 *
	 * 版本 >= 0
	 */
	 private long RefundToBuyer;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(version);
		bs.pushUInt(isRecvItem);
		bs.pushUInt(isReqitem);
		bs.pushUInt(RefundReasonType);
		bs.pushString(RefundApplyMsg);
		bs.pushUInt(RefundToBuyer);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		version = bs.popUInt();
		isRecvItem = bs.popUInt();
		isReqitem = bs.popUInt();
		RefundReasonType = bs.popUInt();
		RefundApplyMsg = bs.popString();
		RefundToBuyer = bs.popUInt();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取版本号
	 * 
	 * 此字段的版本 >= 0
	 * @return version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return version;
	}


	/**
	 * 设置版本号
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.version = value;
	}


	/**
	 * 获取是否收到货
	 * 
	 * 此字段的版本 >= 0
	 * @return isRecvItem value 类型为:long
	 * 
	 */
	public long getIsRecvItem()
	{
		return isRecvItem;
	}


	/**
	 * 设置是否收到货
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsRecvItem(long value)
	{
		this.isRecvItem = value;
	}


	/**
	 * 获取是否请求退货
	 * 
	 * 此字段的版本 >= 0
	 * @return isReqitem value 类型为:long
	 * 
	 */
	public long getIsReqitem()
	{
		return isReqitem;
	}


	/**
	 * 设置是否请求退货
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setIsReqitem(long value)
	{
		this.isReqitem = value;
	}


	/**
	 * 获取退货原因类型
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundReasonType value 类型为:long
	 * 
	 */
	public long getRefundReasonType()
	{
		return RefundReasonType;
	}


	/**
	 * 设置退货原因类型
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundReasonType(long value)
	{
		this.RefundReasonType = value;
	}


	/**
	 * 获取退货申请详细说明
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundApplyMsg value 类型为:String
	 * 
	 */
	public String getRefundApplyMsg()
	{
		return RefundApplyMsg;
	}


	/**
	 * 设置退货申请详细说明
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRefundApplyMsg(String value)
	{
		this.RefundApplyMsg = value;
	}


	/**
	 * 获取退款金额，单位是分，四舍五入
	 * 
	 * 此字段的版本 >= 0
	 * @return RefundToBuyer value 类型为:long
	 * 
	 */
	public long getRefundToBuyer()
	{
		return RefundToBuyer;
	}


	/**
	 * 设置退款金额，单位是分，四舍五入
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRefundToBuyer(long value)
	{
		this.RefundToBuyer = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(EventParamsRefundPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段isRecvItem的长度 size_of(uint32_t)
				length += 4;  //计算字段isReqitem的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReasonType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundApplyMsg, null);  //计算字段RefundApplyMsg的长度 size_of(String)
				length += 4;  //计算字段RefundToBuyer的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(EventParamsRefundPo)
				length += 4;  //计算字段version的长度 size_of(uint32_t)
				length += 4;  //计算字段isRecvItem的长度 size_of(uint32_t)
				length += 4;  //计算字段isReqitem的长度 size_of(uint32_t)
				length += 4;  //计算字段RefundReasonType的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(RefundApplyMsg, encoding);  //计算字段RefundApplyMsg的长度 size_of(String)
				length += 4;  //计算字段RefundToBuyer的长度 size_of(uint32_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
