package com.qq.qqbuy.thirdparty.idl.wxdLbsStore;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.util.annotation.Protocol;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.common.constant.PpConstants;
import com.qq.qqbuy.common.env.EnvManager;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.LbsStoreCoordinate;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByAdressReq;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByAdressResp;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByNearReq;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByNearResp;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByStoreIdReq;
import com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol.QueryStoreInfoByStoreIdResp;

/**
 * 门店信息IDL接口调用端
 * 
 * @author zhaohuayu
 * 
 */
public class WxdLbsStoreClient {

	@Protocol(cmdid = 0x879e1805L, desc = "查询附近的门店信息", export = true)
	public static QueryStoreInfoByNearResp QueryStoreInfoByNear(long longitude,
			long latitude, String mk, long address3Id, long startPage,
			long pageNum, long category) {
		QueryStoreInfoByNearResp resp = new QueryStoreInfoByNearResp();
		QueryStoreInfoByNearReq req = new QueryStoreInfoByNearReq();
		req.setMachineKey(mk);
		req.setSource(PpConstants.PP_SOURCE);
		req.setSceneId(0L);
		LbsStoreCoordinate coordinate = new LbsStoreCoordinate();
		coordinate.setLatitude(latitude);
		coordinate.setLongitude(longitude);
		if (category > 0) {
			req.getFilter().setCategory(category);
		}
		req.getFilter().setCoordinate(coordinate);
		if (EnvManager.isGamma()) {
			req.getFilter().setAddress3Id(430700);
		} else {
			req.getFilter().setAddress3Id(address3Id);
		}
		req.getFilter().setStartPage(startPage);
		req.getFilter().setPageNum(pageNum);
		int ret = 0;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(System.currentTimeMillis());
		stub.setRouteKey(System.currentTimeMillis());
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("QueryStoreInfoByNear error,ret=" + ret, e);
			e.printStackTrace();
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}

	/**
	 * 这里仅限根据市区ID查区的门店信息
	 * 
	 * @param mk
	 * @param address4Id
	 * @param startPage
	 * @param pageNum
	 * @return
	 */
	@Protocol(cmdid = 0x879e1804L, desc = "根据地址级别查询门店信息", export = true)
	public static QueryStoreInfoByAdressResp QueryStoreInfoByAdress(QueryStoreInfoByAdressReq req) {
		QueryStoreInfoByAdressResp resp = new QueryStoreInfoByAdressResp();		
		int ret = 0;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(System.currentTimeMillis());
		stub.setRouteKey(System.currentTimeMillis());
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("QueryStoreInfoByAdress error,ret=" + ret, e);
			e.printStackTrace();
			ret = -1;
		}
		return ret == 0 ? resp : null;
	}

	@Protocol(cmdid = 0x879e1807L, desc = "根据QQ号或门店ID查询门店信息", export = true)
	public static QueryStoreInfoByStoreIdResp QueryStoreInfoByStoreId(
			String id, short type, String mk, long startPage, long pageNum) {
		QueryStoreInfoByStoreIdResp resp = new QueryStoreInfoByStoreIdResp();
		QueryStoreInfoByStoreIdReq req = new QueryStoreInfoByStoreIdReq() ;
		req.setMachineKey(mk);
		req.setSource(PpConstants.PP_SOURCE);
		req.setSceneId(0l);
		req.getFilter().setType(type);
		if (type == 0) {
			req.getFilter().setUin(Long.parseLong(id)) ;
		} else {
			req.getFilter().setStoreId(id) ;
		}		
		req.getFilter().setStartPage(startPage);
		req.getFilter().setPageNum(pageNum);
		int ret = 0;
		IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		stub.setOperator(System.currentTimeMillis());
		stub.setRouteKey(System.currentTimeMillis());
		try {
			ret = stub.invoke(req, resp);
		} catch (Exception e) {
			Log.run.warn("QueryStoreInfoByStoreId error,ret=" + ret , e);
			e.printStackTrace();
			ret = -1;
		}
		return ret == 0 ? resp : null;
		
	}
}
