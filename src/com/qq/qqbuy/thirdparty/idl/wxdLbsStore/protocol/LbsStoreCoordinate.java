//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;

/**
 *坐标
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class LbsStoreCoordinate  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 当前位置（纬）
	 *
	 * 版本 >= 0
	 */
	 private long Latitude;

	/**
	 * 版本 >= 0
	 */
	 private short Latitude_u;

	/**
	 * 当前位置（经）
	 *
	 * 版本 >= 0
	 */
	 private long Longitude;

	/**
	 * 版本 >= 0
	 */
	 private short Longitude_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(Latitude);
		bs.pushUByte(Latitude_u);
		bs.pushLong(Longitude);
		bs.pushUByte(Longitude_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Latitude = bs.popLong();
		Latitude_u = bs.popUByte();
		Longitude = bs.popLong();
		Longitude_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取当前位置（纬）
	 * 
	 * 此字段的版本 >= 0
	 * @return Latitude value 类型为:long
	 * 
	 */
	public long getLatitude()
	{
		return Latitude;
	}


	/**
	 * 设置当前位置（纬）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLatitude(long value)
	{
		this.Latitude = value;
		this.Latitude_u = 1;
	}

	public boolean issetLatitude()
	{
		return this.Latitude_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Latitude_u value 类型为:short
	 * 
	 */
	public short getLatitude_u()
	{
		return Latitude_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLatitude_u(short value)
	{
		this.Latitude_u = value;
	}


	/**
	 * 获取当前位置（经）
	 * 
	 * 此字段的版本 >= 0
	 * @return Longitude value 类型为:long
	 * 
	 */
	public long getLongitude()
	{
		return Longitude;
	}


	/**
	 * 设置当前位置（经）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setLongitude(long value)
	{
		this.Longitude = value;
		this.Longitude_u = 1;
	}

	public boolean issetLongitude()
	{
		return this.Longitude_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Longitude_u value 类型为:short
	 * 
	 */
	public short getLongitude_u()
	{
		return Longitude_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setLongitude_u(short value)
	{
		this.Longitude_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreCoordinate)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Latitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Latitude_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Longitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Longitude_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreCoordinate)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Latitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Latitude_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Longitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Longitude_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
