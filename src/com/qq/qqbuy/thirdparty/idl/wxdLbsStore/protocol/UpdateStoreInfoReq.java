 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *请求
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class  UpdateStoreInfoReq extends NetMessage
{
	/**
	 * 用户机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 请求来源，不可为空，填写来源文件名
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id,填调用方的cmdid,如果调用方没有命令号,填0
	 *
	 * 版本 >= 0
	 */
	 private long sceneId;

	/**
	 * 用户ID
	 *
	 * 版本 >= 0
	 */
	 private long sellerUin;

	/**
	 * 门店信息
	 *
	 * 版本 >= 0
	 */
	 private LbsStoreInfoPo storeInfo = new LbsStoreInfoPo();

	/**
	 * 保留字段
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(sceneId);
		bs.pushLong(sellerUin);
		bs.pushObject(storeInfo);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		sceneId = bs.popUInt();
		sellerUin = bs.popLong();
		storeInfo = (LbsStoreInfoPo) bs.popObject(LbsStoreInfoPo.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x879e1802L;
	}


	/**
	 * 获取用户机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置用户机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取请求来源，不可为空，填写来源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置请求来源，不可为空，填写来源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id,填调用方的cmdid,如果调用方没有命令号,填0
	 * 
	 * 此字段的版本 >= 0
	 * @return sceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return sceneId;
	}


	/**
	 * 设置场景id,填调用方的cmdid,如果调用方没有命令号,填0
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.sceneId = value;
	}


	/**
	 * 获取用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @return sellerUin value 类型为:long
	 * 
	 */
	public long getSellerUin()
	{
		return sellerUin;
	}


	/**
	 * 设置用户ID
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSellerUin(long value)
	{
		this.sellerUin = value;
	}


	/**
	 * 获取门店信息
	 * 
	 * 此字段的版本 >= 0
	 * @return storeInfo value 类型为:LbsStoreInfoPo
	 * 
	 */
	public LbsStoreInfoPo getStoreInfo()
	{
		return storeInfo;
	}


	/**
	 * 设置门店信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:LbsStoreInfoPo
	 * 
	 */
	public void setStoreInfo(LbsStoreInfoPo value)
	{
		if (value != null) {
				this.storeInfo = value;
		}else{
				this.storeInfo = new LbsStoreInfoPo();
		}
	}


	/**
	 * 获取保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置保留字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateStoreInfoReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 17;  //计算字段sellerUin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(storeInfo, null);  //计算字段storeInfo的长度 size_of(LbsStoreInfoPo)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(UpdateStoreInfoReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段sceneId的长度 size_of(uint32_t)
				length += 17;  //计算字段sellerUin的长度 size_of(uint64_t)
				length += ByteStream.getObjectSize(storeInfo, encoding);  //计算字段storeInfo的长度 size_of(LbsStoreInfoPo)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
