//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.qqbuy.thirdparty.idl.wxdLbsStore.WxdLbsStoreAo.java

package com.qq.qqbuy.thirdparty.idl.wxdLbsStore.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.util.io.ICanSerializeObjectExt4Encoding;

import com.paipai.lang.GenericWrapper;
import java.util.Map;
import java.util.HashMap;

/**
 *门店信息汇总
 *
 *@date 2015-03-18 05:00:00
 *
 *@since version:0
*/
public class LbsStoreInfoAllPo  implements ICanSerializeObjectExt4Encoding
{
	/**
	 * 
	 *
	 * 版本 >= 0
	 */
	 private long Version;

	/**
	 * 版本 >= 0
	 */
	 private short Version_u;

	/**
	 * 用户qq
	 *
	 * 版本 >= 0
	 */
	 private long Uin;

	/**
	 * 版本 >= 0
	 */
	 private short Uin_u;

	/**
	 * 门店id
	 *
	 * 版本 >= 0
	 */
	 private String StoreId = new String();

	/**
	 * 版本 >= 0
	 */
	 private short StoreId_u;

	/**
	 * 门店地址（国）
	 *
	 * 版本 >= 0
	 */
	 private String Address1 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address1_u;

	/**
	 * 门店地址（省）
	 *
	 * 版本 >= 0
	 */
	 private String Address2 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address2_u;

	/**
	 * 门店地址（省id）
	 *
	 * 版本 >= 0
	 */
	 private long Address2Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address2Id_u;

	/**
	 * 门店地址（市）
	 *
	 * 版本 >= 0
	 */
	 private String Address3 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address3_u;

	/**
	 * 门店地址（市id）
	 *
	 * 版本 >= 0
	 */
	 private long Address3Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address3Id_u;

	/**
	 * 门店地址（区）
	 *
	 * 版本 >= 0
	 */
	 private String Address4 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address4_u;

	/**
	 * 门店地址（区id）
	 *
	 * 版本 >= 0
	 */
	 private long Address4Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address4Id_u;

	/**
	 * 门店地址（街道）
	 *
	 * 版本 >= 0
	 */
	 private String Address5 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address5_u;

	/**
	 * 门店地址（街道id）
	 *
	 * 版本 >= 0
	 */
	 private long Address5Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address5Id_u;

	/**
	 * 门店地址预留
	 *
	 * 版本 >= 0
	 */
	 private String Address6 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address6_u;

	/**
	 * 门店地址预留
	 *
	 * 版本 >= 0
	 */
	 private long Address6Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address6Id_u;

	/**
	 * 门店地址预留
	 *
	 * 版本 >= 0
	 */
	 private String Address7 = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Address7_u;

	/**
	 * 门店地址预留
	 *
	 * 版本 >= 0
	 */
	 private long Address7Id;

	/**
	 * 版本 >= 0
	 */
	 private short Address7Id_u;

	/**
	 * 门店详细地址
	 *
	 * 版本 >= 0
	 */
	 private String DetailAddress = new String();

	/**
	 * 版本 >= 0
	 */
	 private short DetailAddress_u;

	/**
	 * 门店坐标（纬）
	 *
	 * 版本 >= 0
	 */
	 private long Flatitude;

	/**
	 * 版本 >= 0
	 */
	 private short Flatitude_u;

	/**
	 * 门店坐标（经）
	 *
	 * 版本 >= 0
	 */
	 private long Flongitude;

	/**
	 * 版本 >= 0
	 */
	 private short Flongitude_u;

	/**
	 * 品类字段id
	 *
	 * 版本 >= 0
	 */
	 private long Category;

	/**
	 * 版本 >= 0
	 */
	 private short Category_u;

	/**
	 * 门店地址hash
	 *
	 * 版本 >= 0
	 */
	 private String Hash = new String();

	/**
	 * 版本 >= 0
	 */
	 private short Hash_u;

	/**
	 * 门店状态0:正常状态1:删除
	 *
	 * 版本 >= 0
	 */
	 private long State;

	/**
	 * 版本 >= 0
	 */
	 private short State_u;

	/**
	 * 添加时间
	 *
	 * 版本 >= 0
	 */
	 private long AddTime;

	/**
	 * 版本 >= 0
	 */
	 private short AddTime_u;

	/**
	 * 更新时间
	 *
	 * 版本 >= 0
	 */
	 private long UpdateTime;

	/**
	 * 版本 >= 0
	 */
	 private short UpdateTime_u;

	/**
	 * 扩展字段
	 *
	 * 版本 >= 0
	 */
	 private Map<String,String> Ext = new HashMap<String,String>();

	/**
	 * 版本 >= 0
	 */
	 private short Ext_u;

	/**
	 * 门店名称
	 *
	 * 版本 >= 0
	 */
	 private String storeName = new String();

	/**
	 * 版本 >= 0
	 */
	 private short storeName_u;

	/**
	 * 主营类目
	 *
	 * 版本 >= 0
	 */
	 private String productcategory = new String();

	/**
	 * 版本 >= 0
	 */
	 private short productcategory_u;

	/**
	 * 联系电话
	 *
	 * 版本 >= 0
	 */
	 private String telphone = new String();

	/**
	 * 版本 >= 0
	 */
	 private short telphone_u;

	/**
	 * 人均价格
	 *
	 * 版本 >= 0
	 */
	 private long renPrice;

	/**
	 * 版本 >= 0
	 */
	 private short renPrice_u;

	/**
	 * 营业时间
	 *
	 * 版本 >= 0
	 */
	 private String openTime = new String();

	/**
	 * 版本 >= 0
	 */
	 private short openTime_u;

	/**
	 * 卖家推荐信息
	 *
	 * 版本 >= 0
	 */
	 private String recommend = new String();

	/**
	 * 版本 >= 0
	 */
	 private short recommend_u;

	/**
	 * 特色服务
	 *
	 * 版本 >= 0
	 */
	 private String specService = new String();

	/**
	 * 版本 >= 0
	 */
	 private short specService_u;

	/**
	 * 简介
	 *
	 * 版本 >= 0
	 */
	 private String introduction = new String();

	/**
	 * 版本 >= 0
	 */
	 private short introduction_u;



	public int serialize(ByteStream bs) throws Exception
	{
		bs.pushUInt(getSize(bs.getDecodeCharset()) - 4);
		bs.pushUInt(Version);
		bs.pushUByte(Version_u);
		bs.pushLong(Uin);
		bs.pushUByte(Uin_u);
		bs.pushString(StoreId);
		bs.pushUByte(StoreId_u);
		bs.pushString(Address1);
		bs.pushUByte(Address1_u);
		bs.pushString(Address2);
		bs.pushUByte(Address2_u);
		bs.pushUInt(Address2Id);
		bs.pushUByte(Address2Id_u);
		bs.pushString(Address3);
		bs.pushUByte(Address3_u);
		bs.pushUInt(Address3Id);
		bs.pushUByte(Address3Id_u);
		bs.pushString(Address4);
		bs.pushUByte(Address4_u);
		bs.pushUInt(Address4Id);
		bs.pushUByte(Address4Id_u);
		bs.pushString(Address5);
		bs.pushUByte(Address5_u);
		bs.pushUInt(Address5Id);
		bs.pushUByte(Address5Id_u);
		bs.pushString(Address6);
		bs.pushUByte(Address6_u);
		bs.pushUInt(Address6Id);
		bs.pushUByte(Address6Id_u);
		bs.pushString(Address7);
		bs.pushUByte(Address7_u);
		bs.pushUInt(Address7Id);
		bs.pushUByte(Address7Id_u);
		bs.pushString(DetailAddress);
		bs.pushUByte(DetailAddress_u);
		bs.pushLong(Flatitude);
		bs.pushUByte(Flatitude_u);
		bs.pushLong(Flongitude);
		bs.pushUByte(Flongitude_u);
		bs.pushUInt(Category);
		bs.pushUByte(Category_u);
		bs.pushString(Hash);
		bs.pushUByte(Hash_u);
		bs.pushUInt(State);
		bs.pushUByte(State_u);
		bs.pushUInt(AddTime);
		bs.pushUByte(AddTime_u);
		bs.pushUInt(UpdateTime);
		bs.pushUByte(UpdateTime_u);
		bs.pushObject(Ext);
		bs.pushUByte(Ext_u);
		bs.pushString(storeName);
		bs.pushUByte(storeName_u);
		bs.pushString(productcategory);
		bs.pushUByte(productcategory_u);
		bs.pushString(telphone);
		bs.pushUByte(telphone_u);
		bs.pushUInt(renPrice);
		bs.pushUByte(renPrice_u);
		bs.pushString(openTime);
		bs.pushUByte(openTime_u);
		bs.pushString(recommend);
		bs.pushUByte(recommend_u);
		bs.pushString(specService);
		bs.pushUByte(specService_u);
		bs.pushString(introduction);
		bs.pushUByte(introduction_u);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{
		long size = bs.popUInt();
		int startPosPop = bs.getReadLength();
		if (size == 0)
				return 0;
		Version = bs.popUInt();
		Version_u = bs.popUByte();
		Uin = bs.popLong();
		Uin_u = bs.popUByte();
		StoreId = bs.popString();
		StoreId_u = bs.popUByte();
		Address1 = bs.popString();
		Address1_u = bs.popUByte();
		Address2 = bs.popString();
		Address2_u = bs.popUByte();
		Address2Id = bs.popUInt();
		Address2Id_u = bs.popUByte();
		Address3 = bs.popString();
		Address3_u = bs.popUByte();
		Address3Id = bs.popUInt();
		Address3Id_u = bs.popUByte();
		Address4 = bs.popString();
		Address4_u = bs.popUByte();
		Address4Id = bs.popUInt();
		Address4Id_u = bs.popUByte();
		Address5 = bs.popString();
		Address5_u = bs.popUByte();
		Address5Id = bs.popUInt();
		Address5Id_u = bs.popUByte();
		Address6 = bs.popString();
		Address6_u = bs.popUByte();
		Address6Id = bs.popUInt();
		Address6Id_u = bs.popUByte();
		Address7 = bs.popString();
		Address7_u = bs.popUByte();
		Address7Id = bs.popUInt();
		Address7Id_u = bs.popUByte();
		DetailAddress = bs.popString();
		DetailAddress_u = bs.popUByte();
		Flatitude = bs.popLong();
		Flatitude_u = bs.popUByte();
		Flongitude = bs.popLong();
		Flongitude_u = bs.popUByte();
		Category = bs.popUInt();
		Category_u = bs.popUByte();
		Hash = bs.popString();
		Hash_u = bs.popUByte();
		State = bs.popUInt();
		State_u = bs.popUByte();
		AddTime = bs.popUInt();
		AddTime_u = bs.popUByte();
		UpdateTime = bs.popUInt();
		UpdateTime_u = bs.popUByte();
		Ext = (Map<String,String>)bs.popMap(String.class,String.class);
		Ext_u = bs.popUByte();
		storeName = bs.popString();
		storeName_u = bs.popUByte();
		productcategory = bs.popString();
		productcategory_u = bs.popUByte();
		telphone = bs.popString();
		telphone_u = bs.popUByte();
		renPrice = bs.popUInt();
		renPrice_u = bs.popUByte();
		openTime = bs.popString();
		openTime_u = bs.popUByte();
		recommend = bs.popString();
		recommend_u = bs.popUByte();
		specService = bs.popString();
		specService_u = bs.popUByte();
		introduction = bs.popString();
		introduction_u = bs.popUByte();

		/**********************为了支持多个版本的客户端************************/
		int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
		for(int i = 0;i< needPopBytes; i++)
				bs.popByte();
		/**********************为了支持多个版本的客户端************************/

		return bs.getReadLength();
	} 


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version value 类型为:long
	 * 
	 */
	public long getVersion()
	{
		return Version;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setVersion(long value)
	{
		this.Version = value;
		this.Version_u = 1;
	}

	public boolean issetVersion()
	{
		return this.Version_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Version_u value 类型为:short
	 * 
	 */
	public short getVersion_u()
	{
		return Version_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setVersion_u(short value)
	{
		this.Version_u = value;
	}


	/**
	 * 获取用户qq
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin value 类型为:long
	 * 
	 */
	public long getUin()
	{
		return Uin;
	}


	/**
	 * 设置用户qq
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUin(long value)
	{
		this.Uin = value;
		this.Uin_u = 1;
	}

	public boolean issetUin()
	{
		return this.Uin_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Uin_u value 类型为:short
	 * 
	 */
	public short getUin_u()
	{
		return Uin_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUin_u(short value)
	{
		this.Uin_u = value;
	}


	/**
	 * 获取门店id
	 * 
	 * 此字段的版本 >= 0
	 * @return StoreId value 类型为:String
	 * 
	 */
	public String getStoreId()
	{
		return StoreId;
	}


	/**
	 * 设置门店id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStoreId(String value)
	{
		this.StoreId = value;
		this.StoreId_u = 1;
	}

	public boolean issetStoreId()
	{
		return this.StoreId_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return StoreId_u value 类型为:short
	 * 
	 */
	public short getStoreId_u()
	{
		return StoreId_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStoreId_u(short value)
	{
		this.StoreId_u = value;
	}


	/**
	 * 获取门店地址（国）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address1 value 类型为:String
	 * 
	 */
	public String getAddress1()
	{
		return Address1;
	}


	/**
	 * 设置门店地址（国）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress1(String value)
	{
		this.Address1 = value;
		this.Address1_u = 1;
	}

	public boolean issetAddress1()
	{
		return this.Address1_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address1_u value 类型为:short
	 * 
	 */
	public short getAddress1_u()
	{
		return Address1_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress1_u(short value)
	{
		this.Address1_u = value;
	}


	/**
	 * 获取门店地址（省）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2 value 类型为:String
	 * 
	 */
	public String getAddress2()
	{
		return Address2;
	}


	/**
	 * 设置门店地址（省）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress2(String value)
	{
		this.Address2 = value;
		this.Address2_u = 1;
	}

	public boolean issetAddress2()
	{
		return this.Address2_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2_u value 类型为:short
	 * 
	 */
	public short getAddress2_u()
	{
		return Address2_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress2_u(short value)
	{
		this.Address2_u = value;
	}


	/**
	 * 获取门店地址（省id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2Id value 类型为:long
	 * 
	 */
	public long getAddress2Id()
	{
		return Address2Id;
	}


	/**
	 * 设置门店地址（省id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress2Id(long value)
	{
		this.Address2Id = value;
		this.Address2Id_u = 1;
	}

	public boolean issetAddress2Id()
	{
		return this.Address2Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address2Id_u value 类型为:short
	 * 
	 */
	public short getAddress2Id_u()
	{
		return Address2Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress2Id_u(short value)
	{
		this.Address2Id_u = value;
	}


	/**
	 * 获取门店地址（市）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3 value 类型为:String
	 * 
	 */
	public String getAddress3()
	{
		return Address3;
	}


	/**
	 * 设置门店地址（市）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress3(String value)
	{
		this.Address3 = value;
		this.Address3_u = 1;
	}

	public boolean issetAddress3()
	{
		return this.Address3_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3_u value 类型为:short
	 * 
	 */
	public short getAddress3_u()
	{
		return Address3_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress3_u(short value)
	{
		this.Address3_u = value;
	}


	/**
	 * 获取门店地址（市id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3Id value 类型为:long
	 * 
	 */
	public long getAddress3Id()
	{
		return Address3Id;
	}


	/**
	 * 设置门店地址（市id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress3Id(long value)
	{
		this.Address3Id = value;
		this.Address3Id_u = 1;
	}

	public boolean issetAddress3Id()
	{
		return this.Address3Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address3Id_u value 类型为:short
	 * 
	 */
	public short getAddress3Id_u()
	{
		return Address3Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress3Id_u(short value)
	{
		this.Address3Id_u = value;
	}


	/**
	 * 获取门店地址（区）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4 value 类型为:String
	 * 
	 */
	public String getAddress4()
	{
		return Address4;
	}


	/**
	 * 设置门店地址（区）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress4(String value)
	{
		this.Address4 = value;
		this.Address4_u = 1;
	}

	public boolean issetAddress4()
	{
		return this.Address4_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4_u value 类型为:short
	 * 
	 */
	public short getAddress4_u()
	{
		return Address4_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress4_u(short value)
	{
		this.Address4_u = value;
	}


	/**
	 * 获取门店地址（区id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4Id value 类型为:long
	 * 
	 */
	public long getAddress4Id()
	{
		return Address4Id;
	}


	/**
	 * 设置门店地址（区id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress4Id(long value)
	{
		this.Address4Id = value;
		this.Address4Id_u = 1;
	}

	public boolean issetAddress4Id()
	{
		return this.Address4Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address4Id_u value 类型为:short
	 * 
	 */
	public short getAddress4Id_u()
	{
		return Address4Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress4Id_u(short value)
	{
		this.Address4Id_u = value;
	}


	/**
	 * 获取门店地址（街道）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5 value 类型为:String
	 * 
	 */
	public String getAddress5()
	{
		return Address5;
	}


	/**
	 * 设置门店地址（街道）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress5(String value)
	{
		this.Address5 = value;
		this.Address5_u = 1;
	}

	public boolean issetAddress5()
	{
		return this.Address5_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5_u value 类型为:short
	 * 
	 */
	public short getAddress5_u()
	{
		return Address5_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress5_u(short value)
	{
		this.Address5_u = value;
	}


	/**
	 * 获取门店地址（街道id）
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5Id value 类型为:long
	 * 
	 */
	public long getAddress5Id()
	{
		return Address5Id;
	}


	/**
	 * 设置门店地址（街道id）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress5Id(long value)
	{
		this.Address5Id = value;
		this.Address5Id_u = 1;
	}

	public boolean issetAddress5Id()
	{
		return this.Address5Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address5Id_u value 类型为:short
	 * 
	 */
	public short getAddress5Id_u()
	{
		return Address5Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress5Id_u(short value)
	{
		this.Address5Id_u = value;
	}


	/**
	 * 获取门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @return Address6 value 类型为:String
	 * 
	 */
	public String getAddress6()
	{
		return Address6;
	}


	/**
	 * 设置门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress6(String value)
	{
		this.Address6 = value;
		this.Address6_u = 1;
	}

	public boolean issetAddress6()
	{
		return this.Address6_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address6_u value 类型为:short
	 * 
	 */
	public short getAddress6_u()
	{
		return Address6_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress6_u(short value)
	{
		this.Address6_u = value;
	}


	/**
	 * 获取门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @return Address6Id value 类型为:long
	 * 
	 */
	public long getAddress6Id()
	{
		return Address6Id;
	}


	/**
	 * 设置门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress6Id(long value)
	{
		this.Address6Id = value;
		this.Address6Id_u = 1;
	}

	public boolean issetAddress6Id()
	{
		return this.Address6Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address6Id_u value 类型为:short
	 * 
	 */
	public short getAddress6Id_u()
	{
		return Address6Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress6Id_u(short value)
	{
		this.Address6Id_u = value;
	}


	/**
	 * 获取门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @return Address7 value 类型为:String
	 * 
	 */
	public String getAddress7()
	{
		return Address7;
	}


	/**
	 * 设置门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setAddress7(String value)
	{
		this.Address7 = value;
		this.Address7_u = 1;
	}

	public boolean issetAddress7()
	{
		return this.Address7_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address7_u value 类型为:short
	 * 
	 */
	public short getAddress7_u()
	{
		return Address7_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress7_u(short value)
	{
		this.Address7_u = value;
	}


	/**
	 * 获取门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @return Address7Id value 类型为:long
	 * 
	 */
	public long getAddress7Id()
	{
		return Address7Id;
	}


	/**
	 * 设置门店地址预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddress7Id(long value)
	{
		this.Address7Id = value;
		this.Address7Id_u = 1;
	}

	public boolean issetAddress7Id()
	{
		return this.Address7Id_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Address7Id_u value 类型为:short
	 * 
	 */
	public short getAddress7Id_u()
	{
		return Address7Id_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddress7Id_u(short value)
	{
		this.Address7Id_u = value;
	}


	/**
	 * 获取门店详细地址
	 * 
	 * 此字段的版本 >= 0
	 * @return DetailAddress value 类型为:String
	 * 
	 */
	public String getDetailAddress()
	{
		return DetailAddress;
	}


	/**
	 * 设置门店详细地址
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDetailAddress(String value)
	{
		this.DetailAddress = value;
		this.DetailAddress_u = 1;
	}

	public boolean issetDetailAddress()
	{
		return this.DetailAddress_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return DetailAddress_u value 类型为:short
	 * 
	 */
	public short getDetailAddress_u()
	{
		return DetailAddress_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setDetailAddress_u(short value)
	{
		this.DetailAddress_u = value;
	}


	/**
	 * 获取门店坐标（纬）
	 * 
	 * 此字段的版本 >= 0
	 * @return Flatitude value 类型为:long
	 * 
	 */
	public long getFlatitude()
	{
		return Flatitude;
	}


	/**
	 * 设置门店坐标（纬）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlatitude(long value)
	{
		this.Flatitude = value;
		this.Flatitude_u = 1;
	}

	public boolean issetFlatitude()
	{
		return this.Flatitude_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Flatitude_u value 类型为:short
	 * 
	 */
	public short getFlatitude_u()
	{
		return Flatitude_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlatitude_u(short value)
	{
		this.Flatitude_u = value;
	}


	/**
	 * 获取门店坐标（经）
	 * 
	 * 此字段的版本 >= 0
	 * @return Flongitude value 类型为:long
	 * 
	 */
	public long getFlongitude()
	{
		return Flongitude;
	}


	/**
	 * 设置门店坐标（经）
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setFlongitude(long value)
	{
		this.Flongitude = value;
		this.Flongitude_u = 1;
	}

	public boolean issetFlongitude()
	{
		return this.Flongitude_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Flongitude_u value 类型为:short
	 * 
	 */
	public short getFlongitude_u()
	{
		return Flongitude_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setFlongitude_u(short value)
	{
		this.Flongitude_u = value;
	}


	/**
	 * 获取品类字段id
	 * 
	 * 此字段的版本 >= 0
	 * @return Category value 类型为:long
	 * 
	 */
	public long getCategory()
	{
		return Category;
	}


	/**
	 * 设置品类字段id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCategory(long value)
	{
		this.Category = value;
		this.Category_u = 1;
	}

	public boolean issetCategory()
	{
		return this.Category_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Category_u value 类型为:short
	 * 
	 */
	public short getCategory_u()
	{
		return Category_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setCategory_u(short value)
	{
		this.Category_u = value;
	}


	/**
	 * 获取门店地址hash
	 * 
	 * 此字段的版本 >= 0
	 * @return Hash value 类型为:String
	 * 
	 */
	public String getHash()
	{
		return Hash;
	}


	/**
	 * 设置门店地址hash
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setHash(String value)
	{
		this.Hash = value;
		this.Hash_u = 1;
	}

	public boolean issetHash()
	{
		return this.Hash_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Hash_u value 类型为:short
	 * 
	 */
	public short getHash_u()
	{
		return Hash_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setHash_u(short value)
	{
		this.Hash_u = value;
	}


	/**
	 * 获取门店状态0:正常状态1:删除
	 * 
	 * 此字段的版本 >= 0
	 * @return State value 类型为:long
	 * 
	 */
	public long getState()
	{
		return State;
	}


	/**
	 * 设置门店状态0:正常状态1:删除
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setState(long value)
	{
		this.State = value;
		this.State_u = 1;
	}

	public boolean issetState()
	{
		return this.State_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return State_u value 类型为:short
	 * 
	 */
	public short getState_u()
	{
		return State_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setState_u(short value)
	{
		this.State_u = value;
	}


	/**
	 * 获取添加时间
	 * 
	 * 此字段的版本 >= 0
	 * @return AddTime value 类型为:long
	 * 
	 */
	public long getAddTime()
	{
		return AddTime;
	}


	/**
	 * 设置添加时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setAddTime(long value)
	{
		this.AddTime = value;
		this.AddTime_u = 1;
	}

	public boolean issetAddTime()
	{
		return this.AddTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return AddTime_u value 类型为:short
	 * 
	 */
	public short getAddTime_u()
	{
		return AddTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setAddTime_u(short value)
	{
		this.AddTime_u = value;
	}


	/**
	 * 获取更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @return UpdateTime value 类型为:long
	 * 
	 */
	public long getUpdateTime()
	{
		return UpdateTime;
	}


	/**
	 * 设置更新时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setUpdateTime(long value)
	{
		this.UpdateTime = value;
		this.UpdateTime_u = 1;
	}

	public boolean issetUpdateTime()
	{
		return this.UpdateTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return UpdateTime_u value 类型为:short
	 * 
	 */
	public short getUpdateTime_u()
	{
		return UpdateTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setUpdateTime_u(short value)
	{
		this.UpdateTime_u = value;
	}


	/**
	 * 获取扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext value 类型为:Map<String,String>
	 * 
	 */
	public Map<String,String> getExt()
	{
		return Ext;
	}


	/**
	 * 设置扩展字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<String,String>
	 * 
	 */
	public void setExt(Map<String,String> value)
	{
		if (value != null) {
				this.Ext = value;
				this.Ext_u = 1;
		}
	}

	public boolean issetExt()
	{
		return this.Ext_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return Ext_u value 类型为:short
	 * 
	 */
	public short getExt_u()
	{
		return Ext_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setExt_u(short value)
	{
		this.Ext_u = value;
	}


	/**
	 * 获取门店名称
	 * 
	 * 此字段的版本 >= 0
	 * @return storeName value 类型为:String
	 * 
	 */
	public String getStoreName()
	{
		return storeName;
	}


	/**
	 * 设置门店名称
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setStoreName(String value)
	{
		this.storeName = value;
		this.storeName_u = 1;
	}

	public boolean issetStoreName()
	{
		return this.storeName_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return storeName_u value 类型为:short
	 * 
	 */
	public short getStoreName_u()
	{
		return storeName_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setStoreName_u(short value)
	{
		this.storeName_u = value;
	}


	/**
	 * 获取主营类目
	 * 
	 * 此字段的版本 >= 0
	 * @return productcategory value 类型为:String
	 * 
	 */
	public String getProductcategory()
	{
		return productcategory;
	}


	/**
	 * 设置主营类目
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setProductcategory(String value)
	{
		this.productcategory = value;
		this.productcategory_u = 1;
	}

	public boolean issetProductcategory()
	{
		return this.productcategory_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return productcategory_u value 类型为:short
	 * 
	 */
	public short getProductcategory_u()
	{
		return productcategory_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setProductcategory_u(short value)
	{
		this.productcategory_u = value;
	}


	/**
	 * 获取联系电话
	 * 
	 * 此字段的版本 >= 0
	 * @return telphone value 类型为:String
	 * 
	 */
	public String getTelphone()
	{
		return telphone;
	}


	/**
	 * 设置联系电话
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setTelphone(String value)
	{
		this.telphone = value;
		this.telphone_u = 1;
	}

	public boolean issetTelphone()
	{
		return this.telphone_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return telphone_u value 类型为:short
	 * 
	 */
	public short getTelphone_u()
	{
		return telphone_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setTelphone_u(short value)
	{
		this.telphone_u = value;
	}


	/**
	 * 获取人均价格
	 * 
	 * 此字段的版本 >= 0
	 * @return renPrice value 类型为:long
	 * 
	 */
	public long getRenPrice()
	{
		return renPrice;
	}


	/**
	 * 设置人均价格
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setRenPrice(long value)
	{
		this.renPrice = value;
		this.renPrice_u = 1;
	}

	public boolean issetRenPrice()
	{
		return this.renPrice_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return renPrice_u value 类型为:short
	 * 
	 */
	public short getRenPrice_u()
	{
		return renPrice_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRenPrice_u(short value)
	{
		this.renPrice_u = value;
	}


	/**
	 * 获取营业时间
	 * 
	 * 此字段的版本 >= 0
	 * @return openTime value 类型为:String
	 * 
	 */
	public String getOpenTime()
	{
		return openTime;
	}


	/**
	 * 设置营业时间
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setOpenTime(String value)
	{
		this.openTime = value;
		this.openTime_u = 1;
	}

	public boolean issetOpenTime()
	{
		return this.openTime_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return openTime_u value 类型为:short
	 * 
	 */
	public short getOpenTime_u()
	{
		return openTime_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setOpenTime_u(short value)
	{
		this.openTime_u = value;
	}


	/**
	 * 获取卖家推荐信息
	 * 
	 * 此字段的版本 >= 0
	 * @return recommend value 类型为:String
	 * 
	 */
	public String getRecommend()
	{
		return recommend;
	}


	/**
	 * 设置卖家推荐信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setRecommend(String value)
	{
		this.recommend = value;
		this.recommend_u = 1;
	}

	public boolean issetRecommend()
	{
		return this.recommend_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return recommend_u value 类型为:short
	 * 
	 */
	public short getRecommend_u()
	{
		return recommend_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setRecommend_u(short value)
	{
		this.recommend_u = value;
	}


	/**
	 * 获取特色服务
	 * 
	 * 此字段的版本 >= 0
	 * @return specService value 类型为:String
	 * 
	 */
	public String getSpecService()
	{
		return specService;
	}


	/**
	 * 设置特色服务
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSpecService(String value)
	{
		this.specService = value;
		this.specService_u = 1;
	}

	public boolean issetSpecService()
	{
		return this.specService_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return specService_u value 类型为:short
	 * 
	 */
	public short getSpecService_u()
	{
		return specService_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setSpecService_u(short value)
	{
		this.specService_u = value;
	}


	/**
	 * 获取简介
	 * 
	 * 此字段的版本 >= 0
	 * @return introduction value 类型为:String
	 * 
	 */
	public String getIntroduction()
	{
		return introduction;
	}


	/**
	 * 设置简介
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setIntroduction(String value)
	{
		this.introduction = value;
		this.introduction_u = 1;
	}

	public boolean issetIntroduction()
	{
		return this.introduction_u != 0;
	}
	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return introduction_u value 类型为:short
	 * 
	 */
	public short getIntroduction_u()
	{
		return introduction_u;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:short
	 * 
	 */
	public void setIntroduction_u(short value)
	{
		this.introduction_u = value;
	}


	/**
	 *   计算类长度
	 *   用于告诉解包者，该类只放了这么长的数据
	 *  
 	 */
	protected int getClassSize()
	{
		int length = getSize() - 4;
		try{

		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	
	/**
	 *   计算类长度
	 *   这个是该类的实际长度，在序列化时bytestream会调用这个方法
	 *  
 	 */
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreInfoAllPo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uin的长度 size_of(uint64_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(StoreId, null);  //计算字段StoreId的长度 size_of(String)
				length += 1;  //计算字段StoreId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address1, null);  //计算字段Address1的长度 size_of(String)
				length += 1;  //计算字段Address1_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address2, null);  //计算字段Address2的长度 size_of(String)
				length += 1;  //计算字段Address2_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address2Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address2Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address3, null);  //计算字段Address3的长度 size_of(String)
				length += 1;  //计算字段Address3_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address3Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address3Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address4, null);  //计算字段Address4的长度 size_of(String)
				length += 1;  //计算字段Address4_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address4Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address4Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address5, null);  //计算字段Address5的长度 size_of(String)
				length += 1;  //计算字段Address5_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address5Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address5Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address6, null);  //计算字段Address6的长度 size_of(String)
				length += 1;  //计算字段Address6_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address6Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address6Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address7, null);  //计算字段Address7的长度 size_of(String)
				length += 1;  //计算字段Address7_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address7Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address7Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DetailAddress, null);  //计算字段DetailAddress的长度 size_of(String)
				length += 1;  //计算字段DetailAddress_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Flatitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Flatitude_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Flongitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Flongitude_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Category的长度 size_of(uint32_t)
				length += 1;  //计算字段Category_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Hash, null);  //计算字段Hash的长度 size_of(String)
				length += 1;  //计算字段Hash_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += 4;  //计算字段AddTime的长度 size_of(uint32_t)
				length += 1;  //计算字段AddTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UpdateTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, null);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(storeName, null);  //计算字段storeName的长度 size_of(String)
				length += 1;  //计算字段storeName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(productcategory, null);  //计算字段productcategory的长度 size_of(String)
				length += 1;  //计算字段productcategory_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(telphone, null);  //计算字段telphone的长度 size_of(String)
				length += 1;  //计算字段telphone_u的长度 size_of(uint8_t)
				length += 4;  //计算字段renPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段renPrice_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(openTime, null);  //计算字段openTime的长度 size_of(String)
				length += 1;  //计算字段openTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(recommend, null);  //计算字段recommend的长度 size_of(String)
				length += 1;  //计算字段recommend_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(specService, null);  //计算字段specService的长度 size_of(String)
				length += 1;  //计算字段specService_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(introduction, null);  //计算字段introduction的长度 size_of(String)
				length += 1;  //计算字段introduction_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


	/**
	 *   计算类长度
	 *   这个是实现String字符集传入的方法
	 *  
 	 */
	public int getSize(String encoding)
	{
		int length = 4;
		try{
				length = 4;  //size_of(LbsStoreInfoAllPo)
				length += 4;  //计算字段Version的长度 size_of(uint32_t)
				length += 1;  //计算字段Version_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Uin的长度 size_of(uint64_t)
				length += 1;  //计算字段Uin_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(StoreId, encoding);  //计算字段StoreId的长度 size_of(String)
				length += 1;  //计算字段StoreId_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address1, encoding);  //计算字段Address1的长度 size_of(String)
				length += 1;  //计算字段Address1_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address2, encoding);  //计算字段Address2的长度 size_of(String)
				length += 1;  //计算字段Address2_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address2Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address2Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address3, encoding);  //计算字段Address3的长度 size_of(String)
				length += 1;  //计算字段Address3_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address3Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address3Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address4, encoding);  //计算字段Address4的长度 size_of(String)
				length += 1;  //计算字段Address4_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address4Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address4Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address5, encoding);  //计算字段Address5的长度 size_of(String)
				length += 1;  //计算字段Address5_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address5Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address5Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address6, encoding);  //计算字段Address6的长度 size_of(String)
				length += 1;  //计算字段Address6_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address6Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address6Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Address7, encoding);  //计算字段Address7的长度 size_of(String)
				length += 1;  //计算字段Address7_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Address7Id的长度 size_of(uint32_t)
				length += 1;  //计算字段Address7Id_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(DetailAddress, encoding);  //计算字段DetailAddress的长度 size_of(String)
				length += 1;  //计算字段DetailAddress_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Flatitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Flatitude_u的长度 size_of(uint8_t)
				length += 17;  //计算字段Flongitude的长度 size_of(uint64_t)
				length += 1;  //计算字段Flongitude_u的长度 size_of(uint8_t)
				length += 4;  //计算字段Category的长度 size_of(uint32_t)
				length += 1;  //计算字段Category_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Hash, encoding);  //计算字段Hash的长度 size_of(String)
				length += 1;  //计算字段Hash_u的长度 size_of(uint8_t)
				length += 4;  //计算字段State的长度 size_of(uint32_t)
				length += 1;  //计算字段State_u的长度 size_of(uint8_t)
				length += 4;  //计算字段AddTime的长度 size_of(uint32_t)
				length += 1;  //计算字段AddTime_u的长度 size_of(uint8_t)
				length += 4;  //计算字段UpdateTime的长度 size_of(uint32_t)
				length += 1;  //计算字段UpdateTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(Ext, encoding);  //计算字段Ext的长度 size_of(Map)
				length += 1;  //计算字段Ext_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(storeName, encoding);  //计算字段storeName的长度 size_of(String)
				length += 1;  //计算字段storeName_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(productcategory, encoding);  //计算字段productcategory的长度 size_of(String)
				length += 1;  //计算字段productcategory_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(telphone, encoding);  //计算字段telphone的长度 size_of(String)
				length += 1;  //计算字段telphone_u的长度 size_of(uint8_t)
				length += 4;  //计算字段renPrice的长度 size_of(uint32_t)
				length += 1;  //计算字段renPrice_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(openTime, encoding);  //计算字段openTime的长度 size_of(String)
				length += 1;  //计算字段openTime_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(recommend, encoding);  //计算字段recommend的长度 size_of(String)
				length += 1;  //计算字段recommend_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(specService, encoding);  //计算字段specService的长度 size_of(String)
				length += 1;  //计算字段specService_u的长度 size_of(uint8_t)
				length += ByteStream.getObjectSize(introduction, encoding);  //计算字段introduction的长度 size_of(String)
				length += 1;  //计算字段introduction_u的长度 size_of(uint8_t)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}


/**
 ********************以下信息是每个版本的字段********************
 */



	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
