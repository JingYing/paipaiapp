 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;

/**
 *通过商品ID和统计类型来设置统计数值之回复
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  SetDealStatInfoByComdyIdResp extends NetMessage
{
	/**
	 * 保留字段，无用字段
	 *
	 * 版本 >= 0
	 */
	 private String reserver = new String();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushString(reserver);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		reserver = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10518812L;
	}


	/**
	 * 获取保留字段，无用字段
	 * 
	 * 此字段的版本 >= 0
	 * @return reserver value 类型为:String
	 * 
	 */
	public String getReserver()
	{
		return reserver;
	}


	/**
	 * 设置保留字段，无用字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserver(String value)
	{
		this.reserver = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(SetDealStatInfoByComdyIdResp)
				length += ByteStream.getObjectSize(reserver, null);  //计算字段reserver的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(SetDealStatInfoByComdyIdResp)
				length += ByteStream.getObjectSize(reserver, encoding);  //计算字段reserver的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
