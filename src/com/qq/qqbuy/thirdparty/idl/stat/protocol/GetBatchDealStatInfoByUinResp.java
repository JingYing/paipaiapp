 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *批量获取订单的所有统计信息之回复
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  GetBatchDealStatInfoByUinResp extends NetMessage
{
	/**
	 * 业务统计信息
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Map<uint32_t,Integer>> StatDatas = new HashMap<uint32_t,Map<uint32_t,Integer>>();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(StatDatas);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();

		// 生成反序列化属性StatDatas相应的范型参数包裹对象(包裹了该属性中范型的类型)。 
		GenericWrapper StatDatasPaiPai00 = new GenericWrapper();
		StatDatasPaiPai00.setType(HashMap.class);
		GenericWrapper[] StatDatasPaiPaiArray00= new GenericWrapper[2];
		StatDatasPaiPaiArray00[0] = new GenericWrapper(uint32_t.class);
		StatDatasPaiPaiArray00[1] = new GenericWrapper();
		GenericWrapper StatDatasPaiPai11 = new GenericWrapper();
		StatDatasPaiPai11.setType(HashMap.class);
		GenericWrapper[] StatDatasPaiPaiArray11= new GenericWrapper[2];
		StatDatasPaiPaiArray11[0] = new GenericWrapper(uint32_t.class);
		StatDatasPaiPaiArray11[1] = new GenericWrapper(Integer.class);
		StatDatasPaiPai11.setGenericParameters(StatDatasPaiPaiArray11);


		StatDatasPaiPaiArray00[1] = StatDatasPaiPai11;
		StatDatasPaiPai00.setGenericParameters(StatDatasPaiPaiArray00);



		StatDatas = (Map<uint32_t,Map<uint32_t,Integer>>)bs.popObject(StatDatasPaiPai00);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10518813L;
	}


	/**
	 * 获取业务统计信息
	 * 
	 * 此字段的版本 >= 0
	 * @return StatDatas value 类型为:Map<uint32_t,Map<uint32_t,Integer>>
	 * 
	 */
	public Map<uint32_t,Map<uint32_t,Integer>> getStatDatas()
	{
		return StatDatas;
	}


	/**
	 * 设置业务统计信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Map<uint32_t,Integer>>
	 * 
	 */
	public void setStatDatas(Map<uint32_t,Map<uint32_t,Integer>> value)
	{
		if (value != null) {
				this.StatDatas = value;
		}else{
				this.StatDatas = new HashMap<uint32_t,Map<uint32_t,Integer>>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetBatchDealStatInfoByUinResp)
				length += ByteStream.getObjectSize(StatDatas, null);  //计算字段StatDatas的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetBatchDealStatInfoByUinResp)
				length += ByteStream.getObjectSize(StatDatas, encoding);  //计算字段StatDatas的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
