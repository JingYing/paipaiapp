 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: test.StatServDao.java

package com.qq.qqbuy.thirdparty.idl.stat.protocol;


import com.paipai.util.io.ByteStream;
import com.paipai.lang.GenericWrapper;
import com.paipai.netframework.kernal.NetMessage;
import java.util.Map;
import com.paipai.lang.uint32_t;
import java.util.HashMap;

/**
 *通过商品ID获取定单统计信息之回复
 *
 *@date 2014-09-03 04:24:58
 *
 *@since version:0
*/
public class  GetDealStatInfoByComdyIdResp extends NetMessage
{
	/**
	 * 业务统计信息
	 *
	 * 版本 >= 0
	 */
	 private Map<uint32_t,Integer> StatData = new HashMap<uint32_t,Integer>();


	public int serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(StatData);
		return bs.getWrittenLength();
	}
	
	public int unSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		StatData = (Map<uint32_t,Integer>)bs.popMap(uint32_t.class,Integer.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x10518811L;
	}


	/**
	 * 获取业务统计信息
	 * 
	 * 此字段的版本 >= 0
	 * @return StatData value 类型为:Map<uint32_t,Integer>
	 * 
	 */
	public Map<uint32_t,Integer> getStatData()
	{
		return StatData;
	}


	/**
	 * 设置业务统计信息
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Map<uint32_t,Integer>
	 * 
	 */
	public void setStatData(Map<uint32_t,Integer> value)
	{
		if (value != null) {
				this.StatData = value;
		}else{
				this.StatData = new HashMap<uint32_t,Integer>();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GetDealStatInfoByComdyIdResp)
				length += ByteStream.getObjectSize(StatData, null);  //计算字段StatData的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(GetDealStatInfoByComdyIdResp)
				length += ByteStream.getObjectSize(StatData, encoding);  //计算字段StatData的长度 size_of(Map)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
