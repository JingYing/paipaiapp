 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *resp
 *
 *@date 2012-06-01 10:03::40
 *
 *@since version:0
*/
public class  GameSubmitResp implements IServiceObject
{
	public long result;
	/**
	 * 版本 >= 0
	 */
	 private OrderResult orderResult = new OrderResult();


	public int Serialize(ByteStream bs) throws Exception
	{

		bs.pushUInt(result);
		bs.pushObject(orderResult);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{

		result = bs.popUInt();
		orderResult = (OrderResult) bs.popObject(OrderResult.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70618804L;
	}


	/**
	 * 获取
	 * 
	 * 此字段的版本 >= 0
	 * @return orderResult value 类型为:OrderResult
	 * 
	 */
	public OrderResult getOrderResult()
	{
		return orderResult;
	}


	/**
	 * 设置
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:OrderResult
	 * 
	 */
	public void setOrderResult(OrderResult value)
	{
		if (value != null) {
				this.orderResult = value;
		}else{
				this.orderResult = new OrderResult();
		}
	}


	public long getResult()
	{
		return  this.result;
	}

	
	public void setResult(long value)
	{
		this.result = value;
	}

	
	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 4;
		try{
				length = 4;  //size_of(GameSubmitResp)
				length += ByteStream.getObjectSize(orderResult);  //计算字段orderResult的长度 size_of(OrderResult)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
