 
 
//auto gen by paipai.java.augogen ver 1.0
//auther wendyhu

package com.qq.qqbuy.thirdparty.idl.vb2c.protocol;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;


/**
 *req
 *
 *@date 2012-06-01 10:03::40
 *
 *@since version:0
*/
public class  GameSubmitReq implements IServiceObject
{
	/**
	 * 来源
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 下单filter
	 *
	 * 版本 >= 0
	 */
	 private GameOrderFilter gameOrderFilter = new GameOrderFilter();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(source);
		bs.pushObject(gameOrderFilter);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		source = bs.popString();
		gameOrderFilter = (GameOrderFilter) bs.popObject(GameOrderFilter.class);
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x70611804L;
	}


	/**
	 * 获取来源
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取下单filter
	 * 
	 * 此字段的版本 >= 0
	 * @return gameOrderFilter value 类型为:GameOrderFilter
	 * 
	 */
	public GameOrderFilter getGameOrderFilter()
	{
		return gameOrderFilter;
	}


	/**
	 * 设置下单filter
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:GameOrderFilter
	 * 
	 */
	public void setGameOrderFilter(GameOrderFilter value)
	{
		if (value != null) {
				this.gameOrderFilter = value;
		}else{
				this.gameOrderFilter = new GameOrderFilter();
		}
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(GameSubmitReq)
				length += ByteStream.getObjectSize(source);  //计算字段source的长度 size_of(String)
				length += ByteStream.getObjectSize(gameOrderFilter);  //计算字段gameOrderFilter的长度 size_of(GameOrderFilter)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
