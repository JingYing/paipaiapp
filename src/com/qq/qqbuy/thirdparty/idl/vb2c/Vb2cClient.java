package com.qq.qqbuy.thirdparty.idl.vb2c;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang.StringUtils;

import com.paipai.component.c2cplatform.IAsynWebStub;
import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.Log;
import com.qq.qqbuy.thirdparty.idl.SupportIDLBaseClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameOrderFilter;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSubmitReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GameSubmitResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealInfoResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExReqBo;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetDealListExResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameItemReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameItemResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.GetGameProductResp;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.MobileOrderFilter;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.MobileSubmitReq;
import com.qq.qqbuy.thirdparty.idl.vb2c.protocol.MobileSubmitResp;

/**
 * @author winsonwu
 * @Created 2012-6-1 <a href="mailto:wurongxin86@gmail.com">Winson</a>
 */
public class Vb2cClient extends SupportIDLBaseClient
{

    /**
     * 
     * @Title: queryDealList
     * 
     * @Description: 查询用户的虚拟订单列表
     * @param buyerUin
     *            用户qq号码
     * @param pn
     *            起始页 从1开始
     * @param ps
     *            每页的大小，默认为50
     * @param state
     *            获取订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消
     * @param mkey
     *            机器码
     * @param skey
     *            登陆skey
     * @param dealType
     *            查询的订单类型 1是话费，3是网游
     * @return 设定文件
     * 
     * @return GetDealListExResp 返回类型
     * 
     * @author wendyhu modify 20130412 ,add description and fix bugs
     * @throws
     */
    public static GetDealListExResp queryDealList(long buyerUin, int pn,
            int ps, int state, String mkey, String skey, String dealType)
    {
        long start = System.currentTimeMillis();

        // 1、check param
        if (ps <= 0)
        {
            ps = 50;
        }
        if (pn < 1)
        {
            pn = 1;
        }

        GetDealListExReq req = new GetDealListExReq();
        req.setMachineKey(mkey);
        req.setSource(CALL_IDL_SOURCE);
        GetDealListExReqBo value = new GetDealListExReqBo();
        value.setBuyerUin(buyerUin);
        long startIndex = (pn - 1) * ps;
        value.setStart(startIndex);
        value.setPageSize(ps);
        if (StringUtils.isNotBlank(dealType))
        {
            Map<String, String> filter = new HashMap<String, String>();
            filter.put("deal_type", dealType);
            value.setFilter(filter);
        }
        if (state > 0)
        {
            Vector<uint32_t> stateList = new Vector<uint32_t>();
            stateList.add(new uint32_t(state));
            value.setDealStateList(stateList);
        }
        req.setReq(value);

        GetDealListExResp resp = new GetDealListExResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(buyerUin);
        stub.setSkey(skey.getBytes());
        stub.setMachineKey(mkey.getBytes());
        stub.setOperator(buyerUin);
        int ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;

    }

    // winson:为了解决登录态打通问题，但虚拟那边接口没有machineKey字段。故在Source字段中采用source$#machineKey方式将machineKey带过去。
    private static String genVb2cSource(String machineKey)
    {
        return CALL_IDL_SOURCE + "$#" + machineKey;
    }

    /**
     * 
     * @Title: queryDealInfo
     * @Description: 查询虚拟订单详情
     * @param buyerUin
     *            买家UIN
     * @param dealId
     *            订单id
     * @param mkey
     *            机器码
     * @param skey
     *            登陆态
     * @param uin
     *            操作者
     * @return 设定文件
     * @return GetDealInfoResp 返回类型
     * @throws
     */
    public static GetDealInfoResp queryDealInfo(long buyerUin, String dealId,
            String mkey, String skey, long uin)
    {
        long start = System.currentTimeMillis();
        GetDealInfoReq req = new GetDealInfoReq();
        req.setBuyerUin(buyerUin);
        req.setDealId(dealId);
        req.setSource(genVb2cSource(mkey));

        GetDealInfoResp resp = new GetDealInfoResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(uin);
        stub.setSkey(skey.getBytes());
        stub.setMachineKey(mkey.getBytes());
        stub.setOperator(uin);
        int ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;
        return ret == SUCCESS ? resp : null;
    }

    public static MobileSubmitResp mobileOrderDeal(long buyerUin,
            String mobile, long amount, String vb2cTag, String mkey,
            String skey, long uin)
    {
        long start = System.currentTimeMillis();
        MobileSubmitReq req = new MobileSubmitReq();
        MobileOrderFilter reqFilter = new MobileOrderFilter();
        reqFilter.setBuyerUin(buyerUin);
        reqFilter.setAmount(amount);
        reqFilter.setMobile(mobile);
        reqFilter.setVb2ctag(vb2cTag);
        req.setMobileOrderFilter(reqFilter);
        req.setSource(genVb2cSource(mkey));

        MobileSubmitResp resp = new MobileSubmitResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setUin(uin);
        stub.setSkey(skey.getBytes());
        stub.setMachineKey(mkey.getBytes());
        
        stub.setOperator(uin);
        int ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;
         
         //添加监控
         String msg = resp.getResult() == 0 ? "IDL虚拟下单成功" : "IDL虚拟下单失败";
        return ret == SUCCESS ? resp : null;
    }

    public static GameSubmitResp gameOrderDeal(long buyerUin, String itemId,
            long buyCount, String sectionName, String sectionCode,
            String serverName, String serverCode, String account,
            String vb2cTag, String mkey, String skey, long uin)
    {
        long start = System.currentTimeMillis();
        GameSubmitReq req = new GameSubmitReq();
        GameOrderFilter reqFilter = new GameOrderFilter();
        reqFilter.setBuyerUin(buyerUin);
        reqFilter.setItemId(itemId);
        reqFilter.setNum(buyCount);
        reqFilter.setSection(sectionName);
        reqFilter.setSectionCode(sectionCode);
        reqFilter.setServer(serverName);
        reqFilter.setServerCode(serverCode);
        reqFilter.setAccount(account);
        reqFilter.setVb2ctag(vb2cTag);
        req.setGameOrderFilter(reqFilter);
        req.setSource(genVb2cSource(mkey));

        GameSubmitResp resp = new GameSubmitResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        stub.setOperator(uin);
        stub.setUin(uin);
		stub.setSkey(skey.getBytes());
		stub.setMachineKey(mkey.getBytes());
        int ret = invokePaiPaiIDL(req, resp, stub);
        String msg = resp.getResult() == 0 ? "IDL游戏下单成功" : "IDL游戏下单成功";
        return ret == SUCCESS ? resp : null;
    }

    public static GetGameProductResp queryGameProductInfo(String gameName)
    {
        long start = System.currentTimeMillis();
        GetGameProductReq req = new GetGameProductReq();
        req.setGame(gameName);
        req.setSource(CALL_IDL_SOURCE);

        GetGameProductResp resp = new GetGameProductResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        int ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;


        //添加监控
        String msg = resp.getResult() == 0 ? "IDL查询游戏物品成功" : "IDL查询游戏物品失败";
        return ret == SUCCESS ? resp : null;
    }

    public static GetGameItemResp queryGameItemInfo(String productId)
    {
        long start = System.currentTimeMillis();
        GetGameItemReq req = new GetGameItemReq();
        req.setProductId(productId);
        req.setSource(CALL_IDL_SOURCE);

        GetGameItemResp resp = new GetGameItemResp();
        IAsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
        int ret = invokePaiPaiIDL(req, resp, stub);

        long timeCost = System.currentTimeMillis() - start;

        //添加监控
        String msg = resp.getResult() == 0 ? "IDL查询游戏商品成功" : "IDL查询游戏商品失败";
        return ret == SUCCESS ? resp : null;
    }

}
