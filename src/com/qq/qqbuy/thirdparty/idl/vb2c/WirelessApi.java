/*
 * vb2c无线API接口
 * 
 * 2010-11-8       terrellhu
 */

package  com.qq.qqbuy.thirdparty.idl.vb2c;

import java.util.Map;
import java.util.Vector;
import com.paipai.lang.uint8_t;
import com.paipai.lang.uint32_t;
import com.paipai.util.annotation.ApiProtocol;
import com.paipai.util.annotation.Field;
import com.paipai.util.annotation.HeadApiProtocol;
import com.paipai.util.annotation.Member;

@HeadApiProtocol(cPlusNamespace="vb2c::open_api::wireless",needInit=true, timers="60")
public class WirelessApi
{  
	@Member(desc = "订单信息", cPlusNamespace="vb2c::open_api::wireless",  isNeedUFlag = true)
	class DealInfo
	{
		@Field(desc="版本号", defaultValue="20101108")
		uint32_t version;
		@Field(desc="订单ID")	
		String  dealId;
		@Field(desc="订单类型，0-手机；1-网游；3-农场道具")
		uint32_t dealType;
		@Field(desc="商品名称")	
		String  itemName;
		@Field(desc="订单金额")
		uint32_t payFee;
		@Field(desc="购买数量")
		uint32_t  num;
		@Field(desc="下单时间")
		uint32_t  dealGenTime;
		@Field(desc="订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消" )
		uint32_t dealState;
		@Field(desc="订单状态描述" )
		String dealStateDesc;
		@Field(desc="供应商名称")
		String  sellerName;
		@Field(desc="供应商SPID")
		uint32_t sellerSpid;
		@Field(desc="客服电话")
		String  svrPhone;
		@Field(desc="扩展信息，跟据订单类型不同，有不同的字段")
		Map<String, String> extInfo;
		
		@Field(desc="FLAG字段")
		uint8_t version_u;
		uint8_t dealId_u;
		uint8_t dealType_u;
		uint8_t itemName_u;		
		uint8_t payFee_u;
		uint8_t num_u;
		uint8_t dealGenTime_u;
		uint8_t dealState_u;
		uint8_t dealStateDesc_u;
		uint8_t sellerName_u;
		uint8_t sellerSpid_u;
		uint8_t svrPhone_u;
		uint8_t extInfo_u;
	}
	
	@Member(desc = "订单List",cPlusNamespace="vb2c::open_api::wireless")
	class DealInfoList
	{
		Vector<DealInfo> dealInfoList;
	}
	
	@Member(desc = "订单查询filter", cPlusNamespace="vb2c::open_api::wireless",  isNeedUFlag = true)
	class QueryFilter
	{
		@Field(desc="版本号", defaultValue="20101108")
		uint32_t version;
		@Field(desc="买家QQ号码")
		uint32_t buyerUin;
		@Field(desc="订单状态")
		uint32_t dealState;
		@Field(desc="起始位置")
		uint32_t start;
		@Field(desc="每页大小，最大为50，超过50，默认为50")
		uint32_t pageSize;
		@Field(desc="订单总数，第一次取的时候填0")
		uint32_t totalNum;
		
		@Field(desc="FLAG字段")
		uint8_t version_u;
		uint8_t buyerUin_u;
		uint8_t dealState_u;
		uint8_t start_u;
		uint8_t pageSize_u;
		uint8_t totalNum_u;
	}
	
	@Member(desc = "网游产品信息", cPlusNamespace="vb2c::open_api::wireless")
	class GameProductInfo
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="产品ID")
		String productId;
		@Field(desc="产品名称")
		String productName;
		@Field(desc="产品图片")
		String productPic;
		@Field(desc="充值类型")
		String chargeType;
		@Field(desc="充值速度")
		String chargeAce;
		@Field(desc="可充值数量")
		String chargeNum;
	}
	
	@Member(desc = "面值对应网游产品信息", cPlusNamespace="vb2c::open_api::wireless")
	class GameProductByAmount
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="面值")
		uint32_t amount;
		Vector<GameProductInfo> pdtInfo;
	}
	
	@Member(desc = "面值对应网游产品信息列表", cPlusNamespace="vb2c::open_api::wireless")
	class GameProductList
	{
		Vector<GameProductByAmount> gameProductList;
	}
	
	@Member(desc = "网游区服信息结构", cPlusNamespace="vb2c::open_api::wireless")
	class GameSectionServer
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="区名")
		String sectionName;
		@Field(desc="区编码")
		String sectionCode;
		@Field(desc="服名及编码列表")
		Map<String, String> serverList;
	}
	
	@Member(desc = "网游区服信息结构列表", cPlusNamespace="vb2c::open_api::wireless")
	class GameSectionServerList
	{
		@Field(desc="游戏名")
		String gameName;
		@Field(desc="区服列表")
		Vector<GameSectionServer> gssList;
	}
	
	@Member(desc = "商品信息", cPlusNamespace="vb2c::open_api::wireless")
	class ItemInfo
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="商品ID")
		String itemId;
		@Field(desc="商品名称")
		String itemName;
    	@Field(desc="商品价格")
		uint32_t itemPrice;
	}
	@Member(desc = "话费下单filter", cPlusNamespace="vb2c::open_api::wireless")
	class MobileOrderFilter
	{
		@Field(desc="版本号", defaultValue="20111026")
		uint32_t version;
		
		@Field(desc="买家UIN")
		uint32_t buyerUin;
		@Field(desc="手机号码")
		String mobile;
		@Field(desc="面值")
		uint32_t amount;
		@Field(desc="vb2ctag")
		String vb2ctag;
	}
	
	@Member(desc = "网游下单filter", cPlusNamespace="vb2c::open_api::wireless")
	class GameOrderFilter
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="买家UIN")
		uint32_t buyerUin;
		@Field(desc="商品ID")
		String itemId;
		@Field(desc="购买数量")
		uint32_t num;
		@Field(desc="区名")
		String section;
		@Field(desc="区编码")
		String sectionCode;
		@Field(desc="服编码")
		String serverCode;
		@Field(desc="服名")
		String server;
		@Field(desc="网游帐号")
		String account;
		@Field(desc="vb2ctag")
		String vb2ctag;
		@Field(desc="支付类型，1:财付通支付,4:网购-财付通收银台支付", version=20120727)
		uint32_t payType;
	}
	
	@Member(desc = "网游修改订单并发货请求", cPlusNamespace="vb2c::open_api::wireless", isNeedUFlag=true)
	class GameOrderModifyPayFilter
	{
		@Field(desc="版本号", defaultValue="20120711")
		uint32_t version;
		@Field(desc="订单ID")
		String orderId;
		@Field(desc="支付ID")
		String payId;
		@Field(desc="支付类型")
		uint32_t payType;
		@Field(desc="商品ID")
		String itemId;
		@Field(desc="买家UIN")
		uint32_t buyerUin;
		@Field(desc="订单金额，单位:分")
		uint32_t orderAmount;
		@Field(desc="单价")
		uint32_t unitPrice;
		@Field(desc="购买数量")
		uint32_t count;
		@Field(desc="支付金额")
		uint32_t payAmount;
		@Field(desc="实际购买数量")
		uint32_t realCount;
		@Field(desc="支付成功时间")
		uint32_t paySuccTime;
		@Field(desc="扩展字段")
		Map<String,String> extInfo;
		
		@Field(desc="FLAG字段")
		uint8_t version_u;
		uint8_t orderId_u;
		uint8_t payId_u;
		uint8_t payType_u;
		uint8_t itemId_u;
		uint8_t buyerUin_u;
		uint8_t orderAmount_u;
		uint8_t unitPrice_u;
		uint8_t count_u;
		uint8_t payAmount_u;
		uint8_t realCount_u;
		uint8_t extInfo_u;
	}

	@Member(desc = "下单结果", cPlusNamespace="vb2c::open_api::wireless")
	class OrderResult
	{
		@Field(desc="版本号", defaultValue="20101115")
		uint32_t version;
		@Field(desc="订单ID")
		String dealId;
    	@Field(desc="商品名称")
		String itemName;
    	@Field(desc="商品价格")
		uint32_t itemPrice;
	}
	
	@ApiProtocol(cmdid="0x70611801L",desc="查询交易列表") 
	class GetBuyerDealList
    {
        @ApiProtocol(cmdid="0x70611801L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="查询条件")
			QueryFilter queryFilter;
	    }
        
        @ApiProtocol(cmdid="0x70618801L",desc="resp")
        class Resp
        {
        	@Field(desc="订单总数")
            uint32_t totalNum;
			@Field(desc="订单列表")
           	DealInfoList dealList;
        }
    }

	@ApiProtocol(cmdid="0x70611802L",desc="查询订单信息") 
	class GetDealInfo
    {
        @ApiProtocol(cmdid="0x70611802L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="买家UIN")
			uint32_t buyerUin;
			@Field(desc="订单ID")
			String dealId;
	    }
        
        @ApiProtocol(cmdid="0x70618802L",desc="resp")
        class Resp
        {
        	DealInfo dealInfo;
        }
    }
	
	@ApiProtocol(cmdid="0x70611803L",desc="手机下单") 
	class MobileSubmit
    {
        @ApiProtocol(cmdid="0x70611803L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="下单filter")
			MobileOrderFilter mobileOrderFilter;
	    }
        
        @ApiProtocol(cmdid="0x70618803L",desc="resp")
        class Resp
        {
        	OrderResult orderResult;
        }
    }
	
	@ApiProtocol(cmdid="0x70611804L",desc="网游下单") 
	class GameSubmit
    {
        @ApiProtocol(cmdid="0x70611804L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="下单filter")
			GameOrderFilter gameOrderFilter;
	    }
        
        @ApiProtocol(cmdid="0x70618804L",desc="resp")
        class Resp
        {
        	OrderResult orderResult;
        }
    }
	
	@ApiProtocol(cmdid="0x70611807L",desc="网游修改订单并发货") 
	class GameModifyPay
    {
        @ApiProtocol(cmdid="0x70611807L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="修改订单、支付filter")
			GameOrderModifyPayFilter gameOrderModifyPayFilter;
	    }
        
        @ApiProtocol(cmdid="0x70618807L",desc="resp")
        class Resp
        {
        	OrderResult orderResult;
        }
    }
	
	@ApiProtocol(cmdid="0x70611805L",desc="跟据游戏名取网游区服及产品信息") 
	class GetGameProduct
    {
        @ApiProtocol(cmdid="0x70611805L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="游戏名称")
			String game;
	    }
        
        @ApiProtocol(cmdid="0x70618805L",desc="resp")
        class Resp
        {
        	GameSectionServerList gssList;
        	GameProductList gamePdtList;
        }
    }
	
	@ApiProtocol(cmdid="0x70611806L", desc="跟据产品ID取商品信息") 
	class GetGameItem
    {
        @ApiProtocol(cmdid="0x70611806L", desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="产品ID")
			String productId;
	    }
        
        @ApiProtocol(cmdid="0x70618806L",desc="resp")
        class Resp
        {
        	ItemInfo itemInfo;
        }
    }
	
	@ApiProtocol(cmdid="0x70611808L",desc="收银台支付网游直充下单") 
	class GameCashierSubmit
    {
        @ApiProtocol(cmdid="0x70611808L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="下单filter")
			GameOrderFilter gameOrderFilter;
	    }
        
        @ApiProtocol(cmdid="0x70618808L",desc="resp")
        class Resp
        {
        	OrderResult orderResult;
        }
    }
	
	
	//-------------------------------------------------------//
	
	@Member(desc = "订单查询filter", cPlusNamespace="vb2c::open_api::wireless",  isNeedUFlag = true)
	class GetDealListExReqBo
	{
		@Field(desc="版本号", defaultValue="20101108")
		uint32_t version;
		@Field(desc="买家QQ号码")
		uint32_t buyerUin;
		@Field(desc="订单状态，1-等待买家付款 ；2-等待发货； 3-发货中；4-退款中；5-交易成功；6-交易完成，已退款； 7-交易取消" )
		Vector<uint32_t> dealStateList;
		@Field(desc="渠道ID")
		uint32_t channelId;
		@Field(desc="起始位置，从0开始")
		uint32_t start;
		@Field(desc="每页大小，最大为50，超过50，默认为50")
		uint32_t pageSize;
		@Field(desc="其他查询条件")
		Map<String, String> filter;
		
		@Field(desc="FLAG字段")
		uint8_t version_u;
		uint8_t buyerUin_u;
		uint8_t dealStateList_u;
		uint8_t channelId_u;
		uint8_t start_u;
		uint8_t pageSize_u;
		uint8_t filter_u;
	}
	
	@Member(desc = "订单查询filter", cPlusNamespace="vb2c::open_api::wireless",  isNeedUFlag = false)
	class GetDealListExRespBo
	{
		@Field(desc="版本号", defaultValue="20101108")
		uint32_t version;
		@Field(desc="订单总数")
        uint32_t totalNum;
		@Field(desc="订单列表")
       	DealInfoList dealList;
	}
	
	@ApiProtocol(cmdid="0x70611809L",desc="查询交易列表") 
	class GetDealListEx
    {
        @ApiProtocol(cmdid="0x70611809L",desc="req")
        class Req
        {
			@Field(desc="来源")
            String source;
			@Field(desc="来源")
            String machineKey;
			@Field(desc="查询条件")
			GetDealListExReqBo req;
	    }
        
        @ApiProtocol(cmdid="0x70618809L",desc="resp")
        class Resp
        {
        	@Field(desc="查询结果")
        	GetDealListExRespBo resp;
        }
    }
}
