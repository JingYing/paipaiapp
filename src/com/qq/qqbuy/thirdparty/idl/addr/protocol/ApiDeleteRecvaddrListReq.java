 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.qq.o2o.thirdparty.idl.recvaddr.RecvaddrApiAo.java

package com.qq.qqbuy.thirdparty.idl.addr.protocol;


import java.util.Vector;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

/**
 *删除收货地址列表请求类
 *
 *@date 2013-05-09 04:22:53
 *
 *@since version:0
*/
public class  ApiDeleteRecvaddrListReq implements IServiceObject
{
	/**
	 * 机器码
	 *
	 * 版本 >= 0
	 */
	 private String machineKey = new String();

	/**
	 * 来源,不能为空
	 *
	 * 版本 >= 0
	 */
	 private String source = new String();

	/**
	 * 场景id,预留
	 *
	 * 版本 >= 0
	 */
	 private long scene;

	/**
	 * 收货地址Po列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<ApiRecvaddr> vecApiRecvaddr = new Vector<ApiRecvaddr>();

	/**
	 * 请求保留字
	 *
	 * 版本 >= 0
	 */
	 private String inReserve = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(machineKey);
		bs.pushString(source);
		bs.pushUInt(scene);
		bs.pushObject(vecApiRecvaddr);
		bs.pushString(inReserve);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		machineKey = bs.popString();
		source = bs.popString();
		scene = bs.popUInt();
		vecApiRecvaddr = (Vector<ApiRecvaddr>)bs.popVector(ApiRecvaddr.class);
		inReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x22121805L;
	}


	/**
	 * 获取机器码
	 * 
	 * 此字段的版本 >= 0
	 * @return machineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return machineKey;
	}


	/**
	 * 设置机器码
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.machineKey = value;
	}


	/**
	 * 获取来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @return source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return source;
	}


	/**
	 * 设置来源,不能为空
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.source = value;
	}


	/**
	 * 获取场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @return scene value 类型为:long
	 * 
	 */
	public long getScene()
	{
		return scene;
	}


	/**
	 * 设置场景id,预留
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setScene(long value)
	{
		this.scene = value;
	}


	/**
	 * 获取收货地址Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @return vecApiRecvaddr value 类型为:Vector<ApiRecvaddr>
	 * 
	 */
	public Vector<ApiRecvaddr> getVecApiRecvaddr()
	{
		return vecApiRecvaddr;
	}


	/**
	 * 设置收货地址Po列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<ApiRecvaddr>
	 * 
	 */
	public void setVecApiRecvaddr(Vector<ApiRecvaddr> value)
	{
		if (value != null) {
				this.vecApiRecvaddr = value;
		}else{
				this.vecApiRecvaddr = new Vector<ApiRecvaddr>();
		}
	}


	/**
	 * 获取请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @return inReserve value 类型为:String
	 * 
	 */
	public String getInReserve()
	{
		return inReserve;
	}


	/**
	 * 设置请求保留字
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setInReserve(String value)
	{
		this.inReserve = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiDeleteRecvaddrListReq)
				length += ByteStream.getObjectSize(machineKey, null);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, null);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecApiRecvaddr, null);  //计算字段vecApiRecvaddr的长度 size_of(Vector)
				length += ByteStream.getObjectSize(inReserve, null);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}
	public int getSize(String encoding)
	{
		int length = 0;
		try{
				length = 0;  //size_of(ApiDeleteRecvaddrListReq)
				length += ByteStream.getObjectSize(machineKey, encoding);  //计算字段machineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(source, encoding);  //计算字段source的长度 size_of(String)
				length += 4;  //计算字段scene的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(vecApiRecvaddr, encoding);  //计算字段vecApiRecvaddr的长度 size_of(Vector)
				length += ByteStream.getObjectSize(inReserve, encoding);  //计算字段inReserve的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
