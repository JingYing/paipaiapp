
/**
* @file ModifyDefaultRecvAddrReq.java
* @brief ModifyDefaultRecvAddrReq java 文件
* 
* @author 
* @version 1.0
* @date 
* @bug  无
* @warning 无
*/

package com.qq.qqbuy.thirdparty.idl.addr.protocol;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.util.io.ByteStream;

public class ModifyDefaultRecvAddrReq implements IServiceObject {

    private String strMachineKey; ///<
    private String strSource; ///<
    private String strInReserve; ///<
    private long dwUin; ///<
    private long dwAddrId; ///<
    //SETTER函数段
    public void setstrMachineKey(String  value)
    {
        this.strMachineKey = value;
    }

    //GETTER函数段
    public String getstrMachineKey()
    {
        return this.strMachineKey;
    }

    //SETTER函数段
    public void setstrSource(String  value)
    {
        this.strSource = value;
    }

    //GETTER函数段
    public String getstrSource()
    {
        return this.strSource;
    }

    //SETTER函数段
    public void setstrInReserve(String  value)
    {
        this.strInReserve = value;
    }

    //GETTER函数段
    public String getstrInReserve()
    {
        return this.strInReserve;
    }

    //SETTER函数段
    public void setdwUin(long  value)
    {
        this.dwUin = value;
    }

    //GETTER函数段
    public long getdwUin()
    {
        return this.dwUin;
    }

    //SETTER函数段
    public void setdwAddrId(long  value)
    {
        this.dwAddrId = value;
    }

    //GETTER函数段
    public long getdwAddrId()
    {
        return this.dwAddrId;
    }

    private String strOutReserve; ///<
    //SETTER函数段
    public void setstrOutReserve(String  value)
    {
        this.strOutReserve = value;
    }

    //GETTER函数段
    public String getstrOutReserve()
    {
        return this.strOutReserve;
    }


	public int Serialize(ByteStream bs) throws Exception {
        if(this.strMachineKey == null)
            bs.pushUInt(0);
        else{
            bs.pushUInt(this.strMachineKey.length());
            bs.pushBytes(this.strMachineKey.getBytes(), this.strMachineKey.length());
        }
        if(this.strSource == null)
            bs.pushUInt(0);
        else{
            bs.pushUInt(this.strSource.length());
            bs.pushBytes(this.strSource.getBytes(), this.strSource.length());
        }
        if(this.strInReserve == null)
            bs.pushUInt(0);
        else{
            bs.pushUInt(this.strInReserve.length());
            bs.pushBytes(this.strInReserve.getBytes(), this.strInReserve.length());
        }
        bs.pushUInt(this.dwUin);
        bs.pushUInt(this.dwAddrId);
        if(this.strOutReserve == null)
            bs.pushUInt(0);
        else{
            bs.pushUInt(this.strOutReserve.length());
            bs.pushBytes(this.strOutReserve.getBytes(), this.strOutReserve.length());
        }
		return bs.getWrittenLength();
	}

	public int UnSerialize(ByteStream bs) throws Exception {
        this.strMachineKey = bs.popString();
        this.strSource = bs.popString();
        this.strInReserve = bs.popString();
        this.dwUin = bs.popUInt();
        this.dwAddrId = bs.popUInt();
        this.strOutReserve = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId() {
		return 0x1216180f;
	}
}

