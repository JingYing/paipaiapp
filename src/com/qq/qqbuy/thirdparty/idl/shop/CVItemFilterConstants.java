package com.qq.qqbuy.thirdparty.idl.shop;

/**
 * 反编译com.paipai.c2c.api.item.constants.CVItemFilterConstants
 * @author jingying
 *
 */
public class CVItemFilterConstants
{
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_ID_ASC = 0;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_ID_DESC = 1;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_ASC = 2;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_ADDTIME_DESC = 3;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_DOWN_TIME_ASC = 4;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_DOWN_TIME_DESC = 5;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_UP_TIME_ASC = 6;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_UP_TIME_DESC = 7;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_PRICE_ASC = 8;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_PRICE_DESC = 9;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_BUYCNT_ASC = 10;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_BUYCNT_DESC = 11;
  public static final int C2C_WW_COMDY_FIND_ORDERY_LEFT_TIME_ASC = 12;
  public static final int C2C_WW_COMDY_FIND_ORDERY_LEFT_TIME_DESC = 13;
  public static final int C2C_WW_COMDY_FIND_ORDERY_MY_TIME_ASC = 14;
  public static final int C2C_WW_COMDY_FIND_ORDERY_MY_TIME_DESC = 15;
  public static final int C2C_WW_COMDY_FIND_ORDERY_LEFT_NUM_ASC = 16;
  public static final int C2C_WW_COMDY_FIND_ORDERY_LEFT_NUM_DESC = 17;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_SOLD_ASC = 18;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_SOLD_DESC = 19;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_VISIT_ASC = 20;
  public static final int C2C_WW_COMDY_FIND_ORDERY_BY_VISIT_DESC = 21;
  public static final String ITEM_FILTER_KEY_ID = "itemid";
  public static final String ITEM_FILTER_KEY_TITLE = "title";
  public static final String ITEM_FILTER_KEY_STATE = "state";
  public static final String ITEM_FILTER_KEY_STATE_EXCLUDE = "stateexclude";
  public static final String ITEM_FILTER_KEY_SHOPCATEGORY = "shopcategory";
  public static final String ITEM_FILTER_KEY_PRICE_START = "pricestart";
  public static final String ITEM_FILTER_KEY_PRICE_END = "priceend";
  public static final String ITEM_FILTER_KEY_DEALTYPE = "dealtype";
  public static final String ITEM_FILTER_KEY_RECOMMEND = "recommond";
  public static final String ITEM_FILTER_KEY_RECOMMEND_EXCLUDE = "recommondexclude";
  public static final String ITEM_FILTER_KEY_NEED_EXTINFO = "needextinfo";
  public static final String ITEM_FILTER_KEY_NEED_STOCKINFO = "needstockinfo";
  public static final String ITEM_FILTER_KEY_PROPERTY = "property";
  public static final String ITEM_FILTER_KEY_PROPERTY_EXCLUDE = "propertyexclude";
  public static final String ITEM_FILTER_KEY_DETAIL_PROPERTY = "detailproperty";
  public static final String ITEM_FILTER_KEY_LEAFCLASSID = "leafclassid";
  public static final String ITEM_FILTER_KEY_LASTMODIFYTIME_START = "lastmodifytimestart";
  public static final String ITEM_FILTER_KEY_LASTMODIFYTIME_END = "lastmodifytimeend";
  public static final String ITEM_FILTER_KEY_ADDTIME_START = "addttimestart";
  public static final String ITEM_FILTER_KEY_ADDTIME_END = "addtimeend";
  public static final String ITEM_FILTER_KEY_LASTEDITTIME_START = "lasteditttimestart";
  public static final String ITEM_FILTER_KEY_LASTEDITTIME_END = "lastedittimeend";
  public static final String ITEM_FILTER_KEY_LASTUPTIME_START = "lastupttimestart";
  public static final String ITEM_FILTER_KEY_LASTUPTIME_END = "lastuptimeend";
  public static final String ITEM_FILTER_KEY_LASTDOWNTIME_START = "lastdownttimestart";
  public static final String ITEM_FILTER_KEY_LASTDOWNTIME_END = "lastdowntimeend";
  public static final String ITEM_FILTER_KEY_PRODUCT_ID = "productid";
  public static final String ITEM_FILTER_KEY_STOCK_ID = "stockid";
  public static final String ITEM_FILTER_KEY_ITEM_TAGS = "itemtags";
  public static final String ITEM_FILTER_STATE_SELLING = "1110";
  public static final String ITEM_FILTER_STATE_MYDOWN = "1";
  public static final String ITEM_FILTER_STATE_TIMEDOWN = "10000";
  public static final String ITEM_FILTER_STATE_INSTORE = "1000000000000000000000000000000000000000000000000001000000010001";
  public static final String ITEM_FILTER_STATE_TIMEUP = "1000000000000000000000000000000000000000000000000000000000000000";
  public static final String ITEM_FILTER_STATE_NEVERUP = "1000000000000";
  public static final String ITEM_FILTER_STATE_SOLD = "100000";
  public static final String ITEM_FILTER_STATE_WAITOP = "101000000000";
  public static final String ITEM_FILTER_STATE_DELETED = "1100000000";
  public static final int MAP_PROPERTY_OPTYPE_FOR_PROPERTY = 1;
  public static final int MAP_PROPERTY_BUYER_FIND_SELLER_SELLING_ITEM = 2;
}