package com.qq.qqbuy.thirdparty.idl.shop.protocol.boss;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.paipai.util.io.ByteStream;
import com.qq.qqbuy.thirdparty.idl.boss.IBossServiceObject;

/**
 * @author raywu
 * 
 */
public class BizStatQueryOneResp implements IBossServiceObject {
	private long result;
	private List<StatRecord> records = new ArrayList<StatRecord>(); // 统计记录
	private String cmdDesc = "bizstat_srv";

	public void setCmdDesc(String cmdDesc) {
		this.cmdDesc = cmdDesc;
	}

	@Override
	public int Serialize(ByteStream bs) throws Exception {
		return 0;
	}

	@Override
	public int UnSerialize(ByteStream bs) throws Exception {
		bs.popUInt();// cmdId
		result = bs.popUInt();// subCmdId
		if (result == 0) {
			int iSize = bs.popInt();
			for (int i = 0; i < iSize; ++i) {
				StatRecord rc = new StatRecord();
				rc.UnSerialize(bs);
				records.add(rc);
			}
		}
		return bs.getReadLength();
	}

	@Override
	public long getCmdId() {
		return 0x000000a8;
	}

	@Override
	public String getCmdDesc() {
		return cmdDesc;
	}

	@Override
	public long getSubCmdId() {
		return 0;
	}

	@Override
	public void setHasResult(boolean arg0) {

	}

	public List<StatRecord> getRecords() {
		return records;
	}

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}

	public int getStatData(int bizType, int statType, int uin) {
		int iStatData = 0;

		for (StatRecord record : this.records) {
			if (record.getBizType() == bizType
					&& record.getStatType() == statType
					&& record.getUin() == uin) {
				iStatData = record.getStatData() < 0 ? 0 : record.getStatData();
			}
		}

		return iStatData;
	}

	public Map<Integer, Integer> getStatData(int bizType, int statType,
			Collection<Integer> userids) {
		Map<Integer, Integer> result = new HashMap<Integer, Integer>();
		for (Integer userid : userids) {
			Integer statData = getStatData(bizType, statType, userid);
			if (statData > 0) {
				result.put(userid, statData);
			}
		}
		return result;
	}

	public int getStatDataNew(int bizType, int statType, long uin) {
		int iStatData = 0;

		for (StatRecord record : this.records) {
			if (record.getBizType() == bizType
					&& record.getStatType() == statType
					&& record.getUin() == uin) {
				iStatData = record.getStatData() < 0 ? 0 : record.getStatData();
			}
		}
		return iStatData;
	}

	public Map<Long, Integer> getStatDataNew(int bizType, int statType,
			Collection<Long> userids) {
		Map<Long, Integer> result = new HashMap<Long, Integer>();
		for (Long userid : userids) {
			Integer statData = getStatDataNew(bizType, statType, userid);
			if (statData > 0) {
				result.put(userid, statData);
			}
		}
		return result;
	}

	public void getStatDataNew(int bizType, int statType,
			Collection<Long> userids, Map<Long, Integer> result) {
		for (Long userid : userids) {
			Integer statData = getStatDataNew(bizType, statType, userid);
			if (statData > 0) {
				result.put(userid, statData);
			}
		}
	}

}
