/*     */ package com.qq.qqbuy.thirdparty.idl.shop.protocol;
/*     */ 
/*     */ import com.paipai.util.io.ByteStream;
/*     */ import com.paipai.util.io.ICanSerializeObject;
/*     */ 
/*     */ public class ApiShopCategory
/*     */   implements ICanSerializeObject
/*     */ {
/*  27 */   private long version = 1L;
/*     */ 
/*  34 */   private String categoryId = new String();
/*     */ 
/*  41 */   private String desc = new String();
/*     */ 
/*  48 */   private String picUrl = new String();
/*     */ 
/*  55 */   private String parentId = new String();
/*     */   private long shopId;
/*     */   private long sequence;
/*     */   private long commNum;
/*     */   private short version_u;
/*     */   private short categoryId_u;
/*     */   private short shopId_u;
/*     */   private short sequence_u;
/*     */   private short desc_u;
/*     */   private short commNum_u;
/*     */   private short picUrl_u;
/*     */   private short parentId_u;
/*     */ 
/*     */   public int serialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/* 122 */     bs.pushUInt(getClassSize());
/* 123 */     bs.pushUInt(this.version);
/* 124 */     bs.pushString(this.categoryId);
/* 125 */     bs.pushString(this.desc);
/* 126 */     bs.pushString(this.picUrl);
/* 127 */     bs.pushString(this.parentId);
/* 128 */     bs.pushUInt(this.shopId);
/* 129 */     bs.pushUInt(this.sequence);
/* 130 */     bs.pushUInt(this.commNum);
/* 131 */     bs.pushUByte(this.version_u);
/* 132 */     bs.pushUByte(this.categoryId_u);
/* 133 */     bs.pushUByte(this.shopId_u);
/* 134 */     bs.pushUByte(this.sequence_u);
/* 135 */     bs.pushUByte(this.desc_u);
/* 136 */     bs.pushUByte(this.commNum_u);
/* 137 */     bs.pushUByte(this.picUrl_u);
/* 138 */     bs.pushUByte(this.parentId_u);
/* 139 */     return bs.getWrittenLength();
/*     */   }
/*     */ 
/*     */   public int unSerialize(ByteStream bs) throws Exception
/*     */   {
/* 144 */     long size = bs.popUInt();
/* 145 */     int startPosPop = bs.getReadLength();
/* 146 */     if (size == 0L)
/* 147 */       return 0;
/* 148 */     this.version = bs.popUInt();
/* 149 */     this.categoryId = bs.popString();
/* 150 */     this.desc = bs.popString();
/* 151 */     this.picUrl = bs.popString();
/* 152 */     this.parentId = bs.popString();
/* 153 */     this.shopId = bs.popUInt();
/* 154 */     this.sequence = bs.popUInt();
/* 155 */     this.commNum = bs.popUInt();
/* 156 */     this.version_u = bs.popUByte();
/* 157 */     this.categoryId_u = bs.popUByte();
/* 158 */     this.shopId_u = bs.popUByte();
/* 159 */     this.sequence_u = bs.popUByte();
/* 160 */     this.desc_u = bs.popUByte();
/* 161 */     this.commNum_u = bs.popUByte();
/* 162 */     this.picUrl_u = bs.popUByte();
/* 163 */     this.parentId_u = bs.popUByte();
/*     */ 
/* 166 */     int needPopBytes = (int)size - (bs.getReadLength() - startPosPop);
/* 167 */     for (int i = 0; i < needPopBytes; i++) {
/* 168 */       bs.popByte();
/*     */     }
/*     */ 
/* 171 */     return bs.getReadLength();
/*     */   }
/*     */ 
/*     */   public long getVersion()
/*     */   {
/* 184 */     return this.version;
/*     */   }
/*     */ 
/*     */   public void setVersion(long value)
/*     */   {
/* 197 */     this.version = value;
/* 198 */     this.version_u = 1;
/*     */   }
/*     */ 
/*     */   public String getCategoryId()
/*     */   {
/* 211 */     return this.categoryId;
/*     */   }
/*     */ 
/*     */   public void setCategoryId(String value)
/*     */   {
/* 224 */     if (value != null) {
/* 225 */       this.categoryId = value;
/* 226 */       this.categoryId_u = 1;
/*     */     }
/*     */   }
/*     */ 
/*     */   public String getDesc()
/*     */   {
/* 240 */     return this.desc;
/*     */   }
/*     */ 
/*     */   public void setDesc(String value)
/*     */   {
/* 253 */     if (value != null) {
/* 254 */       this.desc = value;
/* 255 */       this.desc_u = 1;
/*     */     }
/*     */   }
/*     */ 
/*     */   public String getPicUrl()
/*     */   {
/* 269 */     return this.picUrl;
/*     */   }
/*     */ 
/*     */   public void setPicUrl(String value)
/*     */   {
/* 282 */     if (value != null) {
/* 283 */       this.picUrl = value;
/* 284 */       this.picUrl_u = 1;
/*     */     }
/*     */   }
/*     */ 
/*     */   public String getParentId()
/*     */   {
/* 298 */     return this.parentId;
/*     */   }
/*     */ 
/*     */   public void setParentId(String value)
/*     */   {
/* 311 */     if (value != null) {
/* 312 */       this.parentId = value;
/* 313 */       this.parentId_u = 1;
/*     */     }
/*     */   }
/*     */ 
/*     */   public long getShopId()
/*     */   {
/* 327 */     return this.shopId;
/*     */   }
/*     */ 
/*     */   public void setShopId(long value)
/*     */   {
/* 340 */     this.shopId = value;
/* 341 */     this.shopId_u = 1;
/*     */   }
/*     */ 
/*     */   public long getSequence()
/*     */   {
/* 354 */     return this.sequence;
/*     */   }
/*     */ 
/*     */   public void setSequence(long value)
/*     */   {
/* 367 */     this.sequence = value;
/* 368 */     this.sequence_u = 1;
/*     */   }
/*     */ 
/*     */   public long getCommNum()
/*     */   {
/* 381 */     return this.commNum;
/*     */   }
/*     */ 
/*     */   public void setCommNum(long value)
/*     */   {
/* 394 */     this.commNum = value;
/* 395 */     this.commNum_u = 1;
/*     */   }
/*     */ 
/*     */   public short getVersion_u()
/*     */   {
/* 408 */     return this.version_u;
/*     */   }
/*     */ 
/*     */   public void setVersion_u(short value)
/*     */   {
/* 421 */     this.version_u = value;
/*     */   }
/*     */ 
/*     */   public short getCategoryId_u()
/*     */   {
/* 434 */     return this.categoryId_u;
/*     */   }
/*     */ 
/*     */   public void setCategoryId_u(short value)
/*     */   {
/* 447 */     this.categoryId_u = value;
/*     */   }
/*     */ 
/*     */   public short getShopId_u()
/*     */   {
/* 460 */     return this.shopId_u;
/*     */   }
/*     */ 
/*     */   public void setShopId_u(short value)
/*     */   {
/* 473 */     this.shopId_u = value;
/*     */   }
/*     */ 
/*     */   public short getSequence_u()
/*     */   {
/* 486 */     return this.sequence_u;
/*     */   }
/*     */ 
/*     */   public void setSequence_u(short value)
/*     */   {
/* 499 */     this.sequence_u = value;
/*     */   }
/*     */ 
/*     */   public short getDesc_u()
/*     */   {
/* 512 */     return this.desc_u;
/*     */   }
/*     */ 
/*     */   public void setDesc_u(short value)
/*     */   {
/* 525 */     this.desc_u = value;
/*     */   }
/*     */ 
/*     */   public short getCommNum_u()
/*     */   {
/* 538 */     return this.commNum_u;
/*     */   }
/*     */ 
/*     */   public void setCommNum_u(short value)
/*     */   {
/* 551 */     this.commNum_u = value;
/*     */   }
/*     */ 
/*     */   public short getPicUrl_u()
/*     */   {
/* 564 */     return this.picUrl_u;
/*     */   }
/*     */ 
/*     */   public void setPicUrl_u(short value)
/*     */   {
/* 577 */     this.picUrl_u = value;
/*     */   }
/*     */ 
/*     */   public short getParentId_u()
/*     */   {
/* 590 */     return this.parentId_u;
/*     */   }
/*     */ 
/*     */   public void setParentId_u(short value)
/*     */   {
/* 603 */     this.parentId_u = value;
/*     */   }
/*     */ 
/*     */   protected int getClassSize()
/*     */   {
/* 614 */     int length = getSize() - 4;
/*     */ 
/* 620 */     return length;
/*     */   }
/*     */ 
/*     */   public int getSize()
/*     */   {
/* 631 */     int length = 4;
/*     */     try {
/* 633 */       length = 4;
/* 634 */       length += 4;
/* 635 */       length += ByteStream.getObjectSize(this.categoryId);
/* 636 */       length += ByteStream.getObjectSize(this.desc);
/* 637 */       length += ByteStream.getObjectSize(this.picUrl);
/* 638 */       length += ByteStream.getObjectSize(this.parentId);
/* 639 */       length += 4;
/* 640 */       length += 4;
/* 641 */       length += 4;
/* 642 */       length++;
/* 643 */       length++;
/* 644 */       length++;
/* 645 */       length++;
/* 646 */       length++;
/* 647 */       length++;
/* 648 */       length++;
/* 649 */       length++;
/*     */     } catch (Exception e) {
/* 651 */       e.printStackTrace();
/*     */     }
/* 653 */     return length;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.protocol.ApiShopCategory
 * JD-Core Version:    0.6.2
 */