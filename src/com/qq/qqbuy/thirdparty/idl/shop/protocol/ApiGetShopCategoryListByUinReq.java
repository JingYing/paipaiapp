/*     */ package com.qq.qqbuy.thirdparty.idl.shop.protocol;
/*     */ 
/*     */ import com.paipai.netframework.kernal.NetMessage;
/*     */ import com.paipai.util.io.ByteStream;
/*     */ 
/*     */ public class ApiGetShopCategoryListByUinReq extends NetMessage
/*     */ {
/*  26 */   private String machineKey = new String();
/*     */ 
/*  33 */   private String source = new String();
/*     */   private long scene;
/*     */   private long uin;
/*     */   private long pageSize;
/*     */   private long startIndex;
/*  68 */   private String inReserve = new String();
/*     */ 
/*     */   public int serialize(ByteStream bs)
/*     */     throws Exception
/*     */   {
/*  73 */     bs.pushString(this.machineKey);
/*  74 */     bs.pushString(this.source);
/*  75 */     bs.pushUInt(this.scene);
/*  76 */     bs.pushUInt(this.uin);
/*  77 */     bs.pushUInt(this.pageSize);
/*  78 */     bs.pushUInt(this.startIndex);
/*  79 */     bs.pushString(this.inReserve);
/*  80 */     return bs.getWrittenLength();
/*     */   }
/*     */ 
/*     */   public int unSerialize(ByteStream bs) throws Exception
/*     */   {
/*  85 */     this.machineKey = bs.popString();
/*  86 */     this.source = bs.popString();
/*  87 */     this.scene = bs.popUInt();
/*  88 */     this.uin = bs.popUInt();
/*  89 */     this.pageSize = bs.popUInt();
/*  90 */     this.startIndex = bs.popUInt();
/*  91 */     this.inReserve = bs.popString();
/*  92 */     return bs.getReadLength();
/*     */   }
/*     */ 
/*     */   public long getCmdId()
/*     */   {
/*  97 */     return 0x20031801L;
/*     */   }
/*     */ 
/*     */   public String getMachineKey()
/*     */   {
/* 110 */     return this.machineKey;
/*     */   }
/*     */ 
/*     */   public void setMachineKey(String value)
/*     */   {
/* 123 */     if (value != null)
/* 124 */       this.machineKey = value;
/*     */     else
/* 126 */       this.machineKey = new String();
/*     */   }
/*     */ 
/*     */   public String getSource()
/*     */   {
/* 140 */     return this.source;
/*     */   }
/*     */ 
/*     */   public void setSource(String value)
/*     */   {
/* 153 */     if (value != null)
/* 154 */       this.source = value;
/*     */     else
/* 156 */       this.source = new String();
/*     */   }
/*     */ 
/*     */   public long getScene()
/*     */   {
/* 170 */     return this.scene;
/*     */   }
/*     */ 
/*     */   public void setScene(long value)
/*     */   {
/* 183 */     this.scene = value;
/*     */   }
/*     */ 
/*     */   public long getUin()
/*     */   {
/* 196 */     return this.uin;
/*     */   }
/*     */ 
/*     */   public void setUin(long value)
/*     */   {
/* 209 */     this.uin = value;
/*     */   }
/*     */ 
/*     */   public long getPageSize()
/*     */   {
/* 222 */     return this.pageSize;
/*     */   }
/*     */ 
/*     */   public void setPageSize(long value)
/*     */   {
/* 235 */     this.pageSize = value;
/*     */   }
/*     */ 
/*     */   public long getStartIndex()
/*     */   {
/* 248 */     return this.startIndex;
/*     */   }
/*     */ 
/*     */   public void setStartIndex(long value)
/*     */   {
/* 261 */     this.startIndex = value;
/*     */   }
/*     */ 
/*     */   public String getInReserve()
/*     */   {
/* 274 */     return this.inReserve;
/*     */   }
/*     */ 
/*     */   public void setInReserve(String value)
/*     */   {
/* 287 */     if (value != null)
/* 288 */       this.inReserve = value;
/*     */     else
/* 290 */       this.inReserve = new String();
/*     */   }
/*     */ 
/*     */   protected int getClassSize()
/*     */   {
/* 297 */     return getSize() - 4;
/*     */   }
/*     */ 
/*     */   public int getSize()
/*     */   {
/* 303 */     int length = 0;
/*     */     try {
/* 305 */       length = 0;
/* 306 */       length += ByteStream.getObjectSize(this.machineKey);
/* 307 */       length += ByteStream.getObjectSize(this.source);
/* 308 */       length += 4;
/* 309 */       length += 4;
/* 310 */       length += 4;
/* 311 */       length += 4;
/* 312 */       length += ByteStream.getObjectSize(this.inReserve);
/*     */     } catch (Exception e) {
/* 314 */       e.printStackTrace();
/*     */     }
/* 316 */     return length;
/*     */   }
/*     */ }

/* Location:           C:\Users\Administrator\Desktop\paipaijar\
 * Qualified Name:     com.paipai.c2c.api.shop.ao.protocol.ApiGetShopCategoryListByUinReq
 * JD-Core Version:    0.6.2
 */