package com.qq.qqbuy.dealopr.po;
public enum DealRateState
{
  DEAL_RATE_NO_EVAL(0, "评价未到期"), 

  DEAL_RATE_BUYER_NO_SELLER_NO(1, "买家未评，卖家未评"), 

  DEAL_RATE_BUYER_DONE_SELLER_NO(2, "买家已评，卖家未评"), 

  DEAL_RATE_BUYER_NO_SELLER_DONE(3, "卖家已评，买家未评"), 

  DEAL_RATE_BUYER_DONE_SELLER_DONE(4, "买家已评，卖家已评"), 

  DEAL_RATE_DISABLE(5, "不可评价"), 

  UNKNOW(-1, "未知评价状态");

  private final int code;
  private final String showMeg;

  private DealRateState(int code, String showMeg) { this.code = code;
    this.showMeg = showMeg; }

  public static DealRateState getEnumByCode(int code)
  {
    for (DealRateState state : values()) {
      if (state.code == code) {
        return state;
      }
    }
    return UNKNOW;
  }

  public int getCode() {
    return this.code;
  }

  public String getShowMeg() {
    return this.showMeg;
  }
}
