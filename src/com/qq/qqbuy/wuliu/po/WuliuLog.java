

package com.qq.qqbuy.wuliu.po;




/**
 *物流日志
 *
 *@date 2011-05-19 08:24::16
 *
 *@since version:1
*/
public class WuliuLog 
{
	/**
	 * 创建时间
	 *
	 * 版本 >= 0
	 */
	 private long dealTime;

	/**
	 * 物流状态描述
	 *
	 * 版本 >= 0
	 */
	 private String wlStateDesc = new String();

    public long getDealTime() {
        return dealTime;
    }

    public void setDealTime(long dealTime) {
        this.dealTime = dealTime;
    }

    public String getWlStateDesc() {
        return wlStateDesc;
    }

    public void setWlStateDesc(String wlStateDesc) {
        this.wlStateDesc = wlStateDesc;
    }




	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	//@Override
	//public String toString() {
	//	return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	//}
}
