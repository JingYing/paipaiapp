package com.qq.qqbuy.version.dao;

import com.qq.qqbuy.version.po.VersionInfo;
/**
 * 处理手机版本信息接口
 * @author zhaohuayu
 *
 */
public interface VersionInfoDao {

	/**
	 * 入库手机版本信息
	 * @param versionInfo
	 * @return
	 */
	public int insertVersionInfo(VersionInfo versionInfo) throws Exception;
	/**
	 * 更新手机版本信息
	 * @param uuId
	 * @param versionInfo
	 * @return
	 */
	public int updateVersionInfo(String mk, String appID, VersionInfo versionInfo) throws Exception;	
	/**
	 * 查询手机版本信息
	 * @param uuId
	 * @return
	 */
	public VersionInfo findVersionInfo(String mk, String appID) throws Exception;
	
	
}
