package com.qq.qqbuy.version.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.qq.qqbuy.version.dao.VersionInfoDao;
import com.qq.qqbuy.version.po.VersionInfo;

public class VersionInfoDaoImpl implements VersionInfoDao {

	private JdbcTemplate jdbcTemplate;

	@Override
	public int insertVersionInfo(VersionInfo versionInfo) throws Exception {
		// TODO Auto-generated method stub
		/*return jdbcTemplate
				.update("insert into versionInfo (adid,appkey,appVersion,brand,channel,client,firstAppVersion"
						+ ",imei,insertDateTime,language,model,networkServer,networkType,openUdid,osType,osVersion,screen,status,"
						+ "updateDateTime,updateTime,uuId,wifiMac)",
						new Object[] { versionInfo.getAdid(),
								versionInfo.getAppKey(),
								versionInfo.getAppVersion(),
								versionInfo.getBrand(),
								versionInfo.getChannel(),
								versionInfo.getClient(),
								versionInfo.getFirstAppVersion(),
								versionInfo.getImei(),
								versionInfo.getInsertDateTime(),
								versionInfo.getLanguage(),
								versionInfo.getModel(),
								versionInfo.getNetworkServer(),
								versionInfo.getNetworkType(),
								versionInfo.getOpenUdid(),
								versionInfo.getOsType(),
								versionInfo.getOsVersion(),
								versionInfo.getScreen(),
								versionInfo.getStatus(),
								versionInfo.getUpdateDateTime(),
								versionInfo.getUpdateTime(),
								versionInfo.getUuId(), versionInfo.getWifiMac() });*/
		return -1 ;
	}

	@Override
	public int updateVersionInfo(String uuId, String appKey,
			VersionInfo versionInfo) throws Exception {
		// TODO Auto-generated method stub

		/*return jdbcTemplate
				.update("update versionInfo set adid = ?, appVersion = ?, brand = ?, channel = ?, client = ?, imei = ?, "
						+ "language = ?, model = ?, networkServer = ?, networkType = ?, openUdid = ?, osType = ?, osVersion = ?, screen = ?, "
						+ "updateDateTime = ?, updateTime = ?, wifiMac = ? where appKey = ? and uuId = ?",
						new Object[] { versionInfo.getAdid(),
								versionInfo.getAppVersion(),
								versionInfo.getBrand(),
								versionInfo.getChannel(),
								versionInfo.getClient(), versionInfo.getImei(),
								versionInfo.getLanguage(),
								versionInfo.getModel(),
								versionInfo.getNetworkServer(),
								versionInfo.getNetworkType(),
								versionInfo.getOpenUdid(),
								versionInfo.getOsType(),
								versionInfo.getOsVersion(),
								versionInfo.getScreen(),
								versionInfo.getUpdateDateTime(),
								versionInfo.getUpdateTime(),
								versionInfo.getWifiMac(), appKey, uuId });*/
		return -1 ;
	}

	@Override
	public VersionInfo findVersionInfo(String mk, String appID) throws Exception {
		// TODO Auto-generated method stub
		/*return (VersionInfo) jdbcTemplate
				.queryForObject(
						"select * from versionInfo where uuId=? and appKey = ? and status = 0",
						new Object[] { uuId, appKey },
						new VersionInfoRowMapper());*/
		return null ;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	class VersionInfoRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			VersionInfo versionInfo = new VersionInfo();
			/*versionInfo.setAdid(rs.getString("adid"));
			versionInfo.setAppKey(rs.getString("appKey"));
			versionInfo.setAppVersion(rs.getString("appVersion"));
			versionInfo.setBrand(rs.getString("brand"));
			versionInfo.setChannel(rs.getString("channel"));
			versionInfo.setClient(rs.getString("channel"));
			versionInfo.setFirstAppVersion(rs.getString("firstAppVersion"));
			versionInfo.setId(rs.getLong("id"));
			versionInfo.setImei(rs.getString("imei"));
			versionInfo.setInsertDateTime(rs.getString("insertDateTime"));
			versionInfo.setLanguage(rs.getString("language"));
			versionInfo.setModel(rs.getString("model"));
			versionInfo.setNetworkServer(rs.getString("NetworkServer"));
			versionInfo.setNetworkType(rs.getString("networkType"));
			versionInfo.setOpenUdid(rs.getString("openUdid"));
			versionInfo.setOsType(rs.getString("osType"));
			versionInfo.setOsVersion(rs.getString("osVersion"));
			versionInfo.setScreen(rs.getString("screen"));
			versionInfo.setStatus(rs.getInt("status"));
			versionInfo.setUpdateDateTime(rs.getString("updateDate"));
			versionInfo.setUpdateTime(rs.getString("updateTime"));
			versionInfo.setUuId(rs.getString("uuId"));
			versionInfo.setWifiMac(rs.getString("wifiMac"));*/
			return versionInfo;
		}
	}

}
