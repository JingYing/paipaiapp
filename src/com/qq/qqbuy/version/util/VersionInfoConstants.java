package com.qq.qqbuy.version.util;

public class VersionInfoConstants {

	public static final String REPORT_VERSION_INFO_KEY = "zhy#@$fsf^*&Fejkfhew5631kfUFE&W78*(&^74" ;//上报版本信息key校验
	
	public static final String FIND_VERSION_INFO_KEY = "%23if9*&F(HFKefhiw23423UFGueifhuie2309*@$#Fyefwe" ;//获取版本信息key校验
	
	public static final Long REPORT_SUCCESS = 0l;//上报信息成功
	
	public static final Long REPORT_ERROR = -1l;//上报信息失败

	public static final int CONNECT_ERROR = 3308;//获取数据库连接失败

	public static final int TABLENAME_ERROR = 3309;//获取表名失败

	public static final String REPORT_VERSION_APP_KEY = "^%F&uewfHF8i8y3428FEF(E2u*(^&F5fwu2332dE%^WFYEWY" ;//上报APP新版本key校验

	public static final String FIND_VERSION_APP_KEY = "*&32ufiTY{><ui&t67wfijdsy78#@$fsoiewfjklefoq4fuis" ;//获取最新的app版本信息key校验

}
