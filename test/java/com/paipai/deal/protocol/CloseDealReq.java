 
 
//auto gen by paipai.java.augogen ver 1.0
//auther skyzhuang
//source idl: com.paipai.deal.ShippingFee.java

package com.paipai.deal.protocol;


import java.util.Vector;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.paipai.component.c2cplatform.IServiceObject;
import com.paipai.lang.uint64_t;
import com.paipai.util.io.ByteStream;

/**
 *取消订单请求
 *
 *@date 2012-12-19 12:24:45
 *
 *@since version:0
*/
public class  CloseDealReq implements IServiceObject
{
	/**
	 * 调用者==>请设置为源文件名
	 *
	 * 版本 >= 0
	 */
	 private String Source = new String();

	/**
	 * 订单id
	 *
	 * 版本 >= 0
	 */
	 private String DealId = new String();

	/**
	 * 场景id
	 *
	 * 版本 >= 0
	 */
	 private long SceneId;

	/**
	 * 关闭原因
	 *
	 * 版本 >= 0
	 */
	 private long CloseReason;

	/**
	 * 子单列表
	 *
	 * 版本 >= 0
	 */
	 private Vector<uint64_t> oTradeIdList = new Vector<uint64_t>();

	/**
	 * 用户MachineKey
	 *
	 * 版本 >= 0
	 */
	 private String MachineKey = new String();

	/**
	 * 保留输入字段
	 *
	 * 版本 >= 0
	 */
	 private String ReserveIn = new String();


	public int Serialize(ByteStream bs) throws Exception
	{
		bs.pushString(Source);
		bs.pushString(DealId);
		bs.pushUInt(SceneId);
		bs.pushUInt(CloseReason);
		bs.pushObject(oTradeIdList);
		bs.pushString(MachineKey);
		bs.pushString(ReserveIn);
		return bs.getWrittenLength();
	}
	
	public int UnSerialize(ByteStream bs) throws Exception
	{
		Source = bs.popString();
		DealId = bs.popString();
		SceneId = bs.popUInt();
		CloseReason = bs.popUInt();
		oTradeIdList = (Vector<uint64_t>)bs.popVector(uint64_t.class);
		MachineKey = bs.popString();
		ReserveIn = bs.popString();
		return bs.getReadLength();
	}

	public long getCmdId()
	{
		return 0x26301804L;
	}


	/**
	 * 获取调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @return Source value 类型为:String
	 * 
	 */
	public String getSource()
	{
		return Source;
	}


	/**
	 * 设置调用者==>请设置为源文件名
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setSource(String value)
	{
		this.Source = value;
	}


	/**
	 * 获取订单id
	 * 
	 * 此字段的版本 >= 0
	 * @return DealId value 类型为:String
	 * 
	 */
	public String getDealId()
	{
		return DealId;
	}


	/**
	 * 设置订单id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setDealId(String value)
	{
		this.DealId = value;
	}


	/**
	 * 获取场景id
	 * 
	 * 此字段的版本 >= 0
	 * @return SceneId value 类型为:long
	 * 
	 */
	public long getSceneId()
	{
		return SceneId;
	}


	/**
	 * 设置场景id
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setSceneId(long value)
	{
		this.SceneId = value;
	}


	/**
	 * 获取关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @return CloseReason value 类型为:long
	 * 
	 */
	public long getCloseReason()
	{
		return CloseReason;
	}


	/**
	 * 设置关闭原因
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:long
	 * 
	 */
	public void setCloseReason(long value)
	{
		this.CloseReason = value;
	}


	/**
	 * 获取子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @return oTradeIdList value 类型为:Vector<uint64_t>
	 * 
	 */
	public Vector<uint64_t> getOTradeIdList()
	{
		return oTradeIdList;
	}


	/**
	 * 设置子单列表
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:Vector<uint64_t>
	 * 
	 */
	public void setOTradeIdList(Vector<uint64_t> value)
	{
		if (value != null) {
				this.oTradeIdList = value;
		}else{
				this.oTradeIdList = new Vector<uint64_t>();
		}
	}


	/**
	 * 获取用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @return MachineKey value 类型为:String
	 * 
	 */
	public String getMachineKey()
	{
		return MachineKey;
	}


	/**
	 * 设置用户MachineKey
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setMachineKey(String value)
	{
		this.MachineKey = value;
	}


	/**
	 * 获取保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @return ReserveIn value 类型为:String
	 * 
	 */
	public String getReserveIn()
	{
		return ReserveIn;
	}


	/**
	 * 设置保留输入字段
	 * 
	 * 此字段的版本 >= 0
	 * @param  value 类型为:String
	 * 
	 */
	public void setReserveIn(String value)
	{
		this.ReserveIn = value;
	}


	protected int getClassSize()
	{
		return  getSize() - 4;
	}

	
	public int getSize()
	{
		int length = 0;
		try{
				length = 0;  //size_of(CloseDealReq)
				length += ByteStream.getObjectSize(Source);  //计算字段Source的长度 size_of(String)
				length += ByteStream.getObjectSize(DealId);  //计算字段DealId的长度 size_of(String)
				length += 4;  //计算字段SceneId的长度 size_of(uint32_t)
				length += 4;  //计算字段CloseReason的长度 size_of(uint32_t)
				length += ByteStream.getObjectSize(oTradeIdList);  //计算字段oTradeIdList的长度 size_of(Vector)
				length += ByteStream.getObjectSize(MachineKey);  //计算字段MachineKey的长度 size_of(String)
				length += ByteStream.getObjectSize(ReserveIn);  //计算字段ReserveIn的长度 size_of(String)
		}catch(Exception e){
				e.printStackTrace();
		}
		return length;
	}

	/**
	 *下面是生成toString()方法
	 此方法用于调试时打开*
	 *如果要打开此方法，请加入commons-lang-2.4.jar
	 *并导入 import org.apache.commons.lang.builder.ToStringBuilder;
	 *      import org.apache.commons.lang.builder.ToStringStyle;
	 *
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
