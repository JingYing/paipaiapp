//package com.qq.qqbuy.thirdparty.deal;
//
//
//import java.util.Map;
//import java.util.Vector;
//
//import net.sf.json.JSONSerializer;
//import net.sf.json.JsonConfig;
//
//import com.paipai.component.c2cplatform.impl.AsynWebStub;
//import com.paipai.component.configagent.Configs;
//import com.paipai.deal.protocol.GetTemplateDetailReq;
//import com.paipai.deal.protocol.GetTemplateDetailResp;
//import com.qq.qqbuy.common.po.QQBuyServiceComm;
//import com.qq.qqbuy.deal.biz.QueryDealBiz;
//import com.qq.qqbuy.deal.biz.impl.QueryDealBizImpl;
//import com.qq.qqbuy.thirdparty.idl.deal.DealClient;
//import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinReq;
//import com.qq.qqbuy.thirdparty.idl.deal.protocol.GetAllStateDealCountByUinResp;
//import com.qq.qqbuy.thirdparty.idl.deelpc.client.PcDealQueryClient;
//import com.qq.qqbuy.thirdparty.idl.deelpc.protocol.CDealInfo;
//import com.qq.qqbuy.thirdparty.idl.deelpc.protocol.GetDealInfoList2Resp;
//import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginReq;
//import com.qq.qqbuy.thirdparty.idl.verifycode.protocol.CheckLoginResp;
//import com.qq.qqbuy.thirdparty.openapi.OpenApi;
//import com.qq.qqbuy.thirdparty.openapi.OpenApiProxy;
//import com.qq.qqbuy.thirdparty.openapi.po.BuyerCancelDealRequest;
//import com.qq.qqbuy.thirdparty.openapi.po.BuyerCancelDealResponse;
//
//public class DealQueryTest
//{
//    public static void main(String[] args) throws Exception
//    {
////        getMyOrderSummary(313946562);
////        
////        getDealList(313946562,"",1,5);
////    	  getDealList(525711678L, "DS_DEAL_END_NORMAL", 1, 5);
//    	  getDealList(525711678L, "DS_WAIT_BUYER_RECEIVE", 100, 1, 20);
//    	
////    	  getDealList(41270865, 42, 1, 10);
//    	  
////    	  getDealStatesList(525711678);
////    	  getDealStatesList(525711678, 0, 0);
////        getDealStatesList(525711678, 1);
//////        
////        getDealInfo(525711678,779000123,"779000123-20130716-1116786210",1);
////        System.out.println(URLEncoder.encode("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201&#61;&#61;".replace("&#61;", "="),"utf-8"));
////        System.out.println(URLDecoder.decode("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201==","utf-8"));
//////        checkLogin("oloche+rXW2+LxjrcQIlz9767LTLt8uk15c3d3d90201==","170.2.23.32");
////        String mk = "11345678912345678";
////        String sk = "@"
////            + MD5Coding.encode2HexStr(("518800364" + mk).getBytes())
////            .toLowerCase().substring(0, 8) + "@";
////        getShippingFee(518800364L,(short)10,sk,mk);
////        System.out.println(mk);
//        
//        
//        //测试关闭订单
////        cancelDeal(313946562L,"1799541383-20121213-978552218",8);
//    }
//    
//
//    public static void cancelDeal(long buyerUin,String dealCode, long closeReason)
//    {
//        OpenApiProxy proxy = null;
//        proxy = OpenApi.getProxy();
//        BuyerCancelDealRequest req = new BuyerCancelDealRequest();
//        req.setBuyerUin(buyerUin);
//        req.setDealCode(dealCode);
//        req.setCloseReason(closeReason);
//        BuyerCancelDealResponse res = proxy.buyerCancelDeal(req);
//        if (res != null && res.isSucceed())
//        {
//           System.out.println("buyerUin:" + buyerUin + " dealCode:" + dealCode + " closeReason:" + closeReason  + res);
//        }
//        else
//            System.out.println("error buyerUin:" + buyerUin + " dealCode:" + dealCode + " closeReason:" + closeReason  + " errCode:" + res.getErrorCode() + " errMsg:" +res.getErrorMessage());
//    }
//    
//    /**
//     * 
//     * @Title: getShippingFee 
//     * 
//     * @Description: 测试运费模板的 
//     * @param uin
//     * @param innerId
//     * @param skey
//     * @param mk    设定文件 
//     * @return void    返回类型 
//     * @throws
//     */
//    public static void getShippingFee(long uin,short innerId, String skey,String mk)
//    {
//        GetTemplateDetailReq req = new GetTemplateDetailReq();
//        req.setUin(uin);
//        req.setCInnerId(innerId);
//        GetTemplateDetailResp resp = new GetTemplateDetailResp();
//        resp.setResult(-7);
//        
//        AsynWebStub webStub = new AsynWebStub();
//        webStub.setUin(50888394); // 应为setUin被服务端用来做负载均衡，故需要设置。
//        webStub.setOperator(50888394);
//        webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
//        webStub.setSkey("@LuYxIIAgi".getBytes());
//        webStub.setMachineKey(mk.getBytes());
//        try {
//            int ret = webStub.invoke(req, resp);
//            System.out.println("uin:" + uin + " innerId:" + innerId + " skey:" + skey + " mk:" + mk 
//                    + "\r\nresp:" + resp);
//        } catch (Exception e) {
//            System.out.println("resp:" + resp);
//        }
//    }
//
//    
////    /**
////     * 
////     * @Title: getMyOrderSummary 
////     * @Description: 获取订单状态数量
////     * @param uin    设定文件 
////     * @return void    返回类型 
////     * @throws
////     */
////    public static void getMyOrderSummary(long uin)
////    {
////        QueryDealBiz biz = new QueryDealBizImpl();
////        JsonConfig jsoncfg = new JsonConfig();
////        String[] excludes =
////        { "size", "empty" };
////        jsoncfg.setExcludes(excludes);
////        String datastr = JSONSerializer.toJSON(biz.queryDealStatesList(uin, null, null),jsoncfg).toString();
////        System.out.println("stats:" + datastr);
////    }
////    
////    
////    /**
////     * 
////     * @Title: getDealList 
////     * @Description: 测试订单列表
////     * @param uin
////     * @param dealType    设定文件 
////     * @return void    返回类型 
////     * @throws
////     */
////    public static void getDealList(long uin,String dealType,  int pageNo,int pageSize)
////    {
////        QueryDealBiz biz = new QueryDealBizImpl();
////        JsonConfig jsoncfg = new JsonConfig();
////        // String[] excludes = { "size", "empty" };
////        String[] excludes =
////        { "size", "empty" };
////        jsoncfg.setExcludes(excludes);
////        String datastr = JSONSerializer.toJSON(biz.queryDealList(uin, null, null, dealType, pageNo, pageSize,"1"), jsoncfg).toString();
////        System.out.println("getDealList:" + datastr);
////        
////    }
//    
////    public static void getDealInfo(long sellerUin,String dealCode,long listItem)
////    {
////        QueryDealBiz biz = new QueryDealBizImpl();
////        JsonConfig jsoncfg = new JsonConfig();
////        String[] excludes =
////        { "size", "empty" };
////        jsoncfg.setExcludes(excludes);
////        String datastr = JSONSerializer.toJSON(biz.dealDetail(sellerUin, dealCode, listItem, "1"),jsoncfg).toString();
////        System.out.println("getDealInfo:" + datastr);
////    }
//    
//    /**
//     * 
//     * @Title: getDealList 
//     * @Description: 测试订单列表
//     * @param uin
//     * @param dealType    设定文件 
//     * @return void    返回类型 
//     * @throws
//     */
//    public static void getDealList(long uin, int dealState, int pageNo, int pageSize)
//    {
////        QueryDealBiz biz = new QueryDealBizImpl();
////        JsonConfig jsoncfg = new JsonConfig();
////        String[] excludes = { "size", "empty" };
////        jsoncfg.setExcludes(excludes);
////        String datastr = JSONSerializer.toJSON(biz.queryDealList(uin, null, null, dealType, pageNo, pageSize,"1",null,null), jsoncfg).toString();
////        System.out.println("getDealList:" + datastr);
//    	
//    	GetDealInfoList2Resp resp = PcDealQueryClient.getDealList(uin, dealState, pageNo, pageSize);
//    	System.out.println("totalNum:" + resp.getTotalNum());
//    	Vector<CDealInfo> vecDealInfoList = resp.getODealInfoList();
//    	for (CDealInfo cDealInfo : vecDealInfoList) {
//			System.out.println(cDealInfo);
//		}
//        
//    }
//    
//    public static void getDealList(long uin, String dealState, int rateState, int pageNo, int pageSize)
//    {
//        QueryDealBiz biz = new QueryDealBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes = { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(biz.queryDealList(uin, dealState, rateState,
//        		pageNo, pageSize, "1"), jsoncfg).toString();
//        System.out.println("getDealList:" + datastr);
//    	
////    	GetDealInfoList2Resp resp = PcDealQueryClient.getDealList(uin, dealState, rateState, pageNo, pageSize);
////    	System.out.println("totalNum:" + resp.getTotalNum());
////    	Vector<CDealInfo> vecDealInfoList = resp.getODealInfoList();
////    	for (CDealInfo cDealInfo : vecDealInfoList) {
////			System.out.println(cDealInfo);
////		}
//        
//    }
//    
//    public static void getDealStatesList(long uin, int ver) {
//    	QueryDealBiz biz = new QueryDealBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes = { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(biz.queryDealStatesList(uin, 1), jsoncfg).toString();
//        System.out.println("dealStatesList:" + datastr);
//    }
//    
//    public static void getDealStatesList(long uin, int dealState, int rateState) {
//    	GetDealInfoList2Resp resp = PcDealQueryClient.getDealStateNum(uin, dealState, rateState);
//    	System.out.println("totalNum:" + resp.getTotalNum());
//    	Vector<CDealInfo> vecDealInfoList = resp.getODealInfoList();
//    	System.out.println("totalNum return:" + vecDealInfoList.size());
//    	for (CDealInfo cDealInfo : vecDealInfoList) {
//			System.out.println(cDealInfo);
//		}
//    }
//    
//    public static void getDealStatesList(long uin) {
//    	GetAllStateDealCountByUinReq allStateReq = new GetAllStateDealCountByUinReq();
//        allStateReq.setUin(uin);
//        allStateReq.setDealType(1);
//        GetAllStateDealCountByUinResp resp = DealClient
//                .getDealStateNum(allStateReq);
//        if (resp != null && resp.getErrCode() == 0) {
//        	Map<Integer, Integer> mp = resp.getPaipaiDealCount();
//		}
//    }
//    
//    public static void getDealInfo(long buyerUin, long sellerUin, String dealCode, long listItem)
//    {
//        QueryDealBiz biz = new QueryDealBizImpl();
//        JsonConfig jsoncfg = new JsonConfig();
//        String[] excludes =
//        { "size", "empty" };
//        jsoncfg.setExcludes(excludes);
//        String datastr = JSONSerializer.toJSON(biz.dealDetail(buyerUin, sellerUin, dealCode, listItem, "1"),jsoncfg).toString();
//        System.out.println("getDealInfo:" + datastr);
//    }
//    
//    public static void checkLogin(String sid,String clientIp)
//    {
//        CheckLoginReq req = new CheckLoginReq();
//        QQBuyServiceComm serviceCommBean = new QQBuyServiceComm();
//        serviceCommBean.setSid(sid);
//        serviceCommBean.setIp(clientIp);
//        serviceCommBean.setStatSid("");
//        serviceCommBean.setSidFromCookie("");
//        serviceCommBean.setSidFromQCookie("");
//        req.setCommParm(serviceCommBean);
//        CheckLoginResp resp = new CheckLoginResp();
//        resp.setResult(-7);
//        
//        AsynWebStub webStub = new AsynWebStub();
//        webStub.setUin(System.currentTimeMillis()); // 应为setUin被服务端用来做负载均衡，故需要设置。
//        webStub.setConfigType(Configs.PAIPAI_CONFIG_TYPE);
//        webStub.setSvc("qgo.sys.loginsvc");
//        try {
//            int ret = webStub.invoke(req, resp);
//            System.out.println("sid:" + sid + " resp:" + resp);
//        } catch (Exception e) {
//            System.out.println("resp:" + resp);
//        }
//    }
//
//}