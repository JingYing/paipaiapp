package com.qq.qqbuy.thirdparty.deal;

import com.paipai.lang.uint32_t;
import com.qq.qqbuy.common.util.DateUtils;
import com.qq.qqbuy.thirdparty.idl.boss.protocol.CSelfMarket;
import com.qq.qqbuy.thirdparty.idl.boss.protocol.CSelfMarketQueryValidReq;
import com.qq.qqbuy.thirdparty.idl.boss.protocol.CSelfMarketQueryValidResp;

public class TestManLiJian
{
    public static void main(String[] args) throws Exception
    {
        System.out.println(DateUtils.formatToStringYMD(System.currentTimeMillis()));
        CSelfMarket oSelfMarket = new CSelfMarket();
        uint32_t uin = new uint32_t(382232782);
        int iRet = 0;

        // 查询当前有效活动
        CSelfMarketQueryValidReq oSelfMarketQueryValidReq = new CSelfMarketQueryValidReq();
        oSelfMarketQueryValidReq.setUin(uin);

        CSelfMarketQueryValidResp oSelfMarketQueryValidResp = new CSelfMarketQueryValidResp();

        iRet = oSelfMarket.QueryValid(oSelfMarketQueryValidReq,
                oSelfMarketQueryValidResp);
        System.out.println("QueryValid ret:" + iRet);
        if (iRet == 0)
        {
            System.out.println("oSelfMarketQueryValidResp.getUin():"
                    + oSelfMarketQueryValidResp.getUin());
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsActiveDesc():"
                            + oSelfMarketQueryValidResp
                                    .getoCBoSlfMktMljActive().getsActiveDesc());
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwActiveID():"
                            + oSelfMarketQueryValidResp
                                    .getoCBoSlfMktMljActive().getDwActiveID());
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getsNickName():"
                            + oSelfMarketQueryValidResp
                                    .getoCBoSlfMktMljActive().getsNickName());
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwBeginTime(:"
                            + oSelfMarketQueryValidResp
                                    .getoCBoSlfMktMljActive().getDwBeginTime());
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getDwUin():"
                            + oSelfMarketQueryValidResp
                                    .getoCBoSlfMktMljActive().getDwUin());
            int size = oSelfMarketQueryValidResp.getoCBoSlfMktMljActive()
                    .getvContents().size();
            System.out
                    .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().size(:"
                            + size);
            if (size > 0)
            {
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwActiveID():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0).getDwActiveID());
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwContentID():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0).getDwContentID());
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostFlag():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0).getDwCostFlag());
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwCostMoney():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0).getDwCostMoney());
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwUin():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0).getDwUin());
                System.out
                        .println("oSelfMarketQueryValidResp.getoCBoSlfMktMljActive().getvContents().get(0).getDwFavorableFlag():"
                                + oSelfMarketQueryValidResp
                                        .getoCBoSlfMktMljActive()
                                        .getvContents().get(0)
                                        .getDwFavorableFlag());

            }

        } else
        {
            if (iRet > 0)
            {
                System.out.println("No Valid Acitive");
            } else
            {
                System.out.println("QueryValid Error");
            }
        }
    }
}
