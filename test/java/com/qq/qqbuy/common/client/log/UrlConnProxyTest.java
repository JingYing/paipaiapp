package com.qq.qqbuy.common.client.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.qq.qqbuy.common.client.log.UrlConnProxy;

public class UrlConnProxyTest {

	public static void main(String[] args) throws IOException {
		// HttpURLConnection c = (HttpURLConnection)new
		// URL("http://127.0.0.1:8080/testweb/servlet/MyServlet").openConnection();
		HttpURLConnection c = (HttpURLConnection) new URL(
				"http://sse1.paipai.com/comm_json?dtype=json&charset=utf-8&KeyWord=%E4%BA%94%E9%87%91&shop=329786348")
				.openConnection();
		InputStream is = new UrlConnProxy(c, true, "utf-8").getInputStream();
		// InputStream is = c.getInputStream();
		System.out.println("available:" + is.available());
		System.out.println("cont-len:" + c.getContentLength());
		long s = System.currentTimeMillis();

		// BufferedInputStream bis = new BufferedInputStream(is);
		// long total = 0;
		// byte[] buf = new byte[2048];
		// int len;
		// while((len = bis.read(buf)) != -1)
		// total += len;
		// System.out.println("消息体:" + total);
		// long e = System.currentTimeMillis()-s;
		// System.out.println((System.currentTimeMillis()-s));
		// System.out.println(e);

		BufferedReader br = new BufferedReader(new InputStreamReader(is,
				"utf-8"));
		String line;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null)
			sb.append(line);

		System.out.println(sb.toString());

	}

}
