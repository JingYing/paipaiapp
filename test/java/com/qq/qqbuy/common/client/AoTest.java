package com.qq.qqbuy.common.client;

import java.io.IOException;

import org.junit.Test;
import org.springframework.aop.framework.ProxyFactory;

import com.paipai.component.c2cplatform.AsynWebStubException;
import com.paipai.component.c2cplatform.impl.AsynWebStub;
import com.qq.qqbuy.common.client.http.HttpClient;
import com.qq.qqbuy.common.client.http.HttpResp;
import com.qq.qqbuy.common.client.log.Utf8RespLogParser;
import com.qq.qqbuy.common.client.udp.UdpClient;
import com.qq.qqbuy.thirdparty.idl.WebStubFactory;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserLinkInfoReq;
import com.qq.qqbuy.thirdparty.idl.userinfo.protocol.GetUserLinkInfoResp;

public class AoTest {
	
	@Test
	public void test1()	{
		
		GetUserLinkInfoReq req = new GetUserLinkInfoReq();
		GetUserLinkInfoResp resp = new GetUserLinkInfoResp();
		req.setUin(157676424);
		req.setReqnum(3);
		
		AsynWebStub stub = WebStubFactory.getWebStub4PaiPaiGBK();
		try {
			stub.invoke(req, resp);
		} catch (AsynWebStubException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void test2()	{
		GetUserLinkInfoReq or = new GetUserLinkInfoReq();
		or.setReqnum(1111);
		ProxyFactory f = new ProxyFactory();
//		f.setTargetClass(GetUserLinkInfoReq.class);
//		f.setProxyTargetClass(true);
		GetUserLinkInfoReq req = (GetUserLinkInfoReq)f.getProxy();
		System.out.println(req);
	}
	
	@Test
	public void test3() throws IOException	{
		HttpClient c = new HttpClient();
		HttpResp resp = c.get("http://www.baidu.com/s?wd=1&ie=UTF-8", null, new Utf8RespLogParser());
		resp = c.get("http://www.baidu.com/s?wd=1&ie=UTF-8", null, new Utf8RespLogParser());
		System.out.println(new String(resp.getResponse()));
	}
	
	@Test
	public void test4() throws IOException	{
		new UdpClient().sendAndReceive("127.0.0.1:8080", "dddddddddd".getBytes());
	}

}
