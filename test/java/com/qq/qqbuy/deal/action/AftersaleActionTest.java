package com.qq.qqbuy.deal.action;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.junit.Test;

import com.opensymphony.xwork2.ActionProxy;
import com.qq.qqbuy.BaseActionTest;

public class AftersaleActionTest extends BaseActionTest{
	
	@Test
	public void testMakeEval() throws Exception	{
		String c = "mk2=865687024204800-0c1dafdb9b6d&longitude=116.421614&latitude=39.862556&mt=android&dsr=5,5,5"
				+ "&info=[{\"c\":\"%E5%AE%B9%E6%98%93\",\"l\":\"3\",\"t\":\"1324891569\"}]&dealId=795019790-20141118-1324891569"
				+ "&versionCode=303&mk=cedd374e668e6373415d825829680964bc3531d1&appID=1&"
				+ "channel=%7B%22market%22%3A%22Channel%22%2C%22pprd_p%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22wid%22%3A%22503683439%22%2C%22gdt_vid%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%7D&pinid=";
		request.setContent(c.getBytes());
		ActionProxy proxy = getActionProxy("/api/aftersale/makeEval.xhtml");
		proxy.execute();
	}
	
	@Test
	public void testMakeEval2() throws Exception	{
		
		request.setParameter("dsr", "5,5,5");
		request.setParameter("info", "[{\"c\":\"%E5%AE%B9%E6%98%93\",\"l\":\"3\",\"t\":\"1324891569\"}]");
		request.setParameter("dealId", "795019790-20141118-1324891569");
		request.setParameter("mk", "cedd374e668e6373415d825829680964bc3531d1");
		ActionProxy proxy = getActionProxy("/api/aftersale/makeEval.xhtml");
		proxy.execute();
	}
	
	@Test
	public void testRecvItem() throws Exception	{
		request.setParameter("token", "fehbGClwoTaTtPOCDbLOAyJDtWh7U4a-HFIbWwaW8A34NW__eA1Lx33SKku6gziT5Z0i6hIEjcEltgZnFXfpeKpvaJoBHjPiOghmNM15rMMQXuSWR5tDHFwh0gPIAUsGdyFIEKRmdSQnZ8tP-z8eyqZL51QfRk4iFLQIJZ-fH7k8JB3rNpqFA2ke6eHYkWk3SRLXW-_b-De1iNevKJ_XWFL4qFzq3QqiW50KW8C0SuDg0FTI4Vo6NUJ4Rr9FZudOuC3zqB7WQMxU8uKK1RiWB-lRZSvwS3s6hSocPSmlMZnr3iqL67GPFyIvCj3NNINB0XLgMaPFd393HNbd4FiXSt89kLe-9Eesfr_P2sdjdboVyb_CPH2T-PsRXOFCPSorvUxjiTmWc3mUr41EMu4KlWwGwyT4HmUdMPv-76_i2yrXEJ1oh0QDeg==");
		request.setParameter("mk", "cedd374e668e6373415d825829680964bc3531d1");
		request.setParameter("dealId", "1275000334-20141205-1330287712");
		request.setParameter("tradeIds", "1330287713,1330287714");
		request.setParameter("appToken", encode("wtOwLs3k5d2DQx2cKAvk73xzr5W01u47ITlVH%2FsOReliKOCrWO2MBjUvUpgrCSGXUBXjA%2FKQOPReK1Ec4Q5c2EatuG5QacEl6OhnFO0Vp0tAk%2BlhSDbCiDeqYsUQDSh0Q6w2C%2BXHt4Cj%2FnWZGFKEwzcdcOdHtgQVdFrJaUGQZVsyZYiKOFzdlk532HkAWmeB0CYukRSFXRk9e%2F5wKVky4Nb2PiKaPEyKQucTl5HIDVkmJwRCajc%2FpLVH5V7uXtDJ1JxudbngrGFODIoirUhPtGuqMY4RAqbcqECve04unhDEM%3D"));
		request.setParameter("pt", "0");
		
		ActionProxy proxy = getActionProxy("/api/aftersale/recvItem.xhtml");
		proxy.execute();
	}
	
	private String encode(String s)	{
		try {
			return URLEncoder.encode(s, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return s;
		}
	}
	
	@Test
	public void testMakeEval3() throws Exception {
		String url = "http://localhost:8080/webapp_paipai/api/aftersale/makeEval.xhtml";
		HttpURLConnection c = (HttpURLConnection)new URL(url).openConnection();
		c.setDoOutput(true);
		c.setRequestMethod("POST");
		PrintWriter pw = new PrintWriter(c.getOutputStream());
		String cont = "mk2=865687024204800-0c1dafdb9b6d&longitude=116.549634&latitude=39.79987&mt=android&dsr=5,5,5&info=%5B%7B%22c%22%3A%22%E8%B7%AF%22%2C%22l%22%3A%222%22%2C%22t%22%3A%221324891569%22%7D%5D&dealId=795019790-20141118-1324891569&versionCode=303&mk=cedd374e668e6373415d825829680964bc3531d1&appID=1&channel=%7B%22gdt_vid%22%3A%22%22%2C%22jd_pop%22%3A%22%22%2C%22market%22%3A%22Channel%22%2C%22pprd_p%22%3A%22%22%2C%22pps%22%3A%22%22%2C%22qz_express%22%3A%22%22%2C%22qz_gdt%22%3A%22%22%2C%22wid%22%3A%22503683439%22%7D&pinid=";
		pw.print(cont);
		pw.flush();
		pw.close();
		
		c.getInputStream();
	}
	
	@Test
	public void testqueryRefund() throws Exception	{
		request.setParameter("dealId", "795019790-20150108-1338930320");
		request.setParameter("tradeId", "1338930320");
		ActionProxy proxy = getActionProxy("/api/aftersale/queryRefund.xhtml");
		proxy.execute();
		System.out.println(response.getContentAsString());
	}

}
