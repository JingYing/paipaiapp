package com.qq.qqbuy;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(locations={
	"classpath*:conf-spring/biz-context-bi.xml",
	"classpath*:conf-spring/biz-context-cfg.xml",
	"classpath*:conf-spring/biz-context-cmdy.xml",
	"classpath*:conf-spring/biz-context-deal.xml",
	"classpath*:conf-spring/biz-context-discover.xml",
	"classpath*:conf-spring/biz-context-favorite.xml",
	"classpath*:conf-spring/biz-context-gchoice.xml",
	"classpath*:conf-spring/biz-context-item.xml",
	"classpath*:conf-spring/biz-context-login.xml",
	"classpath*:conf-spring/biz-context-my.xml",
	"classpath*:conf-spring/biz-context-mysql.xml",
	"classpath*:conf-spring/biz-context-recvaddr.xml",
	"classpath*:conf-spring/biz-context-search.xml",
	"classpath*:conf-spring/biz-context-sendticket.xml",
	"classpath*:conf-spring/biz-context-shippingfee.xml",
	"classpath*:conf-spring/biz-context-shop.xml",
	"classpath*:conf-spring/biz-context-shoppingaround.xml",
	"classpath*:conf-spring/biz-context-tenpay.xml",
	"classpath*:conf-spring/biz-context-user.xml",
	"classpath*:conf-spring/biz-context-vb2c.xml",
	"classpath*:conf-spring/biz-context-verifycode.xml",
	"classpath*:conf-spring/biz-context-versionInfo.xml",
	"classpath*:conf-spring/biz-context-virtual.xml",
	"classpath*:conf-spring/biz-context-virtualsaf.xml",
	"classpath*:conf-spring/biz-context-wuliu.xml",
	"classpath*:conf-spring/biz-context-wxdlbsstore.xml"
})
public class BaseTest extends AbstractJUnit4SpringContextTests{


}