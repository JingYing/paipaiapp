<%@ page contentType="text/html; charset=utf-8" language="java"%>

<div class="header">
    <div id="mlogo" class="qb_bg_gray2 qb_pf">
    <c:choose>
			<c:when test="${isLogin > 0}">
				<a href="<%=basePath%>/w/my/my.xhtml?sid=${sid}&amp;icfa=10120668&amp;${baseParamWithOutIcfa}">
					${nickNameStr}
				</a>
                 -
				<a href="${logoutURL}">
					退出
				</a>
			</c:when>
			<c:otherwise>
				<a href="${loginURL}">登录</a>
			</c:otherwise>
	</c:choose>
     
     <span class="qb_c_gray">|</span>
      <a href="<%=basePath%>/w/dealopr/listQuery.xhtml?sid=${sid}&amp;icfa=10120292&amp;${baseParamWithOutIcfa}">查物流</a> <span class="qb_c_gray">|</span> 
      <a href="<%=basePath%>/w/template.xhtml?tid=itemClass&amp;sid=${sid}&amp;icfa=10120293&amp;${baseParamWithOutIcfa}">搜商品</a>
      </div>
    <div class="qb_pl10 qb_bg_white"><a href="<%=basePath%>?sid=${sid}&amp;icfa=10120294&amp;${baseParamWithOutIcfa}"><img src="<%=wimgPath%>/logo.png" alt=""></a></div>
</div>