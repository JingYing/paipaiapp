<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%@ include file="/include/head.jsp" %>
	<title>我的拍拍</title>
	<style type="text/css">
	</style>
</head>
<body>
<!-- logo区域 -->
<%@ include file="/include/logo.jsp" %>
<div class="mod_tab qb_header_line">
    我的拍拍
</div>
<table>
    <tr>
        <td class="qb_p10"><img src="${userInfoPo.picURL}" width="50px" height="50px"/></td>
        <td class="qb_p10">${userInfoPo.greetingWord}
            <div></div>
            <c:if test="${userInfoPo.level gt 0}">
            	<div><img src="${userInfoPo.levelPicURL}"/> <span class="qb_c_gray">${userInfoPo.levelName}</span></div>
        	</c:if>
        </td>
    </tr>
</table>
<c:if test="${dealSummaryPo.waitBuyerConfirmCount gt 0 or messageCount.status_unread gt 0 or dealSummaryPo.notCompleteCount gt 0}">
<div class="qb_pf qb_bg_yellow">
    <table>    
     	<c:if test="${dealSummaryPo.waitBuyerConfirmCount gt 0}">	  
	        <tr>
	            <td class="qb_lh_b qb_pr10">您有<span class="qb_c_red">${dealSummaryPo.waitBuyerConfirmCount}</span>笔待确认订单</td>
	            <td class="qb_lh_b"><a href="<%=basePath %>/w/dealopr/listQuery.xhtml?sid=${sid}&amp;qtp=1&amp;from=1&amp;${baseParam}">&#160;去确认</a></td>
	        </tr>
	     </c:if>
	    <c:if test="${dealSummaryPo.notCompleteCount gt 0 and dealSummaryPo.notCompleteCount!= dealSummaryPo.waitBuyerConfirmCount}">	
	        <tr>
	            <td class="qb_lh_b qb_pr10">您有<span class="qb_c_red">${dealSummaryPo.notCompleteCount}</span>笔进行中订单</td>
	            <td class="qb_lh_b"><a href="<%=basePath %>/w/dealopr/listQuery.xhtml?sid=${sid}&amp;qtp=3&amp;from=1&amp;${baseParam}">&#160;去查看</a></td>
	        </tr>
	    </c:if>
	    <c:if test="${messageCount.status_unread gt 0}">	
	        <tr>
	            <td class="qb_lh_b qb_pr10">您有<span class="qb_c_red">${messageCount.status_unread}</span>条未查看信息</td>
	            <td class="qb_lh_b"><a href="<%=basePath %>/w/msg/list.xhtml?sid=${sid}&amp;st=1&amp;${baseParam}">&#160;去查看</a></td>
	        </tr>
	    </c:if>
    </table>
</div>
</c:if>

<div class="qb_pf qb_bg_gray qb_mb10">
    <table>
        <tr>
            <td class="qb_lh_b"><a href="<%=basePath %>/w/dealopr/listQuery.xhtml?sid=${sid}&amp;from=1&amp;${baseParam}">我的订单</a></td>
            <td class="qb_lh_b"><a href="<%=basePath %>/w/recvaddr/listRecvAddr.xhtml?sid=${sid}&amp;${baseParam}">&#160;收货地址</a></td>
        </tr>
        <tr> 
           <td class="qb_lh_b"><a href="<%=basePath %>/w/act/listGainAct.xhtml?tp=1&amp;sid=${sid}&amp;${baseParam}">我的活动</a></td>
           <td class="qb_lh_b"><a  href="<%=basePath %>/w/redpacket/list.xhtml?sid=${sid}&amp;tp=100&amp;${baseParam}">&#160;我的红包</a></td>
        </tr>
        <tr> 
           <td class="qb_lh_b"><a href="<%=basePath %>/w/msg/list.xhtml?sid=${sid}&amp;${baseParam}">我的站内信</a></td>
        </tr>
    </table>
</div>
<div class="qb_gap"><div>【更多服务】</div>
    <a href="http://tuan.3g.qq.com/g/s?sid=${sid}&amp;aid=g2_myorders"  class="qb_c_purple">团购</a>
    <a href="http://dianying.3g.qq.com/mycertificate.sc?sid=${sid}&amp;fr=qgo" class="qb_c_yellow">电影票</a>
    <a href="<%=basePath %>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" class="qb_c_cyan">充话费</a>
</div>
<!-- 广告位10 -->
<div>
</div>

<!-- 底部区域 -->
<%@ include file="/include/foot.jsp" %>

</body>
</html>