<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
{ "errCode":"${result.errCode}",
"retCode":"${result.retCode}",
"msg":"${result.msg}", 
"data":{
"totalPrice":${listVo.normalTotalPrice}, "normalItems":[
<c:forEach items='${listVo.normalItems}' var='shop' varStatus='i'>
			 	{
		 		    "sellerUin": "${shop.sellerUin}",
   					"shopName": "${shop.shopName}",
   					"shopType": "${shop.shopType}",
   					"items" : [<c:forEach items='${shop.items}' var='cmdy' varStatus='j'>
			 	{
		 		   "itemTitle": "${cmdy.itemTitle}",
   					"itemCode": "${cmdy.itemCode}",
   					"itemAttr": "${cmdy.itemAttr}",
   					"mostLowPrice":{"priceValue":"${cmdy.mostLowPrice.priceValue}","priceType":"${cmdy.mostLowPrice.priceType}","priceTypeDesc":"${cmdy.mostLowPrice.priceTypeDesc}"},
					"buyNum": "${cmdy.buyNum}",
   					"mainLogoUrl": "${cmdy.mainLogoUrl}",
   					"maxNum": "${cmdy.maxNum}",
   					"supportCod": "${cmdy.supportCod}",
   					"totalPrice": "${cmdy.totalPrice}"
			 	}
			 	<c:if test='${j.last != true}'>,</c:if>
</c:forEach>]
			 	}
			 	<c:if test='${i.last != true}'>,</c:if>
</c:forEach>
], "problemItems":[
<c:forEach items='${listVo.problemItems}' var='shop' varStatus='i'>
			 	{
		 		    "sellerUin": "${shop.sellerUin}",
   					"shopName": "${shop.shopName}",
   					"shopType": "${shop.shopType}",
   					"items" : [<c:forEach items='${shop.items}' var='cmdy' varStatus='j'>
			 	{
		 		   "itemTitle": "${cmdy.itemTitle}",
   					"itemCode": "${cmdy.itemCode}",
   					"itemAttr": "${cmdy.itemAttr}",
   					"mostLowPrice":{"priceValue":"${cmdy.mostLowPrice.priceValue}","priceType":"${cmdy.mostLowPrice.priceType}","priceTypeDesc":"${cmdy.mostLowPrice.priceTypeDesc}"},
					"buyNum": "${cmdy.buyNum}",
   					"mainLogoUrl": "${cmdy.mainLogoUrl}",
   					"maxNum": "${cmdy.maxNum}",
   					"supportCod": "${cmdy.supportCod}",
   					"totalPrice": "${cmdy.totalPrice}"
			 	}
			 	<c:if test='${j.last != true}'>,</c:if>
</c:forEach>]
			 	}
			 	<c:if test='${i.last != true}'>,</c:if>
</c:forEach>
] } }
