<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>
</head>
  
<body>
<!-- 页面顶部下载客户端链接 -->
<div>
	<q:advertpos advertPosId="66" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/>
</div> 
<%@ include file="/include/logo.jsp" %>     
<!-- 导航区1 -->
<div class="div4"></div>
<div class="qb_bg_red qb_mb10 qb_pl10">
 <table class="qb_tb">
        <tr>
            <td class="mod_tab qb_bg_white qb_c_black">话费</td>
            <!-- 
            <td class="mod_tab "><a href="<%=basePath%>/w/vb2c/buyGame.xhtml?sid=${sid}&amp;${baseParam}" title="-网游">网游</a> </td>
            <td class="mod_tab "><a href="<%=basePath%>${itemNavigation.picUrlNoIndex}&amp;${baseParam}" title="-QQ服务">QQ服务</a> </td>          
         -->
        </tr>
  </table>
</div>
<!--  
<div class="tabbox">
<p class="tab">
<span class="cur">话费&nbsp;</span>
<a href="<%=basePath%>/w/vb2c/buyGame.xhtml?sid=${sid}&amp;${baseParam}" title="-网游">网游&nbsp;</a>
<a href="<%=basePath%>/w/vb2c/buyQQService.xhtml?sid=${sid}&amp;${baseParam}" title="-QQ服务">QQ服务&nbsp;</a>
</p>
</div>
-->

<!-- 充值区域 -->
<div class="vb2cbox">
<c:if test="${result.errCode ne 0}">

<!-- 错误信息区域 -->
<div class="errbox">
<p class="errinfo"><img class="pic" alt="出现错误" src="<%=wimgPath%>/icon1_warn.png" />${result.msg}</p>
</div>
</c:if>
<form id="mobileChargeForm1" method="post" action="<%=basePath%>/w/vb2c/buyMobile.xhtml?${baseParam}">
<div class="commonbar">
<p>手机号码(移动/联通/电信):</p>
<p><input class="txt" name="m" type="text" value="${m}"/></p>
</div>
<div class="commonbar">
<c:set var="currAmount" value="${am}"></c:set>
<c:if test="${currAmount == 0}">
<c:set var="currAmount" value="50"></c:set>
</c:if>
<p>请选择面值:</p>
<p><select class="gamecombo" name="am">
<c:forEach items="${amounts}" var="amount" varStatus="sAmount">
<c:choose>
<c:when test="${amount.yuan eq currAmount}"><option value="${amount.yuan}" selected="selected">${amount.yuan}元</option></c:when>
<c:otherwise><option value="${amount.yuan}">${amount.yuan}元</option></c:otherwise>
</c:choose>
</c:forEach>
</select>

<input class="qb_btn_search" type="submit" id="mobileChargeBtn" value="立即充值" /></p>
</div>
<div class="commonbar">
<input name="from" type="hidden" value="1"/>
<c:choose><c:when test="${fn:length(gcfa) gt 0}"><input name="s" type="hidden" value="4_2001_3_${gcfa}_1"/></c:when><c:otherwise><input name="s" type="hidden" value="4_2001_3_0_1"/></c:otherwise></c:choose>
<input name="sid" type="hidden" value="${sid}" />

</div>
</form>

</div>

<c:if test="${gcfa eq 19111 or gcfa eq 19112 or gcfa eq 19113}">
<div><q:advertpos advertPosId="6" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>
</c:if>

<div><q:advertpos advertPosId="9" basePath="<%=basePath%>" sid="${sid}" rid="${rid}" baseParam="${baseParam4ad}"/></div>

<!-- 提示区域 -->
<div class="commonbox">
<p><strong class="red">9.83</strong>折起,1-10分钟内自动到账</p>
</div>
<div class="commonbox">
<p>温馨提示:如果您的手机已欠费,建议选择大面值,以保证能补足欠费恢复使用.</p>
</div>
<div class="commonbox">
<p class="gray">支持财付通余额和手机网银支付.请用电脑登录www.tenpay.com注册财付通并开通手机支付功能.</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_mobile&amp;sid=${sid}&amp;${baseParam}">话费充值帮助</a></p>
<div style="border:1px solid #D9D9D9;padding:5px 10px">
<table>
<tr><td><img src="http://3gimg.qq.com/mobilelife/qqbuy/t/s/css/v2/img/1.jpg" width="30px" height="30px"/></td><td>&nbsp;下载QQ网购手机客户端<br/>
&nbsp;&nbsp;<a href="http://softfile.3g.qq.com/msoft/buyqq/QQBuy2012_Android_paipaicode.apk">安卓版</a>
&nbsp;&nbsp;<a href="https://itunes.apple.com/cn/app/id566249317">IOS版</a></td></tr>
</table>


</div>

<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

