<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>
</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 
    
<!-- 导航区1 -->
<div class="tabbox">
<p class="tab">
<a href="<%=basePath%>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" title="-话费">话费&nbsp;</a>
<span class="cur">网游&nbsp;</span>
<a href="<%=basePath%>/w/vb2c/buyQQService.xhtml?sid=${sid}&amp;${baseParam}" title="-QQ服务">QQ服务&nbsp;</a>
</p>
</div>

<div class="commonbox">
<p>以字母${cname }开头的网游:&nbsp;</p>
<c:forEach items="${games}" var="game" varStatus="sGame">
<p><a href="<%=basePath%>/w/vb2c/detailGame.xhtml?id=${game.id}&amp;sid=${sid}&amp;${baseParam}">${game.itemName}</a></p>
</c:forEach>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/vb2c/buyGame.xhtml?sid=${sid}&amp;${baseParam}">返回上一页</a></p>
</div>

<div class="commonbox">
<p><span class="red">最便宜</span>的网游充值,&nbsp;1-5分钟到账</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_games&amp;sid=${sid}&amp;${baseParam}">网游充值帮助</a></p>
<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

