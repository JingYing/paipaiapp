<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/include/include.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta name="author" content="tencent_winson" />
<%@ include file="/include/head.jsp"%>
<script type="text/javascript" ></script>
<title>充值中心</title>
<style type="text/css">
	<q:css name="pp_base,vb2c,pp_template" type="wap2"/>
</style>
</head>
  
<body>
<%@ include file="/include/logo.jsp" %> 
    
<!-- 导航区1 -->
<div class="tabbox">
<p class="tab">
<a href="<%=basePath%>/w/vb2c/buyMobile.xhtml?sid=${sid}&amp;${baseParam}" title="-话费">话费&nbsp;</a>
<a href="<%=basePath%>/w/vb2c/buyGame.xhtml?sid=${sid}&amp;${baseParam}" title="-网游">网游&nbsp;</a>
<span class="cur">QQ服务&nbsp;</span>
</p>
</div>

<c:if test="${result.errCode ne 0}">
<!-- 错误信息区域 -->
<div class="commonbox">
<div class="errbox">
<p class="errinfo"><img class="pic" alt="出现错误" src="<%=wimgPath%>/icon1_warn.png" />${result.msg}</p>
<c:set var="i" value="${oi}"></c:set>
</div>
</div>
</c:if>

<form id="chargeQQService"  enctype="multipart/form-data" method="post" action="<%=basePath%>/w/vb2c/detailQQService.xhtml?sid=${sid}&amp;${baseParam}">
<div class="commonbox">
<p>你已选择:&nbsp;${qqServiceDetailPo.qqServiceDisName }</p>
</div>
<div class="commonbox">
<c:choose>
<c:when test="${qqServiceDetailPo.qbi}">
<p>充值数量:&nbsp;</p>
<c:set var="currAmount" value="${amount}"></c:set>
<c:if test="${currAmount == 0}">
<c:set var="currAmount" value="30"></c:set>
</c:if>
<p>
<select class="gamecombo" name="amount">
<c:forEach items="${qqServiceDetailPo.currServiceInfo.options}" var="option" varStatus="sOption">
<c:choose><c:when test="${currAmount eq option.amount}"><option value="${option.amount}" selected="selected">${option.amount}</option></c:when>
<c:otherwise><option value="${option.amount}">${option.amount}</option></c:otherwise></c:choose>
</c:forEach></select>&nbsp;Q币<input name="bc" type="hidden"  value="1"/>
</p>
</c:when>
<c:otherwise>
<p>要开通的数量:&nbsp;</p>
<c:choose>
<c:when test="${fn:length(bc) gt 0}"><p><input class="bctxt" name="bc" type="text"  value="${bc}"/>&nbsp;个月<input name="amount" type="hidden"  value="${amount}"/></p></c:when>
<c:otherwise><p><input class="bctxt" name="bc" type="text"  value="3"/>&nbsp;个月<input name="amount" type="hidden"  value="${amount}"/></p></c:otherwise>
</c:choose>
</c:otherwise>
</c:choose>
</div>
<div class="commonbox">
<p>要充值的QQ号:&nbsp;</p>
<p><input class="qqtxt" name="cuin" type="text"  value="${cuin}"/></p>
</div>
<div class="commonbox">
<p>
<input name="gid" type="hidden"  value="${gid }"/>
<c:choose><c:when test="${fn:length(gcfa) gt 0}"><input name="s" type="hidden"  value="4_10_103_${gcfa }_7"/></c:when><c:otherwise><input name="s" type="hidden"  value="4_10_103_0_7"/></c:otherwise></c:choose>
<input name="f" type="hidden"  value="1"/>
<input class="formCommitBtn" type="submit" id="chargeQQServiceBtn" value="立即充值" />&nbsp;
<a href="<%=basePath%>/w/vb2c/buyQQService.xhtml?sid=${sid}&amp;${baseParam}">返回上一步</a></p>
</div>
</form>

<div class="commonbox">
<p><strong class="red">9.3折</strong>起,&nbsp;1分钟内自动到账</p>
</div>

<div class="commonbox">
<p><a href="<%=basePath%>/w/template.xhtml?tid=help_virtual&amp;sid=${sid}&amp;${baseParam}">充值帮助</a></p>
<hr/>
</div>

<div class="commonbox">
<p>
<a href="<%=basePath%>/w/template.xhtml?tid=index&amp;sid=${sid}&amp;${baseParam}">首页</a>&nbsp;&gt;&nbsp;充值中心
</p>
</div>
<div class="emptybar"></div>

<%@ include file="/include/foot.jsp" %>
</body>
</html>

