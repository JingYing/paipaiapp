$(function($) {
	recommends.init();
});
function recommend(data){
	recommends.getActivityHtml(data);
}

var recommends = {
	config : {
		'html' : '<div class="sg_slider" style="margin-bottom:10px">\
            			<div class="sg_slider_ctn">\
              				<ul>\
                				<li>\
                  					<a class="sg_slider_item" href="{#proUrl#}">\
                      				<div class="sg_slider_img">\
                          				<img src="{#imgUrl#}" alt="{#proName#}">\
                      				</div>\
                      				<div class="sg_slider_text">\
                         				<p class="sg_slider_tit">{#proName#}</p>\
                         				<div class="sg_slider_rec buyerClass" data-BuyerId="{#buyerId#}">\
                            				<div class="sg_slider_rec_tit">\
                               					<img src="http://img.qian-duan-she-ji.us/32x32" alt="买手">\
                                				<span>cyin说：</span> \
                            				</div>\
                            				<p class="sg_slider_rec_ctn">{#buyerTitle#}</p>\
                         				</div>\
                         				<p class="sg_slider_price"><i>&yen;</i>{#activityPrice#}</p>\
                         				<p class="sg_slider_mprice">原价：<i>&yen;</i>{#newPrice#}</p>\
                      				</div>\
                  					</a>\
                				</li>\
              				</ul>\
            			</div>\
          			</div>',
		'ptag' : '20499.8.24'
	},
	init : function(){
		this.getActivityData();
	},
	getActivityData : function(){
		var apiUrl = shangouh5.activityApi['recommend'];
		common.getApiData('h5', apiUrl, 'recommend');
	},
	getActivityHtml : function(data){
		var html = '';
		var newData = data.data;
		var ptag = this.config.ptag;
		for(var i=0;i<newData.length;i++){
			var forHtml = shangouh5.getProductHtml(newData[i], ptag, this.config.html);
			forHtml = forHtml.replace(new RegExp("\{#buyerId#\}","g"), (newData[i].vecExtData[2].strValue == '')?7:newData[i].vecExtData[2].strValue);
			forHtml = forHtml.replace(new RegExp("\{#buyerTitle#\}","g"), newData[i].vecExtData[3].strValue);
			html += forHtml;
		}
		$('.sg_box_bd').html(html);
		buyers.init(replaceHtml);
	}
}

function showPageData16945(data){
	buyers.getActivityHtml(data);
}
var buyers = {
	init : function(func){
		this.func = func;
		this.buyersConfig = new Array();
		this.getActivityData();
	},
	getActivityData : function(){
		var apiUrl = shangouh5.activityApi['buyers'];
		common.getApiData('h5', apiUrl, 'showPageData16945');
	},
	getActivityHtml : function(data){
		var html = '';
		var newData = data.data;
		for(var i=0;i<newData.length;i++){
			var _array = {};
			_array.buyerId = newData[i].uin;
			_array.buyerName = newData[i].userNick;
			_array.buyerIco = newData[i].userImg;
			this.buyersConfig.push(_array);
		}
		this.func();
	}
}
function replaceHtml(){
	$('.buyerClass').each(function(index, element) {
		var buyerId = parseInt($(this).attr('data-BuyerId'));
		for(var i=0;buyers.buyersConfig.length;i++){
			if(buyerId == parseInt(buyers.buyersConfig[i].buyerId)){
				$(this).find('img').attr('src', buyers.buyersConfig[i].buyerIco);
				$(this).find('span').html(buyers.buyersConfig[i].buyerName + '说：');
				break;
			}
		}
	});
}