var lastActivity = {
	config : {
		'html' : '<a class="sg_g" href="{#url#}">\
							<div class="sg_g_img">\
							{#subScriptIco#}\
								<img src="{#image#}" alt="{#shopName#}">\
								<p class="sg_g_desc">{#recmdReason#}</p>\
							</div>\
							<p class="sg_g_tit">{#shopName#}</p>\
							<p class="sg_g_info">\
								<span class="sg_g_sale"><em class="sg_g_sale_price"><i>&yen;</i>{#price#}</em>起，已售<em class="sg_g_sale_num">{#sold#}</em>件</span>\
								<span class="sg_g_time last_g_time" startTime="{#startTime#}" endTime="{#endTime#}">{#timerFormatTitle#}</span>\
							</p>\
						</a>',
		'ptag' : {
			'index' : '20499.8.10',
			'info' : '20499.8.21'
		}
	},
	init : function(index){
		this.isExecuteState = false;
		this.currentTime = shangouh5.currentTime;
		this.classId = index;
		this.getActivityData();
	},
	getActivityData : function(){
		var apiUrl = shangouh5.activityApi['more'];
		common.getApiData('h5', apiUrl, 'tjw59066114722');
	},
	getLastActivityTjwData : function(data){
		var data = common.getData('js', data);
		this.lastTjwData = new Array();
		if(data != undefined && typeof data == "object"){
			var ptag = (this.classId=='index')?this.config.ptag['index']:this.config.ptag['info'];
			for(var i=0;i<data.length;i++){
				if(data[i]['nick'] == this.classId){
					var item = data[i];
					var zImage = item.image;
					var actId = item.id;
					var areaId = item.qq;
					
					var subScriptIco = '';
					if(item.favNum != ''){
						//subScriptIco = '<span class="sg_g_tag">'+item.favNum+'</span>';
					}
					var urlConfig = (item.oldPrice == '') ? "detail.shtml?ptag="+ptag+"&classId="+this.classId+"&actId="+actId+"&areaId="+areaId+shangouh5.isApp+shangouh5.is_wv : item.oldPrice ;
					//推荐位数据存放到数组中
					this.lastTjwData.push({
						'image' : item.image,
						'zImage' : zImage,
						'shopName' : item.shopName,
						'recmdReason' : item.recmdReason,
						'userCredit' : item.userCredit,
						'favNum' : item.favNum,
						'remdRegName' : item.recmdRegName,
						'subScriptIco' : subScriptIco,
						'actId' : actId,
						'areaId' : areaId,
						'classId' : this.classId,
						'url' : urlConfig
					})
				}
			}
			this.getLastActivityCossData();
		}
	},
	getLastActivityCossData : function(){
		if(this.lastTjwData != undefined && typeof this.lastTjwData == "object"){
			var jsonList = '[';
			for(var i=0;i<this.lastTjwData.length;i++){
				var item = this.lastTjwData[i];
				if(i!=0){jsonList += ','}
				var seedNum = (item.userCredit == undefined || item.userCredit == '')?shangouh5.getSeedNum(item.actId, item.areaId):(isNaN(item.userCredit))?shangouh5.getSeedNum(item.actId, item.areaId):item.userCredit;
				jsonList += '{"activeId":"'+item.actId+'","poolId":"'+item.areaId+'","seedNum":"'+seedNum+'"}';
			}
			jsonList += ']';
			
			//通过coss获取数据
			$.ajax({
				type : 'GET',
				contentType : 'application/json',
				url : shangouh5.activityApi['flashbuyNew'],
				dataType : 'jsonp',
				jsonpCallback : 'getLastActivityHtml',
				data : "jsonlist=" + jsonList
			});
		}
	},
	getLastActivityHtml : function(data, isTimeOut){
		if(this.isExecuteState){
			return true;
		}
		var html = '';
		var newData = new Array();
		for(var i=0;i<this.lastTjwData.length;i++){
			if(isTimeOut){
				var cossData = data.data;
				for(var j=0;j<cossData.length;j++){
					var getActivityHtmlAreaId = (cossData[j].poolId == undefined)?cossData[j].areaId:cossData[j].poolId;
					if(this.lastTjwData[i].actId == cossData[j].activeId && this.lastTjwData[i].areaId == getActivityHtmlAreaId){
						var cossItem = cossData[j];
						this.lastTjwData[i]['startTime'] = cossData[j]['beginTime'];
						this.lastTjwData[i]['endTime'] = cossData[j]['endTime'];
						this.lastTjwData[i]['sold'] = (cossData[j]['areasalesVolume']!=0) ? cossData[j]['areasalesVolume'] : cossItem.seedNum*5;
						this.lastTjwData[i]['price'] = cossData[j]['arealowestPrice'];
						newData.push(this.lastTjwData[i]);
						break;
					}
				}
			}else{
				this.lastTjwData[i]['startTime'] = '';
				this.lastTjwData[i]['endTime'] = '';
				this.lastTjwData[i]['sold'] = shangouh5.getSeedNum(this.lastTjwData[i].actId, this.lastTjwData[i].areaId) * 10;
				this.lastTjwData[i]['price'] = shangouh5.getSeedNum(this.lastTjwData[i].actId, this.lastTjwData[i].areaId) * 50;
				newData.push(this.lastTjwData[i]);
			}	
		}
		for(var i=0;i<newData.length;i++){
			var basicData = newData[i];
			var price = sold = startTime = endTime = 0;
			if(basicData.sold != undefined){
				sold = basicData.sold;
			}
			if(basicData.price != 'undefined'){
				price = basicData.price;
				price = (price/100).toFixed(2);
			}
			if(basicData.startTime == '' || basicData.endTime == '' || basicData.startTime == '0' || basicData.endTime == '0'){
				startTime = shangouh5.getTimeStamp(basicData.remdRegName);
				endTime = startTime + 86400 * 5;
			}else{
				startTime = basicData.startTime;
				startTime = shangouh5.getStartTime(startTime);
				
				endTime = basicData.endTime;
				endTime = shangouh5.getEndTime(endTime);
			}
			if(this.currentTime>=startTime && this.currentTime<endTime){
				var timerFormatTitle = shangouh5.timerFormat(this.currentTime, startTime, endTime);
				var forHtml = this.config.html;
				forHtml = forHtml.replace(new RegExp("\{#sold#\}","g"), sold);
				forHtml = forHtml.replace(new RegExp("\{#price#\}","g"), price);
				forHtml = forHtml.replace(new RegExp("\{#startTime#\}","g"), startTime);
				forHtml = forHtml.replace(new RegExp("\{#endTime#\}","g"), endTime);
				forHtml = forHtml.replace(new RegExp("\{#url#\}","g"), basicData.url);
				forHtml = forHtml.replace(new RegExp("\{#subScriptIco#\}","g"), basicData.subScriptIco);
				forHtml = forHtml.replace(new RegExp("\{#image#\}","g"), basicData.zImage);
				forHtml = forHtml.replace(new RegExp("\{#shopName#\}","g"), basicData.shopName);
				forHtml = forHtml.replace(new RegExp("\{#recmdReason#\}","g"), basicData.recmdReason);
				forHtml = forHtml.replace(new RegExp("\{#timerFormatTitle#\}","g"), timerFormatTitle);
				html += forHtml;
			}	
		}
		$("#lastActivity").html(html);
		this.isExecuteState = true;
		var timer = setInterval('lastActivity.timerCountDown()', 1000);
	},
	timerCountDown : function(){
		var This = this;
		this.currentTime = this.currentTime + 1;
		$('.last_g_time').each(function(index, element) {
			var startTime = $(this).attr('startTime');
			var endTime = $(this).attr('endTime');
			$(this).html(shangouh5.timerFormat(This.currentTime, startTime, endTime));		
        });
	}
}
function tjw59010184723(data){
	lastActivity.getLastActivityTjwData(data);
	setTimeout(function(){lastActivity.getLastActivityHtml('', false)}, 1000);
}
function getLastActivityHtml(data){
	lastActivity.getLastActivityHtml(data, true);
}