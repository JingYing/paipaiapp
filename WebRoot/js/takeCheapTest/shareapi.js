function shareApi(config){
    var browser="";
    var doc=document;
    var check=function(){
        if(mqq.iOS||mqq.android)
        {
          return browser="qq";
        }
        else
        {
          var ua = window.navigator.userAgent.toLowerCase(); 
          return ua.match(/micromessenger/i) == 'micromessenger'?browser="wechat":"";
        }
    };

    //设定导航栏分享按钮文案
    var base=function(){
        if(browser=="qq")
        {
          window.setTimeout(function(){
           mqq.ui.setOnShareHandler(function(type){
                hidesharetip();
                var urlitem;
                if(config.ptag){
                    urlitem=addPtag(type==0?config.ptag.q2q:(type==1?config.ptag.q2z:(type==2?config.ptag.q2w:config.ptag.q2t)));
                }else{
                  urlitem=config.url;
                }
                mqq.ui.shareMessage({
                  'title':type==0?config.qq_title:(type==1?config.qzone_title:(type==2?config.wechat_title:config.wechat_time_title)),
                  'desc':type==0?config.qq_desc:(type==1?config.qzone_desc:config.wechat_desc),
                  'share_type':type,
                  'share_url':urlitem,
                  'image_url':config.img,
                  'back':true,
                  'shareElement':'news',
                  'flash_url':'',
                  'puin':/iphone os|ipad\;/ig.test(navigator.userAgent.toLowerCase())?(type==0?12345:''):'',
                  'appid':'',
                  'uinType':''
                },function(){});
          })},500);
        }
        else if(browser=="wechat")
        {
          var sendMessage=function(){
             WeixinJSBridge.on('menu:share:timeline', function(argv){
                wechatInvoke('shareTimeline');
             });
             WeixinJSBridge.on('menu:share:appmessage', function(argv){
                wechatInvoke('sendAppMessage');
             });
             WeixinJSBridge.on('menu:share:weibo', function(argv){
                wechatInvoke('shareWeibo');
             });
             WeixinJSBridge.on('menu:share:email', function(argv){
                wechatInvoke('sendEmail');
             });
          };

          if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
                  sendMessage();
          } else {
                  if (doc.addEventListener) {
                      doc.addEventListener("WeixinJSBridgeReady", sendMessage, false);
                  } else if (doc.attachEvent) {
                      doc.attachEvent("WeixinJSBridgeReady", sendMessage);
                      doc.attachEvent("onWeixinJSBridgeReady", sendMessage);
                  }
          }
        }
        else
        {
          hideshare();
        }

    };

    var wechatInvoke=function(action)
    {
      hidesharetip();
      if(action=="shareWeibo")
      {
            WeixinJSBridge.invoke(action,{
                            "url":addPtag(config.ptag?config.ptag.w2b:""),
                            "content":config.wechat_title,
                            "type":"link"
                        }, function(res){}); 
      }
      else if(action=="sendEmail")
      {
            WeixinJSBridge.invoke(action,{              
                    "title":config.wechat_title,
                    "content":addPtag(config.ptag?config.ptag.w2e:"")
                }, function(res){}); 
      }
      else if(action=="shareTimeline")
      {
            WeixinJSBridge.invoke(action, {
                  img_url:config.img,
                  img_width: "240",
                  img_height: "240",
                  link: addPtag(config.ptag?config.ptag.w2t:""),
                  desc: config.wechat_desc,
                  title: config.wechat_time_title
            }, function (res) {});
      }
      else
      {
         WeixinJSBridge.invoke(action, {
                  img_url:config.img,
                  img_width: "240",
                  img_height: "240",
                  link: addPtag(config.ptag?config.ptag.w2w:""),
                  desc: config.wechat_desc,
                  title: config.wechat_title
        }, function (res) {});
      }
    }

    var addPtag=function(pNum){
        var itemUrl=config.url;
        if(pNum){
          if(itemUrl.indexOf("?")!=-1){
                  itemUrl=itemUrl.split('?');
                  itemUrl=itemUrl.splice(0,1)+"?ptag="+pNum+"&"+itemUrl.join("&");
          }else if(itemUrl.indexOf("#")!=-1){
                  itemUrl=itemUrl.split('#');
                  itemUrl=itemUrl.splice(0,1)+"?ptag="+pNum+"#"+itemUrl.join("#");
          }else{
            itemUrl+="?ptag="+pNum;
          }
        }
        return itemUrl;
    }
    
    var hidesharetip=function(){
        var obj=doc.getElementById("shareTipBox");
        if(obj)
        {
           obj.style.display="none";
        }
    };

    var hideshare=function(){
        var obj=doc.getElementById("shareBox");
        if(obj)
        {
            obj.style.display="none";
        }
    };

    var formatData=function(){
       config.qq_title=config.qq_title||config.title;
       config.qzone_title=config.qzone_title||config.qq_title;
       config.wechat_title=config.wechat_title||config.title;
       config.wechat_time_title=config.wechat_time_title||config.wechat_title;
    };

    var init=function(){
       formatData();
       var _doc=doc.getElementsByTagName('head')[0];
       var script=doc.createElement('script');
       script.setAttribute('type','text/javascript');
       script.setAttribute('src','http://static.paipaiimg.com/paipai_h5/js/ttj/qqapi.custom.js?_bid=152');
       _doc.appendChild(script);
       script.onload=script.onreadystatechange=function(){
          if(!this.readyState||this.readyState=='loaded'||this.readyState=='complete'){
             check();
             base();
          }
          else
          {
              hideshare();
          }
          script.onload=script.onreadystatechange=null;
       }
    }();
};