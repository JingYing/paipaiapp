function hotTuanInto(selector){
    window.hotTuanHtml="";
    function getUin(){
        var uin=getCookie("wg_uin") || getCookie("p_uin") || getCookie("uin");
        return uin;
    }
   /*if(getUin() && parseInt(getUin().replace(/^o/,""),10)%2){
    return false;
   }*/
   var decodeNick=function(nick){
      try{
        return decodeURIComponent(nick).replace(/\+/g," ");
      }catch(e){
        return nick;
      }
    };
    var tpl=['<div class="ht_item">',
                '<div class="ht_avatar"><img src="{tuanLeaderImg}" alt="团长头像"></div>',
                '<div class="ht_info">',
                    '<div class="ht_name">{tuanLeaderName}</div>',
                    '<div class="ht_time">剩余<span>{remainingTime}</span>结束</div>',
                '</div>',
                '<a href="{tuanUrl}" class="ht_btn" ptag="20400.7.1">',
                    '<span class="ht_price"><i>&yen;</i>{tuanPrice} / 件</span>',
                    '<span class="ht_btn_go">还差{shortNum}人成团,立即参团</span>',
                '</a>',
            '</div>'].join('');
    function getHtml(tpl, data){
        return tpl.replace(/{([\w\-]+)\}/g, function(match, key) {
            return typeof data[key] !== undefined ? data[key] : '';
        });
    }
    function getCookie(name) {
        var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
            val = document.cookie.match(reg);
        return val ? (val[2] ? unescape(val[2]) : "") : null;
    }
    function getQuery(name, url) {
        var u = arguments[1] || window.location.search,
            reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"),
            r = u.substr(u.indexOf("\?") + 1).match(reg);
        return r != null ? r[2] : "";
    }
    //微信头像使用小头像
      function smallImg(url){
        if(url && url.match(/^http:\/\/wx\.qlogo\.cn/)!=null){
          url=url.replace(/\/0$/,"/46");
        }
        if(!url){
          url="http://static.paipaiimg.com/fd/qqpai/tuan/img/avatar_3_64.png";
        }
        return url;
      }
    function $getTimeDistance(ts){
    //根据时间差计算剩余的时间，返回[天，小时，分，秒]
          var timeLeft=[0,0,0,0];
    //结构：天、小时、分、秒
          timeLeft[0]=(ts>86400)?parseInt(ts/86400):0;
          ts=ts - timeLeft[0] * 86400;
          timeLeft[1]=(ts>3600)?parseInt(ts/3600):0;
          ts=ts - timeLeft[1] * 3600;
          timeLeft[2]=(ts>60)?parseInt(ts/60):0;
          timeLeft[3]=ts - timeLeft[2] * 60;
          return timeLeft;
    }
    function $addZero(v,size){
        for(var i=0,len=size-(v+"").length;i<len;i++){
            v="0"+v;
        };
        return v+"";
    }
    var url="http://b.paipai.com/groupservice/recommendGroupList?commodityId="+(typeof commodityId!='undefined'?commodityId:getQuery("comodityId"));
    $.get(url,function(json){
        if(json && json.ret=="0"){
            var hotTuanList=json.data,tuanLen=hotTuanList.length,validTuanNum=0;
            if(tuanLen<1){return false;}
            hotTuanHtml += '<div class="ht"><div class="ht_tit">以下小伙伴正在发起团购，您可以直接参与</div><div class="ht_list">';
            for(var i=0;i<tuanLen;i++){
                var curHotTuan=hotTuanList[i];
                var timeDiff=parseInt(curHotTuan.endtime)*1000-(new Date()).getTime();
                if(timeDiff < 0 || getQuery("gid")==curHotTuan.tuanId){continue;}
                validTuanNum+=1;
                var leftTimeArr=$getTimeDistance(parseInt(timeDiff/1000));
                hotTuanHtml += getHtml(tpl,{
                    tuanLeaderName:curHotTuan.nickname?decodeNick(curHotTuan.nickname):"你的好友",
                    tuanLeaderImg:smallImg(curHotTuan.headerUrl),
                    tuanPrice:(parseInt(curHotTuan.price)/100).toFixed(2),
                    remainingTime:$addZero(leftTimeArr[1],2)+":"+$addZero(leftTimeArr[2],2)+":"+$addZero(leftTimeArr[3],2),
                    shortNum:curHotTuan.leftNumber,
                    tuanUrl:"tuan_result.html?_wv=1&gid="+curHotTuan.tuanId
                });  
                if(validTuanNum>=5){break;}
            }
            hotTuanHtml = validTuanNum==0?"":hotTuanHtml+"</div></div>";  
        }
        $(selector).append(hotTuanHtml);
    },'jsonp');
}