
    function propSelector(opt) {
        var _opt =$extend({
			domId: 'propertyDiv',
            stocklist: [],
            stockData: '',
            activeClass: 'sku_option_selected',
            disableClass: 'sku_option_disabled',
            propGroupClass: 'sku_attr',
            initSku: '',
            clickEvent: 'touchstart',
            autoSelect: false,
            cmdtyType: 1,
            onSelected: function(propData) {},
            onSkuEmpty: function(propData) {}
        }, opt),
            domObj = $('#' + _opt.domId),
            propStrs = [],
            doc = document,
            propObj = this,
            propKeyReg = /^\d+#(.+)~.*$/ig;

        _opt.stockData = formatStockData(_opt.stockData);
		
		var sku_html_div ='';
		for(var i=0; i<_opt.stocklist.length; i++){
			sku_html_div +='<div class="sku_item"><h3 class="sku_tit">';
			sku_html_div +=_opt.stocklist[i].StockList_Attribute_Name;
			sku_html_div +=':</h3><div class="sku_attr" data-propname="';
			sku_html_div +=_opt.stocklist[i].StockList_Attribute_Name;
			sku_html_div +='">';
			var values =_opt.stocklist[i].attribute_value;
			for(var j=0; j<values.length; j++){
				sku_html_div +='<span class="sku_option" attr-tag="propItem" data-prop="';
				sku_html_div +=_opt.stocklist[i].StockList_Attribute_Name;
				sku_html_div +=':';
				sku_html_div +=values[j].StockList_Attribute_Value;
				sku_html_div +='" data-img="';
				if(values[j].StockList_Attribute_Img && values[j].StockList_Attribute_Img !=''){
					sku_html_div +=values[j].StockList_Attribute_Img;
				}else{
					sku_html_div +='';
				}
				sku_html_div +='">';
				sku_html_div +=values[j].StockList_Attribute_Value;
				sku_html_div +='</span>';
			}
			sku_html_div +='</div></div>';
		}
		
		$('#'+_opt.domId).html(sku_html_div);
		
        function clickProp(propItem) {
            var me = propItem.parents('.' + _opt.propGroupClass),
                attr = propItem.attr('attr-tag'),
                disabledType;

            while (!attr && propItem.length > 0 && propItem[0] !== this) {
                propItem = propItem.parent();
                attr = propItem.attr('attr-tag');
            }

            disabledType = parseInt(propItem.attr('attr-disabled') || 0);

            if (attr === 'propItem' && disabledType === 0) {
                if (!propItem.hasClass(_opt.activeClass)) {
                    me.attr('data-selectedProp', propItem.attr('data-prop')).
                    attr('data-selectedImg', propItem.attr('data-img'));

                    propItem.addClass(_opt.activeClass).
                    siblings().removeClass(_opt.activeClass);
                } else {
                    me.attr('data-selectedProp', '').
                    attr('data-selectedImg', '');

                    propItem.removeClass(_opt.activeClass)
                }

                var selectedData = propObj.getSelectedProp(),
                    prop = selectedData.prop;

                propObj.setEmptyStockStyle(prop.join('|'));

                _opt.onSelected.call(propObj, selectedData);
                //e.preventDefault && e.preventDefault();
                //e.stopPropagation && e.stopPropagation();
            }
        }

        domObj.find('.' + _opt.propGroupClass).on(_opt.clickEvent, function(e) {
            clickProp($(e.srcElement || e.target));
        });

        this.getSelectedProp = function() {
            var data = {},
                propStrs = [],
                propMap = {},
                key,
                img;

            domObj.find('.' + _opt.propGroupClass).each(function() {
                var propValue = this.getAttribute('data-selectedProp');
                propStrs.push(propValue);

                propMap[this.getAttribute('data-propname')] = propValue;

                if (this.getAttribute('data-selectedImg')) {
                    img = this.getAttribute('data-selectedImg');
                }
            });

            key = propStrs.join('|');

            if (_opt.stockData[key]) {
                data.price = _opt.stockData[key].price;
                data.stockCount = _opt.stockData[key].stockCount;
                data.skuid = _opt.stockData[key].skuid;
                data.sellNum = _opt.stockData[key].sellNum;
            } else {
                data.stockCount = getStockCount(key);
            }

            data.img = img;
            data.prop = propStrs;
            data.propMap = propMap;
            return data;
        }
		
        this.init = function() {
            var _prop = _opt.initSku;

            if (_prop && checkInitValueStock(_prop)) {

                this.selectProp(_prop);
            } else if (_opt.autoSelect) {

                _prop = '';

                for (var key in _opt.stockData) {
                    if (_opt.stockData[key].stockCount > 0) {
                        _prop = key;
                        break;
                    }
                }
                if (_prop) {
                    _prop = _prop.replace(propKeyReg, '$1');
                    this.selectProp(_prop);
                } else {
                    _opt.onSkuEmpty.call(propObj, propObj.getSelectedProp());
                }
            } else {

                _prop = getSingleSku();

                if (_prop && checkInitValueStock(_prop)) {
                    this.selectProp(_prop);
                } else {
                    _opt.onSkuEmpty.call(propObj, propObj.getSelectedProp());
                }
            }
        }

        this.selectProp = function(prop) {

            var propItems = prop.split('|');
            for (var i = 0; i < propItems.length; i++) {
                var arr = propItems[i].split(':'),
                    name = arr[0],
                    v = arr[1],
                    propGroup = $('[data-propname="' + name + '"]'),
                    //propItem = propGroup.find('[data-prop="' + name + ':' + v + '"]'),
                    propItem = $(attr('data-prop', name + ':' + v, propGroup[0])),
                    disabledType = parseInt(propItem.attr('attr-disabled') || 0);

                if (propItem.length > 0 && disabledType === 0) {
                    //propItem.trigger(_opt.clickEvent);
                    clickProp(propItem);
                }
            }

        }


        this.setEmptyStockStyle = function(propStr) {
            !propStr && (propStr = '');

            var _propItemMap = {},
                disabledType = propStr ? '1' : '2',
                enableCount = 0,
                propItems = propStr.split('|'),
                hasStockArr = [],
                noStockArr = [];

            for (var i = 0; i < _opt.stocklist.length; i++) {

                var arrProps = _opt.stocklist[i].attribute_value,
                    name = _opt.stocklist[i].StockList_Attribute_Name + ':',
                    _isMatch = true;

                for (var j = 0; j < arrProps.length; j++) {
                    var key = name + arrProps[j].StockList_Attribute_Value;

                    if (parseInt(arrProps[j].StockList_Attribute_Num) > 0 &&
                        checkHaveStock(propItems, i, key) > 0) {

                        hasStockArr.push(key);
                    } else {
                        noStockArr.push(key);
                    }
                }
            }

            for (var i = 0; i < hasStockArr.length; i++) {
                var key = hasStockArr[i];
                //domObj.find('[data-prop="' + key + '"]').removeClass(_opt.disableClass).attr('attr-disabled', 0);
                $(attr('data-prop', key, domObj[0])).removeClass(_opt.disableClass).attr('attr-disabled', 0);
            }

            for (var i = 0; i < noStockArr.length; i++) {
                var key = noStockArr[i];
                //domObj.find('[data-prop="' + key + '"]').addClass(_opt.disableClass).removeClass(_opt.activeClass).attr('attr-disabled', disabledType);
                $(attr('data-prop', key, domObj[0])).addClass(_opt.disableClass).removeClass(_opt.activeClass).attr('attr-disabled', disabledType);
            }

            return false;
        }

        function checkHaveStock(propItems, rowIndex, propValue) {
            var total = 0;

            for (var key in _opt.stockData) {
                var _names = key.split('|'),
                    _stockCount = parseInt(_opt.stockData[key].stockCount) || 0,
                    _isMatch = true;


                if (key.indexOf(propValue) === -1) {
                    _isMatch = false;
                }


                for (var i = 0; i < propItems.length; i++) {
                    if (i !== rowIndex &&
                        propItems[i] &&
                        key.indexOf(propItems[i]) === -1) {

                        _isMatch = false;
                        break;
                    }
                }

                if (!_isMatch) continue;

                total += _stockCount;
            }

            return total;
        }

        function getStockCount(propStr) {
            var _names = propStr.split('|'),
                total = 0,
                _isMatch;

            for (var key in _opt.stockData) {
                _isMatch = true;
                for (var i = 0; i < _names.length; i++) {

                    if (_names[i] && key.indexOf(_names[i]) === -1) {
                        _isMatch = false;
                        break;
                    }
                }

                if (!_isMatch) continue;

                total += _opt.stockData[key].stockCount * 1;
            }

            return total;
        }

        function checkInitValueStock(propStr) {
            var _total = 0,
                propItems = propStr.split('|');

            for (var key in _opt.stockData) {
                var _stockCount,
                    _isMatch = true;

                for (var i = 0; i < propItems.length; i++) {
                    if (key.indexOf(propItems[i]) === -1) {
                        _isMatch = false;
                        break;
                    }
                }

                if (!_isMatch) continue;

                _total += parseInt(_opt.stockData[key].stockCount) || 0;
            }

            return _total > 0;
        }

        function getSingleSku() {
            var _prop = [];
            for (var i = 0; i < _opt.stocklist.length; i++) {
                var pList = _opt.stocklist[i].attribute_value;
                if (pList.length === 1) {
                    _prop.push(_opt.stocklist[i].StockList_Attribute_Name + ':' + pList[0].StockList_Attribute_Value);
                }
            }

            return _prop.join('|');
        }


        function formatStockData(stockString) {
            var stockData = {},
                arr = stockString.split(';')

                for (var i = 0; i < arr.length; i++) {
                    if (!arr[i]) continue;
                    var arr1 = arr[i].split(',');

                    var key = arr1[0].replace(propKeyReg, '$1'),
                        price = arr1[1],
                        stockCount = arr1[2];

                    stockData[key] = {
                        price: price,
                        stockCount: stockCount,
                        skuid: _opt.cmdtyType === 1 ? arr1[4] : arr1[0].split('~')[1],
                        sellNum: _opt.cmdtyType === 1 ? 0 : arr1[6]
                    }
                }

            return stockData;
        }

        function attr(attr, val, node) {
            var results = [],
                node = node || document.body;

            walk(node, function(n) {

                if (window.__skipHidden && n.type == "hidden" && n.tagName == "INPUT") {
                    return false;
                }
                var actual = n.nodeType === 1 && (attr === "class" ? n.className : (typeof n.getAttribute != "unknown" && n.getAttribute(attr)));
                if (typeof actual === 'string' && (actual === val || typeof val !== 'string')) {
                    results.push(n);
                }
            });

            return results;

            function walk(n, func) {
                func(n);
                n = n.firstChild;
                while (n) {
                    walk(n, func);
                    n = n.nextSibling;
                }
            }
        }

        this.setEmptyStockStyle();
        this.init();
    }
	

	var $extend=function(){
		// copy reference to target object
		var target = arguments[0] || {}, i = 1, length = arguments.length, options;
		// Handle case when target is a string or something (possible in deep copy)
		if ( typeof target != "object" && typeof target != "function" )
			target = {};
		for ( ; i < length; i++ )
			// Only deal with non-null/undefined values
			if ( (options = arguments[ i ]) != null )
				// Extend the base object
				for ( var name in options ) {
					var copy = options[ name ];
					// Prevent never-ending loop
					if ( target === copy )
						continue;
					if ( copy !== undefined )
						target[ name ] = copy;
				}
		// Return the modified object
		return target;
	}