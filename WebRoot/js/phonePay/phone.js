/**
 * Created by liupengfei1 on 14-12-23.
 */

var global_obj = {
    indexFlag: "",    //indexFlag ： 选取列表数据中的index
    data: null,      //data : 为获取的列表数据,
    itemId: "",      //商品id
    faceValue: "",   //面值
    realValue: "",
    url_param: "",   //url 中参数
    isallow: false,   //判断手机号是否为空，空时为false
    _paytype: 5,
    isHasVoucher: false,
    voucherpirce: 0,
    packetId: 0,
    banner_src: "http://www.paipai.com/sinclude/app_virtual_qq.js",
    virtualIn: {
        /**
         *   0成功
         *   1参数校验错误
         *   2未查询到归属地信息
         *   3未查询到商品ID
         *   4价格获取失败
         *   5商品不是直充或已下架
         *   6程序内部错误
         * */
        getPhoneNumAreaPrice: "http://virtualorder.paipai.com/calls/virtual/getPhoneNumAreaPrice", //获取充值价格和区域
        /**
         *  0表示成功；
         *    1表示用户未登录；
         *    2表示传递参数错误；
         *    3表示商品库存不足；
         *    4表示程序内部错误；
         *    5表示商品不是自动充值类型；
         */
        submitorder: "http://virtualorder.paipai.com/suborder/order/insert",//下单接口
        payorder: "http://app.paipai.com/api/pay/unifiedPaymentPlatformApi.xhtml",  //支付接口
        getRedPacket: "http://virtualorder.paipai.com/suborder/order/getRedPacket",
        mobileUsedTimes: "http://virtualorder.paipai.com/suborder/order/mobileUsedTimes"
    },
    /**
     * 初始化方法
     * */
    initialize: function () {
        var scope = this;

        scope.getappToken();
        scope.showTab();
        //立即支付
        if (scope.url_param["againpay"] && scope.url_param["againpay"] == 2) {
            scope._pay({orderId: scope.url_param["order_num"]});
            return;
        }
        //再次购买
        if (scope.url_param["againpay"] && scope.url_param["againpay"] == 1) {
            scope.changePage();
            scope.showInfo();
            scope.bind();
            return;
        }

        scope.showVoucher();
        scope.getBanner();
        scope.showRemeberNum();
        setTimeout(function () {
            scope.getOrderList();
        }, 100);

    },
    showTab: function () {
        var scope = this;
        if (scope.url_param["ver"] && scope.url_param["ver"] == 2 && scope.url_param["againpay"] != 1) {
            $("#_tab").show();
        } else if (scope.url_param["ver"] && scope.url_param["ver"] >= 3 && scope.url_param["againpay"] != 1) {
            $("#_tab").show();
            //$("#_order_div").show();
        } else if (scope.url_param["ver"] && scope.url_param["ver"] == 3 && scope.url_param["againpay"] == 1) {
            //$("._cft_pay").show();
            //$(".again_cft_pay").show();
        }
    },
    showVoucher: function () {
        var scope = this;
        if (scope.url_param["ver"] && scope.url_param["ver"] >= 3 && scope.url_param["appToken"]) {
            $.ajax({
                url: scope.virtualIn.getRedPacket,
                type: "GET",
                dataType: "jsonp",
                data: {
                    sellerUin: "393462",
                    source: "vAPP",
                    sign: "111"
                },
                success: function (data) {
                    if (data != null && data.success && data.retMessage != null) {
                        var d = eval("(" + data.retMessage + ")");
                        if (d.length > 0) {
                            var item = d[0],
                                packetPrice = item.packetPrice;
                            scope.packetId = item.packetId;
                            scope.voucherpirce = scope.packetId;
                            $("#packetprice").html((packetPrice / 100).toFixed(2));
                            scope.isHasVoucher = true;
                            $("._voucher").show();


                        }
                    }
                },
                error: function (e) {
                    console.log(e)
                }
            });
        }
    },
    getBanner: function () {
        var scope = this;
        $.ajaxGbk({
            url: scope.banner_src,
            type: "get",
            dataType: "jsonp",
            jsonpCallback: "QQ",
            data: {},
            success: function (data) {
                //鞋包来袭
                if (data != "" && data.banner != "" && data.banner[0].img != "") {
                    $("#_banner_a").attr("href", data.banner[0].url + "?title=" + encodeURI(data.banner[0].title));
                    $("#_banner_a_img").attr("src", data.banner[0].img);
                    //$("#_banner_a_img").attr("title",data.banner[0].title);
                    if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                        $("#_sure_btn").css("top", "175px");
                        $("#_txl_btn").css("top", "175px");
                    } else {
                        $("#_sure_btn").css("top", "135px");
                        $("#_txl_btn").css("top", "135px");
                    }
                    $("#_banner").show();
                } else {
                    $("#_banner").hide();
                    if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                        $("#_sure_btn").css("top", "55px");
                        $("#_txl_btn").css("top", "55px");
                    } else {
                        $("#_sure_btn").css("top", "15px");
                        $("#_txl_btn").css("top", "15px");
                    }

                }
            },
            error: function () {
                $("#_banner").hide();
                $("#_banner").hide();
                if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                    $("#_sure_btn").css("top", "55px");
                    $("#_txl_btn").css("top", "55px");
                } else {
                    $("#_sure_btn").css("top", "15px");
                    $("#_txl_btn").css("top", "15px");
                }
            }
        });

    },
    /**
     * 根据url获取参数
     * */
    getappToken: function () {
        var scope = this;
        var url = location.href;
        scope.url_param = {};
        if (url.indexOf("?") != -1) {
            var str = url.substr(url.indexOf("?") + 1);
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                scope.url_param[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
            }
        }
    },
    /**
     * 根据传入参数name获取cookie方法(暂时没用到)
     * name : cookie的键aa
     * */
    getCookie: function (name) {
        var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
            val = document.cookie.match(reg);
        return val ? (val[2] ? unescape(val[2]) : "") : null;
    },
    /**
     * 根据是否含appToken检查用户是否登陆
     * */
    checkLogin: function () {
        var scope = this;
        return scope.url_param["appToken"] ? true : false;
    },
    /**
     * 根据传进的url跳转到登陆页面
     * rurl : 传入的链接地址
     * */
    plogin: function (rurl) {
        var scope = this;
        if (!rurl) {
            return;
        }
        var v = $("#qq_num").val();
        v = v.replace(/\D/g, '');
        window.location.href = "paipai://login?indexflag=" + scope.indexFlag + "&phone_num=" + v + "&target_flag=phonePay&ver=" + scope.url_param["ver"];
    },
    /**
     * 1.根据localStorage中储存的已充值过的号码进行展示，并将最近充值的显示在输入框中
     * 2.如果没有充值过的判断是否是qq登陆，是的话，显示qq号
     * */
    showRemeberNum: function () {
        var scope = this;
        var phones = localStorage["_phone" + scope.url_param["appToken"]];
        if (phones && phones != "") {
            var phonearr = phones.split(",");
            for (var i = 0, len = phonearr.length; i < len; i++) {
                var str = "<div class='_input_qq'><span class='xx'>" + phonearr[i] + "</span></div>";
                $("#input_qq").append(str);
            }
            var cleardiv = $("<div class='_input_qq_history'></div>");
            var clearbtn = $("<div class='clear_history'>清空充值记录</div>");
            cleardiv.append(clearbtn)
            $("#input_qq").append(cleardiv);
            clearbtn.on("tap", function () {
                scope.clearRememberNum();
            });
            //默认输入已充值的第一个qq号
            $("#qq_num").val(phonearr[0]);
        } else {
            scope.isallow = false;
            $("#_bottom_btn").addClass("bottom_btn_gray");
        }

    },
    /**
     *清空 localStorage 中已存在的充值号码
     * */
    clearRememberNum: function () {
        var scope = this;
        localStorage["_phone" + scope.url_param["appToken"]] = null;
        localStorage["_phone" + scope.url_param["appToken"]] = "";
        $("#input_qq").html("");
    },
    /**
     * 获取订单列表的方法
     * */
    getOrderList: function () {
        var scope = this;
        scope.data = [30, 50, 100, 200, 300, 500];
        for (var i = 0, len = scope.data.length; i < len; i++) {
            $("#order").append('<li><div class="order_wrap" _price="' + scope.data[i] + '" indexflag="' + i + '">' + scope.data[i] + '元</div></li>');
            if (i == 3 && !scope.url_param["indexflag"]) {
                scope.indexFlag = i;
            }
        }
        $("#order div").eq(scope.indexFlag).removeClass().addClass("order_wrap_select");
        scope.giveValue(scope.data);
        scope.bind();

    },
    /**
     * 根据url中的值是否是有填写的手机号号和是否选择了充值样式，如果有的话填入
     * */
    giveValue: function (data) {
        var scope = this;
        if (scope.url_param["indexflag"]) {
            scope.indexFlag = scope.url_param["indexflag"];
            $("#order div").removeClass().addClass("order_wrap");
            $("#order div").eq(scope.indexFlag).removeClass().addClass("order_wrap_select");
        }
        if (scope.url_param["phone_num"] && scope.url_param["phone_num"] != "" && scope.url_param["phone_num"] != null) {
            $("#qq_num").val(scope.url_param["phone_num"]);
            $("#_bottom_btn").removeClass("bottom_btn_gray");
        }
        if ($("#qq_num").val() != "" && $("#qq_num").val() != null) {
            scope.checkqqNull();
        }
    },
    /**
     * 检查手机号是否正确，如果不正确弹出浮动层
     * */
    checkqqNull: function () {
        var scope = this;
        var v = $("#qq_num").val();
        v = v.replace(/\D/g, '');
        var reg = /^\d{11,11}$/;
        $("#face_value").hide();
        $("#totalprice").hide();
        if (v != null && v != "") {
            if (!reg.test(v)) {
                /*alert("请输入正确的QQ号码");*/
                $("#_toast").show();
                scope.isallow = false;
                $("#_bottom_btn").addClass("bottom_btn_gray");
                return false;
            } else {
                scope.getAreaPrice(v);
            }
        } else {
            scope.isallow = false;
            $("#_bottom_btn").addClass("bottom_btn_gray");
        }
    },
    /**
     * 得到价格区间
     * */
    getAreaPrice: function (phone_num) {
        var scope = this;
        scope.isallow = false;
        $("#_bottom_btn").addClass("bottom_btn_gray");
        scope.changeValue(phone_num, true);
        $.ajax({
            url: scope.virtualIn.getPhoneNumAreaPrice,
            type: "GET",
            dataType: "jsonp",
            data: {
                phoneNum: phone_num,
                faceValue: scope.data[scope.indexFlag]
            },
            success: function (data) {
                if (data != null && data.returnCode == "0") {
                    scope.itemId = data.itemId;
                    scope.faceValue = data.faceValue;
                    scope.realValue = data.price;
                    $("#area").html(data.province + data.operators);
                    $("#face_num").html(data.price);
                    $("#face_value").show();
                    scope.getTotalPrice();
                    scope.isallow = true;
                    $("#_bottom_btn").removeClass("bottom_btn_gray");
                    if (scope.url_param["phone_num"] && scope.url_param["phone_num"] != "" && scope.url_param["phone_num"] != null) {
                        scope.isallow = false;
                        $("#_toast_pay").show();
                        scope.url_param["phone_num"]="";
                        scope.beginOrder();
                    }
                } else if (data.returnCode == "5" || data.returnCode == "3" || data.returnCode == "4" || data.returnCode == "6") {//346
                    $("#_toast_face_value_text").html("商品不是直充或已下架");
                    $("#_toast").show();
                    scope.isallow = false;
                    $("#_bottom_btn").addClass("bottom_btn_gray");
                } else {
                    $("#_toast").show();
                    scope.isallow = false;
                    $("#_bottom_btn").addClass("bottom_btn_gray");
                }
            }
        });
    },
    getTotalPrice: function () {
        var scope = this;
        if (scope.packetId && scope.realValue != "" && scope.realValue != null) {
            if (scope.voucherpirce) {
                $("#totalprice").find("em").html((parseFloat(scope.realValue) - parseFloat($("#packetprice").html())).toFixed(2));
                $("#totalprice").show();
            } else {
                $("#totalprice").find("em").html(scope.realValue);
                $("#totalprice").show();
            }
        } else {
            $("#totalprice").hide();
        }
    },
    /**
     * 1.根据输入的qq号和选择的充值方式获得订单号的方法
     * 2.在次购买时根据传过来的参数获得订单号
     * */
    beginOrder: function () {
        var scope = this;
        var param, checkPhone;
        if (scope.url_param["againpay"] && scope.url_param["againpay"] == 1) {
            checkPhone = scope.url_param["phone_num"];
            param = {
                rechargeAccount: scope.url_param["phone_num"],   //充值的手机号
                price: scope.url_param["price"],    //价格
                paytype: scope._paytype,  // 是支付类型，1是财付通，5是微信支付
                itemtype: "v",    //
                itemcnt: 1,      //
                itemId: scope.url_param["itemid"], //商品id
                packetId: "0",  //红包
                source: "vAPP",
                sign: "111",
                dealType: 6
            };
        } else {
            var v = $("#qq_num").val()
            v = v.replace(/\D/g, '');
            checkPhone = v;
            param = {
                rechargeAccount: v,   //充值的QQ号
                price: scope.faceValue,    //价格
                paytype: scope._paytype,  // 是支付类型，1是财付通，5是微信支付
                itemtype: "v",    //
                itemcnt: 1,      //
                itemId: scope.itemId, //商品id
                packetId: scope.voucherpirce, //红包
                source: "vAPP",
                sign: "111",
                dealType: 6
            };
        }
        scope.checkOrderTimes(checkPhone, param);
        return;

    },
    checkOrderTimes: function (phone_num, param) {
        var scope = this;
        $.ajax({
            url: scope.virtualIn.mobileUsedTimes,
            type: "GET",
            dataType: "jsonp",
            data: {rechargeAccount: phone_num},
            success: function (data) {
                if (data.retCode == 0) {
                    $.ajax({
                        url: scope.virtualIn.submitorder,
                        type: "GET",
                        dataType: "jsonp",
                        data: param,
                        success: function (data) {
                            if (data.retCode == 0) {
                                scope._pay(data);
                            } else if (data.retCode == 1) {
                                scope._payhide();
                                scope.plogin(location.href);
                            } else {

                            }
                        }
                    });
                    return;
                } else if (data.retCode == 1) {
                    scope._payhide();
                    scope.plogin(location.href);
                    return;
                } else {
                    $("#_toast_pay").hide();
                    scope.isallow = false;
                    $("#_bottom_btn").addClass("bottom_btn_gray");
                    $("#_toast_face_value_text").html("该手机号充值超过下单次数");
                    $("#_toast").show();
                    return;
                }
            },
            error: function () {
                $("#_toast_pay").hide();
                scope.isallow = false;
                $("#_bottom_btn").addClass("bottom_btn_gray");
                $("#_toast_face_value_text").html("检查手机号充值次数出错");
                $("#_toast").show();
                return false;
            }
        });
    },
    _payhide: function () {
        var scope = this;
        setTimeout(function () {
            scope.isallow = true;
            $("#_toast_pay").hide();
        }, 3000)
    },
    /**
     * 唤起支付
     * */
    _pay: function (order_data) {
        var scope = this;
        var param = {
            deals: order_data.orderId + "~11000",
            payType: "v_wx",
            appToken: unescape(scope.url_param["appToken"]),
            mk: scope.url_param["mk"]
        }
        $.ajax({
            url: scope.virtualIn.payorder,
            type: "GET",
            dataType: "jsonp",
            data: param,
            success: function (data) {
                if (data.errCode == "0") {
                    data = data.data;
                    var url = "paipai://wxpay?1=1";
                    for (var key in data) {
                        url += "&" + key + "=" + data[key];
                    }
                    url += "&order_num=" + order_data.orderId + "&isshare=n&shareTitle=" +
                    encodeURI("立即分享，再获3元充值代金券") + "&shareContent=" + encodeURI("现在下载拍拍APP，马上送话费，快来抢！") +
                    "&shareUrl=" + encodeURI("http://t.cn/RAP0P0b") +
                    "&shareTopic=" + encodeURI("现在下载拍拍APP，马上送话费，快来抢！") +
                    "&shareImg=" + encodeURI("http://t.cn/RAP0w7t");
                    scope._payhide();
                    location.href = url;
                } else {
                    scope._payhide();
                }
            },
            error: function (err) {
                console.log(err)
            }
        });
    },
    /**
     * 保存到localStorage中已充值的qq号 localStorage["qq_nums"]
     * */
    rememberNum: function (qq_num) {
        var scope = this;
        var arr = [];
        qq_num = qq_num.replace(/\D/g, '');
        var qqs = localStorage["_phone" + scope.url_param["appToken"]];
        if (qqs && qqs != "") {
            var qqarr = qqs.split(",");
            for (var i = 0, len = qqarr.length; i < len; i++) {
                if (qq_num == qqarr[i]) {
                    qqarr.splice(i, 1);
                    break;
                }
            }
            if (qqarr.length >= 10) {
                qqarr.splice(qqarr.length - 1, 1);
                qqarr.unshift(qq_num);
            } else {
                qqarr.unshift(qq_num);
            }
            localStorage["_phone" + scope.url_param["appToken"]] = qqarr;
        } else {
            arr.push(qq_num);
            localStorage["_phone" + scope.url_param["appToken"]] = arr;
        }
    },
    /**
     * 改变显示页面的方法
     * */
    changePage: function () {
        var scope = this;
        if (scope.url_param["imgurl"]) {
            $("#qqtx").attr("src", "http://img7.paipaiimg.com/2d88b246/" + scope.url_param["imgurl"]);
        }
        scope.isallow = true;
        $("#_bottom_btn").hide();
        $("#virtual_body").addClass("body_white");
        $("#_tab").hide();
        $("#_order_page").hide();
        $("#_input_div_txl").hide();
        $("#_confirm_page").show();
        $("#_banner").hide();
    },
    /**
     * 在次购买时，显示的qq号，商家信息，和价钱
     * */
    showInfo: function () {
        var scope = this;
        var pay_qq = scope.url_param["phone_num"],
            shop_name = scope.url_param["shop_name"],
            pay_value = scope.url_param["price"],
            commodity_desc = scope.url_param["commodity_desc"];
        if (pay_qq != "" && pay_qq != null && typeof pay_qq != "undefined")
            $("#pay_qq").html(pay_qq);
        if (shop_name != "" && shop_name != null && typeof shop_name != "undefined")
            $("#shop_name").html(shop_name);
        if (commodity_desc != "" && commodity_desc != null && typeof commodity_desc != "undefined")
            $("#commodity_desc").html(commodity_desc);
        if (pay_value != "" && pay_value != null && typeof pay_value != "undefined") {
            pay_value = (parseFloat(pay_value) / 100).toFixed(2);
            $("#pay_value").html("￥" + pay_value);
            $("#total_value").html("￥" + (pay_value * parseInt($("#pay_num").html())).toFixed(2));
        }
    },
    changeValue: function (v, flag) {
        var scope = this;
        v = v.replace(/\D/g, '');
        if (v.length == 11) {
            v = v.substr(0, 3) + " " + v.substr(3, v.length);
            v = v.substr(0, 8) + " " + v.substr(8, v.length);
            if (flag) {
                $("#qq_num").val(v);
            } else {
                $("#qq_num").val("").focus().val(v);
                $("#qq_num").blur();
            }
            if (!flag) {
                $("#_inner_btn").click();
            }
        } else {
            $("#qq_num").val(v);
        }
    },
    /**
     * 绑定点击事件方法
     * */
    bind: function () {
        var scope = this;
        //选择充值类型点击事件
        $("#order div").on("tap", function () {
            scope.isallow = false;
            $("#_bottom_btn").addClass("bottom_btn_gray");
            $("#face_value").hide();
            $("#order div").removeClass().addClass("order_wrap");
            $(this).removeClass().addClass("order_wrap_select");
            scope.indexFlag = $(this).attr("indexflag");
            if ($("#qq_num").val() != "" && $("#qq_num") != null) {
                var v = $("#qq_num").val();
                v = v.replace(/\D/g, '');
                scope.getAreaPrice(v);
            }
        });

        //点击立即充值按钮事件
        $("#btn").on("tap", function () {
            if (!scope.isallow) {
                return false;
            }
            scope.isallow = false;
            $("#_toast_pay").show();
            var qq_num = $("#qq_num").val();
            qq_num = qq_num.replace(/\D/g, '');
            scope.rememberNum(qq_num);
            if (!scope.checkLogin()) {
                scope._payhide();
                scope.plogin(location.href);
            } else {
                scope.beginOrder();
            }
            return;
        });
        //头部返回按钮事件
        $("#_header").on("tap", function () {
            $("#_header").hide();
            $("#_order_page").show();
            $("#_confirm_page").hide();
        });
        //确认订单事件
        $("#confirm_btn").on("tap", function () {
            if (!scope.isallow) {
                return false;
            }
            scope.isallow = false;
            $("#_toast_pay").show();
            scope.beginOrder();
        });
        //点击面值
        $("#face_value").on("tap", function () {
            $("#float_div").show();
            var t = 0;
            var fun = function () {
                var time = setTimeout(function () {
                    t++
                    $("#order_wrap_id").css("bottom", (10 * t - 90) + "px");
                    fun();
                }, 10)
                if (t == 9) {
                    clearTimeout(time);
                }
            }
            fun();
        });
        $("#_opacity_div").on("tap", function () {
            $("#float_div").hide();
            $("#order_wrap_id").css("bottom", "-90px");
        });
        $("._input_qq").on("tap", function () {
            var v = $(this).children("span").text();
            $("#qq_num").val(v);
            $("#input_qq").hide();
            $("#qq_num").blur();
            $("#order_select").show();
            setTimeout(function () {
                $("#_bottom_btn").show();
            }, 600);
            $("#_sure_btn").hide();
            $("#_txl_btn").show();
            scope.checkqqNull();
        });
        //点击输入框
        $("#qq_num").on("click", function () {
            var qqs = localStorage["_phone" + scope.url_param["appToken"]];
            scope.isallow = false;
            $("#_bottom_btn").addClass("bottom_btn_gray");
            $("#_txl_btn").hide();
            $("#_sure_btn").show();
            $("#input_qq").show();
            $("#_bottom_btn").hide();
            $("#order_select").hide();
        });
        $("#qq_num").on("input propertychange", function (evt) {
            evt.preventDefault();
            var v = $(this).val();
            scope.changeValue(v);
        });
        $("#qq_num").on("change", function (evt) {
            evt.preventDefault();
            var v = $(this).val();
            scope.changeValue(v);
        });
        //点击确定
        $("#_inner_btn").on("click", function (event) {
            $("#input_qq").hide();
            $("#qq_num").blur();
            $("#order_select").show();
            setTimeout(function () {
                $("#_bottom_btn").show();
            }, 600);
            $("#_sure_btn").hide();
            $("#_txl_btn").show();
            scope.checkqqNull();
            $("#face_value").focus();
        })
        $("#_txl_btn").on("click", function () {
            location.href = "paipai://mailList?inputVal=qq_num&checkNum=global_obj.getAreaPrice";
        });
        //清空qq号
        $("#_del_btn").on("click", function () {
            $("#qq_num").val("");
            $("#qq_num").focus();
        });
        $("#_toast").on("tap", function () {
            $(this).hide();
            $("#_toast_face_value_text").html("请输入正确的手机号码");
        });

        $(".tab_a").on("click", function () {
            var target_flag = $(this).attr("target_flag");
            var ver = scope.url_param["ver"];
            location.href = "paipai://redirectPage?target_flag=" + target_flag + "&ver=" + ver;
        })

        /*  $(".order_div").on("click", function (e) {
         var t = $(e.target).attr("class");
         if(t=="_wx_pay"){
         scope._paytype=5;
         $(".cft_pay").removeClass("selected");
         $(".wx_pay").addClass("selected");
         }else if(t=="_cft_pay"||t=="again_cft_pay"){
         scope._paytype=1;
         $(".wx_pay").removeClass("selected");
         $(".cft_pay").addClass("selected");
         }
         });*/

        $("#voucher").on("click", function () {
            var voucherflag = $(this).attr("voucherflag");
            if (voucherflag == 1) {
                $(this).attr("voucherflag", "2");
                scope.voucherpirce = scope.packetId;
                $(this).find("img").attr("src", "../img/phonePay/gou.png");
                scope.getTotalPrice();
            } else if (voucherflag == 2) {
                $(this).attr("voucherflag", "1");
                scope.voucherpirce = 0;
                scope.getTotalPrice();
                $(this).find("img").attr("src", "../img/phonePay/nogou.png")
            }
        });
    }
};
//Fasttap.attach(document.body);
global_obj.initialize();


/*function connectWebViewJavascriptBridge(callback) {
 if (window.WebViewJavascriptBridge) {
 callback(WebViewJavascriptBridge)
 } else {
 document.addEventListener('WebViewJavascriptBridgeReady', function() {
 callback(WebViewJavascriptBridge)
 }, false)
 }
 }

 connectWebViewJavascriptBridge(function(bridge) {
 bridge.init(function(message, responseCallback) {
 if (responseCallback) {
 responseCallback("0")
 }
 })

 bridge.registerHandler('WXDcallbackiOS', function(data, responseCallback) {
 alert('good. ' + data.event + ' ' + data.tag)
 log('ObjC called testJavascriptHandler with', data)
 var responseData = { 'Javascript Says':'Right back atcha!' }
 log('JS responding with', responseData)
 responseCallback(responseData)
 })

 var args=
 {
 pageurl: location.origin+location.pathname,
 pageParam: "launchType=new"
 };
 bridge.callHandler('recordPV', JSON.stringify(args), function(response) {
 log('JS got response', response)
 });


 $("#order div").on("tap",function(){
 var op= {
 ptag: "2015.1.1",
 clickparam:"type=充值类型"
 }
 bridge.callHandler('recordCL', JSON.stringify(op), function(response) {
 log('JS got response', response)
 });
 });
 })*/
