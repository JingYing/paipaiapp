$(function($) {
	snipings.init();
});
function sniping(data){
	snipings.getActivityHtml(data);
}

var snipings = {
	config : {
		'html' : '<a class="sg_g4" href="{#proUrl#}">\
							<div class="sg_g4_img">\
								<img src="{#imgUrl#}" alt="{#proName#}">\
							</div>\
							<div class="sg_g4_info">\
								<p class="sg_g4_tit">{#proName#}</p>\
								<span class="sg_g4_price"><i>￥</i>{#activityPrice#}</span>\
								<span class="sg_g4_price sg_g4_mprice"><i>￥</i><del>{#newPrice#}</del></span>\
							</div>\
						</a>',
		'ptag' : '20499.8.24'
	},
	init : function(){
		this.currentTime = shangouh5.currentTime;
		this.startTime = 0;
		this.endTime = 0;
		this.getActivityData();
	},
	getActivityData : function(index){
		var apiUrl = shangouh5.activityApi['sniping'];
		common.getApiData('h5', apiUrl, 'sniping');
	},
	getActivityHtml : function(data){
		var html = '';
		var newData = data.data;
		for(var i=0;i<newData.length;i++){
			if($('.sg_nav_list').attr('id') == 'index'){
				if(i>3){
					break;
				}
				var j = i + 2;
				var ptag = '20499.8.' + j;
			}else{
				var ptag = this.config.ptag;
			}
			if(this.startTime==0 || this.endTime==0){
				this.startTime = newData[i].dwBeginTime;
				this.endTime = newData[i].dwEndTime;
			}
			html += shangouh5.getProductHtml(newData[i], ptag, this.config.html);
		}
		$('.sg_glist4').html(html);
		this.timerCountDown();
		var timer = setInterval('snipings.timerCountDown()', 1000);
	},
	timerCountDown : function(){
		this.currentTime = this.currentTime + 1;
		$('.sg_ms_time_left').html(this.timerFormat(this.currentTime, this.startTime, this.endTime));
	},
	timerFormat : function(currentTime, startTime, endTime){
		var title = '';
		if(currentTime<startTime){
			title = '<span>即将开始</span>';
		}else if(currentTime>=endTime){
			title = '活动已结束';
		}else{
			var differenceTime = endTime - currentTime;
			var differenceTimeTitle = commonDate.getDifferTimeTitle(differenceTime);
			var H = differenceTimeTitle.D * 24;
			var H = commonDate.checkTime(parseInt(H) + parseInt(differenceTimeTitle.H));
			var arrayH = String(H).split("");
			var arrayI = String(differenceTimeTitle.I).split("");
			var arrayS = String(differenceTimeTitle.S).split("");
			title = '<span>'+arrayH[0]+'</span><span>'+arrayH[1]+'</span><i>:</i><span>'+arrayI[0]+'</span><span>'+arrayI[1]+'</span><i>:</i><span>'+arrayS[0]+'</span><span>'+arrayS[1]+'</span>';
		}
		return title;
	}
}