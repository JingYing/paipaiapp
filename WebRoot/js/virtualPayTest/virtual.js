/**
 * Created by liupengfei1 on 14-12-23.
 */

(function () {

    var obj = {
        indexFlag: "",    //indexFlag ： 选取列表数据中的index
        data: null,      //data : 为获取的列表数据
        url_param: "",   //url 中参数
        isallow: true,   //判断qq号是否为空，空时为false
        banner_src: "http://www.paipai.com/sinclude/app_virtual_qq.js",
        _paytype: 5,
        virtualIn: {
            getVirtualItemList: "http://virtualorder.paipai.com/web/virtual/getVirtualItemList", //获取列表接口
            submitorder: "http://virtualorder.paipai.com/suborder/order/insert",  //提交订单接口
            payorder: "http://app.paipai.com/api/pay/unifiedPaymentPlatformApi.xhtml"  //支付接口
        },
        /**
         * 初始化方法
         * */
        initialize: function () {
            var scope = this;
            scope.getappToken();
            scope.showTab();
            if (scope.url_param["againpay"] && scope.url_param["againpay"] == 2) {
                scope._pay({orderId: scope.url_param["order_num"]});
                return;
            }
            if (scope.url_param["againpay"] && scope.url_param["againpay"] == 1) {
                scope.changePage();
                scope.showInfo();
                scope.bind();
                return;
            }
            scope.getBanner();
            scope.showRemeberNum();
            scope.getOrderList();
        },
        showTab: function () {
            var scope = this;
            if (scope.url_param["ver"] && scope.url_param["ver"] == 2 && scope.url_param["againpay"] != 1) {
                $("#_tab").show();
            } else if (scope.url_param["ver"] && scope.url_param["ver"] == 3 && scope.url_param["againpay"] != 1) {
                $("#_tab").show();
                //$("._cft_pay").show();
                //$(".again_cft_pay").show();
            } else if (scope.url_param["ver"] && scope.url_param["ver"] == 3 && scope.url_param["againpay"] == 1) {
                //$("._cft_pay").show();
                //$(".again_cft_pay").show();
            }
        },
        getBanner: function () {
            var scope = this;
            $.ajaxGbk({
                url: scope.banner_src,
                type: "get",
                dataType: "jsonp",
                jsonpCallback: "QQ",
                data: {},
                success: function (data) {
                    //鞋包来袭
                    if (data != "" && data.banner != "" && data.banner[0].img != "") {
                        $("#_banner_a").attr("href", data.banner[0].url + "?title=" + encodeURI(data.banner[0].title));
                        $("#_banner_a_img").attr("src", data.banner[0].img);
                        //$("#_banner_a_img").attr("title",data.banner[0].title);
                        if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                            $("#_sure_btn").css("top", "175px");
                        } else {
                            $("#_sure_btn").css("top", "135px");
                        }

                        $("#_banner").show();
                    } else {
                        $("#_banner").hide();
                        if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                            $("#_sure_btn").css("top", "55px");
                        } else {
                            $("#_sure_btn").css("top", "15px");
                        }

                    }
                },
                error: function () {
                    $("#_banner").hide();
                    if (scope.url_param["ver"] && scope.url_param["ver"] >= 2) {
                        $("#_sure_btn").css("top", "55px");
                    } else {
                        $("#_sure_btn").css("top", "15px");
                    }
                }
            });

        },
        /**
         * 根据url获取参数
         * */
        getappToken: function () {
            var scope = this;
            var url = location.href;
            scope.url_param = {};
            if (url.indexOf("?") != -1) {
                var str = url.substr(url.indexOf("?") + 1);
                var strs = str.split("&");
                for (var i = 0; i < strs.length; i++) {
                    scope.url_param[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
                }
            }
        },
        /**
         * 根据传入参数name获取cookie方法(暂时没用到)
         * name : cookie的键aa
         * */
        getCookie: function (name) {
            var reg = new RegExp("(^| )" + name + "(?:=([^;]*))?(;|$)"),
                val = document.cookie.match(reg);
            return val ? (val[2] ? unescape(val[2]) : "") : null;
        },
        /**
         * 根据是否含appToken检查用户是否登陆
         * */
        checkLogin: function () {
            var scope = this;
            return scope.url_param["appToken"] ? true : false;
        },
        /**
         * 根据传进的url跳转到登陆页面
         * rurl : 传入的链接地址
         * */
        plogin: function (rurl) {
            var scope = this;
            if (!rurl) {
                return;
            }
            window.location.href = "paipai://login?indexflag=" + scope.indexFlag + "&qq_num=" + $("#qq_num").val() + "&target_flag=qqPay&ver=" + scope.url_param["ver"];
        },
        /**
         * 1.根据localStorage中储存的已充值过的号码进行展示，并将最近充值的显示在输入框中
         * 2.如果没有充值过的判断是否是qq登陆，是的话，显示qq号
         * */
        showRemeberNum: function () {
            var scope = this;
            var qqs = localStorage["_" + scope.url_param["appToken"]];
            if (qqs && qqs != "") {
                var qqarr = qqs.split(",");
                for (var i = 0, len = qqarr.length; i < len; i++) {
                    var str = "<div class='_input_qq'><span class='xx'>" + qqarr[i] + "</span></div>";
                    $("#input_qq").append(str);
                }
                var cleardiv = $("<div class='_input_qq_history'></div>");
                var clearbtn = $("<div class='clear_history'>清空充值记录</div>");
                cleardiv.append(clearbtn)
                $("#input_qq").append(cleardiv);
                clearbtn.on("tap", function () {
                    scope.pointReport("ptag", scope.points.ptag[4], "");
                    scope.clearRememberNum();
                });
                //默认输入已充值的第一个qq号
                $("#qq_num").val(qqarr[0]);
            } else {
                if (scope.url_param["user_qq"]) {
                    $("#qq_num").val(scope.url_param["user_qq"]);
                } else {
                    scope.isallow = false;
                    $("#_bottom_btn").addClass("bottom_btn_gray");
                }
            }

        },
        /**
         *清空 localStorage 中已存在的充值号码
         * */
        clearRememberNum: function () {
            var scope = this;
            localStorage["_" + scope.url_param["appToken"]] = null;
            localStorage["_" + scope.url_param["appToken"]] = "";
            $("#input_qq").html("");
            $("#virtual_body").addClass("body_white");
        },
        /**
         * 获取订单列表的方法
         * */
        getOrderList: function () {
            var scope = this;
            $.ajax({
                url: scope.virtualIn.getVirtualItemList,
                type: "GET",
                dataType: "jsonp",
                data: {},
                success: function (data) {
                    scope.data = data;
                    for (var i = 0, len = scope.data.length; i < len; i++) {
                        var item = scope.data[i],
                            show_title = item.faceValue + "个/" + item.faceValue + "元";
                        if (i == 2 && !scope.url_param["indexflag"]) {
                            $("#_price").html("￥" + item.price);
                            $("#_discount").html("（" + item.discount + "折）");
                            $("#face_num").html(item.faceValue + "个/" + item.faceValue + "元");
                            scope.indexFlag = i;
                        }
                        $("#order").append('<li><div class="order_wrap" _price="' + item.price + '" face_num="' + item.faceValue + '" _discount="' + item.discount + '" indexflag="' + i + '">' + show_title + '</div></li>');
                    }
                    $("#order div").eq(scope.indexFlag).removeClass().addClass("order_wrap_select");
                    scope.giveValue(data);
                    scope.bind();
                }
            });

        },
        /**
         * 根据url中的值是否是有填写的qq号和是否选择了充值样式，如果有的话填入
         * */
        giveValue: function (data) {
            var scope = this;
            if (scope.url_param["indexflag"]) {
                scope.indexFlag = scope.url_param["indexflag"];
                var item = data[scope.indexFlag];
                $("#_price").html("￥" + item.price);
                $("#_discount").html("（" + item.discount + "折）");
                $("#face_num").html(item.faceValue + "个/" + item.faceValue + "元");
                $("#order div").removeClass().addClass("order_wrap");
                $("#order div").eq(scope.indexFlag).removeClass().addClass("order_wrap_select");
            }
            if (scope.url_param["qq_num"] && scope.url_param["qq_num"] != "" && scope.url_param["qq_num"] != null) {
                $("#qq_num").val(scope.url_param["qq_num"]);
                $("#_bottom_btn").removeClass("bottom_btn_gray");
                scope.isallow = false;
                $("#_toast_pay").show();
                scope.beginOrder();
            }
            //scope.checkqqNull();
        },
        /**
         * 检查qq是否正确，如果不正确弹出浮动层
         * */
        checkqqNull: function () {
            var scope = this;
            var v = $("#qq_num").val();
            var reg = /^\d{5,12}$/;
            if (v != null && v != "") {
                if (!reg.test(v)) {
                    /*alert("请输入正确的QQ号码");*/
                    $("#_toast").show();
                    scope.isallow = false;
                    $("#_bottom_btn").addClass("bottom_btn_gray");
                    return false;
                }
                scope.isallow = true;
                $("#_bottom_btn").removeClass("bottom_btn_gray");
            } else {
                scope.isallow = false;
                $("#_bottom_btn").addClass("bottom_btn_gray");
            }
        },
        /**
         * 1.根据输入的qq号和选择的充值方式获得订单号的方法
         * 2.在次购买时根据传过来的参数获得订单号
         * */
        beginOrder: function () {
            var scope = this;
            var param;
            if (scope.url_param["againpay"] && scope.url_param["againpay"] == 1) {
                param = {
                    rechargeAccount: scope.url_param["qq"],   //充值的QQ号
                    price: scope.url_param["price"],    //价格
                    paytype: scope._paytype,  // 是支付类型，1是财付通，5是微信支付
                    itemtype: "v",    //
                    itemcnt: 1,      //
                    itemId: scope.url_param["itemid"], //商品id
                    packetId: 0, //红包
                    source: "vAPP",
                    sign: "111",
                    dealType: 5
                };
            } else {
                var item = scope.data[scope.indexFlag];
                param = {
                    rechargeAccount: $("#qq_num").val(),   //充值的QQ号
                    price: item.faceValue,    //价格
                    paytype: scope._paytype,  // 是支付类型，1是财付通，5是微信支付
                    itemtype: "v",    //
                    itemcnt: 1,      //
                    itemId: item.itemId, //商品id
                    packetId: 0, //红包
                    source: "vAPP",
                    sign: "111",
                    dealType: 5
                };
            }

            $.ajax({
                url: scope.virtualIn.submitorder,
                type: "GET",
                dataType: "jsonp",
                data: param,
                success: function (data) {
                    if (data.retCode == 0) {
                        scope._pay(data);
                    } else if (data.retCode == 1) {
                        scope._payhide();
                        scope.plogin(location.href);
                    }
                }
            });
        },
        _payhide: function () {
            var scope = this;
            setTimeout(function () {
                scope.isallow = true;
                $("#_toast_pay").hide();
            }, 3000)
        },
        /**
         * 唤起支付
         * */
        _pay: function (order_data) {
            var scope = this;
            var param = {
                deals: order_data.orderId + "~11000",
                payType: "v_wx",
                appToken: unescape(scope.url_param["appToken"]),
                mk: scope.url_param["mk"]
            }
            $.ajax({
                url: scope.virtualIn.payorder,
                type: "GET",
                dataType: "jsonp",
                data: param,
                success: function (data) {
                    if (data.errCode == "0") {
                        data = data.data;
                        var url = "paipai://wxpay?1=1";
                        for (var key in data) {
                            url += "&" + key + "=" + data[key];
                        }
                        url += "&order_num=" + order_data.orderId;
                        scope._payhide();
                        location.href = url;
                    } else {
                        scope._payhide();
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        },
        /**
         * 保存到localStorage中已充值的qq号 localStorage["qq_nums"]
         * */
        rememberNum: function (qq_num) {
            var scope = this;
            var arr = [];
            var qqs = localStorage["_" + scope.url_param["appToken"]];
            if (qqs && qqs != "") {
                var qqarr = qqs.split(",");
                for (var i = 0, len = qqarr.length; i < len; i++) {
                    if (qq_num == qqarr[i]) {
                        qqarr.splice(i, 1);
                        break;
                    }
                }
                if (qqarr.length >= 10) {
                    qqarr.splice(qqarr.length - 1, 1);
                    qqarr.unshift(qq_num);
                } else {
                    qqarr.unshift(qq_num);
                }
                localStorage["_" + scope.url_param["appToken"]] = qqarr;
            } else {
                arr.push(qq_num);
                localStorage["_" + scope.url_param["appToken"]] = arr;
            }
        },
        /**
         * 改变显示页面的方法
         * */
        changePage: function () {
            //$("#_header").show();
            var scope = this;
            //$("#_again_all_inner").width( window.screen.width-100);
            if (scope.url_param["imgurl"]) {
                $("#qqtx").attr("src", "http://img7.paipaiimg.com/2d88b246/" + scope.url_param["imgurl"]);
            }

            $("#_bottom_btn").hide();
            $("#_tab").hide();
            $("#_order_page").hide();
            $("#_confirm_page").show();
            $("#_banner").hide();
        },
        /**
         * 在次购买时，显示的qq号，商家信息，和价钱
         * */
        showInfo: function () {
            var scope = this;
            var pay_qq = scope.url_param["qq"],
                shop_name = scope.url_param["shop_name"],
                pay_value = scope.url_param["price"],
                commodity_desc = scope.url_param["commodity_desc"];
            if (pay_qq != "" && pay_qq != null && typeof pay_qq != "undefined")
                $("#pay_qq").html(pay_qq);
            if (shop_name != "" && shop_name != null && typeof shop_name != "undefined")
                $("#shop_name").html(shop_name);
            if (commodity_desc != "" && commodity_desc != null && typeof commodity_desc != "undefined")
                $("#commodity_desc").html(commodity_desc);
            if (pay_value != "" && pay_value != null && typeof pay_value != "undefined") {
                pay_value = (parseFloat(pay_value) / 100).toFixed(2);
                $("#pay_value").html("￥" + pay_value);
                $("#total_value").html("￥" + (pay_value * parseInt($("#pay_num").html())).toFixed(2));
            }
        },
        /**
         * 绑定点击事件方法
         * */
        bind: function () {
            var scope = this;
            //选择充值类型点击事件
            $("#order div").on("tap", function () {
                scope.pointReport("ptag", scope.points.ptag[5], "type=" + $(this).attr("face_num"));
                $("#order div").removeClass().addClass("order_wrap");
                $(this).removeClass().addClass("order_wrap_select");
                $("#_price").html("￥" + $(this).attr("_price"));
                $("#_discount").html("（" + $(this).attr("_discount") + "折）");
                $("#face_num").html($(this).attr("face_num") + "个/" + $(this).attr("face_num") + "元");
                scope.indexFlag = $(this).attr("indexflag");
                $("#float_div").hide();
                $("#order_wrap_id").css("bottom", "-90px");
            });

            //点击立即充值按钮事件
            $("#btn").on("tap", function () {
                if (!scope.isallow) {
                    return false;
                }
                scope.pointReport("ptag", scope.points.ptag[7], "");
                scope.isallow = false;
                $("#_toast_pay").show();
                var qq_num = $("#qq_num").val();
                scope.rememberNum(qq_num);
                if (!scope.checkLogin()) {
                    scope._payhide();
                    scope.plogin(location.href);
                } else {
                    scope.beginOrder();
                }
                return;
            });
            //头部返回按钮事件
            $("#_header").on("tap", function () {
                $("#_header").hide();
                $("#_order_page").show();
                $("#_confirm_page").hide();
            });
            //确认订单事件
            $("#confirm_btn").on("tap", function () {
                if (!scope.isallow) {
                    return false;
                }
                scope.pointReport("ptag", scope.points.ptag[8], "");
                scope.isallow = false;
                $("#_toast_pay").show();
                scope.beginOrder();
            });
            //点击面值
            $("#face_value").on("click", function () {
                $("#float_div").show();
                var t = 0;
                var fun = function () {
                    var time = setTimeout(function () {
                        t++
                        $("#order_wrap_id").css("bottom", (10 * t - 90) + "px");
                        fun();
                    }, 10)
                    if (t == 9) {
                        clearTimeout(time);
                    }
                }
                fun();
            });
            $("#_opacity_div").on("tap", function () {
                $("#float_div").hide();
                $("#order_wrap_id").css("bottom", "-90px");
            });
            $("._input_qq").on("tap", function () {
                scope.pointReport("ptag", scope.points.ptag[2], "");
                $("#virtual_body").removeClass("body_white");
                var v = $(this).children("span").text();
                $("#qq_num").val(v);
                $("#input_qq").hide();
                $("#qq_num").blur();
                $("#order_select").show();
                setTimeout(function () {
                    $("#_bottom_btn").show();
                }, 600);
                $("#_sure_btn").hide();
                scope.checkqqNull();
            });
            //点击输入框
            $("#qq_num").on("click", function () {
                scope.pointReport("ptag", scope.points.ptag[0], "");
                var qqs = localStorage["_" + scope.url_param["appToken"]];
                if (!qqs) {
                    $("#virtual_body").addClass("body_white");
                }
                $("#_sure_btn").show();
                $("#input_qq").show();

                $("#_bottom_btn").hide();
                $("#order_select").hide();
            });
            //点击确定
            $("#_inner_btn").on("click", function (event) {
                event.stopPropagation();
                event.preventDefault();
                scope.pointReport("ptag", scope.points.ptag[3], "");
                $("#virtual_body").removeClass("body_white");
                $("#input_qq").hide();
                $("#qq_num").blur();
                $("#order_select").show();
                setTimeout(function () {
                    $("#_bottom_btn").show();
                }, 600);
                $("#_sure_btn").hide();
                scope.checkqqNull();
                $("#value_type").focus();
            })
            //清空qq号
            $("#_del_btn").on("click", function () {
                scope.pointReport("ptag", scope.points.ptag[1], "");
                $("#qq_num").val("");
                $("#qq_num").focus();
            });
            $("#_toast").on("tap", function () {
                $(this).hide();
            });
            $(".tab_a").on("tap", function () {
                var target_flag = $(this).attr("target_flag");
                var ver = scope.url_param["ver"];
                location.href = "paipai://redirectPage?target_flag=" + target_flag + "&ver=" + ver;
            })
            /*  $(".order_div").on("click", function (e) {
             var t = $(e.target).attr("class");
             if(t=="_wx_pay"){
             scope._paytype=5;
             $(".cft_pay").removeClass("selected");
             $(".wx_pay").addClass("selected");
             }else if(t=="_cft_pay"||t=="again_cft_pay"){
             scope._paytype=1;
             $(".wx_pay").removeClass("selected");
             $(".cft_pay").addClass("selected");
             }
             });*/
        },
        /**
         * 埋点开始
         * */
        points: {
            upPv: "http://app.paipai.com/api/bi/upPv.xhtml",
            ptag: ["20381.54.1", "20381.54.2", "20381.54.3", "20381.54.4", "20381.54.5", "20381.54.6", "20381.54.7", "20381.54.8", "20381.54.9"]
        },
        getPoint: function (pv_ptag, ptag, click_p) {
            var scope = this;
            var refer = document.referrer;
            var param = "";
            var page_name = location.origin + location.pathname;
            var launchType = sessionStorage["virtual_launchType"] ? "back" : "new";
            sessionStorage["virtual_launchType"] = "1";
            var devicetime = (new Date()).valueOf();
            var isHttp = "",
                duration = "",
                pageParam = "launchType=" + launchType,
                rawPvid = scope.url_param["mk"] + "|" + devicetime + "|" + Math.floor(Math.random() * 10),
                dap = "";
            refer = pv_ptag == "pv" ? document.referrer : "";
            ptag = pv_ptag == "pv" ? "" : ptag;
            click_p = pv_ptag == "pv" ? "" : click_p;
            param = {
                content: devicetime + "|||" + page_name + "|||" + isHttp + "|||" + duration + "|||" + pageParam + "|||" + ptag + "|||" + click_p + "|||" + refer + "|||" + rawPvid + "|||" + dap
            };
            for (var key in scope.url_param) {
                param[key] = scope.url_param[key];
            }
            $.ajax({
                url: scope.points.upPv,
                type: 'GET',
                data: param,
                dataType: 'json',
                timeout: 15000,
                success: function (data) {
                }
            });
        },
        pointReport: function (pv_ptag, ptag, click_p) {
            var scope = this;
            scope.getPoint(pv_ptag, ptag, click_p);
        }
        /**
         * 埋点结束
         * */
    };

    obj.initialize();
    obj.pointReport("pv", "", "");
})();